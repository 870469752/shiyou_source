<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 14-12-15
 * Time: 上午10:49
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
use backend\models\Export;

class ExportController extends Controller{
    public function actionIndex()
    {
        echo 1;die;
    }

    /**
     * 自动导出 报表方法 需要传递相应的参数
     * 根据 export_config 表： 点位id序列， 数据统计类型， 导出时间范围， 路径， 格式， 保存方式
     * 注： 可以没有但需要用空格填充
     * 访问方式： yii export/export-table 参数值（注： 其中point_id、 time_range 需要用数组表示）
     * @param array $point_id           导出点位id的数组    yii export/export-table 1,2,3   => [1, 2, 3];
     * @param $stat_type                统计时间类型        以 时 、 天 、 月 、年 等..
     * @param array $time_range         统计时间范围        包含一个开始时间和一个结束时间的 索引数组 2014-01-01，
     * @param string [$stat_mode]       统计数据函数        默认为 sum group by 后sql中的 聚合函数 max/min/avg/sum
     * @param string [$path]            导出文件路径        默认为 当前安装盘/export 文件下
     * @param string [$format]          导出文件格式        默认为 excel    excel\pdf\cvs\图表
     * @param string [$save_mode]       导出保存方式        默认为本地保存(1)
     */
    public function actionExportTable(array $point_id, $stat_type, array $time_range, $stat_mode = 'sum', $path = 'c:/', $format = 'excel', $save_mode = '1')
    {
        //实例化 模型
        $model = new Export();
        $model->load([
                'point_id' => $point_id,
                'statistics_type' => $stat_type,
                'statistics_range' => $time_range,
                'statistics_mode' => $stat_mode,
                'path' => $path,
                'format' => $format,
                'saving_mode' => $save_mode,
                'method' => 'static'
            ],'');
        $model->autoExport();
    }

} 