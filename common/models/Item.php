<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment $authAssignment
 * @property AuthRule $ruleName
 * @property AuthItemChild $authItemChild
 */
class Item extends \yii\db\ActiveRecord
{
    public $updatedAt;

    public static function tableName()
    {
        return 'core.{{%auth_item}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique', 'message' => '此名称已经存在，换一个吧'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created_at',
            'updated_at' => 'Updated_at',
        ];
    }

    /* public function scenarios()
     {
         return [
             'Empowerment' => ['name', 'description'],
         ];
     }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasOne(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChild()
    {
        return $this->hasOne(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
    *实例化 
    */
    public function createItem($item, $param){
        $auth = Yii::$app->authManager;
        if($param == 1){
            $createItem = $auth->createRole($item);
        }else{
            $createItem = $auth->createPermission($item);
        }
        return array('auth' => $auth, 
                     'create' => $createItem);
    }

    /**
    *添加一个 许可 或 角色
    */
    public function addItem($item, $param){
        $_Item = $this->createItem($item, $param);
        if($param == 1)
            $_Item['create']->description =  '创建了 ' . $item . ' 角色';
        else
            $_Item['create']->description =  '创建了 ' . $item . ' 许可';
        $_Item['auth']->add($_Item['create']);
    }


    /**
     * 创建一个许可
     *
     */
    public function createPermission($item)
    {
        $auth = Yii::$app->authManager;

        $createPost = $auth->createPermission($item);
        $createPost->description = '创建了 ' . $item . ' 许可';
        $auth->add($createPost);
    }

    /**
     * 创建一个角色
     *
     */
    public function createRole($item)
    {
        $auth = Yii::$app->authManager;

        $role = $auth->createRole($item);
        $role->description = '创建了 ' . $item . ' 角色';
        $auth->add($role);
    }

    /**
    * 删除一个角色 或 许可
    */
    public function deleteItem($item, $param){


        //创建 一个 item
         $_Item = $this->createItem($item, $param);
         $_Item['auth']->remove($_Item['create']);

        return true;     
    }

    /**
    *更新 一个角色或许可
    */
    public function updateItem($old_name, $item, $param){
        $_Item = $this->createItem($item->name, $param);
        $_Item['auth']->update($old_name, $_Item['create']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert)
                $this->created_at = $this->updated_at = time();
            else
                $this->updated_at = time();
            return true;
        } else {
            return false;
        }
    }

  
}
