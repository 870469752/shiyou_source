<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'components' => [
        'cache' => [
            //'class' => 'yii\caching\FileCache',
            'class' => 'yii\redis\Cache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '11.29.1.19',
            'port' => '6379',
            'database' => 0,
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=11.29.1.20;dbname=ems',//sI8aNCjRSsa0b_OJQGW4YiIUpHyDwxLJ
//            'dsn' => 'pgsql:host=127.0.0.1;dbname=ems_0304_copy',
            'username' => 'postgres',
            'password' => 'ems_passw0rd',
            'charset' => 'utf8',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
