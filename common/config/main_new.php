<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'components' => [
        'cache' => [
            'class' => [['yii\caching\FileCache'],['yii\caching\MemCache']],
            'servers' =>[
                'host' => '11.29.1.19',
                'port' => 11211
            ]
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;dbname=ems_1221',//sI8aNCjRSsa0b_OJQGW4YiIUpHyDwxLJ
//            'dsn' => 'pgsql:host=127.0.0.1;dbname=ems_0304_copy',
            'username' => 'postgres',
            'password' => 'ems_passw0rd',
            'charset' => 'utf8',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
