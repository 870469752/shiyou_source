<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'HighChartsPointMax' => 300,
    'uploadPath' => 'uploads/',
    'InterfaceAddress' => 'http://127.0.0.1:5000/',
];
