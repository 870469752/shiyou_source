<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 14-12-17
 * Time: 上午11:05
 */

namespace common\library;

use yii\helpers\ArrayHelper;
use Yii;

class MyArrayHelper extends ArrayHelper{

    /**
     * 功能： 将多维数组 转为 一维数组
     * @param $array                目标数组
     * @param string $connection    键值 键名 的连接 方式 :
     *                                                  如果此参数 不为空 将会返回一个以$connection 为连接的 key -- value一维数组
     * @PARAM bool  $is_static      判断 是否 为 静态
     * @param bool $clearRepeated   是否去掉重复
     * @return array|bool           转化成功返回一个 一维数组 | 失败返回 false
     */
    public static function ParseToKeyValueString($array, $connection = '', $is_static = false, $clearRepeated = false)
    {
        if(!isset($array)||!is_array($array)||empty($array)){
            return false;
        }
        if(!in_array($clearRepeated,array('true','false',''))){
            return false;
        }
        if($is_static)
            static $result_array = [];
        else
            $result_array = [];
        foreach($array as $key => $value){
            if(is_array($value)){
                self:: ParseToKeyValueString($value, $connection, $is_static);
            }else{
                if($connection){
                    $result_array[]=$key. $connection . $value;
                }
                else{
                    $result_array[]=$value;
                }
            }
        }
        if($clearRepeated){
            $result_array=array_unique($result_array);
        }
        return $result_array;
    }

    /**
     * 将多维数组合并为一位数组
     * $array:需要合并的数组
     * $clearRepeated:是否清除并后的数组中得重复值
     * @return [array] 返回一个以为数组(去掉键名)
     **/
    public static function array_multiToSingle($array,$clearRepeated=false){
        if(!isset($array)||!is_array($array)||empty($array)){
            return false;
        }
        if(!in_array($clearRepeated,array('true','false',''))){
            return false;
        }
        static $result_array=array();
        foreach($array as $value){
            if(is_array($value)){
                self:: array_multiToSingle($value);
            }else{
                $result_array[]=$value;
            }
        }
        if($clearRepeated){
            $result_array=array_unique($result_array);
        }
        return $result_array;
    }

    /**
     * GoJs 处理方法  将从js端 获取的gojs数组数据 转为 树形数组
     * @param $data                     gojs数据中的 linkDataArray需要将from 对应的nodeDataArray 中的category包含进来
     * @param $to                       上级节点的 编号
     * @return array                    返回一个gojs树形数组
     */
    public static function getGoJsTree($data, $to)
    {
        $tree = [];
        foreach($data as $key => $value){
            if($value['to'] == $to){
                $value['chl'] = self::getGoJsTree($data, $value['from']);
                $tree[$value['from']] = $value;
            }
        }
        return $tree;
    }

    /**
     * GoJs 数组数据的处理
     *
     * @param $data             传递一个 树形的数组数据 数组中 需要包含当前 'from' 在 nodeDataArray 中对应的逻辑元素category
     * @return array            会返回一个所有分子 逻辑组合的 数组： [
     *                                                              0 => '[逻辑元素]' + '[元素名称]' + '[-4]' + '[元素名称]]'
     *                                                              1 => [
     *                                                                    -4 => '[元素名称]' + '[-3]' + '[元素名称]'
     *                                                                    -3 => '[元素名称]' + '[逻辑元素]' + '[元素名称]'
     *                                                                  ]
     *                                                              ]
     *                                                              其中  [逻辑元素] 带表 not\or\and 对应的 key
     *                                                                   [元素名称] 在本项目 代表 event_name , point_name..
     */
    public static function getGoJsRule($data)
    {
        $rule = '';
        static $rule_arr = [];
        foreach($data as $key => $value)
        {
            if(!empty($value['chl']))
            {
                $an = $value['from'];
                foreach($value['chl'] as $k1 => $v1)
                {

                    $v1['dd'] = self::getGoJsRule([$v1]);
                    /*如果有子级  说明当前 元素 可以 被替换为一个逻辑表达式
                            匹配规则： 1、 ‘{}’ 代表 内部 元素 可以被替换
                                      2、 ‘[]’ 代表 内部元素 就是一个 基础点位 或者 逻辑符号*/
                    if(!empty($v1['chl']))
                    {
                        /*判断 是否 是一个新的分支 是否为 not  因为not 需要放在规则字符串的 最前面
                            是
                                $rule .= [当前节点]
                            不是
                                就需要 用上级节点元素 作为 连接的逻辑符号 再进行拼接
                                 $rule .= [逻辑符号] + [当前节点]*/
                        $rule .= (!empty($rule) || $value['category'] == 'not') ? ' ['. $an .'] ({' .$v1['from'] .'}) ' : '({' .$v1['from'] .'})';
                    }
                    else
                    {
                        $rule .= (!empty($rule) || $value['category'] == 'not') ? ' ['. $an .'] ([' .$v1['from'] .']) ' : '([' .$v1['from'] .'])' ;
                    }
                    $rule_arr[$v1['from']] = $v1['dd'][0];
                }
            }
        }
        return array($rule, $rule_arr);
    }

    public static function highchartsMultiUnit($unit){
        foreach($unit as $k => $v){
            $row = explode(',', $v['id']);
            $axis[$k] = [
                'opposite' => $k%2,
                'title' => [
                    'text' => $v['unit']['name']
                ]
            ];
            foreach($row as $point_id){
                $axis_point[$point_id]['rel'] = $k;
                $axis_point[$point_id]['unit'] = $v['unit']['name'];
            }
        }
        $data['axis_point'] = isset($axis_point)?$axis_point:[];
        $data['axis'] = isset($axis)?json_encode($axis, JSON_UNESCAPED_UNICODE):'{title:{text:"无单位"}, opposite:false}';
        return $data;
    }
    
    /**
     * 此方法作用是将 一个复杂的数组 转为 key => name 的形式 其中 name可以为json;
     * 重写 yii\helpers\BaseArrayHelper 下 map 方法的返回值
     * @param  [type] $array [description] 期望传入一个较全的数组 包括 id， $from, $to
     * @param  [type] $from  [description] 数组的键名
     * @param  [type] $to    [description] 数组的键值
     * @param  [type] $group [description] 数组组合方式,可参考  ---- BaseArrayHelper::map();
     * @return [type]        [description]
     */
    public static function _map($array, $from, $to, $group = null){

        $lg = Yii::$app->language;
        //调用 自带方法map 生成一个一维关联数组
        $res = self::map($array, $from, $to, $group);
        //判断当前数组的 键和值 的类型进行处理
        foreach($res as $key => $value){
            /**判断是否存在 name：
             *                  1、存在 就选取当前语言类型的 name；
             *                  2、不存在 就将能够充当 当前数组的元素 作为'name'
             */

            if($value){
                $data = json_decode($value, true);
//				$res[$key] = isset($data['data'][$lg]) ? $data['data'][$lg] : @$data['data']['en-us'];
                if(isset($data['data'][$lg]) && !empty($data['data'][$lg]))
                {
                    $res[$key] = $data['data'][$lg];
                }
                else
                {
                    //如果不存在
                    $lang_name = '';
                    foreach($data['data'] as $k => $v)
                    {
                        if(!empty($v))
                        {
                            $lang_name = $v;
                            break;
                        }
                    }
                    $res[$key] = empty($lang_name) ? $lang_name : 'ID_'.$key;
                }
            }
        }
        return $res;
    }

    /**
     * 此方法作用是将 一个复杂的数组 转为 key => name 的形式 其中 name可以为json; 与_map 区别是 name字段 是 将id 及所有语言的name加以合并
     * 重写 yii\helpers\BaseArrayHelper 下 map 方法的返回值
     * @param  [type] $array [description] 期望传入一个较全的数组 包括 id， $from, $to
     * @param  [type] $from  [description] 数组的键名
     * @param  [type] $to    [description] 数组的键值
     * @param  [type] $group [description] 数组组合方式,可参考  ---- BaseArrayHelper::map();
     * @return [type]        [description]
     */
    public static function _mapDiverse($array, $from, $to, $group = null){
        $lg = Yii::$app->language;
        //调用 自带方法map 生成一个一维关联数组
        $res = self::map($array, $from, $to, $group);
        //判断当前数组的 键和值 的类型进行处理
        foreach($res as $key => $value){
            /**判断是否存在 name：
             *                  1、存在 就选取当前语言类型的 name；
             *                  2、不存在 就将能够充当 当前数组的元素 作为'name'
             */

            if($value){
                $data = json_decode($value, true);
//				$res[$key] = isset($data['data'][$lg]) ? $data['data'][$lg] : @$data['data']['en-us'];
                $res[$key] = $key;
                $res[$key] .= "  |  " . (isset($data['data']['zh-cn']) && !empty($data['data']['zh-cn'])? $data['data']['zh-cn'] : 'None');
                $res[$key] .= "  |  " . (isset($data['data']['en-us']) && !empty($data['data']['en-us'])? $data['data']['en-us'] : 'None');
            }
        }
        return $res;
    }

} 