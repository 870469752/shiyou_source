<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 14-12-10
 * Time: 下午2:41
 * 导出 文件方式上 分为 静态导出 和 网页导出
 */
namespace common\library;

use common\library\MyFunc;

class MyExport {

    /**
     * 运行导出
     * @param $data 数据库查出来的数据
     * @param array $header 列名 格式：[['name', 'name', 'name']]
     * 配置项格式：
     * [
     *      'save_file' => [
     *          'file_name' => '报表',
     *          'file_type' => 'pdf',
     *          'path' => 'c:/',
     *      ],
     *      'send_email' => [
     *          'to_email' => 'name@163.com'
     *      ]
     * ]
     * @param array $options
     */
    public static function run($data, $header = [], $options = [])
    {

        if(isset($options['save_file']) || $options == []){ // 默认保存文件
            $file_name = isset($options['save_file']['file_name'])?$options['save_file']['file_name']:'报表';
            $file_title = isset($options['save_file']['title'])?$options['save_file']['title']:'';
            $file_type = isset($options['save_file']['file_type'])?$options['save_file']['file_type']:'xls';
            $obj = self::generateExportObject($data, $header, $file_title);
            if(isset($options['save_file']['path'])){ // 如果有路径就导出到本地
                $path = $options['save_file']['path'];
                // 不存在目录就创建
                if(! file_exists ($path))
                {
                    mkdir($path);
                }
                switch($file_type)
                {
                    case 'xls':
                        $objWriter = new \PHPExcel_Writer_Excel5($obj);
                        echo $objWriter->save($path.'/'.iconv('utf-8', 'gbk//ignore', $file_name).'.xls');
                        break;
                    case 'pdf':
                        break;
                }
            } else{ // 在网页上保存
                switch($file_type)
                {
                    case 'xls':
                        header('Content-Type: application/vnd.ms-excel;');
                        header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
                        $objWriter = new \PHPExcel_Writer_Excel5($obj);
                        $objWriter->save('php://output');
                        break;
                    case 'pdf':
                        break;

                }
            }
        }

    }

    /**
     * 生成导出对象
     * @param $data
     * @param $header
     * @return \PHPExcel
     */
    public static function generateExportObject($data, $header, $file_title)
    {
        // 创建PHPExcel对象，此对象包含输出的内容及格式
        $obj_excel = new \PHPExcel();
        // 创建一个 工作表
        $obj_excel->createSheet();
        //操作 工作表
        $obj_excel->setActiveSheetIndex(0);
        //保护单元格
        $obj_excel->getActiveSheet()->getProtection()->setSheet(true);
        //设置标题
//        $obj_excel->getActiveSheet()->setTitle($file_title);
        //设置 宽度
        $obj_excel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
        $obj_excel->getActiveSheet()->getColumnDimension()->setWidth(15);
        $obj_excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
        //数据处理
        $obj_excel->getActiveSheet()->fromArray(
            !empty($header)?array_merge($header,$data):$data
        );
        // 设置默认字体和大小
        $obj_excel->getDefaultStyle()->getFont()->setSize(10);
        $obj_excel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLACK);

        return $obj_excel;
    }
}