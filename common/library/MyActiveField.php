<?php
/**
 * Created by PhpStorm.
 * User: DUCAT
 * Date: 14-11-11
 * Time: 上午9:51
 */

namespace common\library;

use Yii;
use yii\widgets\ActiveField;


class MyActiveField extends ActiveField {

    public function dropDownList($items, $options = [])
    {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = MyHtml::activeDropDownList($this->model, $this->attribute, $items, $options);

        return $this;
    }

    public function hiddenInput($options = [])
    {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = MyHtml::activeHiddenInput($this->model, $this->attribute, $options);
        $this->template = '{input}';
        return $this;
    }

} 