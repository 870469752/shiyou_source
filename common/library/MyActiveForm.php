<?php
/**
 * Created by PhpStorm.
 * User: DUCAT
 * Date: 14-11-11
 * Time: 下午1:20
 */

namespace common\library;

use Yii;
use yii\widgets\ActiveForm;

class MyActiveForm extends ActiveForm{

    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        if (!isset($this->fieldConfig['class'])) {
            $this->fieldConfig['class'] = MyActiveField::className();
        }
        echo MyHtml::beginForm($this->action, $this->method, $this->options);
    }

    public function field($model, $attribute, $options = [])
    {
        return Yii::createObject(array_merge($this->fieldConfig, $options, [
            'model' => $model,
            'attribute' => $attribute,
            'form' => $this,
        ]));
    }
} 