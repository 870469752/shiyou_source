<?php
/**
 * @link http://www.gcxj.com/
 * @copyright Copyright (c) 2014 gcxj
 * @license http://www.gcxj.com/license/
 */

namespace common\library;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;
use Yii\web\View;
use backend\models\ModuleRole;
use backend\models\authAssignment;
use yii\web\ForbiddenHttpException;

/**
 * 项目常用的数据处理方法
 *
 * 不要随意添加，必须先查看内部有无相同方法。
 *
 * @author 葛盛庭 <ducooo@163.com>
 * @since 1.0
 */
class MyFunc extends MyArrayHelper
{
    /**
     * SmartAdmin 自带的递归菜单方法
     * @param array $nav_item
     * @return string
     */
    public static function process_sub_nav($nav_item)
    {
        $language = Yii::$app->language;
//        $request_url = self::getRequestUrl();
        $request_url = Url::to();
        $sub_item_htm = "";
        if (isset($nav_item["sub"]) && $nav_item["sub"]) {
            $sub_nav_item = $nav_item["sub"];
            $sub_item_htm = static::process_sub_nav($sub_nav_item);
        } else {
            $sub_item_htm .= '<ul>';
            // 循环递归生成菜单
            if(is_array($nav_item)){
                foreach ($nav_item as $key => $sub_item) {
                    //判断是否设置了url 如果有则生成
                    $url_data = !empty($sub_item["url"]) ? Url::toRoute('/' . $sub_item["url"]) : "#G1049";
                    //判断是否含有参数
                    $url_param = MyFunc::DisposeJSON($sub_item['param']);
                    //将菜单名称转为数组
                    $title_name = json_decode($sub_item['description'], true);
                    $url = !empty($url_param) ? $url_data.'?'.$url_param : $url_data;
                    // 菜单注释
                    $url_target = isset($sub_item["url_target"]) ? 'target="' . $sub_item["url_target"] . '"' : "";
                    // 图标
                    $icon = !empty($sub_item["icon"]) ? '<i class="fa fa-lg fa-fw ' . $sub_item["icon"] . '"></i>' : "";
                    // 菜单名称处理
                    $nav_title = isset($title_name['data']["$language"]) ? $title_name['data']["$language"] : YII::t('app', '(No Name)');
                    // 菜单名称扩展
                    $label_htm = isset($sub_item["label_htm"]) ? $sub_item["label_htm"] : "";
                    // 判断活跃菜单的后缀
                    $suffix_url = '/';
                    // 如果没有权限就不显示 没有链接的菜单
                    if (!empty($sub_item['url']) && strpos($url, '#G1049') === false) {
                        // 如果没有权限就不显示  || \Yii::$app->user->id == 0
                        if (  \Yii::$app->user->id == 0 ||
                            ((empty($sub_item['user_id']) ||
                                    Yii::$app->user->id == $sub_item['user_id'])/* &&
                                ModuleRole::accessAuth($sub_item["url"])*/ ) ) {
                            // 拼合菜单标签字符串
                            if(Yii::$app->params['i'] != -1){

                                /**
                                 * 如果存储的连接 和访问的连接 一样 就将其前面的活跃的连接去掉
                                 * 可能某个菜单的连接时另一个功能访问连接的子集
                                 * 如： user/update?id=4 是 user/update 的子集，当前访问 user/update?id=4
                                 * 所以当菜单中既含有 user/update?id=4 也含有user/update时 需要将user/update活跃移除
                                 */
                                if(!strcasecmp($request_url,$url)){
                                    $sub_item_htm = str_replace('active', '', $sub_item_htm);
                                    Yii::$app->params['i'] = -1;
                                    $suffix_url = '';
                                }

                                /**
                                 * 添加活跃连接
                                 * 一般的模块只有一个菜单即 index 为了满足该模块下所有功能的访问，
                                 * 都使该模块被活跃，那么只能匹配部分
                                 * 如：菜单中的连接时user/index 但访问了user/update(并没有该连接的菜单)
                                 * 但该链接是user下的所以应当将user/index该菜单激活
                                 *
                                 * 注意：
                                 * 防止可能会匹配连接的 一部分
                                 * 如：point/crud/update?id=1 和 crud/update?id=1 可能
                                 * 前者是模块下的功能 后者是基础功能，如果访问point/crud/update?id=1也会使crud/update?id=1活跃，
                                 * 所以我考虑在起前面加上 '--'简单区分
                                 *
                                 */

                                if(trim($request_url) == trim($url)){
                                    $sub_item_htm .= '<li class = "active">';
                                }else{
                                    if((strpos('--' . $request_url, '--' . $url .$suffix_url)!==false)){
                                        $sub_item_htm .= '<li class = "active">';
                                    }else{
                                        $sub_item_htm .='<li class = "">';
                                    }
                                }

                                $sub_item_htm .= '<a href="' . $url . '" ' . $url_target . '>' . $icon . " <span>" . $nav_title . '</span>' . $label_htm . '</a>'
                                    . (isset($sub_item["sub"]) ? static::process_sub_nav($sub_item["sub"]) : '')
                                    . '</li>';

                                /*$sub_item_htm .= '<li '
                                    . (strpos('--' . $request_url, '--' . $url .$suffix_url) !== false ? 'class = "active"' : '') . '>'
                                    . '<a href="' . $url . '" ' . $url_target . '>' . $icon . " <span>" . $nav_title . '</span>' . $label_htm . '</a>'
                                    . (isset($sub_item["sub"]) ? static::process_sub_nav($sub_item["sub"]) : '')
                                    . '</li>';*/
                            }
                            else{
                                $sub_item_htm .= '<li>'
                                    . '<a href="' . $url . '" ' . $url_target . '>'. $icon  . " <span>" . $nav_title . '</span>' . $label_htm . '</a>'
                                    . (isset($sub_item["sub"]) ? static::process_sub_nav($sub_item["sub"]) : '')
                                    . '</li>';
                            }
                        }
                    }
                    else{
                        $sub_item_htm .= '<li>'
                            . '<a href="javascript:void(0)" ' . $url_target . '>' . $icon
                            . ($sub_item['parent_id'] == 0 ? "<span class = 'menu-item-parent'>" : "<span>")
                            . $nav_title . '</span>' . $label_htm . '</a>'
                            . (isset($sub_item["sub"]) ? static::process_sub_nav($sub_item["sub"]) : '')
                            . '</li>';
                    }
                }

            }
            $sub_item_htm .= '</ul>';
        }
        return $sub_item_htm;
    }

    /**
     * 获取 当前完整 请求
     * @return string
     */
    public static function getRequestUrl()
    {
        //请求模块 控制器 方法
        $request_url = preg_replace('#^/#', '',Url::to());
        //请求参数
        if(\Yii::$app->request->isPost)
        {
            $param = Yii::$app->request->post();
        }
        else if(\Yii::$app->request->isGet)
        {
            $param = Yii::$app->request->get();
        }
        //将连接 与 参数合并
        if(is_array($param) && !empty($param))
        {
            $param_arr = self::ParseToKeyValueString($param, '=');
            $param_str = implode('&', $param_arr);
            //拼接
            $request_url = $request_url .'?'. $param_str;
        }
        return $request_url;
    }



	/**
	 * 递归菜单数据 获得分类后的层级菜单
	 * @param  array  $data 操作数组
	 * @param  integer $pId  父级id
	 * @return array       排序后的数组
	 */
	public static function getTreeArray( $data, $pId = 0 )
    {

		$tree = array();
		foreach ( $data as $key => $value ) {  
			if(isset($value['depth']) && $value['depth'] == 0)
				continue;
//			$_data = json_decode($value['description'], true);
//			$value['title'] = $_data['data'];
			if ( $value['parent_id'] == $pId ) {
				if(isset($value['id']) && $sub = static::getTreeArray( $data, $value['id'] )){
                    foreach($sub as $v){
                        $url = trim($v['url']);
                        if(!empty($url)){
                            $value['sub'][] = $v;
                            $value['url'] = '#G1049';
//                            $value['url'] = !empty($value['url']) ? $value['url'] : '#';                      此项是可以将任何菜单都可以含有连接
                        }
                    }

                }
                if(isset($value['sub']) || !empty($value['sub']) || isset($value['url'])){
                    $tree[] = $value;
                }
			}
		}
		return $tree;  
	} 

    public static function analyse_json($arr)
    {
        foreach($arr as $key => $value){
            $arr[$key]['name'] = self::DisposeJSON($value['name']);
        }

        return $arr;
    }


	/**
	 * 各种不同类型JSON的处理
	 * @param json $JSON
	 * @return array
	 */
	public static function DisposeJSON($JSON)
    {
		$lg = Yii::$app->language;
        if(($arr = json_decode($JSON, true)) && isset($arr['data_type'])){ // 提交过来的数据是否为JSON格式,数据类型是否存在

            switch (@$arr['data_type'])
            {
                case 'description':
                    return isset($arr['data'][$lg])?$arr['data'][$lg]:'-';
                break;
                case 'formula':
                    return [
                                'formula' => isset($arr['temp'][$lg])?$arr['temp'][$lg]:'[No Formula]',
                                'unit' => [
                                    'calculate' => isset($arr['unit']['calculate'])?$arr['unit']['calculate']:'',
                                    'result' => isset($arr['unit']['result'])?$arr['unit']['result']:''
                                ]
                            ];
                    break;
                case 'list':
                    return isset($arr['data']) && !empty($arr['data'])?implode(',', $arr['data']):'';
                break;
                case "field": // 字段名称展示
                    return implode(',', static::arrayTwoOne($arr['data'], 'name'));
                break;
                case 'action_info':
					return implode(array_keys($arr['data'][0]), ',');
                break;
                case 'url_param':
                    return http_build_query($arr['data']);
                break;
                default:
                    return $arr['data'];
            }
        }else{ // 如果无格式那就直接返回JSON
            return $JSON;
        }
	}

    /**
     * 时间解析 方法 对json格式的 时间字符串进行处理 返回一个处理好的json字符串
     * @param string $tiem_type 时间类型
     * @param String $time_range 时间字符串
     * @return json 返回一个json
     */
    public static function TimeAnalysis($time_str, $time_type)
    {
        $time_range['data_type'] = 'time';
        $time_range['data']['type'] = $time_type;
        $str_time = trim($time_str, ';');
        $time_arr = explode(';', $str_time);

        foreach($time_arr as $key => $value)
        {
            $value = trim($value, ',');
            $_time = explode(',', $value);
            $time_range['data']['time_range'][$key]['start'] = $_time[0];
            $time_range['data']['time_range'][$key]['end'] = !empty($_time[1]) ? $_time[1] : $_time[0];
        }

        return json_encode($time_range);
    }

	/**
	 * 修改 json中的某个元素
	 * @param  [type] $new      新的值
	 * @param  String $old_Json   json字符串
	 * @return Json           返回新的json
	 */
	public static function updateJson($new, $old_Json, $param_key = '')
    {
		$lg = empty($param_key) ? Yii::$app->language : $param_key;
		$arr = JSON::decode($old_Json);
		switch ($arr['data_type']){
			case 'description':
				$arr['data'][$lg] = trim($new);
				break;
		}
        return MyFunc::JsonTransfer($arr);
	}

	/**
	 * 创建 对应类型的json 
	 * @param  [type] $value [description]
	 * @param  string $type  [description]
	 * @return [type]        [description]
	 */
	public static function createJson($data, $type = 'description', $param_key = '')
    {
		switch($type){
			case 'description':
				$new_data = array('data_type' => 'description',
                                 'data' => [
                                     (empty($param_key) ? \Yii::$app->language : $param_key) => trim($data)
                                    ],
                                 );
				break;
			case 'time':
				$new_data = array('data_type' => 'time',
								  'data' => [
								  	'type' => $data['type'],
								  	'time_range' => $data['time_range'],
								  	],
								);
				break;
			case 'trigger':
				$new_data = array('data_type' => 'trigger',
								  'data' => $data,
								);
                break;
            case 'value_pattern':
                $new_data = array('data_type' =>'value_pattern',
                                  'data' => [
                                      'cycle' => $data['type'],
                                      'value' => $data['value'],
                                  ]);
                break;
            case 'base_history':
                $new_data = array('data_type' =>'base_history',
                                    'data' => [
                                        $data,
                                    ]);
                break;
            case 'list':
                $new_data = array('data_type' => 'list',
                                  'data' => $data,
                                );
                break;
            case 'seasonal':
                $new_data = array('data_type' => 'seasonal',
                                    'data' => $data,
                                );
                break;
            case 'schedule_mode':
                $new_data = array('data_type' => 'schedule_mode',
                                   'data' => [
                                                'type' => $data->mission_mode,
                                                $data->mission_mode => isset( $data->export_interval_time ) && !empty( $data->export_interval_time ) ?
                                                                            ( $data->mission_mode == 'interval' ? intval( $data->export_interval_time ) : $data->export_interval_time ) :
                                                                            ( $data->mission_mode == 'interval' ? 86400 : '0 1 * * *' ),
                                                'start_date' => date('Y-m-d H:i:s', strtotime($data->export_start)),
                                                'end_date' => date('Y-m-d H:i:s', strtotime($data->export_end)),
                                   ]
                                );
                break;
            case 'args_export':
                $new_data = array('data_type' => 'task_args',
                    'data' => [
                        'point_id' => $data->export_ids,
                        'statistic_gap' => isset($data->statistic_time) && !empty($data->statistic_time) ? intval($data->statistic_time) : 3600,
                        'export_args' => [
                            'type' => $data->export_type,
                            'title' => $data->export_title,
                            'water_mark' => $data->export_water_mark,
                            'table_of_contents' => $data->export_directory,
                            'heading_order' => $data->export_order,
                        ],
                        'export_type' => [
                            'type' => $data->export_method,
                            'file_name' => ''
                        ]
                    ]
                );
                if($data->export_type != 'pdf')
                {
                    unset($new_data['data']['export_args']['water_mark']);
                    unset($new_data['data']['export_args']['table_of_contents']);
                    unset($new_data['data']['export_args']['heading_order']);
                }
                break;
            case 'args_db_backup':
                $new_data = ['data_type' => 'task_args',
                    'data' => [
                        'data_backup_mode' => $data->db_backup_type
                    ]
                ];
                break;
            case 'action_info':
                $new_data = [
                    'data_type' => 'action_info',
                    'data' => $data,
                ];
                break;
            case 'time_span':
                $new_data = [
                    'data_type' => 'time_span',
                ];
                $new_data['data']['start'] = self::createJsonByTimeSpan($data, 'start_time');
                $new_data['data']['end'] = self::createJsonByTimeSpan($data, 'end_time');
                break;
            default:
                $new_data = [
                    'data_type' => 'report_point',
                    'data' => $data,
                ];
		}

		return MyFunc::jsonTransfer($new_data);
	}

    /**
     * 创建json时 类型是time_span的处理
     * @param $data
     * @param $type
     * @return array
     */
    public static function createJsonByTimeSpan($data, $type)
    {
        if(! $data){ return [];}
        $_type = $type .'_type';
        if($data->$_type == 'absolute') {
            //如果是 绝对时间 直接返回数据
            return [
                        'type' => 'absolute',
                        'timestamp' => $data->$type
            ];
        } else {
            //如果是相对时间 需要判断 是否为自定义
            $custom_type = 'custom_' .$type .'_type';
            $custom_num = 'custom_' .$type .'_num';
            if($data->$type != 'custom') {
                //如果不是自定义 那么时间数据只会存在 start_time_value 中 需要做进一步的数据处理
                $relative = explode(',', $data->$type);
                $custom_type = $relative[1];
                $custom_num = $relative[0];
            }
            return [
                        'type' => 'relative',
                        // 如果是自定义就从 对象中取值 否则从数组中取得
                        'unit' => isset($data->$custom_type) ? $data->$custom_type : $custom_type,
                        'value' => isset($data->$custom_num) ? intval($data->$custom_num) : intval($custom_num),
            ];
        }
    }

    /**
     * 利用系统自带的 查找数组中键名对应的值 getValue， 求json 中某个键名对应的 键值
     * @param $json
     * @param $key
     * @return mixed
     */
    public static function JsonFindValue($json, $key)
    {
        $arr_data = json_decode($json, true);
        $result = MyArrayHelper::getValue($arr_data, $key);
        return $result;
    }


	/**
	 * 将含有中文的数组转为json时 不会发生编码错误
	 * @param Array $array 需要转为json的数组
	 */
	public static function JsonTransfer($array)
    {
//		MyFunc::arrayRecursive($array, 'urlencode', true);
		$_json = json_encode($array);
		return urldecode($_json);
	}

	/**
	 * 给每一个数组 作用 某个方法
	 * @param  [type]  $array              [description]
	 * @param  [type]  $function           [description]
	 * @param  boolean $apply_to_keys_also [description]
	 * @return [type]                      [description]
	 */
	public static function arrayRecursive(&$array, $function, $apply_to_keys_also = false)  
	{  
	    static $recursive_counter = 0;  
	    if (++$recursive_counter > 1000) {  
	        die('possible deep recursion attack');  
	    }  
	    foreach ($array as $key => $value) {  
	        if (is_array($value)) {  
	            MyFunc::arrayRecursive($array[$key], $function, $apply_to_keys_also);  
	        } else {  
	            $array[$key] = $function($value);  
	        }  
	   
	        if ($apply_to_keys_also && is_string($key)) {  
	            $new_key = $function($key);  
	            if ($new_key != $key) {  
	                $array[$new_key] = $array[$key];  
	                unset($array[$key]);  
	            }
	        }  
	    }  
	    $recursive_counter--;  
	}

	/**
	 * 查询JSON数据的时候字段格式
	 * @param string $field
	 * @param string $data_type
	 * @return string
	 */
	public static function QueryJSON($field, $data_type)
    {
		switch ($data_type)
		{
			case "description":
				return $field."->'data'->>'".Yii::$app->language."'";
				break;
		}
	}


    /**
 * 获取树状数据
 * @param $data
 * @param int $pid
 * @return array
 */
    public static function TreeData($data, $pid = 0)
    {
        $tree = [];
        foreach($data as $k => $v){
            if($v['id'] == 0){ // 如果是顶级就跳过
                continue;
            }
            if($v['parent_id'] == $pid){ // 找到多个PID等于传递值的存起来
                if($sub = static::TreeData($data, $v['id'])){ // 如果没有就不赋值
                    $v['sub'] = $sub;
                }
                $tree[$k] = $v;
            }
        }
        return $tree;
    }

    /**
     * 向分类中添加擦你但需要的参数 如 url  param
     * @param $data         所有分类数据
     * @param $parent_ids   所有父级元素id
     * @param int $pid      当前遍历的父级id
     * @param int $level    分类层级
     * @param int $menu_level 允许展示的最大层级数
     * @return array
     */
    public static function CategoryOfMenu($data, $parent_ids = [], $pid = 0, $menu_level = 3, $level = 0)
    {
        $tree = [];
        $level ++;
        foreach($data as $k => $v){
            if($v['id'] == 0 || $level > $menu_level || !in_array($v['id'], $parent_ids)){ // 如果是顶级 或者 超过用户定义的层级就跳过 或者 当前分类没有子分类
                continue;
            }
            $v['url'] = 'category-data/analyze/statistical-analysis';
            $url_params = ['data_type' => 'url_param', 'data' => [
                                                                    'parent_category_ids' => $v['id'],
                                                                    'tag' => $level == $menu_level ? 1 : 0
                                                                  ]
                           ];
            $v['param'] = json_encode($url_params);
            $v['icon'] = $level == 1 ? 'fa-folder-open' : '';
            $v['description'] = $v['name'];
            if($v['parent_id'] == $pid){ // 找到多个PID等于传递值的存起来
                if($sub = static::CategoryOfMenu($data, $parent_ids, $v['id'],  $menu_level, $level)){ // 如果没有就不赋值
                    $v['sub'] = $sub;
                }
                $tree[$k] = $v;
            }
        }
        return $tree;
    }




    /**
     * 获取 fancytree 插件的json数据格式
     * @param $data
     * @return json
     */
    public static function get_fancyTree_data($data, $pid = 0)
    {
        $tree = [];
        foreach($data as $k => $v){
            $v['title'] = self::DisposeJSON($v['title']);
            if($v['key'] == 0){ // 如果是顶级就跳过
                continue;
            }
            if($v['parent_id'] == $pid){ // 找到多个PID等于传递值的存起来
                unset($v['parent_id']);
                if($sub = static::get_fancyTree_data($data, $v['key'])){ // 如果没有就不赋值
                    $v['hideCheckbox'] = true;
                    $v['children'] = $sub;
                }

                $tree[] = $v;
            }

        }
        return $tree;
    }



    /**
     * 获取 fancytree 插件的json数据格式
     * @param $data
     * @return json
     */
    public static function get_fancyTree_data1($data, $pid = 0)
    {
        $tree = [];
        foreach($data as $k => $v){
            $v['title'] = self::DisposeJSON($v['title']);
            if($v['key'] == 0){ // 如果是顶级就跳过
                continue;
            }
            if($v['parent_id'] == $pid){ // 找到多个PID等于传递值的存起来
                unset($v['parent_id']);
                if($sub = static::get_fancyTree_data($data, $v['key'])){ // 如果没有就不赋值
                    //$v['hideCheckbox'] = true;
                    $v['children'] = $sub;
                }

                $tree[] = $v;
            }

        }
        return $tree;
    }
    /**
     * 表格搜索HTML和JS
     * @param object $searchModel
     */
    public static function TableSearch($searchModel)
    {
        $model_name = static::getLastName(get_class($searchModel));;
        unset($arr);
        $field = '<select class="form-control" name="' . $model_name . '[field][]" required>';
        foreach ($searchModel->attributeLabels() as $k => $v) {
            if ($k != 'id') {
                $field .= '<option value="' . $k . '">' . $v . '</option>';
            }
        }
        $field .= '</select>';
        $condition = '

			<div class="form-group">
				<div class="row">
					<div class="col-md-4">
						' . $field . '
					</div>
					<div class="col-md-3">
						<select class="form-control" name="' . $model_name . '[relation][]" required>
							<option value="=">' . YII::t('app', 'EQ') . '</option>
			    			<option value="like">' . YII::t('app', 'Like') . '</option>
							<option value=">">' . YII::t('app', 'GT') . '</option>
							<option value="<">' . YII::t('app', 'LT') . '</option>
							<option value=">=">' . YII::t('app', 'EGT') . '</option>
							<option value="<=">' . YII::t('app', 'ELT') . '</option>
							<option value="not like">' . YII::t('app', 'Not like') . '</option>
							<!--
							<option value="in">' . YII::t('app', 'Include') . '</option>
							<option value="not in">' . YII::t('app', 'Not include') . '</option>
							-->
						</select>
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" name="' . $model_name . '[value][]" required />
					</div>
					<div class="col-md-1" onclick="$(this).parent().parent().remove()">
						<span class="close" style="font-size:32px">×</span>
					</div>
				</div>
			</div>
		';
        $condition = preg_replace("/\r\n|\t/", "", $condition);

        $js_content = <<<JAVASCRIPT
var condition = '{$condition}';
$('#add_condition').click(function(){
	$('#condition').append(condition);
});
JAVASCRIPT;

        // 条件保持
        foreach ($searchModel->value as $i => $v) {
            $field = $searchModel->field[$i];
            $relation = $searchModel->relation[$i];
            $js_content .= <<<JAVASCRIPT
$('#condition').append(condition);
$('[name="{$model_name}[field][]"]:eq({$i}) [value="{$field}"]').attr('selected','selected');
$('[name="{$model_name}[relation][]"]:eq({$i}) [value="{$relation}"]').attr('selected','selected');
$('[name="{$model_name}[value][]"]:eq({$i})').val('{$v}');
JAVASCRIPT;
        }
        // 如果一个条件都没有就创建一个新的。
        if (count($searchModel->value) == 0) {
            $js_content .= "$('#condition').append(condition);";
        }

        $view = Yii::$app->getView();
        $view->registerJs($js_content, View::POS_READY);

        $search_box = '
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form" role="form">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">' . YII::t('app', 'Condition Search') . '</h4>
							</div>
							<div class="modal-body">
								<div class="well" id="condition">
					
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									' . YII::t('app', 'Cancel') . '
								</button>
								<button type="button" class="btn btn-success" id="add_condition">
									' . YII::t('app', 'Add condition') . '
								</button>
								<button type="submit" class="btn btn-primary">
									' . YII::t('app', 'Search') . '
								</button>
							</div>
						</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		';
        echo $search_box;
    }

    /**
     * 取路径的最后名称
     * @param string $path
     * @return string
     */
    public static function getLastName($path, $suffix = '')
    {
        if (($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) == $suffix) {
            $path = mb_substr($path, 0, -$len);
        }
        $path = rtrim(str_replace('\\', '/', $path), '/\\');
        if (($pos = mb_strrpos($path, '/')) !== false) {
            return mb_substr($path, $pos + 1);
        }

        return $path;
    }

    /**
     * 把二维数组或对象中某个值作为键名
     * @param array $arr 数组或对象都可以
     * @param string $key
     * @param array $options 改变键名如name改为title: ['name' => 'title']
     * @return array
     */
    public static function ArrayIndexBy($arr, $key, $options = [])
    {
        $data = [];
        foreach ($arr as $row) {
            foreach ($row as $k => $v) {
                $k = isset($options[$k]) ? $options[$k] : $k;
                $new_row[$k] = $v;
            }
            $data += [$row[$key] => $new_row];
        }
        return $data;
    }



    /**
     * 利用 将一维数组转为
     * 以 ','分割 带 ‘’ 的字符串：$leaf_str = "'a','b','c'""
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public static function getArr2Str($arr)
    {
        $str = '';
        foreach ($arr as $key => $value) {
            $str .= "'" . $value . "',";
        }
        return trim($str, ',');
    }

    /**
     * 获取当前访问 地址对应的 操作权限
     * @return [type] [description]
     */
    public static function getPerate($rid_action)
    {
        $UserId = yii::$app->user->identity->id;
        $role = authAssignment::getOperate($UserId, $rid_action);
        if (!$role) {
            return 2;
        }
        return $role->operate;
    }

	/**
	* 将多维数组合并为一维数组
	* $array:需要合并的数组
	* $clearRepeated:是否清除并后的数组中得重复值
    * $is_static: 是否数组为静态 静态保存上次的值
	* @return [array] 返回一个一维数组(去掉键名)
**/
	public static function array_multiToSingle($array,$clearRepeated=false, $is_static = true){
	    if(!isset($array)||!is_array($array)||empty($array)){
	        return false;
	    }
	    if(!in_array($clearRepeated,array('true','false',''))){
	        return false;
	    }
        if($is_static)
	        static $result_array=array();
        else
            $result_array=array();
	    foreach($array as $value){
	        if(is_array($value)){
	           self:: array_multiToSingle($value);
	        }else{
	            $result_array[]=$value;
	        }
	    }
	    if($clearRepeated){
	        $result_array=array_unique($result_array);
	    }
	    return $result_array;
	}

    /**
     * 遍历 相关文件夹 下的文件
     * @param string $path 文件路径
     * @param string $type 文件类型
     * @return array
     */
    public static function getAllLang($path = '../../backend/messages', $type = '/*')
    {
        $file = [];
        foreach(glob($path.$type) as $single)
        {
            if(is_dir($single))
            {
                $file = array_merge($file, MyFunc::getAllLang($single, $type));
            }
            else{
                $arr = explode('/', $single);
                $count = count($arr);
                $file[$arr[$count - 2]][] = $arr[$count - 1];
            }
        }
        return $file;
    }

    /**
     * 有些地方不能通过 yii自带的方法 来获取但可以用 $_SERVER;
     * @return string
     */
    public static function getCurUrl($server = '')
    {
        $url = empty($server) ? (isset($server['REQUEST_URI']) ? $server['REQUEST_URI'] : Yii::$app->language) :$server;
        if(strpos($url ,'en-us'))
        {
            $lg = 'en-us';
        }
        else
        {
            $lg = 'zh-cn';
        }
        return $lg;
    }

    /**
     * 将数组或对象中两个属性转换成[键 => 值]对（主要用于类似下拉菜单）
     * @param $data
     * @param $key
     * @param $value
     * @return array
     */
    public static function dataKV($data, $key, $value, $dispose = true)
    {
        if ($dispose) { // 处理JSON或不处理
            foreach ($data as $v) {
                $array[$v[$key]] = static::DisposeJSON($v[$value]);
            }
        } else {
            foreach ($data as $v) {
                $array[$v[$key]] = $v[$value];
            }
        }
        return $array;
    }

    /**
     * 根据数组中的某个键值大小进行排序，仅支持二维数组
     *
     * @param array $array 排序数组
     * @param string $key 键值
     * @param bool $asc 默认正序
     * @return array 排序后数组
     */
    public static function arraySortByKey(array $array, $key, $asc = true)
    {
        $result = array();
        // 整理出准备排序的数组
        foreach ($array as $k => &$v) {
            $values[$k] = isset($v[$key]) ? $v[$key] : '';
        }
        unset($v);
        // 对需要排序键值进行排序
        $asc ? asort($values) : arsort($values);
        // 重新排列原有数组
        foreach ($values as $k => $v) {
            $result[$k] = $array[$k];
        }

        return $result;
    }

    /**
     * 递归输出树状方块HTML
     * @param array $data
     * @return string
     */
    public static function NestableRecursion($data)
    {
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-title=' . $v['title'] . '>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">' . (MyFunc::DisposeJSON($v['title'])?MyFunc::DisposeJSON($v['title']):'请点击修改名称') . '</span>
								<em class="close pull-right " style="padding-left:10px;">×</em>
								<a href="javascript:void(0)" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="left" data-original-title="' . Yii::t('app', 'Add Subclass') . '">
									<i class="fa fa-sitemap fa-fw"></i>
								</a>
							</div>';
            $html .= isset($v['sub']) ? static::NestableRecursion($v['sub']) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;
    }

    /**
     * 时间单位换算
     * @param $value
     * @param array $unit
     * @return bool|string
     */
    public static function TimeUnitTransition($value, $unit = ['second', 'minute'])
    {
        if ($value > 0) {
            switch ($unit[0]) {
                case "millisecond":
                    $value = floor($value);
                    break;
                case "second":
                    $value = floor($value) * 1000;
                    break;
                case "minute":
                    $value = floor($value * 1000 * 60);
                    break;
                case "hour":
                    $value = floor($value * 1000 * 60 * 60);
                    break;
                case "day":
                    $value = floor($value * 1000 * 60 * 60 * 24);
                    break;
            }
            $row['day'] = floor($value / 1000 / 60 / 60 / 24);
            $row['hour'] = floor($value / 1000 / 60 / 60);
            $row['minute'] = floor($value / 1000 / 60);
            $row['second'] = floor($value / 1000);
            $row['millisecond'] = floor($value);
        }
        return isset($row[$unit[1]]) ? $row[$unit[1]] . ' ' . Yii::t('app', ucfirst($unit[1])) : false;
    }

    /**
     * 将秒转化为具体的时间
     * @param $second
     * @return string
     */
    public static function SecondChangeToTime($second)
    {
        $months = floor($second/2678400);
        $second -= $months * 2678400;
        $weeks = floor($second/604800);
        $second -= $weeks * 604800;
        $day = floor($second/86400);
        $second -= $day * 86400;
        $hours = floor($second/3600);
        $second -= $hours * 3600;
        $minutes = floor($second/60);
        $second -= $minutes * 60;
        return $second .'秒, ' .$minutes .'分钟, '.$hours .'小时, '.$day .'天, '.$weeks .'星期, '.$months .'月(31天).';
    }

    /**
     * 时间向上取整,如2014-01-01 03:03:03, 转化后:2014-01-02
     * @param $datetime
     * @param string $time_format
     * @return bool|string
     */
    public static function TimeCeil($datetime, $time_format = 'Y-m-d')
    {
        $timestamp = is_string($datetime) ? strtotime($datetime) : $datetime;
        if (strpos($time_format, 'd') !== false) {
            if ($timestamp % (60 * 60 * 24) != 0) {
                return date($time_format, strtotime($datetime . "+1 day"));
            }
            return date($time_format, strtotime($datetime));
        }
    }




    /**
     * 转化图标插件数据形式  只适合做 折线、柱状、脉冲
     * @param $data
     * @param $name
     * @param string $x
     * @param string $y
     * @return string
     */
    public static function HighChartsData($data, $name = [], $chart = [], $axis_point = [], $json = true, $x = 'timestamp', $y = 'value')
    {

        foreach ($data as $index => $row) {
            $array[$index]['name'] = isset($name[$index]) ? $name[$index] : '';
            // 绘制柱状或者线或脉冲图
            $chart_type = isset($chart[$index]) ? $chart[$index] : '';
            if($chart_type == 'step'){
                $array[$index]['step'] = 'true';
            } else {
                $array[$index]['type'] = $chart_type;
            }

            // 如果有不同轴线那么就配置相应参数
            if(isset($axis_point[$index]['rel'])){
                $array[$index]['yAxis'] = intval($axis_point[$index]['rel']);
            }

            // 如果存在单位就配置相应参数
            if(isset($axis_point[$index]['unit'])){
                $array[$index]['tooltip']['valueSuffix'] = ' '.$axis_point[$index]['unit'];
            }

    // 制作HIGHCHART能识别的数组并转成JSON
foreach ($row as $v) {
$array[$index]['data'][] = [strtotime($v[$x]) * 1000, floatval($v[$y])];
}
}
        if($json){
            return json_encode(array_values($array), JSON_UNESCAPED_UNICODE);
        } else {
            return array_values($array);
        }
    }

    /**
     * 转化时间戳
     * @param $data
     * @return int|string
     */
    public static function timestamp($data)
    {
        return is_numeric($data) ? $data : strtotime($data);
    }

    /**
     * 本地化时间
     * @param $data
     * @return bool|string
     */
    public static function TimeFormat($data, $format = 'Y-m-d H:i:s')
    {
        $timestamp = static::timestamp($data);
        // $timestamp = $timestamp>9999999999?$timestamp/1000:$timestamp; // 如果是毫秒就转换
        return date($format, $timestamp);
    }

    /**
     * 二维数组拿取一个值转换为一维数组
     * @param $arr
     * @param $key
     * @return array
     */
    public static function arrayTwoOne($arr, $key)
    {
        foreach($arr as $v){
            $data[] = $v[$key];
        }
        return isset($data)?$data:[];
    }

    /**
     * PHP函数date的时间格式参数
     * @param $category
     * @return string
     */
    public static function TimeCategory($category)
    {
        switch ($category) {
            case "year":
                return 'Y';
            case "month":
                return 'Y-m';
            case "day":
                return 'Y-m-d';
            case "hour":
                return 'Y-m-d H';
            case "":
                return 'Y-m-d H:i:s';
        }
      }

    /**
     * 此方法是按照 MyStringHelper::getRelDefault() 对应来的
     * 例： if $category == 'month' Time Format is 'Y-m-d'
     * @param $category
     * @return string
     */
    public static function TimeDefaultCategory($category)
    {
        switch ($category) {
            case "year":
                return 'Y-m';
            case "month":
                return 'Y-m-d';
            case "day":
                return 'Y-m-d H:00';
            case "":
                return 'Y-m-d H:i:s';
        }
    }
    /**
     * 根据类型 来制造对应 完整的时间区间
     * @param $datetime  时间戳 或 时间字符串
     * @param string $type  时间类型
     * 该方法 不返回值 直接以GLOBALS 全局变量 进行赋值
     */
    public static function TimeFloor($datetime, $type = 'month')
    {
        $timestamp = is_string($datetime) ? strtotime($datetime) : $datetime;
        if($type == 'year'){
            $GLOBALS['start_time'] = $datetime.'-01-01 00:00';
            $GLOBALS['end_time'] = $datetime.'-12-31 23:59';
        }
        else if($type == 'month')
        {
            $GLOBALS['start_time'] = date('Y-m', $timestamp).'-01 00:00';
            $GLOBALS['end_time'] = date('Y-m-t', $timestamp).' 23:59';
        }
        else if($type == 'day')
        {
            $GLOBALS['start_time'] = date('Y-m-d', $timestamp).' 00:00';
            $GLOBALS['end_time'] = date('Y-m-d', $timestamp).' 23:59';

        }
        else if($type == 'week')
        {
            $week = date('w', $timestamp) == 0 ? 7 : date('w', $timestamp);
            $over_week = 7 - $week;
            $GLOBALS['start_time'] =  date('Y-m-d', strtotime($datetime . "-$week day")).' 23:59';
            $GLOBALS['end_time'] =  date('Y-m-d', strtotime($datetime . "$over_week day")).' 23:59';
        }
        else if($type == 'hour')
        {
            $GLOBALS['start_time'] = date('Y-m-d h', $timestamp).':00';
            $GLOBALS['end_time'] = date('Y-m-d h', $timestamp).':59';
        }
        return ['start' => $GLOBALS['start_time'], 'end' => $GLOBALS['end_time']];
    }

    public static function toTimeFormat($date, $date_type = 'day')
    {
        switch($date_type){
            case 'year':
                $format = 'Y';
                break;
            case 'month':
                $format = 'Y-m';
                break;
            case 'day':
            case 'week':
                $format = 'Y-m-d';
                break;
        }
        return self::TimeFormat($date, $format);
    }

    /**
     * 此方法 用于 select 的placeholder属性 如果需要一个默认提示 需要添加一个 空的 <option></option>
     * @param $data 传递 一个数组
     * @return mixed  返回一个 在数组顶部添加一个 空值的 数组
     */
    public static function SelectDataAddArrayTop($data)
    {
        $result[''] = '';
        foreach($data as $key => $value)
        {
            $result[$key] = $value;
        }
        return $result;
    }

    /**
     * 生成多段SQL条件语句
     *
     * 格式：[
     *          ['start_time' => '1970-01-01', 'end_time' => '1970-01-01'],
     *          ['start_time' => '1970-01-01', 'end_time' => '1970-01-01'],
     *      ]
     * @param $date
     *
     * 结果: timestamp BETWEEN 1970-01-01 and 1970-01-01 or timestamp BETWEEN 1970-01-01 and 1970-01-01
     * @return string
     */
    public static function MultiDateSql($date)
    {
        $where_timestamp = '';
        foreach($date as $v){
            $v['end_time'] = strlen($v['end_time'])==10?$v['end_time'].' 23:59:59':$v['end_time'];
            $where_timestamp .= "timestamp BETWEEN '".$v['start_time']."' and '".$v['end_time']."' or ";
        }
        return '('.substr($where_timestamp, 0,-4).')';
    }


    public static function listDir($dir = '../../../../')
    {
        if(is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    echo "<ul>";
                    if((is_dir($dir."/".$file)) && $file!="." && $file!=".."){
                        echo "<li><font color='red'>文件名:".$file."</font></li>";
                        self::listDir($dir."/".$file."/");
                    }else{

                    }
                    echo "</ul>";
                }
                closedir($dh);

            }
        }
    }

    /**
     * 获取系统生成 点位、事件、日志等记录的 name
     * @param $system_type                      系统点位的类型：
     *                                               为了方便管理，方便处理 会在不同 功能生成 的系统点位名上添加不同的 系统前缀
     *
     * @param $config_type                      命名 变量的类型  可能是 点位、事件、规则 等
     * @param $point_id                         点位的 id
     * @return string                           返回以'System'为前缀 以'- point_id' 为后缀的名字
     */
    public static function setSystemName($system_type, $config_type, $point_id)
    {
        return 'System_'.$system_type.'_'.$config_type.'-'.$point_id;
    }

    //判断 一个 字符串 是否在数组中
    public static function stringFindArray_value($arr, $str)
    {
        foreach($arr as $v)
        {
            if(strpos(','.$str.',', ','.$v.',') !== false)
            {
                return 1;
            }
        }
        return 0;
    }


    /**
     * 数组按键名倒序重新排序
     * @param $arr
     */
    public static function Row_transform($arr)
    {
        krsort($arr);
        $i = 0;
        foreach($arr as $value) {
            $arr[$i] = $value;
            $i++;
        }
        return $arr;
    }

    /************************* highchart 数据 处理方法 **********************************************************/

    public static function getChartDataType($chart_data = [], $unit = '', $exclude_key)
    {
        $result_data = self::getChartDataByPercent($chart_data, $unit, $exclude_key);
        foreach($result_data['data'] as $key => $value) {
            $ori_data=array_fill(0,count($result_data['data']),0);
            $ori_data[$key] = $value['data'][0];
            $result_data['data'][$key]['data'] = $ori_data;
        }
        return $result_data;
    }


    public static function getChartDataByPercent($chart_data = [], $unit = '', $exclude_key = [])
    {
        $_chart = [];
        $_categories = [];
        if(!empty($chart_data)) {
            foreach($chart_data as $key => $value) {
                $point_name = self::DisposeJSON($value['point']['name']);
                $_categories[] = $point_name;
                $_chart[] = [
                            'name' => $point_name,
                            'data' => [round($value['value'], 2)],
                            'tooltip' => ['valueSuffix' => " ". $unit .""]
                            ];
            }
        }
        return ['data' => $_chart,
                'categories' => $_categories];
    }


    /**
     * highcharts 数据格式处理
     *            处理内容：   处理由point_data类中的行列转换得到的数据
     *            返回内容：   返回一个含有两种类型图数据（一个是stacked图的数据, 一个是 折现图的数据）
     * @param $chart_data
     * @param string $primary_key
     * @return array
     */
    public static function get_chart_data_by_stacked($chart_data, $primary_key = 'timestamp')
    {
        $result_data = [];
        if(!empty($chart_data)) {
            foreach($chart_data as $key => $value) {
                $name = $value[$primary_key];
                unset($value[$primary_key]);
                $_data = [];
                foreach($value as $v1) {
                    $_data[] = floatval($v1);
                }
                $result_data[] = ['name' => $name, 'data' => $_data];
            }
        }
        return $result_data;
    }

    public static function get_chart_data_by_stacked_1($chart_data, $primary_key = 'timestamp')
    {
        $stacked_data = [];
        $total_data = ['name' => '小时统计图'];
        if(!empty($chart_data)) {
            foreach($chart_data as $key => $value) {
                $name = $value[$primary_key];
                unset($value[$primary_key]);
                $_data = [];
                $total = 0;
                foreach($value as $v1) {
                    $_data[] = floatval($v1);
                    $total += $v1;
                }
                $total_data['data'][] = $total;
                $stacked_data[] = ['name' => $name, 'data' => $_data];
            }
        }
        return array_merge($stacked_data, $total_data);
    }

    public static function multi_select($arr) {
        $nulti_arr = [];
        foreach($arr as $key => $value) {
            $nulti_arr[] = ['value' => $key, 'text' => $value];
        }
        echo json_encode($nulti_arr);
    }

    /**
     * @param $arr
     */
    public static function multi_select_point($arr) {
//        echo '<pre>';
//        print_r($arr);die;
        $nulti_arr = [];
        foreach($arr as $key => $value) {
            $pointName = json_decode($value['name'], true);
            $nulti_arr[] = [
                'pointId' => $value['id'],
                'pointName' =>$value['name'],
//                'pointName' => $pointName['data']['zh-cn'],
//                'protocolName' => $value['protocol_id'],
//                'controller_id' => $value['controller_id'],
//                'unit' => $value['unit'],
                'value_type' => $value['value_type']
            ];
        }
        return json_encode($nulti_arr);
    }

    /**
     * 专门处理 报警事件 重复时间的一个方法
     *      在此可以看出 每一个模型 对应以个Form类的重要性
     * @param $time_range   时间区间
     * @param $time_type    重复类型
     * return array 返回一个数组 包含处理好的时间区间 和 原始时间区间
     */
    public static function getTimeRangeOfAlarm($time_range, $time_type = '')
    {
        //将最后的分号去掉
        $_time_range = trim($time_range, ';');
        $time_arr = explode(';', $_time_range);
        $time['data_type'] = 'time';
        $original_time['data_type'] = 'time';
        $time['data']['type'] = !empty($time_type) ? $time_type : 'day';
        $original_time['data']['type'] = !empty($time_type) ? $time_type : 'day';
        //将时间 转为json 以分号 为一组区间
        foreach($time_arr as $key => $value)
        {
            $value = trim($value, ',');
            $_time = explode(',', $value);
            $time_start = self::getTimeCutOffByType(date('Y-m-d H:i:s', strtotime($_time[0])), $time_type);
            $original_end = !empty($_time[1]) ? date('Y-m-d H:i:s', strtotime($_time[1])) : date('Y-m-d 23:59:59', strtotime($time_start));
            $time_end = self::getTimeCutOffByType($original_end, $time_type);
            $time['data']['time_range'][$key]['start'] = $time_start;
            $time['data']['time_range'][$key]['end'] = $time_end;
            $original_time['data']['time_range'][$key]['start'] = date('Y-m-d H:i:s', strtotime($_time[0]));
            $original_time['data']['time_range'][$key]['end'] = $original_end;
            //如果是按星期 循环 需要加上 是星期几
            if($time_type == 'week') {
                $time['data']['time_range'][$key]['wek'] = date("w", strtotime($_time[0]));
            }
        }

        return [
            'range_time' => json_encode($time),
            'original_time' => json_encode($original_time)
        ];
    }

    public static function status($value)
    {
        $status = json_decode($value, true);
        $count = count($status['data']);
        $send = 0;
        $id = uniqid();
        $html = "<div id = '_status" . $id . "' style = 'display:none'><ul>";
        foreach ($status['data'] as $key => $value) {
            $html .= '<li>' . $key . ":" . ($value == 1 ? '发送成功' : '发送失败') . '</li>';
            $send += $value;
        }
        $html .= '</ul></div>';
        switch ($send) {
            case 0:
                return '发送失败';
                break;
            case $count:
                return '发送成功';
                break;
            default:
                return "<a id = '$id' class = '_status'  >部分发送成功</a>" . $html;
                break;
        }
    }

    /**
     * 根据类型截断时间
     * @param $time
     * @param $type
     */
    public static function getTimeCutOffByType($time, $type)
    {
        $Ctime = $time;
        switch($type) {
            case 'day':
                $Ctime = date('H:i', strtotime($time));
                    break;
            case 'month':
                $Ctime = date('d', strtotime($time));
                break;
            default:
                $Ctime = date('H:i', strtotime($time));
        }
        return $Ctime;
    }

    /**
     * 为search 提供json数据类型在此方法中只返回 并列 条件 既 andWhere
     * @param $column
     * @return array|string
     */
    public static function getColumnJsonType($column)
    {
        if(in_array($column, ['core.alarm_log.description']) !== false) {
            //json 类型为 description
            return ['type' => 'description',
                'data' =>[Yii::$app->language]];
        } else if(in_array($column, ['core.alarm_log.send_status']) !== false) {
            //json 类型为 send_status
            return ['type' => 'send_status',
                'data' =>['mail', 'sms']];
        } else {
            //json 类型为 description
            return ['type' => 'description',
                'data' =>[Yii::$app->language]];
        }
        return '';
    }
    /**
     * 将php输出内容打印到浏览器控制台
     * @params mix
     */
    public static function console_debug($data)
    {
        echo("<script>console.log('debug info :". json_encode($data) . "');</script>");
    }

    public static function get_energy_yoy_category_data($data, $pid = 0)
    {
        $tree = [];
        foreach($data as $k => $v){
            $v['title'] = self::DisposeJSON($v['title']);
            if($v['key'] == 0){ // 如果是顶级就跳过
                continue;
            }
            if($v['parent_id'] == $pid){ // 找到多个PID等于传递值的存起来
                unset($v['parent_id']);
                if($sub = static::get_energy_yoy_category_data($data, $v['key'])){ // 如果没有就不赋值
                    $v['hideCheckbox'] = false;
                    $v['children'] = $sub;
                }

                $tree[] = $v;
            }

        }

        if (empty($tree)) {
            $tree[2] = [];
        }
        return $tree;
    }
    public static function object_array($array) {
        if(is_object($array)) {
            $array = (array)$array;
        } if(is_array($array)) {
            foreach($array as $key=>$value) {
                $array[$key] = object_array($value);
            }
        }
        return $array;
    }

    public static function RedisPoint($id,$key){
        $id="DB:point:".$id;
//        $key = "Database:Point:8999675:value";   //值
        //$key_value = "Database:Point:8985462:value_timestamp";  //更新时间
        //$key = "test";
        //$tem_set = Yii::$app->redis->set($key,"test222");
        $value = Yii::$app->redis->hget($id,$key);
//        echo "<pre>";print_r($tem_get);die;
        if($key=="value_timestamp")
            if($value!=null)
                $value=date('Y-m-d H:i:s',$value);
            else $value='无';
        return $value;
    }
    public static function RedisEvent($id,$key){
        $id="DB:event:".$id;
        $value = Yii::$app->redis->hget($id,$key);
        if($value=="True") $value=1;
        if($value=="False")$value=0;
        if($key=="timestamp"){
            if($value!=null)
                $value=date('Y-m-d H:i:s',$value);
            else $value='无';
        }
        if($value==null)$value=0;
        return $value;
    }

    public static function getRoleId(){
        $str_role_id = explode(',', MyFunc::DisposeJSON(Yii::$app->user->identity->role_id));
        return $str_role_id[0];
    }

}