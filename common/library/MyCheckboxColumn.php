<?php

namespace common\library;

use yii\grid\CheckboxColumn;

class MyCheckboxColumn extends CheckboxColumn
{
    public $checked_array;

    public $header_content;

    public function init()
    {
        parent::init();
        if (empty($this->name)) {
            throw new InvalidConfigException('The "name" property must be set.');
        }
        // 如果有[]就去掉[]
        if (substr($this->name, -2) === '[]') {
            $this->name = substr($this->name, 0, -2);
        }
    }

    protected function renderHeaderCellContent()
    {
        if($this->header_content){
            return $this->header_content;
        }
        return parent::renderHeaderCellContent();
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->checkboxOptions instanceof Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
            if (!isset($options['value'])) {
                $options['value'] = is_array($key) ? json_encode($key) : $key;
            }
        }
        $checked = isset($this->checked_array[$key])?$this->checked_array[$key]:null;
        return MyHtml::checkbox($this->name."[$key]", $checked, $options);
    }
}