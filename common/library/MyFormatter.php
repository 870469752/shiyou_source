<?php
/**
 * @link http://www.gcxj.com/
 * @copyright Copyright (c) 2014 gcxj
 * @license http://www.gcxj.com/license/
 */

namespace common\library;

use backend\models\Unit;
use backend\models\Protocol;
use Yii;
use yii\base\Formatter;
use common\library\MyFunc;
use common\models\User;
use yii\helpers\ArrayHelper;
use backend\models\ImageGroup;
use backend\models\Point;
use backend\models\EquipmentPointRelation;
/**
 * 本类主要为了格式化表格中列值
 *
 * @author 葛盛庭 <ducooo@163.com>
 * @since 1.0
 *
 */
class MyFormatter extends Formatter
{
    /**
     * 格式化JSON数据
     * @param JSON $value
     * @return string
     */
    public function asJSON($value, $col = '')
    {
        return MyFunc::DisposeJSON(!empty($col) ? $value[$col] : $value);
    }

    /**
     * 给数字、字母等类型的数据 附加含义
     * @param  [type] $value [description]
     * @param  Array $arr [description]
     * @return [type]        [description]
     */
    public function asMean($value, $arr)
    {
        return $arr[$value];
    }

    /**
     * 处理报警 发送状态
     * @param $value
     */
    public function asstatus($value)
    {
        $status = json_decode($value, true);
        $count = count($status['data']);
        $send = 0;
        $id = uniqid();
        $html = "<div id = '_status" . $id . "' style = 'display:none'><ul>";
        foreach ($status['data'] as $key => $value) {
            $html .= '<li>' . $key . ":" . ($value == 1 ? '发送成功' : '发送失败') . '</li>';
            $send += $value;
        }
        $html .= '</ul></div>';
        switch ($send) {
            case 0:
                return '发送失败';
                break;
            case $count:
                return '发送成功';
                break;
            default:
                return "<a id = '$id' class = '_status'  >部分发送成功</a>" . $html;
                break;
        }
    }

    public function asTimeFormatter($value)
    {
        return MyFunc::TimeFormat($value);
    }

    /**
     * 时间单位转换
     * @param $value
     * @param array $unit 把什么转换为什么
     * @return bool
     */
    public function asTimeUnitTransition($value, $unit = ['second', 'minute'])
    {
        return MyFunc::TimeUnitTransition($value, $unit);
    }

    /**
     * 获取对象某属性值, 并可继续格式化
     * @param $value
     * @param string $attr 属性名称
     * @param array $format 可嵌套填写 ['Object', 'name', ['Object', 'last_name']]
     * @return string
     */
    public function asObject($value, $attr, $format = [])
    {
        if(!isset($value->$attr)){
            return Yii::t('yii', '(not set)');
        }
        if ($format) {
            return $this->format($value->$attr, $format);
        }
        return $value->$attr;
    }

    /**
     * 获取数组值
     * @param $value
     * @param string $key 键名
     * @param array $format 可嵌套填写 ['arrayKey', 'name', ['arrayKey', 'last_name']]
     * @return string
     */
    public function asArrayKey($value, $key, $format = [])
    {
        if(!isset($value[$key])){
            return Yii::t('yii', '(not set)');
        }
        if ($format) {
            return $this->format($value[$key], $format);
        }
        return $value[$key];
    }

    /**
     * 利用 user_id数组 获取 对应 用户表的 响应信息
     * @param $value
     */
    public function asUser($value)
    {
        $id_arr = json_decode($value, true);
        $res = User::getUser(['id' => $id_arr['data']]);
        $names = '';
        foreach ($res as $value) {
            $names .= $value['username'] . ',';
        }
        return trim($names, ',');
    }
    /*
     * 根据uid返回username
     * @param numeric $uid
     * @return varchar $username
     */
    public function asUid($uid){
        $user_info = User::getUser(['id'=>$uid]);
        foreach($user_info as $k => $v){
            $result = $v['username'];
        }
        return $result;
    }

    public function asUnit($value)
    {
        $unit = Unit::getById($value);
        return isset($unit->name) ? $unit->name : '-';
    }


    /**
     * 取两位小数
     * @param $value
     * @param int $quantity
     * @return float
     */
    public function asFloor($value, $quantity = 2)
    {
        $num = pow(10, $quantity);
        return floor($value * $num) / $num;
    }

    public function asArray($value, $index = ''){
        if($index != '' && is_array($value)){
            foreach($value as $v){
                $arr[] = MyFunc::DisposeJSON($v[$index]);
            }
        }else{
            $arr = $value;
        }
        return isset($arr)?implode(' , ', $arr):'';
    }
    
    /**
     * 报警 事件
     * 判断 如果在  点位报警设置 里 生成的 才会将其 显示到页面上
     *             生成名字前缀格式： 'System_alarm_event'
     * @param $value            当前字段查询出的数据
     * @param $column_type      字段名
     * @param $filter_name      数据值 对应的name
     * @return string
     */
    public function asTriggerExtreme($value, $column_type = 'trigger', $key = 'max_value', $filter_name = 'System_alarm_event')
    {
        if(!empty($value) && is_array($value))
        {
            foreach($value as $k => $v)
            {
                //筛选出 点位报警设置页面所 生成的 事件
                if(strpos($v->name, $filter_name) !== false && $v->is_system)
                {
                        $trigger = json_decode($v->$column_type, true);
                    //echo "<pre>";print_r($trigger);die;
                        $data = $trigger['data'][$key];

                        return $data;
                }
            }
        }
        return 'null';
    }

    /**
     * 报警 规则
     * 判断 如果在  点位报警设置 里 生成的 才会将其 显示到页面上
     *             生成名字前缀格式： 'System_alarm_rule'
     * @param $value            当前字段查询出的数据
     * @return string
     */
    public function asRuleExtreme($value)
    {
        if(!empty($value) && is_array($value))
        {
            //循环 alarmevent
            foreach($value as $k => $v)
            {
                //判断 规则 是否为空
                if($v->is_system && !empty($v->alarmRules))
                {
                    //循环 alarmrule
                    foreach($v->alarmRules as $k_1 => $v_1)
                    {
                        if($v_1->is_system)
                        {
                            $level = json_decode($v_1['level_sequence'], true);
                            return implode(',', $level['data']);
                        }
                    }
                }
            }
        }
        return 'null';
    }

    /**
     * 重写方法，可为链接取名，选择是否跳出窗口
     * @param mixed $value
     * @param string $name
     * @param bool $window
     * @return string
     */
    public function asUrl($value, $name = '', $window = true)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        return MyHtml::a($name?$name:$value, $value, $window?['target' => '_black']:[]);
//        return MyHtml::img('/uploads/'.$value, ['style' => 'width:40px;height:40px;overflow:hidden;']);
    }

    /**
     * 显示图片
     * @param string path
     * @return string
     */
    public function asImg($value)
    {
        return MyHtml::img($value, ['style' => 'width:40px;height:40px;overflow:hidden;']);
    }

    /**
     * 报警日志结束时间处理
     */
    public function asLogEndTime($value)
    {
        return $value?MyFunc::TimeFormat($value):Yii::t('app', 'Is alarm');
    }

    /**
     * 根据equipment_id返回name
     */
    public function asGroupName($value){
        $model = new ImageGroup();
        $result = $model->find()->select('name')->where("id=$value")->asArray()->all();
        return $result[0]['name'];
    }
    /**
     * 根据point_id返回name
     */
    public function asPointName($value){
        if(isset($value)){
            $model = new point();
            $result = $model->find()->select('name')->where("id=$value")->asArray()->all();
            return MyFunc::DisposeJSON($result[0]['name']);
        }else{
            return $value;
        }
    }
    /*
     * 比较规则返回名字
     */
    public function asDynamicName($value){
        if(isset($value)){
            switch($value){
                case 0:
                    return YIi::t('equipment','Equal') ;
                    break;
                case 1:
                    return YIi::t('equipment','Not Equal') ;
                    break;
                case 2:
                    return YIi::t('equipment','Greater Than') ;
                    break;
                case 3:
                    return YIi::t('equipment','Less Than') ;
                    break;
                case 4:
                    return YIi::t('equipment','Greater Equal') ;
                    break;
                case 5:
                    return YIi::t('equipment','Less Equal') ;
                    break;
            }
        }else{
            return;
        }

    }
    /*
     * 返回时间模式重复模式
     * 'Repeat/Month' => '重复/月',
    'Repeat/Year' => '重复/年',
    'Repeat/Week' => '重复/周',
    'Repeat/Day' => '重复/天',
     */
    public function asModeName($value)
    {
        if(isset($value)){
            switch($value){
                case 'year':
                    return Yii::t('app','Repeat/Year') ;
                    break;
                case 'month':
                    return Yii::t('app','Repeat/Month') ;
                    break;
                case 'week':
                    return Yii::t('app','Repeat/Week') ;
                    break;
                case 'day':
                    return Yii::t('app','Repeat/Day') ;
                    break;
                case 'custom':
                    return Yii::t('app','Custom') ;
                    break;
            }
        }else{
            return;
        }
    }

    /**
     * json 中 获取某个键值
     * @param  [type] $value [description]
     * @param  [type] $key   [description]
     * @return [type]        [description]
     */
    public function asResolve($value, $key)
    {
        return MyFunc::JsonFindValue($value, $key);
    }

    public function asSeconds2time($value)
    {
        return MyFunc::SecondChangeToTime($value);
    }

    public function asTimeConversion($value)
    {
        return MyFunc::SecondChangeToTime($value);
    }

    /**
     * 根据协议编号 显示对应的协议名称
     */
    public function asProtocol($value)
    {
        $protocol=Protocol::getById($value);
        return isset($protocol->name) ? $protocol->name : '-';
    }
}
