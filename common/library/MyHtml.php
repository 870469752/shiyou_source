<?php
/**
 * @link http://www.gcxj.com/
 * @copyright Copyright (c) 2014 gcxj
 * @license http://www.gcxj.com/license/
 */

namespace common\library;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class MyHtml extends Html
{
    /**
     * 重写 单选框 能够输出html代码
     * @param  [type] $name      [description]
     * @param  [type] $selection [description]
     * @param  [type] $items     [description]
     * @param  [type] $options   [description]
     * @return [type]            [description]
     */
    public static function radioList($name, $selection = null, $items = [], $options = [])
    {
        $encode = !isset($options['encode']) || $options['encode'];
        $formatter = isset($options['item']) ? $options['item'] : null;
        $itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
        $lines = [];
        $index = 0;
        foreach ($items as $value => $label) {
            $checked = $selection !== null &&
                (!is_array($selection) && strpos($label, $selection)
                    || is_array($selection) && in_array($value, $selection));
            if ($formatter !== null) {
                $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
            } else {
                $lines[] = static::radio($name, $checked, array_merge($itemOptions, [
                    'value' => $value,
                    'label' => $encode ? static::encode($label) : $label,
                ]));
            }
            $index++;
        }

        $separator = isset($options['separator']) ? $options['separator'] : "\n";
        if (isset($options['unselect'])) {
            // add a hidden field so that if the list box has no option being selected, it still submits a value
            $hidden = static::hiddenInput($name, $options['unselect']);
        } else {
            $hidden = '';
        }

        $tag = isset($options['tag']) ? $options['tag'] : 'div';
        unset($options['tag'], $options['unselect'], $options['encode'], $options['separator'], $options['item'], $options['itemOptions']);

        return $hidden . static::tag($tag, implode($separator, $lines), $options);
    }

    /**
     * 生成树状option
     * @param $tree
     * @param string $ico
     * @param string $retract
     */
    public static function TreeOption($selection = null, $tree, $options = [])
    {

        $options['ico'] = isset($options['ico'])?$options['ico']:'┕';
        $options['char'] = isset($options['char'])?$options['char']:'&nbsp;&nbsp;&nbsp;&nbsp;';
        $options['retract'] = isset($options['retract'])?$options['retract'].$options['char']:'';
        $rel_retract = $options['retract'].$options['ico'].'&nbsp;';
        foreach ($tree as $v){
            $attrs = isset($options[$v['id']]) ? $options[$v['id']] : [];
            $attrs['value'] = (string) $v['id'];
            $attrs['selected'] = $selection !== null &&
                (!is_array($selection) && !strcmp($v['id'], $selection) ||
                is_array($selection) && in_array($v['id'], $selection));

            $option[] = static::tag('option', $rel_retract.static::encode(MyFunc::DisposeJSON($v['name'])), $attrs);
            if(isset($v['sub'])){
                $option[] = static::TreeOption($selection, $v['sub'], $options);
            }
        }

        return isset($option) ?implode("\n", $option):'';
    }

    /**
     * 生成树状option
     * @param $tree
     * @param string $ico
     * @param string $retract
     */
    public static function dropDownList($name, $selection = null, $items = [], $options = [])
    {
        $options['name'] = $name;
        if (!empty($options['multiple'])) {
            if(!empty($options['tree'])){ // 是树状，并且是多选
                return static::tag('select', "\n<option></option>".static::TreeOption($selection, $items, $options)."\n", $options);
            }
            // 只是多选
            return static::listBox($name, $selection, $items, $options);
        }
        elseif(!empty($options['tree'])){ // 只是树状
            return static::tag('select', "\n<option></option>".static::TreeOption($selection, $items, $options)."\n", $options);
        }
        // 单选
        return static::tag('select', "\n<option></option>".static::renderSelectOptions($selection, $items, $options)."\n", $options);

    }

    public static function activeDropDownList($model, $attribute, $items, $options = [])
    {
        if (!empty($options['multiple'])) {
            return static::activeListBox($model, $attribute, $items, $options);
        }
        $name = isset($options['name']) ? $options['name'] : static::getInputName($model, $attribute);
        $selection = static::getAttributeValue($model, $attribute);
        if (!array_key_exists('id', $options)) {
            $options['id'] = static::getInputId($model, $attribute);
        }

        return static::dropDownList($name, $selection, $items, $options);
    }

    /**
     * 重写YII原方法，把自动生成隐藏变量给去掉
     * @param string $action
     * @param string $method
     * @param array $options
     * @return string
     */
    public static function beginForm($action = '', $method = 'post', $options = [])
    {
        $action = Url::to($action);

        $hiddenInputs = [];

        $request = Yii::$app->getRequest();
        if ($request instanceof \yii\web\Request) {
            if (strcasecmp($method, 'get') && strcasecmp($method, 'post')) {
                // simulate PUT, DELETE, etc. via POST
                $hiddenInputs[] = static::hiddenInput($request->methodParam, $method);
                $method = 'post';
            }
            if ($request->enableCsrfValidation && !strcasecmp($method, 'post')) {
                $hiddenInputs[] = static::hiddenInput($request->csrfParam, $request->getCsrfToken());
            }
        }

        if (!strcasecmp($method, 'get') && ($pos = strpos($action, '?')) !== false) {
            // 这里去掉了GET方法 自动生成隐藏框，系统本身的目的是为了保留连接上的参数，但是给查询带来了麻烦，有值的话就自己传吧。
            $action = substr($action, 0, $pos);
        }

        $options['action'] = $action;
        $options['method'] = $method;
        $form = static::beginTag('form', $options);
        if (!empty($hiddenInputs)) {
            $form .= "\n" . implode("\n", $hiddenInputs);
        }

        return $form;
    }

    /**
     * 递归输出树状方块HTML
     * @param $data
     * @param array $options
     * @return string
     */
    public static function EnergyTreeRecursion($data, $options = [])
    {
        $button = '';
        if(isset($options['add_point'])){
            $button .= '<sapn class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i></sapn>';
        }
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $name=json_decode($v['title'],1);
            $zh=isset($name['data']['zh-cn'])?$name['data']['zh-cn']:'null';
            $en=isset($name['data']['en-us'])?$name['data']['en-us']:'null';
            if($v['rgba']==null) $v['rgba']="#92cddc";

            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id']. '" data-title="aaa"  data-zh=' .$zh.' data-en='.$en.'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">'  . ($zh?$zh:'请点击修改名称') .  '</span>
								<em class="close pull-right " style="padding-left:10px;">×</em>
								<a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Subclass') . '">
                                    <i class="fa fa-sitemap fa-fw"></i>
                                </a>
                                 <a value="'. $v['id'] .'"style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top"  >
                                    <input class="noIndColor" style="display:none;" value="'. $v['rgba'] .'" />
                                </a>
                                '.$button.'
                                </div>';
            $html .= isset($v['sub']) ? static::EnergyTreeRecursion($v['sub'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;

    }



    /**
     * 递归输出树状方块HTML
     * @param $data
     * @param array $options
     * @return string
     */
    public static function LocationTreeRecursion($data, $options = [])
    {
        $button = '';
        if(isset($options['add_point'])){
            $button .= '<sapn class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i></sapn>';
        }
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $name=json_decode($v['title'],1);
            $zh=isset($name['data']['zh-cn'])?$name['data']['zh-cn']:'null';
            $en=isset($name['data']['en-us'])?$name['data']['en-us']:'null';
            if($v['rgba']==null) $v['rgba']="#92cddc";

            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id']. '" data-title="aaa"  data-zh=' .$zh.' data-en='.$en.'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">'  . ($zh?$zh:'请点击修改名称') .  '</span>
								<em class="close pull-right " style="padding-left:10px;">×</em>

								<a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Subclass') . '">
                                    <i class="fa fa-sitemap fa-fw"></i>
                                </a>
                                <a href="add-table?id='. $v['id'] .'" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Table') . '">
                                    <i class="fa  fa-table"></i>
                                </a>
                                <a href="location-detail?id='. $v['id'] .'" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Update detail') . '">
                                    <i class="fa  fa-pencil fa-fw"></i>
                                </a>
                                 <a href="location-view?id='. $v['id'] .'" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Update system') . '">
                                    <i class="fa  fa-eye fa-fw"></i>
                                </a>
                                  <a value="'. $v['id'] .'"style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top"  >
                                    <input class="noIndColor" style="display:none;" value="'. $v['rgba'] .'" />
                                </a>
                                '.$button.'
                                </div>';

            $html .= isset($v['sub']) ? static::LocationTreeRecursion($v['sub'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;
    }

    public static function ReportManageTreeRecursion($data, $options = [])
    {
        $button = '';
        if(isset($options['add_point'])){
            $button .= '<sapn class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i></sapn>';
        }
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-role="' . $v['role_id'] . '"  data-title=' . $v['title'] . '>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">' . (MyFunc::DisposeJSON($v['title'])?MyFunc::DisposeJSON($v['title']):'请点击修改名称') . '</span>
								<em class="close pull-right " style="padding-left:10px;">×</em>
								  <a href="/scheduled-report/crud/new-index?id='. $v['id'] .'&type=create" style="padding-left:10px;" class="pull-right txt-color-blueDark reprot" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Report') . '">
                                     <i class="fa  fa-table fa-fw "></i>
                                </a>
								   <a href="javacript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark role" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Role') . '">
                                    <i class="fa  fa-pencil fa-fw"></i>
                                </a>
								<a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Subclass') . '">
                                    <i class="fa fa-sitemap fa-fw"></i>
                                </a>
                                '.$button.'
                                </div>';
            $html .= isset($v['sub']) ? static::ReportManageTreeRecursion($v['sub'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;

    }

    public static function SubSystemManageTreeRecursion($data, $options = [])
    {
        $button = '';
        if(isset($options['add_point'])){
            $button .= '<span class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i><span>';
        }
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $name=json_decode($v['title'],1);
            $zh=isset($name['data']['zh-cn'])?$name['data']['zh-cn']:'null';
            $en=isset($name['data']['en-us'])?$name['data']['en-us']:'null';
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-role="' . $v['role_id'] . '" data-title="aaa"  data-zh=' .$zh.' data-en='.$en.'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">' . ($zh?$zh:'请点击修改名称') . '</span>
								<em class="close pull-right " style="padding-left:10px;">×</em>
								  <a href="/subsystem_manage/crud/location-view?id='. $v['id'] .'" style="padding-left:10px;" class="pull-right txt-color-blueDark reprot" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Update system') . '">
                                     <i class="fa  fa-eye fa-fw "></i>
                                </a>
								   <a href="javacript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark role" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Role') . '">
                                    <i class="fa  fa-pencil fa-fw"></i>
                                </a>
								<a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Add Subclass') . '">
                                    <i class="fa fa-sitemap fa-fw"></i>
                                </a>
                                '.$button.'
                                </div>';
            $html .= isset($v['sub']) ? static::SubSystemManageTreeRecursion($v['sub'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;

    }
	
	/**
     * 递归输出树状方块HTML
     * @param $data
     * @param array $options
     * @return string
     */
    public static function MenuTreeRecursion($data, $options = [])
    {
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-description=' . $v['description']  .'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content" >
								<span class="name">' . MyFunc::DisposeJSON($v['description']) . '</span>
								<a class="fa fa-times pull-right"  href="/sys-menu/deletes?id='. $v['id'] .'" data-confirm="'.Yii::t('app', 'This will remove all the leaf nodes are removed!').'"></a>
                             <a class="fa fa-pencil pull-right" href="/sys-menu/update_menu?id='. $v['id'] .'"  data-original-title="' . Yii::t('app', 'Update system menu') . '"></a>
                         </div>';
            $html .= isset($v['children']) ? static::MenuTreeRecursion($v['children'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;
    }

    /**
     * 递归输出树状方块HTML
     * @param $data
     * @param array $options
     * @return string
     */
    public static function UserMenuTreeRecursion($data, $options = [])
    {
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-description=' . $v['description']  .'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content" >
								<span class="name">' . MyFunc::DisposeJSON($v['description']) . '</span>
								<a class="fa fa-plus pull-right"  href="/user-menu/create_child?id='. $v['id'] .'" data-original-title="'.Yii::t('app',  'Update system menu').'"></a>
								<a class="fa fa-times pull-right"  href="/user-menu/deletes?id='. $v['id'] .'" data-confirm="'.Yii::t('app', 'This will remove all the leaf nodes are removed!').'"></a>
                             <a class="fa fa-pencil pull-right" href="/user-menu/update_menu?id='. $v['id'] .'"  data-original-title="' . Yii::t('app', 'Update system menu') . '"></a>
                         </div>';

            $html .= isset($v['children']) ? static::UserMenuTreeRecursion($v['children'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;
    }


    /**
     * 递归输出树状方块HTML
     * @param $data
     * @param array $options
     * @return string
     */
    public static function LocationTreeRecursion_only_show($data, $options = [])
    {
        $button = '';
        if(isset($options['add_point'])){
            $button .= '<sapn class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i></sapn>';
        }
        $html = '<ol class="dd-list">';
        foreach ($data as $k => $v) {
            $html .= '
						<li class="dd-item dd3-item" data-id="' . $v['id'] . '" data-title=' . $v['title'] .'>
							<div class="dd-handle dd3-handle">Drag</div>
							<div class="dd3-content">
								<span class="name">' . (MyFunc::DisposeJSON($v['title'])?MyFunc::DisposeJSON($v['title']):'请点击修改名称') . '</span>
                                 <a href="location-only-view?id='. $v['id'] .'" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" data-placement="top" data-original-title="' . Yii::t('app', 'Update detail') . '">
                                    <i class="fa  fa-eye fa-fw"></i>
                                </a>
                                '.$button.'
                                </div>';
            $html .= isset($v['sub']) ? static::LocationTreeRecursion($v['sub'], $options) : '';
            $html .= '</li>';
        }
        $html .= '</ol>';
        return $html;
    }
    /**
     * 生成多个树状选择框
     * @param $name 标签名称
     * @param $tree 数据项
     * @param $selected 已选择项
     * @param array $options
     * @return string
     */
    public static function multiTree($name, $tree, $selected, $options = []){
        $html = '';
        $temp_name = $name; // 防止循环叠加名称
        foreach($tree as $k => $v){
            if(isset($v['sub'])){
                if(isset($options['multiple'])){ // 如果是多选那么就是2维数组 名称后面就不能带"[]"号
                    $selected = $selected[$k];
                    $name = $temp_name."[$k][]";
                }
                $label = static::tag('label',MyFunc::DisposeJSON($v['name']), ['class' => 'control-label']);
                $input = static::tag(
                    'select',
                    MyHtml::TreeOption($selected, $v['sub']),
                    ['name' => $name, 'class' => 'select2']+$options
                );
                $help = static::tag('div', '', ['class' => 'help-block']);
                $html .= static::tag('div', $label."\n".$input."\n".$help, ['class' => 'form-group'])."\n";
            }
        }
        return $html;
    }

    public static function treeView($data, $inputName)
    {
        $button = '';
        $temp = "$inputName";
        $html = '<ul>';
        foreach ($data as $key => $v)
        {
            if ($key != 0 ) {
                $display = (bool)$v['parent_id'] == false ? 'block':'none';
                $html .= '<li class="" style="display:'.$display.';"><span style="padding-left:30px;"><label class="checkbox inline-block" onclick="javascript:checkTheValue(this.parentNode);"><input type="radio" name="'.$temp.'" value="'.$v['id'].'"><i></i>'.json_decode($v['name'], true)['data']['zh-cn'].'</label></span>';
                $html .= isset($v['sub'])?static::treeView($v['sub'], $inputName):'';
                $html .= '</li>';
            }
        }
        $html .= '</ul>';
        return $html;
    }

}