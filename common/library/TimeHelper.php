<?php
/**
 * @link http://www.gcxj.com/
 * @copyright Copyright (c) 2014 gcxj
 * @license http://www.gcxj.com/license/
 */

namespace common\library;

use Yii;

/**
 * 项目常用的数据处理方法
 *
 * 不要随意添加，必须先查看内部有无相同方法。
 *
 * @author 葛盛庭 <ducooo@163.com>
 * @since 1.0
 */
class TimeHelper
{
    /**
     * 给定时间，返回开始和结束时间
     * @param $date
     * @return array
     */
    public static function StartEndTime($date, $fetch = '')
    {
        switch(strlen($date)){
            case 10:
                $time_range = [
                    'start_time' => date('Y-m-d 00:00:00',strtotime($date)),
                    'end_time' => date('Y-m-d 23:59:59',strtotime($date)),
                ];
                break;
            case 7:
                $time_range = [
                    'start_time' => date('Y-m-01 00:00:00',strtotime($date)),
                    'end_time' => date('Y-m-d H:i:s',strtotime($date.' +1 month -1 second')),
                ];
                break;
            case 4:
                $date = $date.'-01'; // 格式化一下不然认为是数字
                $time_range = [
                    'start_time' => date('Y-01-01 00:00:00',strtotime($date)),
                    'end_time' => date('Y-m-d H:i:s',strtotime($date.' +1 year -1 second')),
                ];
                break;
            default:
                $time_range = [
                    'start_time' => '1970-01-01',
                    'end_time' => date('Y-m-d H:i:s'),
                ];
        }
        return empty($fetch)?$time_range:$time_range[$fetch];
    }
}