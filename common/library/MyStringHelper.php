<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\library;

/**
 * StringHelper
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alex Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class MyStringHelper extends \yii\helpers\StringHelper
{
    public static function getRelMethod($method){
        switch ($method) {
            case 'max':
                return ['value' => "max(value)"];
            case 'min':
                return ['value' => "min(value)"];
            case 'avg':
                return ['value' => "avg(value)"];
            case 'sum':
                return ['value' => "sum(value)"];
            default:
                return ['value' => 'max(value)'];
        }
    }

    public static function getRelGroup($group){
        switch ($group) {
            case 'hour':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM-DD HH24:00:00')"];
            case 'day':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM-DD 00:00:00')"];
            case 'month':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM-01 00:00:00')"];
            case 'year':
                return ['timestamp' => "to_char(timestamp, 'YYYY-01-01 00:00:00')"];
            default:
                return ['timestamp' => 'max(timestamp)'];
        }
    }

    public static function getRelDefault($group)
    {
        switch ($group) {
            case 'week':
            case 'day':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM-DD HH24:00:00')"];
            case 'month':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM-DD 00:00:00')"];
            case 'year':
                return ['timestamp' => "to_char(timestamp, 'YYYY-MM')"];
            default:
                return ['timestamp' => 'max(timestamp)'];
        }
    }

    public static function getFormatTime($group)
    {
        switch ($group) {
            case 'day':
            case 'week':
                return "to_char(timestamp, 'YYYY-MM-DD') as _timestamp";
            case 'month':
                return "to_char(timestamp, 'YYYY-MM') as _timestamp";
            case 'year':
                return "to_char(timestamp, 'YYYY') as _timestamp";
            default:
                return 'max(timestamp) as _timestamp';
        }
    }

    public static function getFormatTimeDefault($group)
    {
        switch ($group) {
            case 'day':
                return "to_char(timestamp, 'YYYY-MM-DD HH24:00') as _timestamp";
            case 'month':
            case 'week':
                return  "to_char(timestamp, 'YYYY-MM-DD') as _timestamp";
            case 'year':
                return "to_char(timestamp, 'YYYY-MM') as _timestamp";
            default:
                return 'timestamp as _timestamp';
        }
    }

    public static function getTimeByType($time, $time_type)
    {
        switch($time_type) {
            case 'day':
                return date('H:i', strtotime($time));
                break;
            case 'month':
                return $time;
                break;
            case 'year':
                return $time;
                break;
            default:
                return $time;
                break;
        }
    }
}
