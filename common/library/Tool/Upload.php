<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-1-27
 * Time: 上午10:01
 * 上传文件同一处理
 */

namespace common\library\Tool;

use yii\web\UploadedFile;
use Yii;

class Upload extends UploadedFile{

    /**
     * 保存文件
     * @param $model object
     * @param $attr string
     * @return string
     */
    public static function uploadByFile($model, $attr)
    {

        if(!is_object($model)) return;

        $model->$attr = self::getInstance($model, $attr);
        $upload_path = YII::$app->params['uploadPath'];

        //上传
        if ($model->$attr->error == 0) {
            $file_path = $upload_path . date('Ymd',time()) . md5($model->$attr->baseName) . '.' . $model->$attr->extension;
//            $file_name = date('Ymd',time()) . md5($model->$attr->baseName) . '.' . $model->$attr->extension;
            $model->$attr->saveAs($file_path);
            return $file_path;
        } else {
            $model->$attr = '';
        }
    }

    public static function uploadArrayFile($model, $attr)
    {
        if(!is_object($model)) return;
        //保存上传并把Image[Gif][Gif1/Gif2...]写入model中

        $model->$attr = self::getInstances($model, $attr);
        $upload_path = YII::$app->params['uploadPath'];
        $num=0;
        $path=[];
        foreach ($model->$attr as $file) {
            //上传
            if ($file->error == 0) {

                $file_path =$upload_path . date('Ymd',time()) .$num. md5($file->baseName) . '.' . $file->extension;
                $num++;
                $file->saveAs($file_path);
                $path[]=$file_path;
            } else {
                $path[]=$file_path;
            }
        }
        return $path;


    }
    /**
     * 保存文件
     * @param $model object
     * @param $attr string
     * @return string
     */
    public static function uploadFile($model, $attr)
    {
        if(!is_object($model)) return;

        $model->$attr = self::getInstance($model, $attr);
        $upload_path = YII::$app->params['uploadPath'];

        //上传
        if ($model->$attr->error == 0) {
            $file_path = $upload_path . date('Ymd',time()) . md5($model->$attr->baseName) . '.' . $model->$attr->extension;
            $model->$attr->saveAs($file_path);
            return $file_path;
        } else {
            $model->$attr = '';
        }
    }
}