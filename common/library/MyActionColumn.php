<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\library;

use frontend\widgets\Alert;
use Yii;
use Closure;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\Column;
use backend\models\authAssignment;

/**
 * ActionColumn is a column for the [[GridView]] widget that displays buttons for viewing and manipulating the items.
 *
 * To add an ActionColumn to the gridview, add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 *     // ...
 *     [
 *         'class' => 'yii\grid\ActionColumn',
 *         // you may configure additional properties here
 *     ],
 * ]
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MyActionColumn extends Column
{
    /**
     * @var string the ID of the controller that should handle the actions specified here.
     * If not set, it will use the currently active controller. This property is mainly used by
     * [[urlCreator]] to create URLs for different actions. The value of this property will be prefixed
     * to each action name to form the route of the action.
     */
    public $controller;
    /**
     * @var string the template used for composing each cell in the action column.
     * Tokens enclosed within curly brackets are treated as controller action IDs (also called *button names*
     * in the context of action column). They will be replaced by the corresponding button rendering callbacks
     * specified in [[buttons]]. For example, the token `{view}` will be replaced by the result of
     * the callback `buttons['view']`. If a callback cannot be found, the token will be replaced with an empty string.
     * @see buttons
     */
    public $template = '{view} {update} {delete} {test}
    {point-update} {accredit} {confirm} {restore} {export} {report-table} {download-excel}{download-pdf}{update-point} {report-chart} {statistic-data}';
    /**
     * @var array button rendering callbacks. The array keys are the button names (without curly brackets),
     * and the values are the corresponding button rendering callbacks. The callbacks should use the following
     * signature:
     *
     * ```php
     * function ($url, $model) {
     *     // return the button HTML code
     * }
     * ```
     *
     * where `$url` is the URL that the column creates for the button, and `$model` is the model object
     * being rendered for the current row.
     */
    public $buttons = [];
    /**
     * @var callable a callback that creates a button URL using the specified model information.
     * The signature of the callback should be the same as that of [[createUrl()]].
     * If this property is not set, button URLs will be created using [[createUrl()]].
     */
    public $urlCreator;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->initDefaultButtons();
    }

    /**
     * Initializes the default button rendering callbacks
     */
    protected function initDefaultButtons()
    {
        // $operate = MyFunc::getPerate();
        // if($operate == 2){
        if (!isset($this->buttons['next'])) {
            $this->buttons['next'] = function ($url, $model) {
                return Html::a(
                    '<span  class="fa fa-plus-square"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'next'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model) {
                return Html::a(
                    '<span  class="glyphicon glyphicon-eye-open"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'View'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-trash"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        //点位 更新 判断点位类型进行删除
        if (!isset($this->buttons['point-update'])) {
            $this->buttons['point-update'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'Update'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        //授权 按钮
        if (!isset($this->buttons['accredit'])) {
            $this->buttons['accredit'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-floppy-saved"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'accredit'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        //确认 按钮
        if (!isset($this->buttons['confirm'])) {
            $this->buttons['confirm'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-check _confirm"></span>',
                    $url,
                    [
                        'style' => 'padding-right:20px',
                        'class' => '_conf',
                        'title' => Yii::t('app', 'Confirm') . Yii::t('app', 'Alarm information'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        // 传值到condition方法
        if (!isset($this->buttons['condition'])) {
            $this->buttons['condition'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-eye-open"></span>',
                    $url,
                    [
                        'title' => Yii::t('yii', 'View'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 屏蔽点位方法
        if (!isset($this->buttons['shield'])) {
            $this->buttons['shield'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-trash"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', '屏蔽'),
                        'data-confirm' => Yii::t('app', '你确定要屏蔽点位？'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 还原
        if (!isset($this->buttons['recovery'])) {
            $this->buttons['recovery'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-recycle"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', '还原'),
                        'data-confirm' => Yii::t('app', '你确定要还原点位？'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        if (!isset($this->buttons['/site-info/item-crud/index'])) {
            $this->buttons['/site-info/item-crud/index'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-table"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', '详细信息'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        if (!isset($this->buttons['/user-menu/set-session'])) {
            $this->buttons['/user-menu/set-session'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-tree"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', '修改菜单'),
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 数据库恢复
        if (!isset($this->buttons['restore'])) {
            $this->buttons['restore'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-history"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'Restore'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 数据库导出
        if (!isset($this->buttons['export'])) {
            $this->buttons['export'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-mail-reply-all"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'Export'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 自定义报表查看report_chart
        if (!isset($this->buttons['report-table'])) {
            $this->buttons['report-table'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-table"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'View Data'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        //报表下载
        if (!isset($this->buttons['download-excel'])) {
            $this->buttons['download-excel'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-cloud-download"></span>',
                    $url,
                    [
                        'title' => "excel下载",
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        //pdf下载
        if (!isset($this->buttons['download-pdf'])) {
            $this->buttons['download-pdf'] = function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-cloud-download"></span>',
                    $url,
                    [
                        'title' => "pdf下载",
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        //给设备绑定点位
        //pdf下载
        if (!isset($this->buttons['update-point'])) {
            $this->buttons['update-point'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-table"></span>',
                    $url,
                    [
                        'title' => "点位设置",
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
        // 自定义图表查看
        if (!isset($this->buttons['report-chart'])) {
            $this->buttons['report-chart'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-bar-chart-o"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'View Report'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }

        // 自定义数据统计
        if (!isset($this->buttons['statistic-data'])) {
            $this->buttons['statistic-data'] = function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-th-list"></span>',
                    $url,
                    [
                        'title' => Yii::t('app', 'Statistic Data'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]
                );
            };
        }
    }

    /**
     * Creates a URL for the given action and model.
     * This method is called for each button and each row.
     * @param string $action the button name (or action ID)
     * @param \yii\db\ActiveRecord $model the data model
     * @param mixed $key the key associated with the data model
     * @param integer $index the current row index
     * @return string the created URL
     */
    public function createUrl($action, $model, $key, $index)
    {
        $id_name = 'id';
        if ($action == '/site-info/item-crud/index') { // 特殊处理，必要时修正
            $id_name = 'info_group_id';
        }
        if ($this->urlCreator instanceof Closure) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index);
        } else {
            if (isset($model->type)) {
                $params = is_array($key) ? $key : [$id_name => (string)$key, 'type' => $model->type];
            } else {
                $params = is_array($key) ? $key : [$id_name => (string)$key];
            }
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;
            // 增加附带参数
            if ($action == 'point-update') {
                $params['protocol_id'] = $model->protocol_id;
            }
            if ($action == 'shield') {
                $params['protocol_id'] = $model->protocol_id;
            }
            return Url::toRoute($params + Yii::$app->request->get());
        }
    }


    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return preg_replace_callback(
            '/\\{([\w\-\/]+)\\}/',
            function ($matches) use ($model, $key, $index) {
                $name = $matches[1];
                if (isset($this->buttons[$name])) {
                    $url = $this->createUrl($name, $model, $key, $index);

                    return call_user_func($this->buttons[$name], $url, $model);
                } else {
                    return '';
                }
            },
            $this->template
        );
    }
}
