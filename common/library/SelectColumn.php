<?php

namespace common\library;

use Closure;
use yii\base\InvalidConfigException;
use yii\grid\Column;

class SelectColumn extends Column
{

    public $label = '';
    public $name = 'chart';
    public $item;
    public $selection;
    public $selectOptions = [];

    public function init()
    {
        parent::init();
        if (empty($this->name)) {
            throw new InvalidConfigException('The "name" property must be set.');
        }
        // 如果有[]就去掉[]
        if (substr($this->name, -2) === '[]') {
            $this->name = substr($this->name, 0, -2);
        }
    }

    protected function renderHeaderCellContent()
    {
        return $this->label;
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->selectOptions instanceof Closure) {
            $options = call_user_func($this->selectOptions, $model, $key, $index, $this);
        } else {
            $options = $this->selectOptions;
        }
        if(isset($this->item)){
            $selection = isset($this->selection[$key])?$this->selection[$key]:null;
            return MyHtml::dropDownList($this->name."[$key]", $selection, $this->item, $options);
        } else {
            throw new InvalidConfigException('没有选择项，请传值！');
        }
    }
}