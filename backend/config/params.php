<?php
return [
'adminEmail' => 'admin@example.com',
'icon_data' => [
'fa-folder-open'=>"<span class = 'fa fa-lg fa-fw fa-folder-open'></span>",
'fa-flask'=> "<span class = 'fa fa-lg fa-fw fa-flask'></span>",
'fa-rocket'=>"<span class = 'fa fa-lg fa-fw fa-rocket'></span>",
'fa-dot-circle-o'=>"<span class = 'fa fa-lg fa-fw fa-dot-circle-o'></span>",
'fa-exclamation-triangle'=>"<span class = 'fa fa-lg fa-fw fa-exclamation-triangle'></span>",
'fa-bomb'=>"<span class = 'fa fa-lg fa-fw fa-bomb'></span>",
'fa-stethoscope'=>"<span class = 'fa fa-lg fa-fw fa-stethoscope'></span>",
'fa-book'=>"<span class = 'fa fa-lg fa-fw fa-book'></span>",
'fa-cogs'=>"<span class = 'fa fa-lg fa-fw fa-cogs'></span>",
'fa-automobile'=>"<span class = 'fa fa-lg fa-fw fa-automobile'></span>",
'fa-child'=>"<span class = 'fa fa-lg fa-fw fa-child'></span>",
'fa-file-code-o'=>"<span class = 'fa fa-lg fa-fw fa-file-code-o'></span>",
'fa-ge'=>"<span class = 'fa fa-lg fa-fw fa-ge'></span>",
'fa-paper-plane'=>"<span class = 'fa fa-lg fa-fw fa-paper-plane'></span>",
'fa-qq'=>"<span class = 'fa fa-lg fa-fw fa-qq'></span>",
'fa-share-alt'=>"<span class = 'fa fa-lg fa-fw fa-share-alt'></span>",
'fa-camera'=>"<span class = 'fa fa-lg fa-fw fa-camera'></span>",
'fa-asterisk'=>"<span class = 'fa fa-lg fa-fw fa-asterisk'></span>",
'fa-clock-o'=>"<span class = 'fa fa-lg fa-fw fa-clock-o'></span>",
'fa-envelope'=>"<span class = 'fa fa-lg fa-fw fa-envelope'></span>",
'fa-flag-checkered'=>"<span class = 'fa fa-lg fa-fw fa-flag-checkered'></span>",
'fa-tree'=>"<span class = 'fa fa-lg fa-fw fa-tree'></span>",

],
    'i' => 0,
//默认权限
    // 默认 提供的前缀 如site控制器 point模块等 不需要验证的连接
    'default_prefix' => ['site', 'Info', 'my'],
    // 默认 不验证的 方法
    'default_action' => ['logout', 'login', 'error'],


    //系统 不需要验证的请求链接 主要是一些js段调用的  ajax请求
    //  链接数组                                                                                                   解 释
    'default_link' => ['alarm-log/crud/ajax-sources',                                               //ajax获取报警日志散点图数据的
                        'bacnet/ajax-interface',                                                     //系统自动监测接口
                        'plugin/default/ajax-real-time'
                        ],

    //chart color
    'chart_color' => [
                    '#ff9e11', '#5eaee1', '#a9d86e', '#f0f0f0', '#00FFCC', '#339999', '#6666CC', '#6699CC', '#66CCCC',
                    '#66FFCC', '#9966CC', '#99CCCC', '#99FFCC', '#CCFFCC', ''
    ],

    //chart color ---2
    'chart_color_2' => [
                    '#BF0B23', '#ff7f50', '#DDDF00', '#50B432', '#058DC7', '#ffa07a', '#4b0082'
    ]
];
