<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Unit $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Unit',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'standard_unit' => $standard_unit,
        'unit_category' => $unit_category,
    ]) ?>
