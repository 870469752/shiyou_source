<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\TimeModeSearch $searchModel
 */

//TableAsset::register ( $this );
?>
	<section id="widget-grid">
		<!-- row -->
		<div id="temp_width" class="row" >
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-5" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
					<header><span class="widget-icon"><i class="fa fa-eye"></i></span><h2>视频轮巡</h2></header>
					<div>
						<div class="widget-body" style="width:auto;height: 700px">
                            <div id="divPlugin" class="plugin" style="width: 100%; height: 100%"></div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

	<script language="JavaScript">
		window.onload = function () {
            szPort = 80;
            szUsername = "admin";
            szPassword = "12345";
            indexNum = 0;

			// 初始化插件参数及插入插件
			WebVideoCtrl.I_InitPlugin('100%', '100%', {
				iWndowType: 2,
                bDebugMode:true,
				cbSelWnd: function (xmlDoc) {
					g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
				}
			});
			WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

			// 窗口事件绑定
			$(window).bind({
				resize: function () {
					var $Restart = $("#restartDiv");
					if ($Restart.length > 0) {
						var oSize = getWindowSize();
						$Restart.css({
							width: oSize.width + "px",
							height: oSize.height + "px"
						});
					}
				}
			});

			//初始化日期时间
			var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
			$("#starttime").val(szCurTime + " 00:00:00");
			$("#endtime").val(szCurTime + " 23:59:59");

            $(document).ready(function () {
                //show_video();
                to_login();
                setTimeout(function(){setInterval('show_video()',5000);},30000);
                //
            });
		}

        function show_video(){
            $.ajax({
                url: '/video-surveillance/crud/get-camera-point'    ,
                async: false,
                dateType: 'json',
                success: function(data)
                {
                    camera_patrol(data);
                }
            });
        }

        function to_login(){
            $.ajax({
                url: '/video-surveillance/crud/get-camera-ip',
                async: false,
                dateType: 'json',
                success: function(data)
                {
                    $.each(JSON.parse(data), function (k, v) {
                        clickLogin3(v.ip);
                        sleep(100);
                    })
                }
            });
        }

        function camera_patrol(data){
            $.each(JSON.parse(data), function (k, v) {
                      var tem_ip = v.ip;   var tem_channel = v.channel;
                        RealPlay(tem_ip,tem_channel);
                        sleep(100);
            })
        }

        function sleep(n)
        {
            var  start=new Date().getTime();
            while(true) if(new Date().getTime()-start>n)  break;
        }

		// 登录
		function clickLogin3(szIP) {
            var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
                success: function (xmlDoc) {
                    console.log(szIP + " 登录成功！");
                },
                error: function () {
                    console.log(szIP + " 登录失败！");
                }
            });
            if (-1 == iRet) {
                console.log(szIP + " 已登录过！");
            }
		}

        function clickLogin2(szIP,ID) {
            var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
                success: function (xmlDoc) {
                    console.log(szIP + " 登录成功！");
                    setTimeout(function(){
                        if(indexNum==5){indexNum = 0;}
                        console.log(szIP+"**"+ID+"**"+indexNum);
                        //var oWndInfo = WebVideoCtrl.I_GetWindowStatus(indexNum);

                        //if (oWndInfo != null) {// 已经在播放了，先停止

                        //}
                        var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
                            iWndIndex:indexNum,
                            iStreamType: 1,
                            iChannelID: ID,
                            bZeroChannel: false
                        });
                        indexNum++;
                        if (0 == iRet) {
                            szInfo = "开始预览成功！";
                            var iRet = WebVideoCtrl.I_Logout(szIP);
                        } else {
                            szInfo = "开始预览失败！";
                            var iRet = WebVideoCtrl.I_Logout(szIP);
                        }
                        //var iRet = WebVideoCtrl.I_Logout(szIP);
                        /*if (0 == iRet) {
                         szInfo = "退出成功！";
                         } else {
                         szInfo = "退出失败！";
                         }*/
                        console.log(szIP + " " + szInfo);
                    },100)
                },
                error: function () {
                    console.log(szIP + " 登录失败！");
                }
            });
            if (-1 == iRet) {
                console.log(szIP + " 已登录过！");
            }
        }

        // 退出
        function clickLogout2(szIP) {
            var iRet = WebVideoCtrl.I_Logout(szIP);
            if (0 == iRet) {
                szInfo = "退出成功！";
            } else {
                szInfo = "退出失败！";
            }
            console.log(szIP + " " + szInfo);
        }

        //显示图像
        function RealPlay(ip,channel) {
            if(indexNum==5){indexNum = 0;}
            //var oWndInfo = WebVideoCtrl.I_GetWindowStatus(indexNum);

            //if (oWndInfo != null) {// 已经在播放了，先停止
                WebVideoCtrl.I_Stop(indexNum);
            //}
            var iRet = WebVideoCtrl.I_StartRealPlay(ip, {
                iWndIndex:indexNum,
                iStreamType: 1,
                iChannelID: channel,
                bZeroChannel: false
            });
            indexNum++;
            if (0 == iRet) {
                szInfo = "开始预览成功！";
            } else {
                szInfo = "开始预览失败！";
            }
            console.log(ip +"**"+channel+ "**" + szInfo);
        }

    </script>
<?php
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
?>