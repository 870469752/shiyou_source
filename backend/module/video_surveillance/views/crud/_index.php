<!-- html 区域 start-->
	<button id="show_video" onclick="show_video('507')">video</button>
<a  id="show_vedio_div"  data-toggle="modal" data-target="#myModal" ></a>
<div class="modal"  style="padding-top: 200px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 700px;height: 500px">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
				<h6 class="modal-title" id="myModalLabel">视频监控 <span style = 'margin-left:70%'>操作</span>:</h6>
			</div>
			<div class="modal-body" style = 'height: 300px'>
				<div class="row">
					<div class="col-md-12">
						<div class = 'col-md-9' >
							<div id="divPlugin"></div>
						</div>
						<div class = 'col-md-3' style = "padding-top: 30px">
							<div style = 'margin-top:18px'>
								<table style = 'margin-left:13px'>
									<tr>
										<td></td>
										<td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(1);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">
												<i class="glyphicon glyphicon-circle-arrow-up"></i></a></td>
										<td></td>
									</tr>
									<tr>
										<td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(3);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">
												<i class="glyphicon glyphicon-circle-arrow-left"></i></a></td>
										<td><a href="javascript:void(0);" onclick="mouseDownPTZControl(9);" class="btn  btn-circle btn-lg">
												<i class="glyphicon glyphicon-fullscreen"></i>
											</a></td>
										<td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(4);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">
												<i class="glyphicon glyphicon-circle-arrow-right"></i></a></td>
									</tr>
									<tr>
										<td></td>
										<td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(2);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">
												<i class="glyphicon glyphicon-circle-arrow-down"></i>
											</a></td>
										<td></td>
									</tr>
								</table>
							</div>
                            <br>
                            <br>
                            <div class = 'col-md-3' style = 'padding:0px;font-size: 6px'>
                                <table style = 'margin-left:13px;font-size: 15px'>
                                    <tr >
                                        <td></td>
                                        <td  style = 'padding:6px'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr >
                                        <td ><a href="javascript:void(0);" onmousedown="PTZZoomout()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>变倍</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZZoomIn()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:void(0);" onmousedown="PTZFoucusOut()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>焦距</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZFocusIn()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisOut()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>光圈</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisIn()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                </table>
                            </div>
						</div>
					</div>
				</div>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- END MAIN CONTENT -->



<?php
/*$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerCssFile("css/skin-bootstrap/skin-win7/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);*/
?>
<script>
	var ip;
	var g_iWndIndex = 0; //可以不用设置这个变量，有窗口参数的接口中，不用传值，开发包会默认使用当前选择窗口
	window.onload = function() {
		$('.ui-fancytree').css({'height': '400px'});
	}

	function init_env(){
		// 检查插件是否已经安装过
		if (-1 == WebVideoCtrl.I_CheckPluginInstall()) {
			alert("您还未安装过插件，双击开发包目录里的WebComponents.exe安装！");
			return;
		}

		// 初始化插件参数及插入插件
		WebVideoCtrl.I_InitPlugin(490,380, {
			iWndowType: 1,
			cbSelWnd: function (xmlDoc) {
				g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
			}
		});
		WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

		// 检查插件是否最新
		if (-1 == WebVideoCtrl.I_CheckPluginVersion()) {
			alert("检测到新的插件版本，双击开发包目录里的WebComponents.exe升级！");
			return;
		}

		// 窗口事件绑定
		$(window).bind({
			resize: function () {
				var $Restart = $("#restartDiv");
				if ($Restart.length > 0) {
					var oSize = getWindowSize();
					$Restart.css({
						width: oSize.width + "px",
						height: oSize.height + "px"
					});
				}
			}
		});

		//初始化日期时间
		var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
		$("#starttime").val(szCurTime + " 00:00:00");
		$("#endtime").val(szCurTime + " 23:59:59");
	}

	function show_video(id){

		$.ajax({
			url: '/video-surveillance/crud/get-info?&id='+id,
			async: false,
			dateType: 'json',
			success: function(data)
			{
				var temp = JSON.parse(data);
				$("#show_vedio_div").click();
                init_env();
                tmp_ip = '11.8.173.59';
                tmp_channel = 4;
				tem_state = clickLogin2(tmp_ip,tmp_channel);
			}
		});
	}

	//登录
	function clickLogin2(szIP,id) {
		clickLogout2(szIP);
        tem_szIP = szIP;
        tem_id = id;
		var iRet = WebVideoCtrl.I_Login(szIP, 1, 80, "admin", "12345", {
			success: function (xmlDoc) {
                setTimeout('RealPlay(tem_szIP,tem_id)',100);
			},
            error: function() { //失败的回调函数
                alert("登录失败！");
            }
		});
	}
	//退出登录
	function clickLogout2(szIP) {
		var iRet = WebVideoCtrl.I_Logout(szIP);
	}

	function RealPlay(ip,id) {
		var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

		if (oWndInfo != null) {// 已经在播放了，先停止
			WebVideoCtrl.I_Stop();
		}
		var iRet = WebVideoCtrl.I_StartRealPlay(ip, {
			iStreamType: 1,
			iChannelID: id,
			bZeroChannel: false
		});
	}
</script>

<?php
//$this->registerJsFile("js/vedio/jquery-1.7.1.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
?>