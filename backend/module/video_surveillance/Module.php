<?php

namespace backend\module\video_surveillance;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\video_surveillance\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
