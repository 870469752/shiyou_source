<?php

namespace backend\module\video_surveillance\controllers;

use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
