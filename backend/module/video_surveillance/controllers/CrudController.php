<?php

namespace backend\module\video_surveillance\controllers;

use Yii;
use backend\controllers\MyController;
use yii\filters\VerbFilter;
use backend\models\UnitCategoryRel;
use backend\models\point;
use backend\models\CameraPoint;
use backend\models\VideoPatrol;
/**
 * CrudController implements the CRUD actions for Unit model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $CameraPoint = new CameraPoint();
        $arr_ip = $CameraPoint::baseSelect()->select('ip')->groupBy('ip')->orderBy('ip')->asArray()->all();
        return $this->render('index',[
            'arr_ip' => $arr_ip
        ]);
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionTempIndex()
    {
        return $this->render('_index');
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */

    public function actionGetInfo()
    {
        $id = Yii::$app->request->get('id');
        $CameraPoint = new CameraPoint();
        $info = $CameraPoint::baseSelect()->select('ip,channel_name as name,channel_id as channel')->where("id = $id")->asArray()->one();
        echo json_encode($info);
    }
//    public function actionGetInfo()
//    {
//        $id = Yii::$app->request->get('id');
//        $CameraPoint = new CameraPoint();
//        $info = $CameraPoint::baseSelect()->select('ip,channel_id as channel')->where("id = $id")->asArray()->one();
//        echo json_encode($info);
//    }

    public function actionVideoPatrolSet(){
        $video_patrol=new VideoPatrol();
        $video_arr = $video_patrol->getAllVideoArr();
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionVideoPatrolIndex()
    {
        $this->cameraPoint();
        return $this->render('video_patrol_index');
    }

    static function cameraPoint()
    {
        $CameraPoint = new CameraPoint();
        $info = $CameraPoint::baseSelect()->select('ip,channel_id as channel,channel_name')->where("substring(channel_name from 1 for 3) !='Cam'")
            ->andWhere('channel_id > 0')
            ->andWhere("ip = '192.168.50.69'")
            ->asArray()->all();

        $result = array();
        $tem = 0;
        $j = 0;
        for ($i = 0; $i < count($info); $i++) {
            if ($tem == 4) {
                $tem = 0;
                $j++;
            } else {
                $result[$j][] = $info[$i];
                $tem++;
            }
        }
        $tem_num = count($result) - 1;
        if (count($result[$tem_num]) < 4) {
            unset($result[$tem_num]);
        }
        //echo "<pre>";print_r($result);die;
        $session = Yii::$app->session;
        if (!isset($_SESSION['video_patrol'])) {
            $session['video_patrol'] = $result;
        }
    }

    public function actionGetCameraIp()
    {
        $CameraPoint = new CameraPoint();
        $info = $CameraPoint::baseSelect()->select('ip')
            ->groupBy('ip')
            ->orderBy('ip desc')
            ->asArray()->all();
        //echo "<pre>";print_r($info);die;
        return json_encode($info);
    }

    public function actionGetCameraPoint(){
        $session = Yii::$app->session;
        if (!isset($_SESSION['video_patrol_key'])) {
            $session['video_patrol_key'] = -1;
        }
        if($session['video_patrol_key'] ==count($session['video_patrol'])-1 ){
            $session['video_patrol_key'] = -1;
        }

        $session['video_patrol_key']=$session['video_patrol_key']+1;
        $temp_s = $session['video_patrol_key'];
        return json_encode($session['video_patrol'][$temp_s]);
    }

    //get-info-by-id
    public function actionGetInfoById()
    {
        //$id = '308,311,379,380,381,382,383';
        $id = trim(Yii::$app->request->get('id'),',');

        $CameraPoint = new CameraPoint();
        $info = $CameraPoint::baseSelect()->select('ip,channel_name as name,channel_id as channel')->where("id  in ($id)")->orderby("id asc,channel asc")->asArray()->all();
        $ips = [];
        //echo "<pre>";print_r($info);die;
        foreach($info as $key => $value){
            if(!in_array($value['ip'],$ips)){
                $ips[]=$value['ip'];
            }
        }
        $data['info']=$info;
        $data['ips']=$ips;
        //echo "<pre>";print_r($ips);die;
        echo json_encode($data);
    }
}
