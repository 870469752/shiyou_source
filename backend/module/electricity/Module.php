<?php

namespace backend\module\electricity;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\electricity\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
