<?php

namespace backend\module\electricity\controllers;

use Yii;
use backend\models\SimulatePoint;
use backend\models\search\SimulatePointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Unit;
use common\library\MyFunc;
/**
 * CrudController implements the CRUD actions for SimulatePoint model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SimulatePoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SimulatePointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SimulatePoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SimulatePoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SimulatePoint;
        $model->unit = 'Yuan';
        if ($model->load(Yii::$app->request->post()) && $model->SimulateSave()) {
            return $this->redirect(['index']);
        } else {
            $unit = Unit::getAllUnit();
            return $this->render('create', [
                'unit' => $unit,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SimulatePoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->SimulateSave()) {
            return $this->redirect(['index']);
        } else {
            $unit = Unit::getAllUnit();
            $model->name = Myfunc::DisposeJSON($model->name);
            $model->filter_max_value = MyFunc::JsonFindValue($model->value_filter, 'data.max');
            $model->filter_min_value = MyFunc::JsonFindValue($model->value_filter, 'data.min');
            $model->interval = MyFunc::SecondChangeToTime($model->interval);
            return $this->render('update', [
                'model' => $model,
                'unit' => $unit,
            ]);
        }
    }

    /**
     * Deletes an existing SimulatePoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SimulatePoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SimulatePoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SimulatePoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
