<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 */
$this->title = Yii::t('app', 'Create Electricity Price Point');
?>
    <?= $this->render('_form', [
        'unit' => $unit,
        'model' => $model,
    ]) ?>
