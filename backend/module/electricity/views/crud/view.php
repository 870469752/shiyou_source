<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulate Points'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simulate-point-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'id',
            'interval',
            'name:JSON',
            'protocol_id',
            'base_value',
            'base_history:JSON',
            'is_available:boolean',
            'is_shield:boolean',
            'is_upload:boolean',
            'is_record:boolean',
            'is_system:boolean',
            'last_update',
            'last_value',
            'unit',
            'value_type',
            'value_filter:JSON',
            'value_pattern:JSON',
        ],
    ]) ?>

</div>
