<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <?= Html::hiddenInput('protocol_id', isset($protocol_id)?$protocol_id:'')?>

                            <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Electricity Price Name')]) ?>

                            <?= Html::hiddenInput('SimulatePoint[time_type]', 'day')?>
                            <!--在系统中选择 点位单位-->
                            <?= $form->field($model, 'unit')->dropDownList(
                                $unit,
                                ['class'=>'select2', 'style' => 'width:200px']
                            )?>

                            <?= Html::hiddenInput("SimulatePoint[value_pattern]"); ?>
                            <!--开始 日历插件-->
                            <div class="jarviswidget jarviswidget-color-magenta"
                                 data-widget-deletebutton="false"
                                 data-widget-editbutton="false"
                                 data-widget-colorbutton="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                                    <h2> <?= Yii::t('app', 'Select Time')?> </h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <div class="widget-body no-padding">
                                        <!-- content goes here -->
<!--                                        <div class="widget-body-toolbar">-->
<!--                                            <input type="text" style = "width:110px;display:none" id ="timepick" name="mydate" readonly placeholder="Select a date" class="form_datetime"/>-->
<!--                                            <div id="calendar-buttons">-->
<!--                                                <!-- add: non-hidden - to disable auto hide -->
<!--                                                <div class="btn-group" id = 'select_btn'>-->
<!--                                                    <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">-->
<!--                                                        Showing <i class="fa fa-caret-down"></i>-->
<!--                                                    </button>-->
<!--                                                    <ul class="dropdown-menu js-status-update pull-right">-->
<!--                                                        <li>-->
<!--                                                            <a href="javascript:void(0);" id="mt">Month</a>-->
<!--                                                        </li>-->
<!--                                                        <li>-->
<!--                                                            <a href="javascript:void(0);" id="ag">Agenda</a>-->
<!--                                                        </li>-->
<!--                                                        <li>-->
<!--                                                            <a href="javascript:void(0);" id="td">Today</a>-->
<!--                                                        </li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                                <div class="btn-group">-->
<!--                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"-->
<!--                                                       id="btn-prev"><i class="fa fa-chevron-left"></i></a>-->
<!--                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"-->
<!--                                                       id="btn-next"><i class="fa fa-chevron-right"></i></a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                        <div id="calendar"></div>
                                        <!-- end content -->
                                    </div>
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!--结束  日历插件-->

<!--                            <lable>--><?//=Yii::t('app', 'Record Interval')?><!-- :</lable>-->
<!--                            <a href="#"  id="interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>-->
<!--                            <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框//注意-->
<!--                            --//= Html::hiddenInput('SimulatePoint[interval]', $model->interval ? $model->interval : 'Empty', ['id' => 'interval_time'])-->
<!--                            <hr />-->
                            <?= $form->field($model, 'is_shield')->checkbox() ?>

                            <?= $form->field($model, 'is_upload')->checkbox() ?>

                            <div class="form-actions">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/qtip/jquery.qtip-1.0.0-rc3.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/bootbox/bootbox.js", ['yii\web\JqueryAsset']);
//$this->registerJsFile("js/my_js/calendar.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset'])
?>

<script>

window.onload = function () {

    var date = new Date();
    var i = 1;
    /*添加一个 时间事件*/
    function addEvent(event, end)
    {

        return $('#calendar').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])
    }

    /*添加一个 时间事件*/
    function addEventByDay(event, end)
    {
        $('#calendar').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            allDay:false,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])

    }

    /*删除一个 时间事件*/
    function removeEvent(calEvent)
    {
        $('#calendar').fullCalendar('removeEvents',calEvent._id);
        $('.qtip-content').parent().remove();
    }

    /*时间操作提示层*/
    function hintEvent(event, cal_arr, revertFunc, start, end)
    {
        bootbox.dialog({
            message: "事件发生冲突或超出范围，请选择操作：",
            title: "冲突操作",
            buttons: {
                success: {
                    label: "处理(合并)事件",
                    className: "btn-success",
                    callback: function() {
                        removeEvent(event);
                        $.each(cal_arr, function (k, v) {
                            removeEvent(v);
                        })
                        //如果含有 小时或分钟 表明该日历view不是month 需要禁用 allDay
                        if(start.getHours() || start.getMinutes()){
                            addEventByDay(start, end);
                        }
                        else{
                            addEvent(start, end);
                        }

                    }

                },
                danger: {
                    label: "返回上次设置",
                    className: "btn-danger",
                    callback: function() {
                        revertFunc();

                    }
                },
                main: {
                    label: "删除事件",
                    className: "btn-primary",
                    callback: function() {
                        removeEvent(event);
                    }
                }
            }
        });
        $('.close').css('display','none');


    }

    /*判断时间是否有冲突 -- 判断较多..*/
    function conflictEvent(event, revertFunc)
    {
        var arr_obj = [];
        var cl_start = event.start;
        var cl_end = event.end;
        var _date = $("#calendar").fullCalendar('getDate');
        var cur_month = _date.getMonth();
        var cur_year = _date.getFullYear();
        var cur_month_day = new Date(cur_year, cur_month+1, 0).getDate();
        if(event.end == null){
            if(event.start.getMonth() != cur_month){
//                revertFunc();
                cl_start = new Date(cur_year, cur_month, cur_month_day);
                // hintEvent(event, event,  revertFunc, _start, event.end);

            }
        }
        else if(event.end.getMonth() != cur_month){
            cl_start =  new Date(cur_year, cur_month, event.start.getDate()- event.end.getDate());
            if(event.start.getMonth() != cur_month)
            {
                cl_start = new Date(cur_year, cur_month, cur_month_day- (event.end.getDate()- event.start.getDate()))
            }
//            revertFunc();
//			event.start = _start;
            cl_end = new Date(cur_year, cur_month, cur_month_day);
            // hintEvent(event, event,  revertFunc, _start, _end);
        }
        $('#calendar').fullCalendar('clientEvents', function ( cal )
        {
            if(event._id != cal._id)
            {
                var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-mm");
                var event_start = $.fullCalendar.formatDate(cl_start, "yyyy-MM-dd-HH-mm");
                var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-mm");
                var event_end =  $.fullCalendar.formatDate(cl_end, "yyyy-MM-dd-HH-mm");
                var view = $('#calendar').fullCalendar('getView');
                console.log('event_start:'+event_start+',cal_start:'+cal_start);
                console.log('event_end'+event_end+',cal_end'+cal_end);
                //改为半开区间event_start < cal_end 等。
                if(view.name == 'month' && (cal_start <= event_start &&  event_start <= cal_end ) ||
                    view.name != 'month' && (cal_start <= event_start &&  event_start < cal_end ))
                {
                    console.log(1);
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start){}
                    cl_start = cal.start;
                    if(event_end <= cal_end){
                        if(cl_end == '' || cal.end >= cl_end)
                            cl_end = cal.end;
//							hintEvent(event, cal,  revertFunc, cal.start, cal.end);
                    }
                    else
                    {
                        cl_end = cl_end;
//							hintEvent(event, cal,  revertFunc, cal.start, event.end);
                    }
                }
                else if(cal_start < event_end && event_end <= cal_end)
                {
                    console.log(2);
                    arr_obj.push(cal);
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
                    if(event_start <= cal_start)
                    {
                        cl_start = cl_start
//							hintEvent(event, cal, revertFunc, event.start, cal.end);
                    }
                    else
                    {
                        if(cl_start == '' || cal.start <= cl_start)
                            cl_start = cal.start
//							hintEvent(event, cal, revertFunc, cal.start, cal.end);
                    }

                }
                else if(view.name == 'month' && event_start <= cal_start && event_end >= cal_start ||
                    view.name != 'month' && event_start <= cal_start && event_end > cal_start)
                {
                    console.log(3);
                    arr_obj.push(cal);
                    cl_start = cl_start;
                    cl_end = cl_end;
//						hintEvent(event, cal, revertFunc, event.start, event.end);
                }
                else if(event_end == '' &&
                    ((cal_start <= event_start && event_start < cal_end) ||
                        (cal_start == event_start)))
                {
                    console.log(4);
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start)
                        cl_start = cal.start;
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
//						hintEvent(event, cal, revertFunc, cal.start, cal.end);
                }
            }
            $('.qtip-content').parent().remove();
        });
        console.log(cl_start +'--'+cl_end)
        if(cl_start != event.start || arr_obj.length){
            hintEvent(event, arr_obj, revertFunc, cl_start, cl_end);
        }
    }

    /*创建一个弹出层 设置*/
    function crePopup( _jsEvent, _start, view )
    {
        var _height = $("#popover").height();
        var _width = $("#popover").width();
        var _time_type = view.name;
        var day=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
        $("#error").remove()
        $("#num").val('');
        console.log(view.name)
        if(_time_type == 'month')
        {
            var gotoDate_day = $.fullCalendar.formatDate(_start, "yyyy-MM");
            //在此 对比的日期是 利用 gotoDate 设置的日期 说明是以月为时间类型
            if(gotoDate_day == '2008-09')
            {
                $('#_times').text($.fullCalendar.formatDate(_start, "dd") + '日');
            }
            else
            {
                $('#_times').text($.fullCalendar.formatDate(_start, "MM-dd"));
            }

        }
        else if(_time_type == 'agendaWeek')
        {
            $('#_times').text(day[_start.getDay()]);
        }
        else if(_time_type == 'agendaDay')
        {
            $('#_times').text($.fullCalendar.formatDate(_start, "H:mm"));
        }
        $("#popover").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre").text("创建");
        $("#popover").attr('start_time', _start);
        $("#popover").show();
    }

    /*修改一个弹出层 设置*/
    function updatePopup(_jsEvent, cal, view)
    {
        var v = cal.title.replace(/[A-Za-z ]|[^u4E00-u9FA5|^\.]/g,'');
        $("#num").val(v);
        var _height = $("#popover").height();
        var _width = $("#popover").width();
        var _time_type = view.name;
        var day=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
        console.log(view.name)
        if(_time_type == 'month')
        {
            var gotoDate_day = $.fullCalendar.formatDate(cal.start, "yyyy-MM");
            if(gotoDate_day == '2008-09')
            {
                $('#_times').text($.fullCalendar.formatDate(cal.start, "dd") + '日 -- ' + $.fullCalendar.formatDate(cal.end, "dd") + '日')
            }
            else
            {
                $('#_times').text($.fullCalendar.formatDate(cal.start, "MM-dd") + ' -- ' + $.fullCalendar.formatDate(cal.end, "MM-dd"))
            }
        }
        else if(_time_type == 'agendaWeek')
        {
            $('#_times').text(day[cal.start.getDay()]);
        }
        else if(_time_type == 'agendaDay')
        {
            $('#_times').text($.fullCalendar.formatDate(cal.start, "H:mm") + ' -- ' + $.fullCalendar.formatDate(cal.end, "H:mm"))
        }
        $("#popover").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre").text("修改");
        $("#popover").attr('_id',cal. _id);
        $("#popover").attr('start_time', cal.start);
        $("#popover").show();
    }

    /*判断 是否有未添加 值得事件*/
    function VlaueNotNull()
    {
        $('#calendar').fullCalendar('clientEvents', function ( cal )
        {
            if(cal.title == '点击编辑时间事件'){
                $("#popover").hide();
                removeEvent(cal);
            }
        })
    }

    /*日历插件主体*/
    $('#calendar').fullCalendar (
        {
            editable: false,
            selectable:false,
            firstDay:1,
            weekNumbers:true,
            contentHeight: 300,
            slotEventOverlap:false,
            weekMode: 'liquid',
            header:{
                left: '',
                center: 'title',
                right: ''
            },
            columnFormat:{
                //  month: '-',
                week: "ddd",
                day: 'Day',
                default:false
            },
            //中文汉化
//        weekMode: 'liquid',
//        axisFormat: 'H:mm',
//        allDayText:'全天',
//        header:{
//            left: '',
//            center: 'title',
//            right: ''
//        },
//        titleFormat: {
//            month: 'yyyy' + '年' + 'MMMM',
//            week: "MMM d[ yyyy]{'&#8212:'[ MM] d yyyy}",
//            day: 'dddd, MMM d, yyyy'
//        },
//        columnFormat:{
//            month: 'ddd',
//            week: "ddd",
//            day: '天'
//        },
            //        dayNamesShort:["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            //        weekNamesShort:["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            //
            /*点击时  添加一个时间事件*/
            dayClick: function ( date, allDay, jsEvent, view )
            {
                if(date.getMonth() !== view.start.getMonth())
                {
                    return false;
                }
                else{
                    $('#calendar').fullCalendar('clientEvents', function ( cal )
                    {
                        if(cal.title == '点击编辑时间事件'){
                            removeEvent(cal);
                        }
                        var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-ss");
                        var event_start = $.fullCalendar.formatDate(date, "yyyy-MM-dd-HH-ss");
                        var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-ss");
                        if(cal_start == event_start || cal_start <= event_start && event_start <= cal_end)
                        {
                            i = i + 1;
                            //最后才会 弹出提示框
                            bootbox.alert("时间事件有冲突，请重新添加！", function() {
                                i = 1;
                                $("#popover").hide();
                            });

                            $('.close').css('display','none');
                        }
                    })
                    if(i == 1){
                        i = 1;
                        var view = $('#calendar').fullCalendar('getView');
                        if(date.getHours() || date.getMilliseconds()){
                            addEventByDay(date);
                        }
                        else{
                            addEvent(date);
                        }
                    }
                    crePopup(jsEvent, date, view);
                }
            },

            /* 监听 拖动事件  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc)
            {
                var view = $('#calendar').fullCalendar('getView');
                if(view.name != 'month' && allDay){
                    revertFunc();
                }
                VlaueNotNull();
                $("#popover").hide();
                $('#calendar').fullCalendar('viewRender ', function (view, element){

                    console.log(view)
                });
                conflictEvent(event, revertFunc);
            },

            /* 监听 改变时间块儿的大小  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventResize: function(event, dayDelta, minuteDelta, revertFunc)
            {

                VlaueNotNull();
                $("#popover").hide();
                conflictEvent(event, revertFunc);
            },


            /*友情 提示 事件*/
            eventRender: function ( event, element, view )
            {
                if(view.name == 'month'){
                    element.qtip({
                        content: event.description
                    });
                }
            },
            /*监听鼠标点击事件 ：1、点击对应的事件 将会被删除；2、消除提示框*/
            eventClick: function ( calEvent, jsEvent, view )
            {
                if(calEvent.title == '点击编辑时间事件')
                {
                    crePopup(jsEvent, calEvent.start, view);
                }
                else
                {
                    VlaueNotNull();
                    updatePopup( jsEvent, calEvent, view);
                }
//			removeEvent(calEvent);
            },
            /*渲染*/
            windowResize: function ( view )
            {
                $('#calendar').fullCalendar('render');
                var view = $('#calendar').fullCalendar('getView');
                console.log(view);
            }
        });
    //时间插件 表单弹出层
    var _div = '<div class = "rows"><div  class="col-md-4 col-sm-6 popover fade top in alert alert-info fade in" id="popover" style="display: none;">' +
        '<div class="arrow"></div>' +
        '<h3 class="popover-title alert alert-danger fade in">时间事件编辑</h3>' +
        '时间：<span id = "_times"></span><div></div>' +
        '数值：<input type="text" style = "width: 100px" id="num">' +
        '<div class="arrow"></div><hr />' +
        '&nbsp;&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "del" class = "btn bg-color-purple txt-color-white" style="ime-mode:disabled " >删除</a>' +
        '&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "cre" class = "btn btn-info">创建</a>' +
        '<a href = "javascript:void(0)" style="ime-mode:disabled;margin-left:40px" > -->more</a>' +
        '</div></div>';
    $(window).load(function(){
        $(document.body).append(_div);
        $("#del").click(function () {
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
                    console.log(cal.start + ":" + _start_time)
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                    $('#popover').hide();
                }
            })
        })

        $("#cre").click(function () {
            $("#error").remove();
            var _value = $("#num").val();
            if(! _value)
            {
                $("#num").after('<br /><span id = "error" style = "color:red;margin-left:40px">数值不能为空</span>');
                return false;
            }
            console.log('value :' + _value);
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
                    cal.title = 'value is ' + _value;
                    cal.editable = true;
                    $('#calendar').fullCalendar( 'rerenderEvents' );
                    $('#popover').hide();
                }
            })
        })

        /*限制数字*/
        $('#num').keydown(function (){
            if(!(event.keyCode==46)&&!(event.keyCode==8)&&!(event.keyCode==37)&&!(event.keyCode==39))
                if(!((event.keyCode>=48&&event.keyCode<=57)||(event.keyCode>=96&&event.keyCode<=105)||event.keyCode == 110 || event.keyCode == 190))
                    event.returnValue=false;
        })
    });
    /*************************************************************************************************************/


    var date = new Date();
    var str = '';
    var up_type = 'year';
    var _data = '';
    var $interval = $('#interval_time').val();
    /* x-editable 生成时间选择插件 */
    timeOfEditable( $("#interval") );
    $('#interval').text($interval);
    /*初始化日历事件  获取所有event 并展示到日历上*/
    <?php if ($value_pattern = $model->value_pattern): ?>
    str = <?=$value_pattern;?>;
    $.each(str, function (key, value) {
        if (key == 'data') {
            $.each(value, function (k1, v1) {
                if (k1 == 'cycle') {
                    up_type = v1;
                }
                else if (k1 == 'value') {
                    _data = '['
                    $.each(v1, function (k2, v2) {
                        var curent_day = new Date(v2['start']);
                        _data += '{';
                        _data += "start:$.fullCalendar.parseDate('" + v2['start'] + "') ,";
                        _data += "end:$.fullCalendar.parseDate('" + v2['end'] + "')  ,";
                        _data += "title: 'value is " + v2['formula'] + "',";
                        _data += 'description:' + "'<span><?=Yii::t('app', 'Prompt')?></span>:<li><?=Yii::t('app', 'Click delete this time')?></li><li><?=Yii::t('app', 'Drag the right margin change the time')?></li>',";
                        _data += 'className:' + "['event', 'bg-color-darken'],";
                        if(curent_day.getHours() || curent_day.getMinutes()){
                            _data += 'allDay: false';
                        }
                        _data += '},';
                    })

                }
            })
        }

    })
    _data = _data.substring(0, _data.length - 1);
    _data += ']';
    $('#calendar').fullCalendar('addEventSource', eval("(" + _data + ")"));
    <?php endif;?>
    /*                                                                                      结束*/


    /*添加自定义按钮 点击事件*/
    $('#calendar-buttons #btn-prev').click(function () {
        $('#calendar').fullCalendar('prev');
        return false;
    });
    $('#calendar-buttons #btn-next').click(function () {
        $('#calendar').fullCalendar('next');
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });
    /* 隐藏 日历切换按钮 */
    function hide_buttons() {
        $('.fc-header-right, .fc-header-center').hide();
        $('#calendar-buttons #btn-prev').hide();
        $('#calendar-buttons #btn-next').hide();
        $('#timepick').hide();
    }

    /* 显示 日历切换按钮 */
    function show_buttons() {
        $('.fc-header-right, .fc-header-center').show();
        $('#calendar-buttons #btn-prev').show();
        $('#calendar-buttons #btn-next').show();
        $('#timepick').show();
    }

    $('header').click(function () {
        $('#popover').hide();
    })

    $('#calendar').fullCalendar('changeView', 'agendaDay');
    $('#calendar').fullCalendar('rerenderEvents');
    $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
    hide_buttons();$('#select_btn').hide();
    /*                                                                                 对默认类型 下拉菜单 进行监听*/
//    $('#simulatepoint-time_type').change(function () {
//        var _type = $(this).val();
//        $('#select_btn').hide();
//        $('#popover').hide();
//        if (up_type != _type) {
//            $('#calendar').fullCalendar('removeEvents');
//        }
//        if (_type == 'year') {
//            timeTypeChange('month');
//            $('#calendar').fullCalendar('changeView', 'month');
//            $('#calendar').fullCalendar('rerenderEvents');
//            $('#calendar').fullCalendar('today');
//            show_buttons();
//        }
//        else if (_type == 'month') {
//            $('#calendar').fullCalendar('changeView', 'month');
//            $('#calendar').fullCalendar('rerenderEvents');
//            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
//            hide_buttons();
//        }
//        else if (_type == 'week') {
//
//            $('#calendar').fullCalendar('changeView', 'agendaWeek');
//            $('#calendar').fullCalendar('rerenderEvents');
//            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
//            hide_buttons();
//        }
//        else if (_type == 'day') {
//            $('#calendar').fullCalendar('changeView', 'agendaDay');
//            $('#calendar').fullCalendar('rerenderEvents');
//            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
//            hide_buttons();
//        }
//        else if(_type == 'custom') {
//            $('#calendar').fullCalendar('changeView', 'month');
//            $('#calendar').fullCalendar('rerenderEvents');
//            $('#calendar').fullCalendar('today');
//            show_buttons();
//            $('#select_btn').show();
//            $('#mt').click(function () {
//                timeTypeChange('month');
//                $('#popover').hide();
//                $('#calendar').fullCalendar('changeView', 'month');
//            });
//
//            $('#ag').click(function () {
//                timeTypeChange('day');
//                $('#popover').hide();
//                $('#calendar').fullCalendar('changeView', 'agendaWeek');
//            });
//
//            $('#td').click(function () {
//                timeTypeChange('day');
//                $('#popover').hide();
//                $('#calendar').fullCalendar('changeView', 'agendaDay');
//            });
//        }
//    });

    function timeTypeChange(time_type)
    {
        $(".form_datetime").datetimepicker('remove');
        $(".form_datetime").val('');
        if(time_type == 'month')
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm",
                startView: 'year',
                minView: 'year',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01');
        }
        else
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm-dd",
                startView: 'month',
                minView: 'month',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01-01');
        }
    }

    /*时间选择框中的 值 变化时 对应的 fullcalendar 也发生变化*/
    $('.form_datetime').datetimepicker().on('changeDate', function(ev){
        var time_data = new Date($(".form_datetime").val());
        $("#calendar").fullCalendar('gotoDate', time_data.getFullYear(),time_data.getMonth(),time_data.getDate());
    });

    /*                                                                                                  结束*/
    $('#simulatepoint-time_type').val(up_type);
    $('#simulatepoint-time_type').trigger('change');
    $('.jarviswidget-ctrls').hide();
    /*点击提交  收集日历上所有的事件 并将时间值赋给 value_pattern input框*/
    $('.btn').click(function () {
        var _datas = '';
        var data = $('#calendar').fullCalendar('clientEvents');
        $.each(data, function (key, value) {
            $.each(value, function (k, v) {
                if (k == 'start' || k == 'end') {
                    _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm") + ',';
                    if(k == 'end'){
                        _datas += value['title'].replace(/[A-Za-z ]|[^u4E00-u9FA5|^\.]/g,'');
                    }
                }

            })

            _datas += ';';
        })
        $("input[name='SimulatePoint[value_pattern]']").val(_datas);
    })
}
</script>