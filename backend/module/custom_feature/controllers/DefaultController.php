<?php

namespace backend\module\custom_feature\controllers;

use backend\controllers\MyController;
use backend\models\search\CustomFeatureSearch;
use Yii;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        $model = new CustomFeatureSearch();
        $dataProvider = $model->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
}
