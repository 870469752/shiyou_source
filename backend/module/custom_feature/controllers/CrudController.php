<?php

namespace backend\module\custom_feature\controllers;

use backend\controllers\MyController;
use backend\models\CustomFeature;
use backend\models\search\CustomFeatureSearch;
use Yii;
use backend\models\Menu;

class CrudController extends MyController
{
    public function actionIndex()
    {
        $model = new CustomFeatureSearch;
        $dataProvider = $model->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new CustomFeature();
        $post_custom_feature = Yii::$app->request->post($model->formName());
        if($model->load($post_custom_feature,'')){
            if(!empty($post_custom_feature['parent_id'])){
                $Menu = new Menu();
                $Menu->load($post_custom_feature,'');
                $Menu->attributes = $model->toArray();
                $Menu->depth = Menu::find()->where(['id' => $Menu->parent_id])->one()->depth + 1;
                $Menu->save();
            }
            $model->attributes = $model->toArray();

            $model->save();
        }
        return $this->redirect(['default/index']);
    }

    public function actionDelete($id)
    {
        CustomFeature::findOne($id)->delete();

        return $this->redirect(['index']);
    }
}
