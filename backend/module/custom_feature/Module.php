<?php

namespace backend\module\custom_feature;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\custom_feature\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
