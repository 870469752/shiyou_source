<?php

namespace backend\module\test;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\test\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
