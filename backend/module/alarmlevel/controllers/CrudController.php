<?php

namespace backend\module\alarmlevel\controllers;

use Yii;
use backend\models\AlarmLevel;
use backend\models\search\AlarmLevelSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\OperateLog;
/**
 * CrudController implements the CRUD actions for AlarmLevel model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AlarmLevel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlarmLevelSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single AlarmLevel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AlarmLevel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AlarmLevel;

        if ($model->load(Yii::$app->request->post()) && $model->AlarmLevelsave()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AlarmLevel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->AlarmLevelsave()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改报警等级';
            $op['en'] = 'Update alarm level';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);

            $model->name = empty($model->name) ? '[No Name]' : $model->name;

            $model->sms_receivers = json_decode($model->sms_receivers, true);

            $model->mail_receivers = json_decode($model->mail_receivers, true);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AlarmLevel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AlarmLevel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AlarmLevel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AlarmLevel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetLevelInfo($id){
        $level_info = AlarmLevel::findLevelInfo($id);
        if(!empty($level_info)){
            foreach ($level_info as $k => $v) {
                $style['title'] = $v['name'];
                $style['color'] = $this->actionAlarmColor($v['notice_style']);
                $style['timeout'] = $v['notice_siren'] * 1000;
                $style['sound'] = $this->actionAlarmSound($v['notice_fade']);
                $style['icon'] = 'fa fa-bell swing animated';
            }
        }else{
            return false;
        }
        return json_encode($style);
    }

    /**
     * 返回报警框颜色
     * @return string
     */
    public function actionAlarmColor($data)
    {
        switch($data){
            case "0":
                return "#CD0000";
                break;
            case "1":
                return "#5D478B";
                break;
            case "2":
                return "#EEB422";
                break;
            case "3":
                return "#90EE90";
                break;
            default :
                return "#90EE90";
                break;
        }
    }

    /**
     * 返回报警框颜色
     * @return string
     */
    public function actionAlarmSound($data)
    {
        $url = "http://".$_SERVER['HTTP_HOST']."/sound/";
        switch($data){
            case "0":
                return 0;
                break;
            case "1":
                return $url."voice_alert1.mp3";
                break;
            case "2":
                return $url."voice_alert2.mp3";
                break;
            case "3":
                return $url."voice_alert3.mp3";
                break;
            case "4":
                return $url."voice_alert4.mp3";
                break;
            default :
                return 0;
                break;
        }
    }
}
