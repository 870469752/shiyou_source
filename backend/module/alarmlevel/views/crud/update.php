<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmLevel $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Alarm Level',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
