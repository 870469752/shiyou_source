<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmLevel $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Alarm Level',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
