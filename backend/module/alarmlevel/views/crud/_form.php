<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmLevel $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                            <div>
                        <?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?><!--['disabled' => 'disabled']-->

						<?= $form->field($model, 'name')->textInput() ?>

						<?= $form->field($model, 'upgrade_delay')->textInput() ?>

						<?= $form->field($model, 'mail_receivers')->dropDownList(ArrayHelper::map(User::getUser(), 'id', 'username'),['class' => 'select2', 'multiple' => 'multiple']) ?>

						<?= $form->field($model, 'sms_receivers')->dropDownList(ArrayHelper::map(User::getUser(), 'id', 'username'),['class' => 'select2', 'multiple' => 'multiple']) ?>

                        <?= $form->field($model, 'notice_setting')->dropDownList(['0' => '否', '1' => '是'], [ 'class'=>'select2','style' => 'width:200px']); ?>

                        <?= $form->field($model, 'notice_style')->dropDownList(['0' => '样式一',
                                    '1' => '样式二',
                                    '2' => '样式三',
                                    '3' => '样式四'], [ 'class'=>'select2','style' => 'width:200px']); ?>

                        <?= $form->field($model, 'notice_siren')->textInput([ 'placeholder' => "填0时表示不消失，大于0的数字表示几秒后消失"]) ?>

                        <?= $form->field($model, 'notice_fade')->dropDownList(['0' => '无提示音',
                            '1' => '提示音一',
                            '2' => '提示音二',
                            '3' => '提示音三',
                            '4' => '提示音四'], [ 'class'=>'select2','style' => 'width:200px']); ?>

						<?= $form->field($model, 'log_setting')->checkbox() ?>

						<?= $form->field($model, 'upload_setting')->checkbox() ?>

						<?= $form->field($model, 'mail_setting')->checkbox() ?>

						<?= $form->field($model, 'sms_setting')->checkbox() ?>
                            </div>

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

                        <div>
                            <audio id="bgMusic" src="" autoplay="autoplay" loop="loop"></audio>
                        </div>
						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<script language="JavaScript">

    window.onload = function () {
        /* 警报框样式 */
        $("#alarmlevel-notice_style").change(function(e){
            var style_value = $("#alarmlevel-notice_style").val();
            switch (style_value){
                case "0":
                    $.bigBox({
                        title: "样式一",
                        content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                        color: "#CD0000",
                        timeout: 4000,
                        sound: false,
                        icon: "fa fa-bell swing animated",
                        number: "1"
                    });
                    break;
                case "1":
                    $.bigBox({
                        title: "样式二",
                        content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                        color: "#5D478B",
                        timeout: 4000,
                        sound: false,
                        icon: "fa fa-bell swing animated",
                        number: "2"
                    });
                    break;
                case "2":
                    $.bigBox({
                        title: "样式三",
                        content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                        color: "#EEB422",
                        sound: false,
                        timeout: 4000,
                        icon: "fa fa-bell swing animated",
                        number: "3"
                    });
                    break;
                default :
                    $.bigBox({
                        title: "样式四",
                        content: "Lorem ipsum dolor sit amet, test consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                        color: "#90EE90",
                        sound: false,
                        timeout: 4000,
                        icon: "fa fa-bell swing animated",
                        number: "4"
                    }, function() {
                        //此处可编写关闭警报后的操作，如关闭页面声音
                    });
                    break;
            }
        })

        /* 报警声音 */
        $("#alarmlevel-notice_fade").change(function(e){
            var url = "http://<?=$_SERVER['HTTP_HOST']?>"+"/sound/";
            var style_value = $("#alarmlevel-notice_fade").val();
            var soundpath = '';
            //alert(url);
            switch (style_value){
                case "0":
                    soundpath = "";
                    break;
                case "1":
                    soundpath = url+"voice_alert1.mp3"
                    break;
                case "2":
                    soundpath = url+"voice_alert2.mp3"
                    break;
                case "3":
                    soundpath = url+"voice_alert3.mp3"
                    break;
                case "4":
                    soundpath = url+"voice_alert4.mp3"
                    break;
                default :
                    soundpath = "";
                    break;
            }
            $("#bgMusic").attr('src',soundpath);
            setTimeout("$('#bgMusic').attr('src','')",5000);
        })
    }
</script>