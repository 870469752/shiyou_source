<?php

namespace backend\module\alarmlevel;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\alarmlevel\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
