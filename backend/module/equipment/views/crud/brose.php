<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('equipment', 'Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .info{font-size:10px;border-left-width:0;opacity:0.8;width:140px;}
</style>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <div class="row">
                        <div class="col-lg-12" style="">
                            <img src="/<?=$img_base['path']?>" id="base_img" class="img-responsive"
                                 style="height:<?=($img_base['height']) . 'px'?>; width:<?=($img_base['width']) . 'px'?>; alt="Base image">
                            <input type="hidden" id="e_base_pk" value="<?=$img_base['id']?>">
                        </div>
                        <div class="col-lg-2">
                            <!-- widget content -->
                            <div class="widget-body">
                                <?php foreach($point_ids as $k=>$v): ?>
                                    <?php if($v != null){ ?>
                                        <h4 class="alert alert-info info" id="info_<?=$v['id']?>" style="position:absolute; top:<?=($v['point_x']) . 'px'?>; left:<?=($v['point_y']) . 'px'?>">
                                            <?=MyFunc::DisposeJSON($v['name'])?> : <span id="<?=$v['id']?>" class="alert-span"><?=$v['last_value']?></span>
                                        </h4>
                                        <input type="hidden" value="<?=$v['r_id']?>" />
                                    <?php } ?>
                                <?php endforeach;?>
                            </div>
                            <!-- end widget content -->
                        </div>
                    </div>

                </div>
            </div>
        </article>
    </div>
</section>