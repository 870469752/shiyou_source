<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('equipment', 'Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .info{font-size:10px;border-left-width:0;opacity:0.8;width:140px;}
</style>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <div class="row">
                        <div class="col-lg-12" style="">
                            <img src="/<?=$img_base['path']?>" id="base_img" class="img-responsive base-img"
                                 style="height:<?=($img_base['height']) . 'px'?>; width:<?=($img_base['width']) . 'px'?>; alt="Base image">
                            <input type="hidden" id="e_base_pk" value="<?=$img_base['id']?>">
                        </div>
                        <div class="col-lg-2">
                            <!-- Point_data -->
                            <div class="widget-body">
                                <?php foreach($point_ids as $k=>$v): ?>
                                    <?php if($v != null){ ?>
                                        <h4 class="alert alert-info info" id="info_<?=$v['id']?>" style="position:absolute; top:<?=($v['point_x']) . 'px'?>; left:<?=($v['point_y']) . 'px'?>">
                                            <?=MyFunc::DisposeJSON($v['name'])?> : <span id="<?=$v['id']?>" class="alert-span"><?=$v['last_value']?></span>
                                        </h4>
                                        <input type="hidden" value="<?=$v['r_id']?>" />
                                    <?php } ?>
                                <?php endforeach;?>
                            </div>
                            <!-- end Point_data -->
                        </div>
                    </div>
                    <!-- Image_Group -->
                    <div class="row">
                        <div class="col-lg-12 group_div" style="">
                            <?php foreach($img_group as $val){ ?>
                            <div value="<?=$val['id']?>" >
                                <img src="/<?=$val['path']?>" class="img-responsive group_img" value="<?=$val['group_id']?>"
                                 style="position:absolute; top:<?=($val['group_x']).'px'?>; left:<?=($val['group_y']).'px'?>; height:<?=($val['height']).'px'?>; width:<?=($val['width']).'px'?>;" title="<?=$val['group_name']?>">
                                <input type="hidden" value="<?=$val['group_id']?>">
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- end Image_group -->

                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerCssFile("css/jquery-ui-1.8.23.custom.css", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function(){

        var eid = <?=$model->id?>;//equipment_id
        var bid = <?=$img_base['id']?>//base_id
        /*点位数据块position*/
        $(".alert-info").each(function(){
            $(this).draggable({
                cursor: 'move',
                containment: '.content',
                distance: 20,
                stack: '.group_img .base-img',
                stop: function(event, ui) {
                    var ele_id = String($(this).attr('id')).split('_')[1];
                    var pos = $(this).position();
                    console.log(pos);
                    $.ajax({
                        type:'post',
                        url:'/equipment/crud/save-point',
                        data:'equipment_id='+eid+'&point_id='+ele_id+'&related_point_x='+pos.top+'&related_point_y='+pos.left,
                        success:function(msg){
                            console.log(msg);
                        }
                    });
                }
            });
        });
        /*图组效果*/

        $('.group_img').each(function(){
            $(this).resizable().parent().draggable({
                stop:function(event,ui){
                    var pos = $(this).position();
                    var w = $(this).width();
                    var h = $(this).height();
                    var id = $(this).parent().attr('value');
                    $.ajax({
                        type:'post',
                        url:'/equipment/crud/save-group',
                        data:'&equipment_id='+eid+'&id='+id+'&image_group_x='+pos.top+'&image_group_y='+pos.left+'&image_group_width='+w+'&image_group_height='+h,
                        success:function(msg){
                            console.log(msg);
                        }
                    });
                    console.log($(this).parent().attr('value'));
                }
            });
        })
        /*底图效果拖动*/
        $("#base_img").resizable({
            scroll: false,
            distance: 0,
            stop: function(event, ui) {
                var e_base_pk = $('#e_base_pk').val();
                var ele_id = String($(this).attr('id'));
                var w = $(this).width();
                var h = $(this).height();
                $.ajax({
                    type:'post',
                    url:'/equipment/crud/save-base',
                    data:'id='+e_base_pk+'&equipment_id='+eid+'&image_base_id='+bid+'&image_base_width='+w+'&image_base_height='+h,
                    success:function(msg){
                        console.log(msg);
                    }
                });
            }
        });

    }
</script>