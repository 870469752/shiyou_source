<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 */

$this->title = Yii::t('equipment', 'Update {modelClass}: ', [
  'modelClass' => 'Equipment',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'point_all' => $point_all,
        'img_base_all' => $img_base_all,
        'img_group_all' => $img_group_all,
    ]) ?>
