<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\EquipmentImageGroupRelation $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('equipment', 'Equipment Image Group Relations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-image-group-relation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('equipment', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('equipment', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('equipment', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'id',
            'equipment_id',
            'image_group_id',
            'image_group_x',
            'image_group_y',
            'image_group_width',
            'image_group_height',
            'point_id',
            'dynamic_rule',
            'dynamic_value',
        ],
    ]) ?>

</div>
