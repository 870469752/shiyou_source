<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\EquipmentImageGroupRelation $model
 */

$this->title = Yii::t('equipment', 'Update {modelClass}: ', [
  'modelClass' => 'Equipment Image Group Relation',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'dynamic_rule' => $dynamic_rule,
        'point_all' => $point_all,
    ]) ?>
