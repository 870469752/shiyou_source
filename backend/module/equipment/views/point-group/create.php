<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\EquipmentImageGroupRelation $model
 */
$this->title = Yii::t('equipment', 'Create {modelClass}', [
  'modelClass' => 'Equipment Image Group Relation',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
