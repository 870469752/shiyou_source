<?php

namespace backend\module\equipment\controllers;

use backend\models\EquipmentPointRelation;
use Yii;
use backend\models\Equipment;
use backend\models\search\EquipmentSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\EquipmentImageBaseRelation;
use backend\models\EquipmentImageGroupRelation;
/**
 * CrudController implements the CRUD actions for Equipment model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Equipment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EquipmentSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Equipment model.
     * @param integer $id equipment_id
     * @return mixed
     */
    public function actionView($id)
    {
        $point_ids = EquipmentPointRelation::getOnePoint($id);

        $img_base = EquipmentImageBaseRelation::getOneImageBase($id);
        $img_group = EquipmentImageGroupRelation::getOneImageGroup($id);
        //判断点位是否开启
        foreach($img_group as $key => $value){
            foreach($point_ids as $k => $v){
                switch($value['dynamic_rule']){
                    case 0:
                        if(intval($v['last_value']) == intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    case 1:
                        if(intval($v['last_value']) != intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    case 2:
                        if(intval($v['last_value']) > intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    case 3:
                        if(intval($v['last_value']) < intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    case 4:
                        if(intval($v['last_value']) >= intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    case 5:
                        if(intval($v['last_value']) <= intval($value['dynamic_value'])){
                            $img_group[$key]['path'] = $value['dynamic_path'];
                        }else{
                            $img_group[$key]['path'] = $value['static_path'];
                        }
                    break;
                    default:
                        return;
                        break;
                }
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'point_ids' => $point_ids,
            'img_base'=> $img_base,
            'img_group'=> $img_group,
        ]);
    }
    /**
     * View a single Equipment
     */
    public function actionBrose($id){
        return $this->render('brose', [
            'model' => $this->findModel($id),
            'point_ids' => EquipmentPointRelation::getOnePoint($id),
            'img_base'=> EquipmentImageBaseRelation::getOneImageBase($id),
            'img_group'=> EquipmentImageGroupRelation::getOneImageGroup($id),
        ]);
    }
    /**
     * ajax 保存点位数据块位置
     */
    public function actionSavePoint(){
        $data = Yii::$app->request->post();
        EquipmentPointRelation::savePointPosition($data);
    }
    /**
     * ajax保存底图大小
     */
    public function actionSaveBase(){
        $data = Yii::$app->request->post();
        EquipmentImageBaseRelation::saveImageBase($data);
    }
    /**
     * ajax保存图组位置
     */
    public function actionSaveGroup(){
        $data = Yii::$app->request->post();
        EquipmentImageGroupRelation::saveImageGroup($data);
    }
    /**
     * Creates a new Equipment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Equipment;
        if($model->load($data = Yii::$app->request->post())){
            $model->image_base_id = $data['Equipment']['image_base_id'];
            $model->image_group_id = $data['Equipment']['image_group_id'];
            $result = $model->save();
            $model->savePointRelation();
            $model->saveImageBaseRelation();
            $model->saveImageGroupRelation();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '添加设施';
            $op['en'] = 'Create equipment';
            @$this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'point_all' => $model->getAllPoint(),
                'img_base_all' => $model->getAllImageBase(),
                'img_group_all' => $model->getAllImageGroup(),
            ]);
        }
    }

    /**
     * Updates an existing Equipment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->load($data = Yii::$app->request->post())){
            $model->image_base_id = $data['Equipment']['image_base_id'];
            $model->image_group_id = $data['Equipment']['image_group_id'];

            $result = $model->save();
            $model->savePointRelation();
            $model->saveImageBaseRelation();
            $model->saveImageGroupRelation();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改设施';
            $op['en'] = 'Update equipment';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'point_all' => $model->getAllPoint(),
                'img_base_all' => $model->getAllImageBase(),
                'img_group_all' => $model->getAllImageGroup(),
            ]);
        }
    }
    /**
     * 设置点位与组图关联
     */
    public function actionAccredit($id)
    {
        $this->redirect(["/equipment/point-group/index?equipment_id=$id"]);
    }

    /**
     * Deletes an existing Equipment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '添加设施';
        $op['en'] = 'Create equipment';
        @$this->getLogOperation($op,'','');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Equipment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Equipment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Equipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * ajax get point data
     * @param integer point_id
     * @return varchar data
     */
    public function actionPointData()
    {
        $rModel = new EquipmentPointRelation;
        $data = Yii::$app->request->post();
        $point_ids = $rModel->getOnePoint($data['equipment_id']);
        $info = [];
        foreach($point_ids as $v){
            $tmp['id'] = $v['id'];
            $tmp['last_value'] = $v['last_value'];
            $info[] = $tmp;
        }
        print_r($info);
    }

}
