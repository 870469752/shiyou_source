<?php

namespace backend\Module\equipment\controllers;

use Yii;
use backend\models\EquipmentImageGroupRelation;
use backend\models\search\EquipmentImageGroupRelationSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\EquipmentPointRelation;
use common\library\MyFunc;
/**
 * PointGroupController implements the CRUD actions for EquipmentImageGroupRelation model.
 */
class PointGroupController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EquipmentImageGroupRelation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EquipmentImageGroupRelationSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single EquipmentImageGroupRelation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EquipmentImageGroupRelation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EquipmentImageGroupRelation;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EquipmentImageGroupRelation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) &&$model->save()) {


            return $this->redirect(['index']);
        } else {
            $pModel = new EquipmentPointRelation();
            $point_info = $pModel->getOnePoint($model->equipment_id);
            $point_all = [];
            foreach($point_info as $k => $v){
                $point_all[$v['id']] = MyFunc::DisposeJSON($v['name']);
            }
            return $this->render('update', [
                'model' => $model,
                'dynamic_rule' => $this->_dynamic_rule,
                'point_all' => $point_all,
            ]);
        }
    }
    private $_dynamic_rule=[
        '等于','不等于','大于','小于','大于等于','小于等于'
    ];
    /**
     * Deletes an existing EquipmentImageGroupRelation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EquipmentImageGroupRelation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EquipmentImageGroupRelation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EquipmentImageGroupRelation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
