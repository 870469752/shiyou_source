<?php

namespace backend\module\equipment;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\equipment\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
