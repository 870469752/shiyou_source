<?php

namespace backend\module\subsystem_manage;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\subsystem_manage\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
