<?php
use yii\helpers\Html;
use common\library\MyHtml;
use yii\helpers\BaseHtml;
use common\library\MyFunc;
use common\library\MyActiveForm;
use yii\bootstrap;
use yii\bootstrap\ActiveForm;


?>

<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }

    #infoBoxHolder {
        z-index: 300;
        position: absolute;
        left: 5px;
    }

    #infoBox {
        border: 1px solid #999;
        padding: 8px;
        background-color: whitesmoke;
        opacity:0.9;
        position: relative;
        width: 250px;
    //height: 60px;
        font-family: arial, helvetica, sans-serif;
        font-weight: bold;
        font-size: 11px;
    }

    /* this is known as the "clearfix" hack to allow
       floated objects to add to the height of a div */
    #infoBox:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }

    div.infoTitle {
        width: 50px;
        font-weight: normal;
        color:  #787878;
        float: left;
        margin-left: 4px;
    }

    div.infoValues {
        width: 150px;
        text-align: left;
        float: right;
    }

</style>

<!--右键菜单-->
<div id="contextMenu">
    <ul>
        <li><a href="#" id="layer_set" onclick="">层次</a></li>
        <li><a href="#" id="attribute_set" onclick="">属性</a></li>
        <li><a href="#" id="console" onclick=" ">发送控制命令</a></li>
        <li><a href="#" id="font-style" onclick=" ">更新字体</a></li>
        <li><a href="#" id="font-color" onclick=" ">更新颜色</a></li>
        <li><a href="#" id="event_defend" onclick=" ">布防撤防</a></li>
    </ul>
</div>



<!-- html 区域 start-->
<section id="widget-grid" class="">
<!-- 分隔提示线 -->
<hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>
<!-- 参数主体 start-->
<div id = '_loop2' style = 'display: none;height:800px;border:2px solid #D5D5D5;padding-bottom:15px'>
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">
            <div  style = 'text-align: center'><i   rel="tooltip" data-placement="bottom" class="fa fa-align-justify _cl" data-original-title="点击收缩"></i></div>

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                    >
                    <header>
                        <div class="widget-toolbar smart-form" data-toggle="buttons">
                            <button class="btn btn-xs btn-primary" id="search_new"  >
                                点位搜索
                            </button>
                            <button class="btn btn-xs btn-primary" id="pointsUpdate"  >
                                更新点位
                            </button>
                        </div>
                    </header>
                    <!-- 参数主体 start-->
                    <div class="widget-body" style="display: inline-block;">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="well well-sm well-light">
                                    <!--               tabs----start                         -->
                                    <div id="tabs" >
                                        <ul>
                                            <li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
                                            <li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                                            <li><a href="#others" id="others_show">其他点位</a></li>
                                        </ul>
                                        <div id="bacnet">
                                            <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                                                    <th data-class="expand">ID</th>
                                                    <th data-class="expand">中文名</th>
                                                    <th data-class="expand">操作</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div id="modbus">
                                            <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                                                    <th data-class="expand">ID</th>
                                                    <th data-class="expand">中文名</th>
                                                    <th data-class="expand">操作</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div >
                                        <div id="others">
                                            <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                                                    <th data-class="expand">ID</th>
                                                    <th data-class="expand">中文名</th>
                                                    <th data-class="expand">操作</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <!--               tabs-----end                         -->

                                    <!--            点位选择框-----start              -->

                                    <div>
                                        <table id="datatable_select" class="table table-striped table-bordered table-hover" style="width:100%;">
                                            <thead>
                                            <tr>
                                                <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                                                <th data-class="expand">ID</th>
                                                <th data-class="expand">中文名</th>
                                                <th data-class="expand">操作</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!--            点位选择框-----end                -->
                                    <a id="point_allot_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                                    <?=Yii::t('app', 'Update')?>
                                    </a>

                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                    </div>
                </div>
            </article>

            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                    >
                    <header>
                        <!--                                                    <div class="widget-toolbar smart-form" data-toggle="buttons">-->
                        <!--                                                        <button class="btn btn-xs btn-primary" id="search_new"  >-->
                        <!--                                                            点位搜索添加-->
                        <!--                                                        </button>-->
                        <!--                                                    </div>-->
                    </header>
                    <!--           模式选择信息  start                        -->
                    <?=HTML::dropDownList('schema',null,$schemaSelect,['class'=>'select2','id'=>'schema'])?>
                    <!--           模式选择信息  end                        -->

                    <div id="timebutton" style=""></div>
                    <!-- 开始时间，结束时间-->
                    <div id='control_info' style="border: 1px solid #36b1ff">
                        <fieldset >
                            <fieldset>
                                <label style="margin-left: -10px" class="label">执行计划</label>
                            </fieldset>
                            <div  style="float:left;margin-right: 108px">
                                <label class="label" style="float:left">开始时间</label>
                                <select id="begin"  class="form-control" name="begin" style="float:left;">
                                    <option value="0" selected >0点</option>
                                    <?php  for($i=1;$i<24;$i++){ ?>
                                        <option value="<?=$i?>"><?=$i?>点</option>
                                    <?php }?>
                                </select>
                            </div>
                            <div id="enddate"style="float:left">
                                <label class="label" style="float:left">结束时间</label>
                                <select id="end" class="form-control" name="end">
                                    <option value="0" selected>0点</option>
                                    <?php  for($i=1;$i<24;$i++){ ?>
                                        <option value="<?=$i?>"><?=$i?>点</option>
                                    <?php }?>
                                </select>
                            </div>
                            <div style="float:left;margin-left: 800px;margin-top:20px">
                                <button type="button" id="delete" class="create btn-success2" onclick="deleteBytime()" style="display: none">删除</button>
                            </div>
                        </fieldset>
                        <!--控制字段-->
                        <fieldset id="control_attr">
                            <div style="clear:both">
                                <div style="float:left;margin-right: 10px">
                                    <label class="label">控制字段</label>
                                    <input type="text"  class="form-control" name="control_attr_name0" value="" style="width:200px">
                                </div>
                                <div style="float:left">
                                    <label class="label">默认值</label>
                                    <input type="text"  class="form-control" name="control_attr_name0" value="" style="width:200px">
                                </div>
                                <div  style="float:left;margin: 25px 100px ">
                                    <label  class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>
                                    <label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
            </article>
        </div>
    </section>
    <!--提交-->

</div>

<div  style = 'text-align: center'><i   rel="tooltip" data-placement="bottom" class="fa fa-align-justify _cl" data-original-title="点击收缩"></i></div>
<!-- 参数主体 end-->

<!-- content  饼图内容 start-->
<div>
<!-- 编辑界面html start-->
<section id="edit-UI" class="">

<!-- row -->
<div class="row">

<!-- NEW WIDGET START -->
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<!-- Widget ID (each widget will need unique ID)-->
<div class="jarviswidget jarviswidget-color-blueDark"
     data-widget-deletebutton="false"
     data-widget-editbutton="false"
     data-widget-colorbutton="false"
     data-widget-sortable="false"
    >
<header>

    <div class="widget-toolbar smart-form" data-toggle="buttons">
        <button class="btn btn-xs btn-primary" id="subsystem_config"  >
            配置
        </button>
        <button class="btn btn-xs btn-primary" id="add_subsystem"  >
            添加子系统
        </button>

        <button class="btn btn-xs btn-primary" id="save"  >
            保存
        </button>
        <button class="btn btn-xs btn-primary" id="add_link"  >
            链接
        </button>
        <button class="btn btn-xs btn-primary" id="font_color"  >
            字体/颜色
        </button>
        <button class="btn btn-xs btn-primary" id="remove_attribute"  >
            属性框
        </button>

        <button class="btn btn-xs btn-primary" id="remove_attribute_new"  >
            点位绑定
        </button>
        <button class="btn btn-xs btn-primary" id="scale_test"  >
            字体颜色放大
        </button>
    </div>



    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
    <h2>楼层子系统 </h2>

</header>


<div>
                        <span style="display: inline-block; vertical-align: top; padding: 5px">

                              <div>
                                  <div id="image_preview_parent">
                                      <div id="image_preview" style="border: solid 1px black;width: 150px; height: 100px"></div>
                                  </div>
                                  <div id="myPaletteSmall" style="border: solid 1px black;width: 150px; height: 600px">
                                      <!-- menu start -->
                                      <div id="accordion" style="width: 150px;">
                                          <h3>图1</h3>
                                          <div id="sample1" style="max-height: 450px;">
                                          </div>
                                          <h3>摄像头图</h3>
                                          <div style="max-height: 450px;">
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头1.jpg">摄像头1</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头2.jpg">摄像头2</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头3.jpg">摄像头3</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头4.jpg">摄像头4</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头5.jpg">摄像头5</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头6.jpg">摄像头6</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头7.jpg">摄像头7</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头8.jpg">摄像头8</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/state_black.svg"
                                                  config="{1:'/uploads/pic/state_red.svg',0:'/uploads/pic/state_green.svg',3:'/uploads/pic/state_black.svg'}" >1/红0绿</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/state_black.svg"
                                                  config="{0:'/uploads/pic/state_red.svg',1:'/uploads/pic/state_green.svg',3:'/uploads/pic/state_black.svg'}" >0/红1绿</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/state_black.svg"
                                                  config="{255:'/uploads/pic/state1_green.svg',0:'/uploads/pic/state_red.svg',3:'/uploads/pic/state_black.svg'}" >0/红255绿</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/belt_red.png"
                                                  config="{255:'/uploads/pic/belt_green.png',0:'/uploads/pic/belt_red.png'}" >灯带</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/Up.png"
                                                  config="{2:'/uploads/pic/Up.png',1:'/uploads/pic/Down.png',0:'/uploads/pic/Stop.png'}" >上下行</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/belt_off.png"
                                                  config="{1:'/uploads/pic/belt_on.png',2:'/uploads/pic/belt_on.png',3:'/uploads/pic/belt_on.png'}" >警铃</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state_defend" url="/uploads/pic/defend_green.png"
                                                  config="{1:'/uploads/pic/defend_green.png',2:'/uploads/pic/defend_red.png',3:'/uploads/pic/undefend_green.png',3:'/uploads/pic/undefend_red.png'}" >撤防/布防</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state_defend_1" url="/uploads/pic/defend_green.png"
                                                  config="{1:'/uploads/pic/defend_green.png',2:'/uploads/pic/defend_red.png',3:'/uploads/pic/undefend_green.png',3:'/uploads/pic/undefend_red.png'}" >防区撤防/布防</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state_defend_ctrl" url="/uploads/pic/defend_green.png"
                                                  config="{1:'/uploads/pic/defend_green.png',2:'/uploads/pic/defend_red.png',3:'/uploads/pic/undefend_green.png',3:'/uploads/pic/undefend_red.png'}" >周界撤防/布防</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/on.svg"
                                                  config="{1:'/uploads/pic/on.svg',0:'/uploads/pic/off.svg',3:'/uploads/pic/on.svg'}" >闸刀</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/on1.svg"
                                                  config="{1:'/uploads/pic/on1.svg',0:'/uploads/pic/off1.svg',3:'/uploads/pic/on1.svg'}" >竖闸刀</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state_control" url="/uploads/pic/A16_red_green.svg"
                                                  config="{3:'/uploads/pic/A16_green.svg',0:'/uploads/pic/A16_red_green.svg','state':{0:'关',255:'开'}}" >应急照明</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state_control" url="/uploads/pic/A16_red_purple.svg"
                                                  config="{3:'/uploads/pic/A16_purple.svg',0:'/uploads/pic/A16_red_purple.svg','state':{0:'关',255:'开'}}" >场景照明</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/water_g.svg"
                                                  config="{1:'/uploads/pic/water_y.jpg',0:'/uploads/pic/water_g.jpg',3:'/uploads/pic/water_g.jpg'}" >水滴</a>

                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/water_green_alarm.png"
                                                  config="{1:'/uploads/pic/water_red_alarm.png',2:'/uploads/pic/water_green_alarm.png',3:'/uploads/pic/water_green_alarm.png',4:'/uploads/pic/water_green_alarm.png'}" >漏水报警</a>
                                          </div>
                                          <h3>编辑框</h3>
                                          <div style="max-height: 450px;">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value" url="/uploads/pic/按钮6.png">值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value" url="/uploads/pic/按钮3.png">值2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="only_value" url="">only值</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="rule_value" url="">rule值</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value_1" url="" config="{'default':'-1'}">电梯值-1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value_1" url="" config="{'default':'-1',2:'null'}">电梯值无2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="edit">编辑</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="onoffvalue" url="/uploads/pic/按钮3.png">开关值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="onoffvalue" url="/uploads/pic/按钮6.png">开关值2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="alarmvalue" url="/uploads/pic/按钮3.png">警报值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="alarmvalue" url="/uploads/pic/按钮6.png">警报值2</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 正常/警报"
                                                 config="{0:'正常',1:'警报'}"         >0/1 正常/警报</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 警报/正常"
                                                 config="{0:'警报',1:'正常'}"         >0/1 警报/正常</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 正常/故障"
                                                 config="{0:'正常',1:'故障'}"         >0/1 正常/故障</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 故障/正常"
                                                 config="{0:'故障',1:'正常'}"         >0/1 故障/正常</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 运行/停止"
                                                 config="{0:'运行',1:'停止'}"         >0/1 运行/停止</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1停止/运行"
                                                 config="{0:'停止',1:'运行'}"         >0/1 停止/运行</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 开/关"
                                                 config="{0:'开',1:'关'}"         >0/1 开/关</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 关/开"
                                                 config="{0:'关',1:'开'}"         >0/1 关/开</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 手动/自动"
                                                 config="{0:'手动',1:'自动'}"         >0/1 手动/自动</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="state_value" url="0/1 就地/远控"
                                                 config="{0:'自动',1:'手动'}"         >0/1 自动/手动</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="table">表格</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group"  stroke="white" fill="transparent">透明分组</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group"  stroke="#1D1B1D" fill="#989898">灰色分组</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group"  stroke="#1D1B1D" fill="#FFFFD1">淡黄色分组</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group"  stroke="#1D1B1D" fill="#DFFFD1">淡绿色分组</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group"  stroke="#1D1B1D" fill="#FFDFDF">淡红色分组</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="many" url="/uploads/pic/many_point.jpg">many</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="main_edit" url="/uploads/pic/按钮6.png">main_edit</a>
                                          </div>
                                          <h3>按钮</h3>
                                          <div style="max-height: 450px;" id="sample4">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮1.png">按钮2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮2.png">按钮3</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮3.png">按钮4</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'启动',1:'停止'}"         >0/1 启动/停止</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'停止',1:'启动'}"         >0/1 停止/启动</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'冬季',1:'夏季'}"         >0/1 冬季/夏季</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'夏季',1:'冬季'}"         >0/1 夏季/冬季</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'远控',1:'就地'}"         >0/1 远控/就地</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'就地',1:'远控'}"         >0/1 就地/远控</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'开',1:'关'}"         >0/1 开/关</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'关',1:'开'}"         >0/1 关/开</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'关',-1:'开'}"         >0/-1 关/开</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'关',255:'开'}"         >0/255 关/开</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="control" url="/uploads/pic/按钮3.png"
                                                 config="{0:'1'}"         >手动设置</a>

                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="line1">线1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="Inductor">电容</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="Ground">接地</a>
                                          </div>

                                      </div>
                                      <!-- end-->
                                  </div>


                              </div>
                 </span>

                     <span style="display: inline-block; vertical-align: top; padding: 5px; width:80%">
                    <div id="tabs2">
                        <ul>
                            <!--            隐藏初始化页面                -->
                            <li style="display:none;">
                                <a href="#tabs-0">sample</a>
                            </li>
                        </ul>
                        <div id="tabs-0">
                            <!--         隐藏初始化页面                  -->
                            <div id="myDiagramX" style="display:none;border: solid 1px black; width:100%; height:650px;"></div>
                        </div>
                    </div>
                     </span>
    <div id="infoBoxHolder">
        <!-- Initially Empty, it is populated when updateInfoBox is called -->
    </div>
</div>




</div>
</article>
</div>
</section>
<!-- 编辑界面html end-->
</div>


</section>
<!-- html 区域 end-->

<!--add link-->
<div id="link_update" title="子系统" style="display:none;width: auto">

    <form>
        <fieldset>
            <div class="form-group">
                <label>名称</label>

                <input class="form-control" id="node_name" value="" placeholder="Text field" type="text">

            </div>


            <div class="form-group">
                <label>节点链接</label>

                <input class="form-control" id="node_link" value="" placeholder="Text field" type="text">

            </div>





        </fieldset>

    </form>

</div>

<!-- font  color-->
<div id="update_font_color" title="字体/颜色" style="width: 400px;;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体类型',null,  ['class' => 'control-label' ])?>
        <select id="font_family" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="微软雅黑">微软雅黑</option>
            <option   value="华文细黑">华文细黑</option>
            <option   value="中易宋体">中易宋体</option>
            <option   value="sans-serif">sans-serif</option>
            <option   value="Arial">Arial</option>
        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体变形',null,  ['class' => 'control-label' ])?>
        <select id="font_variant" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="normal">正常</option>
            <option   value="small-caps">小型</option>
            <option   value="inherit">继承型</option>

        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体粗细',null,  ['class' => 'control-label' ])?>
        <select id="font_weight" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="normal">正常</option>
            <option   value="bold">粗体</option>
            <option   value="bolder">更粗</option>
            <option   value="lighter">更细</option>
        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体大小',null,  ['class' => 'control-label' ])?>
        <select id="font_size" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">

            <option   value="xx-small">最小</option>
            <option   value="x-small">较小</option>
            <option   value="small">小</option>
            <option   value="medium">正常</option>
            <option   value="large">大</option>
            <option   value="x-large">较大</option>
            <option   value="xx-large">最大</option>
        </select>
    </div>
    <!--            <div class="col-md-9" style="margin: 5px;">-->
    <div style="margin: 5px;width: 300px;height: 200px;">
        <?=Html::label('字体颜色',null,  ['class' => 'control-label' ])?>
        <select id="fontcolor" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="#D2651D">红色</option>
            <option   value="black">黑色</option>
            <option   value="white">白色</option>
            <option   value="#FFFF00">黄色</option>
        </select>
    </div>
    <div style="margin: 5px;width: 300px;height: 200px;">
        <label>scale</label>
        <input class="form-control" id="scale" value="" placeholder="Text field" type="text">
    </div>

</div>


<div id="layer_name" title="层次选择" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('类型',null,  ['class' => 'control-label'])?>
        <select id="layername" class= "chosen_select"   style="width: 100%" name="layername" placeholder="请选择相应点位">
            <option   value="Background">底图</option>
            <option   value="Foreground">非底图</option>-->
        </select>
    </div>
</div>

<!-- font  color-->
<div id="console_board" title="命令控制台" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('类型',null,  ['class' => 'control-label'])?>
        <div id="control_drop" style="width: 300px;height: 110px;   ">

        </div>
    </div>
</div>

<!--属性设置 -->
<div id="attribute_set_dialog" title="属性设置" style="width: 400px;">
    <div class="col-md-9" style="margin: 1px;">
        <?=Html::label('操作',null,  ['class' => 'control-label'])?>
        <?=Html::textInput('operator', '',  ['class' => 'form-control','placeholder' => '', 'id' => 'operator'])?>
    </div>
    <div class="col-md-9" style="margin: 1px;">
        <?=Html::label('后缀符号 ',null,  ['class' => 'control-label'])?>
        <?=Html::textInput('output', '',  ['class' => 'form-control','placeholder' => '', 'id' => 'output'])?>
    </div>
</div>

<!-- add tab Demo -->
<div id="addtab" title="子系统" style="display:none;width: auto">

    <form>

        <fieldset>
            <input name="authenticity_token" type="hidden">
            <div class="form-group">
                <label>子系统名称</label>
                <input class="form-control" id="sub_system_name" value="" placeholder="Text field" type="text">
            </div>

            <div class="form-group">
                <label>有无底图</label>
                <?=HTML::dropDownList('image_base',null,[0=>'无',1=>'有'],['class'=>'select2','id'=>'image_base'])?>

            </div>
            <div class="form-group" id="selected_image_div" style="display:none">
                <label>底图选择</label>
                <?=HTML::dropDownList('image',null,$image_data,['class'=>'select2','id'=>'selected_image'])?>

            </div>
            <div class="form-group">
                <label>子系统类型</label>
                <!--                <select id="sub_system_category" class="select2" name="sub_system_category">-->
                <!--                    <option value="3">&nbsp;给排水</option>-->
                <!--                    <option value="4">排风机</option>-->
                <!--                </select>-->
                <?=HTML::dropDownList('sub_system_category',null,$sub_system_in,['class'=>'select2','id'=>'sub_system_category'])?>

            </div>


        </fieldset>

    </form>

</div>

<!--多点位展示-->
<div id="many_points" >
    <!--         Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-4" data-widget-editbutton="false" data-widget-deletebutton="false">

        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2><?=Yii::t('point', '点位')?></h2>

        </header>

        <div>

            <table id="selected_datatable_tabletools3" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                    <th data-class="expand">值</th>
                </tr>
                </thead>
                <tbody id="tbodyData_many_points">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!--属性面板-->
<div id="AttributeMenu"  style="width: auto;position:relative;z-index:10;display: ">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="mydiagram_data" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                <?=Html::hiddenInput('image_group_id',$sub_system_model->id,['id'=>'image_group_id'])?>
                                <div class="row" style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('sub_system_id',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('sub_system_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'sub_system_id'])?>
                                    </div>
                                </div>

                                <div class="row" style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                </div>

                                <div id='infoBox'>
                                    <div>Info</div>
                                    <div class='infoTitle'>属性名称</div>
                                    <div class='infoValues'>值</div>

                                    <div  class='infoTitle'>子系统id</div>
                                    <div id='subsystem' class='infoValues'>值</div>

                                    <div  class='infoTitle'>key</div>
                                    <div id='node_key' class='infoValues'>值</div>

                                    <div  class='infoTitle'>分类</div>
                                    <div id='node_category' class='infoValues'>值</div>

                                    <div  class='infoTitle'>点位id</div>
                                    <div id='point_id' class='infoValues'>值</div>

                                    <div class='infoTitle'>点位值</div>
                                    <div  id='point_value'class='infoValues'>值</div>

                                    <div class='infoTitle'>更新时间</div>
                                    <div id='update_time' class='infoValues'>值</div>
                                </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>

<!--<input type="button" id="save" value="save">-->
<!--<input type="button" id="update" value="update">-->
<textarea id="mySavedModel" style="width:100%;height:300px;display:none ;">

</textarea>
<p id="diagramEventsMsg" style="display: none;">Msg</p>


<!-- html 点位搜索弹出框 start-->
<section id="point_search" class=""style="width: 600px;position:relative;z-index:10;display:none ">
<!-- 分隔提示线 -->
<hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

<!-- content  图表内容 start-->
<div class="jarviswidget jarviswidget-color-blueDark"
     data-widget-deletebutton="false"
     data-widget-editbutton="false"
     data-widget-colorbutton="false"
     data-widget-sortable="false"
     data-widget-Collapse="false"
     data-widget-custom="false"
     data-widget-togglebutton="false" id = '_jarviswidget'>
<header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
    <h2><?=Yii::t('app', 'Search Point')?></h2>
    <div class="jarviswidget-ctrls">

    </div>
</header>
<!-- 点位 搜索展示框 start-->
<!--         Widget ID (each widget will need unique ID)-->
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">

    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2><?=Yii::t('point', '点位')?></h2>

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->

        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body no-padding">

            <table id="selected_datatable_tabletools1" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr class="tr_head">
                    <th data-hide="phone">点位ID</th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>

                </tr>
                </thead>
                <tbody id="tbodyData_one">

                </tbody>
            </table>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<div id="_loop1" class="form-actions">
    <?= Html::submitButton(Yii::t('app', '搜索添加点位') , ['class' => 'btn btn-primary', 'id' => 'sub_prev']) ?>
    <?= Html::submitButton(Yii::t('app', '绑定点位') , ['class' => 'btn btn-primary', 'id' => 'sub_next']) ?>
</div>
<!-- end widget -->
<!-- 点位 搜索展示框 end-->
<!-- 参数主体 start-->
<!-- 点位搜索框 start      -->
<div id = '_loop' style = 'border:2px solid #D5D5D5;padding-bottom:15px'>
    <div style = 'padding:0px 7px 15px 7px'>
        <?php $form = MyActiveForm::begin(['method' => 'get', 'id' => '_ws']) ?>
        <fieldset>
            <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Point Name'), 'class' => 'form-control','id'=>'event_name'])?>
            <div class="form-group" style="margin-top: 10px">

                <!--                        <div class="col-md-1"></div>-->
                <div class="col-md-6">
                    <?=MyHtml::dropDownList('energy_category_id', 0, $category_tree
                        , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true])?>
                </div>
                <div class="col-md-6">
                    <?=MyHtml::dropDownList('location_category_id', 0, $location_tree
                        , ['placeholder' => Yii::t('app', 'Choose Location'), 'class' => 'select2', 'tree' => true])?>
                </div>


            </div>

            <!--提交-->
            <div style="float: right;margin-top: 12px">
                <a id="point_search_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                <?=Yii::t('app', 'Search Point')?>
                </a>


                <a id="event_search_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                <?=Yii::t('app', 'Search Event')?>
                </a>


                <a id="point_event_search_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                <?=Yii::t('app', 'Search Point Event')?>
                </a>
            </div>
        </fieldset>
    </div>

    <?php MyActiveForm::end(); ?>
</div>


<div id= 'point_search_div' class="widget-body"  ">

<?=Html::hiddenInput('points_event', null,['id'=>'points_event'])?>
<?= Html::hiddenInput('point_ids', '', ['id' => 'point_selected'])?>

<fieldset style="display:none;">

    <!-- widget div-->
    <div class = 'form-group'>
        <label class="col-sm-1 control-label"><?=Yii::t('app', 'Bacth Filter')?></label>
        <div class = 'col-sm-6' >
            <?=Html::dropDownList('point_ids', '', ['' => '',
                ],
                ['id' => 'point_select', 'multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Point Type')])?>
        </div>
    </div>

    <!--    批量修改 点位的一些属性      -->
    <div class = 'form-group' style = 'margin-top:230px'>

    </div>


    <div style="display: inline-block;">
    </div>

</fieldset>



<!-- Widget ID (each widget will need unique ID)-->
<div  class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false" data-widget-deletebutton="false">

    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2><?=Yii::t('point', 'Point List')?></h2>

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->

        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body no-padding">

            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr class="tr_head">
                    <th data-hide="phone">点位ID</th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                </tr>
                </thead>
                <tbody id="tbodyData">

                </tbody>
            </table>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->
</div>
<!-- end widget -->





<!-- Widget ID (each widget will need unique ID)-->
<div style="display: none" class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">

    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2><?=Yii::t('point', 'Selected Point')?></h2>

    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->

        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body no-padding">

            <table id="selected_datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr class="tr_head">
                    <th data-hide="phone">点位ID</th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                </tr>
                </thead>
                <tbody id="tbodySelected">

                </tbody>
            </table>

        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<!-- end widget -->



<div class="form-actions">
    <?= Html::submitButton(Yii::t('app', '更新名字') , ['class' => 'btn btn-primary', 'id' => '_update_name']) ?>
    <?= Html::submitButton(Yii::t('app', '下一步') , ['class' => 'btn btn-primary', 'id' => '_sub']) ?>

    <div style="">
        <div style = 'padding-left:12px;margin-right:30px;margin-bottom:30px;color:red;text-align: left; display:none' id = '_error_log'></div>
    </div>

</div>

<!-- <?php //ActiveForm::end(); ?> -->
<!--            --><?php //ActiveForm::end(); ?>
<!--            <button id="checkAllBtn" class="button">CheckAll</button>-->
</div>

<!-- 点位搜索框 end      -->
</div>
<!-- 图表内容 end-->
</section>

<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/ScrollingTable.js" );

$this->registerJsFile("js/gojs/node_template_edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/socket/socket.io.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);


$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<script>
var sub_system_data=new Array();
//子系统详细信息
var sub_system_info=<?=$sub_system_data?>;
console.log(sub_system_info);
//子系统绑定的  点位 事件点位
var point_data=new Array();
var event_data=new Array();
var error_data=new Array();
//搜索框的数据
window.GlobalData=new Array();
//选择框的数据
window.selectedPointContainer = new Array();
//新的选择框数据
var new_selected_point=new Array();
var sub_system_msg=null;
var link=null;
var link_name=null;
var button=null;
var button_key=null;
//当前点击的diagram图对象
var diagram_now=null;
//当前点击的图内的节点对象
var node_now=null;
//    当前楼层所有点位信息
var location_points=eval(<?=$points?>);
//读取模式信息生成下拉信息配置
var schemaInfo=<?=$schema_info?>;
var all_image=<?=json_encode($all_image)?>;
var image_url='<?=$image_base?>';
image_url='uploads/20151223edb59ef8fc9ecae9b207906dfcd00426.jpg';
//data数组,time,control数组
var timeModelData=[];
var time=[];
var control=[];
//控制字段与默认值
var control_name_value=[];
//时间段个数
var time_count=0;
//每个时间段控制记录数
var control_name_count=0;
//总记录数
var count=0;
//得到data中所有时间
var alltime=[];
//修改标志
var flag=false;
//当前修改的时间段
var currentTime=[];
//初始页面数据
var initData;
//删除的起始时间
var deletestart;
console.log(schemaInfo);

var oTable_others; var oTable_bacnet; var oTable_modbus;
var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0;
//判断元素是否在数组中
//第三位参数为类型  判断是 key还是value
function in_array(array,element,type){
    for(var i in array){
        if(type=='element') {
            if (array[i] == element)
                return 1;
        }
        if(type=='key') {
            if (i == element)
                return 1;
        }
    }
    return 0;
}

function toDecimal(x){
    var f=parseFloat(x);
    if(isNaN(f)){
        return;
    }
    f=Math.round(x*100)/100;
    return f;
}
function contextmenu(type){
    var flag='';
    var menus=["layer_set","attribute_set","console","font-style","font-color","event_defend"];
    if(type=='hide')flag='none';
    for(var i in menus){
        var element=menus[i];
        document.getElementById(menus[i]).style.display=flag;
    }
}
function showMyMenu(array){
    for(var i in array){
        var element=array[i];
        document.getElementById(array[i]).style.display='';
    }
}
function resize() {
    for(var key in sub_system_data) {
        var diagram = sub_system_data[key];
        diagram.requestUpdate();
//             diagram.rebuildParts();
    }

}
//修改点位名称后更改selectedPointContainer 内的值
function update_selected(id,value){
    for(var key in selectedPointContainer){
        if(selectedPointContainer[key]['pointId']== id)
            selectedPointContainer[key]['pointName']= value;
    }
}
//点击时间按钮显示详细信息
function show(object){
    flag=true;
    var text=$(object).html();
    var begin=parseInt(text.substring(0,1));
    var end=parseInt(text.substring(4,5));
    deletestart=begin;
    $('#delete').show();
    $('#control_attr').html('');
    for(var i=0;i<timeModelData.length;i++){
        control_name_count=0;
        if(begin==timeModelData[i][0][0] && end==timeModelData[i][0][1]){
            timeNbumber=i;
            $('#begin').val(begin);
            $('#end').val(end);
            for(var j=0;j<timeModelData[i][1].length;j++){
                control_name_count=j;
                var control_name=timeModelData[i][1][j][0];
                var control_value=timeModelData[i][1][j][1];
                var name='control_attr_name'+j;
                var str='<div style="clear:both">'+
                    '<div style="float:left;margin-right: 10px">'+
                    '<label class="label">控制字段</label>'+
                    '<input type="text"  class="form-control" name='+name+' value='+control_name+ ' style="width:200px">'+
                    '</div>'+
                    '<div style="float:left">'+
                    '<label class="label">默认值</label>'+
                    '<input type="text"  class="form-control" name='+name+' value='+control_value+ ' style="width:200px">'+
                    '</div>'+
                    '<div  style="float:left;margin: 25px 100px ">'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
                    '</div>'+
                    '</div>';
                $('#control_attr').append(str);
            }
            currentTime.push(begin);
            currentTime.push(end);
        }
    }
}
window.onload = function () {

    //redis  socket.io
    // 得到point  event数据
    // 放入 point_data event_data 缓冲池
    var socket = io('http://11.29.1.17:9090');
    socket.on('connection', function () {
        console.log('connection setup for socket.io')
    });
    var socket_name='Point:'+'<?=$location_id?>';
    var points=sub_system_info['points'];
    var events=sub_system_info['events'];
    var send_message={
        socket_name:socket_name,
        Points:points,
        Events:events
    };
    console.log(send_message);
//        points=JSON.stringify(points);
//        events=JSON.stringify(events);
//        console.log(typeof (socket_name));
//        var send_message=eval('[{socket_name:'+socket_name+',Points:'+points+',Events:'+events+'}]');

    socket.emit('socket_connect',send_message);
    socket.on(socket_name, function (msg) {
        console.log(msg);
        var channel=msg['channel'];
        if(msg['value']=="False")msg['value']=0;
        if(msg['value']=="True")msg['value']=1;
        var channel_id=channel.substr(6, channel.length);
        if(channel.indexOf('Point')!=-1) {
//                console.log('Point');
            if (in_array(sub_system_info.points,channel_id)==1,'element') {
                point_data[channel_id]=toDecimal(Number(msg['value']));
            }
        }
        if(channel.indexOf('Event')!=-1) {
//                console.log('Event');
            if (in_array(sub_system_info.events,channel_id)==1,'element') {
                event_data[channel_id]=toDecimal(Number(msg['value']));
            }
        }
    })

    //参数设定 js
    $('._cl').on('click', function () {
        if ($("#_loop2").css('display') == 'none') {
            $('#_line').hide();
            $('#edit-UI').hide();
        } else {
            $('#_line').show();
            $('#edit-UI').show();
//                if($chart_data.length){$('#_jarviswidget').show();}
        }
        //
        $("#_loop2").slideToggle("slow");

    });
    $("#scale_test").click(function(){
        fontupdate('scale');
    })
//        console.log(111111);
//        console.log(selectedPointContainer.length);
    //把image加入到左侧 可预览的列表中
    var html='';
    html_gif='';
    <?php foreach ($all_image as $value) {?>
    switch (<?=$value['type']?>){
        case 4:
            var config=<?=$value['phase_config']?>;

            if(config['data']['Gif']!=undefined) {
//                    console.log( JSON.stringify(config['data']['Gif']));
                html_gif = html_gif +
                    '<a  ' +
                    'href="javascript:void(0);"   ' +
                    'class="btn btn-default btn-xs btn-block" ' +
                    'category="gif" ' +
                    'url="/' + '<?=$value['url']?>' +'"'+
                    'config=' + JSON.stringify(config['data']['Gif'])  +'>' +
                    '<?=MyFunc::DisposeJSON($value['name'])?>' +
                    '</a>';
            }
            break;
        default :
            html=html+'<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" category="main" url="/'+'<?=$value['url']?>'+'">'+'<?=MyFunc::DisposeJSON($value['name'])?>'+'</a>';
            break;
    }
    <?php } ?>

    $("#sample1").append(html);
    var gifdata='["uploads/pic/gif1.png","uploads/pic/gif2.png","uploads/pic/gif3.png"]';
//        var gif1='["uploads\/201603020d794ec9b197e64ef4973149ef5769b20.jpg","uploads\/20160302185ca073b40d16b7b2b9f83c36b4daf1b.jpg","uploads\/2016030222a1b2084879b4d83a9ede76d698a125a.jpg","uploads\/201603023f01ffc16109ad478f70c3a110d2bcad3.jpg","uploads\/201603024d4fbe35bf02c60830b0aa32b8654029e.jpg"]'
    var gif1= '<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" ' +
        'category="gif" url="/uploads/pic/gif.png" ' +
        'config='+gifdata +'>'+
        'test'+
        '</a>';
    var gif_value_error='["uploads/pic/transparent.png","uploads/pic/Alarm-Error.png"]';
    var gif_value1='<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" ' +
        'category="gif_value" url="/uploads/pic/Alarm-Error.png" ' +
        'active_value ="405"'+
        'config='+gif_value_error +'>'+
        '故障405'+
        '</a>';

    var gif_value_alert='["uploads/pic/transparent.png","uploads/pic/alert-round.png"]';
    var gif_value2='<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" ' +
        'category="gif_value" url="/uploads/pic/alert-round.png" ' +
        'active_value ="10900"'+
        'config='+gif_value_alert +'>'+
        '报警10900'+
        '</a>';

    var gif_value_camera='["uploads/pic/摄像头8.jpg","uploads/pic/transparent.png"]';
    var gif_value_camera='<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" ' +
        'category="gif_camera" url="/uploads/pic/摄像头8.jpg" ' +
        'active_value ="1"'+
        'config='+gif_value_camera +'>'+
        '警报摄像头'+
        '</a>';
    $("#sample4").append(gif1);
    $("#sample4").append(html_gif);
    $("#sample4").append(gif_value1);
    $("#sample4").append(gif_value2);
    $("#sample4").append(gif_value_camera);
    //标签绑定点击事件 生成预览图
    $("a").click(function(){
        preview_test(this);
    });

    $("#attribute_set").click(function(){
        if(node_now.category=='rule_value'){
            if(node_now.config!=undefined){
            $("#operator").val(node_now.config.operator);
            $("#output").val(node_now.config.output);
            }
        }
        attribute_set_dialog.dialog("open");
        contextmenu('hide');
    });


    function attribute_update(attribute,value){
        var diagram=diagram_now;
        var key=node_now.key;
        var model = diagram.model;
        var arr = model.nodeDataArray;
        model.startTransaction("flash");
        for (var i = 0; i < arr.length; i++) {
            var data = arr[i];
            if (data.key==key) {
                model.setDataProperty(data, attribute, value);
            }
            diagram.model.commitTransaction("flash");
        }
    }
    //字体颜色改变
    $("#font-color").click(function(){
        fontupdate('color');
    })

    //字体颜色改变
    $("#font-style").click(function(){
        fontupdate('font');
    })
    //撤防布防  set撤防布防的值
    $("#event_defend").click(function(){
        var sub_system_id = document.getElementById("sub_system_id").value;
        var key=document.getElementById("element_id").value;
        var defend=1;
        var node_type=node_now.category;
        //撤防布防 为第三张图时为撤防
        if(node_now.img==node_now.text[3])
            defend=0;
        $.ajax({
            type: "POST",
            url:"/subsystem_manage/crud/ajax-set-defend",
            data: {
                sub_system_id: sub_system_id,
                element_id: key,
                node_type:node_type,
                defend: defend
            },
            success: function (msg) {
                console.log('msg');
            }
        });
        diagram_now.currentTool.stopTool();
    })
//      点击按钮预览节点
    function preview_test(obj){
        var url=obj.getAttribute("url");
        var config=obj.getAttribute("config");
        var category=obj.getAttribute("category");
//            console.log(url);
        var value={category: 'error'};
        switch (category) {
            case 'main'         :var value = { category: 'main',size: '75 75',img: url};    break;
            case 'many'         :var value = { category: 'many',size: '75 75',img: url};   break;
            case 'state'        :
            case 'state_control'   :
                console.log(typeof (config));
                config=eval("["+config+"]")[0];
                console.log(config);
                var value = { category: category, size : '75 75',img:url,text:config};                   break;
            case 'value'        :var value = {category: 'value',img:url};                                                   break;
            case 'edit'         :var value = {category: 'edit'};                                                            break;
            case 'onoffvalue'   :var value = {category: 'onoffvalue',img:url};                                              break;
            case 'alarmvalue'   :var value = {category: 'alarmvalue',img:url};                                              break;
            case 'button'       :var value = {category: 'button',img:'/uploads/pic/按钮1.jpg',link:'',text:'按钮1'};         break;
            case 'test'         :var value = {category: 'test',img:url,link:'',text:'按钮'};                                 break;
            case 'control' :
                config=eval("["+config+"]")[0];
                var name=obj.innerText;
//                    console.log(name);
//                    console.log(config);
                var value=  {category:'control',img:url,text:name,value:config};                                 break;
            case 'gif'          :
                console.log(config);
                config= JSON.parse(config);

                //active 表示现在激活的是 第0张图片
//                    config={active:0,pic:config};
                config={active:0,pic:config,gif:0};
                var value = {category: 'gif',size: '75 75',img:url,text:config};                                                break;
            case 'gif_value'          :
                console.log(config);
                var active_value=obj.getAttribute("active_value");
                config= JSON.parse(config);

                //active 表示现在激活的是 第0张图片
//                    config={active:0,pic:config};
                config={active:0,active_value:active_value,pic:config,gif:0};
                var value = {category: 'gif_value',size: '75 75',img:url,text:config};                                                break;
            case 'gif_camera'          :
                console.log(config);
                var active_value=obj.getAttribute("active_value");
                config= JSON.parse(config);

                //active 表示现在激活的是 第0张图片
//                    config={active:0,pic:config};
                config={active:0,active_value:active_value,pic:config,gif:0};
                var value = {category: 'gif_camera',size: '75 75',img:url,text:config};                                                break;

            case 'state_value' :
                config=eval("["+config+"]")[0];
//                    console.log(config);
                var value=  {category:'state_value',text:url,value:config};                                 break;
            case 'state_defend' :
                config=eval("["+config+"]")[0];
//                    console.log(config);
                var value=  {category:'state_defend',img:url,text:config};                                 break;
            case 'state_defend_1' :
                config=eval("["+config+"]")[0];
//                    console.log(config);
                var value=  {category:'state_defend_1',img:url,text:config};                                 break;
            case 'state_defend_ctrl':
                config=eval("["+config+"]")[0];
//                    console.log(config);
                var value=  {category:'state_defend_ctrl',img:url,text:config};                                 break;


            case 'group'        :
                var fill=obj.getAttribute("fill");var stroke=obj.getAttribute("stroke");
                var value=  {text: "Lane1", isGroup: true, color: "lightblue",stroke:stroke,fill:fill};                            break;
            case 'figure'       :var value=  {category:"figure",figure:url};                                                break;
            case 'main_edit'    :var value=  {category:"main_edit"};                                                         break;
            case 'only_value'   :var value=  {category:"only_value"};                                                        break;
            case 'rule_value'   :var value = {category: 'rule_value'};  break;
            case 'value_1'   :
                config=eval("["+config+"]")[0];
                console.log(config);
                var value=  {category:"value_1",config:config};                                                           break;

            case 'table'        :var value = {
                category: 'table',
                items:
                    [
                        { name: "示例1", value: 1 },
                        { name: "示例2", value: 2 }
                    ]
            };break;
        }
        $("#image_preview").remove();
        $("#image_preview_parent").append('<div id="image_preview" style="border: solid 1px black;width: 150px; height: 100px"></div>');
        preview(value);
    }
    function preview(value){
        var $ = go.GraphObject.make;
        $(go.Palette,"image_preview",  // must name or refer to the DIV HTML element
            {
                maxSelectionCount: 1,
                nodeTemplateMap: myDiagram2.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel([  // specify the contents of the Palette
                    value
                ])
            });
    }

    $( "#accordion" ).accordion();
    //属性窗口可拖动
    $('#AttributeMenu').draggable();
    $("#point_search").draggable();
    $("#mydiagram_data").click(function () {
        var display=document.getElementById("mySavedModel").style.display
        if(display=='none') document.getElementById("mySavedModel").style.display='';
        else document.getElementById("mySavedModel").style.display='none';
    })

    $("#remove_attribute").click(function () {
        var display=document.getElementById("AttributeMenu").style.display
        if(display=='none') document.getElementById("AttributeMenu").style.display='';
        else document.getElementById("AttributeMenu").style.display='none';
    })

    $("#remove_attribute_new").click(function () {

//            point_search_dialog.dialog("open");

        var display=document.getElementById("point_search").style.display
        if(display=='none') {
            document.getElementById("point_search").style.display='';
            //显示点位展示页面
            $("#_loop").hide();
            $("#point_search_div").hide();
            $("#wid-id-1").show();
            $("#_loop1").show();
        }
        else document.getElementById("point_search").style.display='none';
    })

    //初始化下拉框
    $("#select_point option[value='"+'en'+"']").attr("selected","selected");
    $("#select_point").chosen();
    $(".chosen_select option[value='"+'en'+"']").attr("selected","selected");
    $(".chosen_select").chosen();
    document.getElementById("AttributeMenu").style.display='none';
    //提供 图id与 设施key  ajax得到设施信息
    function ajaxGetAttribute(sub_system_id,key){
        var id=$("#image_group_id").val();
        //ajax获取信息
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/ajax-get-element",
            data: {sub_system_id:sub_system_id, element_id:key},
            success: function (msg) {
                //根据image_group_id   element_id 得到属性框信息

                var data = eval('[' + msg + ']')[0];
//                    console.log(data);
                //只有一个点位  同时更新到原来的属性框中
                //不在更新旧的属性框内的点位值
                if (data.length == 1 && false) {
                    update(data[0]['id']);
                }

                //把绑定的点位信息更新到新的属性框中
                //更新 绑定点位信息
                selectedPointContainer = data;
//                    console.log(selectedPointContainer);
//                regenerateTable(selectedPointContainer, 'tbodyData_one', 'exedel');
                regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');

            }
        });
    }
    $("#update").click(function(){
        updatevalue();
    })
    setInterval(gif,200);


    //动态图测试
    function gif() {
        for (var key in sub_system_data) {
            //tabs_id 对应div的value值为
            var tabs_id = "tabs-myDiagram-" + key;
            var sub_system_id = $("#" + tabs_id).attr("value");
            //加入指定的node
            var diagram = sub_system_data[key];
            var model = diagram.model;
            var arr = model.nodeDataArray;
            model.startTransaction("flash");
            for (var i = 0; i < arr.length; i++) {
                var data = arr[i];
                //gif类型动态图
                if (data.category == 'gif') {
                    var img = data.img;
                    var text = data.text;
                    var active = data.text.active;
                    if(data.text.gif==0) {

                        active++;
                        if (text.pic[active] != undefined) {
                            img = text.pic[active];
                            text.active = active;
                        }
                        else {
                            img = text.pic[0];
                            text.active = 0;
                        }
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                    else {
                        img = text.pic[0];
                        text.active = 0;
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                }
                //gif_value类型动态图
                if (data.category == 'gif_value' || data.category == 'gif_camera') {
                    var img = data.img;
                    var text = data.text;
                    var active = data.text.active;
                    if(data.text.gif==data.text.active_value) {
                        active++;
                        if (text.pic[active] != undefined) {
                            img = text.pic[active];
                            text.active = active;
                        }
                        else {
                            img = text.pic[0];
                            text.active = 0;
                        }
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                    else {
                        img = text.pic[0];
                        text.active = 0;
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                }
                diagram.model.commitTransaction("flash");
            }
        }
    }

    //字体以及颜色更新
    function fontupdate(style){
        var diagram=diagram_now;
        //得到 字体字段
        var font_css=$("#font_css").val();
        var font_color=$("#fontcolor").val();
        var key=$("#element_id").val();
        var scale=$("#scale").val();
        console.log(scale);
        var model = diagram.model;
        var arr = model.nodeDataArray;
        model.startTransaction("flash");
        for (var i = 0; i < arr.length; i++) {
            var data = arr[i];
            if (data.key==key) {
//                    console.log(data);
                if(style=='font')
                    model.setDataProperty(data, "font", font_css);
                if(style=='color')
                    model.setDataProperty(data, "color", font_color);
                if(style=='scale') {
                    model.setDataProperty(data, "scale", scale);
                    console.log(1111);
                }
            }
            diagram.model.commitTransaction("flash");
        }
    }

    //控制按钮值的更新
    function controlupdate(console_type){
        var diagram=diagram_now;
        //得到 字体字段
        var font_css=$("#font_css").val();
        var font_color=$("#fontcolor").val();
        var key=$("#element_id").val();
        console.log(font_color);
        var model = diagram.model;
        var arr = model.nodeDataArray;
        model.startTransaction("flash");
        for (var i = 0; i < arr.length; i++) {
            var data = arr[i];
            if (data.key==key) {
                model.setDataProperty(data, "color", '#333');
                if(data.value[0]==1){
                    switch (console_type) {
                        case undefined:
                            model.setDataProperty(data, "text", '?????');
                            break;
                        case null:
                            model.setDataProperty(data, "text", '?????');
                            break;
                        default :
                            model.setDataProperty(data, "text", console_type);
                            break;
                    }
                }
                //其他显示config值
                else
                {
                    switch (console_type) {
                        case undefined:
                            model.setDataProperty(data, "text", console_type);
                            break;
                        case null:
                            model.setDataProperty(data, "text", "????");
                            break;
                        case '0':case 0:
                        model.setDataProperty(data, "text", data.value[0]);
                        break;
                        case '-1':case -1:
                        model.setDataProperty(data, "text", data.value[-1]);
                        break;
                        case '1':case 1:
                        model.setDataProperty(data, "text", data.value[1]);
                        break;
                        case '255':case 255:
                        model.setDataProperty(data, "text", data.value[255]);
                        break;
                        default :
                            model.setDataProperty(data, "text", "????");
                            break;
                    }
                }
            }
            diagram.model.commitTransaction("flash");
        }


    }

    //节点层次更新
    function layerupdate(){
        var diagram=diagram_now;
        //得到 字体字段
        var layername=$("#layername").val();

        var temp = diagram.model.toJson();
        var data = eval('(' + temp + ')');
        //得到节点对象
        var nodeDataArray = data['nodeDataArray'];
        //得到此节点key
        var key=$("#element_id").val();
        for(var nodeDatas in nodeDataArray){
            var nodekey=nodeDataArray[nodeDatas]['key'];
            //找到value节点实时更新点位的值
            if(nodekey==key){
                nodeDataArray[nodeDatas]['layername']=layername;
            }
        }
        diagram.model.startTransaction("flash");
        diagram.model.nodeDataArray=nodeDataArray;
        diagram.model.commitTransaction("flash");
    }


    init_updatevalue();
//        setInterval(updatevalue,5311);
    setInterval(socket_updatevalue,5000);

    //初始化更新节点值  读取 control值一次
    function init_updatevalue(){
//            console.log(sub_system_info);
        if(sub_system_info.points.length==0 && sub_system_info.events.length==0)
            console.log('无点');
        else {
            var ajax_data=JSON.stringify(sub_system_info);
            //ajax 查询点位信息后更新
            $.ajax({
                type: "POST",
                url: "/subsystem_manage/crud/ajax-update-value-new",
                data: {data: ajax_data},
                success: function (msg) {
//                                        return false;
                    //返回value绑定点位的值
//                    console.log(msg);
                    msg = eval('(' + msg + ')');
                    sub_system_msg = msg;
                    console.log('初始化sub_system_msg数据:');
                    console.log(sub_system_msg);
                    for (var key in sub_system_data) {
                        var tabs_id = "tabs-myDiagram-" + key;
                        var sub_system_id = $("#" + tabs_id).attr("value");
                        //加入指定的node
                        var diagram = sub_system_data[key];
                        var temp = diagram.model.toJson();
                        var data = eval('(' + temp + ')');
                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        var key_value = msg[sub_system_id];

                        var model = diagram.model;
                        var arr = model.nodeDataArray;
                        model.startTransaction("flash");

                        if (key_value != undefined) {
                            //标记此图的value是否改变
                            var flag = false;

//                            var node_value=key_value[nodekey];
                            for (var i = 0; i < arr.length; i++) {
                                var data = arr[i];
                                var key = data.key;
                                if (key_value[key] != undefined) {
                                    switch (data.category) {
                                        case 'value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'onoffvalue':
                                            switch (key_value[key]['value']) {

                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '关');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '开');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'alarmvalue':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '正常');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '报警');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'state':
//                                            console.log(data.text);
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case '0':
                                                case 0:
                                                case '2':
                                                case 2:
                                                case '3':
                                                case 3:
                                                case '4':
                                                case 4:
                                                case '1':
                                                case 1:
                                                case 255:
                                                case '255':
//                                                        model.setDataProperty(data, "img", data.text[2]);
                                                    model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                            }
                                            break;
                                        case 'state_control':
                                            //TODO state_control节点的处理
                                            //节点值为 key_value[key] 数组 代表多个点位
                                            //点位值 0关 255开
                                            // 只有所有点位值为255开时才表示场景开  其他表示场景关
                                            var state=3;
                                            for(var p_key in key_value[key]){
                                                if(key_value[key][p_key]['value']==0)
                                                    state=0;
                                            }
                                            model.setDataProperty(data, "img", data.text[state]);
                                            break;
                                        case 'state_event':
                                            var fire_state=-1;
                                            var equip_state=-1;
                                            //节点值为 key_value[key] 数组 前两个数据为 报警 火灾的值
                                            for(var f_key in key_value[key]){
//                                                    console.log(key_value[key][f_key]['name']);
//                                                    console.log(key_value[key][f_key]['value']);
                                                //循环数组数据 根据名字来判断点位属于[报警 火灾]
                                                if(key_value[key][f_key]['name'].indexOf('火警报警')>0) {
                                                    fire_state = key_value[key][f_key]['value'];
                                                }
                                                if(key_value[key][f_key]['name'].indexOf('设备故障')>0) {
                                                    equip_state = key_value[key][f_key]['value'];
                                                }
                                            }
                                            if(fire_state || equip_state)
                                                model.setDataProperty(data, "img", data.text[1]);//red
                                            else model.setDataProperty(data, "img", data.text[2]);//green
//                                                console.log(key+':state:'+(fire_state || equip_state));
                                            break;
                                        case 'state_value':
                                        case 'control':
                                            model.setDataProperty(data, "color", '#908988');
                                            //手动设置 显示点位值
                                            if (data.value[0] == 1) {

                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                            }
                                            //其他显示config值
                                            else {
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", "????");
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", data.value[0]);
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", data.value[1]);
                                                        break;
                                                    case '255':
                                                    case 255:
                                                        model.setDataProperty(data, "text", data.value[255]);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", "????");
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'state_defend':
                                        case 'state_defend_1':
                                        case 'state_defend_ctrl':
                                            var a = -1;
                                            var b = -1;
                                            var img = data.text[3];
                                            //遍历 绑定的事件
                                            if(key_value[key].length!=0)
                                                for (var abc in key_value[key]) {
                                                    var name = key_value[key][abc]['name'];
                                                    if (
                                                        (data.category=='state_defend' && name.indexOf('布防操作成功') != -1)||
                                                            (data.category=='state_defend_1' && name.indexOf('布防') != -1)||
                                                            (data.category=='state_defend_ctrl' && name.indexOf('布防') != -1)
                                                        )
                                                        a = key_value[key][abc]['value'];
                                                    if (
                                                        (data.category=='state_defend' && name.indexOf('盗警防区报警') != -1)||
                                                            (data.category=='state_defend_1' && name.indexOf('劫盗/周界报警') != -1)||
                                                            (data.category=='state_defend_ctrl' && name.indexOf('报警') != -1)
                                                        )
                                                        b = key_value[key][abc]['value'];
                                                }
                                            if (a == 0){
                                                img = data.text[3];
//                                        document.getElementById("element_id").innerHTML="撤防";
                                            }
                                            else {
//                                        document.getElementById("element_id").innerHTML="布防";
                                                if ( b== 0) img = data.text[1];
                                                if ( b== 1) img = data.text[2];
                                            }
//                                                if(key=-14) {console.log(a+' '+b);}
                                            model.setDataProperty(data, "img", img);
                                            break;
                                        case 'only_value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    var text =  '?????';
                                                    if(key_value[key][0]!=undefined) {
                                                        var text = 0;
                                                        for (var value_num  in key_value[key]) {
                                                            text += key_value[key][value_num]["value"];
                                                        }
                                                    }
                                                    model.setDataProperty(data, "text", text);
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'rule_value':
                                            var input=key_value[key]['value'];
                                            var config=data.config;
                                            var operator=config.operator;
                                            var output=eval(input+operator).toFixed(1)+config.output;
                                            model.setDataProperty(data, "text", output);
                                            break;
                                        case 'value_1':

                                            if(data.config==undefined ) {
                                                data.config=new Array();
                                                data.config={'default':-1};
                                            }
                                            else if(data.config.length==0){
                                                data.config={'default':-1};
                                            }
                                            var value=key_value[key]['value'];
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    var defalut=Number(data.config.default);

                                                    for(var aaa in data.config){

                                                        if(aaa!='defalut' && value>aaa ){
//                                                                        console.log(111111);
                                                            defalut++;
                                                        }
                                                    }
                                                    var floor=value + defalut;
                                                    if(floor==0)floor='B1';
                                                    model.setDataProperty(data, "text", floor);
                                                    break;
                                            }
                                            break;
                                        case 'gif':

                                            var text = data.text;
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    text.gif = 0;
                                                    break;
                                                case 1:
                                                case'1':
                                                    text.gif = 1;
                                                    break;
                                                default :
                                                    text.gif = 0;
                                                    break;
                                            }
                                            model.setDataProperty(data, "text", text);
                                            break;
                                        case 'gif_value':
                                            var text = data.text;
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    text.gif = 0;
                                                    break;
                                                default :
                                                    text.gif = key_value[key]['value'];
                                                    break;
                                            }
                                            model.setDataProperty(data, "text", text);
                                            break;
                                        case 'main_edit':
                                            if (key_value != undefined) {
                                                if (
                                                    data['name'] == '0'
                                                    ) {
                                                    model.setDataProperty(data, "name", key_value[key]['name']);
                                                }
                                                else {
//                                            console.log(nodeDataArray[nodeDatas]['name']);
                                                }
                                                model.setDataProperty(data, "value", key_value[key]['value']);
                                            }
                                            break;
                                        case 'table':
                                            model.setDataProperty(data, "items", key_value[key]);
                                            break;
                                    }
                                }
                                diagram.model.commitTransaction("flash");
                            }

                        }
                    }
                }
            });
        }
    }

    //ajax 更新节点值
    function updatevalue(){
        if(sub_system_info.points.length==0 && sub_system_info.events.length==0)
            console.log('无点');
        else {
            var ajax_data=JSON.stringify(sub_system_info);
            //ajax 查询点位信息后更新
            $.ajax({
                type: "POST",
                url: "/subsystem_manage/crud/ajax-update-value-new",
                data: {data: ajax_data},
                success: function (msg) {
                    //返回value绑定点位的值
                    msg = eval('(' + msg + ')');

                    sub_system_msg = msg;
                    for (var key in sub_system_data) {
                        var tabs_id = "tabs-myDiagram-" + key;
                        var sub_system_id = $("#" + tabs_id).attr("value");
                        //加入指定的node
                        var diagram = sub_system_data[key];
                        var temp = diagram.model.toJson();
                        var data = eval('(' + temp + ')');
                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        var key_value = msg[sub_system_id];
//                            console.log(sub_system_id);
//                            console.log(key_value);
                        var model = diagram.model;
                        var arr = model.nodeDataArray;
                        model.startTransaction("flash");

                        if (key_value != undefined) {
                            //标记此图的value是否改变
                            var flag = false;

//                            var node_value=key_value[nodekey];
                            for (var i = 0; i < arr.length; i++) {
                                var data = arr[i];
                                var key = data.key;
                                if (key_value[key] != undefined) {
                                    switch (data.category) {
                                        case 'value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'onoffvalue':
                                            switch (key_value[key]['value']) {

                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '关');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '开');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'alarmvalue':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '正常');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '报警');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'state':
//                                            console.log(data.text);
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case '0':
                                                case 0:
                                                case '2':
                                                case 2:
                                                case '1':
                                                case 1:
                                                case 255:
                                                case '255':
                                                    model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "img", data.text[1]);
                                                    break;
                                            }
                                            break;
                                        case 'state_event':
                                            var fire_state=-1;
                                            var equip_state=-1;
                                            //节点值为 key_value[key] 数组 前两个数据为 报警 火灾的值
                                            for(var f_key in key_value[key]){
//                                                    console.log(key_value[key][f_key]['name']);
//                                                    console.log(key_value[key][f_key]['value']);
                                                //循环数组数据 根据名字来判断点位属于[报警 火灾]
                                                if(key_value[key][f_key]['name'].indexOf('火警报警')>0) {
                                                    fire_state = key_value[key][f_key]['value'];
                                                }
                                                if(key_value[key][f_key]['name'].indexOf('设备故障')>0) {
                                                    equip_state = key_value[key][f_key]['value'];
                                                }
                                            }
                                            if(fire_state || equip_state)
                                                model.setDataProperty(data, "img", data.text[1]);//red
                                            else model.setDataProperty(data, "img", data.text[2]);//green
//                                                console.log(key+':state:'+(fire_state || equip_state));
                                            break;
                                        case 'state_value':
//                                            case 'control':
                                            //手动设置 显示点位值
                                            if (data.value[0] == 1) {
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                            }
                                            //其他显示config值
                                            else {
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", "????");
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", data.value[0]);
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", data.value[1]);
                                                        break;
                                                    case '255':
                                                    case 255:
                                                        model.setDataProperty(data, "text", data.value[255]);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", "????");
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'only_value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    var text =  '?????';
                                                    if(key_value[key][0]!=undefined) {
                                                        var text = 0;
                                                        for (var value_num  in key_value[key]) {
                                                            text += key_value[key][value_num]["value"];
                                                        }
                                                    }
                                                    model.setDataProperty(data, "text", text);
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'rule_value':
                                            var input=key_value[key]['value'];
                                            var config=data.config;
                                            var operator=config.operator;
                                            var output=eval(input+operator).toFixed(1)+config.output;
                                            model.setDataProperty(data, "text", output);
                                            break;
                                        case 'value_1':

                                            if(data.config==undefined ) {
                                                data.config=new Array();
                                                data.config={'default':-1};
                                            }
                                            else if(data.config.length==0){
                                                data.config={'default':-1};
                                            }
                                            var value=key_value[key]['value'];
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    var defalut=Number(data.config.default);

                                                    for(var aaa in data.config){

                                                        if(aaa!='defalut' && value>aaa ){
//                                                                        console.log(111111);
                                                            defalut++;
                                                        }
                                                    }
                                                    var floor=value + defalut;
                                                    if(floor==0)floor='B1';
                                                    model.setDataProperty(data, "text", floor);
                                                    break;
                                            }
                                            break;
                                        case 'gif':
                                            var text = data.text;
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    text.gif = 0;
                                                    break;
                                                case 1:
                                                    text.gif = 1;
                                                    break;
                                                default :
                                                    text.gif = 0;
                                                    break;
                                            }
                                            model.setDataProperty(data, "text", text);
                                            break;
                                        case 'gif_value':
                                            var text = data.text;
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    text.gif = 0;
                                                    break;
                                                default :
                                                    text.gif = key_value[key]['value'];
                                                    break;
                                            }
                                            model.setDataProperty(data, "text", text);
                                            break;
                                        case 'main_edit':
                                            if (key_value != undefined) {
                                                if (
                                                    data['name'] == '0'
                                                    ) {
                                                    model.setDataProperty(data, "name", key_value[key]['name']);
                                                }
                                                else {
//                                            console.log(nodeDataArray[nodeDatas]['name']);
                                                }
                                                model.setDataProperty(data, "value", key_value[key]['value']);
                                            }
                                            break;
                                        case 'table':
                                            model.setDataProperty(data, "items", key_value[key]);
                                            break;
                                    }
                                }
                                diagram.model.commitTransaction("flash");
                            }

                        }
                    }
                }
            });
        }
    }
//       socket_updatevalue();
    //利用point_data   event_data更新点位信息后更新节点
    function update_subsystem_msg(){
        //sub_system_info['points_key']  point节点 point_data内为point接收到的发布点位
        //sub_system_info['events_key']  event节点 event_data内为event接收到的发布点位
        if(sub_system_msg!=null)
        //循环  节点对应的数据来更新节点值
            for(var sub_system_id in sub_system_msg){
                error_data[sub_system_id]=new Array();
                for(var key in sub_system_msg[sub_system_id]){
                    //绑定的是point点位 则从point_data内获取值 更新 数据数组
                    if(in_array(sub_system_info['points_key'],key,'element')){
                        //如果为 undefined 则不为数组
                        if(sub_system_msg[sub_system_id][key][0]==undefined) {
                            //绑定单个点位得到点位id
                            var value_id = sub_system_msg[sub_system_id][key]['id'];
                            //更新此节点的point值 或 记下无值key
                            if(point_data[value_id]==undefined){
                                error_data[sub_system_id][key]='无值';
//                                       console.log('子系统:'+sub_system_id+'节点:'+key+'无值更新');
                            }
                            else {
                                error_data[sub_system_id][key]= point_data[value_id];
                                sub_system_msg[sub_system_id][key]['value'] = point_data[value_id];
//                                    console.log('子系统:'+sub_system_id+'节点:'+key+'更新:'+point_data[value_id]);
                            }
                        }
                        //否则绑定的点位为多个
                        else{
                            error_data[sub_system_id][key]=new Array();
                            for(var i in sub_system_msg[sub_system_id][key]){
                                var value_id = sub_system_msg[sub_system_id][key][i]['id'];
                                //更新此节点的point值 或 记下无值key
                                if(point_data[value_id]==undefined){
                                    error_data[sub_system_id][key][i]='无值';
//                                       console.log('子系统:'+sub_system_id+'节点:'+key+'无值更新');
                                }
                                else {
                                    error_data[sub_system_id][key][i]=point_data[value_id];
                                    sub_system_msg[sub_system_id][key][i]['value'] = point_data[value_id];
                                }
                            }

                        }

                    }
                    //绑定的是event点位 则从event_data内获取值 更新 数据数组
                    if(in_array(sub_system_info['events_key'],key,'element')){
                        //如果为 undefined 则不为数组
                        if(sub_system_msg[sub_system_id][key][0]==undefined) {
                            //绑定单个点位得到点位id
                            var value_id = sub_system_msg[sub_system_id][key]['id'];
                            //更新此节点的point值 或 记下无值key
                            if(point_data[value_id]==undefined){
                                error_data[sub_system_id][key]='无值';
//                                       console.log('子系统:'+sub_system_id+'节点:'+key+'无值更新');
                            }
                            else {
                                error_data[sub_system_id][key]=event_data[value_id];
                                sub_system_msg[sub_system_id][key]['value'] = event_data[value_id];
                            }
                        }
                        //否则绑定的点位为多个
                        else{
                            error_data[sub_system_id][key]=new Array();
                            for(var i in sub_system_msg[sub_system_id][key]){

                                var value_id = sub_system_msg[sub_system_id][key][i]['id'];
                                //更新此节点的point值 或 记下无值key
                                if(event_data[value_id]==undefined){
                                    error_data[sub_system_id][key][i]='无值';
//                                       console.log('子系统:'+sub_system_id+'节点:'+key+'无值更新');
                                }
                                else {
                                    error_data[sub_system_id][key][i]= point_data[value_id];
                                    sub_system_msg[sub_system_id][key][i]['value'] = event_data[value_id];
                                }
                            }

                        }
                    }
                }
            }
    }

    function socket_updatevalue(){
        if(sub_system_info.points.length==0 && sub_system_info.events.length==0)
            console.log('无点');
        else {

            //更新节点数据数组
            update_subsystem_msg();
            console.log("更新日志");
            console.log(error_data);
            if(sub_system_msg!=null) {
//                    console.log('更新日志:');
//                    console.log(error_data);
                    console.log('更新后的sub_system_msg数据:');
                    console.log(sub_system_msg);
                update_node();
            }
        }
    }

    function update_node(){
        var   msg=sub_system_msg;
        for (var key in sub_system_data) {
            var tabs_id = "tabs-myDiagram-" + key;
            var sub_system_id = $("#" + tabs_id).attr("value");
            //加入指定的node
            var diagram = sub_system_data[key];
            var temp = diagram.model.toJson();
            var data = eval('(' + temp + ')');
            //得到节点对象
            var nodeDataArray = data['nodeDataArray'];
            var key_value = msg[sub_system_id];
//                            console.log(sub_system_id);
//                            console.log(key_value);
            var model = diagram.model;
            var arr = model.nodeDataArray;
            model.startTransaction("flash");

            if (key_value != undefined) {
                //标记此图的value是否改变
                var flag = false;

//                            var node_value=key_value[nodekey];
                for (var i = 0; i < arr.length; i++) {
                    var data = arr[i];
                    var key = data.key;
                    if (key_value[key] != undefined) {
                        switch (data.category) {
                            case 'value':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'onoffvalue':
                                switch (key_value[key]['value']) {

                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case '0':
                                    case 0:
                                        model.setDataProperty(data, "text", '关');
                                        break;
                                    case '1':
                                    case 1:
                                        model.setDataProperty(data, "text", '开');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'alarmvalue':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case '0':
                                    case 0:
                                        model.setDataProperty(data, "text", '正常');
                                        break;
                                    case '1':
                                    case 1:
                                        model.setDataProperty(data, "text", '报警');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'state':
//                                            console.log(data.text);
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "img", data.text[3]);
                                        break;
                                    case null:
                                        model.setDataProperty(data, "img", data.text[3]);
                                        break;
                                    case '0':
                                    case 0:
                                    case '2':
                                    case 2:
                                    case '3':
                                    case 3:
                                    case '4':
                                    case 4:
                                    case '1':
                                    case 1:
                                    case 255:
                                    case '255':
                                        model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                        break;
                                    default :
                                        model.setDataProperty(data, "img", data.text[1]);
                                        break;
                                }
                                break;
                            case 'state_control':
                                //TODO state_control节点的处理
                                //节点值为 key_value[key] 数组 代表多个点位
                                //点位值 0关 255开
                                //图片状态state 0开  3关
                                // 只有所有点位值为255开时才表示场景开  其他表示场景关
                               //默认关着
                                if( key_value[key]['value']==undefined){
                                    var state=0;//默认开着 有一个关就关
                                    for(var p_key in key_value[key]){
                                        if(key_value[key][p_key]['value']==0)
                                            state=3;
                                    }
                                }
                                else {
                                    var state=3;//默认关着 值为255为开着
                                    if(key_value[key]['value']==255)
                                        state=0;

                                }

                                model.setDataProperty(data, "img", data.text[state]);
                                break;
                            case 'state_event':
                                var fire_state=-1;
                                var equip_state=-1;
                                //节点值为 key_value[key] 数组 前两个数据为 报警 火灾的值
                                for(var f_key in key_value[key]){
//                                                    console.log(key_value[key][f_key]['name']);
//                                                    console.log(key_value[key][f_key]['value']);
                                    //循环数组数据 根据名字来判断点位属于[报警 火灾]
                                    if(key_value[key][f_key]['name'].indexOf('火警报警')>0) {
                                        fire_state = key_value[key][f_key]['value'];
                                    }
                                    if(key_value[key][f_key]['name'].indexOf('设备故障')>0) {
                                        equip_state = key_value[key][f_key]['value'];
                                    }
                                }
                                if(fire_state || equip_state)
                                    model.setDataProperty(data, "img", data.text[1]);//red
                                else model.setDataProperty(data, "img", data.text[2]);//green
//                                                console.log(key+':state:'+(fire_state || equip_state));
                                break;
                            case 'state_value':
//                                            case 'control':
                                //手动设置 显示点位值
                                if (data.value[0] == 1) {
                                    switch (key_value[key]['value']) {
                                        case undefined:
                                            model.setDataProperty(data, "text", '?????');
                                            break;
                                        case null:
                                            model.setDataProperty(data, "text", '?????');
                                            break;
                                        default :
                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                            break;
                                    }
                                }
                                //其他显示config值
                                else {
                                    switch (key_value[key]['value']) {
                                        case undefined:
                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                            break;
                                        case null:
                                            model.setDataProperty(data, "text", "????");
                                            break;
                                        case '0':
                                        case 0:
                                            model.setDataProperty(data, "text", data.value[0]);
                                            break;
                                        case '1':
                                        case 1:
                                            model.setDataProperty(data, "text", data.value[1]);
                                            break;
                                        case '255':
                                        case 255:
                                            model.setDataProperty(data, "text", data.value[255]);
                                            break;
                                        default :
                                            model.setDataProperty(data, "text", "????");
                                            break;
                                    }
                                }
                                break;
                            case 'state_defend':
                            case 'state_defend_1':
                            case 'state_defend_ctrl':
                                var a = -1;
                                var b = -1;
                                var img = data.text[3];
                                //遍历 绑定的事件
                                if(key_value[key].length!=0)
                                    for (var abc in key_value[key]) {
                                        var name = key_value[key][abc]['name'];
                                        if (
                                            (data.category=='state_defend' && name.indexOf('布防操作成功') != -1)||
                                                (data.category=='state_defend_1' && name.indexOf('布防') != -1)||
                                                (data.category=='state_defend_ctrl' && name.indexOf('布防') != -1)
                                            )
                                            a = key_value[key][abc]['value'];
                                        if (
                                            (data.category=='state_defend' && name.indexOf('盗警防区报警') != -1)||
                                                (data.category=='state_defend_1' && name.indexOf('劫盗/周界报警') != -1)||
                                                (data.category=='state_defend_ctrl' && name.indexOf('报警') != -1)
                                            )
                                            b = key_value[key][abc]['value'];
                                    }
                                if (a == 0){
                                    img = data.text[3];
//                                        document.getElementById("element_id").innerHTML="撤防";
                                }
                                else {
//                                        document.getElementById("element_id").innerHTML="布防";
                                    if ( b== 0) img = data.text[1];
                                    if ( b== 1) img = data.text[2];
                                }
//                                    if(key=-14) {console.log(a+' '+b);}
                                model.setDataProperty(data, "img", img);
                                break;
                            case 'only_value':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        var text =  '?????';
                                        if(key_value[key][0]!=undefined) {
                                            var text = 0;
                                            for (var value_num  in key_value[key]) {
                                                text += key_value[key][value_num]["value"];
                                            }
                                        }
                                        model.setDataProperty(data, "text", text);
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'rule_value':
                                var input=key_value[key]['value'];
                                var config=data.config;
                                var operator=config.operator;
                                var output=eval(input+operator).toFixed(1)+config.output;
                                model.setDataProperty(data, "text", output);
                                break;
                            case 'value_1':

                                if(data.config==undefined ) {
                                    data.config=new Array();
                                    data.config={'default':-1};
                                }
                                else if(data.config.length==0){
                                    data.config={'default':-1};
                                }
                                var value=key_value[key]['value'];
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        var defalut=Number(data.config.default);

                                        for(var aaa in data.config){

                                            if(aaa!='defalut' && value>aaa ){
//                                                                        console.log(111111);
                                                defalut++;
                                            }
                                        }
                                        var floor=value + defalut;
                                        if(floor==0)floor='B1';
                                        model.setDataProperty(data, "text", floor);
                                        break;
                                }
                                break;
                            case 'gif':
                                var text = data.text;
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        text.gif = 0;
                                        break;
                                    case 1:
                                        text.gif = 1;
                                        break;
                                    default :
                                        text.gif = 0;
                                        break;
                                }
                                model.setDataProperty(data, "text", text);
                                break;
                            case 'gif_value':
                                var text = data.text;
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        text.gif = 0;
                                        break;
                                    default :
                                        text.gif = key_value[key]['value'];
                                        break;
                                }
                                model.setDataProperty(data, "text", text);
                                break;
                            case 'main_edit':
                                if (key_value != undefined) {
                                    if (
                                        data['name'] == '0'
                                        ) {
                                        model.setDataProperty(data, "name", key_value[key]['name']);
                                    }
                                    else {
//                                            console.log(nodeDataArray[nodeDatas]['name']);
                                    }
                                    model.setDataProperty(data, "value", key_value[key]['value']);
                                }
                                break;
                            case 'table':
                                model.setDataProperty(data, "items", key_value[key]);
                                break;
                        }
                    }
                    diagram.model.commitTransaction("flash");
                }

            }
        }
    }

    function update_console_board(type,html){
        $("#control_drop").empty();
        $("#control_drop").append(html);

        //当是下拉框时初始化下拉框并给宽度300px
        if(type==0) {
            $("#console_type").chosen();
            $("#console_type_chosen").width(300);
        }
    }
    //更新属性框内的点位值
    function update(value){
        //chosen 先设置值在执行更新函数
        $("#select_point").val(value);
        $("#select_point").trigger("chosen:updated");
    }

    //点击保存绑定信息
    $('#_submit').click(function(){
        var id=$("#image_group_id").val();
        var sub_system_id=$("#sub_system_id").val();
        var element_id=$("#element_id").val();
        var point_ids=$("#select_point").val();
        //ajax保存信息
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/ajax-save-element",
            data: {sub_system_id:sub_system_id, element_id:element_id,binding_id:point_ids},
            success: function (msg) {
                //根据image_group_id   element_id 得到属性框信息
                msg=eval('['+msg+']');
//                        console.log(msg);
            }
        });

    })

    //跟着窗口滚动
    function scroll(element){
        $(window).scroll(function(){
            var oParent = document.getElementById(element);
            var x =oParent.offsetLeft;
            var y = oParent.offsetTop;
            var yy = $(this).scrollTop();//获得滚动条top值
            if ($(this).scrollTop() < 30) {
                $("#"+element).css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位

            }else{
                $("#"+element).css({"position":"absolute",top:yy+"px",left:x+"px"});
            }
        });
    }
    function update_points(data){
        var html='';
        if(data.length!=0){
            $("#tbodyData_many_points").empty();
            for(var i in data){
                html += '<tr><td>'+data[i]['name']+'</td><td>'+data[i]['value']+'</td></tr>';
            }

            $("#tbodyData_many_points").append(html);
//
//            $('#datatable_tabletools3').dataTable({"bDestroy":true,"pageLength":5});
//                console.log(data);
        }
        many_points.dialog("open");
    }
    //s为所选节点 id为此节点所在的 myDiagram所在数组sub_system_data中的键值
    function showMessage(s,id) {

        var tabs_id = "tabs-myDiagram-" + id;
//            console.log(s.key +"   "+tabs_id);
        //节点信息
        document.getElementById("diagramEventsMsg").textContent = s;
        //所属的子系统id
        var sub_system_id = $("#" + tabs_id).attr("value");

        document.getElementById("sub_system_id").value = sub_system_id;
        document.getElementById("image_group_id").value = sub_system_id;
        document.getElementById("element_id").value = s.key;
        document.getElementById("subsystem").innerText = sub_system_id;
        document.getElementById("node_key").innerText = s.key;
        document.getElementById("node_category").innerText = s.category;
        if(sub_system_msg!=null) {
            if (sub_system_msg[sub_system_id][s.key] != undefined) {
                document.getElementById("point_id").innerText = sub_system_msg[sub_system_id][s.key]['id'];
                document.getElementById("point_value").innerText = sub_system_msg[sub_system_id][s.key]['value'];
                document.getElementById("update_time").innerText = sub_system_msg[sub_system_id][s.key]['time'];
            }
            else {
                document.getElementById("point_id").innerText = '无';
                document.getElementById("point_value").innerText = '无';
                document.getElementById("update_time").innerText = '无';
            }
        }

        ajaxGetAttribute(sub_system_id, s.key);
        update('zh');
        //document.getElementById("AttributeMenu").style.display='';

//            //可拖动
//            drag();
    }

    scroll("AttributeMenu");
    scroll("point_search");

    var location_id=<?=$location_id?>;
    //所有的分类
    var all_sub_system=<?=json_encode($all_sub_system)?>;
    var tab_num=1;
    //console.log(all_sub_system);
    init_blank('myDiagramX');

    $('#tabs').tabs();
    $('#tabs2').tabs();
    //存储是否有底图
    var image_in=new Array();
    //存储底图id
    var image_url_in=new Array();
    //存储图的名称
    var data_name=new Array();


    //数组remove指定键值元素 数据
    function remove(a,id){
        var result=new Array();
        for(var key in a){
            if(key!=id)
                result[key]=a[key];
        }
        return result;
    }
    // Dynamic tabs
    var tabTitle = $("#tab_title");
    var tabContent = $("#tab_content");
    var tabTemplate =   "<li style='position:relative;'> "+
        "<span class='air air-top-left delete-tab' style='top:7px; left:7px;'>"+
        "<button class='btn btn-xs font-xs btn-default hover-transparent'>"+
        "<i class='fa fa-times'>"+
        "</i>" +
        "</button>" +
        "</span>" +
        "</span>" +
        "<a onclick='resize();' href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}" +
        "</a>" +
        "</li>";

    var id = "sub_system";
    var tabs = $("#tabs2").tabs();

    //弹出框内三个值
    var sub_system_name=$("#sub_system_name");
    var image_base=$("#image_base");
    var sub_system_category=$("#sub_system_category");
    var selected_image=$("#selected_image");



    // modal dialog init: custom buttons and a "close" callback reseting the form inside
    var dialog = $("#addtab").dialog({
        autoOpen : false,
        width : 600,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {
            html : "<i class='fa fa-plus'></i>&nbsp; 添加",
            "class" : "btn btn-danger",
            click : function() {
                //验证弹出框内数据完整性
//                    var sub_system_name=$("#sub_system_name").val();
//                    var image_base=$("#image_base").find("option:checked").text();
//                    var sub_system_category=$("#sub_system_category").find("option:checked").text();
//                      alert(sub_system_category.find("option:checked").val())
                if(sub_system_name.val()==''){
                    alert('请输入子系统名称');
                }
                else {
                    var name = sub_system_category.find("option:checked").text()+'_'+sub_system_name.val();
                    var id = sub_system_category.find("option:checked").val();
                    var with_image=image_base.find("option:checked").val();
                    var with_url_id=selected_image.find("option:checked").val();
                    var image_url=all_image[with_url_id]['url'];
                    //如果分类还有则 继续所属分类的标签
                    if(name!='_'+sub_system_name.val()) {
                        image_in[tab_num]=with_image;
                        image_url_in[tab_num]=with_url_id;
                        //添加标签页
                        addTab(name,tab_num,0);
                        //初始化  myDiagram  有底图和无底图
                        if(with_image==0) init2('myDiagram'+tab_num,tab_num);
                        else init1('myDiagram'+tab_num,tab_num,image_url);

                        tab_num++;
                        //去掉添加了的子系统类型    [又不需要去掉了]
//                               $("#sub_system_category option[value=" + id + "]").remove();
//                               //改变选择值为第一个
//                               $("#sub_system_category option:first").prop("selected", 'selected');
//                               var text = $("#sub_system_category option:first").text();
//                               $("#select2-chosen-2").html(text);
                    }
                    //如果分类没有了 则提示
                    else alert('分类用完了');
                }
                $(this).dialog("close");
            }
        }]
    });

    $("#add_link").click(function(){
        linkdialog.dialog("open");
    })
    // modal dialog init: custom buttons and a "close" callback reseting the form inside
    var linkdialog = $("#link_update").dialog({
        autoOpen : false,
        width : 600,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        },
            {

                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-danger",
                click : function() {
                    link=$("#node_link").val();
                    link_name=$("#node_name").val();

                    var diagram=button;
                    var temp = diagram.model.toJson();

                    var data = eval('(' + temp + ')');

                    //得到节点对象
                    var nodeDataArray = data['nodeDataArray'];
                    for(var nodeDatas in nodeDataArray){
                        var nodekey=nodeDataArray[nodeDatas]['key'];
                        if(nodekey==button_key){
                            nodeDataArray[nodeDatas]['link']=link;
                            nodeDataArray[nodeDatas]['text']=link_name;
                        }
                    }
                    diagram.model = go.Model.fromJson(data);
                    $(this).dialog("close");
                    $("#node_link").val('');
                    $("#node_name").val('');
                }
            }]
    });


    $("#font_color").click(function(){
        font_color_dialog.dialog('open');
    })
    var font_color_dialog = $("#update_font_color").dialog({
        autoOpen : false,
        width : 400,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {

            html : "<i class='fa fa-plus'></i>&nbsp; 确定",
            "class" : "btn btn-danger",
            click : function() {
                //获取字体信息 组成font样式
                var font_family=$("#font_family").val();
                var font_variant=$("#font_variant").val();
                var font_weight=$("#font_weight").val();
                var font_size=$("#font_size").val();
                var font_color=$("#fontcolor").val();
                console.log(font_color);
                var font=font_variant+' '+font_weight+' '+font_size+' '+font_family;
                $("#font_css").val(font);
                $(this).dialog("close");
            }
        }
        ]
    });

    $("#layer_set").click(function(){
        layer_name.dialog("open");
    })
    //控制台 操作页面
    var layer_name = $("#layer_name").dialog({
        autoOpen : false,
        width : 400,
        height:270,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {
            html : "<i class='fa fa-plus'></i>&nbsp; 确定",
            "class" : "btn btn-danger",
            click : function() {
                layerupdate();
                $(this).dialog("close");
            }
        }
        ]
    });


    $("#console").click(function(){
        console_board.dialog("open");
    })
    //多点位展示
    //控制台 操作页面
    var many_points = $("#many_points").dialog({
        autoOpen : false,
        width : 'auto',
        resizable : false,
        modal : true
    });
    //控制台 操作页面
    var console_board = $("#console_board").dialog({
        autoOpen : false,
        width : 400,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {

            html : "<i class='fa fa-plus'></i>&nbsp; 确定",
            "class" : "btn btn-danger",
            click : function() {
                //获取所操作子系统sub_system_id 以及节点的element_id
                //以及  命令值
                var sub_system_id=$("#sub_system_id").val();
                var element_id=$("#element_id").val();
//                    var console_value=$("#console_value").val();
                var console_type=$("#console_type").val();
                console.log(console_type);
                //先更新节点值
                //先更新节点值
                if(node_now.categroy=='control')
                    controlupdate(console_type);

                //手动设置 显示点位值
                //ajax 在数据表内写入命令
                $.ajax({
                    type: "POST",
                    url: "/subsystem_manage/crud/ajax-control",
                    data: {sub_system_id: sub_system_id,element_id:element_id,console_type:console_type},
                    success: function (msg) {
                        switch (msg){
                            case '-1':
                                alert('未绑定点位');break;
                            case '0':
                                alert('命令发送失败');break;
                        }
                    }
                });
                $(this).dialog("close");
            }
        }
        ]
    });

    //属性设置 操作页面
    var attribute_set_dialog = $("#attribute_set_dialog").dialog({
        autoOpen : false,
        width : 400,
        height:270,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {
            html : "<i class='fa fa-plus'></i>&nbsp; 确定",
            "class" : "btn btn-danger",
            click : function() {
                var config='error';
                switch (node_now.category){

                    case 'rule_value':
                        //得到rule_value的多个属性值
                        var operator=$("#operator").val();
                        var output=$("#output").val();
                        config={
                            operator:operator,
                            output:output
                        };
                        //更新rule_value节点的config属性
                        attribute_update("config",config);

                        break;
                }

                $(this).dialog("close");
            }
        }
        ]
    });


//        var point_search_dialog = $("#point_search").dialog({
//            autoOpen : false,
//            width : 800,
//            resizable : false,
//            modal : true,
//
//        });

//        point_search_dialog.dialog("open");
    // actual addTab function: adds new tab using the input from the form above
    function addTab(name,id,sub_system_id) {
//            console.log(id);
        data_name[id]=name;
        var tabs_id = "tabs-myDiagram-" + id;
        var label = name || tabContent;
        var li = $(tabTemplate.replace(/#\{href\}/g, "#" + tabs_id).replace(/#\{label\}/g, label));
        var tabContentHtml='<div id="myDiagram'+id+'"style="border: solid 1px black; width:100%; height:650px;background:rgb(29,27,29)" "></div>';
        tabs.find(".ui-tabs-nav").append(li);
        tabs.append("<div id='" + tabs_id + "' value='"+sub_system_id+"'> " + tabContentHtml + " </div>");
        tabs.tabs("refresh");
        // clear fields
        $("#sub_system_name").val("");

    }


    $("#subsystem_config").click(function(){
        //所属的子系统id
        var sub_system_id = document.getElementById("subsystem").innerText;
        if(sub_system_id=='值')
            alert("请选中子系统");
        else {
            //跳转至子系统编辑界面
            var url="/subsystem/crud/config?related_id="+sub_system_id+"&type=subsystem";
            window.location=url;
        }

    });
    // 增加子系统 并删除已经增加的子系统
    $("#add_subsystem").button().click(function() {
        dialog.dialog("open");
    });
    //删除标签
    // close icon: removing the tab on click
    $("#tabs2").on("click", 'span.delete-tab', function() {
        if (!confirm("您确定要删除吗？"))
        { return false;}
        else
        {
            //移除标签li   以及div
            var panelId = $(this).closest("li").remove().attr("aria-controls");


            //得到要删除的标签id  并截取对应的类型id位
            var tab_id=$("#" + panelId).attr("id");
            console.log(tab_id)
            var id=$("#" + panelId).attr("value");
            var category_id=tab_id.substring(15,tab_id.length);
            $("#" + panelId).remove();

            //ajax  delete删除对应的子系统
            $.ajax({
                type: "POST",
                url: "/subsystem_manage/crud/ajax-delete",
                data: {id: id},
                //
                success: function (msg) {
                }
            });
            //在数组内移除对应的子系统信息
            data_name=remove(data_name,category_id);
            image_in=remove(image_in,category_id);
            sub_system_data=remove(sub_system_data,category_id);

            tabs.tabs("refresh");
            //删除标签 并把分类再加入分类框内
            $("#sub_system_category").append("<option value='"+category_id+"'>"+all_sub_system[category_id]+"</option>");
        }
    });

    $("#image_base").change(function(){
        var selected_value=$("#image_base").val();
        alert(selected_value);
        if(selected_value==0) $("#selected_image_div").css("display","none");
        if(selected_value==1) $("#selected_image_div").css("display","block");
    })
    $("#save").click(function(){
//            console.log(data_name);
//            console.log(image_in);
//            console.log(sub_system_data);
        document.getElementById("mySavedModel").value = null;
        //var data1=myDiagram1.model.toJson();
        var data=new Array();

        for( var key in sub_system_data){
            console.log('key= '+key);
            var id_tmep=document.getElementById("tabs-myDiagram-"+key).getAttribute('value');
            //key=key.toString();
//                console.log("tabs-myDiagram-"+key);
//                console.log(id_tmep);
            var data_temp={
                'name':data_name[key],
                'category_id':key,
                'id':id_tmep,
                'data':{'image_base':image_in[key],'image_id':image_url_in[key],'data':sub_system_data[key].model.toJson()}
            };
            document.getElementById("mySavedModel").value = document.getElementById("mySavedModel").value +' '+data_name[key]+' '+key+sub_system_data[key].model.toJson();
            data.push(data_temp)

        }
        //console.log(data);
        var data=JSON.stringify(data);
        //console.log(data);
//            ajax save
//            sub_system Info  [name data category_id location_id]
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/ajax-save",
            data: {data: data,location_id:location_id},
            //暂时未加判定是否成功插入
            success: function (msg) {
                msg=eval('['+msg+']')[0];
                console.log(1111);
                console.log(msg);
                if(msg['result']==true) {
                    console.log('保存成功');
                    sub_system_info=msg['sub_system_data'];
                }
                else
                    alert('保存失败,请重试');
            }
        });



        //更新一次
        updatevalue();

    });
    function doMouseOver(e) {
//            console.log(e);

        if (e === undefined) e = myDiagram.lastInput;
        var doc = e.documentPoint;

//             find all Nodes that are within 100 units
        var list = myDiagram.findObjectsNear(doc, 1, null, function(x) { return x instanceof go.Node; });
        // now find the one that is closest to e.documentPoint
        var closest = null;
        var closestDist = 999999999;

        list.each(function(node) {
            var dist = doc.distanceSquaredPoint(node.getDocumentPoint(go.Spot.Center));
            if (dist < closestDist) {
                closestDist = dist;
                closest = node;
            }
        });


        highlightNode(e, closest);
    }

    // Make sure the infoBox is momentarily hidden if the user tries to mouse over it
    var infoBoxH = document.getElementById("infoBoxHolder");
    infoBoxH.addEventListener("mousemove", function() {
        var box = document.getElementById("infoBoxHolder");
        box.style.left = parseInt(box.style.left) + "px";
        box.style.top = parseInt(box.style.top)+10 + "px";

    }, false);


    // Called with a Node (or null) that the mouse is over or near
    function highlightNode(e, node) {
        //到节点 并且 节点不为table
        if (node !== null && node.data['category']!='table' && node.data['category']!='figure') {
//                var shape = node.findObject("SHAPE");
//                shape.stroke = "white";
//                if (lastStroked !== null && lastStroked !== shape) lastStroked.stroke = null;
//                lastStroked = shape;


            //显示   InfoBox提示框 编辑页面先屏蔽
//                updateInfoBox(e.viewPoint, node.data);

        } else {
//                if (lastStroked !== null) lastStroked.stroke = null;
//                lastStroked = null;
            document.getElementById("infoBoxHolder").innerHTML = "";
        }
    }

    // This function is called to update the tooltip information
    // depending on the bound data of the Node that is closest to the pointer.
    function updateInfoBox(mousePt, data) {
        var x =
            "<div id='infoBox'>" +
                "<div>Info</div>" +
                "<div class='infoTitle'>属性名称</div>" +
                "<div class='infoValues'>值</div>"
        for(var key in data){
            var value='无';
            x=x+  "<div class='infoTitle'>"+key+"</div>";
            if(data[key]!='') {
                value=data[key];
            }
            x=x+   "<div class='infoValues'>" +value + "</div> ";
        }
        x=x+"</div>"
        var box = document.getElementById("infoBoxHolder");

        box.innerHTML = x;
        box.style.left = mousePt.x+120 + "px";
        box.style.top = mousePt.y+20 + "px";
    }

    function relayoutDiagram() {
        myDiagram2.selection.each(function(n) { n.invalidateLayout(); });
        myDiagram2.layoutDiagram();
    }

    function init_blank(element) {
        if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates

        var cellSize = new go.Size(1, 1);


        myDiagram2 =
            $(go.Diagram, element,  // must name or refer to the DIV HTML element
                {
                    grid: $(go.Panel, "Grid",
                        {
                            gridCellSize: cellSize
                        }
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                    )
                }
                ,
                {


                    // allow nodes to be dragged to the diagram's background,
                    // to be removed from any group that they were in
                    mouseDrop: function(e) {
                        var ok = e.diagram.commandHandler.addTopLevelParts(e.diagram.selection, true);
                        if (!ok) e.diagram.currentTool.doCancel();
                    },
                    // a clipboard copied node is pasted into the original node's group (i.e. lane).
                    "commandHandler.copiesGroupKey": true,
                    // automatically re-layout the swim lanes after dragging the selection
                    "SelectionMoved": relayoutDiagram,  // this DiagramEvent listener is
                    "SelectionCopied": relayoutDiagram, // defined above
                    "animationManager.isEnabled": false,
                    // enable undo & redo
                    "undoManager.isEnabled": true,


//                        mouseOver: doMouseOver,
                    "draggingTool.isGridSnapEnabled": true,
                    "draggingTool.gridSnapCellSpot": go.Spot.Center,
                    "resizingTool.isGridSnapEnabled": true,
                    "undoManager.isEnabled": true,
                    allowDrop: true,// must be true to accept drops from the Palette
                    initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.Center,
//                        initialViewportSpot: go.Spot.TopCenter,
                    //isReadOnly: true,  // allow selection but not moving or copying or deleting
                    "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                }
            );
        //diagram分为四层  foreground   second  third  fourth
        var forelayer = myDiagram2.findLayer("Foreground");
        var secondlayer=$(go.Layer, { name: "second" });
        var thirdlayer=$(go.Layer, { name: "third" });
        var fourthlayer=$(go.Layer, { name: "fourth" });
        myDiagram2.addLayerBefore(secondlayer, forelayer);
        myDiagram2.addLayerBefore(thirdlayer, secondlayer);
        myDiagram2.addLayerBefore(fourthlayer, thirdlayer);
        //点击事件
        myDiagram2.addDiagramListener("ObjectSingleClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    if(part.data.category=='button') {
                        button = myDiagram2;
                        button_key = part.data.key;
                        document.getElementById("node_link").value = part.data.link;
                        document.getElementById("node_name").value = part.data.text;
//                            $("#node_link").val(part.data.link);
//                            $("#node_name").val(part.data.text);
                        console.log('button_key');
                        console.log(part);
                        console.log(button_key);
                        linkdialog.dialog("open");
                    }
                }

                //showMessage(part.data,num);
            });
        //右键事件
        myDiagram2.addDiagramListener("ObjectContextClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    if (part.data.category == 'table'||
                        part.data.category == 'edit' ||
                        part.data.category == 'value'||
                        part.data.category == 'only_value'
                        )
                    {
                        diagram_now=myDiagram2;
                        showMessage(part.data, num);
                    }
                }

            });


        var node_template=init_node_template_edit();
        init_context_menu("contextMenu",myDiagram2);
        //
        myDiagram2.nodeTemplateMap.add("main",node_template.main);
        myDiagram2.nodeTemplateMap.add("edit",node_template.edit);
        myDiagram2.nodeTemplateMap.add("value",node_template.value);
        myDiagram2.nodeTemplateMap.add("onoffvalue",node_template.onoff);
        myDiagram2.nodeTemplateMap.add("alarmvalue",node_template.alarm);
        myDiagram2.nodeTemplateMap.add("button", node_template.button);
        myDiagram2.nodeTemplateMap.add("test", node_template.test);
        myDiagram2.nodeTemplateMap.add("gif", node_template.gif);
        myDiagram2.nodeTemplateMap.add("gif_value", node_template.gif_value);
        myDiagram2.nodeTemplateMap.add("gif_camera", node_template.gif_camera);
        myDiagram2.nodeTemplateMap.add("table", node_template.table);
        myDiagram2.nodeTemplateMap.add("figure", node_template.figure);
        myDiagram2.nodeTemplateMap.add("main_edit", node_template.main_edit);
        myDiagram2.nodeTemplateMap.add("only_value", node_template.only_value);
        myDiagram2.nodeTemplateMap.add("state", node_template.state);
        myDiagram2.nodeTemplateMap.add("state_control", node_template.state_control);
        myDiagram2.nodeTemplateMap.add("state_event", node_template.state_event);
        myDiagram2.nodeTemplateMap.add("state_value", node_template.state_value);
        myDiagram2.nodeTemplateMap.add("state_defend", node_template.state_defend);
        myDiagram2.nodeTemplateMap.add("state_defend_1", node_template.state_defend_1);
        myDiagram2.nodeTemplateMap.add("state_defend_ctrl", node_template.state_defend_ctrl);
        myDiagram2.nodeTemplateMap.add("control", node_template.control);
        myDiagram2.nodeTemplateMap.add("many", node_template.many);
        myDiagram2.nodeTemplateMap.add("value_1", node_template.value_1);
        myDiagram2.nodeTemplateMap.add("rule_value", node_template.rule_value);
        myDiagram2.groupTemplate=node_template.group;



        myDiagram2.model = new go.GraphLinksModel(
            [ // node data
                { key: "Lane1", text: "Lane1", isGroup: true, color: "lightblue" },
                { key: "Lane2", text: "Lane1", isGroup: true, color: "lightblue", group: "Lane1"},
                { key: "11", category:"edit",text: "text for oneA", group: "Lane1" },
            ]);  // no link data
    }


    //初始化 有底图的
    function init1(element,num,image_url) {
        //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        var cellSize = new go.Size(5, 5);
        myDiagram =
            $(go.Diagram, element,
                {
                    grid: $(go.Panel, "Grid",
                        {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                    )
                }
                ,
                {
//                        mouseOver: doMouseOver,
                    allowVerticalScroll:true,
                    allowDrop: true,// must be true to accept drops from the Palette
                    "draggingTool.isGridSnapEnabled": true,
                    "draggingTool.gridSnapCellSpot": go.Spot.Center,
                    contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                    //isReadOnly: true,  // allow selection but not moving or copying or deleting
                    "toolManager.hoverDelay":100  // how quickly tooltips are shown
                }
            );


        sub_system_data[num] = myDiagram;


        //单击事件
        sub_system_data[num].addDiagramListener("ObjectSingleClicked",
            function (e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)) {
                    if (part.data.category == 'button') {
                        button = sub_system_data[num];
                        button_key = part.data.key;
                        linkdialog.dialog("open");
                    }
                    else {
                        showMessage(part.data, num);
                    }
                }
            });

        //双击事件
        sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)) {
                    diagram_now=sub_system_data[num];
                    node_now=part.data;
                    //更新属性框信息
                    showMessage(part.data, num);
                    switch (part.data.category) {
                        //control类型 弹出控制台
                        case 'control':
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            var value = part.data.value;
                            console.log(value[0] != 1);
                            if (value[0] != 1) {
                                //根据此节点data.value更新控制台的下拉列表信息
                                var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                for (var key in value) {
                                    option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                }
                                option_html += '</select>';
                                var type = 0;
                            }
                            else {

                                var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                var type = 1;
                            }

                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;
                        //A16照明控制
                        case 'state_control':
                            var value=part.data.text.state;
                            //根据此节点data.value更新控制台的下拉列表信息
                            var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                            for (var key in value) {
                                option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                            }
                            option_html += '</select>';
                            var type = 0;
                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;
                        case 'table':
                            console.log(part.data.value);
                            break;

                    }

                };
            });


        //右键事件
        sub_system_data[num].addDiagramListener("ObjectContextClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    if (
                        part.data.category == 'table'||
                            part.data.category == 'edit' ||
                            part.data.category == 'value'||
                            part.data.category == 'main' ||
                            part.data.category == 'only_value'
                        )
                    {
                        diagram_now=sub_system_data[num];
                        showMessage(part.data, num);
                    }
                }

            });

        // the background image, a floor plan
        sub_system_data[num].add(
            $(go.Part,  // this Part is not bound to any model data
                {
//                        layerName: "Background", position: new go.Point(0, 0),
                    selectable: false,resizable: true ,pickable: false
                },
                $(go.Picture, "/" + image_url)
            ));

        var node_template = init_node_template_edit();
        init_context_menu("contextMenu",sub_system_data[num]);
        //���nodeģ��
        sub_system_data[num].nodeTemplateMap.add("main", node_template.main);
        sub_system_data[num].nodeTemplateMap.add("edit", node_template.edit);
        sub_system_data[num].nodeTemplateMap.add("value", node_template.value);
        sub_system_data[num].nodeTemplateMap.add("onoffvalue", node_template.onoff);
        sub_system_data[num].nodeTemplateMap.add("alarmvalue", node_template.alarm);
        sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
        sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
        sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
        sub_system_data[num].nodeTemplateMap.add("gif_value", node_template.gif_value);
        sub_system_data[num].nodeTemplateMap.add("gif_camera", node_template.gif_camera);
        sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
        sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
        sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
        sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
        sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
        sub_system_data[num].nodeTemplateMap.add("state_control", node_template.state_control);
        sub_system_data[num].nodeTemplateMap.add("state_event", node_template.state_event);
        sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
        sub_system_data[num].nodeTemplateMap.add("state_defend", node_template.state_defend);
        sub_system_data[num].nodeTemplateMap.add("state_defend_1", node_template.state_defend_1);
        sub_system_data[num].nodeTemplateMap.add("state_defend_ctrl", node_template.state_defend_ctrl);
        sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
        sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
        sub_system_data[num].nodeTemplateMap.add("rule_value", node_template.rule_value);
        sub_system_data[num].groupTemplate=node_template.group;
    }
    //初始化 无底图的
    function init2(element,num) {
        //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        var cellSize = new go.Size(5, 5);
        myDiagram =
            $(go.Diagram, element,  // must name or refer to the DIV HTML element
                {
                    grid: $(go.Panel, "Grid",
                        {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                    )
                }
                ,
                {
                    "resizingTool.isGridSnapEnabled": true,
//                        mouseOver: doMouseOver,
                    allowVerticalScroll:true,
                    allowDrop: true,// must be true to accept drops from the Palette
                    "draggingTool.isGridSnapEnabled": true,
                    "draggingTool.gridSnapCellSpot": go.Spot.Center,
                    contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                    //isReadOnly: true,  // allow selection but not moving or copying or deleting
                    "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                }
            );


        //把myDiagram 加入sub_system_data内
        sub_system_data[num]=myDiagram;



        //单击事件
        sub_system_data[num].addDiagramListener("ObjectSingleClicked",
            function(e) {
                var part = e.subject.part;
                diagram_now=sub_system_data[num];
                if (!(part instanceof go.Link)) {
                    if(part.data.category=='button') {
                        button = sub_system_data[num];
                        button_key = part.data.key;
                        linkdialog.dialog("open");
                    }
                    else {
                        showMessage(part.data, num);
                    }
                };
            });
        //双击事件
        sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)) {
                    diagram_now=sub_system_data[num];
                    node_now=part.data;
                    //更新属性框信息
                    showMessage(part.data, num);
                    switch (part.data.category) {
                        //control类型 弹出控制台
                        case 'control':
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            var value = part.data.value;
                            console.log(value[0] != 1);
                            if (value[0] != 1) {
                                //根据此节点data.value更新控制台的下拉列表信息
                                var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                for (var key in value) {
                                    option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                }
                                option_html += '</select>';
                                var type = 0;
                            }
                            else {

                                var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                var type = 1;
                            }

                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;
                        //A16照明控制
                        case 'state_control':
                            var value=part.data.text.state;
                            //根据此节点data.value更新控制台的下拉列表信息
                            var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                            for (var key in value) {
                                option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                            }
                            option_html += '</select>';
                            var type = 0;
                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;
                        case 'table':
                            console.log(part.data);
                            break;
                        case 'many':case 'gif':
                        //得到点击的子系统id  key
                        var sub_system_id=document.getElementById("sub_system_id").value;
                        var element_id=document.getElementById("element_id").value;

                        var value=sub_system_msg[sub_system_id][element_id];
                        update_points(value);
                        break;
                    }

                };
            });

        //右键事件
        sub_system_data[num].addDiagramListener("ObjectContextClicked",
            function(e) {
//                    console.log('右键');
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    //记录当前diagram和node 并展示node信息
                    node_now=part.data;
                    diagram_now=sub_system_data[num];
                    showMessage(part.data, num);
                    //隐藏所有菜单
                    contextmenu('hide');

                    if (part.data.category == 'table'||
                        part.data.category == 'edit' ||
                        part.data.category == 'value'||
                        part.data.category == 'main' ||
                        part.data.category == 'many' ||
                        part.data.category == 'only_value'

                        )
                    {
                        //显示右键菜单
                        showMyMenu(["layer_set","attribute_set","console","font-style","font-color"]);
                    }
                    if(part.data.category == 'rule_value'){
                        //只显示除了撤防布防的按钮
                        showMyMenu(["attribute_set","console","font-style","font-color"]);
                    }

                    if(
                        part.data.category == 'state_defend'||
                            part.data.category == 'state_defend_1'||
                            part.data.category == 'state_defend_ctrl'
                        ){
                        //只显示除了撤防布防的按钮
                        showMyMenu(["event_defend"]);
                    }
                }

            });

        var node_template=init_node_template_edit();
        init_context_menu("contextMenu",sub_system_data[num]);
        //
        sub_system_data[num].nodeTemplateMap.add("main",node_template.main);
        sub_system_data[num].nodeTemplateMap.add("edit",node_template.edit);
        sub_system_data[num].nodeTemplateMap.add("value",node_template.value);
        sub_system_data[num].nodeTemplateMap.add("onoffvalue",node_template.onoff);
        sub_system_data[num].nodeTemplateMap.add("alarmvalue",node_template.alarm);
        sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
        sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
        sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
        sub_system_data[num].nodeTemplateMap.add("gif_value", node_template.gif_value);
        sub_system_data[num].nodeTemplateMap.add("gif_camera", node_template.gif_camera);
        sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
        sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
        sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
        sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
        sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
        sub_system_data[num].nodeTemplateMap.add("state_control", node_template.state_control);
        sub_system_data[num].nodeTemplateMap.add("state_event", node_template.state_event);
        sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
        sub_system_data[num].nodeTemplateMap.add("state_defend", node_template.state_defend);
        sub_system_data[num].nodeTemplateMap.add("state_defend_1", node_template.state_defend_1);
        sub_system_data[num].nodeTemplateMap.add("state_defend_ctrl", node_template.state_defend_ctrl);
        sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
        sub_system_data[num].nodeTemplateMap.add("many", node_template.many);
        sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
        sub_system_data[num].nodeTemplateMap.add("rule_value", node_template.rule_value);
        sub_system_data[num].groupTemplate=node_template.group;
    }


    /*********************     加载页面                  ****************************/
    //根据查询信息加载页面
        <?php  foreach ($sub_system as $key=>$value) { ?>
    var name='<?=MyFunc::DisposeJSON($value['name'])?>';
    var category_id=tab_num;//<?=$value['sub_system_type']?>;
    var data=<?=$value['data']?>;
    var sub_system_id=<?=$value['id']?>;
    //console.log(name);
    //console.log(data['data']);
    //console.log(eval(data));
    //加标签页
    addTab(name,category_id,sub_system_id);
    //初始化标签页  myDiagram
//                        console.log(data);
    if(data['image_base']==0) {
        image_in[category_id]=0;
        init2('myDiagram'+category_id,category_id);
    }
    else  {
        image_in[category_id]=1;
        image_url_in[category_id]=data['image_id'];

        image_url=all_image[data['image_id']]['url'];

        init1('myDiagram'+category_id,category_id,image_url);
    }
    sub_system_data[category_id].model = go.Model.fromJson(data['data']);
    //tab_num=tab_num>=category_id?tab_num:category_id;
    tab_num++;
    $('#tabs2').tabs({active:1});
    <?php  }  ?>





    /*****************************************POINT SEARCH**************************************************************/



    var temp = new Array();

    var $__type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
    var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;


    var $current_keys = [];
    var $count = 0;

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;

    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    /* TABLETOOLS */
    var datable = $('#datatable_tabletools').dataTable({
        "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        },
        "autoWidth" : true,
//            "bPaginate":false,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
//            "bLengthChange":true,
        "iDisplayLength":5,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_tabletools) {
                responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_tabletools.respond();
        }
    });

    var datable2 = $('#selected_datatable_tabletools').dataTable({
        "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        },
        "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
        "iDisplayLength":5,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_tabletools) {
                responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_tabletools.respond();
        }
    });


    var datable3 = $('#selected_datatable_tabletools1').dataTable({
        "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "oTableTools": {
        },
        "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
        "iDisplayLength":5,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_tabletools) {
                responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools1'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_tabletools.respond();
        }
    });




    $('#selected_datatable_tabletools3').dataTable({
        "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "bSort" : false,
//            "sScrollX": "100%",
//            "bScrollCollapse": true,
        "paging":   false
    } );



    $('#point_select').multiSelect({
        selectableHeader: "<div class='custom-header'>搜索结果</div>",
        selectionHeader: "<div class='custom-header'>已选点位</div>",
        keepOrder: true
    });
    $('#point_select').multiSelect('deselect_all');

    $("#checkAllBtn").click(function() {
        console.log(111);
        $("#point_select").multiSelect("select_all");
    });
    //ajax提交
    $('#all_select').click(function(){
        console.log(111);
        $('#point_select').multiSelect('select_all');
    });




    //        timeTypeChange('year', $('#_time'));
    function regenerateTable(data, id, exeparam)
    {
        //动态添加元素
        // $('#point_select').multiSelect('addOption', data);
        var trstr = '<tbody id="'+id+'">';
        for (var i in data) {
            switch (exeparam){
                case 'exeselect':
                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['pointName']+'</td>'+'<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                    break;
                case 'exedel':
                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['pointName']+'</td>'+'<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                    break;
            }

        }

        trstr += '</table>';

        switch (exeparam){
            case 'exeselect':
                //clear the table tbody content and set the new data
                datable.fnClearTable();
                id = '#'+id;
                $(id).replaceWith(trstr);
                //recall the method dataTable() to regenerate the table
                $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                break;
            case 'exedel':
                //clear the table tbody content and set the new data
                datable2.fnClearTable();
                id = '#'+id;
                $(id).replaceWith(trstr);

                //recall the method dataTable() to regenerate the table
                $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                $('#selected_datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                break;
        }

    }
    function regenerateTable_with_editable_name(data, id, exeparam)
    {
        //动态添加元素
        // $('#point_select').multiSelect('addOption', data);
        var trstr = '<tbody id="'+id+'">';
        for (var i in data) {
            switch (exeparam){
                case 'exeselect':
                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td>'+'<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                    break;
                case 'exedel':
                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td>'+'<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                    break;
            }

        }

        trstr += '</table>';

        switch (exeparam){
            case 'exeselect':
                //clear the table tbody content and set the new data
                datable.fnClearTable();
                id = '#'+id;
                $(id).replaceWith(trstr);

                //recall the method dataTable() to regenerate the table
                $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                $('#datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

                break;
            case 'exedel':
                //clear the table tbody content and set the new data
                datable2.fnClearTable();
                id = '#'+id;
                $(id).replaceWith(trstr);

                //recall the method dataTable() to regenerate the table
                $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                $('#selected_datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

                break;
        }

    }
    //消防点位搜索
    $('#event_search_submit').click(function(){
        $('#points_event').attr('value','event');
        console.log('更改为event');
        //更新为事件的表头
        var html= '<tr  class="tr_head">'
            +'<th data-hide="phone">事件ID</th>'
            +'<th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>事件名称</th>'
            +'<th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>'
            +'</tr>';
        $(".tr_head").replaceWith(html);

        //clear the table tbody content and set the new data
        datable.fnClearTable();

        var event_name=$('#event_name').val();
        //得到参数 name
        console.log(event_name);
        if(event_name.length==0)
            alert('名称不能为空');
        else{
            var $url = '/subsystem_manage/crud/fire-event-search';
            console.log($url);

            var get_points = $.ajax({
                type: "post",
                url: $url,
                timeout: 0,
                data: {name: event_name},
                success: function (msg) {
                    var data = eval(msg);
                    GlobalData = data;
                    console.log(data);
                    if (data) {
                        //先从server返回的json里面删除已选择的点位
                        //动态添加表格数据
                        regenerateTable(data, 'tbodyData', 'exeselect');

                    }
                },
                complete: function (XMLHttpRequest, status) { //请求完成后最终执行参数
                    if (status == 'timeout') {//超时,status还有success,error等值的情况

                        get_points.abort(); //取消请求
                        alert("搜索点位太多，请填写更详细信息");

                    }
                }
            });
        }

    });
    //点位事件搜索
    $('#point_event_search_submit').click(function(){
        $('#points_event').attr('value','point_event');
        console.log('更改为point_event');
        //更新为事件的表头
        var html= '<tr  class="tr_head">'
            +'<th data-hide="phone">事件ID</th>'
            +'<th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>事件名称</th>'
            +'<th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>'
            +'</tr>';
        $(".tr_head").replaceWith(html);

        //clear the table tbody content and set the new data
        datable.fnClearTable();

        var event_name=$('#event_name').val();
        //得到参数 name
        console.log(event_name);
        if(event_name.length==0)
            alert('名称不能为空');
        else{
            var $url = '/subsystem_manage/crud/point-event-search';
            console.log($url);

            var get_points = $.ajax({
                type: "post",
                url: $url,
                timeout: 0,
                data: {name: event_name},
                success: function (msg) {
                    var data = eval(msg);
                    GlobalData = data;
                    console.log(data);
                    if (data) {
                        //先从server返回的json里面删除已选择的点位
                        //动态添加表格数据
                        regenerateTable(data, 'tbodyData', 'exeselect');

                    }
                },
                complete: function (XMLHttpRequest, status) { //请求完成后最终执行参数
                    if (status == 'timeout') {//超时,status还有success,error等值的情况

                        get_points.abort(); //取消请求
                        alert("搜索点位太多，请填写更详细信息");

                    }
                }
            });
        }

    });
    //点位搜索
    $('#point_search_submit').click(function() {
        $('#points_event').attr('value','points');
        //更新为点位的表头
        var html= '<tr  class="tr_head">'
            +'<th data-hide="phone">点位ID</th>'
            +'<th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>'
            +'<th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>'
            +'</tr>';
        console.log(111);
        $(".tr_head").replaceWith(html);
        console.log(html);

        //clear the table tbody content and set the new data
        datable.fnClearTable();


        //recall the method dataTable() to regenerate the table
        $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});



        var energy_id = $('select[name=energy_category_id] option:checked').val();
        var location_id = $('select[name=location_category_id] option:checked').val();
        if (energy_id == '') {
            alert('请选择能源分类');
            return false;
        }

        if (location_id == '') {
            alert('请选择地理位置分类');
            return false;
        }


        var $url = '/point-batch/crud/ajax-find-point-specific?' + $('#_ws').serialize();
        console.log($url);

        var get_points=$.ajax({
            type: "GET",
            url: $url,
            timeout:0,
            data:  {},
            success: function (msg) {
                var data=eval(msg);
                GlobalData =data;
                console.log(data);
                if(data) {
                    //先从server返回的json里面删除已选择的点位
                    //动态添加表格数据
                    regenerateTable(data, 'tbodyData', 'exeselect');

                }
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况

                    get_points.abort(); //取消请求
                    alert("搜索点位太多，请填写更详细信息");

                }
            }
        });

//            $.get($url, function(data) {
//                GlobalData = data;
//
//                if(data) {
//                    //先从server返回的json里面删除已选择的点位
//                    //动态添加表格数据
//                    regenerateTable(data, 'tbodyData', 'exeselect');
//
//                }
//
//
//            }, 'json');
    });



    window.exedel = function(data){
        for (var i in selectedPointContainer) {
            if (selectedPointContainer[i]['pointId'] == data.id) {
                GlobalData.push(selectedPointContainer[i]);
                selectedPointContainer.splice(i, 1);
            }
        }
        //判断selectedPointContainer内的点位数据名字是否改变
        //若名字改变则要保留改变的名字信息

        regenerateTable(GlobalData, 'tbodyData', 'exeselect');
        regenerateTable(selectedPointContainer, 'tbodyData_one', 'exedel');
        regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');
        regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');

    };


    window.exeselect = function(data){
        if(selectedPointContainer==undefined)
            selectedPointContainer=new Array();
//            console.log(selectedPointContainer);
        if(selectedPointContainer!=undefined)
            if (selectedPointContainer.length != 0) {
                for (var i in selectedPointContainer) {
                    if (data.id == selectedPointContainer[i]['pointId']) {
                        alert('已经存在该点位');
                        return false;
//
                    }
                }
            }
        console.log(selectedPointContainer);
//                //判断是否已经添加点位
//                if (window.IsEmpty())

        for (var i in GlobalData) {
            if (data.id == GlobalData[i]['pointId']) {
                selectedPointContainer.push(GlobalData[i]);
                GlobalData.splice(i, 1);
            }
        }
        regenerateTable(GlobalData, 'tbodyData', 'exeselect');
        regenerateTable(selectedPointContainer, 'tbodyData_one', 'exedel');
        regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');
//        regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');
    };



    //初始化tabs窗口
    $('#tabs').tabs();
    //初始化accordion窗口
    var accordionIcons = {
        header: "fa fa-plus",    // custom icon class
        activeHeader: "fa fa-minus" // custom icon class
    };
    $(".accordion").accordion({
        autoHeight: false,
        heightStyle: "content",
        collapsible: true,
        animate: 300,
        icons: accordionIcons,
        header: "h4"
    })


    $("#_update_name").click(function(){
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/bacnet",
            data: {},
            success: function (msg) {
                if(msg==0){console.log("已更新");}
                if(msg==1){console.log("无更新");}

            }
        });
    })
    //提交时候，对表单进行验证
    $("#_sub").click(function ()   {
        //获取所有 需要选择的参数
        var time_type = $('#time_type').val();
        var time = $('#_time').val();
        var data_value = [];
        var point_ids = [];
        //            console.log($('#point_select option:checked'));
        $('#point_select option:checked').each(function () {
            //                console.log('dede');
            data_value.push($(this).attr('value'));
        });

        $('#selected_datatable_tabletools button').each(function (i, n) {
            // console.log('#tbodySelected');
            var point={
                id: n.id,
                name: n.name
            };
//                console.log(i);
//                console.log(n.id);
//                console.log(n);
            point_ids.push(point);

        })
        //隐藏搜索表格
        $("#_loop").hide();
        $("#point_search_div").hide();
        //显示点位展示框
        $("#wid-id-1").show();
        $("#_loop1").show();
//        regenerateTable(selectedPointContainer, 'tbodyData_one', 'exedel');
        regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');

        // return false;
//            console.log(data_value);
//            // return false;
//            var $error_infor = '';
//            var $error_signal = 0;
//
//            if (point_ids.length == 0) {
//                $error_infor = '请选择点位';
//                $error_signal = 1;
//            }
        $('#point_selected').val(point_ids);
        var points = [];
        for (var i in selectedPointContainer) {
            points.push(selectedPointContainer[i]['pointId']);
        }

//            console.log(points);

        $('#point_selected').val(points);
        //points 即为选择的点位
        //ajax获取信息
//            $.ajax({
//                type: "POST",
//                url: "/category/crud/ajax-get-element",
//                data: {sub_system_id:sub_system_id, element_id:key},
//                success: function (msg) {
//
//                }
//            });



    });
    $("#sub_prev").click(function(){
        //隐藏搜索表格
        $("#_loop").show();
        $("#point_search_div").show();
        //显示点位展示框
        $("#wid-id-1").hide();
        $("#_loop1").hide();
        //把已经更改的名字信息保存在 selectedPointContainer  内
        $('#selected_datatable_tabletools1 input').each(function (i, n) {

            for(var key in selectedPointContainer){
                console.log(n.mame);
                if(selectedPointContainer[key]['pointId']== n.name)
                    selectedPointContainer[key]['pointName']= n.value;
            }
        });
    });
    //table 信息绑定
    $("#sub_next").click(function(){
        //获取标记位 知道绑定的点位类型  points  和 event
        var points_event=$('#points_event').attr("value");
        console.log(points_event);
        console.log('提交名称和点位id');
        var binding_points=[];

        var sub_system_id=$("#sub_system_id").val();
        var element_id=$("#element_id").val();
        if(sub_system_id.length==0) alert('未获取本图编号,请点击本图');
//            console.log(sub_system_id);
//            console.log(element_id);


        //ajax保存表格配置信息
        switch (points_event) {
            case "points":
                if(element_id.length==0) alert('未获取节点编号');
                //绑定点位时 只需要id和name信息
                for(var key in selectedPointContainer){
                    var point={
                        id:  selectedPointContainer[key]['pointId'],
                        name: selectedPointContainer[key]['pointName']
                    };
                    binding_points.push(point);
                }
                $.ajax({
                    type: "POST",
                    url: "/subsystem_manage/crud/ajax-save-table-element",
                    data: {type:'points',sub_system_id: sub_system_id, element_id: element_id, binding_id: binding_points},
                    success: function (msg) {

                    }
                });
                break;
            case "point_event":
                if(element_id.length==0) alert('未获取节点编号');
                //绑定点位时 只需要id和name信息
                for(var key in selectedPointContainer){
                    var point={
                        id:  selectedPointContainer[key]['pointId'],
                        name: selectedPointContainer[key]['pointName']
                    };
                    binding_points.push(point);
                }
                $.ajax({
                    type: "POST",
                    url: "/subsystem_manage/crud/ajax-save-table-element",
                    data: {type:'point_event',sub_system_id: sub_system_id, element_id: element_id, binding_id: binding_points},
                    success: function (msg) {

                    }
                });
                break;
            case "event":
                //绑定事件信息时 需要id name 以及事件绑定的点位point_id
                for(var key in selectedPointContainer){
                    var point={
                        id:  selectedPointContainer[key]['pointId'],
                        name: selectedPointContainer[key]['pointName'],
                        point_id:selectedPointContainer[key]['point_id'],
                    };
                    binding_points.push(point);
                }
                //生成需要的消防模块 并后台绑定点位



                //ajax保存事件绑定信息
                $.ajax({
                    type: "POST",
                    url: "/subsystem_manage/crud/ajax-save-fire-event-element",
                    data: {sub_system_id: sub_system_id,binding_id: binding_points},
                    success: function (msg) {
                        if(msg.indexOf=='error')
                            alert(msg);
                        else{
                            msg=eval('(' + msg + ')');
                            console.log(msg);
                            var diagram=diagram_now;
                            var temp = diagram.model.toJson();
                            var data = eval('(' + temp + ')');
                            //得到节点对象
                            var nodeDataArray = data['nodeDataArray'];
                            console.log(nodeDataArray);
                            //检测是否已经存在key
                            var only_key=0;
                            for(var nodeDatas in nodeDataArray){
                                for(var node_key in msg['keys']){
                                    //如果key值重复
                                    if(msg['keys'][node_key]==nodeDataArray[nodeDatas]['key']){
                                        only_key=1;
                                    }
                                }
                            }
                            if(only_key==1)alert('存在已经添加的点位');
                            if(only_key==0){
                                var loc_x= 0,loc_y=0;
                                for(var key in msg['data']){
                                    if(loc_x==10) {loc_y++;loc_x=0}
                                    var loc=50*loc_x+' '+50*loc_y;
                                    var node={
                                        'category':'state_event',
                                        'key':key,
                                        'size':'20 20',
                                        'img':'/uploads/pic/event_green.png',
                                        'loc':loc,
                                        'text':{
                                            2:'/uploads/pic/event_green.png',
                                            1:'/uploads/pic/event_red.png',
                                            0:'/uploads/pic/event_gray.png'
                                        },
                                        'name':msg['data'][key]
                                    };
                                    loc_x++;
                                    nodeDataArray.push(node);
                                }

                            }
                            diagram.model.startTransaction("flash");
                            diagram.model.nodeDataArray=nodeDataArray;
                            diagram.model.commitTransaction("flash");
                            diagram_now=diagram;
                        }
                    }
                });
                break;
        }

    });

    $("#time_type").change(function () {
        _type = $(this).val();
        timeTypeChange(_type, $('#_time'));
    });
    $("#time_type").trigger('change');
    $('#_time').val($_date_time);

    $('#tabs').tabs();
    $('#tabs2').tabs();





    /*********************************** 新的点位分页搜索 start  **********************************/
    $("#bacnet_show").click(function(){
        if(!temp_bacnet){
            oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
            temp_bacnet = 1;
        }
    })

    $("#modbus_show").click(function(){
        if(!temp_modbus){
            oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
            temp_modbus = 1;
        }
    })

    $("#others_show").click(function(){
        if(!temp_others){
            oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
            temp_others = 1;
        }
    })
    $(document).ready(function () {
        $('#tabs').tabs({
            activate: function(event, ui) {
                ttInstances = TableTools.fnGetMasters();
                for (i in ttInstances) {
                    if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                }
            }
        });

        //  setTimeout(function(){$("#bacnet_show").click();},1000);
        setTimeout(function(){$("#bacnet_show").click();},1000);

    });
    var table_select=$('#datatable_select').DataTable({
        "iDisplayLength": 5

    });
    $("#search_new").click(function(){
        if ($("#tabs").css('display') == 'none')
            $("#tabs").show();
        else
            $("#tabs").hide();
    });
    $("#point_allot_submit").click(function(){
        console.log(new_selected_point);
        var url="/subsystem_manage/crud/ajax-save-location-points";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                location_id:<?=$location_id?>,
                points: new_selected_point
            },
            success: function (msg) {
                alert(msg);

            }
        });
    });
    $("#pointsUpdate").click(function(){
        console.log(new_selected_point);
        var url="/subsystem_manage/crud/update-location-point-rel";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                id:<?=$location_id?>
            },
            success: function (msg) {
                msg=eval('('+msg+')');
                console.log(msg);
                if(msg['info']=='error'){
                    alert('error');
                    console.log(msg['msg']);
                }
                //保存成功 更新点位选择框
                if(msg['info']=='success'){
                    alert('success');
                    location_points=msg['msg'];
                    updateSelectedPoints();
                }
            }
        });
    });


    //加载页面时要根据location_points信息更新selected表格信息
    updateSelectedPoints();
    function  updateSelectedPoints() {
        if (location_points.length != 0) {
            for (var key in location_points) {
                var point = location_points[key];
                table_select.row.add([
                    '<input type="checkbox" name="checkList_' + point.cn + '" value="' + point.id + '" >',
                    point.id,
                    point.cn,
                    '<label class=""><button type="button" onclick="javascript:exedel_new(this);" class="exedel_new" id="selected' + point.id + '" point_id="' + point.id + '" name="' + point.cn + '"><span class="glyphicon glyphicon-remove"></span></button></label>',
                ]).draw(false);
                //记下已经添加的点位 以待添加时比较
                new_selected_point.push(point.id);
            }
        }
    }

    window.exeselect_new = function(data){
        //把选择的点位data添加到点位选择表内
        //比较此点位是否已经加入到点位选择表中
        console.log(table_select.row.data);
        var element=$(data);
        if(new_selected_point.indexOf(element.attr("point_id"))!=-1){
            alert('点位已经存在');
        }
        else {
            table_select.row.add([
                '<input type="checkbox" name="checkList_' + data.name + '" value="' + data.point_id + '" >',
                element.attr("point_id"),
                element.attr("name"),
                '<label class=""><button type="button" onclick="javascript:exedel_new(this);" class="exedel_new" id="selected' + element.attr("point_id") + '" point_id="' + element.attr("point_id") + '" name="' + element.attr("name") + '"><span class="glyphicon glyphicon-remove"></span></button></label>',
            ]).draw(false);
            //记下已经添加的点位 以待添加时比较
            new_selected_point.push(element.attr("point_id"));
        }
    };
    window.exedel_new = function(data){
        var element=$(data);
        //删除这一行tr标签以及内部内容
        var tr=$('#'+data.id).parent().parent().parent();
        console.log(tr);
        table_select.row(tr).remove().draw( false);
        //去除 new_selected_point中删掉的点位
        for(var point in new_selected_point){
            if(new_selected_point[point]==element.attr("point_id")){
                new_selected_point=remove(new_selected_point,point);
            }
        }
        console.log(new_selected_point);
    };

    /**
     * 初始化表格数据
     * oTable  表格对象
     * name 表格ID
     * tool 初始化表格的编辑栏ID
     * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
     * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
     */
    function initTable(name,oTable,tool,protocol_id,type) {
        /* TABLETOOLS */
        table = $('#'+name).dataTable({
//                "rowCallback": function( row, data, index ) {
//                },
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
            //"sdom": "Bfrtip",
            "oTableTools": {
                "aButtons": [
                ],
                "sRowSelect": "os",
                "sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            "autoWidth": true,
            "lengthMenu": [[5,6, 7, 8, 9, 10, -1], [5,6, 7, 8, 9, 10, "All"]],
            "iDisplayLength": 5,
            //"bSort": false,
            "language": {
                "sProcessing": "处理中...",
                "sClear":"test",
                "sLengthMenu": "显示 _MENU_ 项结果",
                "sZeroRecords": "没有匹配结果",
                "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                "sInfoPostFix": "",
                "sSearch": "搜索:",
                "sUrl": "",
                "sEmptyTable": "表中数据为空",
                "sLoadingRecords": "载入中...",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上页",
                    "sNext": "下页",
                    "sLast": "末页"
                },
                "oAria": {
                    "sSortAscending": ": 以升序排列此列",
                    "sSortDescending": ": 以降序排列此列"
                }
            },
            "processing": false,
            "serverSide": true,
            'bPaginate': true,
            "bDestory": true,
            "bRetrieve": true,
            'bStateSave': true,
            "ajax": {
                "url": "/subsystem_manage/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                "type": "post",
                "error": function () {
                    alert("服务器未正常响应，请重试");
                }

            },
            "aoColumns": [
                {
                    "mDataProp": "id",
                    "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<input type='checkbox' name='checkList_"+name+"' value='" + sData + "' >");
                    }
                },
                {"mDataProp": "id"},
                {"mDataProp": "cn"},
                {
                    "mDataProp": "cn",
                    "mDataProp": "id",
                    "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html('<label class=""><button type="button" onclick="javascript:exeselect_new(this);" class="exeselect_new" id="show'+oData.id+'" point_id="'+oData.id+'" name="'+oData.cn+'" ><span class="glyphicon glyphicon-ok"></span></button></label>');
                        $('.aa').click(function(){
                            var strs= new Array(); //定义一数组
                            strs=$(this).attr("idAndName").split("@"); //字符分割
                            //     alert($ele_val1+'  '+$ele_text);
                            //  alert($(this).attr("idAndName"));
                            $ele_val='p'+strs[0];
                            $ele_text=strs[1];
                            $('#_pointname').val($ele_text);
                        });
                        //   $(nTd).html("<a href='/point_table_report/crud/doexport?point_id="+sData+"'>报表查看</a>"+"<br>"+"<br>"+"<a href='/point_table_report/crud/graphic?point_id="+sData+"'>图形查看</a>");
                    }
                }
            ],
            "fnInitComplete": function (oSettings, json) {

            },
            "order": [1, 'asc']
        });
        /* END TABLETOOLS */
        return table;
    }
    $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
        this.fnClearTable(this);
        this.oApi._fnProcessingDisplay(oSettings, true);
        var that = this;

        $.getJSON(oSettings.sAjaxSource, null, function (json) {
            /* Got the data - add it to the table */
            for (var i = 0; i < json.aaData.length; i++) {
                that.oApi._fnAddData(oSettings, json.aaData[i]);
            }
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            that.fnDraw(that);
            that.oApi._fnProcessingDisplay(oSettings, false);
        });
    }
    /*********************************** 新的点位分页搜索 end  **********************************/


    /**********************************  模式数据加载 start  ***********************************/
        //子系统选择,子系统时间模式选择,子系统本事件模式下的控制模式选择

    Init();
    //初始化data数组
    function Init()
    {
        var test_data=schemaInfo[1];
        console.log(11111);
        console.log(test_data);
        var title=test_data['name'];
        $('#title').val(title);
        var initData=test_data['control_info'];
        var temData=initData['data'];
        if(temData.length<=0)  flag=true;
        for(var i in temData)
        {
            console.log(temData[i]['time']);
            time.push(parseInt(temData[i]['time']['start']));
            time.push(parseInt(temData[i]['time']['end']));
            for(var j in temData[i]['control'])
            {
                var tem=[];
                tem.push(temData[i]['control'][j]['name']);
                tem.push(temData[i]['control'][j]['value']);
                control[j]=tem;
            }
            var tempdata=[];
            tempdata.push(time);tempdata.push(control);
            timeModelData[i]=tempdata;
            drawButton();
            control=[];
            time=[];
        }
        drawButton();
        console.log(timeModelData);
    }

    //初始化序列按钮
    function drawButton()
    {
        if(time.length>0){
            var starttime=time[0];
            var endtime=time[1];
            var s=starttime+'点'+'--'+endtime+'点';
            var str='<button type="button"  class="create btn-success1" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';
            $('#timebutton').append(str);
        }
    }


    /**********************************  模式数据加载 end     **********************************/
}
</script>