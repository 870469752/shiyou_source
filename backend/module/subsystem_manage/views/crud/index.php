<?php
use common\library\MyHtml;
use Yii\web\View;
use common\library\MyFunc;
use yii\helpers\Url;
use yii\helpers\Html;

//echo '<pre>';
//print_r($role);
//die;

$this->title = Yii::t('category', 'SubSystemManage');
?>


    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
                        <span class="widget-icon">
                            <i class="fa fa-table"></i>
                        </span>
                        <h2><?= MyHtml::encode($this->title) ?></h2>
                    </header>

                    <!-- widget content -->
                    <div class="widget-body">
                        <div id="nestable-menu">
                            <button type="button" class="btn btn-default"
                                    data-action="add">
                                <?= Yii::t('category', 'Add Category') ?>
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-action="default">
                                <?= Yii::t('category', 'Restore Modify') ?>
                            </button>
                            <button type="button" class="btn btn-default" data-action="save">
                                <?= Yii::t('category', 'Save Modify') ?>
                            </button>
                        </div>
                        <div class="dd" id="nestable">
                            <?php echo MyHtml::SubSystemManageTreeRecursion($tree); ?>
                        </div>

                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget -->
            </article>
        </div>
        <!-- 自定义公式模板 -->

        <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_role'  style="display: none;">
            <div class="arrow"></div>
            <h3 class="popover-title">选择角色</h3>
            <div class="popover-content">
                <div class="editableform-loading" style="display: none;"></div>
                <form class="form-inline editableform" style="">
                    <div class="control-group form-group"><div >
                            <div class="editable-input">
                                <div class="editable-address">
                                    <select id="select_point"  multiple="multiple"  data-placeholder="Select Your Options">
                                       <?php
                                            foreach($role as $role_id=>$role_name){
                                                echo ' <option value='.$role_id.'>'.$role_name.'</option>';
                                            }
                                       ?>
                                    </select>
                                </div>
                            </div>
                            <div class="editable-buttons">
                                <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                                <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                            </div>
                        </div>
                        <div class="editable-error-block help-block" style="display: none;"></div>
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php
$this->registerJsFile("js/color_picker/js/evol.colorpicker.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("js/color_picker/css/evol.colorpicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.min.css", ['backend\assets\AppAsset']);
?>

    <script>
        window.onload = function () {
            var role_id=''; //保存时选择的角色数组
            var thisobject;    //当前选择的菜单栏目
            var roleArray=[];  //该菜单栏对应的角色ID
            var idArray=[];    //删除的所有ID
            var user_role='{<?=$role_id_temp?>}';
            $('#_role').draggable();
            $('.dd').nestable('collapseAll');
            var GenerateComputePoint = <?=YII::$app->request->get('GenerateState',0)?>;
            if(GenerateComputePoint > 0){
            }

            // 子分类标签模板
            var li = '\
        <li class="dd-item dd3-item" data-id="" data-role='+user_role+'>\
            <div class="dd-handle dd3-handle">Drag</div>\
		    <div class="dd3-content">\
		        <span class="name">请单击修改名称</span>\
		        <em class="close pull-right" style="padding-left:10px;">×</em>\
                <a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark report" rel="tooltip" data-placement="top" data-original-title="新建报表">\
                <i class="fa  fa-eye fa-fw"></i>\
                </a>\
                <a href="javacript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark role" rel="tooltip" data-placement="top" data-original-title="编辑角色">\
                <i class="fa fa-pencil"></i>\
                </a>\
                <a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" title="" data-placement="top" data-original-title="添加子类">\
                    <i class="fa fa-sitemap fa-fw"></i>\
		        </a>\
		    </div>\
		</li>';

            /**
             * 分类中的图标事件
             */
            $('#nestable').on('click', '.close',function (e) {
                if (confirm("是否删除") == true){
                    if($(this).parent().parent().attr('data-id')=='' ||$(this).parent().parent().attr('data-id')==null){

                    }else{
                        idArray.push($(this).parent().parent().attr('data-id'));
                    }
                    $(this).parent().parent().remove();
                }
            }).on('click', '.add',function (e) { // 添加子分类
                var ol = $('<ol>', {"class": "dd-list"});
                var ol_object = $(this).parent().parent().find('ol').first();
                if (ol_object.length <= 0) {
                    $(this).parent().parent().append(ol);
                    ol_object = ol;
                }
                ol_object.append(li);
            }).on('click', '.name',function (e) { // 点击名称弹出文本框
                var input = $('<input>', {"class": "text-name", "height": "20px", "type": "text", "value": $(this).text()});
                $(this).html(input);
                input.focus();
            }).on('blur', '.text-name',function (e) { // 失去焦点保存文本
                edit_text($(this));
            }).on('keydown', '.text-name', function (e) { // 按回车保存文本
                if (e.keyCode == 13) {
                    edit_text($(this));
                }
            }).on('click', '.role', function (e) { // 点击编辑角色按钮.
                thisobject=$(this);
                $('#_role').css({'left': 600, 'top':20});
                roleArray=[];
                var str=$(this).parent().parent().attr('data-role');
                if(str==''|| str==null){
                    roleArray=[];
                }else{
                    var splitstr=str.substr(1,str.length-2);
                    roleArray=splitstr.split(",");
                }
                $("#select_point").chosen({
                    width: "95%"
                });
                $("#select_point").val(roleArray);
                $("#select_point").trigger("chosen:updated");
                $('#_role').show();
            });

            //角色按钮
            $('.editable-container').on('click', '.editable-submit', function(){
                role_id= '{'+$("#select_point").val()+'}';
                edit_role(thisobject);
                $('#_role').hide();
            })
            /************************* 关闭 按钮 *****************************************************/
            $('.editable-container').on('click', '.editable-cancel', function(){
                $('#_role').hide();
            })

            //所选角色ID变化时
            $("#_ele").change(function(){
            });

            /**
             * 编辑分类
             */
            function edit_text(object) {
                var name_value = object.val() ? object.val() : '请单击修改名称'; // 如果没有就显示默认值
//                alert(name_value);
                var zh=object.parent().parent().parent().attr('data-zh');// 获取中文名称
                var en=object.parent().parent().parent().attr('data-en');// 获取英文名称;
                if('<?= Yii::$app->language ?>'=='zh-cn'){
                    object.parent().parent().parent().attr('data-zh', name_value);
                }else{
                    object.parent().parent().parent().attr('data-en', name_value);
                }
                object.parent().html(name_value);
            }

            /**
             * 编辑分类
             */
            function edit_role(object) {
                if(role_id==''|| role_id ==' '|| role_id==null){
                    role_id=<?=$role_id?>;
                }else{

                }
                object.parent().parent().attr('data-role', role_id);
                role_id='';
            }

            /**
             * 最顶上的菜单按钮
             */
            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target), action = target.data('action');
                if (action === 'expand-all') { // 展开所有
                    $('.dd').nestable('expandAll');
                }
                else if (action === 'collapse-all') { // 合并所有
                    $('.dd').nestable('collapseAll');
                }
                else if (action === 'add') { // 添加分类
                    var ol = $('<ol>', {"class": "dd-list"});
                    var ol_object = $("#nestable ol").first();
                    if (ol_object.length <= 0) {
                        $("#nestable").append(ol);
                        ol_object = ol;
                    }
                    ol_object.append(li);
                }
                else if (action === 'default') { // 恢复默认
                    location.reload();
                }
                else if (action === 'save') { // 保存数据
                    console.log($('#nestable').nestable('serialize'));
                    console.log(idArray);
                    $.post("<?= Url::toRoute('/subsystem_manage/crud/subsystem-manage-update') ?>", {
                            "data": $('#nestable').nestable('serialize'),
                            "idArray":idArray
                        },
                        function (link) {
                            alert('保存成功！');
                           location.href = link;
                        });
                }
            });
        }
    </script>
<?php
$this->registerJs("$('#nestable').nestable();", View::POS_READY);
$this->registerJsFile('/js/plugin/jquery-nestable/jquery.nestable.min.js', [
    'depends' => 'yii\web\JqueryAsset'
]);
?>