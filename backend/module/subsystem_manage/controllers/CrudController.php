<?php

namespace backend\module\subsystem_manage\controllers;

use backend\controllers\MyController;
use backend\models\AlarmLog;
use backend\models\AlarmRule;
use yii\filters\VerbFilter;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use backend\models\SubsystemManage;
use backend\models\Image;
use backend\models\ImageGroup;
use backend\models\Sub_System_Category;
use backend\models\EnergyCategory;
use backend\models\Location;
use common\models\User;
use backend\models\Point;
use backend\models\PointEvent;
use backend\models\PointCategoryRel;
use backend\models\PointCategoryConfig;
use backend\models\SchemaControl;
use backend\models\CommandLog;
use backend\models\ScheduleTaskLog;
use common\library\MyFunc;
use common\library\Ssp;
use yii\helpers\Url;
use Yii;
/**
 * Class CrudController
 * @package backend\module\category\controllers
 */
class CrudController extends MyController {
    /**
* @var array 页面上存在的ID
*/
  //  public $ids = [0];
    public $ids=[];
	/**
	 * Lists all Category models.
	 *
	 * @return mixed
	 */
    /**
     * Lists all Category models.
     *
     * @return mixed
     */


    public function actionIndex() {
        //当前登录用户的角色ID
        $user_roleId =  MyFunc::getRoleId();
        $subsystemange=new SubSystemManage();
        // $user_roleId=0;
        $data=$subsystemange->getSubsystems($user_roleId);
        $tree = MyFunc::TreeData($data);
        //print_r($user_roleId);die;
        $role=$subsystemange->findRole();

        return $this->render ( 'index', [
            'tree' => $tree,
            'role'=>$role,
            'role_id_temp'=>$user_roleId
        ] );
    }
    /*
     * 更新报表树
     */
    public function actionSubsystemManageUpdate() {
        if(!isset($_POST['idArray'])) $idArray=[];
        else $idArray=$_POST['idArray'];
        if($data = Yii::$app->request->post('data')){
            $this->subsystem_recursion($data,null); // 递归添加或更新
        }
        // SubSystemManage::deleteAll(['not in', 'id', $idArray]); // 删除页面上没有的ID
        if($idArray ==null || $idArray=='[]'|| $idArray==''){
        }else{
            SubSystemManage::deleteAll(['in', 'id', $idArray]); // 删除页面上没有的ID
        }
        return Url::toRoute('/subsystem_manage/crud/');
    }

    /*
     * 存储报表数组到数据库
     */
    protected function subsystem_recursion($tree, $pid){
        foreach($tree as $row){
            if(isset($row['zh']) || isset($row['en'])){
                $model = $row['id']?$this->findSubSystemManageModel($row['id']):new SubSystemManage(); // 如果没有ID就是添加
                if(isset($model)){
                    $model->parent_id = $pid;
                    $name=array(
                        "data_type"=>"decription",
                        "data"=>array(
                            "zh-cn"=>isset($row['zh'])?$row['zh']:'null',
                            "en-us"=>isset($row['en'])?$row['en']:'null'
                        )
                    );
     //               $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                      $model->name = json_encode($name, JSON_UNESCAPED_UNICODE);
                    //设置type 默认值为0
                    $model->category_type=2;
                    $model->role_id=$row['role'];
                    if($model->save()&& $row['id'] == ''){ // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉\
                        $row['id'] = $model->primaryKey;
                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->subsystem_recursion( $row['children'], $row['id']);
                }
            }
        }
        return ;
    }

    protected function findSubSystemManageModel($id) {
        if (($model = SubSystemManage::findOne ( $id )) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException ( 'The requested page does not exist.' );
        }
    }
    public function actionLocationSubsystem($id){
        $sub_system_model =new SubSystem();
        //更改以前的location类为 subsystem_manage
        $subsystemManage=SubsystemManage::findOne($id);
        $image_base=Image::findOne($subsystemManage->image_id);
        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        if(sizeof($sub_system)==1){
            $id=$sub_system[0]['id'];
            $model =SubSystem::findModel($id);

            $sub_system=ImageGroup::find()->where(['location_id'=>$model->location_id])->asArray()->all();
            $location_info=SubsystemManage::findOne($model->location_id);
            $url=Image::findModel($location_info->image_id);
            $sub_system_data=self::getSubSystemPoint($sub_system);
            $event_points=self::GetEventPoint($sub_system_data['events']);
            $save_state=self::SavePointCommand($event_points);
            $save_state=self::SavePointCommand($sub_system_data['points']);
            $sub_system_data=json_encode($sub_system_data);
            if(empty($url->default)){
                $base_map='uploads/pic/map1.jpg';

            }
            $user_id=Yii::$app->user->id;
            $user=User::findOne($user_id);
            $control=0;
            /*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/

            return $this->render('subsystem', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'all_image'=>$all_image,
                'id' => $id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
        else{

//            $this->redirect('/category/crud/location-only-view?id='.$id);
            $sub_system_model =new SubSystem();
            $location=Location::findOne($id);
//            $image_base=Image::findOne($location->image_id);

            //所有的本楼层的子系统
            $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

            //子系统类别  sub_system_category内的值
            $sub_system_category=new Sub_System_Category();
            $all_sub_system=$sub_system_category->getAll();
            $all_sub_system=MyFunc::_map($all_sub_system,'id','name');



            $sub_system_in=$all_sub_system;
            //TODO
            //系统内容为空 会进不去！！！！！
            $sub_system_data=self::getSubSystemPoint($sub_system);
            $event_points=self::GetEventPoint($sub_system_data['events']);
            $save_state=self::SavePointCommand($event_points);
            $save_state=self::SavePointCommand($sub_system_data['points']);
            if(empty($image_base)){
                $url='uploads/pic/map1.jpg';
            }

            else $url=$image_base->url;

            $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());



            //在此添加一个 所有分类
            array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

            $location_tree = MyFunc::TreeData(Location::getAllCategory());
            //在此添加一个 所有分类
            array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

            $sub_system_data=json_encode($sub_system_data);


            return $this->render('location_only_view', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree
            ]);
        }
    }
    public function actionLocationView($id) {
        $sub_system_model =new SubSystem();
        //更改以前的location类为 subsystem_manage
        $subsystemManage=SubsystemManage::findOne($id);
        $image_base=Image::findOne($subsystemManage->image_id);

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();
        foreach ($all_image as $key=>$value) {
            $image_data[$key]=json_decode($value['name'],1)['data']['zh-cn'];
        }
        $sub_system_in=$all_sub_system;
        //TODO
        //系统内容为空 会进不去！！！！！
        //得到子系统点位信息
        $sub_system_data=self::getSubSystemPoint($sub_system);
        $event_points=self::GetEventPoint($sub_system_data['events']);
        $save_state=self::SavePointCommand($event_points);
        $save_state=self::SavePointCommand($sub_system_data['points']);
//        echo '<pre>';
//        print_r($sub_system_data);die;
        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

        //本图所包含的点位id信息
        $points=[];
        $points=isset($points['points'])?$points['points']:null;
        $points=Point::find()
            ->select("id,name->'data'->>'zh-cn' as cn")
            ->where(['id'=>$points])
            ->asArray()->all();

        $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());

//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());



        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

        $sub_system_data=json_encode($sub_system_data);

//
//        $end_time=microtime(true);
//        print_r($end_time-$start_time);
//        die;
        $schema_info=SchemaControl::find()->indexBy('id')
            ->asArray()->all();

        foreach ($schema_info as $key=>$schema) {
            $schemaSelect[$key]=$key;
            $schema_info[$key]['control_info']=json_decode($schema['control_info']);
        }


        return $this->render('location_view', [
            'sub_system_model' => $sub_system_model,
            'location_id' => $id,
            'sub_system_data'=>$sub_system_data,
            //底图
            'image_base' => $url,
            'sub_system_in' => $sub_system_in,
            'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system' => $sub_system,
            'all_image' => $all_image,
            'image_data'=>$image_data,
            'category_tree' => $tree,
            'location_tree' => $location_tree,
            //楼层所有的点位idxin信息
            'points'=>json_encode($points),
            //模式信息
            'schema_info'=>json_encode($schema_info),
            //模式下拉列表信息
            'schemaSelect'=>$schemaSelect
        ]);
    }

    public function actionAlarmSubsystem($id){
        //更改以前的location类为 subsystem_manage
        $model = SubSystem::findModel($id);

        $sub_system = SubSystem::find()->where(['id' => $id])->asArray()->all();
        $sub_system_data = self::getSubSystemPoint($sub_system);
        $event_points = self::GetEventPoint($sub_system_data['events']);
        $save_state = self::SavePointCommand($event_points);
        $save_state = self::SavePointCommand($sub_system_data['points']);
        $sub_system_data = json_encode($sub_system_data);
        $base_map = 'uploads/pic/map1.jpg';
        $user_id = Yii::$app->user->id;
        $control = 0;
        $result=[
            'control' => $control,
            'user_id' => $user_id,
            'data'=>$model->data,
            'location_id'=>$model->location_id,
            'base_map' => $base_map,
            'id' => $id,
            'sub_system_data' => $sub_system_data
        ];
        return json_encode($result);
    }
    public function actionTest(){
        $value=MyFunc::RedisEvent(1877,'timestamp');
        echo '<pre>';
        print_r($value);
        die;
    }
    //测试电力首页
	public function actionLocationStation1()
	{
        $t1=microtime(true);
		$id=3622;
		$allData=self::getAlarmCount();
		$sub_system_model =new SubSystem();
		//更改以前的location类为 subsystem_manage
		$subsystemManage=SubsystemManage::findOne($id);
		$image_base=Image::findOne($subsystemManage->image_id);

		//所有的本楼层的子系统
		$sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();
		if(sizeof($sub_system)==1){
			$id=$sub_system[0]['id'];
			$model =SubSystem::findModel($id);
			$sub_system=ImageGroup::find()->where(['location_id'=>$model->location_id])->asArray()->all();
			$location_info=SubsystemManage::findOne($model->location_id);
			$url=Image::findModel($location_info->image_id);
			$sub_system_data=self::getSubSystemPoint($sub_system);
			$event_points=self::GetEventPoint($sub_system_data['events']);
			$save_state=self::SavePointCommand($event_points);
			$save_state=self::SavePointCommand($sub_system_data['points']);
			$sub_system_data=json_encode($sub_system_data);
			if(empty($url->default)){
				$base_map='uploads/pic/map1.jpg';
			}
			$user_id=Yii::$app->user->id;
			$user=User::findOne($user_id);
			 $control=0;
            /*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/
            $t2=microtime(true);
//            echo "<pre>";
//            print_r($t2-$t1);
//            die;
			return $this->render('myhtml2', [
				'control'=>$control,
				'user_id'=>$user_id,
				'model' => $model,
				'base_map' => $base_map,
				'id' => $id,
				'sub_system_data'=>$sub_system_data,
				'allData'=>$allData
			]);
		}
		else{
//            $this->redirect('/category/crud/location-only-view?id='.$id);
			$sub_system_model =new SubSystem();
			$location=Location::findOne($id);
			$image_base=Image::findOne($location->image_id);

			//所有的本楼层的子系统
			$sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

			//子系统类别  sub_system_category内的值
			$sub_system_category=new Sub_System_Category();
			$all_sub_system=$sub_system_category->getAll();
			$all_sub_system=MyFunc::_map($all_sub_system,'id','name');


			//所有的图
			$all_image=new Image();
			$all_image=$all_image->findAllImage();

			$sub_system_in=$all_sub_system;
			//TODO
			//系统内容为空 会进不去！！！！！
			$sub_system_data=self::getSubSystemPoint($sub_system);
			$event_points=self::GetEventPoint($sub_system_data['events']);
			$save_state=self::SavePointCommand($event_points);
			$save_state=self::SavePointCommand($sub_system_data['points']);
			if(empty($image_base)){
				$url='uploads/pic/map1.jpg';
			}

			else $url=$image_base->url;

			$tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());

			//在此添加一个 所有分类
			array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

			$location_tree = MyFunc::TreeData(Location::getAllCategory());
			//在此添加一个 所有分类
			array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

			$sub_system_data=json_encode($sub_system_data);

			return $this->render('location_only_view', [
				'sub_system_model' => $sub_system_model,
				'location_id' => $id,
				'sub_system_data'=>$sub_system_data,
				//底图
				'image_base' => $url,
				'sub_system_in' => $sub_system_in,
				'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
				'sub_system' => $sub_system,
				'all_image' => $all_image,

				'category_tree' => $tree,
				'location_tree' => $location_tree
			]);
		}
	}
   //电力总图首页
	public function actionElectrinicIndex()
	{
		return $this->render('electrinic_index');
	}
	//异步加载子系统系统
	public function actionAjaxGetSubsystem()

	{
		$id=3622;
		$sub_system_model =new SubSystem();
		//更改以前的location类为 subsystem_manage
		$subsystemManage=SubsystemManage::findOne($id);
		$image_base=Image::findOne($subsystemManage->image_id);

		//所有的本楼层的子系统
		$sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

		if(sizeof($sub_system)==1){
			$id=$sub_system[0]['id'];
			//$model =SubSystem::findModel($id);
			$model =SubSystem::find()->where(['id'=>$id])->asArray()->one();
			$sub_system=ImageGroup::find()->where(['location_id'=>$model["location_id"]])->asArray()->all();
			$location_info=SubsystemManage::findOne($model['location_id']);
			$url=Image::findModel($location_info->image_id);
			$sub_system_data=self::getSubSystemPoint($sub_system);
//			print_r($id);
//			die;
			$event_points=self::GetEventPoint($sub_system_data['events']);
			$save_state=self::SavePointCommand($event_points);
			$save_state=self::SavePointCommand($sub_system_data['points']);
			$sub_system_data=json_encode($sub_system_data);
			if(empty($url->default)){
				$base_map='uploads/pic/map1.jpg';
			}
			$user_id=Yii::$app->user->id;
			$user=User::findOne($user_id);
			$control=0;
			/*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/
			return json_encode([
				'control'=>$control,
				'user_id'=>$user_id,
				'model' => $model,
				'base_map' => $base_map,
				'id' => $id,
				'sub_system_data'=>$sub_system_data
			]);
		}
	}
	//异步加载每个地块电力报警信息
	public function actionAjaxGetPie()
	{
		return json_encode(self::getAlarmCount());
	}
	//获取某地块数据
	public function actionGetone($id)
	{
		$sub_system_model =new SubSystem();
		//更改以前的location类为 subsystem_manage
		$subsystemManage=SubsystemManage::findOne($id);
		$image_base=Image::findOne($subsystemManage->image_id);

		//所有的本楼层的子系统
		$sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

		if(sizeof($sub_system)==1){
			$id=$sub_system[0]['id'];
			//$model =SubSystem::findModel($id);
			$model =SubSystem::find()->where(['id'=>$id])->asArray()->one();
			$sub_system=ImageGroup::find()->where(['location_id'=>$model["location_id"]])->asArray()->all();
			$location_info=SubsystemManage::findOne($model['location_id']);
			$url=Image::findModel($location_info->image_id);
			$sub_system_data=self::getSubSystemPoint($sub_system);
//			print_r($id);
//			die;
			$event_points=self::GetEventPoint($sub_system_data['events']);
			$save_state=self::SavePointCommand($event_points);
			$save_state=self::SavePointCommand($sub_system_data['points']);
			$sub_system_data=json_encode($sub_system_data);
			if(empty($url->default)){
				$base_map='uploads/pic/map1.jpg';
			}
			$user_id=Yii::$app->user->id;
            //$user_id=19;
			$user=User::findOne($user_id);
			 $control=0;
            /*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/
			return json_encode([
				'control'=>$control,
				'user_id'=>$user_id,
				'model' => $model,
				'base_map' => $base_map,
				'id' => $id,
				'sub_system_data'=>$sub_system_data
			]);
		}
		else{

//            $this->redirect('/category/crud/location-only-view?id='.$id);
			$sub_system_model =new SubSystem();
			$location=Location::findOne($id);
			$image_base=Image::findOne($location->image_id);

			//所有的本楼层的子系统
			$sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

			//子系统类别  sub_system_category内的值
			$sub_system_category=new Sub_System_Category();
			$all_sub_system=$sub_system_category->getAll();
			$all_sub_system=MyFunc::_map($all_sub_system,'id','name');


			//所有的图
			$all_image=new Image();
			$all_image=$all_image->findAllImage();

			$sub_system_in=$all_sub_system;
			//TODO
			//系统内容为空 会进不去！！！！！
			$sub_system_data=self::getSubSystemPoint($sub_system);
			$event_points=self::GetEventPoint($sub_system_data['events']);
			$save_state=self::SavePointCommand($event_points);
			$save_state=self::SavePointCommand($sub_system_data['points']);
			if(empty($image_base)){
				$url='uploads/pic/map1.jpg';
			}

			else $url=$image_base->url;

			$tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());

			//在此添加一个 所有分类
			array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

			$location_tree = MyFunc::TreeData(Location::getAllCategory());
			//在此添加一个 所有分类
			array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

			$sub_system_data=json_encode($sub_system_data);

			return json_encode( [
				'sub_system_model' => $sub_system_model,
				'location_id' => $id,
				'sub_system_data'=>$sub_system_data,
				//底图
				'image_base' => $url,
				'sub_system_in' => $sub_system_in,
				'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
				'sub_system' => $sub_system,
				'all_image' => $all_image,

				'category_tree' => $tree,
				'location_tree' => $location_tree
			]);
		}
	}
    //发布
    public function actionPublish(){
        $data=Yii::$app->request->post();

        //发布本地块对应的摄像头
        Yii::$app->redis->publish("CameraPoint",implode(",",$data['location_id']));
        $user_id=Yii::$app->user->id;
        //更新现在的计划任务数据  标识本次操作已完成
//        $data_=Yii::$app->cache->get("TodayTask");
        $data_=Yii::$app->redis->get("TASK_INFO:".$user_id);
        $data_=msgpack_unpack($data_);
        //当前时间
        $now_date = date("Y-m-d H:i:s", time());
        $today_task=(array)$data_['tasks'];
//        echo '<pre>';
//        print_r($today_task);
//        die;
        foreach ($today_task as $key=>$value) {
            $model=ScheduleTaskLog::findOne($value['id']);
            $model->performer=$user_id;
            $status=$model->status;
            //如果这个时间点有任务  就更新它的action
//            if($value['start_timestamp']<$now_date && $now_date<$value['end_timestamp']) {
                //任务开始
                //统计任务完成数量
                $model->status='2';
                $actions = $value['action'];
                $total_action = 0;
                $complete = 0;
                foreach ($actions as $num => $act) {
                    $total_action++;
                    if ($act == true)
                        $complete++;
                    //修改此次操作的任务为true
                     if ($num == $data['name']){
                        $today_task[$key]['action'][$num] = true;
                         //修改前如果是false则完成度+1
                         if ($act == false)
                             $complete++;
                    }
                }
                //如果任务已经完成
                if ($total_action == $complete) {
                    $model->status='4';
                    $model->save();
                }
                else if($complete!=0 && $status!='4'){
                    $model->status='3';
                    $model->save();
                }

//            }
        }
        $new_data['tasks']=$today_task;
        $new_data['task_count']=$data_['task_count'];
        $new_data['tasks_name']=(array)$data_['tasks_name'];
//        Yii::$app->cache->set("TodayTask",$data_);
        Yii::$app->redis->set("TASK_INFO:".$user_id,msgpack_pack($new_data));
        return json_encode($new_data);
//        echo '<pre>';
//        print_r($data_);
//        die;
    }

	//获取各地块的电力点位
	public function getAlarmCount()
	{
		$all=array();  //各个地块下的所有电力点
		$location=new Location();
		$total=self::getCountAll();
		$location_ids=$location->find()->select(['id','name'])->where(['parent_id'=>1468])->all();
		//查找每个地块下面的点位
		foreach($location_ids as $k=>$v){
			$data=array(
				'id'=>0,
				'name'=>'',
				'count'=>0,
				'percent'=>0
			);
			$data['id']=$v->id;
			$data['name']=MyFunc::DisposeJSON($v->name);
			$data['count']=self::getCount($v->id);
			$data['percent']=round(($data['count']/$total)*100,2);
			$all[]=$data;
		}
		return $all;
	}
	//获取每个地块的报警数目
	public function getCount($id){
		$model=new Location();
        $tomorrow="'".date("Y-m-d",strtotime("-2 month")).' '.'00:00:00'."'";
		$data=$model->findBySql('SELECT COUNT (ID) FROM core.alarm_log WHERE (start_time >'.$tomorrow.') and alarm_rule_id IN (
		SELECT
			ID
		FROM
			core.alarm_rule
		WHERE
			event_id IN (
				SELECT
					ID
				FROM
					core.point_event
				WHERE
					point_id IN (
						SELECT
							point_id
						FROM
							core.point_category_rel
						WHERE
							category_id = 1475
						AND point_id IN (
							SELECT
								point_id
							FROM
								core.point_category_config
							WHERE
								category_id = '.$id.'
						)
					)
			)
)')->asArray()->all();
		return $data[0]['count'];
	}
    //获取所有地块的报警数目
	public function getCountAll(){
		$model=new Location();
        $tomorrow="'".date("Y-m-d",strtotime("-2 month")).' '.'00:00:00'."'";
		$data=$model->findBySql('SELECT COUNT (ID) FROM core.alarm_log WHERE (start_time >'.$tomorrow.') and alarm_rule_id IN (
		SELECT
			ID
		FROM
			core.alarm_rule
		WHERE
			event_id IN (
				SELECT
					ID
				FROM
					core.point_event
				WHERE
					point_id IN (
						SELECT
							point_id
						FROM
							core.point_category_rel
						WHERE
							category_id = 1475
						AND point_id IN (
							SELECT
								point_id
							FROM
								core.point_category_config
							WHERE
								category_id in (select id from core."location" where parent_id=1468)
						)
					)
			)
)')->asArray()->all();
		return $data[0]['count'];
	}
	//获取所有地块的最高温度
	public function actionGetHighTemperature()
	{
		$all=array();  //各个地块下的所有电力点
		$location=new Location();
		$total=self::getCountAll();
		$location_ids=$location->find()->select(['id','name'])->where(['parent_id'=>1468])->all();
		//查找每个地块下面的点位
		foreach($location_ids as $k=>$v){
			$data=array(
				'id'=>0,
				'name'=>'',
				'point_id'=>0,
				'value'=>0
			);
			$data['id']=$v->id;
			$data['name']=MyFunc::DisposeJSON($v->name);
			$data['value']=self::getHighTById($v->id);
			$data['point_id']=0;
			$all[]=$data;
		}
		return json_encode($all);

	}
    //获取某个地块的最高温度
    public function getHighTById($id){
        $data=array();
        $model=new Location();
        $elect_points=$model->findBySql('SELECT
							point_id
						FROM
							core.point_category_rel
						WHERE
							category_id = 12345
							AND point_id IN (
		SELECT
								point_id
							FROM
								core.point_category_config
							WHERE
								category_id = '.$id.')'
		        )->asArray()->all();
		if(count($elect_points)>0){
			foreach($elect_points as $k=>$v){
                $id1="DB:point:".$v['point_id'];
                $value = Yii::$app->redis->hget($id1,'value');
                if(empty($value)){
                    $data[]=0;
                }else{
                    $data[]=round($value,1);
                }
			}
		 return max($data);
		}else{
			return 0;
		}
	}
	//利用yii框架查找每个地块报警数目，执行时间太长
	public function actionGetAlarm()
	{
		$all=array();  //各个地块下的所有电力点
		$location=new Location();
		$location_ids=$location->find()->select(['id','name'])->where(['parent_id'=>1468])->all();
		//查找每个地块下面的点位
		foreach($location_ids as $k=>$v){
			$data=array(
				'id'=>0,
				'name'=>'',
				'point'=>array(),
				'count'=>0
			);
			$data['id']=$v->id;
			$data['name']=MyFunc::DisposeJSON($v->name);;
            $pointcategoryconf=new PointCategoryConfig();
			$pointcategoryconfs=$pointcategoryconf->find()->select(['point_id'])->where(['category_id'=>$v->id])->asArray()->all();
			$data['point']=$pointcategoryconfs;
			$data['count']=count($pointcategoryconfs);
			$all[]=$data;
		}
		//查找电力下面的点位
		$elect_rel=new PointCategoryRel();
		$points_rel=$elect_rel->find()->select(['point_id'])->where(['category_id'=>1475])->asArray()->all();
		$onesubcategory=array();  //转化为一维数组
		foreach($points_rel as $k=>$v){
			$onesubcategory[]=$v['point_id'];
		}
		//找出每个地块下面的属于电力的点
		foreach ($all as $k=>$v) {
			$onelocation=array(); //转化为一维数组
			foreach($v['point'] as $kk=>$vv){
				$onelocation[]=$vv['point_id'];
			}
			$points=array_intersect($onelocation,$onesubcategory);
			$v['point']=$points;
			$all[$k]=$v;
		}

		foreach($all as $k=>$v){
			$num=0;
			foreach($v['point'] as $kk=>$vv){
				$event=new PointEvent(); //在event中根据point_id查找event_id;
				$eventData=$event->find()->select(['id'])->where(['point_id'=>$vv])->asArray()->one();
				$alarm_rule=new AlarmRule(); //在alarm_rule中根据event_id查找rule_id;
				$alarm_rule_data=$alarm_rule->find()->select(['id'])->where(['event_id'=>$eventData['id']])->asArray()->one();
				$alarm_log=new AlarmLog(); //在alarm_log中根据rule_id查找id;
				$alarm_log_count=$alarm_rule->find()->select(['count(id) as sum'])->where(['event_id'=>$alarm_rule_data['id']])->asArray()->one();
			    $num=$num+$alarm_log_count['sum'];
			}
			$v['count']=$num;
			$all[$k]=$v;
		}

	}
	//$sub_system 为子系统数据组成的数组
	public function getSubSystemPoint($sub_system){
		//得到子系统内所有绑定的点位 组成数组
		$points=[];$sub_system_data=[];
		$camera_points=[];
		$keys=[];
		$point_keys=[];
		$camera_key=[];
		$events=[];
		$events_keys=[];
		foreach ($sub_system as $key=>$system) {
			$data=json_decode($system['data'],1);
			$data=json_decode($data['data'],1)['nodeDataArray'];

//            echo '<pre>';
//            print_r($data);
//            die;
			foreach ($data as $data_key=>$data_value) {
				//group 不存在category字段
				$nodecategory=isset($data_value['category'])?$data_value['category']:'';
				switch($nodecategory){
					case 'main':
					case 'many':
					case 'value':
					case 'onoffvalue':
					case 'alarmvalue':
					case 'main_edit':
					case 'only_value':
					case 'state':
                    case 'state_control':
					case 'state_event':
					case 'state_defend':
					case 'state_defend_1':
					case 'state_defend_ctrl':
					case 'gif':
					case 'gif_value':
					case 'control':
					case 'state_value':
					case 'value_1':
                    case 'rule_value':
						//得到 需要查询节点的key
						if( $data_value['key']!=null) {
							$keys[] = $data_value['key'];
							$sub_system_data[$system['id']][$data_value['key']] = null;
						}
//                    if($data_value['img']=="/uploads/pic/摄像头1.jpg")
						//判断是不是摄像头的点
						if(isset($data_value['img']))
							if(
								$data_value['img']=="/uploads/pic/摄像头1.jpg" || $data_value['img']=="/uploads/pic/摄像头2.jpg" ||
								$data_value['img']=="/uploads/pic/摄像头3.jpg" || $data_value['img']=="/uploads/pic/摄像头4.jpg"||
								$data_value['img']=="/uploads/pic/摄像头5.jpg"||$data_value['img']=="/uploads/pic/摄像头6.jpg"||
								$data_value['img']=="/uploads/pic/摄像头7.jpg"||$data_value['img']=="/uploads/pic/摄像头8.jpg"
							) {
								$camera_key[] =$data_value['key'];
							}
						break;
					case 'table':
						$keys[] = $data_value['key'];
						$sub_system_data[$system['id']][$data_value['key']] = null;
						break;
				}
			}


			//根据子系统id 和key 找到需要查询的点位集合
			$sub_system_id=$system['id'];
			$configs=SubSystemElement::find()->select('element_id,config')
				->where(['sub_system_id'=>$sub_system_id])
				->andWhere(['element_id'=>$keys])
				->asArray()->all();
			$type='points';

			foreach ($configs as $key=>$configs_value) {
				$data=json_decode($configs_value['config'],1)['data'];
				if(isset($data['points'])){
					$type='points';
					$points_info=$data['points'];
				}
				if(isset($data['events'])) {
					$type='events';
					$points_info = $data['events'];

				}
				if(isset($data['point_event'])) {
					$type='point_event';
					$points_info = $data['point_event'];

				}
//                echo '<pre>';
//                print_r($points_info);
//                die;
				//获取的data信息中  points 可能为点位id  可能为数组信息[id1=>name1,id2=>name2,...]
				if($points_info!="") {

					if (!is_array($points_info)) {
						$points[] = $points_info;
						$point_keys[]=$configs_value['element_id'];
						$sub_system_data[$system['id']][$configs_value['element_id']] = ['id' => $points_info];
						if(!empty($camera_key)){
							$camera_points[]= $points_info;
						}
					} else {
//                        echo '<pre>';
//                        print_r($points_info);

						foreach ($points_info as $point_id => $point_name) {
							if($type=='events' || $type=='point_event')  {
								if($point_id!='name'){
									$events[]=$point_id;
									$events_keys[]=$configs_value['element_id'];
								}
							}
							if($type=='points')  {$points[] = $point_id; $point_keys[]=$configs_value['element_id'];}
							$sub_system_data[$system['id']][$configs_value['element_id']][] = ['id' => $point_id, 'name' => $point_name];

						}

					}
				}

			}
		}

		if(!empty($sub_system_data )){
			foreach($sub_system_data as $sub_system_id=>$sub_system){
				foreach($sub_system as $key=>$value){
					if(empty($value))
						unset($sub_system_data[$sub_system_id][$key]);
				}
			}
		}
		return [
			'data'=>$sub_system_data,
			'points'=>array_unique($points),'points_key'=>array_unique($point_keys),
			'camera_points'=>array_unique($camera_points),
			'events'=>array_unique($events),'events_key'=>array_unique($events_keys)
		];
	}
	//得到event对应的点位信息
	public function GetEventPoint($events){
		$points=PointEvent::find()
			->select('point_id')
			->where(['id'=>$events])
			->asArray()->all();
		$result=[];
		if(!empty($points)){
			foreach ($points as $key=>$value) {
				$result[]=$value['point_id'];
			}
			$result=array_unique($result);
		}
		else $result;

		return $result;
	}
	//得到 points 点位 写入
	public function SavePointCommand($points){
        $user_id = Yii::$app->user->id;
		$save_state='save';
		if(empty($points)){
			return 'points empty';
		}
		else {
			foreach ($points as $key => $point) {
				$command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
				$time = date('Y-m-d H:i:s', time());
				$data['CommandLog'] = [
					'point_id' => $point,
					'type' => 1,
					'value' => '-1',
					'timestamp' => $time,
					'user_id' => $user_id,
					'status' => 1
				];
				$command_log->load($data);
				$command_log->save();
				/*if(!empty($command_log->errors)){
					echo '<pre>';
					print_r($command_log->errors);
					die;
					$save_state='save_error';
				}*/
			}
			return $save_state;
		}


	}
	//点击图标时显示查询已经绑定点位id
	public function actionAjaxGetElement(){
		$data=Yii::$app->request->post();

		$type='points';
		$model=new SubSystemElement();
		$model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
		$data=json_decode($model->config,1)['data'];
//        echo '<pre>';
//        print_r($data);
//        die;

		if(isset($data[$type])) {
			$key_info = $data[$type];
		}
		if(isset($data['events'])) {
			$type='events';
			$key_info = $data[$type];
		}
		if(isset($data['point_event'])) {
			$type='point_event';
			$key_info = $data[$type];
		}

		if(empty($key_info)){
			$points_info=[];
		}
		else {
			if($type=='points') {
				//如果是数组 则绑定的为表格，查询所有点位信息
				if (!is_array($key_info)) {
					$points[$key_info] = null;

				} else $points = $key_info;

				foreach ($points as $id => $name) {
					$points_id[] = $id;
				}
				$points_info = Point::getPointInfoById($points_id);

				foreach ($points_info as $key => $value) {
					$points_info[$key]['controller_id'] = $value['controller_id'];
					$points_info[$key]['pointId'] = $value['id'];
					$points_info[$key]['time']=Myfunc::RedisPoint($value['id'],"value_timestamp");
					//判断名称是否一样 不一样则显示数据库内的名称
//                        if ($points[$value['id']] == null)
					$points_info[$key]['pointName'] = MyFunc::DisposeJSON($value['name']);
//                        else
//                            $points_info[$key]['pointName'] = $points[$value['id']];

					$points_info[$key]['protocolName'] = $value['protocol_id'];
					$points_info[$key]['unit'] = $value['unit'];
					$points_info[$key]['value type'] = $value['value_type'];
				}
			}
			if($type=='events' || $type=='point_event'){
				$points = $key_info;
				foreach ($points as $id => $name) {
					if($id!='name')
						$points_id[] = $id;
				}
				$points_info = PointEvent::getPointEventInfoById($points_id);
				foreach($points_info as $key => $value) {
					$points_info[$key]['pointId'] = $value['id'];
					$points_info[$key]['pointName'] = MyFunc::DisposeJSON($value['name']);
					$points_info[$key]['point_id'] = $value['point_id'];
					$points_info[$key]['type'] = $value['type'];
				}
			}

		}
		$points_info = json_encode($points_info);
		return $points_info;
		//return json_encode($points);

	}
	//更新图内的点位值
	public function actionAjaxUpdateValueNew()
	{
//        $starttime=explode(' ',microtime());

		$data = Yii::$app->request->post()['data'];
		$data=json_decode($data,1);

		//point点位信息处理
		$points_data=Point::find()
			->select('id,name,value,update_timestamp as timestamp')
			->where(['id'=>$data['points']])
			->asArray()->all();
		foreach ($points_data as $key=>$value) {
			//点位值更新时间与当前时间间隔如果在一分钟内则是实时值 否则值为null
			$time_interval=time()-strtotime($value['timestamp']);
//            if($time_interval>60 || $time_interval<0)
//                $value['value']=null;

			$point_value=Myfunc::RedisPoint($value['id'],"value");

			$points_info[$value['id']]=[
				'name'=>MyFunc::DisposeJSON($value['name']),
				'value'=>$value['value'],
				'timestamp'=>Myfunc::RedisPoint($value['id'],"value_timestamp")

			];
			if($point_value!=null){
				$points_info[$value['id']]['value']=$point_value;
			}
		}
		//event事件信息处理
		$event_data=PointEvent::find()
			->select('id,name,trigger_status as value')
			->where(['id'=>$data['events']])
			->asArray()->all();

		$events_info=[];
		foreach ($event_data as $key=>$value) {
			$event_value=Myfunc::RedisEvent($value['id'],'status');
//            $event_value=$this->RedisEvent('39497','status');
//            echo '<pre>';
//            print_r($event_value);
//            die;
			if($event_value==null)$event_value=0;
			$events_info[$value['id']]=[
				'name'=>MyFunc::DisposeJSON($value['name']),
				'value'=>$event_value,
				'timestamp'=>Myfunc::RedisEvent($value['id'],'timestamp')

			];
		}
//        $value = Yii::$app->redis->hget("Event:39070",'status');
//        var_dump($value);
//        var_dump($events_info[39070]);
//        echo 33333;
//        var_dump(round($events_info[39070]['value'], 2));die;


//        $points_name=MyFunc::map($points_info,'id','name');
//        $points_value=MyFunc::map($points_info,'id','value');
		$result=$data['data'];

		foreach ($result as $key=>$key_value) {
			foreach ($key_value as $key_value_key => $key_value_value) {

				if (in_array($key_value_key, $data['points_key'])) {
					if (isset($key_value_value['id'])) {
						$result[$key][$key_value_key]['time'] = $points_info[$key_value_value['id']]['timestamp'];
						$result[$key][$key_value_key]['name'] = $points_info[$key_value_value['id']]['name'];
						if ($points_info[$key_value_value['id']]['value'] == null)
							$result[$key][$key_value_key]['value'] = null;
						else $result[$key][$key_value_key]['value'] = round($points_info[$key_value_value['id']]['value'], 2);
					} else {
						foreach ($key_value_value as $value_key => $value) {
							$time = $points_info[$value['id']]['timestamp'];
							$name = $result[$key][$key_value_key][$value_key]['name'];
							$result[$key][$key_value_key][$value_key]['time'] = $time;
							if ($name == 0) {
								$result[$key][$key_value_key][$value_key]['name'] = $points_info[$value['id']]['name'];
							}
							if ($points_info[$value['id']]['value'] == null)
								$result[$key][$key_value_key][$value_key]['value'] = null;
							else $result[$key][$key_value_key][$value_key]['value'] = round($points_info[$value['id']]['value'], 2);
						}
					}
				}
				//events节点值处理
				if (in_array($key_value_key, $data['events_key'])) {
					//event节点 绑定两个点位
					//会有一个id='name'  去掉
					foreach ($key_value_value as $value_key => $value) {
						if($value['id']!='name') {
							$time = 'time';
							$result[$key][$key_value_key][$value_key]['time'] = $time;
							$result[$key][$key_value_key][$value_key]['name'] = $events_info[$value['id']]['name'];
							if ($events_info[$value['id']]['value'] == null)
								$result[$key][$key_value_key][$value_key]['value'] = 0;
							else $result[$key][$key_value_key][$value_key]['value'] = round($events_info[$value['id']]['value'], 2);

						}
					}

				}
			}
		}

//        die;

		//如果points只绑定一个点  处理成对应格式
		//[sub_system_id][key]['0'=>[[id][name][value]]] ===>>>[sub_system_id][key][[id][name][value]]
		foreach ($result as $key=>$key_value) {
			foreach ($key_value as $key_value_key=>$key_value_value) {
				if(sizeof($result[$key][$key_value_key])==1){
					$result[$key][$key_value_key]['id']=$result[$key][$key_value_key][0]['id'];
					$result[$key][$key_value_key]['name']=$result[$key][$key_value_key][0]['name'];
					$result[$key][$key_value_key]['value']=$result[$key][$key_value_key][0]['value'];
					$result[$key][$key_value_key]['time']=$result[$key][$key_value_key][0]['time'];
					unset($result[$key][$key_value_key][0]);
				}
			}
		}

		return json_encode($result);
	}

	//撤防布防操作
	public function actionAjaxSetDefend(){
		$post_data=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($post_data);
//        die;
		$defend_value=null;
		$node_type=$post_data['node_type'];
		if($post_data['defend']==0)$defend_value=1;
		else $defend_value=0;
		$model=new SubSystemElement();
		$model=$model->findModelByData($post_data['sub_system_id'],$post_data['element_id']);
		$event_data=json_decode($model->config,1)['data']['point_event'];
		$defend_id=null;
		//找到绑定的点位中的撤防布防点位
		foreach ($event_data as $event_id=>$name) {
			if(
				( $node_type=='state_defend' && strstr($name,'布防操作成功'))||
				( $node_type=='state_defend_1' && strstr($name,'布防'))||
				( $node_type=='state_defend_ctrl' && strstr($name,'布防'))
			){
				$defend_id=$event_id;
			}
		}
		if($defend_id!=null){
			//入侵则手动改redis的值
			if($node_type=='state_defend'|| $node_type=='state_defend_1') {
				Yii::$app->redis->hset('Event:' . $defend_id,'status', $defend_value);
				Yii::$app->redis->publish('Event:' . $defend_id, $defend_value);
				return 'publish Event:'.$defend_id.' '.$defend_value.'success';
			}
			//周界则  插入命令
			if($node_type=='state_defend_ctrl')
			{
				//根据event_id 查找到point_id
				$point_id=PointEvent::find()
					->select('point_id')
					->where(['id'=>$defend_id])
					->asArray()->all()[0]['point_id'];

				$time= date('Y-m-d H:i:s',time());
//
				$user = Yii::$app->user;
				//判定点位类型

				//在command表里插入信息
				$command_log = new CommandLog();

				$data['CommandLog'] = [
					'point_id' => $point_id,
					'type' => 2,
					'value' => (string)$defend_value,
					'timestamp' => $time,
					'user_id' => $user->id,
					'status' => 1
				];
				$command_log->load($data);

				$command_log->save();
				$result=$command_log->errors;
				return json_encode($result);
			}

		}
		else return 'error[no defend event]';


	}


    //给点位发送控制命令
    public function actionAjaxControl(){
        //$data=['sub_system_id'=>     'element_id'=>      'control_type'=> ]
        $data=Yii::$app->request->post();

        //查询点位所操作的节点的点位信息 $config['point']为绑定的点位id
        $config=SubSystemElement::find()->select('config')
            ->where(['sub_system_id'=>$data['sub_system_id'],'element_id'=>$data['element_id']])
            ->asArray()->all();

        if(!empty($config)) {
            $point_id = json_decode($config[0]['config'], 1)['data']['points'];
//            echo '<pre>';
//            print_r($point_id);
//            die;
            $id=[];
            //把该节点绑定的多个点id加入数组内
            if(is_array($point_id))
                foreach($point_id as $key=>$value){
                    $id[]=$key;
                }
            else $id[]=$point_id;
//            $point_model=Point::findOne($point_id);
//            $point_model->value=$data['console_type'];
//            $result1=$point_model->save();
            $user = Yii::$app->user;
            //把所有的点发送一遍命令
            foreach ($id as $key=>$value) {
                $time= date('Y-m-d H:i:s',time());
//

                //判定点位类型

                //在command表里插入信息
                $command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
                $data['CommandLog'] = [
                    'point_id' => (int)$value,
                    'type' => 2,
                    'value' => $data['console_type'],
                    'timestamp' => $time,
                    'user_id' => $user->id,
                    'status' => 1
                ];
                $command_log->load($data);

                $command_log->save();
                $result=$command_log->errors;
            }


//            echo '<pre>';
//            print_r($command_log);
//            die;
        }
        else{
            $result=-1;

        }
        return json_encode($result);
    }

	//子系统删除
	public function actionAjaxDelete(){
		$data=Yii::$app->request->post();
		$model = new SubSystem();

//        echo '<pre>';
		print_r($data);
		$model = $model->findOne($data['id']);
		$model->delete();
	}
	//子系统保存
	public function actionAjaxSave(){
		$data=Yii::$app->request->post();

		$location_id=$data['location_id'];

		$sub_systems_data=json_decode($data['data'],1);
		$result=1;
		foreach ($sub_systems_data as $key=>$value) {

			$load_data['SubSystem'] = [
				'name' => MyFunc::createJson($value['name']),
				'data' => json_encode($value['data']),
				'sub_system_type' => $value['category_id'],
				'location_id' => $location_id

			];

			//id为0则为新建子系统
			if($value['id']==0) {
				$model = new SubSystem();
			}
			//否则为更新
			else {
				$model=SubSystem::findModel($value['id']);
			}
			$model->load($load_data);
			$result=$result && $model->save();
//          print_r($model->errors);
//            die;
		}
		//保存之后更新 要查询的点位
		$sub_system=ImageGroup::find()->where(['location_id'=>$location_id])->asArray()->all();
		$sub_system_data=self::getSubSystemPoint($sub_system);
		return json_encode(['result'=>$result,'sub_system_data'=>$sub_system_data]);

	}
	//属性框保存按钮  保存点位绑定信息
	//only one point Info
	public function actionAjaxSaveElement(){
		$data=Yii::$app->request->post();
		//for  table 点位绑定
//        $data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
		$load_data['SubSystemElement'] = [
			'sub_system_id'=>$data['sub_system_id'],
			'element_id'=>$data['element_id'],
			'element_type'=>1,
			'config'=>json_encode(
				[
					'data_type'=>'sub_system_element_config',
					'data'=>['points'=>$data['binding_id']]
				]
			)
		];
		$model=new SubSystemElement();
		$model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
		$model->load($load_data);
		$model->save();
		print_r($model->errors);
	}
	//属性框保存按钮  保存绑定信息(可以是一个点 或 多个点)
	//one or more  points Info
	public function actionAjaxSaveTableElement(){
		$data=Yii::$app->request->post();
		//for  table 点位绑定
		$data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
		$load_data['SubSystemElement'] = [
			'sub_system_id'=>$data['sub_system_id'],
			'element_id'=>$data['element_id'],
			'element_type'=>1,
			'config'=>json_encode(
				[
					'data_type'=>'sub_system_element_config',
					'data'=>[$data['type']=>$data['binding_id']]
				]
			)
		];
		$model=new SubSystemElement();
		$model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
		$model->load($load_data);
		$model->save();
		print_r($model->errors);
	}
	public function actionAjaxSaveFireEventElement(){
		$data=Yii::$app->request->post();
		$bingding_id=$data['binding_id'];
		$events=[];$error_data=0;
		//根据 point_id字段处理events为两个一组
		foreach ($bingding_id as $key=>$value) {
			$events[$value['point_id']][$value['id']]=$value['name'];
		}

		//检测每个point_id是否对应两个event
		// Y则绑定关系  N则返回输入点位出错
		foreach ($events as $key=>$value) {
			if(sizeof($value)!=2)
				$error_data=1;
			else{
				$name='';
				foreach ($value as $v_key=>$v_value) {
					$all_name=explode('-',$v_value);
					if($name=='') {
						$name = $all_name[sizeof($all_name) - 1];
						$events[$key]['name']=$name;
					}
				}

			}
		}

		//绑定sub_system_id element_id与event的关系
		//element_id刚好设置为对应的point_id 的值
		//并把point_id组成数组返回一边作为key生成图内的节点
		if($error_data==0) {
			$sub_system_node=[];
//            echo '<pre>';
//            print_r($events);
//            die;
			foreach ($events as $key => $value) {
				//
				$load_data['SubSystemElement'] = [
					'sub_system_id' => $data['sub_system_id'],
					'element_id' => $key,
					'element_type' => 2,
					'config' => json_encode(
						[
							'data_type' => 'sub_system_element_config',
							'data' => ['events' => $value]
						]
					)
				];
				$model = new SubSystemElement();
				$model = $model->findModelByData($data['sub_system_id'], $key);
				$model->load($load_data);
				$model->save();
				if (!empty($model->errors)) {
					print_r($model->errors);
					return 'error_save_data';
				}
				else {
					$sub_system_node['keys'][]=$key;
					$sub_system_node['data'][$key]=substr($value['name'],0,strlen($value['name'])-3).'-'.substr($value['name'],strlen($value['name'])-3,3);
				}
			}
			return json_encode($sub_system_node);
		}
		else return 'error_post_data';
	}

	public function actionAjaxSaveLocationPoints(){
		$param=Yii::$app->request->post();
		$model=Location::findOne($param['location_id']);

		$points=[
			'data_type'=>'descri1ption',
			'data'=>[
				'points'=>isset($param['points'])?$param['points']:null
			]
		];

		$model->points=json_encode($points);
		$model->save();
		$result=$model->errors;
		if(empty($result)) return 'success';
		else {
//            print_r($result);
			return 'error';
		}
	}
	//更新已经绑定的点位到location-points字段
	public function actionUpdateLocationPointRel(){
		$param=Yii::$app->request->post();
		//得到location对应点位的关系
		$location=Location::findOne($param['id']);
		//所有的本楼层的子系统
		$sub_system=ImageGroup::find()->where(['location_id'=>$param['id']])->asArray()->all();
		$sub_system_data=self::getSubSystemPoint($sub_system);
		//本图所包含的点位id信息
		$points=json_decode($location->points,1)['data'];
		$points=isset($points['points'])?$points['points']:[];
		//sub_system_data['points']为本图内已经绑定的点位id
		//现在子系统内的点位  统一存储到location表的points字段
		//所以需要把原绑定点位sub_system_data['points']加入到location的points字段
		foreach ($sub_system_data['points'] as $key=>$value) {
			$points[]=$value;
		}
		//存储过程--start
		if(!empty($points))
			$points=array_unique($points);
		$locationPoints= [
			'data_type'=>'descri1ption',
			'data'=>[
				'points'=>$points
			]
		];

		$location->points=json_encode($locationPoints);
		$location->save();
		//存储过程--end
		$result=$location->errors;
		//查询本图内地额所有点位信息
		if(empty($result)) {
			$points = Point::find()
				->select("id,name->'data'->>'zh-cn' as cn")
				->where(['id' => $points])
				->asArray()->all();
			return json_encode([        //TODO  在表内显示的字段需要修改-------暂定为(id,cn)

				'info'=>'success',
				'msg'=>$points
			]);
		}
		else return json_encode([
			'info'=>'error',
			'msg'=>$result
		]);
	}


	/*
    * find event by event_name
    * 更具事件名称查询事件
    * */
	public function actionFireEventSearch(){
		$post_data=Yii::$app->request->post();
		$point_event=new PointEvent();
		$data=$point_event->fuzzySearch($post_data['name'],['id','name','point_id','type']);
		$result=[];
		//更换key名称
		foreach($data as $key => $value) {
			$pointName = json_decode($value['name'], true);
			$result[] = [
				'pointId' => $value['id'],
				'pointName' => $pointName['data']['zh-cn'],
				'point_id' => $value['point_id'],
				'type' => $value['type']
			];
		}
		$result=json_encode($result);
		return  $result;
	}


    /*
 * find the point_event models are related the points,
 * and the points should be selected by the point's name
 * 通过名字查找 关联于点位的point_events
 * @param  String  post data
 * @return json[array]  array[];
 *
 * */
    public function actionPointEventSearch(){
        $post_data=Yii::$app->request->post();

        $point_event=new PointEvent();
        $data=$point_event->SearchEventsByPointNameSearch($post_data['name']);
        $result=[];
        //更换key名称
        foreach($data as $key => $value) {
            $pointName = json_decode($value['name'], true);
            $result[] = [
                'pointId' => $value['id'],
                'pointName' => $pointName['data']['zh-cn'],
                'point_id' => $value['point_id'],
                'type' => $value['type']
            ];
        }
        $result=json_encode($result);
        return  $result;
    }

    //获取表格数据
	public function actionGetData()
	{
		$protocol_id = Yii::$app->request->get('protocol_id');
		$type = Yii::$app->request->get('type');
		if($type == 'point'){
			$con = " is_shield = false ";
		}else{
			$con = " is_shield = true ";
		}

		if($protocol_id){
			$con .= " and protocol_id = $protocol_id ";
		}else{
			if($type == 'point'){
				$con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
			}
		}
		//$db = Yii::$app->db;
		//echo "<pre>";print_r($con);die;
		$table = 'core.point';
		$primaryKey = 'id';
		$columns = array(
			array(
				'db' => "id",
				'dt' => 'DT_RowId',
				'formatter' => function( $d, $row ) {
					return 'row_'.$d;
				},
				'dj' =>'id'
			),
			array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
			array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
		);
		$result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
		echo json_encode( $result);
	}

    //初始化sub_system表的points字段（直接运行此函数）
    public function actionUpdateSubsystemPoints(){
        $subsystems=SubSystem::find()->asArray()->all();
        foreach ($subsystems as $key=>$value) {
            $sub_system_data=self::getSubSystemPoint([$value]);
            $event_points=self::GetEventPoint($sub_system_data['events']);
            $points=array_merge($sub_system_data['points'],$event_points);
            $points=array_unique($points);
            $subsystem_points=json_encode([
                'data_type'=>'description',
                'data'=>[
                    'points'=>$points
                ]
            ]);
            $model=SubSystem::findOne($value['id']);
            $model->points=$subsystem_points;
            $model->save();
        }
    }


    //脚本功能
    //替换子系统内节点方法
    public function actionChange(){

        //A45 A29的视频子系统
        $systemids=[
            3988,3986,3984,3983,3982
        ];
//        $systemids=[61];
        //需要的节点
        $state=[
            'category'=>'state_many',
            'size'=>'75 75',
            'img'=>'/uploads/pic/A16_green.svg',
            'text'=>
                [
                    '1'=>'/uploads/pic/water_red_alarm.png',
                    '2'=>'/uploads/pic/water_green_alarm.png',
                    '3'=>'/uploads/pic/water_green_alarm.png',
                    '4'=>'/uploads/pic/water_green_alarm.png',

                ]
        ];
        $all_system=SubSystem::find()
            ->select('*')
            ->where(['id'=>$systemids])
            ->asArray()->all();

        foreach($all_system as $key=>$system){
            $system_id=$system['id'];
//            if(in_array($system_id,$systemids)){
            //得到子系统的data字段并把json格式分解为数组 重新拼装
            $data=json_decode($system['data'],1);
            $system_data=json_decode($data['data'],1);
            $nodeDataArray=$system_data['nodeDataArray'];

            foreach($nodeDataArray as $node_key=>$node_value){

                //替换要更新的节点  类型是 main
                if(isset($node_value['category']))
                    if($node_value['category']=='main') {

                        //如果是state_control节点则构建该节点数据
                        if (
                            $node_value['img'] == '/uploads/2016092668104f908b0adbb48c234f6a82038b6a.png'||
                            $node_value['img'] == '/uploads/201609268615cc260b2541bd307040bf699efc56.png'
                        ) {
//                                //size key  loc不变
//                                //category img 和 text需要改变
                            $node_value['category']='state';
                            $node_value['text'] = $state['text'];
//
                            $nodeDataArray[$node_key] = $node_value;
                        }
                    }

            }


//                把拼装处理好的data在还原为原来的json格式
            $system_data['nodeDataArray']=$nodeDataArray;
            $system_data=json_encode($system_data);
            $data['data']=$system_data;
            $system['data']=json_encode($data);
            //找到子系统模型把data字段写回去
            $model=SubSystem::findModel($system_id);
            $model->data=$system['data'];
            $result=$model->save();
//            }
        }
        die;

    }
}
