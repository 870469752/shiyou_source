<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\MailConfig $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Mail Config',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
