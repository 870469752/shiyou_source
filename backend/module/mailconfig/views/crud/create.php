<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\MailConfig $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Mail Config',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
