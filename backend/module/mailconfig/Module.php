<?php

namespace backend\module\mailconfig;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\mailconfig\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
