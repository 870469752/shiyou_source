<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 * @var yii\widgets\ActiveForm $form
 */
$this->title = '事件规则设置';
?>

<!-- widget grid -->
<section id="widget-grid" class="">

<!-- START ROW -->
<div class="row">
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false">
            <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                <h2><?= Html::encode($this->title) ?></h2>
            </header>

            <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                    <input class="form-control" type="text">
                </div>
                <!-- end widget edit box -->
                <!-- widget content -->
                <div class="widget-body" id="wid_one">
                    <?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                    <?= Html::hiddenInput('active', '_one_', ['id' => '_active'])?>
                    <fieldset>
                        <div id = '_ones_' >
                            <label>搜索事件</label>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <?= Html::textInput('event_condition', '',
                                            [
                                                'data-type' => 'select2',
                                                'id' => '_tags',
                                                'title' => '请输入关键字',
                                                'placeholder' => '请输入关键字'
                                            ]) ?>
                                        <div class="input-group-btn">
                                            <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('app', 'Search Point'), ['class' =>'btn btn-primary', 'id' => 'scre']) ?>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li id = '_clear'><a href="javascript:void(0);">Clear</a></li>
                                                <li><a href="javascript:void(0);">Cancel</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: 600px;overflow-y: scroll">
                            <?= GridView::widget([
                                'formatter' => ['class' => 'common\library\MyFormatter'],
                                'dataProvider' => $dataProvider,
                                'tableOptions' => [
                                    'class' => 'table table-striped table-bordered table-hover table-middle',
                                    'style' => 'margin-top:20px'
                                ],
                                'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                                'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                                'columns' => [
                                    [
                                        'class' => 'common\library\MyCheckboxColumn',
                                        'header' => Html::checkbox('_check', '', ['id' => '_check', 'label' => Yii::t('app', 'Invert Selected'), 'data-status']),
                                        'name' => 'id',
                                        'checkboxOptions' => ['class' => 'table_checkbox']
                                    ],
                                    ['attribute' => 'id', 'label' => Yii::t('app', 'id'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                    ['attribute' => 'name', 'label' => Yii::t('app', 'name'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                    ['attribute' => 'type', 'label' => Yii::t('app', 'type'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                    ['attribute' => 'is_valid', 'label' => Yii::t('app', 'is_valid'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                ],
                            ]); ?>
                        </div>
                        <!--{"data_type":"trigger","data":[{"trigger_type":"value","operator":">=","value":"9"}]}-->
                        <div class="form-actions">
                            <span id = 'error_tip_one' style="color:red;float:left"></span>
                            <a href="javascript:void(0)" id="to_Batch_event_rule" class="btn btn-success"> <?=Yii::t('app', 'Batch Event Rule')?></a>
                            <a href="javascript:void(0)" id="to_next" class="btn btn-success"> <?=Yii::t('app', 'To Next')?></a>
                        </div>
                </div>

                <!-- widget content -->
                <div class="widget-body" id="wid_two" style="display: none">

                    <div class="widget-body">
                        <div class = 'row'>
                            <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                <?= Html::textInput('delay', '', ['class' => 'form-control', 'id' => 'delay','title' =>  '升级延时（秒）', 'placeholder' => '升级延时（秒）']) ?>
                            </div>
                            <div class = 'col-sm-12 col-md-12 col-lg-10 sortable-grid ui-sortable form-group'>
                                <?= Html::textInput('remark', '', ['class' => 'form-control', 'id' => 'remark','title' =>  '报警信息', 'placeholder' => '报警信息']) ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel-group smart-accordion-default" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class=""> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> 报警规则排序</a></h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <?= Html::hiddenInput('rule', '', ['id' => 'rule']) ?>
                                    <div id = '_alarm_level'>
                                        <div class = 'panel panel-success col-md-5' style="height: 440px;padding: 0px" >
                                            <div class="panel-heading" style = 'text-align: center; padding-left: 0px;  '>报警列表</div>
                                            <table class="table">
                                                <thead><tr>
                                                    <?php foreach($data[0] as $key => $value):?>
                                                        <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                                    <?php endforeach;?>
                                                </tr></thead>
                                                <tbody id="sortable2" class="connectedSortable" style = 'border:0px'>
                                                <?php foreach($data as $key => $value):?>
                                                    <tr  draggable="true" class = '_level' data-id = <?=$value['Level']?>>
                                                        <!--给tr添加class-->
                                                        <?php foreach($value as $k => $v):?>
                                                            <!--循环输出 字段值-->
                                                            <td><?=$v?></td>
                                                        <?php endforeach;?>
                                                    </tr>
                                                <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class = 'col-md-1' style = 'font-size: 60px;text-align: center; height: 440px;line-height: 440px'>⇋</div>
                                        <!--副表格-->
                                        <div class = 'panel panel-info col-md-5'  style="height: 440px;padding: 0px">
                                            <div class="panel-heading" style = 'text-align: center;     '>规则序列</div>
                                            <table class="table table-hover" >
                                                <thead><tr>
                                                    <?php foreach($data[0] as $key => $value):?>
                                                        <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                                    <?php endforeach;?>
                                                </tr></thead>
                                                <tbody id="sortable1" class="connectedSortable">
                                                <tr id = 'Additional' data-id = 0><td colspan="4"></td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <span id = 'error_tip_two' style="color:red;float:left"></span>
                        <a href="javascript:void(0)" id="to_last" class="btn btn-success"> <?=Yii::t('app', 'To Last')?></a>
                        <a href="javascript:void(0)" id="gd" class="btn btn-success"> <?=Yii::t('app', 'Create')?></a>
                        <hr />
                    </div>
                </div>
                <!-- end widget content -->
                </fieldset>


                <?php MyActiveForm::end(); ?>
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->
</div>
<!-- end widget -->
</article>
<!-- END COL -->
</div>
</section>
<?php
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>
<script>
window.onload = function () {
    //获取查找类型的按钮参数
    var $_active = <?="'".$active."'"?>;
    //获取单点查找的条件数据
    var $event_condition = <?="'".$event_condition."'"?>;


    /*******************************************以 点位分类 查找 还是 单个点位搜索 * start **************************************/
    $('#_more_').click(function(){
        $('#_active').val('_more_');
        check_refresh();
        $('#_ones_').hide();
        $('#_mores_').show();
        $('#_tags').tagsinput('removeAll');
    });
    $('#_one_').click(function(){
        //给当前按钮设置data-tag
        $('#_active').val('_one_');
        check_refresh();
        $('#_ones_').show();
        $('#_mores_').hide();
        $('.select2-offscreen').select2('val','');
    })
    /*******************************************以 点位分类 查找 还是 单个点位搜索 * end **************************************/

        //当点击筛选时 清空window.name 内的数据
    $('#scre').click(function(){
        window.name = '';
    })

    //报警规则table-js
    c_sortable('sortable1', 'sortable2', 'rule');
    /****************************************   单点查找 input框事件 * start *******************************************/
    var elt = $("#_tags");
    //绑定 多选框事件
    elt.tagsinput({
        allowDuplicates: true,
        confirmKeys: [13]
    });

    /*******************************************返回到请求页面 并将参数 写入查询框*****************************************************************/
    $('#'+$_active).trigger('click');
    elt.tagsinput('add', $event_condition);

    /*******************************************搜索框清除 ***********************************************************/
    $('#_clear').click(function(){
        $('#_tags').tagsinput('removeAll');
    })

    /***************************************  给选中框 添加 window.name 或 本地保存 *************************************************************************/
    var $checkbox;
    check_refresh();
    //加载 选中checkbox
    function check_refresh(){
        var $check_status = 1;
        var $_check = $('#_check');
        $('tr .table_checkbox').each(function(){
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                $(this).attr('checked', true);
            }
            else
            {
                $check_status = 0;
                $(this).attr('checked', false);
            }
        })
        //判断当前页点位是否全部被选中，如果是则将反选选中，否则取选中
        $check_status ? $_check.prop('checked', true) : $_check.prop('checked', false);
    }

    /*******************************************给每个checkbox框 添加window.name*******************************************************/
    $('tr .table_checkbox').change(function (e) {
        if (this.checked == true) {
            window.name += ',' + $(this).val();
        }
        else {
            window.name = window.name.replace(','+$(this).val(), '');
        }
        $checkbox = window.name;
        console.log(window.name)
    })

    /*****************************************  select2    键盘事件 * start ***********************************************************************/
    /*ctrl + a 选中所有筛选后的*/
    $(document).on("keydown",".select2-input",function(event){
        var currKey=0,e=e||event;
        currKey=e.keyCode||e.which||e.charCode;
        if (!( String.fromCharCode(event.which).toLowerCase() == 'a' && event.ctrlKey) && !(event.which == 19))
        {
            return true;
        }
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");console.log(id)
        var element =$("#"+id);
        var selected = [];
        $('.select2-drop-active').find("ul.select2-results li").each(function(i,e){
            console.log($(e).data("select2-data"))
            selected.push($(e).data("select2-data"));
        });
        var _val = $('#alarmbatch-id').val();
        element.select2("data", selected);
        return false;
    });

    /*ctrl + d 移除所有*/
    $(document).on("keydown",".select2-input",function(event){
        var currKey=0,e=e||event;
        currKey=e.keyCode||e.which||e.charCode;
        if (!( String.fromCharCode(event.which).toLowerCase() == 'd' && event.ctrlKey) && !(event.which == 19))
        {
            return true;
        }
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");
        var element =$("#"+id);
        element.select2("data", []);
        return false;
    });

    /*****************************************  select2    键盘事件 * end ***********************************************************************/

    /*******************************************    全选 取消 * 按钮****************************************************************/

    function check_all()
    {
        $('tr .table_checkbox').each(function(){
            //判断如果不是选中而且点击全选 才会把前前 点位的id放入window.name
            if(!$(this).prop('checked'))
            {
                window.name += ',' + $(this).val() ;
            }
            $(this).prop('checked',true);
        });
        console.log(window.name);
    }

    function check_none()
    {
        $('tr .table_checkbox').each(function(){
            $(this).filter(':checkbox').prop('checked',false);
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                window.name = window.name.replace(','+$(this).val(), '');
            }
        })
        console.log(window.name);
    }

    $('#_check').click(function(){
        var $status = $(this).prop('checked');
        $status ? check_all() : check_none();
    })

    //事件列表
    $("#to_list").click(function(){
        //请求地址
        var url = '/system-public/alarm/list';
        window.location.href = url;
    })

    //下一步
    $("#to_next").click(function(){
        $("#wid_one").hide();
        $("#wid_two").show();
    })

    //上一步
    $("#to_last").click(function(){
        $("#wid_two").hide();
        $("#wid_one").show();
    })

    $("#to_Batch_event_rule").click(function(){
        check_refresh();
        //请求地址
        var url = '/system-public/alarm';
        window.name = '';
        window.location.href = url;
    })

    //创建按钮
    $("#gd").click(function(){
        check_refresh();
        //请求地址
        var url = '/system-public/alarm/quick-add-rule?';
        //传递 参数
        var $form = $('#w0').serializeArray();
        //console.log($form);return false;
        var $param = '';
        $($form).each(function(k, v){
            //移除 checkbox 不完整的 数据
            if(v.name.indexOf('id') != -1)
            {
                console.log(v.name+'---------');
            }
            else
            {
                $param += v.name + '=' + v.value+'&';
            }
            //在此  可以移除

        })
        $param +='ids' + '=' + window.name;
        window.name = '';
        window.location.href = url+$param;
    })
}
</script>
