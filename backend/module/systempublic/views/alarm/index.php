<?php

use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div class="no-padding">
                    <div class="padding-10" style="background: #fafafa">
                        <div>
                            <?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                            <?php foreach ($category_tree as $k => $tree): ?>
                                <div class="form-group field-category-id[<?= $k ?>][] required">
                                    <label class="control-label" for="field-category-id[<?= $k ?>][]"><?= MyFunc::DisposeJSON($tree['name']) ?></label>
                                    <select multiple class="select2" name="<?= $searchModel->formName() ?>[category_id][<?= $k ?>][]">
                                        <?= MyHtml::TreeOption(@$searchModel->id[$k], $tree['sub']) ?>
                                    </select>

                                    <div class="help-block"></div>
                                </div>
                            <?php endforeach ?>
                            <?= $form->field($searchModel, 'name')->textInput() ?>

                            <?= MyHtml::submitButton('筛选点位', ['class' => 'btn btn-success']) ?>

                            <?php MyActiveForm::end(); ?>
                        </div>
                        <div class="margin-top-10">
                            <a href="javascript:void(0)" id="go"
                               class="btn btn-default <?= isset($_COOKIE['point_id']) ? '' : 'disabled' ?>">查询所选点位数据</a>
                        </div>
                    </div>

                    <?= GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover table-middle'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [

                            [
                                'class' => 'common\library\MyCheckboxColumn',
                                'name' => 'point_id',
                                'checkboxOptions' => ['class' => 'table_checkbox']
                            ],
                            [
                                'class' => 'common\library\SelectColumn', 'item' => [
                                    '' => '默认',
                                    'line' => '折线图',
                                    'column' => '柱状图',
                                    'area' => '面积图',
                                    'step' => '脉冲图'
                                ],
                                'label' => '图形样式',
                                'selectOptions' => ['class' => 'table_select select2']
                            ],
                            ['class' => 'common\library\MyActionColumn', 'template' => '{condition}'],
                            ['attribute' => 'name', 'format' => 'JSON'],
                            ['attribute' => 'interval', 'format' => 'TimeUnitTransition', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'last_update', 'format' => 'datetime', 'headerOptions' => ['data-hide' => 'phone']],

                        ],
                    ]); ?>

                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerJsFile('js/my_js/point_default_index.js', 'backend\assets\AppAsset');
?>
<script>
    window.onload = function () {
        point_default_index('<?= Url::toRoute('/point/default/condition') ?>')
    }
</script>