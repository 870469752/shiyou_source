<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>

				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->
					<!-- widget content -->
					<div class="widget-body" id="wid_one">
						<?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                        <?= Html::hiddenInput('active', '_one_', ['id' => '_active'])?>
                        <fieldset>
                            <div id = '_ones_' >
                                <label>搜索点位</label>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <?= Html::textInput('point_condition', '',
                                                        [
                                                            'data-type' => 'select2',
                                                            'id' => '_tags',
                                                            'title' => '请输入关键字',
                                                            'placeholder' => '请输入关键字'
                                                        ]) ?>
                                                    <div class="input-group-btn">
                                                        <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('app', 'Search Point'), ['class' =>'btn btn-primary', 'id' => 'scre']) ?>
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                            <li id = '_clear'><a href="javascript:void(0);">Clear</a></li>
                                                            <li><a href="javascript:void(0);">Cancel</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            <div style="height: 600px;overflow-y: scroll">
                                <?= GridView::widget([
                                        'formatter' => ['class' => 'common\library\MyFormatter'],
                                        'dataProvider' => $dataProvider,
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered table-hover table-middle',
                                            'style' => 'margin-top:20px'
                                        ],
                                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                                        'columns' => [
                                            [
                                                'class' => 'common\library\MyCheckboxColumn',
                                                'header' => Html::checkbox('_check', '', ['id' => '_check', 'label' => Yii::t('app', 'Invert Selected'), 'data-status']),
                                                'name' => 'point_id',
                                                'checkboxOptions' => ['class' => 'table_checkbox']
                                            ],
                                            ['attribute' => 'name', 'label' => Yii::t('app', 'Point'), 'format' => 'JSON'],
                                            //触发 范围值
                                            ['attribute' => 'atomEvents', 'label' => Yii::t('app', 'Alarm Type'), 'format' => ['TriggerExtreme', 'trigger', 'trigger_type', 'System_alarm_event'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                            ['attribute' => 'atomEvents', 'label' => Yii::t('app', 'Operator'), 'format' => ['TriggerExtreme', 'trigger', 'operator', 'System_alarm_event'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                            ['attribute' => 'atomEvents', 'label' => Yii::t('app', 'Trigger Value'), 'format' => ['TriggerExtreme', 'trigger', 'value', 'System_alarm_event'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                            //触发规则
                                            ['attribute' => 'atomEvents', 'label' => Yii::t('app', 'Rule List'), 'format' =>  ['RuleExtreme', 'System_alarm_rule'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                        ],
                                    ]); ?>
                                </div>
                            <!--{"data_type":"trigger","data":[{"trigger_type":"value","operator":">=","value":"9"}]}-->
                            <div class="form-actions">
                                <span id = 'error_tip_one' style="color:red;float:left"></span>
                                <a href="javascript:void(0)" id="to_create_rule" class="btn btn-success"> <?=Yii::t('app', 'Event Create Rule')?></a>
                                <a href="javascript:void(0)" id="to_list" class="btn btn-success"> <?=Yii::t('app', 'Event List')?></a>
                                <a href="javascript:void(0)" id="to_next" class="btn btn-success"> <?=Yii::t('app', 'To Next')?></a>
                            </div>
                    </div>

                    <!-- widget content -->
                    <div class="widget-body" id="wid_two" style="display: none">
                        <div class = 'row'>
                            <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                <?=Html::dropDownList('trigger_type', '', ['value' => '   单值报警   ','range' => '   区间报警   '], ['class' => 'select2', 'placeholder' => ''])?>
                            </div>
                            <div id="single_alarm">
                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?=Html::dropDownList('operator', '', ['>' => '   大于   ','<' => '   小于   ','=' => '   等于   ','!=' => '  不等于  ','>=' => '  大于等于  ','<=' => '  小于等于  '], ['class' => 'select2', 'placeholder' => ''])?>
                                </div>

                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?= Html::textInput('value', '', ['class' => 'form-control', 'id' => 'value','title' =>  Yii::t('app', 'Single Value'), 'placeholder' => Yii::t('app', 'Single Value')]) ?>
                                </div>
                            </div>
                            <div id = "double_alarm" style="display: none">
                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?=Html::dropDownList('range_type', '', ['in' => '  介于区间内  ','no in' => '  介于区间外   '], ['class' => 'select2', 'placeholder' => ''])?>
                                </div>
                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?= Html::textInput('left_value', '', ['class' => 'form-control ', 'id' => 'left_value', 'title' => Yii::t('app', 'Left Value'), 'placeholder' => Yii::t('app', 'Left Value')]) ?>
                                </div>
                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?= Html::textInput('right_value', '', ['class' => 'form-control ', 'id' => 'right_value', 'title' => Yii::t('app', 'Right Value'), 'placeholder' => Yii::t('app', 'Right Value')]) ?>
                                </div>
                            </div>
                            <div id="val_pro" class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group' style="color: #ff0000; line-height: 30px">
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class = 'row'>
                                <div class = 'col-sm-12 col-md-12 col-lg-2 sortable-grid ui-sortable form-group'>
                                    <?= Html::textInput('delay', '', ['class' => 'form-control', 'id' => 'delay','title' =>  '升级延时（秒）', 'placeholder' => '升级延时（秒）']) ?>
                                </div>
                                <div class = 'col-sm-12 col-md-12 col-lg-10 sortable-grid ui-sortable form-group'>
                                    <?= Html::textInput('remark', '', ['class' => 'form-control', 'id' => 'remark','title' =>  '报警信息', 'placeholder' => '报警信息']) ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group smart-accordion-default" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class=""> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> 报警规则排序</a></h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <?= Html::hiddenInput('rule', '', ['id' => 'rule']) ?>
                                        <div id = '_alarm_level'>
                                            <div class = 'panel panel-success col-md-5' style="height: 440px;padding: 0px" >
                                                <div class="panel-heading" style = 'text-align: center; padding-left: 0px;  '>报警列表</div>
                                                <table class="table">
                                                    <thead><tr>
                                                        <?php foreach($data[0] as $key => $value):?>
                                                            <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                                        <?php endforeach;?>
                                                    </tr></thead>
                                                    <tbody id="sortable2" class="connectedSortable" style = 'border:0px'>
                                                    <?php foreach($data as $key => $value):?>
                                                        <tr  draggable="true" class = '_level' data-id = <?=$value['Level']?>>
                                                            <!--给tr添加class-->
                                                            <?php foreach($value as $k => $v):?>
                                                                <!--循环输出 字段值-->
                                                                <td><?=$v?></td>
                                                            <?php endforeach;?>
                                                        </tr>
                                                    <?php endforeach;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class = 'col-md-1' style = 'font-size: 60px;text-align: center; height: 440px;line-height: 440px'>⇋</div>
                                            <!--副表格-->
                                            <div class = 'panel panel-info col-md-5'  style="height: 440px;padding: 0px">
                                                <div class="panel-heading" style = 'text-align: center;     '>规则序列</div>
                                                <table class="table table-hover" >
                                                    <thead><tr>
                                                        <?php foreach($data[0] as $key => $value):?>
                                                            <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                                        <?php endforeach;?>
                                                    </tr></thead>
                                                    <tbody id="sortable1" class="connectedSortable">
                                                    <tr id = 'Additional' data-id = 0><td colspan="4"></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <span id = 'error_tip_two' style="color:red;float:left"></span>
                            <a href="javascript:void(0)" id="to_last" class="btn btn-success"> <?=Yii::t('app', 'To Last')?></a>
                            <a href="javascript:void(0)" id="gd" class="btn btn-success"> <?=Yii::t('app', 'Create')?></a>
                            <hr />
                        </div>
                    </div>
                    <!-- end widget content -->
						</fieldset>


                        <?php MyActiveForm::end(); ?>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>
<script>
window.onload = function () {
    //获取查找类型的按钮参数
    var $_active = <?="'".$active."'"?>;
    //获取单点查找的条件数据
    var $point_condition = <?="'".$point_condition."'"?>;
    //绑定点位
    var $binding

    /*******************************************报警处理 显示不同输入内容 * start **************************************/
    //判断不同警报类型 显示不同输入内容
    $("select[name='trigger_type']").change(function(){
        var trigger_type = $("select[name='trigger_type']").val();
        switch (trigger_type){
            case 'value':
                $("#single_alarm").show();
                $("#double_alarm").hide();
                break;
            case 'range':
                $("#double_alarm").show();
                $("#single_alarm").hide();
                break;
            default :
                $("#single_alarm").show();
                $("#double_alarm").hide();
                break;
        }
    })

    //单值输入框判定
    $("#value").blur(function(){
        var value = $("#value").val();
        if(isNaN(value)){
            $("#value").focus();
            $("#val_pro").text("< "+value+" >不是数字请重新填写！");
        }
        if(value==''){
            $("#value").focus();
            $("#val_pro").text("请填写数值！");
        }
        setTimeout("$('#val_pro').text('')",2000);
    })

    //左值输入框判定
    $("#left_value").blur(function(){
        var left_value = $("#left_value").val();
        if(isNaN(left_value)){
            $("#left_value").focus();
            $("#val_pro").text("< "+left_value+" >不是数字请重新填写！");
        }
        if(left_value==''){
            $("#left_value").focus();
            $("#val_pro").text("请填写最小值！");
        }
        setTimeout("$('#val_pro').text('')",2000);
    })

    //右值输入框判定
    $("#right_value").blur(function(){
        var right_value = $("#right_value").val();
        if(isNaN(right_value)){
            $("#right_value").focus();
            $("#val_pro").text("< "+right_value+" >不是数字请重新填写！");
        }
        if(right_value==''){
            $("#right_value").focus();
            $("#val_pro").text("请填写数值！");
        }
        setTimeout("$('#val_pro').text('')",2000);
    })

    /*******************************************以 点位分类 查找 还是 单个点位搜索 * start **************************************/
    $('#_more_').click(function(){
        $('#_active').val('_more_');
        check_refresh();
        $('#_ones_').hide();
        $('#_mores_').show();
        $('#_tags').tagsinput('removeAll');
    });
    $('#_one_').click(function(){
        //给当前按钮设置data-tag
        $('#_active').val('_one_');
        check_refresh();
        $('#_ones_').show();
        $('#_mores_').hide();
        $('.select2-offscreen').select2('val','');
    })
    /*******************************************以 点位分类 查找 还是 单个点位搜索 * end **************************************/

    //当点击筛选时 清空window.name 内的数据
    $('#scre').click(function(){
        window.name = '';
    })

    //报警规则table-js
    c_sortable('sortable1', 'sortable2', 'rule');
    /****************************************   单点查找 input框事件 * start *******************************************/
    var elt = $("#_tags");
    //绑定 多选框事件
    elt.tagsinput({
        allowDuplicates: true,
        confirmKeys: [13]
    });

    /*******************************************返回到请求页面 并将参数 写入查询框*****************************************************************/
    $('#'+$_active).trigger('click');
    elt.tagsinput('add', $point_condition);

    /*******************************************搜索框清除 ***********************************************************/
    $('#_clear').click(function(){
        $('#_tags').tagsinput('removeAll');
    })
    /****************************************   单点查找 input框事件 * end *******************************************/
//   var $is_temp = //?=$is_temp?><!--;-->

        //在点击提交时  将报警序列取出
//        $('.form-actions .btn').click(function () {
//            window.name = '';
//            var Sequence = '';
//            console.log('--------------------------------------------')
//            $('tbody ._level').each(function (){
//                Sequence += $(this).attr('xl') + ',';
//            })
//            $("input[name='AlarmBatch[level_sequence]']").val(Sequence);
//        })
    /***************************************  给选中框 添加 window.name 或 本地保存 *************************************************************************/
    var $checkbox;
    check_refresh();
    //加载 选中checkbox
    function check_refresh(){
        var $check_status = 1;
        var $_check = $('#_check');
        $('tr .table_checkbox').each(function(){
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                $(this).attr('checked', true);
            }
            else
            {
                $check_status = 0;
                $(this).attr('checked', false);
            }
        })
        //判断当前页点位是否全部被选中，如果是则将反选选中，否则取选中
        $check_status ? $_check.prop('checked', true) : $_check.prop('checked', false);
    }

    /*******************************************给每个checkbox框 添加window.name*******************************************************/
    $('tr .table_checkbox').change(function (e) {
        if (this.checked == true) {
            window.name += ',' + $(this).val();
        }
        else {
            window.name = window.name.replace(','+$(this).val(), '');
        }
        $checkbox = window.name;
        console.log(window.name)
    })

    /*****************************************  select2    键盘事件 * start ***********************************************************************/
    /*ctrl + a 选中所有筛选后的*/
    $(document).on("keydown",".select2-input",function(event){
        var currKey=0,e=e||event;
        currKey=e.keyCode||e.which||e.charCode;
        if (!( String.fromCharCode(event.which).toLowerCase() == 'a' && event.ctrlKey) && !(event.which == 19))
        {
            return true;
        }
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");console.log(id)
        var element =$("#"+id);
        var selected = [];
        $('.select2-drop-active').find("ul.select2-results li").each(function(i,e){
            console.log($(e).data("select2-data"))
            selected.push($(e).data("select2-data"));
        });
        var _val = $('#alarmbatch-point_id').val();
        element.select2("data", selected);
        return false;
    });

    /*ctrl + d 移除所有*/
    $(document).on("keydown",".select2-input",function(event){
        var currKey=0,e=e||event;
        currKey=e.keyCode||e.which||e.charCode;
        if (!( String.fromCharCode(event.which).toLowerCase() == 'd' && event.ctrlKey) && !(event.which == 19))
        {
            return true;
        }
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");
        var element =$("#"+id);
        element.select2("data", []);
        return false;
    });

    /*****************************************  select2    键盘事件 * end ***********************************************************************/

    /**************************************** 处理季节参数 点击事件*****************************************************/

    $('#_more').click(function () {
        console.log($(this).attr('class'))
        if($(this).attr('class') == 'btn btn-success')
        {
            $('#_more_option').show();
            $(this).attr('class', 'btn btn-info');
        }
        else
        {
            $('#_more_option').hide();
            $(this).attr('class', 'btn btn-success');
        }
    })

    /**********************************************  end 处理季节参数  ******************************************************************/

    /*******************************************    全选 取消 * 按钮****************************************************************/

    function check_all()
    {
        $('tr .table_checkbox').each(function(){
            //判断如果不是选中而且点击全选 才会把前前 点位的id放入window.name
            if(!$(this).prop('checked'))
            {
                window.name += ',' + $(this).val() ;
            }
            $(this).prop('checked',true);
        });
        console.log(window.name);
    }

    function check_none()
    {
        $('tr .table_checkbox').each(function(){
            $(this).filter(':checkbox').prop('checked',false);
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                window.name = window.name.replace(','+$(this).val(), '');
            }
        })
        console.log(window.name);
    }

    $('#_check').click(function(){
        var $status = $(this).prop('checked');
        $status ? check_all() : check_none();
    })

    /******************************** 判断是否 绑定点位 *****************************************************************/

    /** 时间插件 处理 ***/
    function hide_buttons() {
        $('.fc-header-right, .fc-header-center').hide();
        $('#calendar-buttons #btn-prev').hide();
        $('#calendar-buttons #btn-next').hide();
        $('#timepick').hide();
    }

    $('#mt').click(function () {
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('changeView', 'month');
        $('#calendar').fullCalendar('rerenderEvents');
        $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2013-08-01'));
        $("#time_type").val('month');
    });

    $('#ag').click(function () {
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('changeView', 'agendaWeek');
        $('#calendar').fullCalendar('rerenderEvents');
        $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2013-09-01'));
        $("#time_type").val('week');
    });

    $('#td').click(function () {
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('changeView', 'agendaDay');
        $('#calendar').fullCalendar('rerenderEvents');
        $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2013-08-01'));
        $("#time_type").val('day');
    });
    /*添加自定义按钮 点击事件*/
    $('#calendar-buttons #btn-prev').click(function () {
        $('#calendar').fullCalendar('prev');
        return false;
    });
    $('#calendar-buttons #btn-next').click(function () {
        $('#calendar').fullCalendar('next');
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });
    hide_buttons();


    /******************************************* 按钮 事件 *****************************************************/
    //创建按钮
    $("#gd").click(function(){
        check_refresh();
        var trigger_type = $("select[name='trigger_type']").val();
        if(trigger_type == 'range'){
            //var range_type = ("select[name='trigger_type']").val();
            var left_value = $("#left_value").val();
            var right_value = $("#right_value").val();
            if(left_value > right_value){
                $("#val_pro").text("左值大于右值，请重新填写！");
                $("#left_value").focus();
                setTimeout("$('#val_pro').text('')",5000);
                return false;
            }
            if(left_value == ''){
                $("#val_pro").text("请填写左值！");
                $("#left_value").focus();
                setTimeout("$('#val_pro').text('')",5000);
                return false;
            }
            if(right_value == ''){
                $("#val_pro").text("请填写右值！");
                $("#right_value").focus();
                setTimeout("$('#val_pro').text('')",5000);
                return false;
            }
        }else{
            var value = $("#value").val();
            if(value == ''){
                $("#val_pro").text("请填写数值！");
                $("#value").focus();
                setTimeout("$('#val_pro').text('')",5000);
                return false;
            }
        }
        //请求地址
        var url = '/system-public/alarm/quick-add-alarm?';
        //传递 参数
        var $form = $('#w0').serializeArray();
        //console.log($form);return false;
        var $Sequence = '';
        var $param = '';
        $($form).each(function(k, v){
            console.log(v.name + ':' + v.value)
            //移除 checkbox 不完整的 数据
            if(v.name.indexOf('point_id') != -1)
            {
                console.log(v.name+'---------');
            }
            else
            {
                $param += v.name + '=' + v.value+'&';
            }
            //在此  可以移除

        })
        $param +='point_ids' + '=' + window.name;
        window.name = '';
        console.log(url+$param);
        window.location.href = url+$param;
    })

    //事件列表
    $("#to_list").click(function(){
        //请求地址
        var url = '/system-public/alarm/list';
        window.location.href = url;
    })

    //进入事件创建规则列表
    $("#to_create_rule").click(function(){
        //请求地址
        var url = '/system-public/alarm/create-rule-index';
        window.location.href = url;
    })

    //下一步
    $("#to_next").click(function(){
        $("#wid_one").hide();
        $("#wid_two").show();
    })

    //上一步
    $("#to_last").click(function(){
        $("#wid_two").hide();
        $("#wid_one").show();
    })
}
</script>
