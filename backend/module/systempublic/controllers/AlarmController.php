<?php

namespace backend\module\systempublic\controllers;

use backend\models\AlarmRule;
use backend\models\search\PointEventSearch;
use Yii;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\search\PointSearch;
use backend\models\search\EventSearch;
use backend\models\AlarmLevel;
use backend\models\AlarmEvent;
use backend\models\PointEvent;
/**
 * CrudController implements the CRUD actions for AlarmBatch model.
 * 此方法 是对 生成报警的 简化操作
 */
class AlarmController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AlarmBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataOfGet = Yii::$app->request->getQueryParams();

        /********** 前台页面需要  查找类型与单点查找天的参数 第一次访问时没有任何参数的 需要对其进行处理     ****************/
        $dataOfGet['point_condition'] = isset($dataOfGet['point_condition']) ? $dataOfGet['point_condition'] : '';
        $dataOfGet['active'] = isset($dataOfGet['active']) ? $dataOfGet['active'] : '_more_';
        $searchModel = new PointEventSearch();
        $dataProvider = $searchModel->_search($dataOfGet);
        //获取 报警 信息
        $data = AlarmLevel::findAllLevel();
        $count = count($data);
        return $this->render('create',[
                 'searchModel' => $searchModel,
                 'dataProvider' => $dataProvider,
                 'point_condition' => $dataOfGet['point_condition'],
                 'data' => $data,
                 'count' => $count,
                 'active' => $dataOfGet['active']
            ]);
    }

    /**
     * Lists all AlarmBatch models.
     * @return mixed
     */
    public function actionCreateRuleIndex()
    {
        $dataOfGet = Yii::$app->request->getQueryParams();

        /********** 前台页面需要  查找类型与单点查找天的参数 第一次访问时没有任何参数的 需要对其进行处理     ****************/
        $dataOfGet['event_condition'] = isset($dataOfGet['event_condition']) ? $dataOfGet['event_condition'] : '';
        $dataOfGet['active'] = isset($dataOfGet['active']) ? $dataOfGet['active'] : '_more_';
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->_searchList($dataOfGet);
        //获取 报警 信息
        $data = AlarmLevel::findAllLevel();
        $count = count($data);
        return $this->render('create_rule',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'event_condition' => $dataOfGet['event_condition'],
            'data' => $data,
            'count' => $count,
            'active' => $dataOfGet['active']
        ]);
    }


    /**
     * 此方法 是快速添加报警的方法 后台需要自动生成的数据包括： 点位报警事件 复合报警事件(于温度点位) 点位报警规则
     * 分为两种情况：
     *              1、利用分类进行批量添加
     *              2、筛选点位
     */
    public function actionQuickAddAlarm()
    {
        /*操作日志*/
        $op['zh'] = '创建点位报警';
        $op['en'] = 'Modify the level alarm Settings';
        $this->getLogOperation($op,'','');

        $param_arr = Yii::$app->request->getQueryParams();
        $rule = trim($param_arr['rule'], ',');
        $trigger_type = $param_arr['trigger_type'];//过滤类型
        $operator = '';//操作符
        $value = '';//过滤值
        if($param_arr['trigger_type'] == 'value'){
            $operator = $param_arr['operator'];
            $value = '['.$param_arr['value'].']';
        }else{
            $operator = $param_arr['range_type'];
            $value = '['.$param_arr['left_value'].','.$param_arr['right_value'].']';
        }

        //判断是否 有选中 点位
        if($param_arr['point_ids'])
        {
            //将window.name 中的点位字符串  转为 数组
            $point_id_arr = explode(',', trim($param_arr['point_ids'], ','));
            //遍历 点位数组 一一生成 点位的系统事件 和 规则
            foreach($point_id_arr as $k => $v)
            {
                //实例化 事件对象
                $atom_event = new PointEvent();
                $atom_event -> load([
                        'name' => MyFunc::setSystemName('alarm', 'event' ,$v),
                        'trigger' => $trigger_type.';'.$operator.';'.$value,
                        'remark' => $param_arr['remark'],
                        'delay' => $param_arr['delay'],
                        'rule' => $rule,
                        'type' => '1',
                        'is_system' => true,
                        'point_id' => $v,

                    ], '');
                 $atom_event -> isTemperatureEvent(false);
            }
        }
      $this->redirect('/system-public/alarm/');
    }

    /**
     * 此方法 是快速添加事件的报警规则
     */
    public function actionQuickAddRule()
    {
        /*操作日志*/
        $op['zh'] = '创建事件规则';
        $op['en'] = ' the Create Event rule';
        $this->getLogOperation($op,'','');
        $param_arr = Yii::$app->request->getQueryParams();
        //echo "<pre>";print_r($param_arr);exit;
        $ids = explode(',', trim($param_arr['ids'], ','));

        //判断是否 有选中 点位
        if(!empty($ids))
        {
            //遍历 点位数组 一一生成 点位的系统事件 和 规则
            foreach($ids as $k => $v)
            {
                //报警规则名称
                $name = MyFunc::setSystemName('alarm', 'rule' , $v);
                $description = '';
                //报文 设置为 当前事件id发生报警
                $alarm_rule = AlarmRule::getRuleObjBySystem($name, $description, $v,$param_arr['delay'], trim($param_arr['rule'], ','), $param_arr['remark']);
                $alarm_rule->bindingRuleEvent(false);
            }
        }

        $this->redirect('/system-public/alarm/create-rule-index');
    }

    public function actionList()
    {
        $dataOfGet = Yii::$app->request->getQueryParams();

        /********** 前台页面需要  查找类型与单点查找天的参数 第一次访问时没有任何参数的 需要对其进行处理     ****************/
        $dataOfGet['point_condition'] = isset($dataOfGet['point_condition']) ? $dataOfGet['point_condition'] : '';
        $dataOfGet['active'] = isset($dataOfGet['active']) ? $dataOfGet['active'] : '_more_';
        $searchModel = new PointEventSearch();

        $dataProvider = $searchModel->_searchList($dataOfGet);
        //获取 报警 信息
        $data = AlarmLevel::findAllLevel();
        $count = count($data);

        return $this->render('list',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'point_condition' => $dataOfGet['point_condition'],
            'data' => $data,
            'count' => $count,
            'active' => $dataOfGet['active']
        ]);
    }

    public function actionQuickDeleteAlarm()
    {
        /*操作日志*/
        $op['zh'] = '删除点位报警';
        $op['en'] = 'Delete the level alarm';
        $this->getLogOperation($op,'','');

        $param_arr = Yii::$app->request->getQueryParams();

        //判断是否 有选中 点位
        if($param_arr['point_ids'])
        {
            //将window.name 中的点位字符串  转为 数组
            $point_id_arr = explode(',', trim($param_arr['point_ids'], ','));
            //遍历 点位数组 一一生成 点位的系统事件 和 规则
            foreach($point_id_arr as $k => $v)
            {
                $this->actionDeleteEvent($v);
                $this->actionDeleteEventRule($v);
            }
        }
        $this->redirect('/system-public/alarm/list');
    }

    /**
     * Deletes an existing AlarmEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteEvent($id)
    {
        /*操作日志*/
        $op['zh'] = '删除报警事件'.$id;
        $op['en'] = 'Delete alarm event'.$id;
        $this->getLogOperation($op);

        $type = 1;
        $this->findEventModel($id, $type)->delete();

    }

    /**
     * Finds the AlarmEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AlarmEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventModel($id, $type)
    {
        if (($model = AlarmEvent::findOne($id)) !== null) {
            if($type == 1){
                $model = PointEvent::findOne($id);
            }
            else if($type == 2){
                $model = CompoundEvent::findOne($id);
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing AlarmEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteEventRule($id)
    {
        /*操作日志*/
        $op['zh'] = '删除报警事件规则'.$id;
        $op['en'] = 'Delete alarm event rule'.$id;
        $this->getLogOperation($op);

        $this->findAlarmRuleModel($id)->delete();
    }

    /**
     * Finds the AlarmEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AlarmEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAlarmRuleModel($id)
    {

        if (($model = AlarmRule::findOneByEventId($id)) !== null) {
            $model = AlarmRule::findOneByEventId($id);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
