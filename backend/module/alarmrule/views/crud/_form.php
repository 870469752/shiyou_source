<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\AlarmLevel;
use backend\models\AlarmEvent;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmRule $model
 * @var yii\widgets\ActiveForm $form
 */
//获取 报警等级的 数据
$data = AlarmLevel::findAllLevel();
$count = count($data);
?>
<style>
    tr:hover {
        cursor: move;
    }
    </style>
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <?= $form->field($model, 'name')->textInput() ?>

                        <?= $form->field($model, 'description')->textInput() ?>

                        <?= $form->field($model, 'event_id')->dropDownList(
                            MyFunc::_map(AlarmEvent::getAllEvent(), 'id', 'name'),
                            ['class' => 'select2', 'style' => 'width:200px', 'placeholder' => '请选择事件ID']
                        ); ?>

						<?= $form->field($model, 'delay')->dropDownList([
                                '300'=>'5 '.Yii::t('app', 'Minute'),
                                '600'=>'10 '.Yii::t('app', 'Minute'),
                                '1800'=>'30 '.Yii::t('app', 'Minute'),
                                '3600'=>'1 '.Yii::t('app', 'Hour'),
                                '18000'=>'5 '.Yii::t('app', 'Hour'),
                                '43200'=>'12 '.Yii::t('app', 'Hour'),
                                '86400'=>'24 '.Yii::t('app', 'Hour'),
                            ],['class' => 'select2', 'placeholder' => '请选择升级时间']) ?>

                        <?= $form->field($model, 'level_sequence')->hiddenInput()?>

                        <hr class="bs-docs-separator">
                        <div id = '_alarm_level'>
                            <div class = 'panel panel-info col-md-5' style = 'border:2px solid #808080;'>
                                <div class="panel-heading" style = 'text-align: center '>规则序列</div>
                                <table class="table table-hover" >
                                    <thead><tr>
                                        <?php foreach($data[0] as $key => $value):?>
                                            <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                        <?php endforeach;?>
                                    </tr></thead>
                                    <tbody id="sortable1" class="connectedSortable">
                                    <tr id = 'Additional' data-id = 0 ><td colspan="4"></td></tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class = 'col-md-1'> <span style = 'font-size: 60px;text-align: center'>⇋</span></div>
                            <!--主表格-->
                            <div class = 'panel panel-success col-md-5' style = 'border:2px solid #808080;'>
                                <div class="panel-heading" style = 'text-align: center '>报警列表</div>
                                <table class="table">
                                    <thead><tr>
                                        <?php foreach($data[0] as $key => $value):?>
                                            <th><?=Yii::t('app', ucwords(str_replace('_', ' ', $key)))?></th>
                                        <?php endforeach;?>
                                    </tr></thead>
                                    <tbody id="sortable2" class="connectedSortable" style = 'border:0px'>
                                    <?php foreach($data as $key => $value):?>
                                        <tr  draggable="true" class = '_level' data-id = <?=$value['Level']?>>
                                            <!--给tr添加class-->
                                            <?php foreach($value as $k => $v):?>
                                                <!--循环输出 字段值-->
                                                <td><?=$v?></td>
                                            <?php endforeach;?>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>

                            </div>

                        </div>
						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<script>
    window.onload = function(){
        c_sortable('sortable1', 'sortable2', 'alarmrule-level_sequence');
    }
    </script>
