<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmRule $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Alarm Rule',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
