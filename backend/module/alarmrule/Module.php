<?php

namespace backend\module\alarmrule;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\alarmrule\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
