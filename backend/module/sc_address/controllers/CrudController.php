<?php

namespace backend\module\sc_address\controllers;

use Yii;
use backend\models\ScAddress;
use backend\models\search\ScAddressSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OperateLog;
/**
 * CrudController implements the CRUD actions for ScAddress model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScAddressSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single ScAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScAddress;

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建SC地址';
            $op['en'] = 'Create SC address';
            $this->getLogOperation($op,$info,$result);

            $get = Yii::$app->request->get();
            unset($get['id']);
            return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ScAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改SC地址';
            $op['en'] = 'Update SC address';
            $this->getLogOperation($op,$info,$result);

            $get = Yii::$app->request->get();
            unset($get['id']);
            return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ScAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除SC地址';
        $op['en'] = 'Delete SC address';
        $this->getLogOperation($op,'','');

        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the ScAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
