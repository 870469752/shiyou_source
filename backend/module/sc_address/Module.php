<?php

namespace backend\module\sc_address;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\sc_address\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
