<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ScAddress $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Sc Address',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
