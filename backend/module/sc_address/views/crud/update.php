<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ScAddress $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Sc Address',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
