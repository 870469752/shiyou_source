<?php

namespace backend\module\db_log;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\db_log\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
