<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DatabaseLog $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Database Log',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
