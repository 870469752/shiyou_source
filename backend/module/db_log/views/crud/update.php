<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DatabaseLog $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Database Log',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
