<?php

namespace backend\module\systempublic;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\systempublic\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
