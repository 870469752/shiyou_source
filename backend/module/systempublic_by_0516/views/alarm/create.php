<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Alarm Batch',
]);
?>
    <?= $this->render('_form', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'point_condition' =>$point_condition,
        'data' => $data,
        'count' => $count,
        'active' => $active
    ]) ?>
