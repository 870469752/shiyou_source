<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 * @var yii\widgets\ActiveForm $form
 */
$this->title = 'Delete the level alarm';
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->
                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                        <fieldset>
                            <div id = '_ones_' >
                                <label>搜索点位</label>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <?= Html::textInput('point_condition', '',
                                                [
                                                    'data-type' => 'select2',
                                                    'id' => '_tags',
                                                    'title' => '请输入关键字',
                                                    'placeholder' => '请输入关键字'
                                                ]) ?>
                                            <div class="input-group-btn">
                                                <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('app', 'Search Point'), ['class' =>'btn btn-primary', 'id' => 'scre']) ?>
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li id = '_clear'><a href="javascript:void(0);">Clear</a></li>
                                                    <li><a href="javascript:void(0);">Cancel</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 600px;overflow-y: scroll">
                                <?= GridView::widget([
                                    'formatter' => ['class' => 'common\library\MyFormatter'],
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered table-hover table-middle',
                                        'style' => 'margin-top:20px'
                                    ],
                                    'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                                    'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                                    'columns' => [
                                        [
                                            'class' => 'common\library\MyCheckboxColumn',
                                            'header' => Html::checkbox('_check', '', ['id' => '_check', 'label' => Yii::t('app', 'Invert Selected'), 'data-status']),
                                            'name' => 'point_id',
                                            'checkboxOptions' => ['class' => 'table_checkbox']
                                        ],
                                        ['attribute' => 'name', 'label' => Yii::t('app', 'Point'), 'format' => 'JSON'],
                                        //触发 范围值
                                        //['attribute' => 'id', 'label' => Yii::t('app', 'ID'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                        ['attribute' => 'trigger_type', 'label' => Yii::t('app', 'Alarm Type'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                        ['attribute' => 'operator', 'label' => Yii::t('app', 'Operator'),  'headerOptions' => ['data-hide' => 'phone,tablet']],
                                        ['attribute' => 'value', 'label' => Yii::t('app', 'Trigger Value'), 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                        ['attribute' => 'level_sequence', 'label' => Yii::t('app', 'Rule List'), 'format' => 'JSON'],
                                    ],
                                ]); ?>
                                </div>
                            <!--{"data_type":"trigger","data":[{"trigger_type":"value","operator":">=","value":"9"}]}-->
                            <div class="form-actions">
                                <span id = 'error_tip_one' style="color:red;float:left"></span>
                                <a href="javascript:void(0)" id="to_delete" class="btn btn-success"> <?=Yii::t('app', 'Delete')?></a>
                                <a href="javascript:void(0)" id="to_Batch_event_rule" class="btn btn-success"> <?=Yii::t('app', 'Batch Event Rule')?></a>
                            </div>
                    </div>

                    </fieldset>


                    <?php MyActiveForm::end(); ?>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->
    </div>
    <!-- end widget -->
    </article>
    <!-- END COL -->
    </div>
</section>
<?php
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function () {
        //获取查找类型的按钮参数
        var $_active = <?="'".$active."'"?>;
        //获取单点查找的条件数据
        var $point_condition = <?="'".$point_condition."'"?>;
        //绑定点位
        var $binding
        $('#_more_').click(function(){
            $('#_active').val('_more_');
            check_refresh();
            $('#_ones_').hide();
            $('#_mores_').show();
            $('#_tags').tagsinput('removeAll');
        });
        $('#_one_').click(function(){
            //给当前按钮设置data-tag
            $('#_active').val('_one_');
            check_refresh();
            $('#_ones_').show();
            $('#_mores_').hide();
            $('.select2-offscreen').select2('val','');
        })
        /*******************************************以 点位分类 查找 还是 单个点位搜索 * end **************************************/

            //当点击筛选时 清空window.name 内的数据
        $('#scre').click(function(){
            window.name = '';
        })

        //报警规则table-js
        c_sortable('sortable1', 'sortable2', 'rule');
        /****************************************   单点查找 input框事件 * start *******************************************/
        var elt = $("#_tags");
        //绑定 多选框事件
        elt.tagsinput({
            allowDuplicates: true,
            confirmKeys: [13]
        });

        /*******************************************返回到请求页面 并将参数 写入查询框*****************************************************************/
        $('#'+$_active).trigger('click');
        elt.tagsinput('add', $point_condition);

        /*******************************************搜索框清除 ***********************************************************/
        $('#_clear').click(function(){
            $('#_tags').tagsinput('removeAll');
        })
        /****************************************   单点查找 input框事件 * end *******************************************/

        /***************************************  给选中框 添加 window.name 或 本地保存 *************************************************************************/
        var $checkbox;
        check_refresh();
        //加载 选中checkbox
        function check_refresh(){
            var $check_status = 1;
            var $_check = $('#_check');
            $('tr .table_checkbox').each(function(){
                if((window.name+',').indexOf(','+$(this).val()+',') != -1)
                {
                    $(this).attr('checked', true);
                }
                else
                {
                    $check_status = 0;
                    $(this).attr('checked', false);
                }
            })
            //判断当前页点位是否全部被选中，如果是则将反选选中，否则取选中
            $check_status ? $_check.prop('checked', true) : $_check.prop('checked', false);
        }

        /*******************************************给每个checkbox框 添加window.name*******************************************************/
        $('tr .table_checkbox').change(function (e) {
            if (this.checked == true) {
                window.name += ',' + $(this).val();
            }
            else {
                window.name = window.name.replace(','+$(this).val(), '');
            }
            $checkbox = window.name;
            console.log(window.name)
        })

        /*****************************************  select2    键盘事件 * start ***********************************************************************/
        /*ctrl + a 选中所有筛选后的*/
        $(document).on("keydown",".select2-input",function(event){
            var currKey=0,e=e||event;
            currKey=e.keyCode||e.which||e.charCode;
            if (!( String.fromCharCode(event.which).toLowerCase() == 'a' && event.ctrlKey) && !(event.which == 19))
            {
                return true;
            }
            var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");console.log(id)
            var element =$("#"+id);
            var selected = [];
            $('.select2-drop-active').find("ul.select2-results li").each(function(i,e){
                console.log($(e).data("select2-data"))
                selected.push($(e).data("select2-data"));
            });
            var _val = $('#alarmbatch-point_id').val();
            element.select2("data", selected);
            return false;
        });

        /*ctrl + d 移除所有*/
        $(document).on("keydown",".select2-input",function(event){
            var currKey=0,e=e||event;
            currKey=e.keyCode||e.which||e.charCode;
            if (!( String.fromCharCode(event.which).toLowerCase() == 'd' && event.ctrlKey) && !(event.which == 19))
            {
                return true;
            }
            var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");
            var element =$("#"+id);
            element.select2("data", []);
            return false;
        });

        /*****************************************  select2    键盘事件 * end ***********************************************************************/

        /*******************************************    全选 取消 * 按钮****************************************************************/

        function check_all()
        {
            $('tr .table_checkbox').each(function(){
                //判断如果不是选中而且点击全选 才会把前前 点位的id放入window.name
                if(!$(this).prop('checked'))
                {
                    window.name += ',' + $(this).val() ;
                }
                $(this).prop('checked',true);
            });
            console.log(window.name);
        }

        function check_none()
        {
            $('tr .table_checkbox').each(function(){
                $(this).filter(':checkbox').prop('checked',false);
                if((window.name+',').indexOf(','+$(this).val()+',') != -1)
                {
                    window.name = window.name.replace(','+$(this).val(), '');
                }
            })
            console.log(window.name);
        }

        $('#_check').click(function(){
            var $status = $(this).prop('checked');
            $status ? check_all() : check_none();
        })
        /******************************************* 按钮 事件 *****************************************************/
        //删除按钮
        $("#to_delete").click(function(){
            check_refresh();
            //请求地址
            var url = '/system-public/alarm/quick-delete-alarm?';
            //传递 参数
            var $form = $('#w0').serializeArray();
            var $param = '';
            $($form).each(function(k, v){
                console.log(v.name + ':' + v.value)
                //移除 checkbox 不完整的 数据
                if(v.name.indexOf('point_id') != -1)
                {
                    console.log(v.name+'---------');
                }
                else
                {
                    $param += v.name + '=' + v.value+'&';
                }
                //在此  可以移除

            })

            $param +='point_ids' + '=' + window.name;
            window.name = '';
            //console.log($param);
            window.location.href = url+$param;
        })

        //删除按钮
        $("#to_Batch_event_rule").click(function(){
            check_refresh();
            //请求地址
            var url = '/system-public/alarm';
            window.name = '';
            window.location.href = url;
        })
    }
</script>
