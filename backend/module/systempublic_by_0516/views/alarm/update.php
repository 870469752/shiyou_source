<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Alarm Batch',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
