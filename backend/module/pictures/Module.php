<?php

namespace backend\module\pictures;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\pictures\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
