<?php

namespace backend\module\pictures\controllers;

use backend\models\Pictures;
use backend\models\PicturesGroup;
use backend\models\Point;
use common\library\MyArrayHelper;
use Yii;
use backend\models\PicturesCategory;
use backend\models\search\PicturesCategorySearch;
use backend\controllers\MyController;
use yii\db\QueryBuilder;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BatchCrudController implements the CRUD actions for PicturesCategory model.
 */
class BatchCrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PicturesCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PicturesCategorySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single PicturesCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = PicturesCategory::find()->where(['id' => $id])->with(['pictures' => function($query){
                $query->with('picturesGroup');
            }])->one();
        if (Yii::$app->request->isPost) {
            $upload_path = YII::$app->params['uploadPath'];
            $files = UploadedFile::getInstancesByName('pictures');
            foreach($files as $file){
                $path = $upload_path . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $pictures = new Pictures();
                $pictures->category_id = $id;
                $pictures->path = iconv('gbk', 'utf-8', $path);
                $pictures->save();
            }
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new PicturesCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PicturesCategory;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PicturesCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PicturesCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPicturesDelete()
    {
        $get = Yii::$app->request->get();
        Pictures::findOne($get['id'])->delete();

        return $this->redirect(['view', 'id' => $get['category_id']]);
    }

    public function actionCombination()
    {
        $request = Yii::$app->request;
        if($combination = $request->post()){ // 如果有了最大最小值的提交就保存数据库

            $model = new Pictures();
            $model->category_id = $combination['category_id'];
            $model->path = $combination['data'][0]['pictures_path'];
            $model->save();

            foreach($combination['data'] as $v){
                $pictures_group = new PicturesGroup();
                $pictures_group->attributes = $v;
                $pictures_group->pictures_id = $model->id;
                $pictures_group->save();
            }
            return $this->redirect(['view', 'id' => $model->category_id]);

        } else if($pictures = $request->get('pictures')){ // 如果有选择了图片就展示填写最大最小值页面
            $model = Pictures::find()->where(['id' => $pictures])->all();
            $point = MyArrayHelper::map(Point::baseSelect()->all(), 'id', 'name');
            $category = MyArrayHelper::map(PicturesCategory::find()->all(), 'id', 'name');
            return $this->render('combination', [
                'model' => $model,
                'point' => $point,
                'category' => $category
            ]);
        }else{ // 如果什么都没有就展示选择页面
            $model = PicturesCategory::find()->with(['pictures' => function($query){
                    $query->with('picturesGroup');
            }])->all();
            return $this->render('combination_select', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Finds the PicturesCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PicturesCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PicturesCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
