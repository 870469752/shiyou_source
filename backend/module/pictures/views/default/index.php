<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use yii\helpers\Url;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\InfoItemSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = '绘画';
?>

<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
                    <span class="widget-icon">
                        <i class="fa fa-image"></i>
                    </span>
                        <h2><?= Html::encode($this->title) ?></h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <div class="widget-body">
                            <ul class="list-inline" style="white-space:nowrap; overflow-x: scroll;">
                                <?php foreach($pictures as $v): ?>
                                    <li><img class="pictures" src="/<?= $v['path'] ?>" style="max-height: 100px"/></li>
                                <?php endforeach ?>
                            </ul>

                            <div id="canvas" style="height:600px; overflow:hidden; position: relative">
                            </div>
                        </div>
                    </div>
                </div>
        </article>
    </div>
</section>
<script>
    window.onload = function() {
        $(".pictures").click(function(){
            $('#canvas').append('<img class="item" style="position: absolute" src="'+$(this).attr('src')+'" />');
            $('.item').resizable().parent().draggable().append('<i class="fa fa-times fa-fw fa-lg cursor-pointer" style="position: absolute; top:0; right:0;"></i>')
        });

        $("#canvas").on('click', '.fa-times', function(){
            $(this).siblings('img').remove();
        });
    };
</script>