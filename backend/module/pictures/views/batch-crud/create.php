<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Pictures Category',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
