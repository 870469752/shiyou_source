<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;
use yii\helpers\Url;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 */

$this->title = $model->name;
?>
<!-- row -->
<div class="row">
    <div class="margin-bottom-10 col-xs-12">
    <?= MyHtml::beginForm('', 'post', ['enctype' => 'multipart/form-data', 'class' => 'form-inline']) ?>
        <div class="form-group">
            <input class="btn btn-file" name="pictures[]" type="file" multiple />
        </div>
        <button type="submit" class="btn btn-primary">批量上传</button>
        <?= Html::a('组合图片', ['combination'], ['class' => 'btn btn-success']) ?>
    <?= MyHtml::endForm() ?>
    </div>
    <!-- SuperBox -->
    <div class="superbox col-sm-12">
        <?php foreach($model->pictures as $v): ?>
            <div class="superbox-list">
                <img src="/<?= $v['path'] ?>"
                    data-img="/<?= $v['path'] ?>"
                    alt="<?php if(empty($v->picturesGroup)): ?>单张图片<?php else: ?>图组图片<?php endif ?>"
                    title="图片信息"
                    class="superbox-img img-thumbnail"
                    data-del="<?= Url::toRoute(['pictures-delete', 'id' => $v['id'], 'category_id' => Yii::$app->request->get('id')]) ?>"
                    >
            </div>
        <?php endforeach ?>

        <div class="superbox-float"></div>
    </div>
    <!-- /SuperBox -->

    <div class="superbox-show" style="height:300px; display: none"></div>

</div>

<?php
$this->registerJsFile("js/plugin/superbox/superbox.min.js", ['yii\web\JqueryAsset']);
?>

<script type="text/javascript">

    window.onload = function() {
        // PAGE RELATED SCRIPTS
        $('.superbox').SuperBox('');

    };

</script>
