<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Pictures Category',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
