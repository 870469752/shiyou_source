<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 * @var yii\widgets\ActiveForm $form
 */
$this->title = '图片组合';
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?= MyHtml::beginForm() ?>
                        <label class="control-label">选择图片分类</label>
                        <?= MyHtml::dropDownList('category_id', null, $category, ['class' => 'select2 margin-bottom-10']) ?>

                        <fieldset>
                            <?php foreach($model as $k => $v): ?>
                                <legend></legend>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="control-label">选择点位</label>
                                        <?= MyHtml::dropDownList('data['.$k.'][point_id]', null, $point, ['class' => 'select2 margin-bottom-10']) ?>
                                        <label class="control-label">最大值</label>
                                        <input type="text" name="data[<?= $k ?>][max]" class="form-control margin-bottom-10">
                                        <label class="control-label">最小值</label>
                                        <input type="text" name="data[<?= $k ?>][min]" class="form-control">
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">显示图片：</label>
                                        <img src="/<?= $v['path'] ?>" class="img-thumbnail">
                                        <input type="hidden" name="data[<?= $k ?>][pictures_path]" value="<?= $v['path'] ?>">
                                    </div>

                                </div>
                            <?php endforeach ?>

                        </fieldset>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">提交配置</button>
                        </div>

                        <?= MyHtml::endForm() ?>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>