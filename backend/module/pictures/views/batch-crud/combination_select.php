<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 */

$this->title = '图片组合';
?>
<!-- row -->
<div class="row">
    <!-- SuperBox -->
    <?= MyHtml::beginForm('', 'get') ?>
    <div class="col-sm-12 margin-bottom-10"><button type="submit" class="btn btn-primary">组合选中项</button></div>
    <div class="superbox col-sm-12">


        <?php foreach($model as $row): ?>
            <legend class="padding-5 margin-bottom-10"><i class="fa fa-image fa-fw"></i> <?= $row->name ?></legend>
            <?php foreach($row->pictures as $v): ?>
                <?php if(empty($v->picturesGroup)): ?>
                <div class="superbox-list">
                    <label>
                        <input type="checkbox" name="pictures[]" class="checkbox" value="<?= $v['id'] ?>">
                        <span></span>
                        <img src="/<?= $v['path'] ?>" class="superbox-img">
                    </label>
                </div>
                <?php endif ?>
            <?php endforeach ?>
        <?php endforeach ?>
    </div>
    <?= MyHtml::endForm() ?>

</div>
