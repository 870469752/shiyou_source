<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageGroup $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
						<fieldset>
						<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
                            <div style = 'width:50%'>
                                <?= $form->field($model, 'static_path')->fileInput(['class' => 'form-control input-lg', 'id' => 'static_image', 'placeholder' => Yii::t('image', 'Static image')]) ?>
                            </div>
                            <div style = 'width:50%'>
                                <?= $form->field($model, 'dynamic_path')->fileInput(['class' => 'form-control input-lg', 'id' => 'dynamic_image', 'placeholder' => Yii::t('image', 'Dynamic image')]) ?>
                            </div>
						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('image', 'Create') : Yii::t('image', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerJsFile("js/fileinput/fileinput.min.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/fileinput/fileinput.min.css", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function() {
        $("#static_image,#dynamic_image").fileinput({
            'uploadUrl': "/uploads/",
            'showUpload':false,
            'previewFileType':'any'
        });
    }
</script>