<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageGroup $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('image', 'Image Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('image', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('image', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('image', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'id',
            'name',
            'static_path',
            'dynamic_path',
        ],
    ]) ?>

</div>
