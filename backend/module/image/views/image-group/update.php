<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageGroup $model
 */

$this->title = Yii::t('image', 'Update {modelClass}: ', [
  'modelClass' => 'Image Group',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
