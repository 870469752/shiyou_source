<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageGroup $model
 */
$this->title = Yii::t('image', 'Create {modelClass}', [
  'modelClass' => 'Image Group',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
