<?php

use yii\helpers\Html;
use common\library\MyActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = MyActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id'=>"image_form"]); ?>
                        <fieldset id="fieldset">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
                            <?= $form->field($model, 'type')->dropDownList([
                                '0'=>Yii::t('app', 'Undefined_Image'),
                                '1'=>Yii::t('app', 'Point_Image'),
                                '2'=>Yii::t('app', 'Facility_Image'),
                                '3'=>Yii::t('app', 'Plan_Image'),
                                '4'=>Yii::t('app', 'Gif_Image'),
                            ],['class' => 'select2','id'=>'image_type', 'placeholder' => '请选择点位记录间隔']) ?>


                            <div style = 'width:100%' id="default_image">

                                <?= $form->field($model, 'default')->fileInput(['class' => 'form-control input-lg image', 'placeholder' => Yii::t('app', 'Image')]) ?>

                            </div>
                            <div style = 'width:50%; float: left;'id="active_image">

                                <?= $form->field($model, 'active')->fileInput(['class' => 'form-control input-lg image',    'placeholder' => Yii::t('app', 'Image')]) ?>

                            </div>
                            <div style = 'width:50%; float: left ;'id="inactive_image">

                                <?= $form->field($model, 'inactive')->fileInput(['class' => 'form-control input-lg image',   'placeholder' => Yii::t('app', 'Image')]) ?>

                            </div>
                            <div style = "width:100%;float: left;" id="add_button_area">
                                <input type="button" class="btn btn-primary" id="gif_add_element" value="add">
                            </div>
                            <input type="hidden" id="Image_num" name="Image[Gif_num]" value="0">
                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php MyActiveForm::end(); ?>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>


<?php
$this->registerJsFile("js/fileinput/fileinput.min.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/fileinput/fileinput.min.css", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function() {
        var Gif_count=1;
//        $("#active_image").hide();
//        $("#inactive_image").hide();
        $("#add_button_area").hide();
        init_image();
        function init_image(){
            $(".image").fileinput({
                'uploadUrl': "/uploads",
                'showUpload': false,
                'previewFileType': 'any'
            });
        }
        //添加gif图片元素 并初始化图片上传界面
        $("#gif_add_element").click(function(){
                //添加动态图组
                /*

                */
                $("#add_button_area").before(
                    '<div style="width:50%;float: left;>' +
                        '<div class="form-group field-image-inactive">' +
                            '<label class="control-label" for="Gif-Gif'+Gif_count+'">Gif' +Gif_count+
                            '</label> ' +
                            '<input type="hidden" name="Gif['+Gif_count+'limit]" value="">' +
                            '<input type="file" class="image" name="Image[Gif]['+Gif_count+']"/>'+
                            '<div class="help-block"> </div>'+
                        '</div>'+
                    '</div>'
                );

            Gif_count++;
            console.log(Gif_count);
            $("#Image_num").attr("value",Gif_count);
            init_image();
        })

        $("#image_type").change(function(){
            var selected_image_type=$('select[id=image_type] option:checked').val();

            switch (selected_image_type){
                case '0'://未定义图片

                    break;
                case '1'://点位图片

                    break;
                case '2'://设施图

                    break;
                case '3'://底图

                    break;
                case '4'://动态图
//                    $("#default_image").hide();
                    $("#add_button_area").show();
                    $("#gif_add_element").trigger("click");
                    break;

            }
        })
    }
</script>
