<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\PicturesCategory $model
 */
$this->title = Yii::t('app', 'Update Image', [
    'modelClass' => 'Image',
]);
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>