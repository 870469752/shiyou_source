<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageBase $model
 */
$this->title = Yii::t('image', 'Create {modelClass}', [
  'modelClass' => 'Image Base',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
