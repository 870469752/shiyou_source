<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ImageBase $model
 */

$this->title = Yii::t('image', 'Update {modelClass}: ', [
  'modelClass' => 'Image Base',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
