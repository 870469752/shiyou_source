<?php

namespace backend\module\image;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\image\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
