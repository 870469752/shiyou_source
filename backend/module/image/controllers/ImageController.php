<?php

namespace backend\module\image\controllers;
use backend\models\Image;
use yii;
use yii\web\UploadedFile;
use backend\controllers\MyController;
use backend\models\search\ImageSearch;

class ImageController extends MyController
{
    public function actionIndex()
    {
        $searchModel = new ImageSearch();
        $query_param=Yii::$app->request->getQueryParams();

        $dataProvider = $searchModel->search($query_param);

        return $this->render('index',[
            'dataProvider'=>$dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    public function actionCreate(){
            $model=new Image();
//        echo '<pre>';
//        print_r(Yii::$app->request->post());
//        die;

            if( $model->load($form_data=Yii::$app->request->post()) && $result = $model->DefaultSave()){

                return $this->redirect(['index']);
            } else {

                return $this->render('create', [
                    'model' => $model
                ]);
            }
    }
    /**
     * Deletes an existing SystemModule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除用户删除图片';
        $op['en'] = 'Delete users to delete image';
        $this->getLogOperation($op);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
   /*   图片管理只有create delete选项 暂时没有update选项 因为前期设计问题导致  改变图片链接后以前图形里保存的链接不可用

   */
    /**
     * Updates an existing SystemModule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($form_data = Yii::$app->request->post()) && $result =  $model->DefaultSave()) {
            /*操作日志*/
            $info = $form_data['SystemModule']['description'];
            $op['zh'] = '修改图片';
            $op['en'] = 'Update image';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $name = json_decode($model->name,1);
//            echo '<pre>';
//            print_r($model);
//            die;
            $model->name=$name['data']['zh-cn'];
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    protected function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
