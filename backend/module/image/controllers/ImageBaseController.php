<?php

namespace backend\module\image\controllers;

use Yii;
use backend\models\ImageBase;
use backend\models\search\ImageBaseSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\library\Tool\Upload;

/**
 * ImageBaseController implements the CRUD actions for ImageBase model.
 */
class ImageBaseController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImageBase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageBaseSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /*
     * 查看图片
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Creates a new ImageBase model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImageBase;
        if ($model->load(Yii::$app->request->post())) {
            $model->timestamp = date('Y-m-d H:i:s');
            $model->path = Upload::uploadByFile($model,'path');
            $result = $model->save();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '添加底图';
            $op['en'] = 'Create image base';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImageBase model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->path = Upload::uploadByFile($model,'path');
            $result = $model->save();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改底图';
            $op['en'] = 'Update image base';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ImageBase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImageBase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImageBase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImageBase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * set relation
     * @param integer $id
     *
     */
    public function actionSetRelation($id){

    }
}
