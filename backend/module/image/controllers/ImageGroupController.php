<?php

namespace backend\module\image\controllers;

use Yii;
use backend\models\ImageGroup;
use backend\models\search\ImageGroupSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\Tool\Upload;
/**
 * ImageGroupController implements the CRUD actions for ImageGroup model.
 */
class ImageGroupController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImageGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageGroupSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /**
     * Creates a new ImageGroup model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImageGroup;

        if ($model->load(Yii::$app->request->post())) {
            $model->static_path = Upload::uploadByFile($model,'static_path');
            $model->dynamic_path = Upload::uploadByFile($model,'dynamic_path');
            $result = $model->save();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '添加组图';
            $op['en'] = 'Create image group';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImageGroup model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->static_path = Upload::uploadByFile($model,'static_path');
            $model->dynamic_path = Upload::uploadByFile($model,'dynamic_path');
            $result = $model->save();
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改组图';
            $op['en'] = 'Update image group';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ImageGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImageGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImageGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImageGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
