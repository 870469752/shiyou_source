<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\EventPoint $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Event Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
