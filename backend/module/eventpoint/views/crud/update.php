<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\EventPoint $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Event Point',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
