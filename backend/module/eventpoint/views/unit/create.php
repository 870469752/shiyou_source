<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Unit $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Unit',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
