<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Unit $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Unit',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
