<?php

namespace backend\module\eventpoint;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\eventpoint\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
