<?php

namespace backend\module\eventpoint\controllers;

use common\library\MyExport;
use common\library\MyFunc;
use Yii;
use backend\models\EventPoint;
use backend\models\search\EventPointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for EventPoint model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventPointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single EventPoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EventPoint;

        if ($model->load(Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $info = MyFunc::DisposeJSON($model->name);
            $op['zh'] = '添加事件点位';
            $op['en'] = 'Create event point';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EventPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $info = MyFunc::DisposeJSON($model->name) ;
            $op['zh'] = '修改事件点位';
            $op['en'] = 'Update event point';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EventPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '修改事件点位';
        $op['en'] = 'Update event point';
        @$this->getLogOperation($op,'','');
        return $this->redirect(['index']);
    }

    /**
     * Finds the EventPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
