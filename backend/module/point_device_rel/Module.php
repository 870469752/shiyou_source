<?php

namespace backend\module\point_device_rel;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\point_device_rel\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
