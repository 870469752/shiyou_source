<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\PointDeviceRel $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Point Device Rel',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
