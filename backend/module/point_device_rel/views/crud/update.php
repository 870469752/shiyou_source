<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\PointDeviceRel $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Point Device Rel',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
