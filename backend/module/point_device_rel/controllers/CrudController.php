<?php

namespace backend\module\point_device_rel\controllers;

use Yii;
use backend\models\PointDeviceRel;
use backend\models\search\PointDeviveRelSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Device;
use backend\models\ModbusController;
use backend\models\Unit;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;
use common\library\Ssp;
use backend\models\Collector;
use backend\models\BacnetController;

/**
 * CrudController implements the CRUD actions for PointDeviceRel model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PointDeviceRel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointDeviveRelSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public  function actionNewIndex()
    {

        $treeData=self::getDevTree();
        $treeCollectData=self::getCollectData();
        $unit=new Unit();
        $unit=BaseArrayHelper::map($unit->find()->orderBy('category_id asc, id asc')->asArray()->all(),'id','name');
//        echo '<pre>';
//        print_r($treeCollectData);
//        die;
         $unit=json_encode($unit);


        return $this->render('index_new', [
            'treeData'=>$treeData,
            'treeCollectData'=>$treeCollectData,
            'active'=>'bacnet_show',
            'unit' => $unit

        ]);
    }
    /**
     * Displays a single PointDeviceRel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PointDeviceRel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($device_id=null,$JsonData=null)
    {
        $data=json_decode($JsonData,1);
        $pointDeviceRel=PointDeviceRel::find()->where(['device_id'=>$device_id])->asArray()->all();
        $points=array();
        foreach($pointDeviceRel as $k=>$v) {
            $points[]=$v['point_id'];
        }
        if(!empty($data)){
            foreach($data as $k=>$v){
                $model = new PointDeviceRel;
                $model->device_id=$device_id;
                $model->point_id=$v;
                if(in_array($v,$points)){
                }else{
                    $model->save();
                }
            }
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['new-index']);
        }
    }

    /**
     * Updates an existing PointDeviceRel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PointDeviceRel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PointDeviceRel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PointDeviceRel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PointDeviceRel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //查询device
    public static function getDevTree()
    {
        $devices=Device::find()->select(['id','name'])->asArray()->all();
        $root=array(
            'name'=>'所有设备',
            'id'=>'root',
            'type'=>'root',
            'children'=>array(

            )
        );
        foreach ($devices as $key=>$value) {
            $child=array(
                'name'=>'',
                'id'=>'',
                'type'=>'device'
            );
            $name=json_decode($value['name'],1);
            $child['name']=$name['data']['zh-cn'];
            $child['id']=$value['id'];
            $root['children'][]=$child;
        }
        $treeData=array();
        $treeData[]=$root;
        return json_encode($treeData);
    }
    //查询collect
    public static function getCollectData_test()
    {
        $collector=Collector::find()->select(['eid','name'])->asArray()->all();
        $collector=MyFunc::map($collector,'eid','name');
        $controller=BacnetController::find()->select(['id','name','src_eid','object_instance_number'])->asArray()->all();
        $treeData=[];
        foreach ($collector as $key=>$value) {
            $treeData[$key]['name']=$value;
            $treeData[$key]['type']='all';
        }
        foreach ($controller as $key=>$value) {
            $value['name']=$value['object_instance_number'].'('.MyFunc::DisposeJSON($value['name']).')';
//            unset($value['name']);
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children'][]=$value;
        }
        return json_encode($treeData);
    }

    //查询collect
    public static function getCollectData()
    {
        $model=new Collector();
        $collector=Collector::find()->select(['eid','name'])->asArray()->all();
        $collector=MyFunc::map($collector,'eid','name');
        $controller_bacnet=BacnetController::find()->select(['id','name','src_eid','object_instance_number'])->asArray()->all();
        $controller_modbus=ModbusController::find()->select(['id','name','src_eid'])->asArray()->all();
        $controller_opc=$model->findBySql('select id,name,src_eid from protocol.opc_controller')->asArray()->all();
        $treeData=[];
        $controller_type=array(
            '0'=>array(
                "id"=>0,
                "name"=>'bacnet',
                "children"=>array()
            ),
            '1'=>array(
                "id"=>1,
                "name"=>'modbus',
                "children"=>array()
            ),
            '2'=>array(
                "id"=>2,
                "name"=>'opc',
                "children"=>array()
            ),
        );
        foreach ($collector as $key=>$value) {
            $treeData[$key]['name']=$value;
            $treeData[$key]['children']=$controller_type;
            $treeData[$key]['type']='all';
        }

        foreach ($controller_bacnet as $key=>$value) {
            $value['name']=$value['object_instance_number'].'('.MyFunc::DisposeJSON($value['name']).')';
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['0']['children'][]=$value;
        }
        foreach ($controller_modbus as $key=>$value) {
            $value['name']=MyFunc::DisposeJSON($value['name']);
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['1']['children'][]=$value;
        }
        foreach ($controller_opc as $key=>$value) {
            $value['name']=MyFunc::DisposeJSON($value['name']);
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['2']['children'][]=$value;
        }

        return json_encode($treeData);
    }
    //查询点位
    public function actionGetPointsDev($id)
    {
        $con = "x.device_id=$id and x.point_id=y.id ";
        $db = Yii::$app->db;

        $table = 'core.point_device_rel x ,core.point y';
        $primaryKey = 'id';
        $columns = array(
            array( 'db' => 'x.id as id', 'dt' => 'id', 'dj'=>"x.id"),
            array( 'db' => "y.name->'data'->>'zh-cn' as cn",  'dt' => "cn" ,'dj'=>"y.name->'data'->>'zh-cn'"),
            array( 'db' => 'x.point_id as point_id',   'dt' => 'point_id' ,'dj'=>"x.point_id" ),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        echo json_encode( $result);
    }

    //删除device里面的某个点位
    public function actionPointDelete($point_id,$device_id)
    {
        $model=PointDeviceRel::find()->where(['device_id'=>$device_id,'point_id'=>$point_id])->one();
        $model->delete();
        $result=$model->errors;
        if(!empty($result)) {
            echo '<pre>';
            print_r($result);
            die;
        }
        return $this->redirect(['index']);
    }
    //批量删除某个device里面的多个点位
    public function actionDeleteAll($device_id=null,$JsonData=null)
    {
        $data=json_decode($JsonData,1);
        if(!empty($data)){
            foreach($data as $k=>$v){
                $model = PointDeviceRel::find()->where(['device_id'=>$device_id,'point_id'=>$v])->one();
                $model->delete();
                $result=$model->errors;
                if(!empty($result)) {
                    echo '<pre>';
                    print_r($result);
                    die;
                }
            }
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['index']);
        }
    }
}
