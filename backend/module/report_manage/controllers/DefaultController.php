<?php

namespace backend\module\report_manage\controllers;

use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
