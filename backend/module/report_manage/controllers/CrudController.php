<?php

namespace backend\module\report_manage\controllers;

use backend\models\CommandLog;
use backend\models\EnergyCategory;
use backend\models\Image;
use backend\models\ImageGroup;
use backend\models\Location;
use backend\models\Menu;
use backend\models\Point;
use backend\models\PointCategoryRelNew;
use backend\models\Sub_System_Category;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use Yii;
use backend\models\Category;
use backend\models\search\CategorySearch;
use backend\controllers\MyController;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use yii\helpers\Url;
use backend\models\PointEvent;
use common\models\User;
use backend\models\ReportManage;
use yii\helpers\Html;
/**
 * Class CrudController
 * @package backend\module\category\controllers
 */
class CrudController extends MyController {
    /**
     * @var array 页面上存在的ID
     */
    //public $ids = [0];
    public $ids=[];
	public function behaviors() {
		return [
				'verbs' => [
						'class' => VerbFilter::className (),
						'actions' => [
								'delete' => [
										'post'
								]
						]
				]
		];
	}

	/**
	 * Lists all Category models.
	 *
	 * @return mixed
	 */


    public function actionIndex() {
        //当前登录用户的角色ID
       $user_roleId =  MyFunc::getRoleId();
        $reportmange=new ReportManage();
       // $user_roleId=0;
        $data=$reportmange->getReports($user_roleId);
        $tree = MyFunc::TreeData($data);
        //print_r($user_roleId);die;
        $role=$reportmange->findRole();
        return $this->render ( 'index', [
             'tree' => $tree,
             'role'=>$role,
             'role_id_temp'=>$user_roleId
        ] );
    }
    /**
     * zouxiaojie
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUser(){
        $data=Menu::find()->where('user_id_new is NULL')->asArray()->all();

        foreach ($data as $key=>$value) {
            $model=Menu::findOne($value['id']);
            $user_id=$model->user_id;
            $user_id_new='{'.$user_id.'}';
            $model->user_id_new=$user_id_new;
            $model->save();
        }

    }


    public function actionLocationShow() {

        $tree = MyFunc::TreeData(
            Location::find()
                ->select(['id','title'=>'name','parent_id'])
                ->OrderBy('id desc')
                ->asArray()->all()
        );
        return $this->render ( 'location_show', [
            'tree' => $tree
        ] );
    }

    public function actionLocationDetail($id) {
        $model=self::findLocationModel($id);
        $select_points=PointCategoryRelNew::find()->select('id')->where(['category_id'=>$id])->asArray()->all();
        $s_points=array();
        foreach ($select_points as $value) {
            $s_points[]=$value['id'];
        }
        //所有底图
        $image=Image::findAllImage();

        $image=MyFunc::_map($image,'id','name');

        //楼层绑定的底图
        $points=Point::findAllPoint();
        $points=MyFunc::_map($points, 'id', 'name');
        return $this->render ( 'location_update', [
                    'model'=>$model,
                    'points'=>$points,
                    's_points'=>$s_points,
                    'image'=>$image
        ] );
    }
    //$sub_system 为子系统数据组成的数组
    public function getSubSystemPoint($sub_system){
        //得到子系统内所有绑定的点位 组成数组
        $points=[];$sub_system_data=[];
        $camera_points=[];
        $keys=[];
        $point_keys=[];
        $camera_key=[];
        $events=[];
        $events_keys=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];

//            echo '<pre>';
//            print_r($data);
//            die;
            foreach ($data as $data_key=>$data_value) {
                //group 不存在category字段
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'state':
                    case 'state_event':
                    case 'state_defend':
                    case 'state_defend_1':
                    case 'state_defend_ctrl':
                    case 'gif':
                    case 'gif_value':
                    case 'control':
                    case 'state_value':
                    case 'value_1':
                    //得到 需要查询节点的key
                    if( $data_value['key']!=null) {
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                    }
//                    if($data_value['img']=="/uploads/pic/摄像头1.jpg")
                    //判断是不是摄像头的点
                    if(isset($data_value['img']))
                    if(
                        $data_value['img']=="/uploads/pic/摄像头1.jpg" || $data_value['img']=="/uploads/pic/摄像头2.jpg" ||
                        $data_value['img']=="/uploads/pic/摄像头3.jpg" || $data_value['img']=="/uploads/pic/摄像头4.jpg"||
                        $data_value['img']=="/uploads/pic/摄像头5.jpg"||$data_value['img']=="/uploads/pic/摄像头6.jpg"||
                        $data_value['img']=="/uploads/pic/摄像头7.jpg"||$data_value['img']=="/uploads/pic/摄像头8.jpg"
                      ) {
                        $camera_key[] =$data_value['key'];
                        }
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //根据子系统id 和key 找到需要查询的点位集合
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();
            $type='points';

            foreach ($configs as $key=>$configs_value) {
                $data=json_decode($configs_value['config'],1)['data'];
                if(isset($data['points'])){
                    $type='points';
                $points_info=$data['points'];
                }
                if(isset($data['events'])) {
                    $type='events';
                    $points_info = $data['events'];

                }
                if(isset($data['point_event'])) {
                    $type='point_event';
                    $points_info = $data['point_event'];

                }
//                echo '<pre>';
//                print_r($points_info);
//                die;
                //获取的data信息中  points 可能为点位id  可能为数组信息[id1=>name1,id2=>name2,...]
                if($points_info!="") {

                    if (!is_array($points_info)) {
                        $points[] = $points_info;
                        $point_keys[]=$configs_value['element_id'];
                        $sub_system_data[$system['id']][$configs_value['element_id']] = ['id' => $points_info];
                        if(!empty($camera_key)){
                            $camera_points[]= $points_info;
                        }
                    } else {
//                        echo '<pre>';
//                        print_r($points_info);

                        foreach ($points_info as $point_id => $point_name) {
                            if($type=='events' || $type=='point_event')  {
                                if($point_id!='name'){
                                    $events[]=$point_id;
                                    $events_keys[]=$configs_value['element_id'];
                                }
                            }
                            if($type=='points')  {$points[] = $point_id; $point_keys[]=$configs_value['element_id'];}
                                $sub_system_data[$system['id']][$configs_value['element_id']][] = ['id' => $point_id, 'name' => $point_name];

                        }

                    }
                }

            }
        }

		if(!empty($sub_system_data )){
			foreach($sub_system_data as $sub_system_id=>$sub_system){
				foreach($sub_system as $key=>$value){
							if(empty($value))
								unset($sub_system_data[$sub_system_id][$key]);
				}
			}
		}
        return [
            'data'=>$sub_system_data,
            'points'=>array_unique($points),'points_key'=>array_unique($point_keys),
            'camera_points'=>array_unique($camera_points),
            'events'=>array_unique($events),'events_key'=>array_unique($events_keys)
        ];
    }

    //copy sub_system with binding_id
    public function actionCopy(){
//        $location_ids=[];

        $sub_system_id=169;
        $new_sub_system_id=2957;
//         165 ---2955
//         167 ---2956
//         169 ---2957
          $subsystem_elemets=SubSystemElement::find()->where(['sub_system_id'=>$sub_system_id])->asArray()->all();
         //delete  from sub_system_element  where sub_system_id=$new)sub_system_id;
        SubSystemElement::deleteAll('sub_system_id=3955');
        //copy data from the $subsystem_elemets
        foreach ($subsystem_elemets as $key=>$value) {
            $model=new SubSystemElement();
            $data['SubSystemElement']=[
                'sub_system_id'=>$new_sub_system_id,
                'element_id'=>$value['element_id'],
                'element_type'=>$value['element_type'],
                'config'=>$value['config']
            ];
            $model->load($data);
            $model->save();
        }

//
    }
    /*
     * 更改menu表内 56号module_id 既subsystem/view
     *             61号       为 location-only-view
     *统一为 location-subsystem
     *
     *
     * */
    public function actionMenu(){

        $menu_ids=Menu::find()->select('id')->asArray()->all();
        foreach ($menu_ids as $key=>$id) {
            $menu=Menu::findModel($id['id']);
            //如果module_id=56
            // 把参数更改为对应的location_id
            // 把module_id更改为68
            if($menu->module_id==56){
                $parm=json_decode($menu->param,1);
                if(!empty($parm['data']['id'])){

                    $location_id=SubSystem::find()
                        ->select('location_id')
                        ->where(['id'=>$parm['data']['id']])
                        ->asArray()->all();
                    if($location_id!=null) {
                        $menu->module_id=68;
                        $parm['data']['id'] = $location_id[0]['location_id'];
                        $parm = json_encode($parm);
                        $menu->param = $parm;
                        $menu->save();
                    }
                }
            }
            if($menu->module_id==61){
                $menu->module_id=68;
                $menu->save();
            }

        }



    }

    //替换子系统内节点方法
    public function actionChange(){
        //A45 A29的视频子系统
        $systemids=[
         515
        ];
//        $systemids=[61];
        //需要的节点

        $state=[
            'category'=>'state',
            'size'=>'',
            'img'=>'/uploads/pic/on.svg',
            'text'=>
                [
                    '0'=>'/uploads/pic/state_red.svg',
                    '1'=>'/uploads/pic/state_green.svg',
                    '3'=>'/uploads/pic/state_black.svg'

                ],
            'key'=>"",
            'loc'=>""
        ];
//        $value=[
//            'category'=>'only_value'
//        ];
//        $all_system=SubSystem::find()->select('*')->where(['id'=>$systemids])->asArray()->all();
        $all_system=SubSystem::find()->select('*')->asArray()->all();
        foreach($all_system as $key=>$system){
            $system_id=$system['id'];
            if(in_array($system_id,$systemids)){
                //得到子系统的data字段并把json格式分解为数组 重新拼装
                $data=json_decode($system['data'],1);
                $system_data=json_decode($data['data'],1);
                $nodeDataArray=$system_data['nodeDataArray'];
//                echo '<pre>';
//                print_r($system_data);
//                die;
                foreach($nodeDataArray as $node_key=>$node_value){


                    //替换要更新的节点
                    if(isset($node_value['category']))
                        if($node_value['category']=='state_defend_1') {
//                            if(!isset($node_value['img'])) {
//                                $nodeDataArray[$node_key]['text'] =$state['text'];
//                            }
                            $nodeDataArray[$node_key]['category']='state_defend_ctrl';
//                                echo '<pre>';
//                                print_r( $nodeDataArray[$node_key]);

                        }


                }



//                把拼装处理好的data在还原为原来的json格式
                $system_data['nodeDataArray']=$nodeDataArray;
                $system_data=json_encode($system_data);
                $data['data']=$system_data;
                $system['data']=json_encode($data);
                //找到子系统模型把data字段写回去
                $model=SubSystem::findModel($system_id);
                $model->data=$system['data'];
                $result=$model->save();
            }
        }
        die;

    }

    public function actionReplace(){
        $id=990 ;
//        $id=989;
//        得到楼层id
       $sub_system=SubSystem::find()->select('id')->where(['location_id'=>$id])->asArray()->all();
        $sub_system_one=SubSystem::findOne($sub_system[0]);
        $data=$sub_system_one->data;
        $ids=[
            990,987,984,981,980,979,978,977,976,975,974,
            1070,1068,1066,1064,1063,1062,1061,1060,1059,1058,1057,
            1084,1082,1080,1078,1077,1076,1075,1074,1073,1072,1071,
            1098,1096,1094,1093,1092,1091,1090,1089,1088,1087,1086,1085
        ];
//        $ids=[
//                989,988,986,985,983,982,
//                1069,1067,1065,
//                1083,1081,1079,
//                1097,1095,1093
//        ];
        echo '<pre>';
        foreach ($ids  as $key=>$id_one) {
            $sub=SubSystem::find()->select('id')->where(['location_id'=>$id_one])->asArray()->all();
            if(sizeof($sub)!=1) {
                print_r($id_one);
                print_r("   ");
            }
            print_r($sub);
            $model=SubSystem::findOne($sub[0]['id']);
            $model->data=$data;
            $model->save();
        }
//        die;

    }


    public function actionLocationView($id) {

//        echo '<pre>';
//        $start_time=microtime(true);


        $sub_system_model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();

        $sub_system_in=$all_sub_system;
        //TODO
       //系统内容为空 会进不去！！！！！
        $sub_system_data=self::getSubSystemPoint($sub_system);
        $save_state=self::SavePointCommand($sub_system_data['points']);
//        echo '<pre>';
//        print_r($sub_system_data);die;
        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

        $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());



        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

        $sub_system_data=json_encode($sub_system_data);

//
//        $end_time=microtime(true);
//        print_r($end_time-$start_time);
//        die;
        return $this->render('location_view', [
            'sub_system_model' => $sub_system_model,
            'location_id' => $id,
            'sub_system_data'=>$sub_system_data,
            //底图
            'image_base' => $url,
            'sub_system_in' => $sub_system_in,
            'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system' => $sub_system,
            'all_image' => $all_image,

            'category_tree' => $tree,
            'location_tree' => $location_tree
        ]);
    }
    public function actionSubsystem($id){
        $model =SubSystem::findModel($id);

        return $this->render('subsystem',[
            'model'=>$model
        ]);
    }

    public function actionLocationSubsystem($id){
//        echo 111;die;
        $sub_system_model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        if(sizeof($sub_system)==1){
            $id=$sub_system[0]['id'];
            $model =SubSystem::findModel($id);

            $sub_system=ImageGroup::find()->where(['location_id'=>$model->location_id])->asArray()->all();
            $location_info=Location::findOne($model->location_id);
            $url=Image::findModel($location_info->image_id);
            $sub_system_data=self::getSubSystemPoint($sub_system);
            $save_state=self::SavePointCommand($sub_system_data['points']);
            $sub_system_data=json_encode($sub_system_data);
            if(empty($url->default)){
                $base_map='uploads/pic/map1.jpg';
            }
            $user_id=Yii::$app->user->id;
            $user=User::findOne($user_id);
            if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;

            return $this->render('subsystem', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'id' => $id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
        else{

//            $this->redirect('/category/crud/location-only-view?id='.$id);
            $sub_system_model =new SubSystem();
            $location=Location::findOne($id);
            $image_base=Image::findOne($location->image_id);

            //所有的本楼层的子系统
            $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

            //子系统类别  sub_system_category内的值
            $sub_system_category=new Sub_System_Category();
            $all_sub_system=$sub_system_category->getAll();
            $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


            //所有的图
            $all_image=new Image();
            $all_image=$all_image->findAllImage();

            $sub_system_in=$all_sub_system;
            //TODO
            //系统内容为空 会进不去！！！！！
            $sub_system_data=self::getSubSystemPoint($sub_system);

            if(empty($image_base)){
                $url='uploads/pic/map1.jpg';
            }

            else $url=$image_base->url;

            $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());



            //在此添加一个 所有分类
            array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

            $location_tree = MyFunc::TreeData(Location::getAllCategory());
            //在此添加一个 所有分类
            array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

            $sub_system_data=json_encode($sub_system_data);


            return $this->render('location_only_view', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree
            ]);
        }
    }
    public  function actionLocationOnlyView($id) {

        $sub_system_model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();

        $sub_system_in=$all_sub_system;
        //TODO
        //系统内容为空 会进不去！！！！！
        $sub_system_data=self::getSubSystemPoint($sub_system);
        $save_state=self::SavePointCommand($sub_system_data['points']);
        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

        $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
//      $tree=MyFunc::fancyTree_data_test(EnergyCategory::getAllCategory());



        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

        $sub_system_data=json_encode($sub_system_data);


        if($id=='0000'){
            return $this->render('location_only_view1', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree
            ]);
        }
        else {
            return $this->render('location_only_view', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree
            ]);
        }
    }
    //得到 points 点位 写入
    public function SavePointCommand($points){
        $user = Yii::$app->user;
        $save_state='save';
        if(empty($points)){
            return 'points empty';
        }
        else {
            foreach ($points as $key => $point) {
                $command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
                $time = date('Y-m-d H:i:s', time());
                $data['CommandLog'] = [
                    'point_id' => $point,
                    'type' => 1,
                    'value' => '-1',
                    'timestamp' => $time,
                    'user_id' => $user->id,
                    'status' => 1
                ];
                $command_log->load($data);
                $command_log->save();
                if(!empty($command_log->errors)){
                    echo '<pre>';
                    print_r($command_log->errors);
                    die;
                    $save_state='save_error';
                }
            }
            return $save_state;
        }


    }

    public function actionAjaxDelete(){
        $data=Yii::$app->request->post();
        $model = new SubSystem();

//        echo '<pre>';
        print_r($data);
        $model = $model->findOne($data['id']);
        $model->delete();
    }

    public function actionAjaxSave(){
        $data=Yii::$app->request->post();

        $location_id=$data['location_id'];

        $sub_systems_data=json_decode($data['data'],1);
        $result=1;
        foreach ($sub_systems_data as $key=>$value) {

                $load_data['SubSystem'] = [
                    'name' => MyFunc::createJson($value['name']),
                    'data' => json_encode($value['data']),
                    'sub_system_type' => $value['category_id'],
                    'location_id' => $location_id

                ];

                //id为0则为新建子系统
                if($value['id']==0) {
                    $model = new SubSystem();
                }
                //否则为更新
                else {
                    $model=SubSystem::findModel($value['id']);
                }
            $model->load($load_data);
            $result=$result && $model->save();
//          print_r($model->errors);
//            die;
            }
        //保存之后更新 要查询的点位
        $sub_system=ImageGroup::find()->where(['location_id'=>$location_id])->asArray()->all();
        $sub_system_data=self::getSubSystemPoint($sub_system);
        return json_encode(['result'=>$result,'sub_system_data'=>$sub_system_data]);

    }

    //属性框保存按钮  保存点位绑定信息
    //only one point Info
    public function actionAjaxSaveElement(){
        $data=Yii::$app->request->post();
            //for  table 点位绑定
//        $data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
        $load_data['SubSystemElement'] = [
            'sub_system_id'=>$data['sub_system_id'],
            'element_id'=>$data['element_id'],
            'element_type'=>1,
            'config'=>json_encode(
                [
                    'data_type'=>'sub_system_element_config',
                    'data'=>['points'=>$data['binding_id']]
                ]
            )
        ];
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $model->load($load_data);
        $model->save();
        print_r($model->errors);
    }

    //属性框保存按钮  保存绑定信息(可以是一个点 或 多个点)
    //one or more  points Info
    public function actionAjaxSaveTableElement(){
        $data=Yii::$app->request->post();
        //for  table 点位绑定
        $data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
        $load_data['SubSystemElement'] = [
            'sub_system_id'=>$data['sub_system_id'],
            'element_id'=>$data['element_id'],
            'element_type'=>1,
            'config'=>json_encode(
                [
                    'data_type'=>'sub_system_element_config',
                    'data'=>[$data['type']=>$data['binding_id']]
                ]
            )
        ];
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $model->load($load_data);
        $model->save();
        print_r($model->errors);
    }
    public function actionAjaxSaveFireEventElement(){
        $data=Yii::$app->request->post();
        $bingding_id=$data['binding_id'];
        $events=[];$error_data=0;
        //根据 point_id字段处理events为两个一组
        foreach ($bingding_id as $key=>$value) {
            $events[$value['point_id']][$value['id']]=$value['name'];
        }

        //检测每个point_id是否对应两个event
        // Y则绑定关系  N则返回输入点位出错
        foreach ($events as $key=>$value) {
            if(sizeof($value)!=2)
                $error_data=1;
            else{
                $name='';
                foreach ($value as $v_key=>$v_value) {
                    $all_name=explode('-',$v_value);
                    if($name=='') {
                        $name = $all_name[sizeof($all_name) - 1];
                        $events[$key]['name']=$name;
                    }
                }

            }
        }

        //绑定sub_system_id element_id与event的关系
        //element_id刚好设置为对应的point_id 的值
        //并把point_id组成数组返回一边作为key生成图内的节点
        if($error_data==0) {
            $sub_system_node=[];
//            echo '<pre>';
//            print_r($events);
//            die;
            foreach ($events as $key => $value) {
                //
                $load_data['SubSystemElement'] = [
                    'sub_system_id' => $data['sub_system_id'],
                    'element_id' => $key,
                    'element_type' => 2,
                    'config' => json_encode(
                        [
                            'data_type' => 'sub_system_element_config',
                            'data' => ['events' => $value]
                        ]
                    )
                ];
                $model = new SubSystemElement();
                $model = $model->findModelByData($data['sub_system_id'], $key);
                $model->load($load_data);
                $model->save();
                if (!empty($model->errors)) {
                    print_r($model->errors);
                    return 'error_save_data';
                }
                else {
                    $sub_system_node['keys'][]=$key;
                    $sub_system_node['data'][$key]=substr($value['name'],0,strlen($value['name'])-3).'-'.substr($value['name'],strlen($value['name'])-3,3);
                }
            }
            return json_encode($sub_system_node);
        }
        else return 'error_post_data';
    }
    //点击图标时显示查询已经绑定点位id
    public function actionAjaxGetElement(){
        $data=Yii::$app->request->post();

        $type='points';
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $data=json_decode($model->config,1)['data'];
//        echo '<pre>';
//        print_r($data);
//        die;

        if(isset($data[$type])) {
            $key_info = $data[$type];
        }
        if(isset($data['events'])) {
            $type='events';
            $key_info = $data[$type];
        }
        if(isset($data['point_event'])) {
            $type='point_event';
            $key_info = $data[$type];
        }

        if(empty($key_info)){
            $points_info=[];
        }
        else {
            if($type=='points') {
                    //如果是数组 则绑定的为表格，查询所有点位信息
                    if (!is_array($key_info)) {
                        $points[$key_info] = null;

                    } else $points = $key_info;

                    foreach ($points as $id => $name) {
                        $points_id[] = $id;
                    }
                    $points_info = Point::getPointInfoById($points_id);

                    foreach ($points_info as $key => $value) {
                        $points_info[$key]['controller id'] = $value['controller_id'];
                        $points_info[$key]['pointId'] = $value['id'];
                        $points_info[$key]['time']=$this->Redis($value['id'],"value_timestamp");
                        if ($points[$value['id']] == null)
                            $points_info[$key]['pointName'] = MyFunc::DisposeJSON($value['name']);
                        else
                            $points_info[$key]['pointName'] = $points[$value['id']];

                        $points_info[$key]['protocolName'] = $value['protocol_id'];
                        $points_info[$key]['unit'] = $value['unit'];
                        $points_info[$key]['value type'] = $value['value_type'];
                        }
                    }
            if($type=='events' || $type=='point_event'){
                $points = $key_info;
                foreach ($points as $id => $name) {
                    if($id!='name')
                    $points_id[] = $id;
                }
                $points_info = PointEvent::getPointEventInfoById($points_id);
                foreach($points_info as $key => $value) {
                    $points_info[$key]['pointId'] = $value['id'];
                    $points_info[$key]['pointName'] = MyFunc::DisposeJSON($value['name']);
                    $points_info[$key]['point_id'] = $value['point_id'];
                    $points_info[$key]['type'] = $value['type'];
                }
            }

        }
            $points_info = json_encode($points_info);
        return $points_info;
        //return json_encode($points);

    }


    //点击图标时显示查询已经绑定点位id
    public function actionAjaxUpdateValue(){
        $data=Yii::$app->request->post();
        $data_temp=array();
        foreach ($data['data'] as $key=>$value) {
                if($value!=null) {
                    //$key 为对应的子系统id $element_id 为子系统内节点的key值
                    foreach ($value as $element_id) {
//                        $subsystem_data=SubSystem::findModel($key)->data;
//                        $subsystem_data=json_decode($subsystem_data,1)['data'];
//                        $subsystem_data=json_decode($subsystem_data,1)['nodeDataArray'];
//                        echo '<pre>';
//                        print_r($subsystem_data);
//                        die;
                        $config=SubSystemElement::find()->select('config')->where(['sub_system_id'=>$key,'element_id'=>$element_id])->asArray()->all();
                        if(!empty($config)) {

                            $point_id=json_decode($config[0]['config'],1)['data']['points'];
                            //非数组为普通value值
                            if(!is_array($point_id)) {
                                $point_value = Point::find()->select('name,value')->where(['id' => $point_id])->asArray()->all();
                                if (!empty($value)) {
                                    //默认显示中文名称
                                    $data_temp[$key][$element_id]['name'] =json_decode($point_value[0]['name'],1)['data']['zh-cn'];

                                    $data_temp[$key][$element_id]['value'] = round($point_value[0]['value'],2);

                                }
                            }
                            //数组则为 table值
                            else {
                                foreach ($point_id as $point_id_key=>$name) {
                                   $points[]=$point_id_key;
                                }
                                $point_value = Point::find()->select('id,value')->where(['id' => $points])->asArray()->all();
                                foreach ($point_value as $point_value_key=>$value) {
                                    $data_temp[$key][$element_id][]=['name'=>$point_id[$value['id']],'value'=>round($value['value'],2)];
                                }
                            }
                        }
                    }
                }
            }

        return json_encode($data_temp);
    }
    public function actionAjaxUpdateValueNew()
    {
//        $starttime=explode(' ',microtime());

        $data = Yii::$app->request->post()['data'];
        $data=json_decode($data,1);

        //point点位信息处理
        $points_data=Point::find()
            ->select('id,name,value,update_timestamp as timestamp')
            ->where(['id'=>$data['points']])
            ->asArray()->all();
        foreach ($points_data as $key=>$value) {
            //点位值更新时间与当前时间间隔如果在一分钟内则是实时值 否则值为null
            $time_interval=time()-strtotime($value['timestamp']);
//            if($time_interval>60 || $time_interval<0)
//                $value['value']=null;

            $point_value=$this->Redis($value['id'],"value");

            $points_info[$value['id']]=[
                'name'=>MyFunc::DisposeJSON($value['name']),
                'value'=>$value['value'],
                'timestamp'=>$this-> Redis($value['id'],"value_timestamp")

            ];
            if($point_value!=null){
                $points_info[$value['id']]['value']=$point_value;
            }
        }
        //event事件信息处理
        $event_data=PointEvent::find()
            ->select('id,name,trigger_status as value')
            ->where(['id'=>$data['events']])
            ->asArray()->all();
        $events_info=[];
        foreach ($event_data as $key=>$value) {
            $event_value=$this->RedisEvent($value['id'],'status');
//            $event_value=$this->RedisEvent('39497','status');
//            echo '<pre>';
//            print_r($event_value);
//            die;
            if($event_value==null)$event_value=0;
            $events_info[$value['id']]=[
                'name'=>MyFunc::DisposeJSON($value['name']),
                'value'=>$event_value,
                'timestamp'=>$this->RedisEvent($value['id'],'timestamp')

            ];
        }
//        echo '<pre>';
//        print_r($events_info);
//        die;

//        $points_name=MyFunc::map($points_info,'id','name');
//        $points_value=MyFunc::map($points_info,'id','value');
        $result=$data['data'];

        foreach ($result as $key=>$key_value) {
            foreach ($key_value as $key_value_key => $key_value_value) {

                if (in_array($key_value_key, $data['points_key'])) {
                    if (isset($key_value_value['id'])) {
                        $result[$key][$key_value_key]['time'] = $points_info[$key_value_value['id']]['timestamp'];
                        $result[$key][$key_value_key]['name'] = $points_info[$key_value_value['id']]['name'];
                        if ($points_info[$key_value_value['id']]['value'] == null)
                            $result[$key][$key_value_key]['value'] = null;
                        else $result[$key][$key_value_key]['value'] = round($points_info[$key_value_value['id']]['value'], 2);
                    } else {
                        foreach ($key_value_value as $value_key => $value) {
                            $time = $points_info[$value['id']]['timestamp'];
                            $name = $result[$key][$key_value_key][$value_key]['name'];
                            $result[$key][$key_value_key][$value_key]['time'] = $time;
                            if ($name == 0) {
                                $result[$key][$key_value_key][$value_key]['name'] = $points_info[$value['id']]['name'];
                            }
                            if ($points_info[$value['id']]['value'] == null)
                                $result[$key][$key_value_key][$value_key]['value'] = null;
                            else $result[$key][$key_value_key][$value_key]['value'] = round($points_info[$value['id']]['value'], 2);
                        }
                    }
                }
                //events节点值处理
                if (in_array($key_value_key, $data['events_key'])) {
                    //event节点 绑定两个点位
                    //会有一个id='name'  去掉
                    foreach ($key_value_value as $value_key => $value) {
                       if($value['id']!='name') {
                           $time = 'time';
                           $result[$key][$key_value_key][$value_key]['time'] = $time;
                           $result[$key][$key_value_key][$value_key]['name'] = $events_info[$value['id']]['name'];
                           if ($events_info[$value['id']]['value'] == null)
                               $result[$key][$key_value_key][$value_key]['value'] = 0;
                           else $result[$key][$key_value_key][$value_key]['value'] = round($events_info[$value['id']]['value'], 2);
                       }
                    }

                }
            }
        }


//        die;

        //如果points只绑定一个点  处理成对应格式
        //[sub_system_id][key]['0'=>[[id][name][value]]] ===>>>[sub_system_id][key][[id][name][value]]
        foreach ($result as $key=>$key_value) {
            foreach ($key_value as $key_value_key=>$key_value_value) {
                if(sizeof($result[$key][$key_value_key])==1){
                    $result[$key][$key_value_key]['id']=$result[$key][$key_value_key][0]['id'];
                    $result[$key][$key_value_key]['name']=$result[$key][$key_value_key][0]['name'];
                    $result[$key][$key_value_key]['value']=$result[$key][$key_value_key][0]['value'];
                    $result[$key][$key_value_key]['time']=$result[$key][$key_value_key][0]['time'];
                    unset($result[$key][$key_value_key][0]);
                }
            }
        }

        return json_encode($result);
    }
    public function actionAjaxControl(){
        //$data=['sub_system_id'=>     'element_id'=>      'control_type'=> ]
        $data=Yii::$app->request->post();

        //查询点位所操作的节点的点位信息 $config['point']为绑定的点位id
        $config=SubSystemElement::find()->select('config')
            ->where(['sub_system_id'=>$data['sub_system_id'],'element_id'=>$data['element_id']])
            ->asArray()->all();
        if(!empty($config)) {
            $point_id = json_decode($config[0]['config'], 1)['data']['points'];
            if(is_array($point_id))
                foreach($point_id as $key=>$value){
                    $id=$key;
               }
            else $id=$point_id;
//            $point_model=Point::findOne($point_id);
//            $point_model->value=$data['console_type'];
//            $result1=$point_model->save();

            $time= date('Y-m-d H:i:s',time());
//
            $user = Yii::$app->user;
            //判定点位类型

            //在command表里插入信息
            $command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
            $data['CommandLog'] = [
                'point_id' => (int)$id,
                'type' => 2,
                'value' => $data['console_type'],
                'timestamp' => $time,
                'user_id' => $user->id,
                'status' => 1
            ];
            $command_log->load($data);

            $command_log->save();
            $result=$command_log->errors;
//            echo '<pre>';
//            print_r($command_log);
//            die;
        }
        else{
            $result=-1;

        }
        return json_encode($result);
    }

    public function actionLocationViewTest($id) {
        $model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();


        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;



        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;
        return $this->render ( 'location_view_old', [
            'model'=>$model,
            'location_id'=>$id,
            'num'=>$points,
            //底图
            'image_base'=>$url,
            'sub_system_in'=>$sub_system_in,
            'all_sub_system'=>$all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system'=>$sub_system,
            'all_image'=>$all_image
        ] );
    }

    public function actionTest() {
//        $check=Yii::$app->redis->hcheck('Event:39497');
            $value=$this->RedisEvent('39497','status');
      print_r($value);
        die;
//        $instance->executeCommand('SUBSCRIBE',$channel,'cb');

        return $this->render ('test');
    }

    public function actionTest1() {

        $model=new ImageGroup();
        return $this->render ('test1',[
            'model'=>$model
        ]);
    }

    public function actionLocationDetailUpdate($id){
        $data = Yii::$app->request->post();
        $model=self::findLocationModel($id);
        $model->load($data);
        $model->name=MyFunc::createJson($model->name);
        $result=$model->save();
        if($result)
        return $this->redirect('/category/crud/location');
        else print_r($model->errors);
    }

/*
 * 更新报表树
 */
    public function actionReportManageUpdate() {
        if(!isset($_POST['idArray'])) $idArray=[];
        else $idArray=$_POST['idArray'];
        if($data = Yii::$app->request->post('data')){
            $this->report_recursion($data,null); // 递归添加或更新
            //$this->recursion($data,0); // 递归添加或更新
        }
      /*  echo '<pre>';
        print_r($data);
        die;*/
      // ReportManage::deleteAll(['not in', 'id', $idArray]); // 删除页面上没有的ID
        if($idArray ==null || $idArray=='[]'|| $idArray==''){
        }else{
            ReportManage::deleteAll(['in', 'id', $idArray]); // 删除页面上没有的ID
        }
        return Url::toRoute('/report_manage/crud');
    }



/*
 * 存储报表数组到数据库
 */
    protected function report_recursion($tree, $pid){
        foreach($tree as $row){
            if(isset($row['title'])){
                $model = $row['id']?$this->findReportManageModel($row['id']):new ReportManage(); // 如果没有ID就是添加
                if(isset($model)){
                    $model->parent_id = $pid;
                    $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                    //设置type 默认值为0
                    //$model->type='0';
                    $model->role_id=$row['role'];
                    if(  $model->save()&& $row['id'] == ''){ // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉\
                        $row['id'] = $model->primaryKey;
                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->report_recursion( $row['children'], $row['id']);
                }
            }
        }
        return ;
    }

    public function actionGenerateComputePoint()
    {
        return Url::toRoute('/category/crud');
    }

    /*
     * find event by event_name
     * 更具事件名称查询事件
     * */
    public function actionFireEventSearch(){
        $post_data=Yii::$app->request->post();
        $point_event=new PointEvent();
        $data=$point_event->fuzzySearch($post_data['name'],['id','name','point_id','type']);
        $result=[];
        //更换key名称
        foreach($data as $key => $value) {
            $pointName = json_decode($value['name'], true);
            $result[] = [
                'pointId' => $value['id'],
                'pointName' => $pointName['data']['zh-cn'],
                'point_id' => $value['point_id'],
                'type' => $value['type']
            ];
        }
        $result=json_encode($result);
        return  $result;
    }
    /*
     * find the point_event models are related the points,
     * and the points should be selected by the point's name
     * 通过名字查找 关联于点位的point_events
     * @param  String  post data
     * @return json[array]  array[];
     *
     * */
    public function actionPointEventSearch(){
        $post_data=Yii::$app->request->post();

        $point_event=new PointEvent();
        $data=$point_event->SearchEventsByPointNameSearch($post_data['name']);
        $result=[];
        //更换key名称
        foreach($data as $key => $value) {
            $pointName = json_decode($value['name'], true);
            $result[] = [
                'pointId' => $value['id'],
                'pointName' => $pointName['data']['zh-cn'],
                'point_id' => $value['point_id'],
                'type' => $value['type']
            ];
        }
        $result=json_encode($result);
        return  $result;
    }

    public function actionAjaxSetDefend(){
        $post_data=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($post_data);
//        die;
        $defend_value=null;
        $node_type=$post_data['node_type'];
        if($post_data['defend']==0)$defend_value=1;
        else $defend_value=0;
        $model=new SubSystemElement();
        $model=$model->findModelByData($post_data['sub_system_id'],$post_data['element_id']);
        $event_data=json_decode($model->config,1)['data']['point_event'];
        $defend_id=null;
        //找到绑定的点位中的撤防布防点位
        foreach ($event_data as $event_id=>$name) {
            if(
            ( $node_type=='state_defend' && strstr($name,'布防操作成功'))||
            ( $node_type=='state_defend_1' && strstr($name,'布防'))||
            ( $node_type=='state_defend_ctrl' && strstr($name,'布防'))
            ){
                $defend_id=$event_id;
            }
        }
        if($defend_id!=null){
            //入侵则手动改redis的值
            if($node_type=='state_defend'|| $node_type=='state_defend_1') {
                Yii::$app->redis->set('Event:' . $defend_id, $defend_value);
                Yii::$app->redis->publish('Event:' . $defend_id, $defend_value);
                return 'publish Event:'.$defend_id.' '.$defend_value.'success';
            }
            //周界则  插入命令
            if($node_type=='state_defend_ctrl')
            {
                //根据event_id 查找到point_id
                $point_id=PointEvent::find()
                    ->select('point_id')
                    ->where(['id'=>$defend_id])
                    ->asArray()->all()[0]['point_id'];

                $time= date('Y-m-d H:i:s',time());
//
                $user = Yii::$app->user;
                //判定点位类型

                //在command表里插入信息
                $command_log = new CommandLog();

                $data['CommandLog'] = [
                    'point_id' => $point_id,
                    'type' => 2,
                    'value' => (string)$defend_value,
                    'timestamp' => $time,
                    'user_id' => $user->id,
                    'status' => 1
                ];
                $command_log->load($data);

                $command_log->save();
                $result=$command_log->errors;
                return json_encode($result);
            }

        }
        else return 'error[no defend event]';


    }
	/**
	 * Find the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */


    public function Redis($id,$key){
        $id="Database:Point:".$id;
//        $key = "Database:Point:8999675:value";   //值
        //$key_value = "Database:Point:8985462:value_timestamp";  //更新时间
        //$key = "test";
        //$tem_set = Yii::$app->redis->set($key,"test222");
        $value = Yii::$app->redis->hget($id,$key);
//        echo "<pre>";print_r($tem_get);die;
        if($key=="value_timestamp")
            if($value!=null)
            $value=date('Y-m-d H:i:s',$value);
            else $value='无';
        return $value;
    }
    public function RedisEvent($id,$key){
        $id="Event:".$id;
//        $key = "Event:8999675:status";   //值
        //$key_value = "Event:8985462:timestamp";  //更新时间
        //$key = "test";
        //$tem_set = Yii::$app->redis->set($key,"test222");
//        $check=Yii::$app->redis->hcheck($id);
        $value = Yii::$app->redis->hget($id,$key);
        if($value=="True") $value=1;
        if($value=="False")$value=0;
//        echo "<pre>";print_r($tem_get);die;
        if($key=="timestamp"){
            if($value!=null)
                $value=date('Y-m-d H:i:s',$value);
            else $value='无';
        }
        if($value==null)$value=0;
        return $value;
    }


    protected function findReportManageModel($id) {
        if (($model = ReportManage::findOne ( $id )) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException ( 'The requested page does not exist.' );
        }
    }
    /*
     * 初始化角色选项
     */
    public function actionInitRole()
    {
        $initRoleArray=$_GET['initRole'];
        $reportmange=new ReportManage();
        $roleAll=$reportmange->findRole();
        if($initRoleArray==null || $initRoleArray==''|| $initRoleArray=='[]')
        {
            $initRoleArray=0;
        }
    $html = '<div class="arrow"></div>';
    $html .= '<h3 class="popover-title">当前角色</h3>';
    $html .= '<div class="popover-content">';
    $html .= '<div class="editableform-loading" style="display: none;"></div>';
    $html .= '<form class="form-inline editableform" style="">';
    $html .= '<div class="control-group form-group"><div>';
    $html .= '<div class="editable-input">';
    $html .= '<div class="editable-address">';
    $html .= Html::dropDownList('report_role', $initRoleArray, $roleAll,['placeholder' => '请选择角色', 'class' => 'select2',  'id' => '_ele2','multiple'=>true,'disabled' => 'disabled']);
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="editable-buttons">';
 //   $html .= ' <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>';
    $html .= '<button type="button" class="btn btn-default btn-sm editable-cancel1"><i class="glyphicon glyphicon-remove"></i></button>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="editable-error-block help-block" style="display: none;"></div>';
    $html .= '</div>';
    $html .= '</form>';
    $html .= '</div>';
        return $html;
    }
}
