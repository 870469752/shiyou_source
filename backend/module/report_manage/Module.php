<?php

namespace backend\module\report_manage;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\report_manage\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
