<?php
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use common\library\MyFunc;

//echo '<pre>';
//print_r($all_sub_system);
//die;

?>

<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }
</style>
<div id="contextMenu">
    <ul>
        <li><a href="#" id="menu1" onclick="AtomicEvent()">原子事件</a></li>
        <li><a href="#" id="menu5" onclick="">属性</a></li>
        <li><a href="#" id="menu3" onclick=" ">持续时间</a></li>
        <li><a href="#" id="menu4" onclick=" ">删除</a></li>
    </ul>
</div>


<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                    <header>


    <div class="widget-toolbar smart-form" data-toggle="buttons">
        <button class="btn btn-xs btn-primary" id="add_link"  >
            链接
        </button>
        <button class="btn btn-xs btn-primary" id="save"  >
            保存
        </button>
        <button class="btn btn-xs btn-primary" id="add_subsystem"  >
            添加子系统
        </button>
        <button class="btn btn-xs btn-primary" id="remove_attribute"  >
            属性框
        </button>

    </div>



    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
    <h2>楼层子系统 </h2>

</header>



                <div>
                        <span style="display: inline-block; vertical-align: top; padding: 5px">

                              <div>
                                  <div id="image_preview_parent">
                                      <div id="image_preview" style="border: solid 1px black;width: 150px; height: 100px"></div>
                                  </div>
                                  <div id="myPaletteSmall" style="border: solid 1px black;width: 150px; height: 600px">
                                      <!-- menu start -->
                                      <div id="accordion" style="width: 150px;">
                                          <h3>图1</h3>
                                          <div id="sample1" style="max-height: 450px;">
                                          </div>
                                          <h3>摄像头图</h3>
                                          <div style="max-height: 450px;">
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头1.jpg">摄像头1</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头2.jpg">摄像头2</a>
                                          </div>
                                          <h3>示例图3</h3>
                                          <div style="max-height: 450px;">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value">值</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="edit">编辑</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="onoffvalue">开关值</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="alarmvalue">警报值</a>

                                          </div>
                                          <h3>按钮</h3>
                                          <div style="max-height: 450px;">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="button">按钮1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮1.png">按钮2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮2.png">按钮3</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮3.png">按钮4</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮5.png">按钮5</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="gif" url="/uploads/pic/按钮5.png">按钮5</a>
                                          </div>
                                      </div>
                                      <!-- end-->
                                  </div>


                              </div>
                 </span>

                     <span style="display: inline-block; vertical-align: top; padding: 5px; width:85%">
                    <div id="tabs2">
                        <ul>
                            <li>
                                <a href="#tabs-0">sample</a>
                            </li>
                        </ul>
                        <div id="tabs-0">
                            <div id="myDiagramX" style="border: solid 1px black; width:100%; height:650px"></div>
                        </div>
                    </div>
                     </span>
                </div>




            </div>
        </article>
    </div>
</section>
            <!--add link-->
<div id="link_update" title="子系统" style="display:none;width: auto">

    <form>
        <fieldset>
            <div class="form-group">
                <label>名称</label>

                <input class="form-control" id="node_name" value="" placeholder="Text field" type="text">

            </div>


            <div class="form-group">
                <label>节点链接</label>

                <input class="form-control" id="node_link" value="" placeholder="Text field" type="text">

            </div>





        </fieldset>

    </form>

</div>

<!-- add tab Demo -->
<div id="addtab" title="子系统" style="display:none;width: auto">

    <form>

        <fieldset>
            <input name="authenticity_token" type="hidden">
            <div class="form-group">
                <label>子系统名称</label>
                <input class="form-control" id="sub_system_name" value="" placeholder="Text field" type="text">
            </div>

            <div class="form-group">
                <label>有无底图</label>
                <?=HTML::dropDownList('image_base',null,[0=>'无',1=>'有'],['class'=>'select2','id'=>'image_base'])?>

            </div>
            <div class="form-group">
                <label>子系统类型</label>
<!--                <select id="sub_system_category" class="select2" name="sub_system_category">-->
<!--                    <option value="3">&nbsp;给排水</option>-->
<!--                    <option value="4">排风机</option>-->
<!--                </select>-->
                <?=HTML::dropDownList('sub_system_category',null,$sub_system_in,['class'=>'select2','id'=>'sub_system_category'])?>

            </div>


        </fieldset>

    </form>

</div>

<!--属性面板-->
<div id="AttributeMenu" style="width: 400px;position:relative;z-index:10;display: ">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                <?=Html::hiddenInput('image_group_id',$model->id,['id'=>'image_group_id'])?>
                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('sub_system_id',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('sub_system_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'sub_system_id'])?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                </div>

                                <div class="col-md-1"></div>
                                <div class="help-block"></div>
                                <?php echo'<div class="row"><div class="col-md-9" style="margin: 5px;">';?>
                                <?=Html::label('点位',null,  ['class' => 'control-label',   'id' => '_name'])?>
                                <?php echo '<select id= "select_point"   style="width: 100%" name="point_id" placeholder="请选择相应点位">';
                                foreach($num as $key=>$value)
                                    echo '<option   value='.$key.'>'.$value.'</option>';
                                echo '</select></div></div>';
                                ?>
                                <div class="help-block"></div>


                                <div class="row" >
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>

<!--<input type="button" id="save" value="save">-->
<!--<input type="button" id="update" value="update">-->
<textarea id="mySavedModel" style="width:100%;height:300px;display: '';">

  </textarea>
<p id="diagramEventsMsg" style="display: none;">Msg</p>

<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/gojs/node_template_edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);

?>

<script>
    var sub_system_data=new Array();
    var link=null;
    var link_name=null;
    var button=null;
    var button_key=null;
    function resize() {
         for(var key in sub_system_data) {
             var diagram = sub_system_data[key];
             diagram.requestUpdate();
//             diagram.rebuildParts();
         }
    }
    window.onload = function () {

//        init_node_template_edit();
        //把image加入到左侧 可预览的列表中
        var html='';
        <?php foreach ($all_image as $value) {?>
        html=html+'<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" category="main" url="/'+'<?=$value['url']?>'+'">'+'<?=MyFunc::DisposeJSON($value['name'])?>'+'</a>';
        <?php } ?>
        $("#sample1").append(html);
        //标签绑定点击事件 生成预览图
        $("a").click(function(){
            preview_test(this);
        });
//      点击按钮预览节点
        function preview_test(obj){
            var url=obj.getAttribute("url");
            var category=obj.getAttribute("category");
//            console.log(url);
            var value={category: 'error'};
            switch (category) {
                case 'main'         :var value = { category: 'main',size: '75 75',img: url};                            break;
                case 'value'        :var value = {category: 'value',img:'/uploads/pic/按钮3.png'};                                                   break;
                case 'edit'         :var value = {category: 'edit'};                                                    break;
                case 'onoffvalue'   :var value = {category: 'onoffvalue'};                                              break;
                case 'alarmvalue'   :var value = {category: 'alarmvalue'};                                              break;
                case 'button'       :var value = {category: 'button',img:'/uploads/pic/按钮1.jpg',link:'',text:'按钮1'};break;
                case 'test'         :var value = {category: 'test',img:url,link:'',text:'按钮'};break;
                case 'gif'         :var value = {category: 'gif',size: '75 75',img:'/uploads/pic/图1.jpg'};break;
            }
            $("#image_preview").remove();
            $("#image_preview_parent").append('<div id="image_preview" style="border: solid 1px black;width: 150; height: 100px"></div>');
            preview(value);
        }
        function preview(value){
            var $ = go.GraphObject.make;
            $(go.Palette,"image_preview",  // must name or refer to the DIV HTML element
                {
                    maxSelectionCount: 1,
                    nodeTemplateMap: myDiagram2.nodeTemplateMap,  // share the templates used by myDiagram
                    model: new go.GraphLinksModel([  // specify the contents of the Palette
                        value
                    ])
                });
        }
        $( "#accordion" ).accordion();
        //属性窗口可拖动
        $('#AttributeMenu').draggable();
        $("#remove_attribute").click(function () {
            var display=document.getElementById("AttributeMenu").style.display
             if(display=='none') document.getElementById("AttributeMenu").style.display='';
                else document.getElementById("AttributeMenu").style.display='none';
        })
        //初始化下拉框
        $("#select_point option[value='"+'en'+"']").attr("selected","selected");
        $("#select_point").chosen();
        document.getElementById("AttributeMenu").style.display='none';
        //提供 图id与 设施key  ajax得到设施信息
        function ajaxGetAttribute(sub_system_id,key){
            var id=$("#image_group_id").val();
            //ajax获取信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-get-element",
                data: {sub_system_id:sub_system_id, element_id:key},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
//                    msg=eval('['+msg+']');
//                    var data=msg[0];
//                    var binding_id=data['binding_id'];
//                    binding_id=eval(binding_id);
//                    console.log(typeof (binding_id));
                    //更新 绑定点位信息
                    update(msg);
                }
            });
        }
        $("#update").click(function(){
            updatevalue();
        })
        setInterval(updatevalue,5000);
//        setInterval(gif,500);
        //动态图测试
        function gif(){
            for(var key in sub_system_data){
                //tabs_id 对应div的value值为
                var tabs_id = "tabs-myDiagram-" + key;
                var sub_system_id=$("#"+tabs_id).attr("value");
                //加入指定的node
                var diagram=sub_system_data[key];
                var temp = diagram.model.toJson();
                var data = eval('(' + temp + ')');
                //得到节点对象
                var nodeDataArray = data['nodeDataArray'];
                var flag=false;
                //把查询返回的值重新写入节点数组中
                for(var nodeDatas in nodeDataArray){
                    var nodecategory=nodeDataArray[nodeDatas]['category'];
                    //找到value节点实时更新点位的值
                    if(nodecategory=='gif'){

                        var nodekey=nodeDataArray[nodeDatas]['key'];
                        //console.log(key_value);
                        //如果 value值改变了 则 更新 值 并使flag=true
                        if(nodeDataArray[nodeDatas]['img'] =='/uploads/pic/图1.jpg')
                        {
                            nodeDataArray[nodeDatas]['img']='/uploads/pic/双风机.jpg';
                        }
                        else if(nodeDataArray[nodeDatas]['img'] =='/uploads/pic/双风机.jpg' )
                        {
                            nodeDataArray[nodeDatas]['img']='/uploads/pic/图1.jpg';
                        }

                    }
                }
                diagram.model.startTransaction("flash");
                diagram.model.nodeDataArray=nodeDataArray;
                diagram.model.commitTransaction("flash");
            }
        }
        //ajax更新所有value节点的值
        function updatevalue() {
            var location_system=new Array();
            for(var key in sub_system_data){
                //tabs_id 对应div的value值为
                var tabs_id = "tabs-myDiagram-" + key;
                var sub_system_id=$("#"+tabs_id).attr("value");
                //加入指定的node
                var diagram=sub_system_data[key];
                var temp = diagram.model.toJson();
                var data = eval('(' + temp + ')');
                //得到节点对象
                var nodeDataArray = data['nodeDataArray'];
//                console.log(nodeDataArray);
                //找到key所表示的节点更新text为value
                var element_ids=new Array();

                for(var nodeDatas in nodeDataArray){

//                    console.log(nodeDatas);
                    var nodecategory=nodeDataArray[nodeDatas]['category']
                    //找到value节点实时更新点位的值
//                    console.log(nodecategory=='value');
                    if(nodecategory=='value' || nodecategory=='onoffvalue' || nodecategory=='alarmvalue')
                    {
//                        console.log(nodeDataArray[nodeDatas]);
//                        console.log(nodecategory);
                        //得到value节点的key
                        var nodekey=nodeDataArray[nodeDatas]['key'];
//                        console.log(nodeDatas);
                        element_ids.push(nodekey);
                    }
                }
                location_system[sub_system_id]=sub_system_id;
                location_system[sub_system_id]=element_ids;
            }

//            console.log(location_system);
            //ajax保存到数据库
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-update-value",
                data: { data:location_system},
                success: function (msg) {

                    //返回value绑定点位的值
//                    console.log(msg);
                    msg = eval('(' + msg + ')');

//                    console.log(msg);
                    for (var key in sub_system_data) {
                        var tabs_id = "tabs-myDiagram-" + key;
                        var sub_system_id = $("#" + tabs_id).attr("value");
                        //加入指定的node
                        var diagram = sub_system_data[key];
                        var temp = diagram.model.toJson();
                        var data = eval('(' + temp + ')');
                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        var key_value = msg[sub_system_id];
                        console.log(key_value);
                        if (key_value!=undefined) {
                            //标记此图的value是否改变
                            var flag=false;
                    //把查询返回的值重新写入节点数组中
                             for(var nodeDatas in nodeDataArray){
                                var nodecategory=nodeDataArray[nodeDatas]['category'];
                                    //找到value节点实时更新点位的值
                                    if(nodecategory=='value'){
                                        var nodekey=nodeDataArray[nodeDatas]['key'];
                                            if(key_value[nodekey]!=undefined) {
                                                //console.log(key_value);
                                                //如果 value值改变了 则 更新 值 并使flag=true
                                                if(nodeDataArray[nodeDatas]['text'] != key_value[nodekey])
                                                {
                                                    flag=true;
                                                    nodeDataArray[nodeDatas]['text'] = key_value[nodekey];
                                                }
                                            }
                                            else {
                                                flag=true;
                                                nodeDataArray[nodeDatas]['text'] = '?????';
                                                }
                                        }
                                 if(nodecategory=='onoffvalue'){

                                     var nodekey=nodeDataArray[nodeDatas]['key'];

                                     if(key_value[nodekey]!=undefined) {
                                         //console.log(key_value);
                                             //如果 value值改变了 则 更新 值 并使flag=true
                                                 flag=true;
                                                 if(key_value[nodekey]=='0')
                                                 nodeDataArray[nodeDatas]['text'] ='On';
                                                  if(key_value[nodekey]=='1')
                                                     nodeDataArray[nodeDatas]['text'] = 'Off';
                                                 if(key_value[nodekey]!='1' && key_value[nodekey]!='0')
                                                     nodeDataArray[nodeDatas]['text'] = key_value[nodekey];
                                        }
                                        else {
                                             flag=true;
                                             nodeDataArray[nodeDatas]['text'] = '????';
                                             }
                                 }
                                 if(nodecategory=='alarmvalue'){
                                     var nodekey=nodeDataArray[nodeDatas]['key'];
                                     if(key_value[nodekey]!=undefined) {
                                         //console.log(key_value);
                                             //如果 value值改变了 则 更新 值 并使flag=true
                                                 flag=true;
                                                 if(key_value[nodekey]=='0')
                                                     nodeDataArray[nodeDatas]['text'] ='正常';
                                                 if(key_value[nodekey]=='1')
                                                     nodeDataArray[nodeDatas]['text'] = '报警';
                                                 if(key_value[nodekey]!='1' && key_value[nodekey]!='0')
                                                     nodeDataArray[nodeDatas]['text'] = key_value[nodekey];

                                        }
                                        else {
                                         flag=true;
                                         nodeDataArray[nodeDatas]['text'] = '????';
                                            }
                                 }
                             }
                            if(flag){
                                console.log(nodeDataArray);
                                diagram.model.startTransaction("flash");
                                diagram.model.nodeDataArray=nodeDataArray;
                                diagram.model.commitTransaction("flash");
                            }
                        }
                    }
                }
            });
        }

        //更新属性框内的点位值
        function update(value){
            //chosen 先设置值在执行更新函数
            $("#select_point").val(value);
            $("#select_point").trigger("chosen:updated");
        }

        //点击保存绑定信息
            $('#_submit').click(function(){
                var id=$("#image_group_id").val();
                var sub_system_id=$("#sub_system_id").val();
                var element_id=$("#element_id").val();
                var point_ids=$("#select_point").val();
                //ajax保存信息
                $.ajax({
                    type: "POST",
                    url: "/category/crud/ajax-save-element",
                    data: {sub_system_id:sub_system_id, element_id:element_id,binding_id:point_ids},
                    success: function (msg) {
                        //根据image_group_id   element_id 得到属性框信息
                        msg=eval('['+msg+']');
//                        console.log(msg);
                    }
                });

            })

        //跟着窗口滚动
        function scroll(){
            $(window).scroll(function(){
                var oParent = document.getElementById('AttributeMenu');
                var x =oParent.offsetLeft;
                var y = oParent.offsetTop;
                var yy = $(this).scrollTop();//获得滚动条top值
                if ($(this).scrollTop() < 30) {
                    $("#AttributeMenu").css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位

                }else{
                    $("#AttributeMenu").css({"position":"absolute",top:yy+"px",left:x+"px"});
                }
            });
        }
        //s为所选节点 id为此节点所在的 myDiagram所在数组sub_system_data中的键值
        function showMessage(s,id) {

                var tabs_id = "tabs-myDiagram-" + id;
//            console.log(s.key +"   "+tabs_id);
                //节点信息
                document.getElementById("diagramEventsMsg").textContent = s;
                //所属的子系统id
                var sub_system_id = $("#" + tabs_id).attr("value");

                document.getElementById("sub_system_id").value = sub_system_id;
                document.getElementById("image_group_id").value = sub_system_id;
                document.getElementById("element_id").value = s.key;

                ajaxGetAttribute(sub_system_id, s.key);
                update('zh');
                //document.getElementById("AttributeMenu").style.display='';

//            //可拖动
//            drag();
        }

        scroll();
        var image_url='<?=$image_base?>';
        var location_id=<?=$location_id?>;
        //所有的分类
        var all_sub_system=<?=json_encode($all_sub_system)?>;
        var tab_num=1;
        //console.log(all_sub_system);
        init_blank('myDiagramX');

        $('#tabs').tabs();
        $('#tabs2').tabs();
        var image_in=new Array();
        var data_name=new Array();


        //数组remove指定键值元素 数据
        function remove(a,id){
            var result=new Array();
            for(var key in a){
                if(key!=id)
                result[key]=a[key];
            }
            return result;
        }
        // Dynamic tabs
        var tabTitle = $("#tab_title");
        var tabContent = $("#tab_content");
        var tabTemplate =   "<li style='position:relative;'> "+
                                "<span class='air air-top-left delete-tab' style='top:7px; left:7px;'>"+
                                    "<button class='btn btn-xs font-xs btn-default hover-transparent'>"+
                                    "<i class='fa fa-times'>"+
                                    "</i>" +
                                    "</button>" +
                                "</span>" +
                                "</span>" +
                                    "<a onclick='resize();' href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}" +
                                    "</a>" +
                            "</li>";

        var id = "sub_system";
        var tabs = $("#tabs2").tabs();

        //弹出框内三个值
        var sub_system_name=$("#sub_system_name");
        var image_base=$("#image_base");
        var sub_system_category=$("#sub_system_category");

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-danger",
                click : function() {
                    //验证弹出框内数据完整性
//                    var sub_system_name=$("#sub_system_name").val();
//                    var image_base=$("#image_base").find("option:checked").text();
//                    var sub_system_category=$("#sub_system_category").find("option:checked").text();
//                      alert(sub_system_category.find("option:checked").val())
                       if(sub_system_name.val()==''){
                        alert('请输入子系统名称');
                    }
                    else {
                           var name = sub_system_category.find("option:checked").text()+'_'+sub_system_name.val();
                           var id = sub_system_category.find("option:checked").val();
                           var with_image=image_base.find("option:checked").val();
                           //如果分类还有则 继续所属分类的标签
                           if(name!='_'+sub_system_name.val()) {
                               image_in[tab_num]=with_image;

                               //添加标签页
                               addTab(name,tab_num,0);
                               //初始化  myDiagram  有底图和无底图
                               if(with_image==0) init2('myDiagram'+tab_num,tab_num);
                               else init1('myDiagram'+tab_num,tab_num);

                               tab_num++;
                               //去掉添加了的子系统类型    [又不需要去掉了]
//                               $("#sub_system_category option[value=" + id + "]").remove();
//                               //改变选择值为第一个
//                               $("#sub_system_category option:first").prop("selected", 'selected');
//                               var text = $("#sub_system_category option:first").text();
//                               $("#select2-chosen-2").html(text);
                           }
                           //如果分类没有了 则提示
                           else alert('分类用完了');
                       }
                    $(this).dialog("close");
                }
            }]
        });

        $("#add_link").click(function(){
            linkdialog.dialog("open");
        })
        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var linkdialog = $("#link_update").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            },
                {

                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-danger",
                click : function() {
                        link=$("#node_link").val();
                        link_name=$("#node_name").val();

                    var diagram=button;
                    var temp = diagram.model.toJson();

                    var data = eval('(' + temp + ')');

                    //得到节点对象
                    var nodeDataArray = data['nodeDataArray'];
                    for(var nodeDatas in nodeDataArray){
                        var nodekey=nodeDataArray[nodeDatas]['key'];
                            if(nodekey==button_key){
                                nodeDataArray[nodeDatas]['link']=link;
                                nodeDataArray[nodeDatas]['text']=link_name;
                            }
                    }
                    diagram.model = go.Model.fromJson(data);
                    $(this).dialog("close");
                    $("#node_link").val('');
                    $("#node_name").val('');
                }
            }]
        });



        // actual addTab function: adds new tab using the input from the form above
        function addTab(name,id,sub_system_id) {
//            console.log(id);
            data_name[id]=name;
            var tabs_id = "tabs-myDiagram-" + id;
            var label = name || tabContent;
            var li = $(tabTemplate.replace(/#\{href\}/g, "#" + tabs_id).replace(/#\{label\}/g, label));
            var tabContentHtml='<div id="myDiagram'+id+'"style="border: solid 1px black; width:100%; height:650px" "></div>';
            tabs.find(".ui-tabs-nav").append(li);
            tabs.append("<div id='" + tabs_id + "' value='"+sub_system_id+"'> " + tabContentHtml + " </div>");
            tabs.tabs("refresh");
            // clear fields
            $("#sub_system_name").val("");

        }



        // 增加子系统 并删除已经增加的子系统
        $("#add_subsystem").button().click(function() {
            dialog.dialog("open");
        });
        //删除标签
        // close icon: removing the tab on click
        $("#tabs2").on("click", 'span.delete-tab', function() {
            if (!confirm("您确定要删除吗？"))
            { return false;}
            else
            {
            //移除标签li   以及div
            var panelId = $(this).closest("li").remove().attr("aria-controls");


            //得到要删除的标签id  并截取对应的类型id位
            var tab_id=$("#" + panelId).attr("id");
            console.log(tab_id)
            var id=$("#" + panelId).attr("value");
            var category_id=tab_id.substring(15,tab_id.length);
            $("#" + panelId).remove();

            //ajax  delete删除对应的子系统
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-delete",
                data: {id: id},
                //
                success: function (msg) {
                }
            });

            //在数组内移除对应的子系统信息
            data_name=remove(data_name,category_id);
            image_in=remove(image_in,category_id);
            sub_system_data=remove(sub_system_data,category_id);

            tabs.tabs("refresh");
            //删除标签 并把分类再加入分类框内
            $("#sub_system_category").append("<option value='"+category_id+"'>"+all_sub_system[category_id]+"</option>");
            }
        });

        $("#save").click(function(){
            console.log(data_name);
            console.log(image_in);
            console.log(sub_system_data);
            document.getElementById("mySavedModel").value = null;
            //var data1=myDiagram1.model.toJson();
            var data=new Array();

                for( var key in sub_system_data){
                    console.log('key= '+key);
                    var id_tmep=document.getElementById("tabs-myDiagram-"+key).getAttribute('value');
                    //key=key.toString();
                    console.log("tabs-myDiagram-"+key);
                    console.log(id_tmep);
                    var data_temp={
                        'name':data_name[key],
                        'category_id':key,
                        'id':id_tmep,
                        'data':{'image_base':image_in[key],'data':sub_system_data[key].model.toJson()},
                    };
        document.getElementById("mySavedModel").value = document.getElementById("mySavedModel").value +' '+data_name[key]+' '+key+sub_system_data[key].model.toJson();
                    data.push(data_temp);

                }
            //console.log(data);
            var data=JSON.stringify(data);
            //console.log(data);
//            ajax save
//            sub_system Info  [name data category_id location_id]
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save",
                data: {data: data,location_id:location_id},
                //暂时未加判定是否成功插入
                success: function (msg) {
                }
            });

        })



        function init_blank(element) {
            if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates

            var cellSize = new go.Size(10, 10);
            myDiagram2 =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        "resizingTool.isGridSnapEnabled": true,
                        "undoManager.isEnabled": true,
                        allowDrop: true,// must be true to accept drops from the Palette
                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.Center,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );
            //点击事件
            myDiagram2.addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if(part.data.category=='button') {
                            button = myDiagram2;
                            button_key = part.data.key;
                            document.getElementById("node_link").value = part.data.link;
                            document.getElementById("node_name").value = part.data.text;
//                            $("#node_link").val(part.data.link);
//                            $("#node_name").val(part.data.text);
                            console.log('button_key');
                            console.log(part);
                            console.log(button_key);
                            linkdialog.dialog("open");
                        }
                    }

                    //showMessage(part.data,num);
                });

            var mainTemplate=$(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                {resizable: true, resizeObjectName: "pic"},
                //contextMenu右键菜单属性
                { contextMenu: $(go.Adornment)},
                $(go.Picture,
                    {
                        //desiredSize: new go.Size(75, 75)
                        //maxSize: new go.Size(75, 75)
                    },
                    new go.Binding("source", "img")
                ),
                //new go.Binding("desiredSize","size"),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        //console.log('删除');
                        //console.log('init');
                        var diagram = myDiagram2;
//                        diagram.commandHandler.deleteSelection();
//                        diagram.currentTool.stopTool();
                    }
                },
                { // handle mouse enter/leave events to show/hide the ports
                    //mouseEnter: function(e, node) { console.log('enter'); },
                    //mouseLeave: function(e, node) { console.log('leave'); }
                }
            );

            var editTemplate=
                $(go.Node,"Spot",
                    //绑定位置 loc信息
                    // { locationSpot: go.Spot.Center },//按中心定位
                    { locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                                             //绑定位置 输入框text信息
                        $(go.TextBlock, "编辑",{
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: true
                            }
                            ,
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text").makeTwoWay()
                        )
                    ),

                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var valueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Auto",//横向排列
                        $(go.Picture,'/uploads/pic/按钮3.png',
                            {
                                name: "PIC",
                                desiredSize: new go.Size(100, 25)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img")),
                        $(go.TextBlock,"value",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var OnOffValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },
                        $(go.TextBlock,"OnOff",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var AlarmValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                                             //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },

                        $(go.TextBlock,"报警/正常",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );

            var ButtonTemplate=
            $(go.Node,"Auto",
                    { locationSpot: go.Spot.Center },
                    {resizable: true, resizeObjectName: "PIC"},
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),

                $(go.Panel, "Horizontal",//横向排列
                                         //contextMenu右键菜单属性
                    {
                        contextMenu: $(go.Adornment)
                    },

                    $(go.Picture,
                        {
                            name: "PIC",
//                            desiredSize: new go.Size(75, 75)
                            //maxSize: new go.Size(75, 75)
                        },
                        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                        new go.Binding("source", "img"))

                ),
                $(go.Panel, "Horizontal",//横向排列
                                         //contextMenu右键菜单属性
                    {
                        contextMenu: $(go.Adornment)
                    },

                    $(go.TextBlock,"报警/正常",
                        {

//                                background: "lightgreen",
                            font: "normal normal 200 10px /1.2 微软雅黑",
                            //font: "bold 14px sans-serif",
                            stroke: '#333',
                            margin: 0,
                            isMultiline: true,
                            editable: false
                        },
                        //暂时无背景颜色
                        //new go.Binding("background", "background"),
                        new go.Binding("text", "text"))

                ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件



                        }
                    }
                );

            var TestTemplate=
                $(go.Node,"Auto",
                    { locationSpot: go.Spot.Center },
//                    {resizable: true, resizeObjectName: "PIC"},
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                        $(go.Picture,
                            {
                                name: "PIC",
                            desiredSize: new go.Size(100, 25)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img")),


                        $(go.TextBlock,"报警/正常",
                            {

//                                background: "lightgreen",
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 10,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))

                );
            var GifTemplate=$(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                {resizable: true, resizeObjectName: "pic"},
                $(go.Picture,
                    {
                    },
                    new go.Binding("source", "img")
                ),
                { // handle mouse enter/leave events to show/hide the ports
                    //mouseEnter: function(e, node) { console.log('enter'); },
                    //mouseLeave: function(e, node) { console.log('leave'); }
                }
            );
            //添加node模板
            myDiagram2.nodeTemplateMap.add("main", mainTemplate);
            myDiagram2.nodeTemplateMap.add("edit", editTemplate);
            myDiagram2.nodeTemplateMap.add("value", valueTemplate);
            myDiagram2.nodeTemplateMap.add("onoffvalue", OnOffValueTemplate);
            myDiagram2.nodeTemplateMap.add("alarmvalue", AlarmValueTemplate);
            myDiagram2.nodeTemplateMap.add("button", ButtonTemplate);
            myDiagram2.nodeTemplateMap.add("test", TestTemplate);
//            var myPalette1 =
//                $(go.Palette, "myPalette1",  // must name or refer to the DIV HTML element
//                    {
//                        maxSelectionCount: 1,
//                        nodeTemplateMap: myDiagram2.nodeTemplateMap,  // share the templates used by myDiagram
//                        model: new go.GraphLinksModel([  // specify the contents of the Palette
//                            <?php
//                            foreach ($all_image as $value) {
//                                 echo '{category:"main", text: "Start",size:"75 75", img:"/'.$value['url'].'" },';
//                            }
//                                                //"size":"205 139"
//                            ?>
//                            //{category:"main", text: "Start",size:new go.Size(75, 75), img:"/uploads/pic/双风机.jpg" },
//                            //{category:"main", text: "Start",  img:"/uploads/pic/图1.jpg" },
//                            //{ category:"main",text: "水泵", img:"/uploads/pic/水泵.png" },
//                            //{ category:"main",text: "冷却机",img:"/uploads/pic/冷却机.png" },
//                            { category:"main",size:"75 75",img:"/uploads/pic/冷却机.png" }
//                            //{ category:"main",img:"/uploads/pic/test.gif" }
//                        ])
//                    });
//
//            var myPalette2 =
//                $(go.Palette,"myPalette2",  // must name or refer to the DIV HTML element
//                    {
//                        maxSelectionCount: 1,
//                        nodeTemplateMap: myDiagram2.nodeTemplateMap,  // share the templates used by myDiagram
//                        model: new go.GraphLinksModel([  // specify the contents of the Palette
//                            { category:"main",size:"75 75",img:"/uploads/pic/摄像头1.jpg" },
//                            { category:"main",size:"75 75",img:"/uploads/pic/摄像头2.jpg" },
//                            { category:"edit" },
//                            //{category:"main", text: "Start",size:new go.Size(75, 75), img:"/uploads/pic/双风机.jpg" },
//                            { category:"value" }
//                        ], [
//                            // the Palette also has a disconnected Link, which the user can drag-and-drop
////                        { points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"red",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]},
//                        ])
//                    });
        }


        //初始化 有底图的
        function init1(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {

                        allowVerticalScroll:false,
                        allowDrop: true,// must be true to accept drops from the Palette
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
                        contentAlignment:go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    });


            sub_system_data[num]=myDiagram;

            //点击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if(part.data.category=='button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
                        }
                    }
                });

            // the background image, a floor plan
            sub_system_data[num].add(
                $(go.Part,  // this Part is not bound to any model data
                    {
//                        layerName: "Background", position: new go.Point(0, 0),
                        selectable: false, pickable: false
                    },
                    $(go.Picture, "/"+image_url)
                ));

            var mainTemplate=$(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                {resizable: true, resizeObjectName: "pic"},
                //contextMenu右键菜单属性
                { contextMenu: $(go.Adornment)},
                $(go.Picture,
                    {
                        //maxSize: new go.Size(75, 75)
                    },
                    new go.Binding("source", "img")),
                //new go.Binding("desiredSize","size"),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        //console.log('删除');
                        //console.log('init1');
                        //var diagram = myDiagram1;
//                        sub_system_data[num].commandHandler.deleteSelection();
//                        sub_system_data[num].currentTool.stopTool();
//                        diagram.commandHandler.deleteSelection();
//                        diagram.currentTool.stopTool();
                    }
                },
                { // handle mouse enter/leave events to show/hide the ports
                    //mouseEnter: function(e, node) { console.log('enter'); },
                    //mouseLeave: function(e, node) { console.log('leave'); }
                }
            );
            var editTemplate=
                $(go.Node,"Spot",
                    //绑定位置 loc信息
                    // { locationSpot: go.Spot.Center },//按中心定位
                    { locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //绑定位置 输入框text信息
                        $(go.TextBlock, "编辑",{
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: true
                            }
                            ,
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text").makeTwoWay()
                        )
                    ),

                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var valueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Auto",//横向排列
                        $(go.Picture,
                            {
                                name: "PIC",
                                desiredSize: new go.Size(100, 25)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img")),
                        $(go.TextBlock,"value",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );

            var OnOffValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                                             //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },
                        $(go.TextBlock,"OnOff",
                            {
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var AlarmValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },
                        $(go.TextBlock,"报警/正常",
                            {
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );

            var ButtonTemplate=
                $(go.Node,"Spot",
                    {resizable: true, resizeObjectName: "PIC"},

                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },

                        $(go.Picture,
                            {
                                name: "PIC",
//                                desiredSize: new go.Size(75, 75)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img"))

                    ),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },

                        $(go.TextBlock,"报警/正常",
                            {

//                                background: "lightgreen",
//                                fill:'/uploads/pic/按钮1.jpg',
//                                background:'/uploads/pic/按钮1.jpg',
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))

                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件

                        }
                    }
                );
            var TestTemplate=
                $(go.Node,"Auto",
                    { locationSpot: go.Spot.Center },
//                    {resizable: true, resizeObjectName: "PIC"},
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Picture,
                        {
                            name: "PIC",
                            desiredSize: new go.Size(100, 25)
                            //maxSize: new go.Size(75, 75)
                        },
                        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                        new go.Binding("source", "img")),


                    $(go.TextBlock,"报警/正常",
                        {

//                                background: "lightgreen",
                            font: "normal normal 200 15px /1.2 微软雅黑",
                            //font: "bold 14px sans-serif",
                            stroke: '#333',
                            margin: 10,
                            isMultiline: true,
                            editable: false
                        },
                        //暂时无背景颜色
                        //new go.Binding("background", "background"),
                        new go.Binding("text", "text"))

                );
            //添加node模板
            sub_system_data[num].nodeTemplateMap.add("main", mainTemplate);
            sub_system_data[num].nodeTemplateMap.add("value",valueTemplate);
            sub_system_data[num].nodeTemplateMap.add("edit", editTemplate);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue", OnOffValueTemplate);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue", AlarmValueTemplate);
            sub_system_data[num].nodeTemplateMap.add("button", ButtonTemplate);
            sub_system_data[num].nodeTemplateMap.add("test", TestTemplate);
        }
        //初始化 无底图的
        function init2(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {
                        allowVerticalScroll:false,
                        allowDrop: true,// must be true to accept drops from the Palette
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );

            //把myDiagram 加入sub_system_data内
            sub_system_data[num]=myDiagram;
            //点击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if(part.data.category=='button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
                        }
                    };
                });
            var mainTemplate=$(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                {resizable: true, resizeObjectName: "pic"},
                //contextMenu右键菜单属性
                { contextMenu: $(go.Adornment)},
                $(go.Picture,
                    {
                        //maxSize: new go.Size(75, 75)
                    },
                    new go.Binding("source", "img")),
                //new go.Binding("desiredSize","size"),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        //console.log('删除'+'init2');
//                        sub_system_data[num].commandHandler.deleteSelection();
//                        sub_system_data[num].currentTool.stopTool();
                    }
                },
                { // handle mouse enter/leave events to show/hide the ports
                    //mouseEnter: function(e, node) { console.log('enter'); },
                    //mouseLeave: function(e, node) { console.log('leave'); }
                }
            );
            var editTemplate=
                $(go.Node,"Spot",
                    //绑定位置 loc信息
                    // { locationSpot: go.Spot.Center },//按中心定位
                    { locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //绑定位置 输入框text信息
                        $(go.TextBlock, "编辑",{
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: true
                            }
                            ,
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text").makeTwoWay()
                        )
                    ),

                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var valueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Auto",//横向排列
                        $(go.Picture,
                            {
                                name: "PIC",
                                desiredSize: new go.Size(100, 25)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img")),
                        $(go.TextBlock,"value",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );

            var OnOffValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                                             //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },
                        $(go.TextBlock,"OnOff",
                            {
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var AlarmValueTemplate=
                $(go.Node,"Spot",
                    { locationSpot: go.Spot.Center },
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },
                        $(go.TextBlock,"报警/正常",
                            {
                                font: "normal normal 200 15px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))
                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );
            var ButtonTemplate=
                $(go.Node,"Spot",
                    {resizable: true, resizeObjectName: "PIC"},
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    {resizable: true, resizeObjectName: "PIC"},
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },

                        $(go.Picture,
                            {
                                name: "PIC",
//                                desiredSize: new go.Size(75, 75)
                                //maxSize: new go.Size(75, 75)
                            },
                            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                            new go.Binding("source", "img"))

                    ),
                    $(go.Panel, "Horizontal",//横向排列
                        //contextMenu右键菜单属性
                        {
                            contextMenu: $(go.Adornment)
                        },

                        $(go.TextBlock,"报警/正常",
                            {

//                                background: "lightgreen",
//                                fill:'/uploads/pic/按钮1.jpg',
//                                background:'/uploads/pic/按钮1.jpg',
                                font: "normal normal 200 10px /1.2 微软雅黑",
                                //font: "bold 14px sans-serif",
                                stroke: '#333',
                                margin: 0,
                                isMultiline: true,
                                editable: false
                            },
                            //暂时无背景颜色
                            //new go.Binding("background", "background"),
                            new go.Binding("text", "text"))

                    ),
                    { // if double-clicked, an input node will change its value, represented by the color.
                        doubleClick: function (e, obj) { //双击事件
                            //console.log('改变');
                            var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                        }
                    }
                );

            var TestTemplate=
                $(go.Node,"Auto",
                    { locationSpot: go.Spot.Center },
//                    {resizable: true, resizeObjectName: "PIC"},
                    new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                    $(go.Picture,
                        {
                            name: "PIC",
                            desiredSize: new go.Size(100, 25)
                            //maxSize: new go.Size(75, 75)
                        },
                        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                        new go.Binding("source", "img")),


                    $(go.TextBlock,"报警/正常",
                        {

//                                background: "lightgreen",
                            font: "normal normal 200 15px /1.2 微软雅黑",
                            //font: "bold 14px sans-serif",
                            stroke: '#333',
                            margin: 10,
                            isMultiline: true,
                            editable: false
                        },
                        //暂时无背景颜色
                        //new go.Binding("background", "background"),
                        new go.Binding("text", "text"))

                );
            //添加node模板
            sub_system_data[num].nodeTemplateMap.add("main", mainTemplate);
            sub_system_data[num].nodeTemplateMap.add("value",valueTemplate);
            sub_system_data[num].nodeTemplateMap.add("edit", editTemplate);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue", OnOffValueTemplate);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue", AlarmValueTemplate);
            sub_system_data[num].nodeTemplateMap.add("button", ButtonTemplate);
            sub_system_data[num].nodeTemplateMap.add("test", TestTemplate);
        }


        //根据查询信息加载页面
         <?php  foreach ($sub_system as $key=>$value) { ?>
                        var name='<?=MyFunc::DisposeJSON($value['name'])?>';
                        var category_id=tab_num;//<?=$value['sub_system_type']?>;
                        var data=<?=$value['data']?>;
                        var sub_system_id=<?=$value['id']?>;
                        //console.log(name);
                        //console.log(data['data']);
                        //console.log(eval(data));
                        //加标签页
                        addTab(name,category_id,sub_system_id);
                        //初始化标签页  myDiagram
//                        console.log(data);
                        if(data['image_base']==0) { image_in[category_id]=0;init2('myDiagram'+category_id,category_id);}
                        else  {image_in[category_id]=1;init1('myDiagram'+category_id,category_id);}
                        sub_system_data[category_id].model = go.Model.fromJson(data['data']);
                        //tab_num=tab_num>=category_id?tab_num:category_id;
                        tab_num++;
        <?php  }  ?>




    }
</script>