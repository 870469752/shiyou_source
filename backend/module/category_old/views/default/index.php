<?php

use common\library\MyHtml;
use yii\helpers\Url;
use common\library\MyActiveForm;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

$this->title = Yii::t('category', 'Category');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>

                <!-- widget content -->
                <div class="widget-body">
                    <div class="widget-body-toolbar bg-color-white">
                        <?php $form = MyActiveForm::begin(['method' => 'get']); ?>

                        <?php foreach($category_tree as $k => $tree): ?>
                            <div class="form-group field-category-id[<?=$k?>][] required">
                                <label class="control-label" for="field-category-id[<?=$k?>][]"><?= MyFunc::DisposeJSON($tree['name']) ?></label>
                                <select multiple class="select2" name="<?= $model->formName() ?>[id][<?=$k?>][]" placeholder="请选择分类">
                                    <?= MyHtml::TreeOption(@$model->id[$k], $tree['sub']) ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        <?php endforeach ?>
                            <?= $form->field($model, 'start_time')->textInput(['value' => date('Y-m-d', strtotime($model->start_time))]) ?>
                            <?= $form->field($model, 'end_time')->textInput(['value' => date('Y-m-d', strtotime($model->end_time))]) ?>

                            <?= MyHtml::submitButton('完成', ['class' => 'btn btn-success']) ?>

                        <?php MyActiveForm::end(); ?>

                    </div>
                    <?php if($result_json != '[]'): ?>
                    <div id="CategoryPie"><h1 class="col-xs-12 text-align-center"><strong>请稍等...</strong></h1></div>
                    <div id="CategoryChart"></div>
                    <?php else: ?>
                        没有数据，请选择分类
                    <?php endif ?>


                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('/js/plugin/ion-slider/ion.rangeSlider.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<script>
    window.onload = function () {

        datepicker_lang_date('<?= $model->getTime('min') ?>', '<?= $model->getTime('max') ?>');

        var start_time = $("[name='<?= $model->formName() ?>[start_time]']");
        var end_time = $("[name='<?= $model->formName() ?>[end_time]']");
        start_time.datepicker({
            onClose: function (selectedDate) {
                end_time.datepicker("option", "minDate", selectedDate);
            }
        });
        end_time.datepicker({
            onClose: function (selectedDate) {
                start_time.datepicker("option", "maxDate", selectedDate);
            }
        });

        highcharts_lang_date();

        var ajax_url = '<?= Url::toRoute(['/category/default/ajax-category-data']+$model->toArray()) ?>';


        // 饼图单色渐变
        Highcharts.getOptions().plotOptions.pie.colors = (function () {
            var colors = [],
                base = Highcharts.getOptions().colors[0],
                i;

            for (i = 0; i < 10; i += 1) {
                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
            }
            return colors;
        }());

        // 饼图
        $('#CategoryPie').highcharts({
            chart: {
                type: 'pie'
            },
            title: {
                text: '分类<br/>占比',
                verticalAlign: 'middle',
                y: 50
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    innerSize: '50%',
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.2f} %'
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<b>{point.name}</b>: {point.y:.2f} KWH'
            },
            series: <?= $result_json ?>
        });

        // 柱状图异步方法
        function afterSetExtremes(e) {
            var chart = $('#CategoryChart').highcharts();
            chart.showLoading('加载中，请稍等...');
            $.getJSON(ajax_url +
                '&start_time=' + Math.round(e.min / 1000) +
                '&end_time=' + Math.round(e.max / 1000),
                function (result) {
                    // 他必须得一组一组数据插入
                    for(var i = 0; i < result.length; i++){
                        chart.series[i].setData(result[i].data);
                    }
                    chart.hideLoading();
                });
        }

        // 柱状图
        $.getJSON(ajax_url, function (data) {
            // create the chart
            $('#CategoryChart').highcharts('StockChart', {
                chart: {
                    type: 'column',
                    zoomType: 'x'
                },
                navigator: {
                    adaptToUpdatedData: false,
                    series: data
                },
                legend: {
                    enabled: true,
                    align: '',
                    verticalAlign: 'top'
                },
                scrollbar: {
                    liveRedraw: false
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                rangeSelector: {
                    enabled: false
                },
                xAxis: {
                    events: {
                        afterSetExtremes: afterSetExtremes
                    },
                    minRange: 3600 * 1000
                },
                yAxis: {
                    floor: 0
                },
                series: data
            });
        });
    };
</script>