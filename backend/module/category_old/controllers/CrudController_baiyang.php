<?php

namespace backend\module\category\controllers;

use backend\models\CommandLog;
use backend\models\EnergyCategory;
use backend\models\Image;
use backend\models\ImageGroup;
use backend\models\Location;
use backend\models\Point;
use backend\models\PointCategoryRelNew;
use backend\models\Sub_System_Category;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use Yii;
use backend\models\Category;
use backend\models\search\CategorySearch;
use backend\controllers\MyController;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use yii\helpers\Url;
use backend\models\Unit;
/**
 * Class CrudController
 * @package backend\module\category\controllers
 */
class CrudController extends MyController {
    /**
     * @var array 页面上存在的ID
     */
    //public $ids = [0];
    public $ids=[];
	public function behaviors() {
		return [
				'verbs' => [
						'class' => VerbFilter::className (),
						'actions' => [
								'delete' => [
										'post'
								]
						]
				]
		];
	}

	/**
	 * Lists all Category models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
        $data=EnergyCategory::find()
            ->select(['id','title'=>'name','parent_id','rgba'])
            ->OrderBy('id desc')
            ->asArray()->all();
        $tree = MyFunc::TreeData($data);
		return $this->render ( 'index', [
				'tree' => $tree
		] );
	}


    public function actionLocation() {

        $tree = MyFunc::TreeData(
            Category::find()
                ->select(['id','title'=>'name','parent_id','rgba'])
                ->where(['category_type'=>2])
                ->OrderBy('id desc')
                ->asArray()->all()
        );
        return $this->render ( 'location', [
            'tree' => $tree
        ] );
    }
    public function actionLocationShow() {

        $tree = MyFunc::TreeData(
            Location::find()
                ->select(['id','title'=>'name','parent_id'])
                ->OrderBy('id desc')
                ->asArray()->all()
        );
        return $this->render ( 'location_show', [
            'tree' => $tree
        ] );
    }

    public function actionLocationDetail($id) {
        $model=self::findLocationModel($id);
        $select_points=PointCategoryRelNew::find()->select('id')->where(['category_id'=>$id])->asArray()->all();
        $s_points=array();
        foreach ($select_points as $value) {
            $s_points[]=$value['id'];
        }
        //所有底图
        $image=Image::findAllImage();

        $image=MyFunc::_map($image,'id','name');

        //楼层绑定的底图
        $points=Point::findAllPoint();
        $points=MyFunc::_map($points, 'id', 'name');
        return $this->render ( 'location_update', [
                    'model'=>$model,
                    'points'=>$points,
                    's_points'=>$s_points,
                    'image'=>$image
        ] );
    }
    //$sub_system 为子系统数据组成的数组
    public function getSubSystemPoint($sub_system){
        //得到子系统内所有绑定的点位 组成数组
        $points=[];$sub_system_data=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];
            $keys=[];

//            echo '<pre>';
//            print_r($data);
//            die;
            foreach ($data as $data_key=>$data_value) {
                //group 不存在category字段
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'state':
                    case 'gif':
                    case 'control':
                    case 'value_1':
                    case 'state_value':
                    //得到 需要查询节点的key
                    $keys[] = $data_value['key'];
                    $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //根据子系统id 和key 找到需要查询的点位集合
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();

            foreach ($configs as $key=>$configs_value) {
                $points_info=json_decode($configs_value['config'],1)['data']['points'];
                //获取的data信息中  points 可能为点位id  可能为数组信息[id1=>name1,id2=>name2,...]
                if(!is_array($points_info))
                {
                    $points[]=$points_info;
                    $sub_system_data[$system['id']][$configs_value['element_id']]=['id'=>$points_info];
                }
                else {
                    foreach ($points_info as $point_id=>$point_name) {
                        $points[]=$point_id;
                        $sub_system_data[$system['id']][$configs_value['element_id']][]=['id'=>$point_id,'name'=>$point_name];
                    }

                }
            }
        }
		if(!empty($sub_system_data )){
			foreach($sub_system_data as $sub_system_id=>$sub_system){
				foreach($sub_system as $key=>$value){
							if(empty($value))
								unset($sub_system_data[$sub_system_id][$key]);
				}
			}
		}
        return ['data'=>$sub_system_data,'points'=>$points];
    }

    //替换子系统内节点方法
    public function actionChange(){
        //A45 A29的视频子系统
//        $systemids=[
//            888,814,812,810
//        ];
//        $systemids=[61];
        //需要的节点
        $state=[
            'category'=>'state',
            'size'=>'75 75',
            'img'=>'/uploads/pic/state_red.svg',
            'text'=>
                [
                    '0'=>'/uploads/pic/belt_red.png',
                    '1'=>'/uploads/pic/belt_green.png'
                ]
        ];
        $value=[
            'category'=>'only_value'
        ];
        $all_system=SubSystem::find()->select('*')->asArray()->all();
        foreach($all_system as $key=>$system){
            $system_id=$system['id'];
//            if(in_array($system_id,$systemids)){
                //得到子系统的data字段并把json格式分解为数组 重新拼装
                $data=json_decode($system['data'],1);
            $system_data=json_decode($data['data'],1);
                $nodeDataArray=$system_data['nodeDataArray'];
//                echo '<pre>';
//                print_r($system_data);
//                die;
                foreach($nodeDataArray as $node_key=>$node_value){




                    //替换要更新的节点
                    if(isset($node_value['category']))
                    if($node_value['category']=='state')
                    if($node_value['img']=='/uploads/pic/belt_green.png')
                    {
//                        $node_value['size']='17 17';
                        $node_value['text']=$state['text'];
//                            print_r($node_value);
                        $nodeDataArray[$node_key]=$node_value;
//
                    }

                }


//                把拼装处理好的data在还原为原来的json格式
                $system_data['nodeDataArray']=$nodeDataArray;
                $system_data=json_encode($system_data);
                $data['data']=$system_data;
                $system['data']=json_encode($data);
                //找到子系统模型把data字段写回去
                $model=SubSystem::findModel($system_id);
                $model->data=$system['data'];
                $result=$model->save();
            }
//        }
die;

    }


    public function actionLocationView($id) {

        $sub_system_model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();

        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;
        //TODO
       //系统内容为空 会进不去！！！！！
        $sub_system_data=self::getSubSystemPoint($sub_system);
//        $sub_system_data=[];
        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;


        $tree = MyFunc::TreeData(Category::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);


        $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
        //TODO 更改
//        $category_data=Location::getCategoryInfo();


//        $tree_data = MyFunc::get_fancyTree_data1(Category::getCategoryInfo());
//
//        $tree_data_location=MyFunc::get_fancyTree_data1(Location::getCategoryInfo());
//        $tree_json = json_encode($tree_data);
//        $tree_json_location=json_encode($tree_data_location);
        $sub_system_data=json_encode($sub_system_data);

        //根据部分地理位置
        $special_ids=[1680,948,594,341,79,82];
//        //查找下属的子系统id
//        //所有的电力监测子系统为白色底图
//        $white_background_sub_ids=Location::getManySubCategory($special_ids);
//        echo'<pre>';
//        print_r($white_background_sub_id);
//        die;

//        $white_background_sub_id所属的sub_system_id为白色背景

//        if(in_array($id,$white_background_sub_ids)){
        if($id=='0000'){
//            echo 111;
//            die;
            return $this->render('location_view1', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'num' => $points,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree,
                'unit' => $unit
//                ,
//                'tree' => $tree_data,
//                'tree_json' => $tree_json,
//                'tree_json_location' => $tree_json_location
            ]);
        }
        else {
            return $this->render('location_view', [
                'sub_system_model' => $sub_system_model,
                'location_id' => $id,
                'num' => $points,
                'sub_system_data'=>$sub_system_data,
                //底图
                'image_base' => $url,
                'sub_system_in' => $sub_system_in,
                'all_sub_system' => $all_sub_system,
//                  楼层内已经存在的子系统全部信息
                'sub_system' => $sub_system,
                'all_image' => $all_image,

                'category_tree' => $tree,
                'location_tree' => $location_tree,
                'unit' => $unit
//                ,
//                'tree' => $tree_data,
//                'tree_json' => $tree_json,
//                'tree_json_location' => $tree_json_location
            ]);
        }
    }
    public function actionSubsystem($id){
        $model =SubSystem::findModel($id);

        return $this->render('subsystem',[
            'model'=>$model
        ]);
    }

    public function actionLocationOnlyView($id) {
        $model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();


        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;



        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;
        return $this->render ( 'location_only_view', [
            'model'=>$model,
            'location'=>$location,
            'location_id'=>$id,
            'num'=>$points,
            //底图
            'image_base'=>$url,
            'sub_system_in'=>$sub_system_in,
            'all_sub_system'=>$all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system'=>$sub_system,
            'all_image'=>$all_image
        ] );
    }

    public function actionAjaxDelete(){
        $data=Yii::$app->request->post();
        $model = new SubSystem();

//        echo '<pre>';
        print_r($data);
        $model = $model->findOne($data['id']);
        $model->delete();
    }

    public function actionAjaxSave(){
        $data=Yii::$app->request->post();

        $location_id=$data['location_id'];

        $sub_systems_data=json_decode($data['data'],1);
        $result=1;
        foreach ($sub_systems_data as $key=>$value) {

                $load_data['SubSystem'] = [
                    'name' => MyFunc::createJson($value['name']),
                    'data' => json_encode($value['data']),
                    'sub_system_type' => $value['category_id'],
                    'location_id' => $location_id

                ];

                //id为0则为新建子系统
                if($value['id']==0) {
                    $model = new SubSystem();
                }
                //否则为更新
                else {
                    $model=SubSystem::findModel($value['id']);
                }
            $model->load($load_data);
            $result=$result && $model->save();
//          print_r($model->errors);
//            die;
            }
        //保存之后更新 要查询的点位
        $sub_system=ImageGroup::find()->where(['location_id'=>$location_id])->asArray()->all();
        $sub_system_data=self::getSubSystemPoint($sub_system);
        return json_encode(['result'=>$result,'sub_system_data'=>$sub_system_data]);

    }

    //属性框保存按钮  保存点位绑定信息
    public function actionAjaxSaveElement(){
        $data=Yii::$app->request->post();
            //for  table 点位绑定
//        $data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
        $load_data['SubSystemElement'] = [
            'sub_system_id'=>$data['sub_system_id'],
            'element_id'=>$data['element_id'],
            'element_type'=>1,
            'config'=>json_encode(
                [
                    'data_type'=>'sub_system_element_config',
                    'data'=>['points'=>$data['binding_id']]
                ]
            )
        ];
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $model->load($load_data);
        $model->save();
        print_r($model->errors);
    }
    //属性框保存按钮  保存表格绑定信息
    public function actionAjaxSaveTableElement(){
        $data=Yii::$app->request->post();
        //for  table 点位绑定
        $data['binding_id']=MyFunc::map($data['binding_id'],'id','name');
//        echo '<pre>';
//        print_r($data);
//        die;
        $load_data['SubSystemElement'] = [
            'sub_system_id'=>$data['sub_system_id'],
            'element_id'=>$data['element_id'],
            'element_type'=>1,
            'config'=>json_encode(
                [
                    'data_type'=>'sub_system_element_config',
                    'data'=>['points'=>$data['binding_id']]
                ]
            )
        ];
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $model->load($load_data);
        $model->save();
        print_r($model->errors);
    }
    //点击图标时显示查询已经绑定点位id
    public function actionAjaxGetElement(){
        $data=Yii::$app->request->post();

        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);

        $points=json_decode($model->config,1)['data']['points'];
        //如果是数组 则绑定的为表格，查询所有点位信息
        if(is_array($points)) {
            foreach ($points as $id=>$name) {
                $points_id[]=$id;
            }

            $points_info=Point::getPointInfoById($points_id);
            foreach ($points_info as $key=>$value) {
                $points_info[$key]['device id']=$value['device_id'];
                $points_info[$key]['pointId']=$value['id'];
                $points_info[$key]['pointName']=$points[$value['id']];
                $points_info[$key]['protocolName']=$value['protocol_id'];
                $points_info[$key]['unit']=$value['unit'];
                $points_info[$key]['value type']=$value['value_type'];
            }

           $points=json_encode($points_info);

        }

        return $points;
        //return json_encode($points);

    }


    //点击图标时显示查询已经绑定点位id
    public function actionAjaxUpdateValue(){
        $data=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($data);
//        die;
        $data_temp=array();
        foreach ($data['data'] as $key=>$value) {
                if($value!=null) {
                    //$key 为对应的子系统id $element_id 为子系统内节点的key值
                    foreach ($value as $element_id) {
//                        $subsystem_data=SubSystem::findModel($key)->data;
//                        $subsystem_data=json_decode($subsystem_data,1)['data'];
//                        $subsystem_data=json_decode($subsystem_data,1)['nodeDataArray'];
//                        echo '<pre>';
//                        print_r($subsystem_data);
//                        die;
                        $config=SubSystemElement::find()->select('config')->where(['sub_system_id'=>$key,'element_id'=>$element_id])->asArray()->all();
                        if(!empty($config)) {

                            $point_id=json_decode($config[0]['config'],1)['data']['points'];
                            //非数组为普通value值
                            if(!is_array($point_id)) {
                                $point_value = Point::find()->select('name,value')->where(['id' => $point_id])->asArray()->all();
                                if (!empty($value)) {
                                    //默认显示中文名称
                                    $data_temp[$key][$element_id]['name'] =json_decode($point_value[0]['name'],1)['data']['zh-cn'];

                                    $data_temp[$key][$element_id]['value'] = round($point_value[0]['value'],2);

                                }
                            }
                            //数组则为 table值
                            else {
                                foreach ($point_id as $point_id_key=>$name) {
                                   $points[]=$point_id_key;
                                }
                                $point_value = Point::find()->select('id,value')->where(['id' => $points])->asArray()->all();
                                foreach ($point_value as $point_value_key=>$value) {
                                    $data_temp[$key][$element_id][]=['name'=>$point_id[$value['id']],'value'=>round($value['value'],2)];
                                }
                            }
                        }
                    }
                }
            }

        return json_encode($data_temp);
    }
    public function actionAjaxUpdateValueNew()
    {
        $data = Yii::$app->request->post()['data'];
//        echo '<pre>';
//        print_r($data);
//        die;
        $points_data=Point::find()
            ->select('id,name,value,update_timestamp as timestamp')
            ->where(['id'=>$data['points']])
            ->asArray()->all();

        foreach ($points_data as $key=>$value) {
            //点位值更新时间与当前时间间隔如果在一分钟内则是实时值 否则值为null
            $time_interval=time()-strtotime($value['timestamp']);
//            if($time_interval>60 || $time_interval<0)
//                $value['value']=null;

            $points_info[$value['id']]=[
                'name'=>MyFunc::DisposeJSON($value['name']),
                'value'=>$value['value'],
                'timestamp'=>$value['timestamp']
            ];
        }

//        echo '<pore>';
//        print_r($points_info);
//        die;
//        $points_name=MyFunc::_map($points_info,'id','name');
//        $points_value=MyFunc::map($points_info,'id','value');
        $result=$data['data'];

        foreach ($result as $key=>$key_value) {
                foreach ($key_value as $key_value_key=>$key_value_value) {
                    if(isset($key_value_value['id'])) {
                        $result[$key][$key_value_key]['name'] = $points_info[$key_value_value['id']]['name'];
                        if($points_info[$key_value_value['id']]['value']==null)
                             $result[$key][$key_value_key]['value']=null;
                        else $result[$key][$key_value_key]['value'] = round($points_info[$key_value_value['id']]['value'],2);
                    }
                    else {
                        foreach ($key_value_value as $value_key=>$value) {
                            $name=$result[$key][$key_value_key][$value_key]['name'];
                            if($name==0) {
                                $result[$key][$key_value_key][$value_key]['name'] = $points_info[$value['id']]['name'];
                            }
                            if($points_info[$value['id']]['value']==null)
                                 $result[$key][$key_value_key][$value_key]['value'] =null;
                            else $result[$key][$key_value_key][$value_key]['value'] =round($points_info[$value['id']]['value'],2);
                        }
                    }
                }
        }
        return json_encode($result);
    }
    public function actionAjaxControl(){
        //$data=['sub_system_id'=>     'element_id'=>    'console_value'=>     'control_type'=> ]
        $data=Yii::$app->request->post();

        //查询点位所操作的节点的点位信息 $config['point']为绑定的点位id
        $config=SubSystemElement::find()->select('config')
            ->where(['sub_system_id'=>$data['sub_system_id'],'element_id'=>$data['element_id']])
            ->asArray()->all();
        if(!empty($config)) {
            $point_id = json_decode($config[0]['config'], 1)['data']['points'];
//            $point_model=Point::findOne($point_id);
//            $point_model->value=$data['console_type'];
//            $point_model->save();
            $time=date('Y-m-d H:i:s',time());
//            echo'<pre>';
//            print_r($time);
//            die;
            $user = Yii::$app->user;
            //判定点位类型

            //在command表里插入信息
            $command_log = new CommandLog();
            $aaa=$command_log::find()->select('*')->asArray()->all();

            $data['CommandLog'] = [
                'point_id' => $point_id,
                'type' => 2,
                'value' => $data['console_type'],
                'timestamp' => $time,
                'user_id' => $user->id,
                'status' => 1
            ];
            $command_log->load($data);
            $result = $command_log->save();
        }
        else{
            $result=-1;

        }
        return $result;
    }

    public function actionLocationViewTest($id) {
        $model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();


        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;



        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;
        return $this->render ( 'location_view_old', [
            'model'=>$model,
            'location_id'=>$id,
            'num'=>$points,
            //底图
            'image_base'=>$url,
            'sub_system_in'=>$sub_system_in,
            'all_sub_system'=>$all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system'=>$sub_system,
            'all_image'=>$all_image
        ] );
    }

    public function actionTest() {

        $model=new ImageGroup();
        return $this->render ('test',[
            'model'=>$model
        ]);
    }

    public function actionTest1() {

        $model=new ImageGroup();
        return $this->render ('test1',[
            'model'=>$model
        ]);
    }
    /**
     * 更新分类树
     * @return mixed
     */

    public function actionLocationUpdate() {


        if($data = Yii::$app->request->post('data')){
            $this->location_recursion($data, null); // 递归添加或更新

        }
        Location::deleteAll(['not in', 'id', $this->ids]); // 删除页面上没有的ID

        return Url::toRoute('/category/crud/location');
    }
    public function actionLocationDetailUpdate($id){
        $data = Yii::$app->request->post();
        $model=self::findLocationModel($id);
        $model->load($data);
        $model->name=MyFunc::createJson($model->name);
        $result=$model->save();
        if($result)
        return $this->redirect('/category/crud/location');
        else print_r($model->errors);
    }

    /**
	 * 更新分类树
	 * @return mixed
	 */

	public function actionUpdate() {

        if($data = Yii::$app->request->post('data')){

            $this->recursion($data,null); // 递归添加或更新
            //$this->recursion($data,0); // 递归添加或更新
        }

        Category::deleteAll(['not in', 'id', $this->ids]); // 删除页面上没有的ID
        return Url::toRoute('/category/crud');
	}



    /**
     * 存储树状数组到数据库
     * @param $tree
     * @param $pid
     */
    protected function recursion($tree, $pid){

        foreach($tree as $row){

            if(isset($row['title'])) {

                $model = $row['id'] ? $this->findModel($row['id']) : new EnergyCategory(); // 如果没有ID就是添加

                if (isset($model)) {

                    $model->parent_id = $pid;
                    $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                    $model->rgba = $row['rgba'];
                    if ($model->save() && $row['id'] == '') { // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉
                        $row['id'] = $model->primaryKey;
                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->recursion($row['children'], $row['id']);
                }
            }
        }
        return ;
    }


    /**
     * 存储树状数组到数据库
     * @param $tree
     * @param $pid
     */
    protected function location_recursion($tree, $pid){

        foreach($tree as $row){
            if(isset($row['title'])){
                $model = $row['id']?$this->findLocationModel($row['id']):new Location(); // 如果没有ID就是添加
                if(isset($model)){
                    $model->parent_id = $pid;
                    $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                    if(isset($row['rgba'])) {$rgba=$row['rgba'];}
                    else $rgba="#92cddc";
                    $model->rgba =$rgba ;

                    //设置type 默认值为0
                    //$model->type='0';
                   if(  $model->save()&& $row['id'] == ''){ // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉\

                        $row['id'] = $model->primaryKey;

                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->location_recursion( $row['children'], $row['id']);
                }
            }
        }

        return ;
    }



    public function actionGenerateComputePoint()
    {
        return Url::toRoute('/category/crud');
    }

	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */

	protected function findModel($id) {
		if (($model = EnergyCategory::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
    protected function findLocationModel($id) {
        if (($model = Location::findOne ( $id )) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException ( 'The requested page does not exist.' );
        }
    }
}
