<?php

namespace backend\module\category\controllers;

use backend\models\PointCategoryRel;
use Yii;
use backend\controllers\MyController;
use backend\models\PointData;
use backend\models\Category;
use common\library\MyFunc;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        $category_tree = MyFunc::TreeData(Category::find()->asArray()->all());
        $model = new Category();
        $model->load(Yii::$app->request->get());
        $model->validate();
        $PointDataTotal = $model->CategoryPointDataTotal();
        // 转换数据为HIGHTCHARTS的JSON格式
        foreach($PointDataTotal as $k => $row){
            $result[0]['data'][$k][] = MyFunc::DisposeJSON($row['name']);
            $result[0]['data'][$k][] = isset($row['pointData'][0]['total'])?intval($row['pointData'][0]['total']):0;
        }
        $result_json = isset($result)?json_encode($result, JSON_UNESCAPED_UNICODE):'[]';

        return $this->render('index', [
            'category_tree' => $category_tree,
            'model' => $model,
            'result_json' => $result_json
        ]);
    }

    /**
     * 缩放图形时异步载入
     */
    public function actionAjaxCategoryData()
    {
        $model = new Category();
        $model->load(Yii::$app->request->get(),'');
        $Point = $model->CategoryPointData();
        foreach($Point as $k => $row){
            $point_name[$k] = MyFunc::DisposeJSON($row['name']);
            $result[$k] = $row['pointData'];
        }
        // 好像有自带JSON和JSONP跨域请求，但是不知道怎么用
        echo isset($result)?MyFunc::HighChartsData($result, $point_name):'[]';
    }
}
