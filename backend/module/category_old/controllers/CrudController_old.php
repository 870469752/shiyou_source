<?php

namespace backend\module\category\controllers;

use backend\models\EnergyCategory;
use backend\models\Image;
use backend\models\ImageGroup;
use backend\models\Location;
use backend\models\Point;
use backend\models\PointCategoryRelNew;
use backend\models\Sub_System_Category;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use Yii;
use backend\models\Category;
use backend\models\search\CategorySearch;
use backend\controllers\MyController;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use yii\helpers\Url;

/**
 * Class CrudController
 * @package backend\module\category\controllers
 */
class CrudController extends MyController {
    /**
     * @var array 页面上存在的ID
     */
    //public $ids = [0];
    public $ids=[];
	public function behaviors() {
		return [
				'verbs' => [
						'class' => VerbFilter::className (),
						'actions' => [
								'delete' => [
										'post'
								]
						]
				]
		];
	}

	/**
	 * Lists all Category models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
        $data=EnergyCategory::find()
            ->select(['id','title'=>'name','parent_id','rgba'])
            ->OrderBy('id desc')
            ->asArray()->all();
        $tree = MyFunc::TreeData($data);
		return $this->render ( 'index', [
				'tree' => $tree
		] );
	}


    public function actionLocation() {

        $tree = MyFunc::TreeData(
            Category::find()
                ->select(['id','title'=>'name','parent_id','rgba'])
                ->where(['category_type'=>2])
                ->OrderBy('id desc')
                ->asArray()->all()
        );
        return $this->render ( 'location', [
            'tree' => $tree
        ] );
    }
    public function actionLocationShow() {

        $tree = MyFunc::TreeData(
            Location::find()
                ->select(['id','title'=>'name','parent_id'])
                ->OrderBy('id desc')
                ->asArray()->all()
        );
        return $this->render ( 'location_show', [
            'tree' => $tree
        ] );
    }

    public function actionLocationDetail($id) {
        $model=self::findLocationModel($id);
        $select_points=PointCategoryRelNew::find()->select('id')->where(['category_id'=>$id])->asArray()->all();
        $s_points=array();
        foreach ($select_points as $value) {
            $s_points[]=$value['id'];
        }
        //所有底图
        $image=Image::findAllImage();

        $image=MyFunc::_map($image,'id','name');

        //楼层绑定的底图
        $points=Point::findAllPoint();
        $points=MyFunc::_map($points, 'id', 'name');
        return $this->render ( 'location_update', [
                    'model'=>$model,
                    'points'=>$points,
                    's_points'=>$s_points,
                    'image'=>$image
        ] );
    }

    public function actionLocationView($id) {
        $model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();


        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;



        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;
        return $this->render ( 'location_view', [
                    'model'=>$model,
                    'location_id'=>$id,
                    'num'=>$points,
                    //底图
                    'image_base'=>$url,
                    'sub_system_in'=>$sub_system_in,
                    'all_sub_system'=>$all_sub_system,
//                  楼层内已经存在的子系统全部信息
                    'sub_system'=>$sub_system,
                    'all_image'=>$all_image
        ] );
    }
    public function actionSubsystem($id){
        $model =SubSystem::findModel($id);

        return $this->render('subsystem',[
            'model'=>$model
        ]);
    }

    public function actionLocationOnlyView($id) {
        $model =new SubSystem();
        $location=Location::findOne($id);
        $image_base=Image::findOne($location->image_id);
        $points=Point::find()->asArray()->all();
        $points=MyFunc::_map($points,'id','name');

        //所有的本楼层的子系统
        $sub_system=ImageGroup::find()->where(['location_id'=>$id])->asArray()->all();


        //子系统类别  sub_system_category内的值
        $sub_system_category=new Sub_System_Category();
        $all_sub_system=$sub_system_category->getAll();
        $all_sub_system=MyFunc::_map($all_sub_system,'id','name');


        //所有的图
        $all_image=new Image();
        $all_image=$all_image->findAllImage();


        $sub_system_in=$all_sub_system;



        if(empty($image_base)){
            $url='uploads/pic/map1.jpg';
        }

        else $url=$image_base->url;

//        echo '<pre>';
//        print_r($all_sub_system);
//        print_r($sub_system);
//        die;
        return $this->render ( 'location_only_view', [
            'model'=>$model,
            'location_id'=>$id,
            'num'=>$points,
            //底图
            'image_base'=>$url,
            'sub_system_in'=>$sub_system_in,
            'all_sub_system'=>$all_sub_system,
//                  楼层内已经存在的子系统全部信息
            'sub_system'=>$sub_system,
            'all_image'=>$all_image
        ] );
    }

    public function actionAjaxDelete(){
        $data=Yii::$app->request->post();
        $model = new SubSystem();

//        echo '<pre>';
        print_r($data);
        $model = $model->findOne($data['id']);
        $model->delete();
    }

    public function actionAjaxSave(){
        $data=Yii::$app->request->post();
        $location_id=$data['location_id'];

        $sub_systems_data=json_decode($data['data'],1);

        foreach ($sub_systems_data as $key=>$value) {

                $load_data['SubSystem'] = [
                    'name' => MyFunc::createJson($value['name']),
                    'data' => json_encode($value['data']),
                    'sub_system_type' => $value['category_id'],
                    'location_id' => $location_id

                ];

                //id为0则为新建子系统
                if($value['id']==0) {
                    $model = new SubSystem();
                }
                //否则为更新
                else {
                    $model=SubSystem::findModel($value['id']);
                }
            $model->load($load_data);
            $model->save();
//          print_r($model->errors);
            }

    }

    //属性框保存按钮  保存点位绑定信息
    public function actionAjaxSaveElement(){
        $data=Yii::$app->request->post();

//        echo '<pre>';
        $load_data['SubSystemElement'] = [
            'sub_system_id'=>$data['sub_system_id'],
            'element_id'=>$data['element_id'],
            'element_type'=>1,
            'config'=>json_encode(
                [
                    'data_type'=>'sub_system_element_config',
                    'data'=>['points'=>$data['binding_id']]
                ]
            )
        ];
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);
        $model->load($load_data);
        $model->save();
        print_r($model->errors);
    }

    //点击图标时显示查询已经绑定点位id
    public function actionAjaxGetElement(){
        $data=Yii::$app->request->post();
        $model=new SubSystemElement();
        $model=$model->findModelByData($data['sub_system_id'],$data['element_id']);

        $points=json_decode($model->config,1)['data']['points'];
        return $points;
        //return json_encode($points);

    }


    //点击图标时显示查询已经绑定点位id
    public function actionAjaxUpdateValue(){
        $data=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($data['data']);
        $data_temp=array();
        foreach ($data['data'] as $key=>$value) {
                if($value!=null) {
                    //$key 为对应的子系统id $element_id 为子系统内节点的key值
                    foreach ($value as $element_id) {
                        $config=SubSystemElement::find()->select('config')->where(['sub_system_id'=>$key,'element_id'=>$element_id])->asArray()->all();
                        if(!empty($config)) {

                            $point_id=json_decode($config[0]['config'],1)['data']['points'];
                            $point_value=Point::find()->select('value')->where(['id'=>$point_id])->asArray()->all();
                            if(!empty($value)){
                                $data_temp[$key][$element_id]=$point_value[0]['value'];
                            }

                        }
                    }
                }
            }

        return json_encode($data_temp);
    }
    public function actionLocationViewTest() {

        return $this->render ( 'location_view_test');
    }

    public function actionTest() {

        $model=new ImageGroup();
        return $this->render ('test',[
            'model'=>$model
        ]);
    }

    /**
     * 更新分类树
     * @return mixed
     */

    public function actionLocationUpdate() {

        if($data = Yii::$app->request->post('data')){
            $this->location_recursion($data, null); // 递归添加或更新

        }
        Location::deleteAll(['not in', 'id', $this->ids]); // 删除页面上没有的ID

        return Url::toRoute('/category/crud/location');
    }
    public function actionLocationDetailUpdate($id){
        $data = Yii::$app->request->post();
        $model=self::findLocationModel($id);
        $model->load($data);
        $model->name=MyFunc::createJson($model->name);
        $result=$model->save();
        if($result)
        return $this->redirect('/category/crud/location');
        else print_r($model->errors);
    }

    /**
	 * 更新分类树
	 * @return mixed
	 */

	public function actionUpdate() {

        if($data = Yii::$app->request->post('data')){

            $this->recursion($data,null); // 递归添加或更新
            //$this->recursion($data,0); // 递归添加或更新
        }

        Category::deleteAll(['not in', 'id', $this->ids]); // 删除页面上没有的ID
        return Url::toRoute('/category/crud');
	}



    /**
     * 存储树状数组到数据库
     * @param $tree
     * @param $pid
     */
    protected function recursion($tree, $pid){

        foreach($tree as $row){

            if(isset($row['title'])) {

                $model = $row['id'] ? $this->findModel($row['id']) : new EnergyCategory(); // 如果没有ID就是添加

                if (isset($model)) {

                    $model->parent_id = $pid;
                    $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                    $model->rgba = $row['rgba'];
                    if ($model->save() && $row['id'] == '') { // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉
                        $row['id'] = $model->primaryKey;
                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->recursion($row['children'], $row['id']);
                }
            }
        }
        return ;
    }


    /**
     * 存储树状数组到数据库
     * @param $tree
     * @param $pid
     */
    protected function location_recursion($tree, $pid){
//            echo '<pre>';
//        print_r($tree);

        foreach($tree as $row){
            if(isset($row['title'])){
                $model = $row['id']?$this->findLocationModel($row['id']):new Location(); // 如果没有ID就是添加
                if(isset($model)){
                    $model->parent_id = $pid;
                    $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                    if(isset($row['rgba'])) {$rgba=$row['rgba'];}
                    else $rgba="#92cddc";
                    $model->rgba =$rgba ;

                    //设置type 默认值为0
                    //$model->type='0';
                   if(  $model->save()&& $row['id'] == ''){ // 添加完并且把主键ID拿过来，防止被认为页面上没有而删掉\

                        $row['id'] = $model->primaryKey;

                    }
                    unset($model);
                }
                $this->ids[] = $row['id']; // 把不需要删除的ID加进去
                if (!empty($row['children'])) {
                    $this->location_recursion( $row['children'], $row['id']);
                }
            }
        }

        return ;
    }



    public function actionGenerateComputePoint()
    {
        return Url::toRoute('/category/crud');
    }

	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */

	protected function findModel($id) {
		if (($model = EnergyCategory::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
    protected function findLocationModel($id) {
        if (($model = Location::findOne ( $id )) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException ( 'The requested page does not exist.' );
        }
    }
}
