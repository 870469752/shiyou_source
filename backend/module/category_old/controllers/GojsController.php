<?php

namespace backend\module\category\controllers;

use backend\models\PointCategoryRel;
use Yii;
use backend\controllers\MyController;
use backend\models\PointData;
use backend\models\Category;
use common\library\MyFunc;
use backend\models\ImageGroup;

class GojsController extends MyController
{
    public function actionIndex()
    {
        $category_tree = MyFunc::TreeData(Category::find()->asArray()->all());
        $model = new Category();
        $model->load(Yii::$app->request->get());
        $model->validate();
        $PointDataTotal = $model->CategoryPointDataTotal();
        // 转换数据为HIGHTCHARTS的JSON格式
        foreach($PointDataTotal as $k => $row){
            $result[0]['data'][$k][] = MyFunc::DisposeJSON($row['name']);
            $result[0]['data'][$k][] = isset($row['pointData'][0]['total'])?intval($row['pointData'][0]['total']):0;
        }
        $result_json = isset($result)?json_encode($result, JSON_UNESCAPED_UNICODE):'[]';

        return $this->render('index', [
            'category_tree' => $category_tree,
            'model' => $model,
            'result_json' => $result_json
        ]);
    }



    public function actionColumn() {

        $model=new ImageGroup();
        return $this->render ('gojs_test_column',[
            'model'=>$model
        ]);
    }
    public function actionScrollingTable() {

        $model=new ImageGroup();
        return $this->render ('gojs_test_scrolling',[
            'model'=>$model
        ]);
    }
    public function actionDataVisualization() {

        $model=new ImageGroup();
        return $this->render ('data_visualization',[
            'model'=>$model
        ]);
    }
    public function actionSelectTable() {

        $model=new ImageGroup();
        return $this->render ('selectablefields',[
            'model'=>$model
        ]);
    }
}
