<?php
use Yii\web\View;

?>
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<article class="col-sm-12">
			<!-- new widget -->
			<div class="jarviswidget" data-widget-togglebutton="false"
				data-widget-editbutton="false" data-widget-fullscreenbutton="false"
				data-widget-colorbutton="false" data-widget-deletebutton="false">
				<header>
					<span class="widget-icon">
						<i class="glyphicon glyphicon-stats txt-color-darken"></i>
					</span>
					<h2><?= YII::t('plugin', 'Energy trend') ?></h2>

					<ul class="nav nav-tabs pull-right in" id="myTab">
						<li class="active">
							<a data-toggle="tab" href="#s1">
								<i class="fa fa-eye"></i>
								<span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Energy Monitor') ?></span>
							</a>
						</li>

						<li>
							<a data-toggle="tab" href="#s2">
								<i class="fa fa-clock-o"></i>
								<span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Formerly situation') ?></span>
							</a>
						</li>

						<li>
							<a data-toggle="tab" href="#s3">
								<i class="fa fa-rmb"></i>
								<span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Historical spending') ?></span>
							</a>
						</li>
					</ul>

				</header>

				<!-- widget div-->
				<div class="no-padding">

						<!-- content -->
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in padding-10" id="s1">
								<div class="row no-space">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
										<div id="updating-chart" class="chart-large has-legend-unique"></div>

									</div>
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats">

										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
												<span class="text">
													<i class="fa fa-bolt fa-fw fa-lg"></i>
													<?= YII::t('plugin', 'Electric')?>
													<span class="pull-right">130/200 KWH</span>
												</span>
												<div class="progress">
													<div class="progress-bar bg-color-orangeDark"
														style="width: 65%;"></div>
												</div>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
												<span class="text">
													<i class="fa fa-tint fa-fw fa-lg"></i>
													<?= YII::t('plugin', 'Water')?>
													<span class="pull-right">100/300 T</span>
												</span>
												<div class="progress">
													<div class="progress-bar bg-color-blue" style="width: 34%;"></div>
												</div>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
												<span class="text">
													<i class="fa fa-fire fa-fw fa-lg"></i>
													<?= YII::t('plugin', 'Gas')?>
													<span class="pull-right">100/200 m³</span>
												</span>
												<div class="progress">
													<div class="progress-bar bg-color-greenLight"
														style="width: 77%;"></div>
												</div>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
												<span class="text">
													<i class="fa fa-spotify fa-fw fa-lg"></i>
													<?= YII::t('plugin', 'CO2')?>
													<span class="pull-right">100/300 T</span>
												</span>
												<div class="progress">
													<div class="progress-bar bg-color-blueDark"
														style="width: 84%;"></div>
												</div>
											</div>

											<span
												class="btn-group btn-group-justified col-xs-6 col-sm-6 col-md-12 col-lg-12">
												<a href="javascript:void(0);"
													class="btn btn-default btn-block hidden-xs"><?= YII::t('plugin', 'Today') ?></a>
												<a href="javascript:void(0);"
													class="btn btn-default btn-block hidden-xs"><?= YII::t('plugin', 'This month') ?></a>
												<a href="javascript:void(0);"
													class="btn btn-default btn-block hidden-xs"><?= YII::t('plugin', 'This year') ?></a>
											</span>

										</div>

									</div>
								</div>

								<div class="show-stat-microcharts">
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

										<div class="easy-pie-chart txt-color-orangeDark"
											data-percent="33" data-pie-size="50">
											<span class="percent percent-sign"></span>
										</div>
										<span class="easy-pie-title">
											<?= YII::t('plugin', 'Residue electric')?>
											<i class="fa fa-bolt fa-fw fa-lg"></i>
										</span>
										<ul class="smaller-stat hidden-sm pull-right">
											<li>
												<span class="label bg-color-blue">
													<i class="fa fa-caret-up"></i>
													97
												</span>
											</li>
											<li>
												<span class="label bg-color-blueLight">
													<i class="fa fa-caret-down"></i>
													44
												</span>
											</li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
										<div class="easy-pie-chart txt-color-blue" data-percent="78.9"
											data-pie-size="50">
											<span class="percent percent-sign"></span>
										</div>
										<span class="easy-pie-title">
											<?= YII::t('plugin', 'Residue gas')?>
											<i class="fa fa-tint fa-fw fa-lg"></i>
										</span>
										<ul class="smaller-stat hidden-sm pull-right">
											<li>
												<span class="label bg-color-blue">
													<i class="fa fa-caret-up"></i>
													11
												</span>
											</li>
											<li>
												<span class="label bg-color-blueLight">
													<i class="fa fa-caret-down"></i>
													11
												</span>
											</li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
										<div class="easy-pie-chart txt-color-greenLight"
											data-percent="23" data-pie-size="50">
											<span class="percent percent-sign"></span>
										</div>
										<span class="easy-pie-title">
											<?= YII::t('plugin', 'Residue trend')?>
											<i class="fa fa-fire fa-fw fa-lg"></i>
										</span>
										<ul class="smaller-stat hidden-sm pull-right">
											<li>
												<span class="label bg-color-blue">
													<i class="fa fa-caret-up"></i>
													10
												</span>
											</li>
											<li>
												<span class="label bg-color-blueLight">

													<i class="fa fa-caret-down"></i>
													10
												</span>
											</li>
										</ul>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
										<div class="easy-pie-chart txt-color-blueDark"
											data-percent="36" data-pie-size="50">
											<span class="percent percent-sign"></span>
										</div>
										<span class="easy-pie-title">
											<?= YII::t('plugin', 'Residue CO2')?>
											<i class="fa fa-spotify fa-fw fa-lg"></i>
										</span>
										<ul class="smaller-stat hidden-sm pull-right">
											<li>
												<span class="label bg-color-blue">
													<i class="fa fa-caret-up"></i>
													13
												</span>
											</li>
											<li>
												<span class="label bg-color-blueLight">
													<i class="fa fa-caret-down"></i>
													13
												</span>
											</li>
										</ul>
									</div>
								</div>

							</div>
							<!-- end s1 tab pane -->

							<div class="tab-pane fade padding-10" id="s2">
								<div id="statsChart" class="has-legend-unique"></div>
							</div>
							<!-- end s2 tab pane -->

							<div class="tab-pane fade padding-10" id="s3">
								<div id="flotcontainer" class="has-legend-unique"></div>
							</div>
							<!-- end s3 tab pane -->
						</div>

						<!-- end content -->

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
			<!-- new widget -->
			<div class="jarviswidget" data-widget-colorbutton="false"
				data-widget-editbutton="false" data-widget-deletebutton="false">

				<header>
					<span class="widget-icon">
						<i class="fa fa-map-marker"></i>
					</span>
					<h2><?= YII::t('plugin', 'Area Overview') ?></h2>
					<div class="widget-toolbar hidden-mobile">
						<div class="btn-group">
							<button class="btn dropdown-toggle btn-xs btn-default"
								data-toggle="dropdown"><?= YII::t('plugin', 'Today')?><i
									class="fa fa-caret-down fa-fw"></i>
							</button>
							<ul class="dropdown-menu js-status-update pull-right">
								<li class="active">
									<a href="javascript:void(0);" id="mt"><?= YII::t('plugin', 'Today')?></a>
								</li>
								<li class="">
									<a href="javascript:void(0);" id="ag"><?= YII::t('plugin', 'This month')?></a>
								</li>
								<li class="">
									<a href="javascript:void(0);" id="td"><?= YII::t('plugin', 'This year')?></a>
								</li>
							</ul>
						</div>
					</div>
				</header>

				<!-- widget div-->
				<div>

					<div class="widget-body no-padding">
						<!-- content goes here -->

						<div id="wrapper">
							<div id="container"></div>
							<div id="info">
								<span class="f32"><span id="flag"></span></span>
								<h2></h2>
								<div id="country-chart"></div>
							</div>
						</div>

						<table class="table table-striped table-hover no-footer">
							<thead>
								<tr>
									<th>
										<a class="txt-color-darken" href="javascript:void(0)">
											<i class="fa-fw fa fa-building fa-lg"></i><?=  YII::t('plugin', 'Name') ?><i
												class="fa fa-sort fa-fw"></i>
										</a>
									</th>
									<th class="text-align-center">
										<a class="txt-color-darken" href="javascript:void(0)">
											<i class="fa fa-bolt fa-fw fa-lg"></i><?=  YII::t('plugin', 'Electric') ?><i
												class="fa fa-sort fa-fw"></i>
										</a>
									</th>
									<th class="text-align-center">
										<a class="txt-color-darken" href="javascript:void(0)">
											<i class="fa fa-tint fa-fw fa-lg"></i><?=  YII::t('plugin', 'Water') ?><i
												class="fa fa-sort fa-fw"></i>
										</a>
									</th>
									<th class="text-align-center">
										<a class="txt-color-darken" href="javascript:void(0)">
											<i class="fa fa-fire fa-fw fa-lg"></i><?=  YII::t('plugin', 'Gas') ?><i
												class="fa fa-sort fa-fw"></i>
										</a>
									</th>
									<th class="text-align-center">
										<a class="txt-color-darken" href="javascript:void(0)">
											<i class="fa fa-spotify fa-fw fa-lg"></i><?=  YII::t('plugin', 'CO2') ?><i
												class="fa fa-sort fa-fw"></i>
										</a>
									</th>
									<th class="text-align-center">
										<a class="txt-color-darken" href="javascript:void(0)">
									¥ <?=  YII::t('plugin', 'Total') ?><i class="fa fa-sort fa-fw"></i>
										</a>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>china</td>
									<td class="text-align-center">4977 KWH/m²</td>
									<td class="text-align-center">4977 T/m²</td>
									<td class="text-align-center">4977 m³/m²</td>
									<td class="text-align-center">4977 T/m²</td>
									<td class="text-align-center">¥ 4977</td>
								</tr>
								<tr>
									<td>Mongolia</td>
									<td class="text-align-center">4977 KWH/m²</td>
									<td class="text-align-center">4977 T/m²</td>
									<td class="text-align-center">4977 m³/m²</td>
									<td class="text-align-center">4977 T/m²</td>
									<td class="text-align-center">¥ 4977</td>
								</tr>
							</tbody>

						</table>

						<!-- end content -->
						<div class="dt-toolbar-footer">
							<div class="col-sm-6 col-xs-12 hidden-xs dataTables_info">
								第
								<b>1-10</b>
								条，共
								<b>11</b>
								条数据.
							</div>
							<div class="col-sm-6 col-xs-12">
								<ul class="pagination">
									<li class="prev disabled">
										<span>«</span>
									</li>
									<li class="active">
										<a href="#" data-page="0">1</a>
									</li>
									<li>
										<a href="#" data-page="1">2</a>
									</li>
									<li class="next">
										<a href="#" data-page="1">»</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
	</div>
	<!-- end row -->
</section>

<link rel="stylesheet" type="text/css" href="http://cloud.github.com/downloads/lafeber/world-flags-sprite/flags32.css">

<?php
$this->registerJsFile('js/highcharts/highcharts.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/map.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/world.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$js_content = <<<JAVASCRIPT
Highcharts.setOptions({
	global: {     
    	useUTC: false     
    },
	lang: {
		months: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
		shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
		weekdays: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'],
	}
});
$.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=world-population-history.csv&callback=?', function (csv) {

        // Parse the CSV Data
        /*Highcharts.data({
            csv: data,
            switchRowsAndColumns: true,
            parsed: function () {
                console.log(this.columns);
            }
        });*/

        // Very simple and case-specific CSV string splitting
        function CSVtoArray(text) {
            return text.replace(/^"/, '')
                .replace(/",$/, '')
                .split('","');
        };

        csv = csv.split(/\\n/);

        var countries = {},
            mapChart,
            countryChart,
            numRegex = /^[0-9\\.]+$/,
            quoteRegex = /\\"/g,
            categories = CSVtoArray(csv[1]).slice(4);

        // Parse the CSV into arrays, one array each country
        $.each(csv.slice(2), function (j, line) {
            var row = CSVtoArray(line),
                data = row.slice(4);

            $.each(data, function (i, val) {
                
                val = val.replace(quoteRegex, '');
                if (numRegex.test(val)) {
                    val = parseInt(val);
                } else if (!val) {
                    val = null;
                }
                data[i] = val;
            });
            countries[row[1]] = {
                name: row[0],
                code3: row[1],
                data: data
            };
        });

        // For each country, use the latest value for current population
        var data = [];
        for (var code3 in countries) {
            var value = null,
                year,
                itemData = countries[code3].data,
                i = itemData.length;

            while (i--) {
                if (typeof itemData[i] === 'number') {
                    value = itemData[i];
                    year = categories[i];
                    break;
                }
            }
            data.push({
                name: countries[code3].name,
                code3: code3,
                value: value,
                year: year
            });
        }
        
        // Add lower case codes to the data set for inclusion in the tooltip.pointFormat
        var mapData = Highcharts.geojson(Highcharts.maps['custom/world']);
        $.each(mapData, function () {
            this.id = this.properties['hc-key']; // for Chart.get()
            this.flag = this.id.replace('UK', 'GB').toLowerCase();
        });

        // Wrap point.select to get to the total selected points
        Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

            proceed.apply(this, Array.prototype.slice.call(arguments, 1));

            var points = mapChart.getSelectedPoints();

            if (points.length) {
                if (points.length === 1) {
                    $('#info #flag').attr('class', 'flag ' + points[0].flag);
                    $('#info h2').html(points[0].name);
                } else {
                    $('#info #flag').attr('class', 'flag');
                    $('#info h2').html('区域比较');

                }

                if (!countryChart) {
                    countryChart = $('#country-chart').highcharts({
                        chart: {
                            height: 250,
                            spacingLeft: 0
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        subtitle: {
                            text: null
                        },
                        xAxis: {
                            tickPixelInterval: 50,
                            crosshair: true
                        },
                        yAxis: {
                            title: null,
                            opposite: true
                        },
                        tooltip: {
                            shared: true
                        },
                        plotOptions: {
                            series: {
                                animation: {
                                    duration: 500
                                },
                                marker: {
                                    enabled: false
                                },
                                threshold: 0,
                                pointStart: parseInt(categories[0]),
                            }
                        }
                    }).highcharts();
                }

                $.each(points, function (i) {
                    // Update
                    if (countryChart.series[i]) {
                        /*$.each(countries[this.code3].data, function (pointI, value) {
                            countryChart.series[i].points[pointI].update(value, false);
                        });*/
                        countryChart.series[i].update({
                            name: this.name,
                            data: countries[this.code3].data,
                            type: points.length > 1 ? 'line' : 'area'
                        }, false);
                    } else {
                        countryChart.addSeries({
                            name: this.name,
                            data: countries[this.code3].data,
                            type: points.length > 1 ? 'line' : 'area'
                        }, false);
                    }
                });
                while (countryChart.series.length > points.length) {
                    countryChart.series[countryChart.series.length - 1].remove(false);
                }
                countryChart.redraw();

            } else {
                $('#info #flag').attr('class', '');
                $('#info h2').html('');
                $('#info .subheader').html('');
                if (countryChart) {
                    countryChart = countryChart.destroy();
                }
            }

            

        });
        
        // Initiate the map chart
        mapChart = $('#container').highcharts('Map', {
            
            title : {
                text : '区域能耗图'
            },

            subtitle: {
                text: '按住 Shift 并点击可多个对比'
            },
			credits: {
				enabled : false
			},
            mapNavigation: {
                enabled: true,
                buttonOptions: {
                    verticalAlign: 'bottom'
                }
            },

            colorAxis: {
                type: 'logarithmic',
                endOnTick: false,
                startOnTick: false,
                min: 50000
            },

            tooltip: {
                footerFormat: '<span style="font-size: 10px">点击查看详情</span>'
            },

            series : [{
                data : data,
                mapData: mapData,
                joinBy: ['iso-a3', 'code3'],
                name: 'Current population',
                allowPointSelect: true,
                cursor: 'pointer',
                states: {
                    select: {
                        color: '#a4edba',
                        borderColor: 'black',
                        dashStyle: 'shortdot'
                    }
                }
            }]
        }).highcharts();

        // Pre-select a country
        mapChart.get('us').select();
    });

	// 区域图
    $('#updating-chart').highcharts({
        chart: {
            type: 'area'
        },
		credits: {
			enabled : false
		},
        title: {
            text: ''
        },legend: {
            enabled : false
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            pointFormat: '<b>{point.y:,.0f}</b> T'
        },
        plotOptions: {
            area: {
                pointStart: 1411896796*1000,
				pointInterval: 60*1000,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            data: [123,34,345,234,432,234,54]
        }]
    });

    // 缩放图
    function afterSetExtremes(e) {

        var chart = $('#statsChart').highcharts();

        chart.showLoading('加载中，请稍后。');
        $.getJSON('http://www.highcharts.com/samples/data/from-sql.php?start=' + Math.round(e.min) +
                '&end=' + Math.round(e.max) + '&callback=?', function (data) {

                chart.series[0].setData(data);
                chart.hideLoading();
            });
    }

    // See source code from the JSONP handler at https://github.com/highslide-software/highcharts.com/blob/master/samples/data/from-sql.php
    $.getJSON('http://www.highcharts.com/samples/data/from-sql.php?callback=?', function (data) {

        // Add a null value for the end date
        data = [].concat(data, [[Date.UTC(2011, 9, 14, 19, 59), null, null, null, null]]);

        // create the chart
        $('#statsChart').highcharts('StockChart', {
            chart : {
                type: 'area',
                zoomType: 'x'
            },

            navigator : {
                adaptToUpdatedData: false,
                series : {
                    data : data
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            title: {
                text: ''
            },
			credits: {
				enabled: false
			},
            rangeSelector : {
                enabled: false
            },

            xAxis : {
                events : {
                    afterSetExtremes : afterSetExtremes
                },
                minRange: 3600 * 1000 // one hour
            },

            yAxis: {
                floor: 0
            },

            series : [{
                data : data,
                dataGrouping: {
                    enabled: false
                }
            }]
        });
		
		// 支出
		$('#flotcontainer').highcharts('StockChart', {
            chart : {
                type: 'area',
                zoomType: 'x'
            },

            navigator : {
                adaptToUpdatedData: false,
                series : {
                    data : data
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            title: {
                text: ''
            },
			credits: {
				enabled: false
			},
            rangeSelector : {
                enabled: false
            },

            xAxis : {
                events : {
                    afterSetExtremes : afterSetExtremes
                },
                minRange: 3600 * 1000 // one hour
            },

            yAxis: {
                floor: 0
            },

            series : [{
                data : data,
                dataGrouping: {
                    enabled: false
                }
            }]
        });
    });
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);

$this->registerCss ( '
#wrapper {
	height: 500px;
	width: 1000px;
	margin: 0 auto;
	padding: 0;
}
#container {
	float: left;
	height: 500px; 
	width: 700px; 
	margin: 0;
}
#info {
	float: left;
	width: 270px;
	padding-left: 20px;
	margin: 100px 0 0 0;
	border-left: 1px solid silver;
}
#info h2 {
	display: inline;
}
#info .f32 .flag {
	vertical-align: bottom !important;
}

#info h4 {
	margin: 1em 0 0 0;
}
');
?>