<?php

use yii\widgets\ActiveForm;
use common\library\MyHtml;

?>


<div class="well well-sm">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'well padding-bottom-10']]); ?>
        <?= $form->field($model, 'content', ['template' => '{input}'])->textarea(['placeholder' => '您想说点什么']) ?>
        <?= $form->field($model, 'user_id', ['template' => '{input}'])->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>
        <?= $form->field($model, 'timestamp', ['template' => '{input}'])->hiddenInput() ?>
        <?= MyHtml::submitButton('发送', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>

    <?php foreach($message as $v):?>
        <div style="overflow: hidden" class="padding-10 fa-border">
            <div class="pull-left">
                <img src="/img/avatars/male.png">
            </div>
            <div>
                <div class="row" style="overflow: hidden">
                    <div class="col-xs-6"><?= $v['user']['username'] ?></div>
                    <div class="col-xs-6 text-align-right"><?= $v['timestamp'] ?></div>
                </div>
                <div class="row" style="overflow: hidden">
                    <div class="col-xs-12" style="word-wrap: break-word; ">
                        <?= $v['content'] ?>
                    </div>
                    <ul class="list-inline font-xs col-xs-12">
                        <li>
                            <a href="javascript:void(0);" class="text-info"><i class="fa fa-reply"></i> 回复</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i> 赞同 (32)</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="text-muted">显示所有评论 (3)</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="text-primary">修改</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="text-danger">删除</a>
                        </li>
                    </ul>
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <img src="/img/avatars/male.png">
                        </div>
                        <div>
                            <div class="row" style="overflow: hidden">
                                <div class="col-xs-12"><?= $v['user']['username'] ?></div>
                            </div>
                            <div class="row" style="overflow: hidden">
                                <div class="col-xs-12" style="word-wrap: break-word; ">
                                    <?= $v['content'] ?>
                                </div>
                                <ul class="list-inline font-xs col-xs-12">
                                    <li>
                                        <span class="text-muted"><?= $v['timestamp'] ?></span>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i> 赞同 (32)</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="text-primary">修改</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="text-danger">删除</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach?>


</div>
<script>
    window.onload = function () {
        $('form').submit(function(){
            $("[name='<?= $model->formName() ?>[timestamp]']").val(TimestampToDate(''));
        })
    }
</script>