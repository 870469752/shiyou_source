<?php
use \yii\helpers\Url;
use common\library\MyHtml;
?>
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <div class="jarviswidget" data-widget-editbutton="false"
                 data-widget-deletebutton="false" data-widget-colorbutton="false">
                <header>
                    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>

                    <h2>首页配置</h2>

                </header>
                <div class="widget-body">

                    <?= MyHtml::beginForm() ?>
                    <div class="form-group">
                        <label class="control-label">请选择温度点位</label>
                        <?= MyHtml::dropDownList('temperature', @$plugin_config['temperature'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择湿度点位</label>
                        <?= MyHtml::dropDownList('humidity', @$plugin_config['humidity'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择COP点位</label>
                        <?= MyHtml::dropDownList('cop', @$plugin_config['cop'], $point,['class' => 'select2 margin-bottom-10', 'multiple' => true]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择功率点位</label>
                        <?= MyHtml::dropDownList('power', @$plugin_config['power'], $point,['class' => 'select2 margin-bottom-10', 'multiple' => true]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择冷量点位</label>
                        <?= MyHtml::dropDownList('cooling', @$plugin_config['cooling'], $point,['class' => 'select2 margin-bottom-10', 'multiple' => true]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择电量点位</label>
                        <?= MyHtml::dropDownList('electricity', @$plugin_config['electricity'], $point,['class' => 'select2 margin-bottom-10', 'multiple' => true]) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择累计冷量点位</label>
                        <?= MyHtml::dropDownList('refrigeration', @$plugin_config['refrigeration'], $point,['class' => 'select2 margin-bottom-10', 'multiple' => true]) ?>
                    </div>
                    <?= MyHtml::submitButton('保存配置', ['class' => 'btn btn-success']) ?>
                    <?= MyHtml::endForm() ?>
                </div>
            </div>
        </article>
    </div>
</section>