<?php
use Yii\web\View;
use \yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\library\MyHtml;

?>

<section id="widget-grid" class="">
<input type="hidden" id="csrf" value="<?= Yii::$app->request->csrfToken ?>">
    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget" data-widget-togglebutton="false"
                data-widget-editbutton="false" data-widget-fullscreenbutton="false"
                data-widget-colorbutton="false" data-widget-deletebutton="false" style="margin-bottom:10px;">
                <header>
                    <span class="widget-icon">
                        <i class="glyphicon glyphicon-stats txt-color-darken"></i>
                    </span>
                    <h2><?= YII::t('plugin', 'Energy trend') ?></h2>

                    <ul class="nav nav-tabs pull-right in" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#s1">
                                <i class="fa fa-eye"></i>
                                <span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Energy Monitor') ?></span>
                            </a>
                        </li>

<!--                         <li>
                            <a data-toggle="tab" href="#s2">
                                <i class="fa fa-clock-o"></i>
                                <span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Formerly situation') ?></span>
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s3">
                                <i class="fa fa-rmb"></i>
                                <span class="hidden-mobile hidden-tablet"><?= YII::t('plugin', 'Historical spending') ?></span>
                            </a>
                        </li> -->
                    </ul>

                </header>

                <!-- widget div-->
                <div class="no-padding">

                        <!-- content -->
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in padding-10" id="s1">
                                <div class="row no-space">
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">


                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" style="margin-bottom:0px;">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
        
                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"
        
                        -->

                        <!-- widget div-->
                        <div>
        
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
        
                            </div>
                            <!-- end widget edit box -->
        
                            <!-- widget content -->
                            <div class="widget-body">
        
                                <div class="tabs-left" style="margin-left:-25px;">
                                    <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" style="padding-left:11px;width:60px;margin-left:0px;margin-right:0px;">
                                        <li class="active">
                                            <a href="#tab-r1" data-toggle="tab" value="electric"><span class="text">
<!--                                                <i class="fa fa-bolt fa-fw fa-lg"></i>-->
                                                <?= Yii::t('plugin', 'Electric')?>
                                            </span></a>
                                        </li>
                                        <li>
                                            <a href="#tab-r2" data-toggle="tab" value="water"><span class="text">
<!--                                                <i class="fa fa-tint fa-fw fa-lg"></i>-->
                                                <?= Yii::t('plugin', 'Water')?>
                                            </span></a>
                                        </li>
                                        <li>
                                            <a href="#tab-r3" data-toggle="tab" value="gas"><span class="text">
<!--                                                <i class="fa fa-fire fa-fw fa-lg"></i>-->
                                                <?= Yii::t('plugin', 'Gas')?>
                                            </span></a>
                                        </li>                                        
                                        <li>
                                            <a href="#tab-r4" data-toggle="tab" value="co2"><span class="text">
<!--                                                <i class="fa fa-spotify fa-fw fa-lg"></i>-->
                                                    CO<sub>2</sub>
<!--                                                --><?//= Yii::t('plugin', 'CO2')?>
                                            </span></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" style="margin-left:60px;">
                                        <span id="converter" style="float:right;margin-bottom:px;">
                                            <select id="subconverter">
                                                <option value="1">年</option>
                                                <option value="2">月</option>
                                                <option value="3" selected>日</option>
                                            </select>
                                        </span>
                                        <div style="width:auto;height:25px;"></div>
                                        <div class="tab-pane active" id="tab-r1" value="electric">
                                            <div id="tab-one-electric" class="" style="margin:auto;height:230px;">
                                                <div id="container_electric" class="containera"></div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-r2" value="water">
                                            <div id="tab-one-water" class="" style="margin:auto;height:230px;">
                                                <div id="container_water" class="containera"></div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-r3" value="gas">
                                            <div id="tab-one-gas" class="" style="margin:auto;height:230px;">
                                                <div id="container_gas" class="containera"></div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab-r4" value="co2">
                                            <div id="tab-one-co2" class="" style="margin:auto;height:230px;">
                                                <div id="container_co2" class="containera"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                            <!-- end widget content -->
        
                        </div>
                        <!-- end widget div -->
        
                    </div>
                    <!-- end widget -->
                                    

                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats  tabbable tabs-below" style="padding-left:20px;">

                                    <div class="tab-content padding-10">

                                        <div class="row tab-pane active" id="currentmonth" style="margin-top:15px;margin-bottom:30px;">
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-bolt fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Electric')?>
                                                    <span class="pull-right"><?= $monthdata[29] ?>/20000 KWH</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-orangeDark"
                                                        style="width: <?= 100*$monthdata[29]/20000?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-tint fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Water')?>
                                                    <span class="pull-right"><?= $monthdata[18]?>/30000 T</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-blue" style="width: <?= 100*$monthdata[18]/30000?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-fire fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Gas')?>
                                                    <span class="pull-right"><?= $monthdata[13]?>/200 m³</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-greenLight"
                                                        style="width: <?= 100*$monthdata[13]/200?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-spotify fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'CO2')?>
                                                    <span class="pull-right"><?= $monthdata[178]?>/300000 T</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-blueDark"
                                                        style="width: <?= 100*$monthdata[178]/300000?>%;"></div>
                                                </div>
                                            </div>


                                        </div>



                                        <div class="row tab-pane" id="currentyear" style="margin-top:15px;margin-bottom:30px;">
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-bolt fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Electric')?>
                                                    <span class="pull-right"><?= $yeardata[29]?>/300000 KWH</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-orangeDark"
                                                         style="width: <?= 100*$yeardata[29]/300000?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-tint fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Water')?>
                                                    <span class="pull-right"><?= $yeardata[18]?>/3000000 T</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-blue" style="width: <?= 100*$yeardata[18]/300000?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-fire fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'Gas')?>
                                                    <span class="pull-right"><?= $yeardata[13]?>/200 m³</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-greenLight"
                                                         style="width: <?= $yeardata[13]/200?>%;"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <span class="text">
                                                    <i class="fa fa-spotify fa-fw fa-lg"></i>
                                                    <?= YII::t('plugin', 'CO2')?>
                                                    <span class="pull-right"><?= $yeardata[178]?>/600000 T</span>
                                                </span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-color-blueDark"
                                                         style="width: <?= 100*$yeardata[178]/600000?>%;"></div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                        <ul class="nav nav-tabs">
                                            <!--                                                 <a href="javascript:void(0);"
                                                    class="btn btn-default btn-block hidden-xs"><?= YII::t('plugin', 'Today') ?></a> -->
                                            <li class="active pull-right"><a id="first" data-toggle="tab" href="#currentmonth"
                                                                             class="btn btn-default btn-block hidden-xs" value="1"><?= YII::t('plugin', 'This month') ?></a></li>

                                            <li class="pull-right"><a id="second" data-toggle="tab" href="#currentyear"
                                                                      class="btn btn-default btn-block hidden-xs" value="2"><?= YII::t('plugin', 'This year') ?></a></li>

                                        </ul>

                                    </div>
                                </div>


                            </div>
                            <!-- end s1 tab pane -->

                            <div class="tab-pane fade padding-10" id="s2">
                                <div id="statsChart" class="has-legend-unique"></div>
                            </div>
                            <!-- end s2 tab pane -->

                            <div class="tab-pane fade padding-10" id="s3">
                                <div id="flotcontainer" class="has-legend-unique"></div>
                            </div>
                            <!-- end s3 tab pane -->
                        </div>

                        <!-- end content -->

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
            <!-- new widget -->
            <div class="jarviswidget" data-widget-colorbutton="false"
                data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsebutton="false">

                <header>
                    <span class="widget-icon">
                        <i class="fa fa-map-marker"></i>
                    </span>
                    <h2><?= YII::t('plugin', 'Area Overview') ?></h2>
                    <div class="widget-toolbar hidden-mobile">
                        <div class="btn-group">
                            <button id="custombutton" class="btn dropdown-toggle btn-xs btn-default"
                                data-toggle="dropdown"><?= YII::t('plugin', 'Today')?><!--<i
                                    class="fa fa-caret-down fa-fw"></i>-->
                            </button>
                            <ul class="dropdown-menu js-status-update pull-right">
                                <li class="">
                                    <a href="javascript:void(0);" id="mt"><?= YII::t('plugin', 'Today')?></a>
                                </li>
                                <li class="">
                                    <a href="javascript:void(0);" id="ag"><?= YII::t('plugin', 'This month')?></a>
                                </li>
                                <li class="">
                                    <a href="javascript:void(0);" id="td"><?= YII::t('plugin', 'This year')?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>

                <!-- widget div-->
                <div>

                    <div class="widget-body no-padding">

                        <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa-fw fa fa-building fa-lg"></i><?=  YII::t('plugin', 'NumberName') ?><i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th>
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa-fw fa fa-building fa-lg"></i><?=  YII::t('plugin', 'BuildingName') ?><i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th>
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa-fw fa fa-building fa-lg"></i><?=  YII::t('plugin', 'FloorName') ?><i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th class="text-align-center">
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa fa-bolt fa-fw fa-lg"></i><?=  YII::t('plugin', 'Electric') ?>(KWH)<i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th class="text-align-center">
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa fa-tint fa-fw fa-lg"></i><?=  YII::t('plugin', 'Water') ?>(T)<i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th class="text-align-center">
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa fa-fire fa-fw fa-lg"></i><?=  YII::t('plugin', 'Gas') ?>(m<super>3</super>)<i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                    <th class="text-align-center">
                                        <a class="txt-color-darken" href="javascript:void(0)">
                                            <i class="fa fa-spotify fa-fw fa-lg"></i><?=  YII::t('plugin', 'CO2') ?>(T)<i
                                                class="fa fa-sort fa-fw"></i>
                                        </a>
                                    </th>
                                </tr>

                            </thead>
                            <tbody>
                                <?php
                                    foreach ($tableDataDay as $key => $value) {
                                        echo '<tr>';
                                        foreach ($value as $key1 => $value1) {
                                            switch ($key1){
                                                case 3:
                                                    echo '<td class="text-align-center">'.$value1.' </td>';
                                                    break;
                                                case 4:
                                                    echo '<td class="text-align-center">'.$value1.' </td>';
                                                    break;
                                                case 5:
                                                    echo '<td class="text-align-center">'.$value1.' </td>';
                                                    break;
                                                case 6:
                                                    echo '<td class="text-align-center">'.$value1.' </td>';
                                                    break;
                                                default:
                                                    echo '<td class="text-align-center">'.$value1.' </td>';
                                                    break;
                                            }

                                        }
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>

                        </table>

                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>
    </div>
    <!-- end row -->
</section>




<link rel="stylesheet" type="text/css" href="http://cloud.github.com/downloads/lafeber/world-flags-sprite/flags32.css">

<?php
$this->registerJsFile('js/highcharts/highcharts.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/map.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/world.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
$js_content = <<<JAVASCRIPT
Highcharts.setOptions({
    global: {     
        useUTC: false     
    },
    lang: {
        months: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
        shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdays: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'],
    }
});

JAVASCRIPT;

$this->registerCss ( '
#wrapper {
    height: 500px;
    width: 1000px;
    margin: 0 auto;
    padding: 0;
}
#container {
    float: left;
    height: 500px; 
    width: 700px; 
    margin: 0;
}
#info {
    float: left;
    width: 270px;
    padding-left: 20px;
    margin: 100px 0 0 0;
    border-left: 1px solid silver;
}
#info h2 {
    display: inline;
}
#info .f32 .flag {
    vertical-align: bottom !important;
}

#info h4 {
    margin: 1em 0 0 0;
}
');


$this->registerJs ( $js_content, View::POS_READY);
?>
<script type = 'text/javascript'>
    window.onload = function(){


        $(document).ready(function(){

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var tabledatayear = '';
            var tabledatamonth = '';
            var tabledataday = <?=json_encode($tableDataDay)?>;
//            console.log(tabledataday);
            var data = <?=json_encode($data); ?>;

            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            /* TABLETOOLS */
            var datable = $('#datatable_tabletools').dataTable({
                "sDom": "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "oTableTools": {
                },
                "autoWidth" : true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_tabletools) {
                        responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_tabletools.respond();
                }
            });



            $('#td').click(function(){
                if (!tabledatayear) {
                    $.ajax({
                        type:"POST",
                        url:"/plugin/default/get-table-data-ajax",
                        data:{'_csrf':$('#csrf').val(),'data':data,'param':'year'},
                        success:function(msg) {
                            tabledatayear = JSON.parse(msg);
                            regenerateTable(tabledatayear, 'year');
                        }
                    })
                } else{
                    regenerateTable(tabledatayear, 'year');
                }
            });

            $('#ag').click(function(){
                if (!tabledatamonth) {
                    $.ajax({
                        type:"POST",
                        url:"/plugin/default/get-table-data-ajax",
                        data:{'_csrf':$('#csrf').val(),'data':data,'param':'month'},
                        success:function(msg) {
                            tabledatamonth = JSON.parse(msg);
                            regenerateTable(tabledatamonth, 'month');
                        }
                    })
                } else{
                    regenerateTable(tabledatamonth, 'month');
                }
            });



            $('#mt').click(function(){
                if (!tabledataday) {
                    $.ajax({
                        type:"POST",
                        url:"/plugin/default/get-table-data-ajax",
                        data:{'_csrf':$('#csrf').val(),'data':data,'param':'day'},
                        success:function(msg) {
                            tabledataday = JSON.parse(msg);
                            regenerateTable(tabledataday, 'day');
                        }
                    })
                } else{
                    regenerateTable(tabledataday, 'day');
                }
            });

            /**
             * @desc regenerate table
             * @auther pzzrudlf pzzrudlf@gmail.com
             * @date 2016-1-19
             */
            function regenerateTable(tabledatamonth, time_type)
            {
                //show the specific data from variable tabledatamonth
                var html = '<tbody>';
                for (var i in tabledatamonth) {
                    html += '<tr>';
                    for (var j in tabledatamonth[i]) {
                        if (j == 3) {
                            html += '<td class="text-align-center">'+tabledatamonth[i][j]+' </td>';
                        } else if (j == 4) {
                            html += '<td class="text-align-center">'+tabledatamonth[i][j]+' </td>';
                        } else if (j == 5) {
                            html += '<td class="text-align-center">'+tabledatamonth[i][j]+' </td>';
                        } else if (j == 6) {
                            html += '<td class="text-align-center">'+tabledatamonth[i][j]+' </td>';
                        } else {
                            html += '<td class="text-align-center">'+tabledatamonth[i][j]+' </td>';
                        }
                    }
                    html += '</tr>';
                }
                html += '</tbody>';

                //clear the table tbody content and set the new data
                datable.fnClearTable();
                $('tbody').replaceWith(html);

                //recall the method dataTable() to regenerate the table
                $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});

                //set the button content
                switch (time_type) {
                    case 'year':
                        $('#custombutton').text('<?= YII::t('plugin', 'This year')?>');
                        break;
                    case 'month':
                        $('#custombutton').text('<?= YII::t('plugin', 'This month')?>');
                        break;
                    case 'day':
                        $('#custombutton').text('<?= YII::t('plugin', 'Today')?>');
                        break;
                }

            }

            //设置highChart全局配置信息
            Highcharts.setOptions({
                global: {
                    useUTC: false
                },
                lang:{
                    loading: '加载中...',
                    months:['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
                    shortMonths: ['1月', '2月', '3月', '4月', '5月', '6月', '7月','8月', '9月', '10月', '11月', '12月'],
                    weekdays: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    exportButtonTitle: '导出',
                    printButtonTitle: '打印',
                    rangeSelectorFrom: '从',
                    rangeSelectorTo: '到',
                    rangeSelectorZoom: "缩放",
                    downloadPNG: '下载PNG格式',
                    downloadJPEG: '下载JPEG格式',
                    downloadPDF: '下载PDF格式',
                    downloadSVG: '下载SVG格式'

                }
            });

            //能耗监测
            $('#first').click(function(){//本月
                $(this).css('color:red;')
                var val = $(this).attr('value');
            });

            $('#second').click(function(){
                $(this).css('color:green;')
            });

            //initialize the variables
            var electric_data = <?= isset($electric_data) ? $electric_data : "''"?>;
            var water_data = <?= isset($water_data) ? $water_data : "''"?>;
            var gas_data = <?= isset($gas_data) ? $gas_data : "''"?>;
            var co2_data = <?= isset($co2_data) ? $co2_data : "''"?>;

            //initialize the chart
            launchhighchart(electric_data, 'day', '#container_electric');

            //选项卡切换
            $("#demo-pill-nav li a").click(function(){

                var type = $(this).attr('value');
                var time_type = 'day';
                var selector = '#container_'+type;
                $(selector).empty();

                switch (type) {
                    case 'electric':
                        launchhighchart(electric_data, time_type, selector);
                        break;
                    case 'water':
                        launchhighchart(water_data, time_type, selector);
                        break;
                    case 'gas':
                        launchhighchart(gas_data, time_type, selector);
                        break;
                    case 'co2':
                        launchhighchart(co2_data, time_type, selector);
                        break;
                }

            });

            //时间切换
            $('#subconverter').change(function(){
                var idx = $(this).children('option:selected').val();//1:year,2:month,3:day

                //get the current energy type
                var chart_data_type = $("#demo-pill-nav li[class='active'] a").attr('value');

                var conselector = '#container_'+chart_data_type;

                $(conselector).empty();

                switch (idx)
                {
                    case '1':
                        if (chart_data_type == 'electric') {
                            launchhighchart(electric_data, 'year', conselector);
                        }
                        if (chart_data_type == 'water') {
                            launchhighchart(water_data, 'year', conselector);
                        }
                        if (chart_data_type == 'gas') {
                            launchhighchart(gas_data, 'year', conselector);
                        }
                        if (chart_data_type == 'co2') {
                            launchhighchart(co2_data, 'year', conselector);
                        }
                        break;
                    case '2':
                        if (chart_data_type == 'electric') {
                            launchhighchart(electric_data, 'month', conselector);
                        }
                        if (chart_data_type == 'water') {
                            launchhighchart(water_data, 'month', conselector);
                        }
                        if (chart_data_type == 'gas') {
                            launchhighchart(gas_data, 'month', conselector);
                        }
                        if (chart_data_type == 'co2') {
                            launchhighchart(co2_data, 'month', conselector);
                        }
                        break;
                    case '3':
                        if (chart_data_type == 'electric') {
                            launchhighchart(electric_data, 'day', conselector);
                        }
                        if (chart_data_type == 'water') {
                            launchhighchart(water_data, 'day', conselector);
                        }
                        if (chart_data_type == 'gas') {
                            launchhighchart(gas_data, 'day', conselector);
                        }
                        if (chart_data_type == 'co2') {
                            launchhighchart(co2_data, 'day', conselector);
                        }
                        break;
                }
            });

            //画图函数
            function launchhighchart(data, type, selector)
            {
                var arr = [];
                var res = {};
                var las = [];
                if (type == 'day') {
                    $.each(data['day'], function(key, value) {
                        arr.push(value);
                    });
                }
                if (type == 'month') {
                    $.each(data['month'], function(key, value) {
                        arr.push(value);
                    });
                }
                if (type == 'year') {
                    $.each(data['year'], function(key, value) {
                        arr.push(value);
                    });
                }
                res['data'] = arr;
                las.push(res);
//                console.log(las);
                //判断横坐标显示格式
                switch(type){
                    case 'day':
                        $_date_format = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","24:00"];
                        $_ch_type = '天';
                        ticker = 3
                        break;
                    case 'month':
                        $_date_format = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
                        $_ch_type = '月';
                        ticker = 7
                        break;
                    case 'year':
                        $_date_format =['一月', '二月', '三月', '四月', '五月', '六月','七月', '八月', '九月', '十月', '十一月', '十二月'];
                        $_ch_type = '年';
                        ticker = 2
                        break;
                }

                $(selector).highcharts({
                    chart: {
                        height:210,
                        type: 'line',
                        reflow:true
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '',
                        x: -20
                    },
                    xAxis: {
                        categories: $_date_format,
                        tickInterval:ticker
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        formatter:function(){
                            console.log(this);
//                            return this.y+'<br>'+this.x;
                        }
                    },
                    plotOptions: {
                      series: {
                          marker: {
                              enabled: false,
                          }
                      }
                    },
                    legend: {
                        enabled:false
                    },
                    series:las
                });
            }
        })
    }
</script>