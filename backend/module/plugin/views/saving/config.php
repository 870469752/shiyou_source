<?php
use \yii\helpers\Url;
use common\library\MyHtml;
?>
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <div class="jarviswidget" data-widget-editbutton="false"
                 data-widget-deletebutton="false" data-widget-colorbutton="false">
                <header>
                    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>

                    <h2>节能对比配置</h2>

                </header>
                <div class="widget-body">
                    <?= MyHtml::beginForm() ?>

                    <div class="form-group">
                        <label class="control-label">请选择总COP点位</label>
                        <?= MyHtml::dropDownList('cop', @$plugin_config['cop'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择节能日电量点位</label>
                        <?= MyHtml::dropDownList('saving_electricity', @$plugin_config['saving_electricity'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择节能COP点位</label>
                        <?= MyHtml::dropDownList('saving_cop', @$plugin_config['saving_cop'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择非节能日电量点位</label>
                        <?= MyHtml::dropDownList('not_saving_electricity', @$plugin_config['not_saving_electricity'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">请选择非节能COP点位</label>
                        <?= MyHtml::dropDownList('not_saving_cop', @$plugin_config['not_saving_cop'], $point,['class' => 'select2 margin-bottom-10']) ?>
                    </div>
                    <?= MyHtml::submitButton('保存配置', ['class' => 'btn btn-success']) ?>
                    <?= MyHtml::endForm() ?>
                </div>
            </div>
        </article>
    </div>
</section>