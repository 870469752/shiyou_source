<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', '选择查看条件');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div class="widget-body">
                    <?= MyHtml::beginForm('/plugin/saving/index', 'get') ?>
                    <div id="time_segment">

                    </div>
                    <?= MyHtml::submitButton('查询', ['class' => 'btn btn-success']) ?>
                    <?= MyHtml::button('添加时间段', ['id' => 'add_time', 'type' => 'button', 'class' => 'btn btn-primary']) ?>
                    <?= MyHtml::button('清除所有时间段', ['id' => 'del_time', 'type' => 'button', 'class' => 'btn btn-danger']) ?>
                    <?= MyHtml::endForm() ?>
                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css");
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function () {


    }
</script>