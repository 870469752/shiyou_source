<?php
use \yii\helpers\Url;
use common\library\MyHtml;

// 节能和非节能模式的总量
$saving_total_electricity = $not_saving_total_electricity = 0;
// 节能和非节能模式的天数
$saving_day_quantity = $not_saving_day_quantity = 0;
foreach($result['saving_electricity'] as $v){
    // 小于10就不算1天
    if($v['value'] > 10){
        $saving_day_quantity++;
    }
    // 节能模式用量
    $saving_total_electricity += $v['value'];
}

foreach($result['not_saving_electricity'] as $v){
    if($v['value'] > 10){
        $not_saving_day_quantity++;
    }
    // 非节能模式用量
    $not_saving_total_electricity += $v['value'];
}
// 节能日平均 = 节能模式用量 / 节能天数
$avg_day_saving_electricity = @round($saving_total_electricity / $saving_day_quantity, 2);
// 非节能日平均 = 非节能模式用量 / 非节能天数
$avg_day_not_saving_electricity = @round($not_saving_total_electricity /  $not_saving_day_quantity, 2);
// 每天平均节能量 = 节能日平均 - 非节能日平均
$day_saving_quantity = $avg_day_not_saving_electricity - $avg_day_saving_electricity;
// 总节能量 = 每天平均节能量 * 节能天数就是
$total_saving_quantity = round($day_saving_quantity * $saving_day_quantity, 2);
// 节能率 = 每天平均节能量 / 非节能日平均
$saving_ratio = @round($day_saving_quantity / $avg_day_not_saving_electricity, 2);

// 节能模式平均COP
$saving_cop = round($result['saving_cop'], 2);
// 非节能模式平均COP
$not_saving_cop = round($result['not_saving_cop'], 2);
// 平均COP
$cop = round($result['cop'], 2);

?>
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <div class="jarviswidget" data-widget-editbutton="false"
                 data-widget-deletebutton="false" data-widget-colorbutton="false">
                <header>
                    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>

                    <h2>节能对比配置</h2>
                    <div class="jarviswidget-ctrls">
                        <a id="aSearch" class="button-icon" href="#" data-toggle="modal" data-target="#search" rel="tooltip" data-original-title="时间段搜索" data-placement="bottom">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                </header>
                <div class="widget-body">
                    <!-- 节能和非节能的总用量必须都大于0 -->
                    <?php if($saving_total_electricity > 0 && $not_saving_total_electricity > 0):?>
                        <strong>节能时段总耗电量：</strong><?=$saving_total_electricity?> KWH |
                        <strong>非节能时段总耗电量：</strong><?=$not_saving_total_electricity?> KWH<br/>
                        <strong>节能时段平均COP：</strong><?=$saving_cop?> |
                        <strong>非节能时段平均COP：</strong><?=$not_saving_cop?> |
                        <strong>总时段平均COP：</strong><?=$cop?><br/>
                        <strong>节能量：</strong><?=$total_saving_quantity?> KWH |
                        <strong>节能率：</strong><?=$saving_ratio?>%
                        <a class="pull-right" id="excel" href="javascript:void(0)">导出EXCEL</a>
                        <div id="point_chart" style="height:550px;"></div>
                    <?php else:?>
                        <h3 style="padding-left:20px">
                            节能或非节能的总量为0！<br/>
                            不具备对比条件，请按浏览器上的返回重新选择时间！
                        </h3>
                    <?php endif?>
                </div>
            </div>
        </article>
    </div>
</section>
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">保存配置</h4>
            </div>
            <?= MyHtml::beginForm('/plugin/saving/index', 'get') ?>
            <div class="modal-body">
                <div class="well" id="condition">
                    <div id="time_segment"></div>
                </div>
            </div>
            <div class="modal-footer">
                <?= MyHtml::submitButton('查询', ['class' => 'btn btn-success']) ?>
                <?= MyHtml::button('添加时间段', ['id' => 'add_time', 'type' => 'button', 'class' => 'btn btn-primary']) ?>
                <?= MyHtml::button('清除所有时间段', ['id' => 'del_time', 'type' => 'button', 'class' => 'btn btn-danger']) ?>
                <?= MyHtml::button('取消', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
            </div>
            <?= MyHtml::endForm() ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css");
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script type="text/javascript">
    window.onload = function () {

        // 图表展示部分代码 ------------------------------

        highcharts_lang_date();

        $('#point_chart').highcharts('StockChart', {
            chart: {
                type: 'column',
                zoomType: 'x'
            },
            legend: {
                enabled: true,
                verticalAlign: 'bottom'
            },
            title: {
                text: '节能与非节能日电量对比图'
            },
            subtitle: {
                text: '<?php foreach($date as $v): ?><?=date('Y年m月d日', strtotime($v['start_time']))?> 至 <?=date('Y年m月d日', strtotime($v['end_time']))?><br/><?php endforeach ?>'
            },
            rangeSelector: {
                enabled: false
            },
            tooltip: {
                shared: true
            },
            yAxis: {
                opposite: false,
                title: {
                    text: "KWH"
                }
            },
            series: <?=$json_electricity?>
        });

        var excel_url = "<?=Url::toRoute('/export/default/saving-mode')?>" +
            "?saving_total=<?=$saving_total_electricity?>KWH" +
            "&not_save_total=<?=$not_saving_total_electricity?>KWH" +
            "&saving_cop=<?=$saving_cop?>" +
            "&not_save_cop=<?=$not_saving_cop?>" +
            "&cop=<?=$cop?>" +
            "&saving_quantity=<?=$total_saving_quantity?>KWH" +
            "&saving_ratio=<?=$saving_ratio?>%"
            <?php foreach($date as $k=>$v): ?>
            + "&date[<?=$k?>][start_time]=<?=$v['start_time']?>"
            + "&date[<?=$k?>][end_time]=<?=$v['end_time']?>"
        <?php endforeach ?>

        $('#excel').attr('href', excel_url);



        // 时间段选择部分JS代码 ------------------------------

        var i = 0; // 当前是第0段时间
        var input_time_class = 'form-control  margin-bottom-10';

        // 如果已保存了COOKIE就显示选择项
        if(document.cookie.indexOf('start_time') != -1){
            var arrTime = document.cookie.split("; ");
            for(var k in arrTime){
                var value = arrTime[k].split("=")[1];
                var name = arrTime[k].split("=")[0];
                // 如果是开始时间就设定容器
                if(name.indexOf('start_time') != -1){
                    $("#time_segment").append('\
                        <div class="control-group">\
                            <label class="control-label" for="report_type">时间段</label>\
                            <div class="controls"></div>\
                        </div>\
                    ');
                    $("#time_segment .controls:eq("+i+")").
                        append('<input type="text" class="'+input_time_class+'" name="'+name+'" />');
                    var name_obj = $("[name='"+name+"']");
                    timeTypeChange(null, name_obj);
                    name_obj.val(value);
                }
                // 结束之后把时间组序列加1
                if(name.indexOf('end_time') != -1){
                    $("#time_segment .controls:eq("+i+")").
                        append('<input type="text" class="'+input_time_class+'" name="'+name+'" />');
                    var name_obj = $("[name='"+name+"']");
                    timeTypeChange(null, name_obj);
                    name_obj.val(value);
                    i++;
                }
            }
        }
        else{ // 如果没有就添加一个时间段
            add_time()
        }

        // 添加时间方法
        function add_time() {
            var start_name = 'date[' + i + '][start_time]';
            var end_name = 'date[' + i + '][end_time]';
            $("#time_segment").append('\
                <div class="control-group">\
                    <label class="control-label" for="report_type">时间段</label>\
                    <div class="controls">\
                        <input class="'+input_time_class+'" type="text" name="'+start_name+'">\
                        <input class="'+input_time_class+'" type="text" name="'+end_name+'">\
                    </div>\
                 </div>\
            ');
            var start_name_obj = $("[name='"+start_name+"']");
            var end_name_obj = $("[name='"+end_name+"']");
            timeTypeChange(null, start_name_obj);
            timeTypeChange(null, end_name_obj);
            start_name_obj.val('<?= date('Y-m-d', strtotime('-1 month')) ?>');
            end_name_obj.val('<?= date('Y-m-d') ?>');
            i++;
        }

        // 点击添加时间就执行方法
        $("#add_time").click(function(){add_time();})

        // 点击删除时间就清除所有COOKIE然后增加一个时间
        $("#del_time").click(function(){
            var arr = document.cookie.split("; ");
            var date=new Date();
            date.setTime(date.getTime()-10000);
            for(var k in arr){
                var name = arr[k].split("=")[0];
                if(name.indexOf('time') != -1){
                    document.cookie=name+'=; expires='+date.toGMTString()+';path=/';
                }
            }
            i = 0;
            $("#time_segment").html('');
            add_time();
        });

        // 提交之后保存选择COOKIE
        $('form').submit(function(){
            $("#time_segment input").each(function(){
                document.cookie=this.name+'='+this.value+';path=/';
            })
        })
    }
</script>