<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', '选择查看条件');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div class="widget-body">
                    <?= MyHtml::beginForm() ?>
                    <div id="time_segment">

                    </div>
                    <?= MyHtml::submitButton('查询', ['class' => 'btn btn-success']) ?>
                    <?= MyHtml::button('添加时间段', ['id' => 'add_time', 'type' => 'button', 'class' => 'btn btn-primary']) ?>
                    <?= MyHtml::button('清除所有时间段', ['id' => 'del_time', 'type' => 'button', 'class' => 'btn btn-danger']) ?>
                    <?= MyHtml::endForm() ?>
                </div>
            </div>
        </article>
    </div>
</section>
<script>
    window.onload = function () {

        datepicker_lang_date(null, null);

        var myDate = new Date();
        var year = myDate.getFullYear();
        var month = myDate.getMonth() + 1;
        var today = myDate.getDate() - 1;
        // 前一天
        var date = year+'-'+month+'-'+today;
        var i = 0;
        var input_time_class = 'form-control  margin-bottom-10';

        // 如果已保存了COOKIE就显示选择项
        if(document.cookie.indexOf('start_time') != -1){
            var arrTime = document.cookie.split("; ");
            for(var k in arrTime){
                var value = arrTime[k].split("=")[1];
                var name = arrTime[k].split("=")[0];
                // 如果是开始时间就设定容器
                if(name.indexOf('start_time') != -1){
                    $("#time_segment").append('\
                        <div class="control-group">\
                            <label class="control-label" for="report_type">时间段</label>\
                            <div class="controls"></div>\
                        </div>\
                    ');
                    $("#time_segment .controls:eq("+i+")").
                        append('<input type="text" class="'+input_time_class+'" name="'+name+'" value="'+value+'"/>');
                    $("[name='"+name+"']").datepicker();
                }
                // 结束之后把时间组序列加1
                if(name.indexOf('end_time') != -1){
                    $("#time_segment .controls:eq("+i+")").
                        append('<input type="text" class="'+input_time_class+'" name="'+name+'" value="'+value+'"/>');
                    $("[name='"+name+"']").datepicker();
                    i++;
                }
            }
        }
        else{ // 如果没有就添加一个时间段
            add_time()
        }

        // 添加时间方法
        function add_time() {
            var start_name = 'time[' + i + '][start_time]';
            var end_name = 'time[' + i + '][end_time]';
            $("#time_segment").append('\
                <div class="control-group">\
                    <label class="control-label" for="report_type">时间段</label>\
                    <div class="controls">\
                        <input value="' + date + '" class="'+input_time_class+'" type="text" name="'+start_name+'">\
                        <input value="' + date + '" class="'+input_time_class+'" type="text" name="'+end_name+'">\
                    </div>\
                 </div>\
            ');
            $("[name='"+start_name+"']").datepicker();
            $("[name='"+end_name+"']").datepicker();
            i++;
        }

        // 点击添加时间就执行方法
        $("#add_time").click(function(){add_time();})

        // 点击删除时间就清除所有COOKIE然后增加一个时间
        $("#del_time").click(function(){
            var arr = document.cookie.split("; ");
            var date=new Date();
            date.setTime(date.getTime()-10000);
            for(var k in arr){
                var name = arr[k].split("=")[0];
                if(name.indexOf('time') != -1){
                    document.cookie=name+'=; expires='+date.toGMTString();
                }
            }
            i = 0;
            $("#time_segment").html('');
            add_time();
        });

        // 提交之后保存选择COOKIE
        $('form').submit(function(){
            $("#time_segment input").each(function(){
                document.cookie=this.name+'='+this.value;
            })
        })
    }
</script>