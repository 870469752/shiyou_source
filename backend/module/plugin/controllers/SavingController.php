<?php

namespace backend\module\plugin\controllers;

use backend\controllers\MyController;
use backend\models\PointData;
use common\library\MyFunc;
use Yii;

class SavingController extends MyController
{
    public function actionIndex()
    {
        //临时写的，以后修改
        $plugin_config = json_decode((new \yii\db\Query())
            ->select('parameter')
            ->where(['plugin_id' => $this->id])
            ->from('core.plugin_config')
            ->one()['parameter'], true);
        $query_config = [
            ['name' => 'saving_electricity', 'data_quantity' => 'all'], //节能时电量点位
            ['name' => 'saving_cop', 'data_quantity' => 'one'], // 节能时COP点位
            ['name' => 'not_saving_electricity', 'data_quantity' => 'all'], // 非节能时电量点位
            ['name' => 'not_saving_cop', 'data_quantity' => 'one'], // 非节能时COP点位
            ['name' => 'cop', 'data_quantity' => 'one'], // 总COP点位
        ];

        $date = isset($_COOKIE['date'])?$_COOKIE['date']:[['start_time' => date('Y-m-d', strtotime('-1 month')), 'end_time' => date('Y-m-d')]];
        // 如果连接有参数就用连接参数
        $date = YII::$app->request->get('date', $date);
        foreach($query_config as $v){
            $id = isset($plugin_config[$v['name']])?$plugin_config[$v['name']]:0;
            $obj_query = PointData::findMultiDate($id, $date);
            if($v['data_quantity'] == 'all'){
                $result[$v['name']] = $obj_query->orderBy('timestamp')->asArray()->all();
            }else{
                $result[$v['name']] = $obj_query->average('value');
            }
        }
        if(isset($result['saving_electricity'])){
            $name[] = '节能模式电量';
            $data[] = $result['saving_electricity'];
        }
        if(isset($result['not_saving_electricity'])){
            $name[] = '非节能模式电量';
            $data[] = $result['not_saving_electricity'];
        }
        $json_electricity = isset($data)?MyFunc::HighChartsData($data, $name):'[]';

        return $this->render('index',[
            'result' => $result,
            'json_electricity' => $json_electricity,
            'date' => $date
        ]);
    }

    public function actionCondition(){

        return $this->render('condition');
    }
}
