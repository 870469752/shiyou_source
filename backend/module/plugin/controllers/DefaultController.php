<?php

namespace backend\module\plugin\controllers;

use backend\controllers\MyController;
use backend\module\category_data\controllers\CrudController;
use backend\models\Discuss;
use backend\models\Point;
use backend\models\PointData;
use backend\models\PointDataStatistic;
use backend\models\Category;
use backend\models\PointCategoryRel;
use backend\models\Location;
use common\library\MyFunc;
use Yii;
use yii\helpers\ArrayHelper;
use backend\models\OperateLog;
class DefaultController extends MyController
{
    public $plugin_config;
    public $plugin_id;

    public function init()
    {
        parent::init();
        $this->plugin_id = YII::$app->request->get('id', $this->id);
        //临时写的，以后修改
        $this->plugin_config = json_decode((new \yii\db\Query())
            ->select('parameter')
            ->where(['plugin_id' => $this->plugin_id])
            ->from('core.plugin_config')
            ->one()['parameter'], true);
    }

    //脚本添加数据
    public function actionAddPointData(){

        $start_time = '2014-01-01 00:00:00';
        $end_time = '2016-01-01 00:00:00';

        $points_=Point::find()->select('id')->asArray()->all();
        foreach ($points_ as $value) {
            $points[]=$value['id'];

        }

        echo'<pre>';
        //调用函数
        for($i=0;$i<999;$i++) {
            $model=new PointDataStatistic();
            $time=PointDataStatistic::rand_time($start_time, $end_time);
            $data=[
                'PointDataStatistic'=>[
                    'point_id'=>array_rand($points),
                    'interval_type'=>(string)array_rand([1,2,3]),
                    'value_type'=>(string)array_rand([1,2,3,4,5,6,7,8]),
                    'value'=>(string)rand(1,100),
                    'timestamp'=>$time,
                ]
            ];
//            print_r(array_rand($points).'<br />');
//            print_r(PointDataStatistic::rand_time($start_time, $end_time).'<br />');
//            print_r(rand(1,100).'<br />');
//            print_r($time);
            $model->load($data);
//            print_r($model);
            $model->save();
//            print_r($model->errors);

        }
        die;
    }

    public function actionIndex()
    {
        $tableArray = [];
        //select sql:select id from core.point_category where name -> 'data' ->> 'zh-cn' = '电';
        $data = [29,18,13,178];//29:电,18:水,13:照明插座用电,178:动力用电
        //generate the table start
        //pzzrudlf pzzrudlf@gmail.com
        //year
        $query_param_year['time_type'] = 'year';
        $query_param_year['date_time'] = date('Y',time());
        $query_param_year['date_time'] = '2015';
        $query_param_year['interval_type'] = 3;
        $query_param_year['value_type'] = 5;
        $wantedData = $this->gettable($data, $query_param_year);
//        $result = $this->gettabledataarray($wantedData[0]);
//        $tableDataYear = $result;
        $yeardata = $wantedData[1];
        //month
        $query_param_year['time_type'] = 'month';
        $query_param_year['date_time'] = date('Y-m',time());
        $query_param_year['date_time'] = '2015-12';
        $query_param_year['interval_type'] = 3;
        $query_param_year['value_type'] = 5;
        $wantedData = $this->gettable($data, $query_param_year);
//        $result = $this->gettabledataarray($wantedData[0]);
//        $tableDataMonth = $result;
        $monthdata = $wantedData[1];
        //day
        $query_param_year['time_type'] = 'day';
        $query_param_year['date_time'] = date('Y-m-d',time());
        $query_param_year['date_time'] = '2015-12-23';
        $query_param_year['interval_type'] = 3;
        $query_param_year['value_type'] = 5;
        $wantedData = $this->gettable($data, $query_param_year);
        $result = $this->gettabledataarray($wantedData[0]);
        $tableDataDay = $result;
        //generate the table end

        $timer = date('Y-m-d',time());
        $category_id = $data;

        for ($i = 0; $i < count($category_id); $i++) {
            $key = $category_id[$i];
            if ($key == 29) {
                $electric_data = $this->getPointDataByDateAndType($key, $timer);
            }
            if ($key == 18) {
                $water_data = $this->getPointDataByDateAndType($key, $timer);
            }
            if ($key == 13) {
                $gas_data = $this->getPointDataByDateAndType($key, $timer);
            }
            if ($key == 178) {
                $co2_data = $this->getPointDataByDateAndType($key, $timer);
            }
        }




        return $this->render('index',[
//            'monthdata' => ['29'=>1, '18'=>2, '13'=>3, '178'=>4],
            'monthdata' => $monthdata,
//            'yeardata' => ['29'=>1, '18'=>2, '13'=>3, '178'=>4],
            'yeardata' => $yeardata,
            'data'=>$data,
            'electric_data'=>json_encode($this->getfixeddata($electric_data)),
            'water_data'=>json_encode($this->getfixeddata($water_data)),
            'gas_data'=>json_encode($this->getfixeddata($gas_data)),
            'co2_data'=>json_encode($this->getfixeddata($co2_data)),
//            'tableDataYear'=>$tableDataYear,
//            'tableDataMonth'=>$tableDataMonth,
            'tableDataDay'=>$tableDataDay,
        ]);
    }

    /**
     * ajax获取表格数据
     */
    public function actionGetTableDataAjax()
    {

        $param = Yii::$app->request->post();
        //select sql:select id from core.point_category where name -> 'data' ->> 'zh-cn' = '电';
        $data = $param['data'];//29:电,18:水,13:照明插座用电,178:动力用电
        $time_type = $param['param'];
        switch ($time_type) {
            case 'year':
                $query_param_year['time_type'] = 'year';
                $query_param_year['date_time'] = date('Y',time());
                $query_param_year['date_time'] = '2015';
                $query_param_year['interval_type'] = 3;
                break;
            case 'month':
                $query_param_year['time_type'] = 'month';
                $query_param_year['date_time'] = date('Y-m',time());
                $query_param_year['date_time'] = '2015-12';
                $query_param_year['interval_type'] = 2;
                break;
            case 'day':
                $query_param_year['time_type'] = 'day';
                $query_param_year['date_time'] = date('Y-m-d',time());
                $query_param_year['date_time'] = '2015-12-23';
                $query_param_year['interval_type'] = 1;
                break;
        }

        $query_param_year['value_type'] = 5;
        $wantedData = $this->gettable($data, $query_param_year);
        $result = $this->gettabledataarray($wantedData[0]);
        return json_encode($result);
    }

    /**
     * ajax获取图表数据
     */
    public function getChartDataAjax()
    {

    }

    /**
     * @param $data
     * @param $query_param_year
     * @auther pzzrudlf pzzruflf@gmail.com
     */
    public function gettable($data, $query_param_year)
    {
        $last = [];
        $totalData = [];
        $category=new Category();

        $result = [];
        $top = Location::find()->select('id')->where(['parent_id'=>null])->asArray()->column();
        if ($top) {
            foreach ($top as $key => $value) {
                $tableArrayLocation = Location::find()->select('id')->where("category_id @> '{".$value."}'")->andWhere('id !='.$value)->asArray()->column();
                if ($tableArrayLocation) {
                    foreach ($data as $energy_key => $energy_value) {
                        foreach ($tableArrayLocation as $key => $value) {
                            $query_param_year['Point']['category_id'] = $energy_value;
                            $res = $category->getPointdataIndex($value, $query_param_year);
                            if ($res) {
                                $sum_temp = 0;
                                if($res['data']) {
                                    foreach ($res['data'] as $key1 => $value1) {
                                        $sum_temp += $value1['value'];
                                    }
                                }
                                $result[$value] = $sum_temp;
                            } else {
                                $result[$value] = 0;
                            }
                        }
                        $last[$energy_value] = $result;
                    }
                }
            }
        }

//        echo '<pre>';
//        print_r($last);die;
        $wantedData = [];
        if ($last) {
            foreach ($last as $key => $value) {
                $sum_temp = 0;
                foreach ($value as $key1 => $value1) {
                    $wantedData[$key1][$key] = $value1;
                    $sum_temp += $value1;
                }
                $totalData[$key] = $sum_temp;
            }
        }

        return [$wantedData,$totalData];
    }

    /**
     * @param $wantedData
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function gettabledataarray($wantedData)
    {
//        echo "<pre>";
//        print_r($wantedData);
        $temp = [];
        $location = new Location();
        if (!$wantedData) {
            return 0;
        }

        $i = 0;
        foreach ($wantedData as $key => $value) {
            $tempp = [];
            if ($pid = Location::IsHaveParent($key)) {//有父级
                //父级地理位置名字
                //先查父级id
                $parent = Location::findOne($pid);
                $parentNameTemp = $parent->name;
                $parentName = json_decode($parentNameTemp,true)['data']['zh-cn'];
            } else {//没有父级
                $parentName = '-';
            }
//            print_r($parentName);
            //本身地理位置名字
            $self = Location::findOne($key);
            $selfNameTemp = $self->name;
            $selfName = json_decode($selfNameTemp,true)['data']['zh-cn'];
//            print_r($selfName);
            for ($j=0;$j<7;$j++) {
                switch ($j) {
                    case 0:
                        $tempp[$j] = $i+1;
                        break;
                    case 1:
                        $tempp[$j] = $parentName;
                        break;
                    case 2:
                        $tempp[$j] = $selfName;
                        break;
                    case 3:
                        $tempp[$j] = $value[29];
                        break;
                    case 4:
                        $tempp[$j] = $value[18];
                        break;
                    case 5:
                        $tempp[$j] = $value[13];
                        break;
                    case 6:
                        $tempp[$j] = $value[178];
                        break;
                }
            }

            $i++;
            $temp[] = $tempp;
        }

        return $temp;
    }

    /**
     * @param $electric_data
     * @return mixed
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function getfixeddata($electric_data)
    {
        $totalDays = cal_days_in_month(CAL_GREGORIAN,date('m',time()),date('Y',time()));
        $electric_data_res = [];
        foreach ($electric_data as $key => $value) {
                if ($key == 'year') {
                    foreach ($value as $key1 => $val) {
                        $ye = substr($val['_timestamp'],0,4);
                        $mo = substr($val['_timestamp'],5,2);
                        $totalDays = cal_days_in_month(CAL_GREGORIAN,$mo,$ye);
                        $idex = (integer)substr($val['_timestamp'],5,2);
                        $electric_data_res[$key][$idex] = $val['value'];
                    }
                }

                if ($key == 'month') {
                    foreach ($value as $key2 => $val) {
                        $ye = substr($val['_timestamp'],0,4);
                        $mo = substr($val['_timestamp'],5,2);
                        $totalDays = cal_days_in_month(CAL_GREGORIAN,$mo,$ye);
                        $idex = (integer)substr($val['_timestamp'],8,2);
                        $electric_data_res[$key][$idex] = $val['value'];
                    }
                }

                if ($key == 'day') {
                    foreach ($value as $key3 => $val) {
                        $idex = (integer)substr($val['_timestamp'],11,2);
                        $electric_data_res[$key][$idex] = $val['value'];
                    }
                }
        }


        for ($i=0; $i<12; $i++) {
            if (isset($electric_data_res['year'][$i+1])) {
                $electric_data_res['year'][$i] = (integer)$electric_data_res['year'][$i+1];
            }
            if (!isset($electric_data_res['year'][$i+1])) {
                $electric_data_res['year'][$i] = 0;
            }
        }

        unset($electric_data_res['year'][12]);

        for ($i=0; $i<$totalDays; $i++) {
            if (isset($electric_data_res['month'][$i+1])) {
                $electric_data_res['month'][$i] = (integer)$electric_data_res['month'][$i+1];
            }
            if (!isset($electric_data_res['month'][$i+1])) {
                $electric_data_res['month'][$i] = 0;
            }
        }

        for ($i=0; $i<24; $i++) {
            if (isset($electric_data_res['day'][$i+1])) {
                $electric_data_res['day'][$i] = (integer)$electric_data_res['day'][$i+1];
            }
            if (!isset($electric_data_res['day'][$i+1])) {
                $electric_data_res['day'][$i] = 0;
            }
        }

        return $electric_data_res;
    }
	
	
    /**
     * @param $category_id
     * @param $timer
     * @return [category_id=>
     * [
     *  year=>[],
     * month=>[],
     * day=>[]
     * ]
     * ]
     */
    public function getPointDataByDateAndType($category_id, $timer)
    {
        $times['Point']['category_id'] = $category_id;
        $category = new Category();
        //first: get all point id belong to category
        //$allPointId = $this->getAllPointIdByCategoryId($category_id);
        $allPointId = $this->getAllPointIddsByCategoryId($category_id);
//        echo '<pre>';
//        print_r($allPointId);
//        die;
        //second:get all point data by $timer and point id
        $year = date('Y',time());
        $month = date('Y-m',strtotime("-1 month"));
        $day = date('Y-m-d',time());

        $times['time_type'] = 'year';
        $times['date_time'] = $year;
        $times['interval_type'] = 3;

        $data = $category->getWantedData($allPointId, $times);

        foreach ($data as $key => $val) {
            if ($key == 'data') {
                $data_year = $val;
            }
        }

        $times['time_type'] = 'month';
        $times['date_time'] = $month;
        $times['interval_type'] = 2;

        $data = $category->getWantedData($allPointId, $times);

        foreach ($data as $key => $val) {
            if ($key == 'data') {
                $data_month = $val;
            }
        }


        $times['time_type'] = 'day';
//        $times['date_time'] = '2015-11-12';
        $times['date_time'] = $day;
        $times['interval_type'] = 1;
        $data = $category->getWantedData($allPointId, $times);

        foreach ($data as $key => $val) {
            if ($key == 'data') {
                $data_day = $val;
            }
        }

        return [
            'year'=>$data_year,
            'month'=>$data_month,
            'day'=>$data_day
        ];
    }

    /**
     * @param $query_param
     * @return mixed
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function getTargetedData ($point, $query_param)
    {
//        $id = $query_param['Point']['category_id'];
        $tempp = $query_param;
        $category = new Category();
        //获取当前时间该能源分类下所有点位数据，$point_ids = [$category_id=>[]]
        $chart['current'] = $category->getWantedData($point, $tempp);
        return $chart;
    }

    //get the point id that belongs to the category id
    public function getAllPointIdsByCategoryId($categoryId)
    {
        $category = new Category();
        //获取能源分类category_id下各级子类id
        $temp = $category->getSubCategoryId($categoryId);
        $sub_category_id = $this->convertToSinleArray($temp);
        array_unshift($sub_category_id,$categoryId);
        //获取能源分类category_id下所有点位id,$point_ids = [$category_id=>[]]
        $all_point_ids = [];
        if ($sub_category_id) {
            foreach ($sub_category_id as $key => $value) {
                $temp = PointCategoryRel::getPointIds($value);
                if ($temp) {
                    foreach ($temp as $val) {
                        if ($val) {
                            foreach($val as $va) {
                                array_push($all_point_ids,$va);
                            }
                        }

                    }
                }
            }
        }
        array_unique($all_point_ids);
        foreach ($all_point_ids as $key => $val) {

        }

        return $all_point_ids;
    }
	
	/**
	 *@auther pzzrudlf pzzrudlf@gmail.com
	 *@date 2015-12-21
	 */
	 
	    //find the category
    public function getAllPointIddsByCategoryId($categoryId)
    {
        $category = new Category();
        $all_point_ids=[];
        //获取能源分类category_id下各级子类id

        $temp = $category->getAllSubCategoryIdsByCategoryId($categoryId);
        if ($temp) {
            foreach ($temp as $key => $value) {
                array_push($all_point_ids,$value['id']);
            }
        }
        return [$categoryId => $all_point_ids];
    }


    //find the category
    public function getAllPointIdByCategoryId($categoryId)
    {
        $category = new Category();
        //获取能源分类category_id下各级子类id
        $temp = $category->getSubCategoryId($categoryId);
        $sub_category_id = $this->convertToSinleArray($temp);
        array_unshift($sub_category_id,$categoryId);
        //获取能源分类category_id下所有点位id,$point_ids = [$category_id=>[]]
        $all_point_ids = [];
        if ($sub_category_id) {
            foreach ($sub_category_id as $key => $value) {
                $temp = PointCategoryRel::getPointIds($value);
                if ($temp) {
                    foreach ($temp as $val) {
                        if ($val) {
                            foreach($val as $va) {
                                array_push($all_point_ids,$va);
                            }
                        }
                    }
                }
            }
        }
        array_unique($all_point_ids);
        return [$categoryId => $all_point_ids];
    }

    /**
     * recursive function in order to convert a multiple dimensions array to a single dimension array
     * @param array $array
     * @param array $res
     * @return $res
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function convertToSinleArray($array, &$res=[])
    {
        foreach ($array as $key => $value) {
            array_push($res,$key);
            self::convertToSinleArray($value, $res);
        }
        return $res;
    }


//    public function actionIndex()
//    {
//        $start_time = date('Y-m-d 00:00:00');
//        $end_time = date('Y-m-d H:i:s');
//
//        // 此为劣质代码请勿模仿，目前需求未确定为了加快编写速度逼不得已，确定之后更正
//        if(isset($this->plugin_config['electricity'])){
//            foreach($this->plugin_config['electricity'] as $k => $v){
//                $point_info = Point::findOne($v);
//                $result['electricityPie'][0]['data'][$k] = [
//                    MyFunc::DisposeJSON($point_info['name']),
//                    intval(PointData::findIdTimeRange($v,$start_time,$end_time)->sum('value'))
//                ];
//                $result['electricity'][$v]['name'] = $result['electricityPie'][0]['data'][$k][0];
//                $result['electricity'][$v]['data'] = [$result['electricityPie'][0]['data'][$k][1]];
//            }
//        }
//        if(isset($this->plugin_config['refrigeration'])){
//            foreach($this->plugin_config['refrigeration'] as $k => $v){
//                $point_info = Point::findOne($v);
//                $result['refrigerationPie'][0]['data'][$k] = [
//                    MyFunc::DisposeJSON($point_info['name']),
//                    intval(PointData::findIdTimeRange($v,$start_time,$end_time)->sum('value'))
//                ];
//                $result['refrigeration'][$v]['name'] = $result['refrigerationPie'][0]['data'][$k][0];
//                $result['refrigeration'][$v]['data'] = [$result['refrigerationPie'][0]['data'][$k][1]];
//            }
//        }
//
//        // SC地址
//        $sc_address_obj = (new \yii\db\Query())->select('name,ip,port')->from('project.sc_address')->all();
//        // 拼组IP和端口号
//        foreach($sc_address_obj as $k => $v){
//            if($v['port']){
//                $sc_address_obj[$k]['ip'] = $v['ip'].':'.$v['port'];
//            }
//        }
//        $sc_address = ArrayHelper::map(
//            $sc_address_obj,
//            'ip',
//            'name'
//        );
//        return $this->render('index',[
//            'electricity' =>
//                isset($result['electricity'])?json_encode(array_values($result['electricity']), JSON_UNESCAPED_UNICODE):'[]',
//            'refrigeration' =>
//                isset($result['refrigeration'])?json_encode(array_values($result['refrigeration']), JSON_UNESCAPED_UNICODE):'[]',
//            'electricityPie' =>
//                isset($result['electricityPie'])?json_encode($result['electricityPie'], JSON_UNESCAPED_UNICODE):'[]',
//            'refrigerationPie' =>
//                isset($result['refrigerationPie'])?json_encode($result['refrigerationPie'], JSON_UNESCAPED_UNICODE):'[]',
//            'sc_address' => $sc_address
//        ]);
//    }

    public function actionAjaxRealTime()
    {
        $point_id = @$this->plugin_config[YII::$app->request->get('type')];

        // 今天凌晨0点到目前的曲线
        $start_time = date('Y-m-d 00:00:00');
        $end_time = date('Y-m-d H:i:s');
        if (isset($point_id)) {
            foreach ($point_id as $v) {
                $data[$v] = PointData::findIdTimeRange($v, $start_time, $end_time)
                    ->orderBy('timestamp')
                    ->asArray()->all();
                $point_info = Point::find()->select(['name', 'unit'])->where(['id' => $v])->with('unit')->asArray()->one();
                $name[$v] = MyFunc::DisposeJSON($point_info['name']);
                $axis[$v]['unit'] = $point_info['unit']['name'];
            }
            $result['data'] = MyFunc::HighChartsData($data, $name, [], $axis, false);
            $result['unit'] = isset($point_info['unit']['name']) ? $point_info['unit']['name'] : '';
        }
        return isset($result)?json_encode($result, JSON_UNESCAPED_UNICODE):'[]';
    }

    public function actionConfig()
    {
        $point = ArrayHelper::map(
            Point::baseSelect()
            ->select(['id','name' => "name->'data'->>'".YII::$app->language."'"])
            ->asArray()->all(),
            'id',
            'name'
        );
        /*操作日志*/
        $op['zh'] = '配置首页';
        $op['en'] = 'Configuring Home';
        $this->getLogOperation($op,'','');

        if($post = Yii::$app->request->post()){
            unset($post['_csrf']); // 去掉令牌
            Yii::$app->db->createCommand()->delete('core.plugin_config',['plugin_id' => $this->plugin_id])->execute();
            Yii::$app->db->createCommand()->insert('core.plugin_config',[
                'parameter' => json_encode($post, JSON_UNESCAPED_UNICODE),
                'plugin_id' => $this->plugin_id
                ])->execute();
            return $this->redirect('/plugin/'.$this->plugin_id);
        }
        else{
            return $this->render('/'.$this->plugin_id.'/config',[
                'point' => $point ,
                'plugin_config' => $this->plugin_config
            ]);
        }
    }

    public function actionDemo()
    {
        return $this->render('demo');
    }

    public function actionChat()
    {
        $model = new Discuss;

        if ($model->load(Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $info = $model->content;
            $op['zh'] = '创建话题';
            $op['en'] = 'Create theme';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['chat']);
        }
        else{
            $message = Discuss::find()->with('user')->orderBy(['timestamp' => SORT_DESC])->asArray()->all();
            return $this->render('chat',[
                'message' => $message,
                'model' => $model,
            ]);
        }
    }

    public function gettabledata($query_param)
    {
        $locationcategory = new Location();
        $res = $locationcategory->getPointdataIndex($query_param);
        if (!$res) {
            return null;
        }
        $temp = [];
        $i = 0;
        $j = 0;
        foreach ($res as $key => $value) {
            $firstName = $value['name'];

            if ($value['children']) {
                foreach ($value['children'] as $key1 => $value1) {
                    $secondName = $value1['name'];
                    $temp[$j] = [$firstName, $secondName, round($value1['value'],2)];
                    $j++;
                }
            }else {
                $temp[$i][] = $firstName;
                $temp[$i][] = 0;
                $temp[$i][] = round($value['value'],2);
            }
            $i++;
        }

        return $temp;
    }


}
