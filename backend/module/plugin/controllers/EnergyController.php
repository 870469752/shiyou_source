<?php

namespace backend\module\plugin\controllers;

use backend\controllers\MyController;
use common\library\MyFunc;
use Yii;

class EnergyController extends MyController
{
    public function actionIndex()
    {
        $this->layout = [];
        foreach(glob($this->module->controllerPath.'/*') as $v){
            preg_match('#/(\w+)Controller.php#', $v, $matches);
            $plugin = strtolower($matches[1]);
            echo $plugin;
        }
        return $this->render('index');
    }
}
