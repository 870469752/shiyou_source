<?php

namespace backend\module\schemaControl;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\schemaControl\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
