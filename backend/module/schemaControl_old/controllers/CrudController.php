<?php

namespace backend\module\schemaControl\controllers;

use backend\models\schemaManage;
use Yii;
use backend\models\schemaControl;
use backend\models\search\schemaControlSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;

/**
 * CrudController implements the CRUD actions for schemaManage model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all schemaManage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new schemaControlSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single schemaManage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new schemaManage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new schemaControl();
        $postdata=Yii::$app->request->post();
        if($postdata !=null){
            $data=json_decode($postdata['control_data'],1);
            //获取时间模式ID
            $scheme_manage_id=$postdata['_timemode'];
            $title=$postdata['name'];
            //获取数组
            $control_info=array(
                'data_type'=>'control_mode',
                'data'=>array()
            );
            $namearr=array(
               'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$title,
                    'en-cn'=>null
                )
            );
            //先遍历时间段，在遍历控制字段
            for($i=0;$i<count($data);$i++)
            {
                $info=array(
                    'time'=>array(),
                    'control'=>array()
                );
                $info['time']['start']=$data[$i][0][0];
                $info['time']['end']=$data[$i][0][1];
                for($j=0;$j<count($data[$i][1]);$j++)
                {
                    $name=$data[$i][1][$j][0];
                    $value=$data[$i][1][$j][1];
                    $info['control'][$j]=array(
                        'name'=>$name,
                        'value'=>$value
                    );
                }
                $control_info['data'][$i]=$info;
            }
            $model->control_info=json_encode($control_info);
            $model->shema_manage_id=$scheme_manage_id;
            $model->name=json_encode($namearr);
        }
        if ( Yii::$app->request->post()&&$model->save()) {
            return $this->redirect(['index']);
        }else {
            $m=new schemaControl();
            $modeselect=$m->findSelect();
            return $this->render('create', [
                'model' => $model,
                'modeselect'=>$modeselect,
            ]);
        }
    }

    /**
     * Updates an existing schemaManage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelarr = schemaControl::find()->where(['id'=>$id])->asArray()->one();
        $postdata=Yii::$app->request->post();
        if($postdata !=null){
            //获取数组
            $data=json_decode($postdata['control_data'],1);
            //获取时间模式ID
            $scheme_manage_id=$postdata['_timemode'];
            $title=$postdata['name'];

//            $data=isset($postdata['control_data'])?json_decode($postdata['control_data'],1):null;
//            $scheme_manage_id=isset($postdata['_timemode'])?$postdata['_timemode']:null;
//            $title=isset($postdata['name'])?$postdata['name']:'';

            $control_info=array(
                'data_type'=>'control_mode',
                'data'=>array()
            );
            $namearr=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$title,
                    'en-cn'=>null
                )
            );
            //先遍历时间段，在遍历控制字段
            for($i=0;$i<count($data);$i++)
            {
                $info=array(
                    'time'=>array(),
                    'control'=>array()
                );
                //$info['time']=$data[i][0];
                $info['time']['start']=$data[$i][0][0];
                $info['time']['end']=$data[$i][0][1];
                for($j=0;$j<count($data[$i][1]);$j++)
                {
                    $name=$data[$i][1][$j][0];
                    $value=$data[$i][1][$j][1];
                    $info['control'][$j]=array(
                        'name'=>$name,
                        'value'=>$value
                    );
                }
           /*     echo '<pre>';
                print_r($info);
                die;*/
                $control_info['data'][$i]=$info;
            }
            $model->control_info=json_encode($control_info);
            $model->shema_manage_id=$scheme_manage_id;
            $model->name=json_encode($namearr);
        }
        if (Yii::$app->request->post()&&$model->save()) {
            //   return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            $m=new schemaControl();
            $modeselect=$m->findSelect();
            return $this->render('update', [
                'model' => $model,
                'modeselect'=>$modeselect,
                'modelarr'=>$modelarr,
            ]);
        }
    }

    /**
     * Deletes an existing schemaManage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the schemaManage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return schemaManage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = schemaControl::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * 根据时间模式ID，查找详细信息
     */

    public function actionGetTimeMode()
    {
        $id=$_POST['id'];
        $schemeManage=new schemaManage();
        $data=$schemeManage->find()->where(['id'=>$id])->asArray()->one();
        return json_encode($data);
    }
}
