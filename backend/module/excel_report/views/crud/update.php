<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ExcelReport $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Excel Report',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
