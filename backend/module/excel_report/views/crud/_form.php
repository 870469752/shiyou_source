<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\TimeMode;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var backend\models\ExcelReport $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
						<?= $form->field($model, 'time_mode_id')->dropDownList(ArrayHelper::map(TimeMode::getOneDate('id,name'),'id','name'), ['class'=>'select2']) ?>
                            <br>
                            <section id="widget-grid" class="">
                                <!-- START ROW -->
                                <div class="row">
                                    <!-- NEW COL START -->
                                    <article class="col-sm-12 col-md-12 col-lg-12">

                                        <!-- Widget ID (each widget will need unique ID)-->
                                        <div class="jarviswidget"
                                             data-widget-deletebutton="false"
                                             data-widget-editbutton="false"
                                             data-widget-colorbutton="false"
                                             data-widget-sortable="false">
                                            <header>
					                            <span class="widget-icon">
						                            <i class="fa fa-edit"></i>
					                            </span>
                                                <h2><?= Html::encode('编辑表头') ?></h2>
                                            </header>
                                            <!-- widget div-->
                                            <div>
                                                <!-- widget edit box -->
                                                <div class="jarviswidget-editbox">
                                                    <!-- This area used as dropdown edit box -->
                                                    <input class="form-control" type="text">
                                                </div>

                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-default" id="add_row">Left</button>

                                                </div><br>
                                                <div class="row">
                                                    <div class="panel panel-default col-lg-4 col-xs-6 col-md-6">
                                                        <div class="panel-body"><p>1#制冷机</p></div>
                                                    </div>
                                                    <div class="panel panel-default col-lg-4 col-xs-6 col-md-6">
                                                        <div class="panel-body"><p>2#制冷机</p></div>
                                                    </div>
                                                    <div class="panel panel-default col-lg-4 col-xs-6 col-md-6">
                                                        <div class="panel-body"><p>3#制冷机</p></div>
                                                    </div>
                                                </div>
                                                <!-- end widget edit box -->
                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <!-- end widget -->
                                    </article>
                                    <!-- END COL -->
                                </div>
                            </section>
						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		<!-- END COL -->
	</div>
</section>
<?php

?>
<script>
    window.onload = function(){
        var html = $('#report_time_title_field').html();
        var div = '<div class="form-group"><div class="col-lg-11"><div class="col-lg-6"> <div class="input-group"> <span class="input-group-addon"> <input type="checkbox"> </span> <select class="form-control" name="ExcelReport[name]"> <option>点位选择</option> <option>COP1</option> <option>Chiller</option> </select> </div> </div> <div class="col-lg-6"> <select class="form-control" name="ExcelReport[name]"> <option>取值方式</option> <option>实时值</option> <option>累计值</option> <option>最大值</option> <option>最小值</option> </select> </div> <br></div><div class="col-lg-1"><button type="button" class="btn btn-default btn_remove">-</button></div></div><br>';
//        $('#add_point_field').on('click',function(){
//            $('#report_time_title_field_all').after(div);
//        });
        $('.btn-add').click(function(){
//            alert(111);
            var htm = $(this).parent().parent().html();
            console.log(htm);
            $(this).parent().parent().after(htm);
        });



    }
</script>