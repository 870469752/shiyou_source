<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ExcelReport $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Excel Report',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
