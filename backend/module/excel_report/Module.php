<?php

namespace backend\module\excel_report;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\excel_report\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
