<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\library\MyFunc;
use backend\assets\TableAsset;

$this->title = '时间模式';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                        <h2><?= Html::encode($this->title) ?></h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <div class="widget-body no-padding">
                            <>
                        </div>
                    </div>
                </div>
        </article>
    </div>
</section>
