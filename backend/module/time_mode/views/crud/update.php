<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\TimeMode $model
 */

$this->title = Yii::t('time', 'Update {modelClass}: ', [
  'modelClass' => 'Time Mode',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
