<?php

use common\library\MyHtml;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\TimeMode $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
						<?= $form->field($model, 'name')->textInput() ?>
                        <?= $form->field($model, 'repeat')->dropDownList([
                            '' => Yii::t('app', '----'),
                            'day' => Yii::t('app', 'Repeat/Day'),
                            'week' => Yii::t('app', 'Repeat/Week'),
                            'month' => Yii::t('app', 'Repeat/Month'),
                            'custom' => Yii::t('app', 'Custom')],
                            ['id'=>'repeat_time', 'class'=>'select2','style' => 'width:200px']) ?>
                            <div class="row" >
                                <div class="col-lg-12" id="month_week_one">
                                    <!--开始 日历插件-->
                                    <div class="jarviswidget jarviswidget-color-black"
                                         data-widget-deletebutton="false"
                                         data-widget-editbutton="false"
                                         data-widget-colorbutton="false"
                                         data-widget-sortable="false">
                                        <header>
                                            <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                                            <h2> <?= Yii::t('app', '划分间隔')?> </h2>
                                        </header>
                                        <!-- widget div-->
                                        <div id="month_week">
                                            <div class="widget-body no-padding">
                                                <!-- content goes here -->
                                                <div class="widget-body-toolbar">
                                                    <div class="widget-body-toolbar">
                                                        <input type="text" style = "width:110px;display:none" id ="timepick" name="mydate" readonly placeholder="Select a date" class="form_datetime"/>
                                                        <div id="calendar-buttons">
                                                            <!-- add: non-hidden - to disable auto hide -->
                                                            <div class="btn-group" id = 'select_btn'>
                                                                <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                                                    Showing <i class="fa fa-caret-down"></i>
                                                                </button>
                                                                <ul class="dropdown-menu js-status-update pull-right">
                                                                    <li>
                                                                        <a href="javascript:void(0);" id="mt">Month</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:void(0);" id="ag">Agenda</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:void(0);" id="td">Today</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="btn-group">
                                                                <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                                   id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                                                <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                                   id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="calendar"></div>
                                                    <!-- end content -->
                                                </div>
                                            </div>
                                            <input type="hidden" name="TimeMode[interval]">
                                            <!-- end widget div -->
                                        </div>
                                    </div>
                                    <!--结束  日历插件-->
                                </div>
                                <div class="col-lg-6" style="display:none" id="month_day">
                                    <!--开始 日历插件-->
                                    <div class="jarviswidget jarviswidget-color-black"
                                         data-widget-deletebutton="false"
                                         data-widget-editbutton="false"
                                         data-widget-colorbutton="false"
                                         data-widget-sortable="false">
                                        <header>
                                            <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                                            <h2> <?= Yii::t('app', '划分每天')?> </h2>
                                        </header>
                                        <!-- widget div-->
                                        <div id="month_week">
                                            <div class="widget-body no-padding">
                                                <!-- content goes here -->
                                                <div class="widget-body-toolbar">
                                                    <div style="margin-top:24px;"></div>
                                                    <div id="calendar1"></div>
                                                    <!-- end content -->
                                                </div>
                                            </div>
                                            <input type="hidden" name="TimeMode[interval_day]">
                                            <!-- end widget div -->
                                        </div>
                                    </div>
                                    <!--结束  日历插件-->
                                </div>
                            </div>
						</fieldset>
						<div class="form-actions" id="submit">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('time', 'Create') : Yii::t('time', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/ion-slider/ion.rangeSlider.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/qtip/jquery.qtip-1.0.0-rc3.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/bootbox/bootbox.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/my_js/calendar_time.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/my_js/calendar_time1.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function(){
        var date = new Date();
        /*自定义，时间选择*/
        function timeTypeChange(time_type)
        {
            $(".form_datetime").datetimepicker('remove');
            $(".form_datetime").val('');
            if(time_type == 'month')
            {
                $(".form_datetime").datetimepicker({
                    format: "yyyy-mm",
                    startView: 'year',
                    minView: 'year',
                    todayBtn:'linked',
                    autoclose: true
                });
                $(".form_datetime").datetimepicker('setStartDate', '2015-01');
            }else
            {
                $(".form_datetime").datetimepicker({
                    format: "yyyy-mm-dd",
                    startView: 'month',
                    minView: 'month',
                    todayBtn:'linked',
                    autoclose: true
                });
                $(".form_datetime").datetimepicker('setStartDate', '2015-01-01');
            }
        }
        /*添加自定义按钮 点击事件*/
        $('#calendar-buttons #btn-prev').click(function () {
            $('#calendar').fullCalendar('prev');
            return false;
        });
        $('#calendar-buttons #btn-next').click(function () {
            $('#calendar').fullCalendar('next');
            return false;
        });
        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-button-today').click();
            return false;
        });
        /* 隐藏 日历切换按钮 */
        function hide_buttons()
        {
            $('.fc-header-right, .fc-header-center').hide();
            $('#calendar-buttons #btn-prev').hide();
            $('#calendar-buttons #btn-next').hide();
            $('#timepick').hide();
            $('#select_btn').hide();
        }
        /* 显示 日历切换按钮 */
        function show_buttons()
        {
            $('.fc-header-right, .fc-header-center').show();
            $('#calendar-buttons #btn-prev').show();
            $('#calendar-buttons #btn-next').show();
            $('#timepick').show();
        }
        /*切换视图*/
        $('#repeat_time').on('click',function(e){
            rtype = $(this).val();
            switch(rtype) {
                case 'year':
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar1').fullCalendar('removeEvents');
                    timeTypeChange('month');
                    $('#calendar').fullCalendar('changeView', 'month');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('today');
                    $('#month_day').css('display','none');
                    break;
                case 'month':
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar1').fullCalendar('removeEvents');
                    $('#month_week_one').removeClass('col-lg-12').addClass('col-lg-6');
                    $('#month_day').css("display","inline");
                    hide_buttons()
                    $('#calendar').fullCalendar('changeView', 'month');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate(date));
                    $('#calendar1').fullCalendar('changeView', 'agendaDay');
                    break;
                case 'week':
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar1').fullCalendar('removeEvents');
                    $('#month_week_one').removeClass('col-lg-12').addClass('col-lg-6');
                    $('#month_day').css("display","inline");
                    hide_buttons()
                    $('#calendar').fullCalendar('changeView', 'agendaWeek');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate(date));
                    $('#calendar1').fullCalendar('changeView', 'agendaDay');
                    break;
                case 'day':
                    $('#calendar').fullCalendar('removeEvents');
                    $('#month_week_one').removeClass('col-lg-6').addClass('col-lg-12');
                    $('#month_day').css('display','none');
                    hide_buttons()
                    $('#calendar').fullCalendar('changeView', 'agendaDay');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate(date));
                    break;
                case 'custom':
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar1').fullCalendar('removeEvents');
                    $('#month_week_one').removeClass('col-lg-6').addClass('col-lg-12');
                    $('#calendar').fullCalendar('changeView', 'month');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('today');
                    $('#month_day').css('display','none');
                    show_buttons();
                    $('#select_btn').show();
                    timeTypeChange('month');
                    $('#mt').click(function () {
                        $('#calendar').fullCalendar('removeEvents');
                        $('#calendar1').fullCalendar('removeEvents');
                        $('#month_week_one').removeClass('col-lg-12').addClass('col-lg-6');
                        $('#month_day').css('display','block');
                        $('#calendar1').fullCalendar('changeView', 'agendaDay');
                        timeTypeChange('month');
                        $('#calendar').fullCalendar('changeView', 'month');
                    });

                    $('#ag').click(function () {
                        $('#calendar').fullCalendar('removeEvents');
                        $('#calendar1').fullCalendar('removeEvents');
                        $('#month_week_one').removeClass('col-lg-6').addClass('col-lg-12');
                        $('#month_day').css('display','none');
                        timeTypeChange('day');
                        $('#calendar').fullCalendar('changeView', 'agendaWeek');
                    });

                    $('#td').click(function () {
                        $('#calendar').fullCalendar('removeEvents');
                        $('#calendar1').fullCalendar('removeEvents');
                        $('#month_week_one').removeClass('col-lg-6').addClass('col-lg-12');
                        $('#month_day').css('display','none');
                        timeTypeChange('day');
                        $('#calendar').fullCalendar('changeView', 'agendaDay');
                    });
                    break;
            }
        });
        /*时间选择框中的 值 变化时 对应的 fullcalendar 也发生变化*/
        $('.form_datetime').datetimepicker().on('changeDate', function(ev){
            var time_data = new Date($(".form_datetime").val());
            $("#calendar").fullCalendar('gotoDate', time_data.getFullYear(),time_data.getMonth(),time_data.getDate());
        });
        //初始化
        <?php if(!$model->isNewRecord):?>
        var interval = <?=$model->interval?>;
        var time_type = '<?=$model->repeat?>';
        $('#repeat_time').trigger('click');
        $("#calendar").fullCalendar('addEventSource',{
            events:interval.data.time_range,
            className:'bg-color-darken',
            editable:true
        });
        $.each(interval.data.time_range,function(){

        })
        $("#calendar1").fullCalendar('addEventSource',{
            events:interval.data.interval_day,
            className:'bg-color-darken',
            editable:true
        });
        console.log(interval.data.time_range);
        console.log(interval.data.interval_day);
        console.log(time_type);
        <?php endif; ?>
        /*点击提交  收集日历上所有的事件 并将时间值赋给 hidden input框*/
        $('#submit').click(function () {
            /*日历数据提交*/
            var _datas = '';
            var data = $('#calendar').fullCalendar('clientEvents');
            $.each(data, function (key, value) {
                $.each(value, function (k, v) {
                    if(k == 'title'){
                        _datas += v + ',';
                    }else if (k == 'start') {
                        _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm:ss") + ',';
                    }else if(k == 'end'){
                        _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm:ss");
                    }
                })
                _datas += ';';
            })
            $("input[name='TimeMode[interval]']").val(_datas);
            /**/
            var flag = $('#month_day').css('display');
            if(flag == 'block'){
                var data_day = '';
                var data_1 = $('#calendar1').fullCalendar('clientEvents');
                $.each(data_1, function (key, value) {
                    $.each(value, function (k, v) {
                        if(k == 'title'){
                            data_day += v + ',';
                        }else if (k == 'start') {
                            data_day += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm:ss") + ',';
                        }else if(k == 'end'){
                            data_day += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm:ss");
                        }
                    })
                    data_day += ';';
                })
                $("input[name='TimeMode[interval_day]']").val(data_day);
            }
        })
    }
</script>