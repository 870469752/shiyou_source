<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\TimeMode $model
 */
$this->title = Yii::t('time', 'Create {modelClass}', [
  'modelClass' => 'Time Mode',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
