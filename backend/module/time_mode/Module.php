<?php

namespace backend\module\time_mode;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\time_mode\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
