<?php

namespace backend\module\time_mode\controllers;

use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
