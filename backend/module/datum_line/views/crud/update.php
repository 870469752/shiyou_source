<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 */

$this->title = Yii::t('app', 'Update Datum Line', [
  'modelClass' => 'Simulate Point',
]) .': ' .$model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit' => $unit
    ]) ?>
