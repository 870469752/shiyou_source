<?php

namespace backend\module\datum_line;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\datum_line\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
