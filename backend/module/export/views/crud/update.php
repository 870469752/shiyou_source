<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Export $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Export',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
