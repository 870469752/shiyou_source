<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Export $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Export',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
