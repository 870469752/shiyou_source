<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;
/**
 * @var yii\web\View $this
 * @var backend\models\Export $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <?= $form->field($model, 'point_id')->dropDownList(
                            MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getAvailablePoint(), 'id', 'name')),
                            ['class' => 'select2','multiple' => 'multiple', 'style' => 'width:200px', 'placeholder' => '请选择点位']
                        ); ?>

                        <?= $form->field($model, 'statistics_type')->dropDownList(['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'week' => Yii::t('app', 'Week'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour'),
                                'run time' => Yii::t('app', 'Run Time')],['class'=>'select2','style' => 'width:200px', 'placeholder' => '请选择导出数据统计时间类型']); ?>

                        <?= $form->field($model, 'statistics_mode')->dropDownList(['' => '',
                                'max' => 'max',
                                'min' => 'min',
                                'avg' => 'avg',
                                'sum' => 'sum',],['class'=>'select2','style' => 'width:200px', 'placeholder' => '请选择导出数据统计方式']); ?>

                        <?= $form->field($model, 'statistics_range')->dropDownList(['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'week' => Yii::t('app', 'Week'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour')],['class'=>'select2','style' => 'width:200px', 'placeholder' => '请选择导出时间范围']); ?>

						<?= Html::hiddenInput('Export[format]', 'excel') ?>

                        <?= Html::hiddenInput('Export[saving_mode]' , '1') ?>

                        <?= Html::hiddenInput('Export[path]', substr(__FILE__, 0, 3).'export') ?>

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>

<script>
    window.onload = function () {
        //select2 需要将默认值用他的方式添加
    }
</script>