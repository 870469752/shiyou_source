<?php

namespace backend\module\export\controllers;

use backend\controllers\MyController;
use backend\models\PointData;
use common\library\MyExport;
use Yii;

class DefaultController extends MyController
{
    /**
     * highcharts导出服务
     */
    public function actionIndex()
    {
        echo 123;
    }

    /**
     * 节能模式专用导出
     */
    public function actionSavingMode()
    {
        $get = YII::$app->request->get();
        //临时写的，以后修改
        $plugin_config = json_decode((new \yii\db\Query())
            ->select('parameter')
            ->where(['plugin_id' => 'saving'])
            ->from('core.plugin_config')
            ->one()['parameter'], true);
        $key_names = [
            'saving_electricity', //节能时电量点位
            'not_saving_electricity', // 非节能时电量点位
        ];
        foreach($key_names as $v){
            $id = isset($plugin_config[$v])?$plugin_config[$v]:0;
            $result[$v] = PointData::findMultiDate($id, $get['date'])
                ->select(['timestamp' => "to_char(timestamp, 'YYYY-MM-DD HH24:MI:SS')",'value' => "\"value\" || 'KWH'"])->orderBy('timestamp')->asArray()->all();
        }
        $data = array_merge($result['saving_electricity'],[[],['非节能模式下趋势']],$result['not_saving_electricity']);
        $header = [
            ['节能时段总耗电量',$get['saving_total']],
            ['非节能时段总耗电量',$get['not_save_total']],
            ['节能时段平均COP',$get['saving_cop']],
            ['非节能时段平均COP',$get['not_save_cop']],
            ['总时段平均COP',$get['cop']],
            ['节能量',$get['saving_quantity']],
            ['节能率',$get['saving_ratio']],
            [],
            ['节能模式下趋势'],
            ['时间','数值'],
        ];
        MyExport::run($data, $header);
    }
}
