<?php

namespace backend\module\export;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\export\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
