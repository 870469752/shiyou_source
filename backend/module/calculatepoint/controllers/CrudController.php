<?php

namespace backend\module\calculatepoint\controllers;

use Yii;
use backend\models\CalculatePoint;
use backend\models\Point;
use backend\models\Template;
use backend\models\search\CalculatePointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\Unit;
use backend\models\Category;
use backend\models\OperateLog;
/**
 * CrudController implements the CRUD actions for CalculatePoint model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CalculatePoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalculatePointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single CalculatePoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CalculatePoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $type = Yii::$app->request->get('type');
        $type = !empty($type) ? $type : 1;
        $model = new CalculatePoint;

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->Calsave()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建计算点位';
            $op['en'] = 'Create calculate point';
            @$this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $points = MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getOfStandardUnit(), 'id', 'name'));
            $formula =Template::getFormula();
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());
            return $this->render($type == 1 ? 'create' : 'custom/create', [
                'model' => $model,
                'unit' => $unit,
                'category_tree' => $category_tree,
                'points' => $points,
                'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
            ]);
        }
    }

    /**
     * Updates an existing CalculatePoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $protocol_id = '')
    {
        $params = Yii::$app->request->post();
        $model = $this->findModel($id);
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->Calsave())
        {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改计算点位';
            $op['en'] = 'Update calculate point';
            @$this->getLogOperation($op,$info,$result);

            //判断当前修改是哪个控制器请求的 如果protocol_id不为空说明来自point/crud的请求在执行完毕后跳转回point/crud,否则跳到当前控制器
            if($params['protocol_id']) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['/point/crud']);
            }
        }
        else {
            $model->name = Myfunc::DisposeJSON($model->name);
            $formula = explode(' ', $model->formula);

            foreach($formula as $key => $value)
            {
                //以 字符 'p'为判断是否为点位
                if(($n = strpos($value, 'p')) !== false)
                {
                    /**
                     *  如果为点位 需要做两件事：
                     *                       1、将 单位转换的 系数去掉
                     *                       2、加上点位的js标识 '[]'
                     */
                    $point_id = substr($value, $n + 1);
                    $point = Point::getById($point_id);
                    //掉用 _map 找到一个 能标识 当前点位的 name;
                    $point_name = Myfunc::_map([$point['attributes']], 'id' , 'name');

                    $formula[$key] = '[' . $point_name[$point_id].']';
                }
            }
//            $model->template_id = Template::getNameById($model->template_id);
            $model->formula = $model->template_id ? implode(',',$formula) : $model->formula;
            $points = MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getOfStandardUnit(), 'id', 'name'));
            $formula =Template::getFormula();
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());
            $model->filter_max_value = MyFunc::JsonFindValue($model->value_filter, 'data.max');
            $model->filter_min_value =MyFunc::JsonFindValue($model->value_filter, 'data.min');
            $model->interval = MyFunc::SecondChangeToTime($model->interval);
            // 把分类关系表的分类ID和类型都载入进来
//            $PointCategoryRel = PointCategoryRel::find()->where(['point_id' => $id])->all();
//            $model->category_id = MyFunc::arrayTwoOne($PointCategoryRel, 'category_id');
            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());
            return $this->render($model->template_id ? 'update' : 'custom/update', [
                'model' => $model,
                'unit' => $unit,
                'protocol_id' => $protocol_id,
                'category_tree' => $category_tree,
                    'category_tree' => $category_tree,
                    'points' => $points,
                    'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
            ]);
        }
    }

    /**
     * 修改删除，将删除点位变为点位屏蔽
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除一个计算点位';
        $op['en'] = 'Delete calculate points';
        @$this->getLogOperation($op,'','');

        $model = $this->findModel($id);
        $model->is_shield = true;

        $res = $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the CalculatePoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalculatePoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CalculatePoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 利用 ajax给 js文件传递 点位信息数据
     */
    public function actionAjaxPoint()
    {
        echo json_encode(MyFunc::_map(Point::getOfStandardUnit(), 'id', 'name'));
    }

    /**
     * 利用 ajax 传递 公式模板
     */
    public function actionAjaxFormula()
    {
        echo json_encode(Template::getFormula());
    }

}
