<?php

namespace backend\module\calculatepoint;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\calculatepoint\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
