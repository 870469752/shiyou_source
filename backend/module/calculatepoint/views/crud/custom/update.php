<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CalculatePoint $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Calculate Point',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'category_tree' => $category_tree,
        'unit' => $unit,
        'points' => $points,
        'formula' => $formula
    ]) ?>
