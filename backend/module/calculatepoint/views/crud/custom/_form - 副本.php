<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;
use yii\helpers\Url;
use backend\models\Template;
/**
 * @var yii\web\View $this
 * @var backend\models\CalculatePoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">
        <div id="tabs"  class="  ui-tabs ui-widget ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                <li>
                    <?=Html::a(Yii::t('app', '模板公式'), ['crud/create','type' => 1], ['class' => 'fa fa-slack'])?>
                </li>
                <li class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                    <?=Html::a(Yii::t('app', '自定义公式'), ['crud/create','type' => 2], ['class' => 'fa fa-sliders'])?>
                </li>

            </ul>
        </div>
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <?= $form->field($model, 'name')->textInput() ?>

                            <?= Html::hiddenInput('CalculatePoint[formula]', '', ['id' => '_calculate'])?>
                            <!--自定义公式-->
                            <span>选择公式元素：</span>
                            <div class = 'btn-group'>
                            <div class="btn-group dropup">
                                <a class = 'btn btn-default btn-xs'   href="javascript:void(0)" id="_operation" data-type="select2" data-pk="1" >运算符号</a>
                            </div>

                            <div class="btn-group dropup">
                                <a class = 'btn btn-info btn-xs' href="javascript:void(0)" id="_func" data-type="select2" data-pk="2" >函数</a>
                            </div>



                            <div class="btn-group dropup">
                                <a class = 'btn btn-default btn-xs' href="javascript:void(0)" id="_num" data-type="text" data-pk="3" >数字</a>
                            </div>


                            <div class="btn-group dropup">
                                <a class = 'btn btn-info btn-xs' href="javascript:void(0)" id="_point" data-type="select2" data-pk="4" >点位</a>
                            </div>
                            </div>



                            <div class="input-group">
                                <div class = 'form-control' id= '_custom' style = 'margin-top:2px'>


                                </div>
                                <div class="input-group-btn" id = '_del'>
                                    <button class="btn btn-default btn-primary" type="button">
                                        <i class="glyphicon glyphicon-backward"></i> Backspace
                                    </button>
                                </div>
                            </div>



                            <!--在系统中选择 点位单位-->
                            <?=$form->field($model, 'unit')->dropDownList(
                                $unit,
//                            ArrayHelper::map(Unit::getAllUnit(), 'id', 'unit'),
                                ['class'=>'select2', 'style' => 'width:200px']
                            )?>

                            <HR />

                            <?= $form->field($model, 'filter_max_value')->textInput()?>

                            <?= $form->field($model, 'filter_min_value')->textInput()?>

                            <lable><?=Yii::t('app', 'Record Interval')?> :</lable>
                            <a href="#"  id="interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                            <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框//注意-->
                            <?= Html::hiddenInput('CalculatePoint[interval]', $model->interval ? $model->interval : 'Empty', ['id' => 'interval_time'])?>
                            <hr />

                            <?= $form->field($model, 'is_shield')->checkbox() ?>

                            <?= $form->field($model, 'is_upload')->checkbox() ?>

                            <?= Html::hiddenInput('template_name', empty($model->template_id) ? '' : Template::getNameById($model->template_id))?>

                            <?= $form->field($model, 'template_id')->hiddenInput()->label('')?>


                            <HR />
                            <!--提示-->
                            <p class="text-danger slideInRight fast animated">
                                <span class="label label-warning">
				                 提示
                                </span> &nbsp; 公式模板中替换的所有点位选项,都需要满足<span style = 'color:#F00'>选择点位的单位必须包含在单位管理列表中</span>,请确保需要替换点位的单位存在单位管理列表中.
                                <span style = 'color:#330066'>如果不存在</span>可以到 <a href = '<?= Url::toRoute('/unit/crud/create');?>' target = '_blank'>单位管理</a> 中添加.
                            </p>
                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>

    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_ate'  style="top: -89px; left: -135px; display: block;">
        <div class="arrow"></div>
        <h3 class="popover-title">选择点位</h3>
        <div class="popover-content"><div>
                <div class="editableform-loading" style="display: none;">
                </div><form class="form-inline editableform" style="">
                    <div class="control-group form-group"><div>
                            <div class="editable-input"><div class="select2-container" id="s2id_autogen4" style="width: 200px;">
                                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                        <span class="select2-chosen" id="select2-chosen-5">选择点位</span>
                                            <abbr class="select2-search-choice-close"></abbr>
                                        <span class="select2-arrow" role="presentation"><b role="presentation"></b></span>
                                    </a>
                                    <label for="s2id_autogen5" class="select2-offscreen"></label>
                                    <input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-5" id="s2id_autogen5">
                                </div><input type="hidden" value="" tabindex="-1" title="" class="select2-offscreen"></div>
                            <div class="editable-buttons">
                                <button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                                <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                            </div></div><div class="editable-error-block help-block" style="display: none;"></div></div>
                </form></div>
        </div>
    </div>

</section>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>
<script>
    window.onload = function (){
        var $interval = $('#interval_time').val();
        var point_str = '';
        var points = [];
        /* x-editable 生成时间选择插件 */
        timeOfEditable( $("#interval") );
        $('#interval').text($interval);
        /*********************************************editables  插件编写*********************************/
        //point 点位数据获取
        $.ajax({
            url: '/calculate-point/crud/ajax-point',
            async:false,
            dateType: 'json',
            success: function(data)
            {
                point_json = JSON.parse(data)
                $.each(point_json, function (k, v) {
                        points.push({
                            value: 'p'+k,
                            text: v
                        });
                });
            }
        });
        $('#_operation').editable({
            source: [{
                value: '',
                text: ''
             },
                {
                value: '＋',
                text: '＋'
            }, {
                value: '－',
                text: '－'
            },{
                value: '*',
                text: '×'
            }, {
                value: '/',
                text: '÷'
            }, {
                value: '(',
                text: '('
            }, {
                value: ')',
                text: ')'
            }
            ],
            select2: {
                width: 200,
                placeholder: '选择运算符号'
            },
            title: "选择运算符号"
        })

        $('#_func').editable({
            source: [{
                value: '',
                text: ''
            },{
                value: 'sin(',
                text: 'sin'
            }, {
                value: 'cos(',
                text: 'cos'
            }],
            select2: {
                width: 200,
                placeholder: '选择函数'
            },
            title: "选择函数"
        })

        $('#_num').editable({
            name: '_num',
            tpl: "<input class = 'temp' type = 'text' />",
            title: '填写数字'
        });

        $('#_point').editable({
            source: points,
            select2: {
                width: 200,
                placeholder: '选择点位'
            },
            title: "选择点位"
        });

        /*********************************************** 公式元素选择 **********************************************/
        var $_operation = $('#_operation');                                           //公式中的运算符
        var $_func = $('#_func');                                                    //公式中的函数
        var $_num = $('#_num');                                                     //公式中的数字
        var $_point = $('#_point');                                                //公式中的点位
        elementToFormula($_operation);
        elementToFormula($_func);
        elementToFormula($_num);
        elementToFormula($_point);
        /************************************** 将添加的公式元素放入公式列表中 ****************************************/
        function elementToFormula($elt)
        {
            $elt.on('save.newuser', function(e, params){
                var $this = $(this);
                var $data = '';
                var $old_text = $this.text();
                var $value = params.newValue;
                //异步将 显示的文本添加到公式框中
                setTimeout(
                    function(){
                        if($elt.selector == '#_func')
                            $data = $value;
                        else
                            $data = $this.text();
                        $('#_custom').append(" <span class = '_fom' data-value = "+$value+" data-type = "+$elt.selector+">"+$data+"</sapn>");

                        var $last_type = elementTraversal();
                        $('.btn-group a').removeAttr('disabled');
                        $('.btn-group a').removeClass('editable-empty');
                        switch($last_type){
                            case '#_operation':
                                $('#_operation').attr('disabled', 'disabled');
                                break;
                            case '#_func':
                                break;
                            case '#_num':
                                $('#_num').attr('disabled', 'disabled');
                                $('#_point').attr('disabled', 'disabled');
                                $('#_func').attr('disabled', 'disabled');
                                break;
                            case '#_point':
                                $('#_num').attr('disabled', 'disabled');
                                $('#_point').attr('disabled', 'disabled');
                                $('#_func').attr('disabled', 'disabled');
                                break;
                        }
                        $elt.editable('setValue', null);
                        $this.text($old_text);
                    },0);
            })
        }

        /************************************* 将公式放入input 隐藏框中 ****************************************/
        function elementTraversal()
        {
            var $last_type = '';
            var $formula = '';
            $('._fom').each(function(){
                $formula += $(this).data('value');
                $last_type = $(this).data('type');
            });
            $('#_calculate').val( $formula);
            return $last_type;
        }

        /************************************ 删除按钮 **************************************************/
        $('#_del').click(function(){
            $('._fom:last').remove();
            elementTraversal();
        });

        $('.editable-submit').on('click', function(){
            alert(1);
        })
    }
</script>