<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CalculatePoint $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Calculate Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit' => $unit,
        'points' => $points,
        'formula' => $formula
    ]) ?>
