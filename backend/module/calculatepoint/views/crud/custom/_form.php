<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;
use yii\helpers\Url;
use backend\models\Template;
/**
 * @var yii\web\View $this
 * @var backend\models\CalculatePoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">
        <div id="tabs"  class="  ui-tabs ui-widget ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                <li>
                    <?=Html::a(Yii::t('app', '模板公式'), ['crud/create','type' => 1], ['class' => 'fa fa-slack'])?>
                </li>
                <li class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                    <?=Html::a(Yii::t('app', '自定义公式'), ['crud/create','type' => 2], ['class' => 'fa fa-sliders'])?>
                </li>

            </ul>
        </div>
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <?= $form->field($model, 'name')->textInput() ?>

                            <?= Html::hiddenInput('CalculatePoint[formula]',$model->isNewRecord ? '' : $model->formula , ['id' => '_calculate'])?>


                            <div class="input-group" id = '_formula'>
                                <div class = 'select2-container select2-container-multi' style = 'margin-top:2px;width: 100%;'>
                                    <ul class="select2-choices">
                                        <li class="select2-search-choice" data-status = '0' data-type = 'plus' style = 'background-color:#AFEDF5;border:none;'>
                                            <div><i class="fa fa-plus"></i></div>
                                            <a style = 'display:none' href="#" class="select2-search-choice-close _delete" tabindex="-1"></a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="input-group-btn" id = '_del'>
                                    <button class="btn btn-default btn-primary" type="button">
                                        <i class="fa fa-mail-reply-all"></i> Clear
                                    </button>
                                </div>
                            </div>

                            <br />

                            <!--在系统中选择 点位单位-->
                            <?=$form->field($model, 'unit')->dropDownList(
                                $unit,
//                            ArrayHelper::map(Unit::getAllUnit(), 'id', 'unit'),
                                ['class'=>'select2', 'style' => 'width:200px', 'placeholder' => '选择单位']
                            )?>

                            <HR />

                            <?= $form->field($model, 'filter_max_value')->textInput()?>

                            <?= $form->field($model, 'filter_min_value')->textInput()?>

                            <lable><?=Yii::t('app', 'Record Interval')?> :</lable>
                            <a href="#"  id="interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                            <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框//注意-->
                            <?= Html::hiddenInput('CalculatePoint[interval]', $model->interval ? $model->interval : 'Empty', ['id' => 'interval_time'])?>
                            <hr />

                            <?= $form->field($model, 'is_shield')->checkbox() ?>

                            <?= $form->field($model, 'is_upload')->checkbox() ?>

                            <?= Html::hiddenInput('template_name', empty($model->template_id) ? '' : Template::getNameById($model->template_id))?>

                            <?= $form->field($model, 'template_id')->hiddenInput()->label('')?>


                            <HR />
                            <!--提示-->
                            <p class="text-danger slideInRight fast animated">
                                <span class="label label-warning">
				                 提示
                                </span> &nbsp; 公式模板中替换的所有点位选项,都需要满足<span style = 'color:#F00'>选择点位的单位必须包含在单位管理列表中</span>,请确保需要替换点位的单位存在单位管理列表中.
                                <span style = 'color:#330066'>如果不存在</span>可以到 <a href = '<?= Url::toRoute('/unit/crud/create');?>' target = '_blank'>单位管理</a> 中添加.
                            </p>
                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_sub', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>

        <!-- END COL -->
    </div>

    <!-- 自定义公式模板 -->
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_ate'  style="display: none;">
        <div class="arrow"></div>
        <h3 class="popover-title">选择点位</h3>
        <div class="popover-content">
                <div class="editableform-loading" style="display: none;"></div>
                    <form class="form-inline editableform" style="">
                        <div class="control-group form-group"><div>
                                <div class="editable-input">
                                    <div class="editable-address">
                                        <?=Html::dropDownList('gs', '', [
                                                    '' => '',
                                                    '_fh' => '运算符',
                                                    '_hs' => '函数',
                                                    '_sz' => '数字',
                                                    '_dw' => '点位',
                                                    '_mb' => '模板',
                                            ], ['class' => 'select2', 'placeholder' => '请选择公式组成元素', 'id' => '_ele'])?>
                                    </div>
                                    <div class="editable-address _calculate" id = '_fh' style = 'display:none'>
                                        <?=Html::dropDownList('_fh', '', [
                                                '' => '',
                                                '＋' => '＋',
                                                '－' => '－',
                                                '*' => '×',
                                                '/' => '÷',
                                                '(' => '(',
                                                ')' => ')'
                                            ], ['class' => 'select2', 'placeholder' => '请选择运算符号'])?>
                                    </div>
                                    <div class="editable-address _calculate" id = '_hs' style = 'display:none'>
                                        <?=Html::dropDownList('_hs', '', [
                                                '' => '',
                                                'sin(' => 'sin',
                                                'cos(' => 'cos',
                                                'sum(' => 'max',
                                                'avg(' => 'avg'
                                            ], ['class' => 'select2', 'placeholder' => '请选择函数'])?>
                                    </div>
                                    <div class="editable-address _calculate" id = '_sz' style = 'display:none'>
                                        <input type = 'text' name = '_sz' style = 'width:100%' placeholder = '请填写数字'>
                                    </div>
                                    <div class="editable-address _calculate" id  = '_dw' style = 'display:none'>
                                        <?=Html::dropDownList('_dw', '', $points, ['class' => 'select2', 'placeholder' => '请选择点位'])?>
                                    </div>
                                    <div class="editable-address _calculate" id  = '_mb' style = 'display:none'>
                                        <?=Html::dropDownList('_mb', '', $formula, ['class' => 'select2', 'placeholder' => '请选择模板公式'])?>
                                    </div>
                                    <div class="editable-address _calculate" id = '_error_log' style = 'display:none;color:indianred'></div>
                                </div>
                                <div class="editable-buttons">
                                    <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                                    <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                                </div></div>
                            <div class="editable-error-block help-block" style="display: none;"></div>
                        </div>
                    </form>
            </div>
    </div>
    <!-- 自定义公式模板 END -->

</section>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>
<script>
    window.onload = function (){
        var $interval = $('#interval_time').val();
        var $formula = $('#_calculate').val().split(' ');
        /* x-editable 生成时间选择插件 */
        timeOfEditable( $("#interval") );
        $('#interval').text($interval);
        var $_last_li = $('.select2-choices li').last();
        $.each($formula, function(k, v){
            var $_html = '';
            if(v){
                //如果值不为空
                $_html += "<li class='select2-search-choice' data-value = "+v+" data-type = 'ele' data-status = '0' style = 'background-color:#FFF;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                    "<div>" + v + "</div>" +
                    "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                    "</li>";
                $_last_li.before($_html);
            }
        })

        /*******************************自定义公式 js***************************************************/

        //清空所有公式元素块儿的状态
        function _clear()
        {
            $('.select2-choices li').each(function(){
               $(this).data('status', 0);
            });
            $('#_error_log').text('');
        }

        //从所有公式元素快中找出当前选择的li
        function _find()
        {
            var $this = null;
            $('.select2-choices li').each(function(){
                if($(this).data('status') == 1){
                    $this = $(this);
                }
            });
            return $this;
        }
        //添加一个公式元素块li

        /********************************公式元素li块点击事件  ****************************************/
        $('.select2-choices').on('click', 'li', function(event){
            _clear();
            var $_p_left = $('#_formula').position().left;
            var $this = $(this);
            $this.data('status', 1);
            var $cha = - Math.round($('#_ate').width()/2) + 33;
            var $_X = $cha + $(this).position().left;
            var $_Y = $(this).parent().parent().parent().position().top + $(this).position().top;
            //设置弹出层在点击的li元素上方
            $('#_ate').show();
            $('#_ate').css({'left': $_X, 'top': $_Y})

        });

        /************************ 鼠标移入元素块事件 *************************************************/
        $('.select2-choices').on('mouseover', 'li',  function(){
            var $this = $(this);
            if($this.data('type') != 'ele' && $this.data('type') != 'mb')
                return false;
            //先将所有附加的li移除
            $('.select2-choices ._temps').remove();
            //显示 删除按钮
            var $_delete = $this.find('._delete');
            $this.css({'padding':'1px 28px 1px 8px'});
            $_delete.css({'background':'#C9CDBD'});
            $_delete.show();
            //在其前后添加一个假的li
            var $_html = "<li class='select2-search-choice _temps' data-type = 'temp' data-status = '0' style = 'background-color:#F4E3F0;border:dashed 1px #08c;margin-left:0px;' >" +
                         "<div><i class='fa fa-plus'></i></div>" +
                         "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                         "</li>";
            $this.after($_html);
            $this.before($_html);
        })


        $('.select2-choices').on('mouseout', 'li',  function(){
            var $this = $(this);
            var $_delete = $this.find('._delete');
            if($this.data('type') != 'ele' && $this.data('type') != 'mb')
                return false;
            //隐藏 删除按钮
            $this.css({'padding':'1px 8px 1px 1px'});
            $_delete.hide();
        })

        /*******************************根据所选择的公式元素 进行联动 ************************/
        $("#_ele").change(function(){
            $('._calculate').hide();
            var $_chose = $(this).val();
            $('#'+$_chose).show();
        });

        /***************************** 公式表单的各个模块的处理 **********************************/
        var $ele_val = '';
        var $ele_text = '';
        var $select_type = '';
        $('#_ele').change(function(){
            $ele_val = '';
            $ele_text = '';
            $select_type = 'sz';
        })
        $('#_fh').change(function(){
            $ele_val = $("#_fh :selected").val();
            $ele_text = $("#_fh :selected").text();
            $select_type = 'fh';
        });
        $('#_hs').change(function(){
            $ele_text = $ele_val = $("#_hs :selected").val();
            $select_type = 'hs';
        });
        $('#_dw').change(function(){
            $ele_val = 'p' + $("#_dw :selected").val();
            $ele_text = $("#_dw :selected").text();
            $select_type = 'dw';
        });
        $('#_mb').change(function(){
            $ele_val = $ele_text = $("#_mb :selected").val();
            $select_type = 'mb';
        })
        $('input[name=_sz]').blur(function(){
            $ele_val = $ele_text = $(this).val();
        })
        /************************** 确定选择 按钮 *******************************************/
        $('.editable-container').on('click', '.editable-submit', function(){
            var $_obj = _find();
            if($_obj == null){
                $('#_ate').hide();
                return false;
            }
            var $_type = $_obj.data('type');
            if(!$ele_val){
                $('#_error_log').show();
                $('#_error_log').text('元素不能为空');
                return false;
            }

            if($select_type == 'mb'){
                var $_html = '';
                var $_formula_arr = $ele_text.split(' ');
                //将模板中的 元素以li的形式加入到 公式列表中
                $.each($_formula_arr, function(k , v){
                    if(v.indexOf('[') !== -1 || v.indexOf('{') !== -1){
                        //[ ] | { }
                        $_html += "<li class='select2-search-choice' data-type = 'mb' data-status = '0' style = 'background-color:#E7F2FC;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }else{
                        $_html += "<li class='select2-search-choice' data-value = "+v+" data-type = 'ele' data-status = '0' style = 'background-color:#FFF;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }
                });
                $_obj.after($_html);
                $_html = '';
                $_obj.remove();
            }
            else{

                $_obj.find('div').text($ele_text);
                $_obj.data('value', $ele_val);
                $_obj.removeClass('_temps');
                $_obj.data('type', 'ele');
                //已确认块的样式
                $_obj.css({'background-color':'#FFF','color':'#000','border':'none','padding':'1px 8px 1px 1px'});
            }
            $('#_'+$select_type + ' .select2').select2('val', '');
            $('#_'+$select_type).hide();
            $("#_ele").select2('val', '');
            $('#_ate').hide();
            if($_type != 'plus' && $select_type != 'mb'){
                return false;
            }
            if($('#_formula li').last().data('type') == 'plus'){
                return false;
            }
            //判断如果当前选中的元素是＋才会执行下面的clone
            //自动生成一个li
            var $clone = $_obj.clone(true);
            $clone.data('value', '');
            $clone.data('type', 'plus');
            $clone.find('div').html('<i class="fa fa-plus"></i>');
            $clone.css({'background-color':'#AFEDF5', 'color':'#FFF', 'border': 'none','padding':'1px 14px 1px 8px'});

            $('#_formula ul').append($clone);
        })

        /************************* 关闭 按钮 *****************************************************/
        $('.editable-container').on('click', '.editable-cancel', function(){
            var $_obj = _find();
            if($_obj == null){}
            else if($_obj.data('type') == 'temp'){
                $_obj.remove();
            }
            $('#_ate').hide();
        })

        $('.select2-choices').on('click', '._delete', function(event){
            $(this).parent().remove();
            //在此 阻止事件冒泡
            event.stopPropagation();
        })

        $('#_formula').on('mouseout', function() {

            $('._temps').hide();
            $('._temps').mouseover(function(){
                $('._temps').show();
                $('._temps').click(function(){
                    $(this).removeClass('_temps');
                })
            })
        })

        //清空公式列表除了 'plus' 之外的 元素
        $('#_del').click(function(){
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        })
        /************************** 遍历 公式列表中的ele 元素 生成完整公式 ************************************/
        function ele2Formula()
        {
            var $_formula = '';
            $('.select2-choices li').each(function(){
                if($(this).data('type') == 'ele'){
                    $_formula += ' ' + $(this).data('value');
                }
            })
            return $_formula;
        }

        $('#_sub').click(function(){
            var $_formula = '';
            $_formula = ele2Formula();
            $('#_calculate').val($_formula);
        })


    }
</script>