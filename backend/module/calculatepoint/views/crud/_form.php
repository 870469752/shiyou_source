<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;
use yii\helpers\Url;
use backend\models\Template;
/**
 * @var yii\web\View $this
 * @var backend\models\CalculatePoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">
                <div id="tabs"  class="  ui-tabs ui-widget ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                        <li class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                            <?=Html::a(Yii::t('app', '模板公式'), ['crud/create','type' => 1], ['class' => 'fa fa-slack'])?>
                        </li>
                        <li>
                            <?=Html::a(Yii::t('app', '自定义公式'), ['crud/create','type' => 2], ['class' => 'fa fa-sliders'])?>
                        </li>

                    </ul>
                </div>
		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">
						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <?= Html::hiddenInput('protocol_id', isset($protocol_id) ? $protocol : '')?>

						<?= $form->field($model, 'name')->textInput() ?>

                        <?= $form->field($model, 'formula')->textInput(['data-type' => 'select2', 'maxlength' => 255, 'id' => '_tags', 'placeholder' => "点击更换点位", 'readonly' => 'readonly']) ?>

                        <!--在系统中选择 点位单位-->
                        <?=$form->field($model, 'unit')->dropDownList(
                            $unit,
//                            ArrayHelper::map(Unit::getAllUnit(), 'id', 'unit'),
                            ['class'=>'select2', 'style' => 'width:200px']
                        )?>

                        <HR />

                        <?= $form->field($model, 'filter_max_value')->textInput()?>

                        <?= $form->field($model, 'filter_min_value')->textInput()?>

                        <lable><?=Yii::t('app', 'Record Interval')?> :</lable>
                        <a href="#"  id="interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                        <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框//注意-->
                        <?= Html::hiddenInput('CalculatePoint[interval]', $model->interval ? $model->interval : 'Empty', ['id' => 'interval_time'])?>
                        <hr />

                        <?= $form->field($model, 'is_shield')->checkbox() ?>

                        <?= $form->field($model, 'is_upload')->checkbox() ?>

                        <?= Html::hiddenInput('template_name', empty($model->template_id) ? '' : Template::getNameById($model->template_id))?>

                        <?= $form->field($model, 'template_id')->hiddenInput()->label('')?>


                            <HR />
                            <!--提示-->
                            <p class="text-danger slideInRight fast animated">
                                <span class="label label-warning">
				                 提示
                                </span> &nbsp; 公式模板中替换的所有点位选项,都需要满足<span style = 'color:#F00'>选择点位的单位必须包含在单位管理列表中</span>,请确保需要替换点位的单位存在单位管理列表中.
                                                <span style = 'color:#330066'>如果不存在</span>可以到 <a href = '<?= Url::toRoute('/unit/crud/create');?>' target = '_blank'>单位管理</a> 中添加.
                            </p>

                        </fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/my_js/select_chose.js", ['backend\assets\AppAsset']);
?>
<script>
    window.onload = function (){
        var $interval = $('#interval_time').val();
        /* x-editable 生成时间选择插件 */
        timeOfEditable( $("#interval") );
        $('#interval').text($interval);
    }
</script>