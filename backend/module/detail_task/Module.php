<?php

namespace backend\module\detail_task;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\detail_task\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
