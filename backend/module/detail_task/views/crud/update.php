<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DetailTask $model
 */

$this->title = Yii::t('task', 'Update {modelClass}: ', [
  'modelClass' => 'Detail Task',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>
