<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\DetailTask $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('task', 'Detail Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('task', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('task', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('task', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'id',
            'detail',
            'time',
            'desc',
        ],
    ]) ?>

</div>
