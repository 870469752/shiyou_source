<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DetailTask $model
 */
$this->title = Yii::t('task', 'Create {modelClass}', [
  'modelClass' => 'Detail Task',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
