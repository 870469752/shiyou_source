<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\DetailTask $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                            <!--开始 日历插件-->
                            <div class="jarviswidget jarviswidget-color-magenta"
                                 data-widget-deletebutton="false"
                                 data-widget-editbutton="false"
                                 data-widget-colorbutton="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                                    <h2><?= Yii::t('task', 'task')?></h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <div class="widget-body no-padding">
                                        <!-- content goes here -->
                                        <div class="widget-body-toolbar">
                                            <input type="text" style = "width:110px;display:none" id ="timepick" name="mydate" readonly placeholder="Select a date" class="form_datetime"/>
                                            <div id="calendar-buttons">
                                                <!-- add: non-hidden - to disable auto hide -->
                                                <div class="btn-group" id = 'select_btn'>
                                                    <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                                        Showing <i class="fa fa-caret-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu js-status-update pull-right">
                                                        <li>
                                                            <a href="javascript:void(0);" id="mt">Month</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" id="ag">Week</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" id="td">Day</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="btn-group">
                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                       id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                       id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="calendar"></div>
                                        <!-- end content -->
                                    </div>
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!--结束  日历插件-->
                            <?= Html::hiddenInput("detail"); ?>
						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('task', 'Create') : Yii::t('task', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/moment.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/qtip/jquery.qtip-1.0.0-rc3.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/bootbox/bootbox.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/my_js/calendar_task.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset'])
?>
<script>
    window.onload = function(){

//        var up_type = 'year';
        var _data = '';
        /*初始化日历事件  获取所有event 并展示到日历上*/
        <?php if(!$model->isNewRecord): ?>
        var info = <?=isset($data)?$data:[];?>;
        _data = '['
        $.each(info, function (key, value) {

            var current_day = new Date();
            _data += '{';
            _data += "id:"+ value.id +",";
            _data += "start:$.fullCalendar.parseDate('" + eval('('+ value.time + ')').start + "') ,";
            _data += "end:$.fullCalendar.parseDate('" + eval('('+ value.time + ')').end + "')  ,";
            _data += "title: '" + value.desc + "',";
            _data += 'className:' + "['event', 'bg-color-darken'],";
            if(current_day.getHours() || current_day.getMinutes()){
                _data += 'allDay: false';
            }
            _data += '},';
        })
        _data = _data.substring(0, _data.length - 1);
        _data += ']';
//        console.log(_data);
        $('#calendar').fullCalendar('addEventSource', eval("(" + _data + ")"));
        <?php endif;?>
        /*                                                                                      结束*/


        /*添加自定义按钮 点击事件*/
        $('#calendar-buttons #btn-prev').click(function () {
            $('#calendar').fullCalendar('prev');
            return false;
        });
        $('#calendar-buttons #btn-next').click(function () {
            $('#calendar').fullCalendar('next');
            return false;
        });
        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-button-today').click();
            return false;
        });

        /* 隐藏 日历切换按钮 */
        function hide_buttons() {
            $('.fc-header-right, .fc-header-center').hide();
            $('#calendar-buttons #btn-prev').hide();
            $('#calendar-buttons #btn-next').hide();
            $('#timepick').hide();
        }

        /*点击切换视图*/
        $('#mt').click(function () {
            timeTypeChange('month');
            $('#popover').hide();
            $('#calendar').fullCalendar('changeView', 'month');
        });
        $('#ag').click(function () {
            timeTypeChange('day');
            $('#popover').hide();
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });
        $('#td').click(function () {
            timeTypeChange('day');
            $('#popover').hide();
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });

        /* 显示 日历切换按钮 */
        function show_buttons() {
            $('.fc-header-right, .fc-header-center').show();
            $('#calendar-buttons #btn-prev').show();
            $('#calendar-buttons #btn-next').show();
            $('#timepick').show();
        }

        $('header').click(function () {
            $('#popover').hide();
        })

        function timeTypeChange(time_type)
        {
            $(".form_datetime").datetimepicker('remove');
            $(".form_datetime").val('');
            if(time_type == 'month')
            {
                $(".form_datetime").datetimepicker({
                    format: "yyyy-mm",
                    startView: 'year',
                    minView: 'year',
                    todayBtn:'linked',
                    autoclose: true
                });
                $(".form_datetime").datetimepicker('setStartDate', '2015-01');
            }
            else
            {
                $(".form_datetime").datetimepicker({
                    format: "yyyy-mm-dd",
                    startView: 'month',
                    minView: 'month',
                    todayBtn:'linked',
                    autoclose: true
                });
                $(".form_datetime").datetimepicker('setStartDate', '2014-01-01');
            }
        }

        /*时间选择框中的 值 变化时 对应的 fullcalendar 也发生变化*/
        $('.form_datetime').datetimepicker().on('changeDate', function(ev){
            var time_data = new Date($(".form_datetime").val());
            $("#calendar").fullCalendar('gotoDate', time_data.getFullYear(),time_data.getMonth(),time_data.getDate());
        });

        $('.jarviswidget-ctrls').hide();

    };
</script>