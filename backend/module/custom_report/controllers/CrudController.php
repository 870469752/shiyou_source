<?php

namespace backend\module\custom_report\controllers;


use backend\models\AlarmEvent;
use backend\models\AlarmLevel;
use backend\models\PointData;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\BaseArrayHelper;
use Yii;

use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;

use backend\models\CustomReport;
use backend\models\search\CustomReportSearch;
use backend\models\Point;
use backend\models\Unit;
use backend\models\search\PointDataSearch;

use yii\web\User;

/**
 * CrudController implements the CRUD actions for CustomReport model.
 */
class CrudController extends MyController//crud控制器集成楼my controller控制器
{
    public function behaviors()//行为的描述方式，暂时不懂？
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomReport models.
     * @return mixed
     */
    public function actionIndex()//Index方法，首页的方法
    {
        $searchModel = new CustomReportSearch;//searchModel变量new出一个CustomReportSearch的对象？
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());//通过getQueryParams方法来获取一个数据

        //此处为将dataProvider打印出来的结果
//        yii\data\ActiveDataProvider Object
//    (
//        [query] => yii\db\ActiveQuery Object
//    (
//        [sql] =>
//            [on] =>
//            [joinWith] =>
//            [select] =>
//            [selectOption] =>
//            [distinct] =>
//            [from] =>
//            [groupBy] =>
//            [join] =>
//            [having] =>
//            [union] =>
//            [params] => Array
//    (
//    )
//
//    [_events:yii\base\Component:private] => Array
//    (
//    )
//
//    [_behaviors:yii\base\Component:private] =>
//            [where] =>
//            [limit] =>
//            [offset] =>
//            [orderBy] =>
//            [indexBy] =>
//            [modelClass] => backend\models\CustomReport
//            [with] =>
//            [asArray] =>
//            [multiple] =>
//            [primaryModel] =>
//            [link] =>
//            [via] =>
//            [inverseOf] =>
//        )
//
//    [key] =>
//    [db] =>
//    [id] =>
//    [_sort:yii\data\BaseDataProvider:private] =>
//    [_pagination:yii\data\BaseDataProvider:private] => yii\data\Pagination Object
//    (
//        [pageParam] => page
//            [pageSizeParam] => per-page
//            [forcePageParam] => 1
//            [route] =>
//            [params] =>
//            [urlManager] =>
//            [validatePage] => 1
//            [totalCount] => 0
//            [defaultPageSize] => 20
//            [pageSizeLimit] => Array
//    (
//        [0] => 1
//                    [1] => 50
//                )
//
//            [_pageSize:yii\data\Pagination:private] => 10
//            [_page:yii\data\Pagination:private] =>
//        )
//
//    [_keys:yii\data\BaseDataProvider:private] =>
//    [_models:yii\data\BaseDataProvider:private] =>
//    [_totalCount:yii\data\BaseDataProvider:private] =>
//    [_events:yii\base\Component:private] => Array
//    (
//    )
//
//    [_behaviors:yii\base\Component:private] =>
//)

        return $this->render('index', [//向界面中输入我们需要的数据
           'dataProvider' => $dataProvider,


            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single CustomReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomReport;

        //这句话的意思是通过post来接受传递来的数据，如果接受成功，在表中保存
        if ($model->load(Yii::$app->request->post(), '') && $model->customReportSave()) {
            //跳转到index页面
            return $this->redirect(['index']);
        } else {
            //如果没有post的话
            $point_info = MyFunc::_map(Point::getAvailablePoint(), 'id', 'name');
            return $this->render('create',[
                'point_info' => $point_info,
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing CustomReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post(), '') && $model->customReportSave()) {
            return $this->redirect(['index']);
        } else {
            $point_info = MyFunc::_map(Point::getAvailablePoint(), 'id', 'name');
            $model->name = MyFunc::DisposeJSON($model->name);
            $model->point_info = MyFunc::DisposeJSON($model->point_info);
            $model->right_point = isset($model->point_info['right_point']) && !empty($model->point_info['right_point']) ? implode(',', $model->point_info['right_point']) : '';
            $model->left_point = isset($model->point_info['left_point']) && !empty($model->point_info['left_point']) ? implode(',', $model->point_info['left_point']) : '';
            //处理 开始时间范围
            $model->preTimeRange();
            // 时间 范围处理
            return $this->render('update', [
                    'point_info' => $point_info,
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$timestamp)
    {
        //写出删除语句
//        $rows=(new \yii\db\Query())
//        ->select(['id'])
//        ->from('custom_report')
//        ->where(['id'=> '$id'])
//        ->delete();
   // $sql=delete from core.custom_report where id=$id;
    // $model=Customreport::findBysql($rows)->delete();
        //执行query语句
//       $this->findBysql($rows)->delete();
      //$this->findModel($id)->delete();
        //在执行findone方式的时候，id与时间戳，错位，导致了，无法争创删除的问题。
        $aaa=CustomReport::deleteAll("id=".$id." and timestamp='".$timestamp."'");
        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /***********************************自定义报表 展示 ************************************/

    /**
     * 报表展示
     * @return string
     */
    public function actionReportTable()
    {
        $param = Yii::$app->request->get();
        $search_data = $this->getTableData($param);
        $param['start_time'] = $search_data['searchModel']->start_time;
        $param['end_time'] = $search_data['searchModel']->end_time;
        return $this->render('table_view', [
                'col' => $search_data['searchModel']->createColumn(),
                'dataProvider' => $search_data['dataProvider'],
                'searchModel' => $search_data['searchModel'],
                'param' => $param,
                'report_name' => $search_data['custom_report_data']['report_name'],
            ]);
    }

    /**
     * 中间数据获取
     * @param $query_param
     * @return array|\yii\web\Response
     *
     */
    public function getTableData($query_param)
    {
        if($custom_report_data = CustomReport::getCustomPointData($query_param['id'])){
            //自定义报表时间
            $start_time = self::timeProcess($custom_report_data['params'], 'start_time');
            $end_time = self::timeProcess($custom_report_data['params'], 'end_time');
            $query_param['point_id'] = $custom_report_data['point_ids'];
            //如果参数中不含有开始结束时间就将报表中设置的时间作为开始结束时间
            $query_param['end_time'] = isset($query_param['end_time']) ? $query_param['end_time'] : $end_time;
            $query_param['start_time'] = isset($query_param['start_time']) ? $query_param['start_time'] : $start_time;
            $searchModel = new PointDataSearch();
            $dataProvider = $searchModel->search($query_param);
            return ['dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'custom_report_data' => $custom_report_data,
                'query_param' => $query_param];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * 导出
     */
    public function actionExport()
    {
        $param = Yii::$app->request->get();
        $search_data = $this->getTableData($param);
        $col = BaseArrayHelper::map($search_data['searchModel']->createColumn(), 'attribute', 'label');
        $data_info = $search_data['dataProvider']->query->asArray()->all();
        foreach($data_info as $k1 => $v1) {
            foreach($v1 as $k2 => $v2) {
                if($k2 == 'time') {
                    $data_info[$k1][$k2] = date('Y-m-d H:i:s',strtotime($v2));
                }else {
                    $data_info[$k1][$k2] = round($v2 ,2);
                }
            }
            break;
        }

        $report_name = $search_data['custom_report_data']['report_name'].'(' .$search_data['query_param']['start_time'] .'——' .$search_data['query_param']['end_time'] .')';

        ini_set('memory_limit','2048M');

        MyExport::run($data_info,[$col], ['save_file' => ['file_name' => $report_name]]);
    }

    public function actionReportChart($id)
    {
        //1获取点位信息，点位信息中应该包括（点位的名称，点位在几个时间戳的数值，以及点位的点位）
        //将点位区分出左轴跟右轴，并切赋予不同的单位，
//        echo '<pre>';
//        print_r($id);
//        die;
        //$data=CustomReport::find()->where(['id'=>$id])->asArray()->all();
        //$newdata=(json_decode(($data['0']['time_range']),1));
        //$nedata=$newdata['data']['start']['timestamp'];






        //通过sql语句来改变这个问题

        //$this->point_info = MyFunc::createJson(['left_point' => $this->left_point,
           // 'right_point' => $this->right_point], 'report_point');



       //加入开始的时间

        $report_info = CustomReport::getCustomPointInfo($id);

//需要的元素，单位，点位名称，时间戳制定为止的数据，以及
        if(!$report_info) { return $this->redirect(['index']);}
        $left_unit = '';
        $right_unit = '';
        //左轴 最大点位值的unit 和 所有的unit
        if(!empty($report_info['point_id']['left'])) {
            $point_unit = unit::getByPointId($report_info['point_id']['left']);
            $left_unit = implode(',', $point_unit);
        }
        //左轴 最大点位值的unit 和 所有的unit
        if(!empty($report_info['point_id']['right'])) {
        $point_unit = unit::getByPointId($report_info['point_id']['right']);
        $right_unit = implode(',', $point_unit);


    }
        //上述方法无法得到点位的单位，这是关键性问题，问题在于取得那一张表，并且获得该表中的单位
        //要获得左值单位，右值单位，并切，获得时间戳内的数值，这都需要在数据表中查找得到，具体操作方法，需要参看已经的代码类2037
        //所有点位的信息
        $point = Point::getPointUnitById(array_merge($report_info['point_id']['left'], $report_info['point_id']['right']));

        //处理需要的信息
        foreach($point as $key => $value) {
            $point_info[$key]['id'] = $value['id']; //id
            $point_info[$key]['unit'] = isset($value['unit']['name']) ? $value['unit']['name'] : ''; //unit
            $point_info[$key]['type'] = in_array($value['id'], $report_info['point_id']['left']) !== false ? 'left' : 'right';
        }


        return $this->render('chart_view.php', [

                'point_info' => $point_info,
                'left_unit' => $left_unit,
                'right_unit' => $right_unit,
                'report_name' => $report_info['report_name']

            ]);

    }

    public function actionAjaxChartData()
    {
        $param = Yii::$app->request->get();
        $param['id'] = isset($param['id']) && !empty($param['id']) ? $param['id'] : die('error_1');
        $param['type'] = isset($param['type']) && !empty($param['type']) ? $param['type'] : die('error_2');
        $param['report_info'] = CustomReport::getCustomPointInfo($param['id']);
        //自定义报表时间,需要2个参数，验证表单中是否有开始时间，以及结束时间
        $start_time = self::timeProcess($param['report_info']['params'], 'start_time');

        $end_time = self::timeProcess($param['report_info']['params'], 'end_time');
       //并且将其格式化
        $param['start_time'] = strtotime($start_time) * 1000;
        $param['end_time'] = strtotime($end_time) * 1000;
//        echo '<pre>';
//        print_r($param['end_time']);
//        die;
        switch($param['type']) {
            case 'all':
                $param['point_id'] = array_merge($param['report_info']['point_id']['left'], $param['report_info']['point_id']['right']);
                break;
            case 'left':
                $param['point_id'] = $param['report_info']['point_id']['left'];
                break;
            case 'right':
                $param['point_id'] = $param['report_info']['point_id']['right'];
                break;
        }

        if(!isset($param['end_time']) || !isset($param['start_time'])) {
            $max_time_range = PointData::getDataTimeRangeByPointId($param['point_id']);
            $param['end_time'] = strtotime($max_time_range['max_time']) * 1000;
            $param['start_time'] = strtotime($max_time_range['min_time']) * 1000;
        }

        $param['start_time'] = $param['start_time'] / 1000;
        $param['end_time'] = $param['end_time'] / 1000;

        return self::getDataByTimeRange($param);
    }

    public function actionAjaxChartDataNew()
    {
        //直接从前台获取传递过来的参数
        $param = Yii::$app->request->get();
//        echo "<pre>";
//        print_r($param);
//        die;
        //print_r($param);die;
        //验证传递过来的参数,都包括id ，type，report_info
        $param['id'] = isset($param['id']) && !empty($param['id']) ? $param['id'] : die('error_1');
        $param['type'] = isset($param['type']) && !empty($param['type']) ? $param['type'] : die('error_2');
        //通过param的id获取信息
        $param['report_info'] = CustomReport::getCustomPointInfo($param['id']);
        //自定义报表时间,需要2个参数，验证表单中是否有开始时间，以及结束时间
        $start_time = $param['start_time'].'00:00:00';
        //开始时间时间设置为

        $end_time = $param['end_time'].'23:59:59';

        //结束时间设置
        //并且将其格式化
        //格式化开始时间，与结束时间格式化成时间戳的形式
        $param['start_time'] = strtotime($start_time) * 1000;


        $param['end_time'] = strtotime($end_time) * 1000;
//        echo '<pre>';
//        print_r($param['end_time']);
//        die;
        //通过param['type']判定，并且将点位信息，组合起来
        switch($param['type']) {
            case 'all':
                $param['point_id'] = array_merge($param['report_info']['point_id']['left'], $param['report_info']['point_id']['right']);
                break;
            case 'left':
                $param['point_id'] = $param['report_info']['point_id']['left'];
                break;
            case 'right':
                $param['point_id'] = $param['report_info']['point_id']['right'];
                break;
        }
       //从表中customreport中查找时间的范围，不需要用
        if(!isset($param['end_time']) || !isset($param['start_time'])) {
            $max_time_range = PointData::getDataTimeRangeByPointId($param['point_id']);
//            echo '<pre>';
//            print_r($max_time_range);
//            die;
            $param['end_time'] = strtotime($max_time_range['max_time']) * 1000;
//            echo '<pre>';
//            print_r($param['end_time']);
//            die;
            $param['start_time'] = strtotime($max_time_range['min_time']) * 1000;
        }

        $param['start_time'] = $param['start_time'] / 1000;
//        echo '<pre>';
//        print_r($param['start_time']);
//        die;
        $param['end_time'] = $param['end_time'] / 1000;

        return self::getDataByTimeRange($param);

    }
    //获取时间范围
    public static function getDataByTimeRange($param)
    {

        $chart_color = Yii::$app->params['chart_color_2'];

        $range = $param['end_time'] - $param['start_time'];
//        echo '<pre>';
//        print_r($param);
//        die;

        if($range < 7 * 24 * 3600) {
//            echo '<pre>';
//            print_r($range);
//            die;
            //如果数据查询的时间是一个星期内则不做统计
            $param['report_type'] = '';
        }else if($range < 90 * 24 * 3600) {
            //如果数据查询的时间是三个月内 统计按照 日
            $param['report_type'] = 'day';
        }else {
            //如果数据查询大于以上两种时间范围那么按 月统计
            $param['report_type'] = 'month';
        }
       $chart_data = [];
        $table_data['thead'][0] = '时间';
        //遍历所有点位进行数据查询 $point_id, $start_time, $end_time, $group, $method
        foreach($param['point_id'] as $key => $value) {
            $point_info = Point::getPointUnitById($value);
            $chart_data[$key]['color'] = isset($chart_color[$key]) ? $chart_color[$key] : '';
            $chart_data[$key]['name'] = isset($point_info[0]['name']) && !empty($point_info[0]['name']) ? MyFunc::DisposeJSON($point_info[0]['name']) : 'P_' .$point_info[0]['point_id'];
            $chart_data[$key]['tooltip']['valueSuffix'] = isset($point_info[0]['unit']['name']) ? ' ' .$point_info[0]['unit']['name'] : '';
            $table_data['thead'][$value] = $chart_data[$key]['name'].'(' .$chart_data[$key]['tooltip']['valueSuffix'] .')';
            switch($point_info[0]['value_type']) {
                case 0:
                    $method = 'avg';
                    break;
                case 1:
                case 2:
                    $method = 'max';
                    break;
                default:
                    $method = 'max';
            }
            $point_data = Point::getPointDataByType($value, $param['start_time'], $param['end_time'], $param['report_type'], $method);


            foreach($point_data[0]['pointData'] as $data){
                $current_time = MyFunc::TimeFormat($data['_timestamp']);
                $chart_data[$key]['data'][] = [strtotime($current_time) * 1000, floatval($data['value'])];
//                $table_data['tbody'][$current_time]['timestamp'] = MyFunc::TimeFormat($data['_timestamp']);
                $table_data['tbody'][$current_time][$value] = floatval($data['value']);
            }
            if(isset($param['report_info']['point_id']['right']) && in_array($value, $param['report_info']['point_id']['right'])) {
                $chart_data[$key]['yAxis'] = 1;
            }else {
                $chart_data[$key]['yAxis'] = 0;
            }
        }
        $body = [];
        //由于数据不可避免的存在缺失 所以需要重新将数据进行整理
        if(isset($table_data['tbody']) && !empty($table_data['tbody'])) {
            foreach($table_data['tbody'] as $tbody_key => $tbody_value) {
                $body[$tbody_key][0] =  $tbody_key;
                foreach($param['point_id'] as $point_id){
                    $body[$tbody_key][$point_id] = isset($tbody_value[$point_id]) ? $tbody_value[$point_id] : null;
                }
            }
            //对数据进行排序
            sort($body);
        }
        //将table中的数据 替换为处理过的数据
        $table_data['tbody'] = array_values($body);
        //添加数据计数 为html端 的分页做准备 ，在js端处理也可以
        $table_data['total'] = count($body);
//        echo '<pre>';
//        print_r($table_data);
//        die;
        return json_encode(['chart' => array_values($chart_data),
                            'table' => $table_data,
                            'param' => $param           //将有用的参数重新传递会前台，添加到导出链接中
                            ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 自定义统计表
     * @return string
     */
    public function actionStatisticData()
    {
        $param = Yii::$app->request->getQueryParams();
        $param = self::dealParams($param);
        $search_data = $this->getTableData($param);
        $param['start_time'] = $search_data['searchModel']->start_time;
        $param['end_time'] = $search_data['searchModel']->end_time;

        return $this->render(
            'table_statistic_view',
            [
                'col' => $search_data['searchModel']->createColumn(),
                'dataProvider' => $search_data['dataProvider'],
                'searchModel' => $search_data['searchModel'],
                'param' => $param,
                'report_name' => $search_data['custom_report_data']['report_name'],
            ]
        );
    }

    /**
     * 自定义统计报表导出
     */
    public function actionStatisticExport()
    {
        $params = Yii::$app->request->getQueryParams();
        $params = self::dealParams($params);
        $search_data = $this->getTableData($params);
        $point_data = $search_data['dataProvider']->query->asArray()->all();
        $col =  $search_data['searchModel']->createColumn();
        //处理 表头字段名
        $export_col = [];
        foreach($col as $key => $value) {
            $export_col[] = $value['label'];
        }

        set_time_limit(0);
        ini_set('memory_limit','2048M');

        MyExport::run($point_data,[$export_col], ['save_file' => ['file_name' => $search_data['custom_report_data']['report_name']]]);

    }

    /**
     * 自定义报表 数据统计的参数统一处理
     * @param $param
     * @return mixed
     */
    public static function dealParams($param)
    {
        $param['date_time'] = isset($param['date_time']) && !empty($param['date_time']) ? $param['date_time'] : Date('Y-m-d');
        $param['report_type'] = isset($param['time_type']) && !empty($param['time_type']) ? $param['time_type'] : 'day';
        $param['fetch_value_method'] = isset($param['statistic_type']) ? $param['statistic_type'] : 'sum';
        //在此判断时间类型进行对选择的时间进行格式化
        $time_range = MyFunc::TimeFloor($param['date_time'], $param['report_type']);
        //将格式化后的时间插入参数中
        $param['start_time'] = $time_range['start'];
        $param['end_time'] = $time_range['end'];
        //该统计是default的
        $param['group_default'] = true;
        return $param;
    }

    /**
     * 点位查看中的时间分为两种：absolute relative
     * 相对时间就是以当前时间为参照，对时间的划分
     * 绝对时间，固定的一个时间日期
     * @param $params   时间参数
     * @param $time_name  时间名称
     * @param $time_type  时间类型
     * @param $custom_num  自定义时间系数
     * @param $custom_type 自定义时间类型
     * @return string    返回一个处理好的时间字符串
     */
    public static function timeProcess($params, $time_name = 'start_time')
    {
        //一个时间范围开始的节奏
        /* 对参数进行 验证*/
        //
        $time_value = $time_name == 'start_time' ?
            (isset($params[$time_name]) && !empty($params[$time_name]) ? $params[$time_name] : (date('Y-m-d')). ' 00:00') :
            (isset($params[$time_name]) && !empty($params[$time_name]) ? $params[$time_name] : (date('Y-m-d')). ' 23:59');
        $time_type = isset($params[$time_name .'_type']) && !empty($params[$time_name .'_type']) ? $params[$time_name .'_type'] : 'absolute';
        $custom_time_type = isset($params['custom_' .$time_name .'_type']) && !empty($params['custom_' .$time_name .'_type']) ? $params['custom_' .$time_name .'_type'] : 0;
        $custom_time_num = isset($params['custom_' .$time_name .'_num']) && !empty($params['custom_' .$time_name .'_num']) ? $params['custom_' .$time_name .'_num'] : '';
        //先处理 开始时间
        //如果时间类型为据对的
        if($time_type == 'absolute') {
            //绝对时间就是直接赋值
            
            $time = !empty($time_value) ? $time_value : '';
        }else {
            //绝对时间 还需要判断是否是自定义
            //如果是时间值是custom自定义
            if($time_value != 'custom') {
                //如果不是将其拆分为系数和时间类型
                $time_info = explode(',', $time_value);
                $custom_time_type = isset($time_info[1]) && !empty($time_info[1]) ? $time_info[1] : '';
                $custom_time_num = isset($time_info[0]) && !empty($time_info[0]) ? $time_info[0] : 0;
            }
            //如果是自定义 需要将自定义的系数 和时间类型进行拼接

            $time = $time_name == 'start_time' ? self::getStartTime($custom_time_type, $custom_time_num) : self::getEndTime($custom_time_type, $custom_time_num);
        }

        return $time;
    }

    /**
     * 对 自定义相对开始时间的处理
     * @param $time_type
     * @param $num
     * @return string
     */
    public static function getStartTime($time_type, $num)
    {
        switch($time_type) {
            case 'day':
            case 'week':
                //将开始时间解析为时间戳，
                $start_time = date('Y-m-d', strtotime('-'.$num.' day')) .' 00:00';
                break ;
            case 'month':
                $start_time = date('Y-m', strtotime('-'.$num.' month')) .'-01 00:00';
                break;
            case 'year':
                $start_time = date('Y', strtotime('-'.$num.' year')) .'-01-01 00:00';
                break;
            default:
                $start_time = date('Y-m-d') .' 00:00';
        }

        return $start_time;
    }

    /**
     * 对自定义结束时间的处理
     * @param $time_type
     * @param $time_num
     * @return string
     */
    public static function getEndTime($time_type, $time_num)
    {
        switch($time_type) {
            case 'day':
            case 'week':
                $end_time = date('Y-m-d', strtotime('-'.$time_num.' day')) .' 23:59';
                break;
            case 'month':
                $y_m = date('Y-m', strtotime('-'.$time_num.' month'));
                $end_time = $y_m.'-'.date('t', strtotime($y_m)) .' 23:59';
                break;
            case 'year':
                $end_time = date('Y', strtotime('-'.$time_num.' year')) .'-12-31 23:59';
                break;
            default:
                $end_time = date('Y-m-d') .' 23:59';
        }
        return $end_time;
    }
//    public function actionShuZu(){
//            $array=[
//            'title'=>[
//                         "text"=>"右轴点位"
//                          ],
//             "labels"=>"",
//           'opposite'=>'true'
//           ];
//        $array=json_encode($array);
//       return $this->render('chart_view',[
//           'array'=>$array
//
//        ]);
//    }

}
