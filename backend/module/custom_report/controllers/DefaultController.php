<?php

namespace backend\module\custom_report\controllers;

use common\library\MyFunc;
use Yii;
use backend\controllers\MyController;
use backend\models\CustomReport;
use backend\models\search\CustomReportSearch;
use backend\models\Point;
use backend\models\search\PointDataSearch;
use common\library\MyExport;
use yii\helpers\BaseArrayHelper;
use backend\models\Unit;
use backend\models\form\PointDataForm;

class DefaultController extends MyController
{

    public function actionCustomTable()
    {
        $searchModel = new CustomReportSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('custom_table', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
    }

    /**
     * 报表展示
     * @return string
     */
    public function actionReportTable()
    {
        $param = Yii::$app->request->get();
        $param['end_time'] = isset($param['end_time']) ? $param['end_time'] : date('Y-m-d');
        $param['start_time'] = isset($param['start_time']) ? $param['start_time'] : date("Y-m-d",strtotime("last month",strtotime($param['end_time'])));
        $search_data = $this->getTableData($param);
        $param['start_time'] = $search_data['searchModel']->start_time;
        $param['end_time'] = $search_data['searchModel']->end_time;
        return $this->render('table_view', [
                'col' => $search_data['searchModel']->createColumn(),
                'dataProvider' => $search_data['dataProvider'],
                'searchModel' => $search_data['searchModel'],
                'param' => $param,
                'report_name' => $search_data['custom_report_data']['report_name'],
            ]);
    }

    /**
     * 中间数据获取
     * @param $query_param
     * @return array|\yii\web\Response
     */
    public function getTableData($query_param)
    {
        if($custom_report_data = CustomReport::getCustomPointData($query_param['id'])){
            $query_param['point_id'] = $custom_report_data['point_ids'];
            $searchModel = new PointDataSearch();
            $dataProvider = $searchModel->search($query_param);
            return ['dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'custom_report_data' => $custom_report_data];
        }else{
            return $this->redirect(['custom-table']);
        }
    }

    /**
     * 导出
     */
    public function actionExport()
    {
        $param = Yii::$app->request->get();
        $param['end_time'] = isset($param['end_time']) ? $param['end_time'] : date('Y-m-d');
        $param['start_time'] = isset($param['start_time']) ? $param['start_time'] : date("Y-m-d",strtotime("last month",strtotime($param['end_time'])));
        $search_data = $this->getTableData($param);

        $col = BaseArrayHelper::map($search_data['searchModel']->createColumn(), 'attribute', 'label');
        $data_info = $search_data['dataProvider']->query->asArray()->all();
        foreach($data_info as $k1 => $v1) {
            foreach($v1 as $k2 => $v2) {
                if($k2 == 'time') {
                    $data_info[$k1][$k2] = date('Y-m-d H:i:s',strtotime($v2));
                }else {
                    $data_info[$k1][$k2] = round($v2 ,2);
                }
            }
        }

        $report_name = $search_data['custom_report_data']['report_name'].'(' .$param['start_time'] .'——' .$param['end_time'] .')';

        MyExport::run($data_info,[$col], ['save_file' => ['file_name' => $report_name]]);
    }

    public function actionReportChart($id)
    {
        $report_info = CustomReport::getCustomPointInfo($id);
        $left_unit = '';
        $right_unit = '';
        //左轴 最大点位值的unit 和 所有的unit
        if(!empty($report_info['point_id']['left'])) {
            $point_unit = unit::getByPointId($report_info['point_id']['left']);
            $left_unit = implode(',', $point_unit);
        }
        //左轴 最大点位值的unit 和 所有的unit
        if(!empty($report_info['point_id']['right'])) {
            $point_unit = unit::getByPointId($report_info['point_id']['right']);
            $right_unit = implode(',', $point_unit);
        }
        //所有点位的信息
        $point = Point::getPointUnitById(array_merge($report_info['point_id']['left'], $report_info['point_id']['right']));
        //处理需要的信息
        foreach($point as $key => $value) {
            $point_info[$key]['id'] = $value['id']; //id
            $point_info[$key]['unit'] = isset($value['unit']['name']) ? $value['unit']['name'] : ''; //unit
            $point_info[$key]['type'] = in_array($value['id'], $report_info['point_id']['left']) !== false ? 'left' : 'right';
        }
        return $this->render('chart_view', [
                'point_info' => $point_info,
                'left_unit' => $left_unit,
                'right_unit' => $right_unit
            ]);
    }

    public function actionAjaxChartData()
    {
        $param = Yii::$app->request->get();
        $param['id'] = isset($param['id']) && !empty($param['id']) ? $param['id'] : die('error_1');
        $param['type'] = isset($param['type']) && !empty($param['type']) ? $param['type'] : die('error_2');
        $param['end_time'] = isset($param['end_time']) ? $param['end_time'] : strtotime(date('Y-m-d')) * 1000;
        $param['start_time'] = isset($param['start_time']) && !empty($param['start_time']) ? $param['start_time'] : $param['end_time'] - 30*24*3600*1000;
        $param['report_info'] = CustomReport::getCustomPointInfo($param['id']);
        switch($param['type']) {
            case 'all':
                $param['point_id'] = array_merge($param['report_info']['point_id']['left'], $param['report_info']['point_id']['right']);
                break;
            case 'left':
                $param['point_id'] = $param['report_info']['point_id']['left'];
                break;
            case 'right':
                $param['point_id'] = $param['report_info']['point_id']['right'];
                break;
        }
        $param['start_time'] = $param['start_time'] / 1000;
        $param['end_time'] = $param['end_time'] / 1000;
        $this->getDataByTimeRange($param);
    }

    public function getDataByTimeRange($param)
    {
        $range = $param['end_time'] - $param['start_time'];
        if($range < 7 * 24 * 3600) {
            //如果数据查询的时间是一个星期内则不做统计
            $param['report_type'] = '';
        }else if($range < 90 * 24 * 3600) {
            //如果数据查询的时间是三个月内 统计按照 日
            $param['report_type'] = 'day';
        }else {
            //如果数据查询大于以上两种时间范围那么按 月统计
            $param['report_type'] = 'month';
        }
        $chart_data = [];
        //遍历所有点位进行数据查询 $point_id, $start_time, $end_time, $group, $method
        foreach($param['point_id'] as $key => $value) {
            $point_info = Point::getPointUnitById($value);
            switch($point_info[0]['value_type']) {
                case 0:
                    $method = 'avg';
                    break;
                case 1:
                case 2:
                    $method = 'max';
                    break;
                default:
                    $method = 'max';
            }
            $point_data = Point::getPointDataByType($value, $param['start_time'], $param['end_time'], $param['report_type'], $method);
            foreach($point_data[0]['pointData'] as $data){
                $chart_data[$key]['data'][] = [strtotime($data['_timestamp']) * 1000, floatval($data['value'])];
            }
            $chart_data[$key]['tooltip']['valueSuffix'] = isset($point_info['unit']['name']) ? $point_info['unit']['name'] : '';
            if(in_array($value, $param['report_info']['point_id']['left'])) {
                $chart_data[$key]['yAxis'] = 0;
            }else {
                $chart_data[$key]['yAxis'] = 1;
            }

        }
        echo json_encode(array_values($chart_data), JSON_UNESCAPED_UNICODE);
    }
}
