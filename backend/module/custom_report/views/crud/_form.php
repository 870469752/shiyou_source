<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\CustomReport $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<style>

    .control-label {
        text-align: right;
        margin-bottom: 0;
        padding-top: 7px;
        width:;
    }
    </style>
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin([
                                'options' => ['class' => 'form-horizontal'],
                            ]); ?>
						<fieldset>

                        <div class="form-group col-md-12">
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report', 'Name')?></label>
                            <div class="col-md-11">
                                <?= Html::textInput('name', isset($model->name) ? $model->name : null, ['class' => 'form-control', 'maxlength' => 255, 'placeholder' => Yii::t('custom_report', 'Name')]) ?>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report', 'Left Axis Point')?></label>
                            <div class="col-md-5">
                                <?=Html::dropDownList('left_point', null, $point_info,  ['id' => 'p_left', 'class' => 'select2 point_info', 'multiple' => 'multiple', 'placeholder' => Yii::t('custom_report', 'Left Axis Point')])?>
                            </div>
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report', 'Right Axis Point')?></label>
                            <div class="col-md-5">
                                <?=Html::dropDownList('right_point', null, $point_info,  ['id' => 'p_right', 'class' => 'select2 point_info', 'multiple' => 'multiple', 'placeholder' => Yii::t('custom_report', 'Right Axis Point')])?>
                            </div>
                        </div>

                        <!-- 开始时间 -->
                        <div class = "form-group col-md-12">
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report', 'Start time')?></label>
                            <div class = "col-md-3">
                                <?=Html::dropDownList('start_time_type', isset($time_type)?$time_type:null, ['' => '',
                                        'absolute' => Yii::t('app', 'Absolute Time'),
                                    ],
                                    ['class' => 'select2', 'placeholder' => Yii::t('app', 'Start Time Type'), 'title' => Yii::t('app', 'Start Time Type'),'id' => 'start_time_type'])?>
                            </div>

                            <!-- 开始时间 -->
                            <div id = '_start'></div>

                        </div>
                        <!-- 开始时间 -->

                        <!-- 结束时间 -->
                        <div class = "form-group col-md-12">
<!--                          2016。  修改的部分-->
<!--                            这是一个翻译问题，将search文件中的search文件进行搜寻，然后在model中定义出来，即可让所需加载的标签显示-->
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report','End time')?></label>
<!--                            修改的部分-->
                            <div class = "col-md-3">
                                <?=Html::dropDownList('end_time_type', isset($time_type)?$time_type:null, ['' => '',
                                        'absolute' => Yii::t('app', 'Absolute Time'),

                                    ],
                                    ['class' => 'select2', 'placeholder' => Yii::t('app', 'End Time Type'), 'title' => Yii::t('app', 'End Time Type'),'id' => 'end_time_type'])?>
                            </div>

                            <div id = '_end'></div>

                        </div>
                        <!-- 结束时间 -->
<!--                       在这里面加载一项，就是问题的单位，将单位存储进入表单的里面，-->


						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function() {
        // 左轴点位数据
        var $pl_selected = '<?=isset($model->left_point) ? $model->left_point : ''?>';
        // 右轴点位数据
        var $pr_selected = '<?=isset($model->right_point) ? $model->right_point : ''?>';
        // timerange 参数
        var $start_time_type = '<?=isset($model->start_time_type) ? $model->start_time_type : 'absolute'?>';
        var $end_time_type = '<?=isset($model->end_time_type) ? $model->end_time_type : 'absolute'?>';
        var $start_time_unit = '<?=isset($model->custom_start_time_type) ? $model->custom_start_time_type : ''?>';
        var $end_time_unit = '<?=isset($model->custom_end_time_type) ? $model->custom_end_time_type : ''?>';
        var $start_time_value = '<?=isset($model->custom_start_time_num) ? $model->custom_start_time_num : date('Y-m-d')?>';
        var $end_time_value = '<?=isset($model->custom_end_time_num) ? $model->custom_end_time_num : date('Y-m-d')?>';

        /*若用户选择相对时间中的自定义 动态添加有关自定义的 dom元素*/
        function appendCustom($name) {
            $('#' + $name).parent().after(
                '<div class="col-md-2">' +
                    '<div class = "input-group">' +
                    '<span class="input-group-addon"><?=Yii::t('app', 'Last')?></span>' +
                    '<input type="text" id= "custom_' + $name + '_num" class="form-control" name = "custom_' + $name + '_num" title="<?=Yii::t('app', 'Time number')?>" placeholder="<?=Yii::t('app', 'Time number')?>">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<select id = "custom_' + $name + '_type" class="select2" name = "custom_' + $name + '_type" title="<?=Yii::t('app', 'Choose custom time type')?>" placeholder="<?=Yii::t('app', 'Choose custom time type')?>">' +
                    '<option value=""></option>' +
                    '<option value="day">日</option>' +
                    '<option value = "week">周</optino>' +
                    '<option value="month">月</option>' +
                    '<option value="year">年</option>' +
                    '</select>' +
                    '</div>'
            );
            $('#custom_' + $name + '_type').select2();
        }

        var $relative_value = '';
        function addChangeEvent($name) {
            $('#' + $name).change(function (){
                $relative_value = $(this).val();
                if($relative_value == 'custom') {
                    appendCustom($name);
                } else {
                    $('#' + $name).parent().find('~ div').remove(); // or $('#' + $name).parent().nextAll().remove();
                }
            })
        }

        /* 根据用户选择的时间类型进行联动 动态添加所需要的参数表单dom */
        function appendTime($time_type, $name, $obj) {
            $obj.empty();
            if($time_type == 'absolute') {
                //添加开始时间的 时间选择器
                $obj.append(
                    '<div class = "col-md-3">' +
                        '<input type="text" id=' + $name + ' class="form-control" name = ' + $name + '  title = <?=Yii::t('app', 'Choose Time')?> placeholder = <?=Yii::t('app', 'Choose Time')?>>' +
                        '</div>'
                );
                timeTypeChange('day', $('#' + $name));
            }else {
                $obj.append(
                    '<div class = "col-md-3">' +
                        '<select id = ' + $name + ' class="select2" name=' + $name + ' title = <?=Yii::t('app', 'Choose Time')?> placeholder = <?=Yii::t('app', 'Choose Time')?>>' +
                        '<option></option>' +
                        '<option value="0,day"><?=Yii::t('app', 'Today')?></option>' +
                        '<option value="0,week"><?=Yii::t('app', 'This week')?></option>' +
                        '<option value="0,month"><?=Yii::t('app', 'This month')?></option>' +
                        '<option value="0,year"><?=Yii::t('app', 'This year')?></option>' +
                        '<option value="1,day"><?=Yii::t('app', 'Last day')?></option>' +
                        '<option value="2,day"><?=Yii::t('app', 'Last two days')?></option>' +
                        '<option value="1,week"><?=Yii::t('app', 'Last week')?></option>' +
                        '<option value="2,week"><?=Yii::t('app', 'Last two weeks')?></option>' +
                        '<option value="1,month"><?=Yii::t('app', 'Last month')?></option>' +
                        '<option value="custom"><?=Yii::t('app', 'Custom')?></option>' +
                        '</select>' +
                        '</div>'
                );
                $('#' + $name).select2();
                addChangeEvent($name);
            }
        }

        $('#start_time_type').change(function() {
            $start_time_type = $(this).val();
            console.log($start_time_type);
            appendTime($start_time_type, 'start_time', $('#_start'));
        });

        $('#end_time_type').change(function() {
            $end_time_type = $(this).val();
            appendTime($end_time_type, 'end_time', $('#_end'));
        });



        $('.point_info').on('change', function(e) {
            var $param = $(this).val();
            if($param === null) return true;
            $('select').not($(this)).find('option').each(function() {
                var $option = $(this);
                $.each($param, function(k, v){
                    if($option.val() === v) {
                        $option.remove();
                    }
                })
            })
        })

        $('.point_info').on('select2-removed', function(e){
            $('select').append(e.choice.element);
        });


        function startTimeSet() {
            //timerange 初始化
            if($start_time_type != '') {
                $('#start_time_type').select2('val', $start_time_type);
                $('#start_time_type').trigger('change');
                if($start_time_type == 'absolute') {
                    //如果是绝对定位
                    $('#start_time').val($start_time_value);
                } else {
                    //否则为相对定位
                    $('#start_time').select2('val', 'custom');
                    appendCustom('start_time');
                    $('#custom_start_time_num').val($start_time_value);
                    $('#custom_start_time_type').select2('val', $start_time_unit);
                }
            }
        }

        function endTimeSet() {
            //timerange 初始化
            if($end_time_type != '') {
                $('#end_time_type').select2('val', $end_time_type);
                $('#end_time_type').trigger('change');
                if($end_time_type == 'absolute') {
                    //如果是绝对定位
                    $('#end_time').val($end_time_value);
                } else {
                    //否则为相对定位
                    $('#end_time').select2('val', 'custom');
                    appendCustom('end_time');
                    $('#custom_end_time_num').val($end_time_value);
                    $('#custom_end_time_type').select2('val', $end_time_unit);
                }
            }
        }
        //初始化 只要将参数放入对应的表单中
        function init() {
            //左右轴 初始化
            $('#p_left').select2('val',  $pl_selected.split(','));
            $('#p_right').select2('val', $pr_selected.split(','));
            $('.point_info').trigger('change');
            //开始、结束时间初始化设置
            startTimeSet();
            endTimeSet();
        }
        init();
    }
    </script>                                                                                                                                                                                                                                          