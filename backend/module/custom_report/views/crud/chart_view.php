<?php
use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\CustomReportSearch $searchModel
 */
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Custom Reports');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false">
        <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

            <h2><?= isset($report_name) ? $report_name : Html::encode($this->title) ?></h2>
            <div class="jarviswidget-ctrls">

            </div>
        </header>

        <!-- widget div-->
        <div role="content">

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">

                <div class="tab-content">

                    <div class="tab-pane active" id="hr2">
                        <!--                在此处增加一个表单选项，来让数据进行实时的传递-->
                        <!-- 开始时间 -->


                        <!-- 开始时间 -->




                        <div class = "form-group col-md-12">
                            <!--                           在label中输入了，custom下的name属性，来填充楼model的模型-->
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report','Start time')?></label>

                            <div id = '_start' ></div>
                            <!-- 开始时间 -->
                            <label class="col-md-1 control-label"><?=Yii::t('custom_report','End time')?></label>
                            <div id = '_end'>

                            </div>
                            <a href="javascript:void(0)" id ="_shijian" class="btn btn-success">开始加载数据</a>
<!--                            <div class="form-actions" >-->
<!--                                <span id="error_tip_one" style="color:red;float:left"></span>-->
<!--                                <a href="javascript:void(0)" id="_shijian" class="btn btn-success"> 创建</a>-->
<!--                            </div>-->







                        </div>



                        <!-- 结束时间 -->



<!--                       结束时间 -->
                        <!--                    在此处之上是增加的部分-->

                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#iss1" id = 'chart_all' style = 'color:#472525!important' data-toggle="tab" data-type = 'all'>双轴</a>
                            </li>
                            <li class="">
                                <a href="#iss2" id = 'chart_left' style = 'color:#472525!important' data-toggle="tab" data-type = 'left'>左轴</a>
                            </li>
                            <li class="">
                                <a href="#iss3" id = 'chart_right' style = 'color:#472525!important' data-toggle="tab" data-type = 'right'>右轴</a>
                            </li>

                        </ul>
                    </div>
                    <div class="tab-content padding-10">
                        <div class="tab-pane active" id="iss1">
                            <div class="widget-body">
                                <div id = 'all_content' style="min-height:500px;width:98%"></div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="iss2">
                            <div class="widget-body">
                                <div id = 'left_content' style="min-height:500px;width:98%"></div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="iss3">
                            <div class="widget-body">
                                <div id = 'right_content' style="min-height:500px;width:98%"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</section>

<!--<div id="test_highchart" style="min-height:500px;width:98%"></div>-->
<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile('js/highcharts/theme/grid.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);


$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

?>

<script>
    window.onload = function () {
        var start_time= $('#start_time').val();
        var end_time= $('#end_time').val();
        var date = new Date();
        var result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        console.log(result);
        console.log(9807);
        //绑定开始时间
        appendTime( 'start_time', $('#_start'));
        //绑定结束时间
        appendTime( 'end_time', $('#_end'));
        //初始化开始时间为空
        $('#start_time').val(result);
        //初始化结束时间为空
        $('#end_time').val(result);
        //执行highchart函数
       highcharts_lang_date();

        var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
        //highcharts 图形中所需要的变量
        var $chart_data = [], $chart_left_data = [], $chart_right_data = [], $chart_current_data = [];
        var $point_info   = <?=isset($point_info) ? json_encode($point_info) : '{}'?>;
       // console.log($point_info);
        var $left_unit = <?=isset($left_unit) ? "'" .$left_unit ."'" : ''?>;
        var $right_unit  = <?=isset($right_unit) ? "'" .$right_unit ."'" : ''?>;
        var $report_name = <?=isset($report_name) ? "'" .$report_name ."'" : ''?>;
        var $type = 'all';
        yAxisCreate('all');

        function appendTime( $name, $obj) {
            $obj.empty();
                //添加开始时间的 时间选择器
                $obj.append(
                    '<div class = "col-md-3" >' +
                        '<input type="text" id=' + $name + ' class="form-control" name = ' + $name + '  title = <?=Yii::t('app', 'Choose Time')?> placeholder = <?=Yii::t('app', 'Choose Time')?>>' +
                        '</div>'
                );
                timeTypeChange('day', $('#' + $name));

        }
        //y轴 构建function
        function yAxisCreate($type)
        {
            $yAxisOptions = [];     //y轴数组
            $title = []; //tilte
            //如果 chart_data 中的数据为空才请求服务器来获取数据
            if($chart_data == '') {
                $.ajaxSetup({async: false});
                $.getJSON(<?="'" .Url::toRoute('ajax-chart-data-new') .'?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?> + "&type=" + $type+"&start_time=" +result+"&end_time="+result,
                    function (data) {

                        // 所有点位的数据
                        $chart_data = data.chart;
                        console.log($chart_data);
                        // Y轴left、right的数据
                        console.log('%c---------------', 'color:red');
                        var $i = 0, $j = 0;
                        $.each($chart_data, function($key, $value) {
                            if($value.yAxis == 0) {
                                $chart_left_data[$i] = $value;
                                $i ++;
                            }else{
                                $chart_right_data[$j] = $value;
                                $j ++;
                            }
                        });
                    }).error(function() {
                        console.log('error');
                    });
            }
            //首次 缓存的数据是 所有数据 如果选择左右轴那么需要进行数据 处理
            switch ($type) {
                case 'all':
                    $yAxisOptions[0] = {
                        title: {

                            text: '左轴点位(' + $left_unit + ')'
                        },
                        labels: {
                            formatter: function() {
                                return this.value;
                            }
                        },
                        opposite:false
                    };
                    $yAxisOptions[1] =  {
                        title: {

                            text: '右轴点位(' + $right_unit + ')'
                        },
                        labels: {
                            formatter: function() {
                                return this.value;
                            }
                        },
                        opposite : true

                    };
                    $title = {
                        text : $report_name
                    };
                    $chart_current_data = $chart_data;
                    break;
                case 'left':
                    $yAxisOptions[0] = {
                        title: {

                            text: '左轴点位(' + $left_unit + ')'
                        },
                        labels: {
                            formatter: function() {
                                return this.value;
                            }
                        },
                        opposite:false
                    };
                    $title = {
                        text : $report_name + ' (左轴)'
                    };
                    $chart_current_data = $chart_left_data;
                    break;
                case 'right':
                    $yAxisOptions[0] = {};
                    $yAxisOptions[1] = {
                        title: {

                            text: '右轴点位('+ $right_unit + ')'
                        },
                        labels: {
                            formatter: function() {
                                return this.value;
                            }
                        }
                    };
                    $title = {
                        text : $report_name + ' (右轴)'
                    };
                    $chart_current_data = $chart_right_data;
                    break;
            }

            console.log('$chart_current_data',$chart_current_data, '');
            ajaxGetData($chart_current_data, $type + '_content', $yAxisOptions);
        }

        function afterSetExtremes(e)
        {
            var start_time;
            start_time=($('#start_time').val());
            console.log(start_time);
            console.log(000000000000000000000000000000000000000);
            var end_time;
            end_time=($('#end_time').val());
//            console.log(e.min);
//            console.log(5555555555555555555555555);
//            console.log(e.max);
            $.ajaxSetup({async: true});
            chart.showLoading('..load..');
            //从方法ajax-chart-data-new中获取字符串,并切获取字符串
            //从服务器加载json编码的数据
            //表示ajax-chart-data-new发送到前台y
            $.getJSON(<?="'" .Url::toRoute('ajax-chart-data-new') .
                           '?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?> +
                "&type=" + $type +
                '&start_time=' + start_time +
                '&end_time=' + end_time,
                function (result) {
                    console.log(result);console.log('22222');
                    // 他必须得一组一组数据插入
                    for (var i = 0; i < result.chart.length; i++) {
                        chart.series[i].setData(result.chart[i].data);
                    }
                    chart.hideLoading();
                });
        }

        //数据获取
        //ajax传递楼3个参数，分别是data，content，纵坐标的值
        function ajaxGetData(data, $content, $yAxisOptions)
        {
                console.log(data);


                 //设置属性
                chart = new Highcharts.StockChart({
                    chart: {
                        renderTo: $content,
                        borderWidth: 0,
                        borderRadius: 0,
                        type: 'spline',
                        zoomType: 'x'
                    },
                    title: $title,
                    credits : {
                        enabled : false
                    },
                    scrollbar: {
                        liveRedraw: false
                    },
                    legend: {
                        enabled: true,
                        align: 'center',
                        verticalAlign: 'bottom',
                        borderWidth: 2,
                        itemStyle: {
                            fontSize: '14px'
                        }
                    },
                    rangeSelector : {
                        enabled:false
                    },
//                    rangeSelector : {
//                        inputEnabled : false,
//                        buttons: [ {
//                            type: 'day',
//                            count: 1,
//                            text: '1d'
//                        }, {
//                            type: 'day',
//                            count: 7,
//                            text: '7d'
//                        }, {
//                            type: 'month',
//                            count: 3,
//                            text: '3m'
//                        }, {
//                            type: 'month',
//                            count: 6,
//                            text: '6m'
//                        }, {
//                            type: 'year',
//                            count: 1,
//                            text: '1y'
//                        }, {
//                            type: 'all',
//                            text: 'All'
//                        }]
//                    },

                    navigator : {
                        adaptToUpdatedData: false,
                        //传递的数组为data
                        series : data
                    },
                    xAxis : {
                        events : {
                            //x轴设置时间
                            afterSetExtremes : afterSetExtremes
                        },
                        minRange: 3600 * 1000
                    },
                    yAxis: $yAxisOptions,
                    tooltip: {
                        //格式化点位的信息放入span当中
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                        valueDecimals: 2,
                        valueDecimals: 2,
                        shared : true,
                        style : {
                            //color : 'white',
                            padding : '7px',
                            fontsize : '9pt'
                        }
                    },
                    series: data
                });

        }

        //标签点击事件
        $('.nav li a').click(function() {
            $type = $(this).data('type');
            yAxisCreate($type);
        });
        $('#_shijian').click(function(){
            var start_time;
            start_time=($('#start_time').val());
            console.log(start_time);
            console.log(000000000000000000000000000000000000000);
            var end_time;
            end_time=($('#end_time').val());

//            $.ajaxSetup({async: true});-->
//            chart.showLoading('Loading data from server...');-->
//          $.getJSON(-->//="'" .Url::toRoute('ajax-chart-data') .
//                           '?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?><!-- +-->
//            $type =  $('.nav li a').data('type');
//            yAxisCreate($type);
            $.ajaxSetup({async: false});
            chart.showLoading('Loading data from server...');
            $.getJSON(<?="'" .Url::toRoute('ajax-chart-data-new') .
                           '?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?> +
                "&type=" + $type +
                //注意当js在拼装的过程当中，变量不加引号
                '&start_time=' + start_time +
                '&end_time=' + end_time,
                function (result) {
//                    console.log(result);console.log('1111');
                    // 他必须得一组一组数据插入
                    for (var i = 0; i < result.chart.length; i++) {
                        chart.series[i].setData(result.chart[i].data);
                    }
                    chart.hideLoading();
                });

         console.log($('#end_time').val());


        })
    }
</script>