<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CustomReport $model
 */

$this->title = Yii::t('custom_report', 'Update {modelClass}: ', [
  'modelClass' => 'Custom Report',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'point_info' => $point_info,
        'model' => $model,
    ]) ?>
