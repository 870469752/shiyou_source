<?php

namespace backend\module\subsystem;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\subsystem\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
