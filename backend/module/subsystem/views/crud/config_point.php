<!-- NEW WIDGET START -->
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
        >
        <header>
            <div class="widget-toolbar smart-form" data-toggle="buttons">
                <button class="btn btn-xs btn-primary" id="search_new"  >
                    点位搜索
                </button>
                <button class="btn btn-xs btn-primary" id="pointsUpdate"  >
                    更新点位
                </button>
            </div>
        </header>
        <!-- 参数主体 start-->
        <div class="widget-body" style="display: inline-block;">
            <!-- Widget ID (each widget will need unique ID)-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                </div>
                <div class="widget-body no-padding">
                    <div class="well well-sm well-light">
                        <!--               tabs----start                         -->
                        <div id="tabs" >
                            <ul>
                                <li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
                                <li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                                <li><a href="#others" id="others_show">其他点位</a></li>
                            </ul>
                            <div id="bacnet">
                                <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                                        <th data-class="expand">ID</th>
                                        <th data-class="expand">中文名</th>
                                        <th data-class="expand">操作</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div id="modbus">
                                <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                                        <th data-class="expand">ID</th>
                                        <th data-class="expand">中文名</th>
                                        <th data-class="expand">操作</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div >
                            <div id="others">
                                <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                                        <th data-class="expand">ID</th>
                                        <th data-class="expand">中文名</th>
                                        <th data-class="expand">操作</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!--               tabs-----end                         -->

                        <!--            点位选择框-----start              -->

                        <div>
                            <table id="datatable_select" class="table table-striped table-bordered table-hover" style="width:100%;">
                                <thead>
                                <tr>
                                    <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                                    <th data-class="expand">ID</th>
                                    <th data-class="expand">中文名</th>
                                    <th data-class="expand">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!--            点位选择框-----end                -->
                        <a id="submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                       下一步
                        </a>

                    </div>
                </div>
                <!-- end widget content -->
            </div>
        </div>
    </div>
</article>
<?php
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>
<script>
    var oTable_others; var oTable_bacnet; var oTable_modbus;
    var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0;
    //新的选择框数据
    var new_selected_point=new Array();
    window.onload = function () {
        $('#tabs').tabs();
        //选定点位后提交到模式绑定界面
        $("#submit").click(function(){
//            console.log(new_selected_point);
            var points=JSON.stringify(new_selected_point);

                //跳转至子系统编辑界面
                var url="/subsystem/crud/config?related_id="+points+"&type=points";
                window.location=url;

        });

        /*********************************** 新的点位分页搜索 start  **********************************/
        $("#bacnet_show").click(function(){
            if(!temp_bacnet){
                oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
                temp_bacnet = 1;
            }
        });

        $("#modbus_show").click(function(){
            if(!temp_modbus){
                oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
                temp_modbus = 1;
            }
        });

        $("#others_show").click(function(){
            if(!temp_others){
                oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
                temp_others = 1;
            }
        });
        $(document).ready(function () {
            $('#tabs').tabs({
                activate: function(event, ui) {
                    ttInstances = TableTools.fnGetMasters();
                    for (i in ttInstances) {
                        if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                    }
                }
            });

            //  setTimeout(function(){$("#bacnet_show").click();},1000);
            setTimeout(function(){$("#bacnet_show").click();},1000);

        });

        var table_select=$('#datatable_select').DataTable({
            "iDisplayLength": 5

        });
        $("#search_new").click(function(){
            if ($("#tabs").css('display') == 'none')
                $("#tabs").show();
            else
                $("#tabs").hide();
        });

        //加载页面时要根据location_points信息更新selected表格信息
        //划掉暂时不用
        function  updateSelectedPoints() {
            if (location_points.length != 0) {
                for (var key in location_points) {
                    var point = location_points[key];
                    table_select.row.add([
                        '<input type="checkbox" name="checkList_' + point.cn + '" value="' + point.id + '" >',
                        point.id,
                        point.cn,
                        '<label class=""><button type="button" onclick="javascript:exedel_new(this);" class="exedel_new" id="selected' + point.id + '" point_id="' + point.id + '" name="' + point.cn + '"><span class="glyphicon glyphicon-remove"></span></button></label>',
                    ]).draw(false);
                    //记下已经添加的点位 以待添加时比较
                    new_selected_point.push(point.id);
                }
            }
        }

        window.exeselect_new = function(data){
            //把选择的点位data添加到点位选择表内
            //比较此点位是否已经加入到点位选择表中
//            console.log(table_select.row.data);
            var element=$(data);
            if(new_selected_point.indexOf(element.attr("point_id"))!=-1){
                alert('点位已经存在');
            }
            else {
                table_select.row.add([
                    '<input type="checkbox" name="checkList_' + data.name + '" value="' + data.point_id + '" >',
                    element.attr("point_id"),
                    element.attr("name"),
                    '<label class=""><button type="button" onclick="javascript:exedel_new(this);" class="exedel_new" id="selected' + element.attr("point_id") + '" point_id="' + element.attr("point_id") + '" name="' + element.attr("name") + '"><span class="glyphicon glyphicon-remove"></span></button></label>',
                ]).draw(false);
                //记下已经添加的点位 以待添加时比较
                new_selected_point.push(element.attr("point_id"));
            }
        };
        window.exedel_new = function(data){
            var element=$(data);
            //删除这一行tr标签以及内部内容
            var tr=$('#'+data.id).parent().parent().parent();
            console.log(tr);
            table_select.row(tr).remove().draw( false);
            //去除 new_selected_point中删掉的点位
            for(var point in new_selected_point){
                if(new_selected_point[point]==element.attr("point_id")){
                    new_selected_point=remove(new_selected_point,point);
                }
            }
            console.log(new_selected_point);
        };

        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable,tool,protocol_id,type) {
            /* TABLETOOLS */
            table = $('#'+name).dataTable({
//                "rowCallback": function( row, data, index ) {
//                },
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,6, 7, 8, 9, 10, -1], [5,6, 7, 8, 9, 10, "All"]],
                "iDisplayLength": 5,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "ajax": {
                    "url": "/category/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }

                },
                "aoColumns": [
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<input type='checkbox' name='checkList_"+name+"' value='" + sData + "' >");
                        }
                    },
                    {"mDataProp": "id"},
                    {"mDataProp": "cn"},
                    {
                        "mDataProp": "cn",
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<label class=""><button type="button" onclick="javascript:exeselect_new(this);" class="exeselect_new" id="show'+oData.id+'" point_id="'+oData.id+'" name="'+oData.cn+'" ><span class="glyphicon glyphicon-ok"></span></button></label>');
                            $('.aa').click(function(){
                                var strs= new Array(); //定义一数组
                                strs=$(this).attr("idAndName").split("@"); //字符分割
                                //     alert($ele_val1+'  '+$ele_text);
                                //  alert($(this).attr("idAndName"));
                                $ele_val='p'+strs[0];
                                $ele_text=strs[1];
                                $('#_pointname').val($ele_text);
                            });
                            //   $(nTd).html("<a href='/point_table_report/crud/doexport?point_id="+sData+"'>报表查看</a>"+"<br>"+"<br>"+"<a href='/point_table_report/crud/graphic?point_id="+sData+"'>图形查看</a>");
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {

                },
                "order": [1, 'asc']
            });
            /* END TABLETOOLS */
            return table;
        }
        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }
        /*********************************** 新的点位分页搜索 end  **********************************/

    }
    </script>