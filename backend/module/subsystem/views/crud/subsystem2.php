<?php
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use common\library\MyFunc;

?>
<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }

    #infoBoxHolder {
        z-index: 300;
        position: absolute;
        left: 5px;
    }

    #infoBox {
        border: 1px solid #999;
        padding: 8px;
        background-color: whitesmoke;
        opacity:0.9;
        position: relative;
        width: 250px;
    //height: 60px;
        font-family: arial, helvetica, sans-serif;
        font-weight: bold;
        font-size: 11px;
    }

    /* this is known as the "clearfix" hack to allow
       floated objects to add to the height of a div */
    #infoBox:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }

    div.infoTitle {
        width: 50px;
        font-weight: normal;
        color:  #787878;
        float: left;
        margin-left: 4px;
    }

    div.infoValues {
        width: 150px;
        text-align: left;
        float: right;
    }

</style>
<!--视频   -->
<!-- html 区域 start-->
<a  id="show_vedio_div"  data-toggle="modal" data-target="#myModal" ></a>
<div class="modal"  style="padding-top: 100px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;height: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h6 class="modal-title" id="myModalLabel">视频监控 <span style = 'margin-left:70%'>操作</span>:</h6>
            </div>
            <div class="modal-body" style = 'height: 300px'>
                <div class="row">
                    <div class="col-md-12">
                        <div class = 'col-md-9' >
                            <div id="divPlugin"></div>
                            <div id="videoMessage"></div>
                        </div>
                        <div class = 'col-md-3' style = "padding-top: 30px">
                            <div style = 'margin-top:18px'>
                                <table style = 'margin-left:13px'>
                                    <tr>
                                        <td></td>
                                        <td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(1);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">
                                                <i class="glyphicon glyphicon-circle-arrow-up"></i></a></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(3);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">
                                                <i class="glyphicon glyphicon-circle-arrow-left"></i></a></td>
                                        <td><a href="javascript:void(0);" onclick="mouseDownPTZControl(9);" class="btn  btn-circle btn-lg">
                                                <i class="glyphicon glyphicon-fullscreen"></i>
                                            </a></td>
                                        <td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(4);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">
                                                <i class="glyphicon glyphicon-circle-arrow-right"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(2);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">
                                                <i class="glyphicon glyphicon-circle-arrow-down"></i>
                                            </a></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <br>
                            <br>
                            <div class = 'col-md-3' style = 'padding:0px;font-size: 6px'>
                                <table style = 'margin-left:13px;font-size: 15px'>
                                    <tr >
                                        <td></td>
                                        <td  style = 'padding:6px'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr >
                                        <td ><a href="javascript:void(0);" onmousedown="PTZZoomout()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>变倍</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZZoomIn()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:void(0);" onmousedown="PTZFoucusOut()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>焦距</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZFocusIn()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisOut()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>
                                        <td>光圈</td>
                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisIn()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- END MAIN CONTENT -->

<!--<div class="modal"  style="padding-top: 200px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content" style="width: 700px;height: 500px">-->
<!--            <div class="modal-header">-->
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">-->
<!--                    ×-->
<!--                </button>-->
<!--                <h6 class="modal-title" id="myModalLabel">视频监控 <span style = 'margin-left:70%'>操作</span>:</h6>-->
<!--            </div>-->
<!--            <div class="modal-body" style = 'height: 300px'>-->
<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <div class = 'col-md-9' >-->
<!--                            <div id="divPlugin"></div>-->
<!--                        </div>-->
<!--                        <div class = 'col-md-3' style = "padding-top: 30px">-->
<!--                            <div style = 'margin-top:18px'>-->
<!--                                <table style = 'margin-left:13px'>-->
<!--                                    <tr>-->
<!--                                        <td></td>-->
<!--                                        <td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(1);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">-->
<!--                                                <i class="glyphicon glyphicon-circle-arrow-up"></i></a></td>-->
<!--                                        <td></td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(3);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">-->
<!--                                                <i class="glyphicon glyphicon-circle-arrow-left"></i></a></td>-->
<!--                                        <td><a href="javascript:void(0);" onclick="mouseDownPTZControl(9);" class="btn  btn-circle btn-lg">-->
<!--                                                <i class="glyphicon glyphicon-fullscreen"></i>-->
<!--                                            </a></td>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(4);" onmouseup="mouseUpPTZControl();" class="btn  btn-circle">-->
<!--                                                <i class="glyphicon glyphicon-circle-arrow-right"></i></a></td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td></td>-->
<!--                                        <td style = 'padding-left: 10px;'><a href="javascript:void(0);" onmousedown="mouseDownPTZControl(2);" onmouseup="mouseUpPTZControl();" class="btn btn-circle">-->
<!--                                                <i class="glyphicon glyphicon-circle-arrow-down"></i>-->
<!--                                            </a></td>-->
<!--                                        <td></td>-->
<!--                                    </tr>-->
<!--                                </table>-->
<!--                            </div>-->
<!--                            <br>-->
<!--                            <br>-->
<!--                            <div class = 'col-md-3' style = 'padding:0px;font-size: 6px'>-->
<!--                                <table style = 'margin-left:13px;font-size: 15px'>-->
<!--                                    <tr >-->
<!--                                        <td></td>-->
<!--                                        <td  style = 'padding:6px'>&nbsp;&nbsp;&nbsp;&nbsp;</td>-->
<!--                                        <td></td>-->
<!--                                    </tr>-->
<!--                                    <tr >-->
<!--                                        <td ><a href="javascript:void(0);" onmousedown="PTZZoomout()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>-->
<!--                                        <td>变倍</td>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="PTZZoomIn()" onmouseup="PTZZoomStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="PTZFoucusOut()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>-->
<!--                                        <td>焦距</td>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="PTZFocusIn()" onmouseup="PTZFoucusStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisOut()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></a></td>-->
<!--                                        <td>光圈</td>-->
<!--                                        <td><a href="javascript:void(0);" onmousedown="PTZIrisIn()" onmouseup="PTZIrisStop()" class="btn  btn-circle"><i class="glyphicon glyphicon-plus-sign"></i></a></td>-->
<!--                                    </tr>-->
<!--                                </table>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div><!-- /.modal-content -->
<!--    </div><!-- /.modal-dialog -->
<!--</div>-->
<!-- END MAIN CONTENT -->



<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>




                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2><?=MyFunc::DisposeJSON($model->name)?> </h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" data-target="#SaveMenu" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>

                        <a id="check_menu" class="button-icon"     rel="tooltip" data-original-title="调试框" data-placement="bottom">
                            <i class="fa fa-table"></i>
                        </a>
                    </div>
                </header>

                <div>
                 <div id="myDiagramX" style="border: solid 1px black; width:100%; height:850px;background:rgb(29,27,29)"></div>
                <div id="infoBoxHolder">
                    <!-- Initially Empty, it is populated when updateInfoBox is called -->
                </div>
                    </div>
        </article>
    </div>
</section>

<!--多点位展示-->
<div id="many_points" >
    <!--         Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-4" data-widget-editbutton="false" data-widget-deletebutton="false">

        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2><?=Yii::t('point', '点位')?></h2>

        </header>

        <div>
            <div class="widget-body no-padding">
                <table id="selected_datatable_tabletools3" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                        <th>值</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyData_many_points">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<!-- font  color-->
<div id="console_board" title="命令控制台" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('类型',null,  ['class' => 'control-label'])?>
        <div id="control_drop" style="width: 300px;height: 120px;">

        </div>
    </div>
</div>


<!--属性面板-->
<div id="AttributeMenu" style="width: auto;position:relative;z-index:10;display:none">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                <?=Html::hiddenInput('image_group_id',$model->id,['id'=>'image_group_id'])?>
                                <div class="row" style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('sub_system_id',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('sub_system_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'sub_system_id'])?>
                                    </div>
                                </div>

                                <div class="row"  style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                </div>




                                <div id='infoBox'>
                                    <div>Info</div>
                                    <div class='infoTitle'>属性名称</div>
                                    <div class='infoValues'>值</div>
                                    <div  class='infoTitle'>点位id</div>
                                    <div id='point_id' class='infoValues'>值</div>
                                    <div class='infoTitle'>点位值</div>
                                    <div  id='point_value'class='infoValues'>值</div>
                                    <div class='infoTitle'>更新时间</div>
                                    <div id='update_time' class='infoValues'>值</div>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>


<p id="sub_system_id" style="display: none;"></p>
<p id="diagramEventsMsg" style="display: none;">Msg</p>
<div id="contextMenu">
    <ul>
        <li><a href="#" id="menu1" onclick="AtomicEvent()">原子事件</a></li>
        <li><a href="#" id="menu5" onclick="">属性</a></li>
        <li><a href="#" id="menu3" onclick=" ">持续时间</a></li>
        <li><a href="#" id="menu4" onclick=" ">删除</a></li>
    </ul>
</div>
<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/ScrollingTable.js" );
$this->registerJsFile("js/gojs/node_template_edit_view.js", ['backend\assets\AppAsset']);


$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);



$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/skin-bootstrap/skin-win7/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);


$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);


?>

<script>
    var user_id=<?=$user_id?>;
    var is_control=<?=$control?>;
    var sub_system_data=new Array();
    var sub_system_info=<?=$sub_system_data?>;

    var responsiveHelper_datatable_tabletools = undefined;
    var sub_system_msg=null;
    var diagram_now=null;

    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    var num=null;
    function resize() {
        for(var key in sub_system_data) {var diagram = sub_system_data[key];diagram.requestUpdate();}
    }
    var ip;
    var g_iWndIndex = 0;
    window.onload = function () {

        document.getElementById("sub_system_id").value= <?=$id?>;

        var data=<?=$model->data?>;
        var image_url='<?=$base_map?>';
//        console.log(data);
        $('.ui-fancytree').css({'height': '400px'});
        //属性窗口可拖动
        $('#AttributeMenu').draggable();
        $("#check_menu").click(function () {
            var display=document.getElementById("AttributeMenu").style.display
            if(display=='none') document.getElementById("AttributeMenu").style.display='';
            else document.getElementById("AttributeMenu").style.display='none';
        })
        //初始化下拉框
        $("#select_point option[value='"+'en'+"']").attr("selected","selected");
        $("#select_point").chosen();

        //提供 图id与 设施key  ajax得到设施信息
        function ajaxGetAttribute(sub_system_id,s){
            var id=$("#image_group_id").val();
            //ajax获取信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-get-element",
                data: {sub_system_id:sub_system_id, element_id: s.key},
                success: function (msg) {
                    var data = eval('[' + msg + ']')[0];
//                    console.log(data);
                    //只有一个点位  同时更新到原来的属性框中
                    if (data.length == 1)
                        update(data[0]['id']);
//                    console.log(data);
//                    console.log(s.key);
                    if(s.img=="/uploads/pic/摄像头1.jpg" || s.img=="/uploads/pic/摄像头2.jpg" || s.img=="/uploads/pic/摄像头3.jpg" ||
                        s.img=="/uploads/pic/摄像头4.jpg"||s.img=="/uploads/pic/摄像头5.jpg"||s.img=="/uploads/pic/摄像头6.jpg"||
                        s.img=="/uploads/pic/摄像头7.jpg"||s.img=="/uploads/pic/摄像头8.jpg") {
                        console.log('摄像头');
                        show_video(data[0]['id']);
                    }
                }
            });
        }
        $("#update").click(function(){
            updatevalue();
        })

        setInterval(gif,50);
        //动态图测试
        function gif() {
            for (var key in sub_system_data) {
                //tabs_id 对应div的value值为
                var tabs_id = "tabs-myDiagram-" + key;
                var sub_system_id = $("#" + tabs_id).attr("value");
                //加入指定的node
                var diagram = sub_system_data[key];
                var model = diagram.model;
                var arr = model.nodeDataArray;
                model.startTransaction("flash");
                for (var i = 0; i < arr.length; i++) {
                    var data = arr[i];
                    if (data.category == 'gif' && data.text.gif==1) {
                        var img = data.img;
                        var text = data.text;
                        var active = data.text.active;
                        active++;
                        if (text.pic[active] != undefined) {
                            img = text.pic[active];
                            text.active = active;
                        }
                        else {
                            img = text.pic[0];
                            text.active = 0;
                        }
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                    diagram.model.commitTransaction("flash");
                }
            }
        }
        //控制按钮值的更新
        function controlupdate(console_type){
            var diagram=diagram_now;
            //得到 字体字段
            var font_css=$("#font_css").val();
            var font_color=$("#fontcolor").val();
            var key=$("#element_id").val();
            console.log(font_color);
            var model = diagram.model;
            var arr = model.nodeDataArray;
            model.startTransaction("flash");
            for (var i = 0; i < arr.length; i++) {
                var data = arr[i];
                if (data.key==key) {
                    model.setDataProperty(data, "color", '#333');
                    if(data.value[0]==1){
                        switch (console_type) {
                            case undefined:
                                model.setDataProperty(data, "text", '开');
                                break;
                            case null:
                                model.setDataProperty(data, "text", '开');
                                break;
                            default :
                                model.setDataProperty(data, "text", console_type);
                                break;
                        }
                    }
                    //其他显示config值
                    else
                    {
                        switch (console_type) {
                            case undefined:
                                model.setDataProperty(data, "text", console_type);
                                break;
                            case null:
                                model.setDataProperty(data, "text", "????");
                                break;
                            case '0':case 0:
                            model.setDataProperty(data, "text", data.value[0]);
                            break;
                            case '-1':case -1:
                            model.setDataProperty(data, "text", data.value[-1]);
                            break;
                            case '1':case 1:
                            model.setDataProperty(data, "text", data.value[1]);
                            break;
                            case '255':case 255:
                            model.setDataProperty(data, "text", data.value[255]);
                            break;
                            default :
                                model.setDataProperty(data, "text", "????");
                                break;
                        }
                    }
                }
                diagram.model.commitTransaction("flash");
            }


        }
        init_updatevalue();
        setInterval(updatevalue,1000);
        //初始化更新节点值  读取 control值一次
        function init_updatevalue(){
//            console.log(sub_system_info);
            if(sub_system_info.points.length==0)
               console.log('无点');
            else {
                //ajax 查询点位信息后更新
                $.ajax({
                    type: "POST",
                    url: "/category/crud/ajax-update-value-new",
                    data: {data: sub_system_info},
                    success: function (msg) {

                        //返回value绑定点位的值
//                    console.log(msg);
                        msg = eval('(' + msg + ')');
                        sub_system_msg = msg;
                        for (var key in sub_system_data) {
                            var tabs_id = "tabs-myDiagram-" + key;
                            var sub_system_id =<?=$id?>;
                            //加入指定的node
                            var diagram = sub_system_data[key];
                            var temp = diagram.model.toJson();
                            var data = eval('(' + temp + ')');
                            //得到节点对象
                            var nodeDataArray = data['nodeDataArray'];
                            var key_value = msg[sub_system_id];

                            var model = diagram.model;
                            var arr = model.nodeDataArray;
                            model.startTransaction("flash");

                            if (key_value != undefined) {
                                //标记此图的value是否改变
                                var flag = false;

//                            var node_value=key_value[nodekey];
                                for (var i = 0; i < arr.length; i++) {
                                    var data = arr[i];
                                    var key = data.key;
                                    if (key_value[key] != undefined) {
                                        switch (data.category) {
                                            case 'value':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'onoffvalue':
                                                switch (key_value[key]['value']) {

                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", '关');
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", '开');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'alarmvalue':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", '正常');
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", '报警');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'state':
//                                            console.log(data.text);
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                    case '0':
                                                    case 0:
                                                    case '2':
                                                    case 2:
                                                    case '1':
                                                    case 1:
                                                    case 255:
                                                    case '255':
                                                        model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                }
                                                break;
                                            case 'state_value':
                                            case 'control':
                                                model.setDataProperty(data, "color", '#908988');
                                                //手动设置 显示点位值
                                                if (data.value[0] == 1) {
                                                    switch (key_value[key]['value']) {
                                                        case undefined:
                                                            model.setDataProperty(data, "text", '开');
                                                            break;
                                                        case null:
                                                            model.setDataProperty(data, "text", '开');
                                                            break;
                                                        default :
                                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                                            break;
                                                    }
                                                }
                                                //其他显示config值
                                                else {
                                                    switch (key_value[key]['value']) {
                                                        case undefined:
                                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                                            break;
                                                        case null:
                                                            model.setDataProperty(data, "text", "开");
                                                            break;
                                                        case '0':
                                                        case 0:
                                                            model.setDataProperty(data, "text", data.value[0]);
                                                            break;
                                                        case '1':
                                                        case 1:
                                                            model.setDataProperty(data, "text", data.value[1]);
                                                            break;
                                                        case '255':
                                                        case 255:
                                                            model.setDataProperty(data, "text", data.value[255]);
                                                            break;
                                                        default :
                                                            model.setDataProperty(data, "text", "开");
                                                            break;
                                                    }
                                                }
                                                break;
                                            case 'only_value':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'value_1':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case 0:
                                                    case '0':
                                                        model.setDataProperty(data, "text", -1);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'gif':

                                                var text = data.text;
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        text.gif = 0;
                                                        break;
                                                    case 1:
                                                        text.gif = 1;
                                                        break;
                                                    default :
                                                        text.gif = 0;
                                                        break;
                                                }
                                                model.setDataProperty(data, "text", text);
                                                break;
                                            case 'main_edit':
                                                if (key_value != undefined) {
                                                    if (
                                                        data['name'] == '0'
                                                    ) {
                                                        model.setDataProperty(data, "name", key_value[key]['name']);
                                                    }
                                                    else {
//                                            console.log(nodeDataArray[nodeDatas]['name']);
                                                    }
                                                    model.setDataProperty(data, "value", key_value[key]['value']);
                                                }
                                                break;
                                            case 'table':
                                                model.setDataProperty(data, "items", key_value[key]);
                                                break;
                                        }
                                    }
                                    diagram.model.commitTransaction("flash");
                                }

                            }
                        }
                    }
                });

            }
        }
        //ajax 更新节点值
        function updatevalue(){
//            console.log(sub_system_info);
            if(sub_system_info.points.length==0)
                console.log('无点');
            else {
                //ajax 查询点位信息后更新
                $.ajax({
                    type: "POST",
                    url: "/category/crud/ajax-update-value-new",
                    data: {data: sub_system_info},
                    success: function (msg) {

                        //返回value绑定点位的值
//                    console.log(msg);
                        msg = eval('(' + msg + ')');
                        sub_system_msg = msg;
                        for (var key in sub_system_data) {
                            var tabs_id = "tabs-myDiagram-" + key;
                            var sub_system_id =<?=$id?>;
                            //加入指定的node
                            var diagram = sub_system_data[key];
                            var temp = diagram.model.toJson();
                            var data = eval('(' + temp + ')');
                            //得到节点对象
                            var nodeDataArray = data['nodeDataArray'];
                            var key_value = msg[sub_system_id];

                            var model = diagram.model;
                            var arr = model.nodeDataArray;
                            model.startTransaction("flash");

                            if (key_value != undefined && key_value.length != 0) {
                                //标记此图的value是否改变
                                var flag = false;

//                            var node_value=key_value[nodekey];
                                for (var i = 0; i < arr.length; i++) {
                                    var data = arr[i];
                                    var key = data.key;
                                    if (key_value[key] != undefined) {
                                        switch (data.category) {
                                            case 'value':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'onoffvalue':
                                                switch (key_value[key]['value']) {

                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", '关');
                                                        break;
                                                    case '1':
                                                    case 1:
                                                    case '255':
                                                    case 255:
                                                        model.setDataProperty(data, "text", '开');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'alarmvalue':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", '正常');
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", '报警');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'state':
//                                            console.log(data.text);
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                    case '0':
                                                    case 0:
                                                    case '1':
                                                    case 1:
                                                    case 255:
                                                    case '255':
                                                        model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "img", data.text[3]);
                                                        break;
                                                }
                                                break;
                                            case 'state_value':
//                                        case 'control':
                                                //手动设置 显示点位值
                                                if (data.value[0] == 1) {
                                                    switch (key_value[key]['value']) {
                                                        case undefined:
                                                            model.setDataProperty(data, "text", '?????');
                                                            break;
                                                        case null:
                                                            model.setDataProperty(data, "text", '?????');
                                                            break;
                                                        default :
                                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                                            break;
                                                    }
                                                }
                                                //其他显示config值
                                                else {
                                                    switch (key_value[key]['value']) {
                                                        case undefined:
                                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                                            break;
                                                        case null:
                                                            model.setDataProperty(data, "text", "????");
                                                            break;
                                                        case '0':
                                                        case 0:
                                                            model.setDataProperty(data, "text", data.value[0]);
                                                            break;
                                                        case '1':
                                                        case 1:
                                                            model.setDataProperty(data, "text", data.value[1]);
                                                            break;
                                                        case '2':
                                                        case 2:
                                                            model.setDataProperty(data, "text", data.value[1]);
                                                            break;
                                                        case '255':
                                                        case 255:
                                                            model.setDataProperty(data, "text", data.value[255]);
                                                            break;
                                                        default :
                                                            model.setDataProperty(data, "text", "????");
                                                            break;
                                                    }
                                                }
                                                break;
                                            case 'only_value':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'value_1':
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '?????');
                                                        break;
                                                    case 0:
                                                    case '0':
                                                        model.setDataProperty(data, "text", -1);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                }
                                                break;
                                            case 'gif':

                                                var text = data.text;
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        text.gif = 0;
                                                        break;
                                                    case 1:
                                                        text.gif = 1;
                                                        break;
                                                    default :
                                                        text.gif = 0;
                                                        break;
                                                }
                                                model.setDataProperty(data, "text", text);
                                                break;
                                            case 'main_edit':
                                                if (key_value != undefined) {
                                                    if (
                                                        data['name'] == '0'
                                                    ) {
                                                        model.setDataProperty(data, "name", key_value[key]['name']);
                                                    }
                                                    else {
//                                            console.log(nodeDataArray[nodeDatas]['name']);
                                                    }
                                                    model.setDataProperty(data, "value", key_value[key]['value']);
                                                }
                                                break;
                                            case 'table':
                                                model.setDataProperty(data, "items", key_value[key]);
                                                break;
                                        }
                                    }
                                    diagram.model.commitTransaction("flash");
                                }

                            }
                        }
                    }
                });
            }
        }

        function update_console_board(type,html){
            $("#control_drop").empty();
            $("#control_drop").append(html);

            //当是下拉框时初始化下拉框并给宽度300px
            if(type==0) {
                $("#console_type").chosen();
                $("#console_type_chosen").width(300);
            }
        }

        //更新属性框内的点位值
        function update(value){
            //chosen 先设置值在执行更新函数
            $("#select_point").val(value);
            $("#select_point").trigger("chosen:updated");
        }

        function update_points(data){
            var html='';
//            console.log(data);
            if(data.length!=0){
                $("#tbodyData_many_points").empty();
                for(var i in data){
                    html += '<tr><td>'+data[i]['name']+'</td><td>'+data[i]['value']+'</td></tr>';
                }
                $("#tbodyData_many_points").append(html);
//                $('#selected_datatable_tabletools3').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

//                console.log(data);
            }
            many_points.dialog("open");
        }
        //点击保存绑定信息
        $('#_submit').click(function(){
            var id=$("#image_group_id").val();
            var sub_system_id=$("#sub_system_id").val();
            var element_id=$("#element_id").val();
            var point_ids=$("#select_point").val();
            //ajax保存信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save-element",
                data: {sub_system_id:sub_system_id, element_id:element_id,binding_id:point_ids},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
                    msg=eval('['+msg+']');

                    console.log(msg);
                }
            });

        })

        //跟着窗口滚动
        function scroll(){
            $(window).scroll(function(){
                var oParent = document.getElementById('AttributeMenu');
                var x =oParent.offsetLeft;
                var y = oParent.offsetTop;
                var yy = $(this).scrollTop();//获得滚动条top值
                if ($(this).scrollTop() < 30) {
                    $("#AttributeMenu").css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位

                }else{
                    $("#AttributeMenu").css({"position":"absolute",top:yy+"px",left:x+"px"});
                }
            });
        }
        //s为所选节点 id为此节点所在的 myDiagram所在数组sub_system_data中的键值
        function showMessage(s,id) {
            var tabs_id = "sub_system_id";
            var sub_system_id =<?=$id?>;
//            console.log(s.key +"   "+tabs_id);
            //节点信息
            document.getElementById("diagramEventsMsg").textContent = s;
            var sub_system_id=document.getElementById("sub_system_id").value;
            document.getElementById("element_id").value = s.key;

            if(sub_system_msg[sub_system_id][s.key]!=undefined) {
                document.getElementById("point_id").innerText = sub_system_msg[sub_system_id][s.key]['id'];
                document.getElementById("point_value").innerText = sub_system_msg[sub_system_id][s.key]['value'];
                document.getElementById("update_time").innerText = sub_system_msg[sub_system_id][s.key]['time'];
            }
            else {
                document.getElementById("point_id").innerText = '无';
                document.getElementById("point_value").innerText = '无';
                document.getElementById("update_time").innerText = '无';
            }

            ajaxGetAttribute(sub_system_id,s);
            //ajaxGetAttribute(sub_system_id,s.key);
            update('zh');
            //document.getElementById("AttributeMenu").style.display='';

//            //可拖动
//            drag();
        }

        scroll();


        var tab_num=1;
        //console.log(all_sub_system);
        if(data['image_base']==0) {
            init2('myDiagramX', 1);
            sub_system_data[1].model = go.Model.fromJson(data['data']);
            sub_system_data[1].requestUpdate();
            num=1;
        }
        if(data['image_base']==1) {
            init1('myDiagramX', 2);
            sub_system_data[2].model = go.Model.fromJson(data['data']);
            sub_system_data[2].requestUpdate();
            num=2;
        }


        $('#tabs').tabs();
        $('#tabs2').tabs();
        var image_in=new Array();
        var data_name=new Array();

        //数组remove指定键值元素 数据
        function remove(a,id){
            var result=new Array();
            for(var key in a){
                if(key!=id)
                    result[key]=a[key];
            }
            return result;
        }
        // Dynamic tabs
        var tabTitle = $("#tab_title");
        var tabContent = $("#tab_content");
        var tabTemplate =   "<li style='position:relative;'> "+
            "<span class='air air-top-left delete-tab' style='top:7px; left:7px;'>"+
            "<button class='btn btn-xs font-xs btn-default hover-transparent'>"+
            "<i class='fa fa-times'>"+
            "</i>" +
            "</button>" +
            "</span>" +
            "</span>" +
            "<a onclick='resize()' href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}" +
            "</a>" +
            "</li>";


        var id = "sub_system";
        var tabs = $("#tabs2").tabs();

        //弹出框内三个值
        var sub_system_name=$("#sub_system_name");
        var image_base=$("#image_base");
        var sub_system_category=$("#sub_system_category");


        //控制台 操作页面
        var console_board = $("#console_board").dialog({
            autoOpen : false,
            width : 400,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
                    //获取所操作子系统sub_system_id 以及节点的element_id
                    //以及  命令值
                    var sub_system_id=$("#sub_system_id").val();
                    var element_id=$("#element_id").val();
//                    var console_value=$("#console_value").val();
                    var console_type=$("#console_type").val();
                    console.log(console_type);
                    //先更新节点值
                    controlupdate(console_type);
                    //ajax 在数据表内写入命令
                    $.ajax({
                        type: "POST",
                        url: "/category/crud/ajax-control",
                        data: {sub_system_id: sub_system_id,element_id:element_id,console_type:console_type},
                        success: function (msg) {
                            switch (msg){
                                case '-1':
                                    alert('未绑定点位');break;
                                case '0':
                                    alert('命令发送失败');break;
                            }
                        }
                    });
                    $(this).dialog("close");
                }
            }
            ]
        });

        //控制台 操作页面
        var many_points = $("#many_points").dialog({
            autoOpen : false,
            width : 'auto',
            resizable : false,
            modal : true
        });

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-danger",
                click : function() {
                    //验证弹出框内数据完整性
//                    var sub_system_name=$("#sub_system_name").val();
//                    var image_base=$("#image_base").find("option:checked").text();
//                    var sub_system_category=$("#sub_system_category").find("option:checked").text();
//                      alert(sub_system_category.find("option:checked").val())
                    if(sub_system_name.val()==''){
                        alert('请输入子系统名称');
                    }
                    else {
                        var name = sub_system_category.find("option:checked").text()+'_'+sub_system_name.val();
                        var id = sub_system_category.find("option:checked").val();
                        var with_image=image_base.find("option:checked").val();
                        //如果分类还有则 继续所属分类的标签
                        if(name!='_'+sub_system_name.val()) {
                            image_in[tab_num]=with_image;

                            //添加标签页
                            addTab(name,tab_num,0);
                            //初始化  myDiagram  有底图和无底图
                            if(with_image==0) init2('myDiagram'+tab_num,tab_num);
                            else init1('myDiagram'+tab_num,tab_num);

                            tab_num++;
                            //去掉添加了的子系统类型    [又不需要去掉了]
//                               $("#sub_system_category option[value=" + id + "]").remove();
//                               //改变选择值为第一个
//                               $("#sub_system_category option:first").prop("selected", 'selected');
//                               var text = $("#sub_system_category option:first").text();
//                               $("#select2-chosen-2").html(text);
                        }
                        //如果分类没有了 则提示
                        else alert('分类用完了');
                    }
                    $(this).dialog("close");
                }
            }]
        });




        // actual addTab function: adds new tab using the input from the form above
        function addTab(name,id,sub_system_id) {
            console.log(id);
            data_name[id]=name;
            var tabs_id = "tabs-myDiagram-" + id;
            var label = name || tabContent;
            var li = $(tabTemplate.replace(/#\{href\}/g, "#" + tabs_id).replace(/#\{label\}/g, label));
            var tabContentHtml='<div id="myDiagram'+id+'" style="border: solid 1px black; width:100%; height:100%;background:rgb(29,27,29)"></div>';
            tabs.find(".ui-tabs-nav").append(li);
            tabs.append("<div style='border: solid 1px black; width:100%; height:600px' id='" + tabs_id + "' value='"+sub_system_id+"'>" + tabContentHtml + "</div>");
            tabs.tabs("refresh");
            // clear fields
            $("#sub_system_name").val("");

        }



        // 增加子系统 并删除已经增加的子系统
        $("#add_subsystem").button().click(function() {
            dialog.dialog("open");
        });


        $("#save").click(function(){
            console.log(data_name);
            console.log(image_in);
            console.log(sub_system_data);
            document.getElementById("mySavedModel").value = null;
            //var data1=myDiagram1.model.toJson();
            var data=new Array();

            for( var key in sub_system_data){
                console.log('key= '+key);
                var id_tmep=document.getElementById("tabs-myDiagram-"+key).getAttribute('value');
                //key=key.toString();
                console.log("tabs-myDiagram-"+key);
                console.log(id_tmep);
                var data_temp={
                    'name':data_name[key],
                    'category_id':key,
                    'id':id_tmep,
                    'data':{'image_base':image_in[key],'data':sub_system_data[key].model.toJson()}
                };
                document.getElementById("mySavedModel").value = document.getElementById("mySavedModel").value +' '+data_name[key]+' '+key+sub_system_data[key].model.toJson();
                data.push(data_temp);

            }
            //console.log(data);
            var data=JSON.stringify(data);
            //console.log(data);
//            ajax save
//            sub_system Info  [name data category_id location_id]
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save",
                data: {data: data,location_id:location_id},
                //暂时未加判定是否成功插入
                success: function (msg) {
                }
            });

        })


        function doMouseOver(e) {
//            console.log(e);

            if (e === undefined) e = myDiagram.lastInput;
            var doc = e.documentPoint;

//             find all Nodes that are within 100 units
            var list = myDiagram.findObjectsNear(doc, 10, null, function(x) { return x instanceof go.Node; });
            // now find the one that is closest to e.documentPoint
            var closest = null;
            var closestDist = 999999999;

            list.each(function(node) {
                var dist = doc.distanceSquaredPoint(node.getDocumentPoint(go.Spot.Center));
                if (dist < closestDist) {
                    closestDist = dist;
                    closest = node;
                }
            });
            highlightNode(e, closest);
        }

        // Make sure the infoBox is momentarily hidden if the user tries to mouse over it
        var infoBoxH = document.getElementById("infoBoxHolder");
        infoBoxH.addEventListener("mousemove", function() {
            var box = document.getElementById("infoBoxHolder");
            box.style.left = parseInt(box.style.left) + "px";
            box.style.top = parseInt(box.style.top)+10 + "px";

        }, false);


        // Called with a Node (or null) that the mouse is over or near
        function highlightNode(e, node) {
            //到节点 并且 节点不为table
            if (node !== null && node.data['category']!='table' && node.data['category']!='figure'&& node.data['category']!='main') {
//                var shape = node.findObject("SHAPE");
//                shape.stroke = "white";
//                if (lastStroked !== null && lastStroked !== shape) lastStroked.stroke = null;
//                lastStroked = shape;
//                updateInfoBox(e.viewPoint, node.data);
            } else {
//                if (lastStroked !== null) lastStroked.stroke = null;
//                lastStroked = null;
                document.getElementById("infoBoxHolder").innerHTML = "";
            }
        }

        // This function is called to update the tooltip information
        // depending on the bound data of the Node that is closest to the pointer.
        function updateInfoBox(mousePt, data) {
//            console.log(sub_system_msg);
            var sub_system_id =<?=$id?>;
            var x =
                "<div id='infoBox'>" +
                "<div>Info</div>" +
                "<div class='infoTitle'>属性名称</div>" +
                "<div class='infoValues'>值</div>";

            //category
            x=x+  "<div class='infoTitle'>"+"category"+"</div>";
            x=x+   "<div class='infoValues'>" +data['category'] + "</div> ";
            //key
            x=x+  "<div class='infoTitle'>"+"key"+"</div>";
            x=x+   "<div class='infoValues'>" + data['key']+ "</div> ";
            //point_id
            if(sub_system_msg!=null) {
                if (sub_system_msg[sub_system_id][data['key']] != undefined)
                    var point_id = sub_system_msg[sub_system_id][data['key']]['id'];
                else point_id = '无';
            }
            else point_id = '无';
            x=x+  "<div class='infoTitle'>"+"点位id"+"</div>";
            x=x+   "<div class='infoValues'>" + point_id+ "</div> ";
            x=x+"</div>";
            var box = document.getElementById("infoBoxHolder");

            box.innerHTML = x;
            box.style.left = mousePt.x + "px";
            box.style.top = mousePt.y + "px";
        }


        function init_blank(element) {
            if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates

            var cellSize = new go.Size(10, 10);
            myDiagram2 =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        )
                    },
                    {
                        mouseOver: doMouseOver,
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        "resizingTool.isGridSnapEnabled": true,
                        "undoManager.isEnabled": true,
                        allowDrop: false,// must be true to accept drops from the Palette
                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.Center,
//                        initialViewportSpot: go.Spot.TopCenter,
                        isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );
            //点击事件
            myDiagram2.addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if(part.data.category=='button') {
                            button = myDiagram2;
                            button_key = part.data.key;
                            document.getElementById("node_link").value = part.data.link;
                            document.getElementById("node_name").value = part.data.text;
//                            $("#node_link").val(part.data.link);
//                            $("#node_name").val(part.data.text);
                            console.log('button_key');
                            console.log(part);
                            console.log(button_key);
                            linkdialog.dialog("open");
                        }
                    }

                    //showMessage(part.data,num);
                });




            var node_template=init_node_template_edit();
//            init_context_menu("contextMenu",myDiagram2);



            myDiagram2.nodeTemplateMap.add("main",node_template.main);
            myDiagram2.nodeTemplateMap.add("edit",node_template.edit);
            myDiagram2.nodeTemplateMap.add("value",node_template.value);
            myDiagram2.nodeTemplateMap.add("onoffvalue",node_template.onoff);
            myDiagram2.nodeTemplateMap.add("alarmvalue",node_template.alarm);
            myDiagram2.nodeTemplateMap.add("button", node_template.button);
            myDiagram2.nodeTemplateMap.add("test", node_template.test);
            myDiagram2.nodeTemplateMap.add("gif", node_template.gif);
            myDiagram2.nodeTemplateMap.add("table", node_template.table);
            myDiagram2.nodeTemplateMap.add("figure", node_template.figure);
            myDiagram2.nodeTemplateMap.add("main_edit", node_template.main_edit);
            myDiagram2.nodeTemplateMap.add("only_value", node_template.only_value);
            myDiagram2.nodeTemplateMap.add("state", node_template.state);
            myDiagram2.nodeTemplateMap.add("state_value", node_template.state_value);
            myDiagram2.nodeTemplateMap.add("control", node_template.control);
            myDiagram2.nodeTemplateMap.add("many", node_template.many);
            myDiagram2.nodeTemplateMap.add("value_1", node_template.value_1);
            myDiagram2.groupTemplate=node_template.group;
        }


        //初始化 有底图的
        function init1(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        )
                    },
                    {
                        mouseOver: doMouseOver,
                        allowVerticalScroll: false,
                        allowDrop: false,// must be true to accept drops from the Palette
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
                        contentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    });


            sub_system_data[num] = myDiagram;


            //单击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function (e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if (part.data.category == 'button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
                        }
                    }
                });

            //双击事件
            sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        //更新属性框信息
                        showMessage(part.data, num);
                        switch (part.data.category) {
                            //control类型 弹出控制台
                            case 'control':
                                button = sub_system_data[num];
                                button_key = part.data.key;
                                var value = part.data.value;
                                console.log(value[0] != 1);
                                if (value[0] != 1) {
                                    //根据此节点data.value更新控制台的下拉列表信息
                                    var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                    for (var key in value) {
                                        option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                    }
                                    option_html += '</select>';
                                    var type = 0;
                                }
                                else {

                                    var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                    var type = 1;
                                }

                                update_console_board(type, option_html);
                                console_board.dialog("open");
                                break;

                            case 'table':
                                console.log(part.data.value);
                                break;

                        }

                    };
                });


            //右键事件
            sub_system_data[num].addDiagramListener("ObjectContextClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if (
                            part.data.category == 'table'||
                                part.data.category == 'edit' ||
                                part.data.category == 'value'||
                                part.data.category == 'main' ||
                                part.data.category == 'only_value'
                            )
                        {
                            diagram_now=sub_system_data[num];
                            showMessage(part.data, num);
                        }
                    }

                });

            // the background image, a floor plan
            sub_system_data[num].add(
                $(go.Part,  // this Part is not bound to any model data
                    {
//                        layerName: "Background", position: new go.Point(0, 0),
                        selectable: false, pickable: false
                    },
                    $(go.Picture, "/" + image_url)
                ));

            var node_template = init_node_template_edit();
//            init_context_menu("contextMenu",sub_system_data[num]);
            //���nodeģ��
            sub_system_data[num].nodeTemplateMap.add("main", node_template.main);
            sub_system_data[num].nodeTemplateMap.add("edit", node_template.edit);
            sub_system_data[num].nodeTemplateMap.add("value", node_template.value);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue", node_template.onoff);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue", node_template.alarm);
            sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
            sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
            sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
            sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
            sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
            sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
            sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
            sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
            sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
            sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
            sub_system_data[num].groupTemplate=node_template.group;
        }
        //初始化 无底图的
        function init2(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        )
                    },
                    {
                        mouseOver: doMouseOver,
//                        allowVerticalScroll:false,
                        allowDrop: false,// must be true to accept drops from the Palette
//                        "draggingTool.isGridSnapEnabled": true,
//                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        isReadOnly: true,  // allow selection but not moving or copying or deleting
//                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );

            //把myDiagram 加入sub_system_data内
            sub_system_data[num]=myDiagram;
            //点击事件
            //单击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function(e) {

                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if(part.data.category=='button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
//                            console.log(part.data);
                        }
                    };
                });
            //双击事件
            sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        //更新属性框信息
                        diagram_now=sub_system_data[num];
                        showMessage(part.data, num);
                        if(
//                            user_id==36||user_id==37||user_id==38 ||user_id==0 ||
                                is_control==1) {
                            switch (part.data.category) {
                                //control类型 弹出控制台
                                case 'control':
                                    button = sub_system_data[num];
                                    button_key = part.data.key;
                                    var value = part.data.value;
                                    console.log(value[0] != 1);
                                    if (value[0] != 1) {
                                        //根据此节点data.value更新控制台的下拉列表信息
                                        var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                        for (var key in value) {
                                            option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                        }
                                        option_html += '</select>';
                                        var type = 0;
                                    }
                                    else {

                                        var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                        var type = 1;
                                    }

                                    update_console_board(type, option_html);
                                    console_board.dialog("open");
                                    break;

                                case 'table':
                                    console.log(part.data);
                                    break;
                                case 'many':
                                    //得到点击的子系统id  key
                                    var sub_system_id = document.getElementById("sub_system_id").value;
                                    var element_id = document.getElementById("element_id").value;

                                    var value = sub_system_msg[sub_system_id][element_id];
                                    console.log(sub_system_id);
                                    console.log(element_id);
                                    console.log(sub_system_msg);
                                    update_points(value);
                                    break;
                            }
                        }

                    };
                });

            //右键事件
            sub_system_data[num].addDiagramListener("ObjectContextClicked",
                function(e) {
//                    console.log('右键');
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if (part.data.category == 'table'||
                            part.data.category == 'edit' ||
                            part.data.category == 'value'||
                            part.data.category == 'main' ||
                            part.data.category == 'many' ||
                            part.data.category == 'only_value'

                            )
                        {
                            diagram_now=sub_system_data[num];
                            showMessage(part.data, num);
                        }
                    }

                });

            var node_template=init_node_template_edit();
//            init_context_menu("contextMenu",sub_system_data[num]);
            //
            sub_system_data[num].nodeTemplateMap.add("main",node_template.main);
            sub_system_data[num].nodeTemplateMap.add("edit",node_template.edit);
            sub_system_data[num].nodeTemplateMap.add("value",node_template.value);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue",node_template.onoff);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue",node_template.alarm);
            sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
            sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
            sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
            sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
            sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
            sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
            sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
            sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
            sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
            sub_system_data[num].nodeTemplateMap.add("many", node_template.many);
            sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
            sub_system_data[num].groupTemplate=node_template.group;
        }



        $('#selected_datatable_tabletools3').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "bSort" : false,
//            "sScrollX": "100%",
//            "bScrollCollapse": true,
            "paging":   false
        } );

    }



    function init_env(){
        // 检查插件是否已经安装过
        if (-1 == WebVideoCtrl.I_CheckPluginInstall()) {
            alert("您还未安装过插件，双击开发包目录里的WebComponents.exe安装！");
            return;
        }

        // 初始化插件参数及插入插件
        WebVideoCtrl.I_InitPlugin(490,380, {
            iWndowType: 1,
            cbSelWnd: function (xmlDoc) {
                g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
            }
        });
        WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

        // 检查插件是否最新
        if (-1 == WebVideoCtrl.I_CheckPluginVersion()) {
            alert("检测到新的插件版本，双击开发包目录里的WebComponents.exe升级！");
            return;
        }

        // 窗口事件绑定
        $(window).bind({
            resize: function () {
                var $Restart = $("#restartDiv");
                if ($Restart.length > 0) {
                    var oSize = getWindowSize();
                    $Restart.css({
                        width: oSize.width + "px",
                        height: oSize.height + "px"
                    });
                }
            }
        });

        //初始化日期时间
        var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
        $("#starttime").val(szCurTime + " 00:00:00");
        $("#endtime").val(szCurTime + " 23:59:59");
    }



    function show_video(id){
        $.ajax({
            url: '/video-surveillance/crud/get-info?&id='+id,
            async: false,
            dateType: 'json',
            success: function(data)
            {
                var temp = JSON.parse(data);
                $("#divPlugin").text('');
                $("#videoMessage").text('');
                $("#show_vedio_div").click();
                init_env();
                tmp_ip = temp.ip;
                tmp_channel = temp.channel;
                tmp_name = temp.name;
                clickLogin2(tmp_ip,tmp_channel,tmp_name);
            }
        });
    }

//    function show_video(id){
//        $.ajax({
//            url: '/video-surveillance/crud/get-info?&id='+id,
//            async: false,
//            dateType: 'json',
//            success: function(data)
//            {
//                var temp = JSON.parse(data);
//                $("#show_vedio_div").click();
//                init_env();
//                tmp_ip = temp.ip;
//                tmp_channel = temp.channel;
//                clickLogin2(tmp_ip,tmp_channel);
//            }
//        });
//    }


    function clickLogin2(szIP,id,name) {
        $("#videoMessage").append("<span>登录IP："+szIP+"</span><span style='padding-left: 30px'>通道号："+id+"</span><span style='padding-left: 30px'>通道名："+name+"</span>");
        tem_szIP = szIP;
        tem_id = id;
        var iRet = WebVideoCtrl.I_Login(szIP, 1, 80, "admin", "12345", {
            success: function (xmlDoc) {
                setTimeout('RealPlay(tem_szIP,tem_id)',10);
            },
            error: function() { //失败的回调函数
                alert("登录失败！");
            }
        });
    }

//    function clickLogin2(szIP,id) {
//
//        clickLogout2(szIP);
//        tem_szIP = szIP;
//        tem_id = id;
//        var iRet = WebVideoCtrl.I_Login(szIP, 1, 80, "admin", "12345", {
//            success: function (xmlDoc) {
//                setTimeout('RealPlay(tem_szIP,tem_id)',100);
//            },
//            error: function() { //失败的回调函数
//                alert("登录失败！");
//            }
//        });
//    }
    //退出登录
    function clickLogout2(szIP) {
        var iRet = WebVideoCtrl.I_Logout(szIP);
    }

    function RealPlay(ip,id) {
        //var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
        WebVideoCtrl.I_Stop(0);
            szIP = ip,
            iStreamType = 1,
            iChannelID = id,
            bZeroChannel = false,
            szInfo = "";

        /*if (oWndInfo != null) {// 已经在播放了，先停止
            WebVideoCtrl.I_Stop();
        }*/

        var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
            iStreamType: iStreamType,
            iChannelID: iChannelID,
            bZeroChannel: bZeroChannel
        });
        clickLogout2(szIP);
    }
</script>