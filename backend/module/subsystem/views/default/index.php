<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use yii\helpers\Url;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\InfoItemSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = '站点信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

</div>
<section id="widget-grid" class="">
    <div>
        <?= Html::Button( '返回',['class' => '_button']) ?>
    </div>

    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?php foreach($info as $v): ?>
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                    <h2><?= Html::encode($v['name']) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <div class="widget-body no-padding">
                        <table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
                            <thead><tr>
                                <?php $field = json_decode($v['field'], true)['data']; ?>
                                <?php foreach($field as $f): ?>
                                    <th><?= $f['name'] ?></th>
                                <?php endforeach ?>
                            </tr></thead>
                            <tbody>
                            <?php foreach($v['item'] as $row): ?>
                                <?php $data = json_decode($row['data'], true) ?>
                                <tr>
                                    <?php foreach($field as $k): ?>
                                        <td><?= @$data[$k['name']] ?></td>
                                    <?php endforeach ?>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </article>
    </div>
</section>
<script>
    window.onload = function(){
     $('._button').click(function(){
         location.href ="crud";
     })
    }
    </script>