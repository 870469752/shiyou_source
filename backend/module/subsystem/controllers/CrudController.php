<?php

namespace backend\module\subsystem\controllers;

use backend\models\ControlInfo;
use backend\models\Image;
use backend\models\Location;
use Yii;
use backend\models\InfoGroup;
use backend\models\search\InfoGroupSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use backend\models\ImageGroup;
use backend\models\CommandLog;
use common\models\User;
use backend\models\Point;
use backend\models\SchemaManage;
use backend\models\SchemaControl;
use common\library\MyFunc;
/**
 * CrudController implements the CRUD actions for InfoGroup model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfoGroupSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    //$sub_system 为子系统数据组成的数组
    public function getSubSystemPoint($sub_system){
        //得到子系统内所有绑定的点位 组成数组
        $points=[];$sub_system_data=[];
        $camera_points=[];
        $keys=[];
        $point_keys=[];
        $camera_key=[];
        $events=[];
        $events_keys=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];

//            echo '<pre>';
//            print_r($sub_system);
//            die;
            foreach ($data as $data_key=>$data_value) {
                //group 不存在category字段
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'state':
                    case 'state_event':
                    case 'state_defend':
                    case 'gif':
                    case 'gif_value':
                    case 'control':
                    case 'state_value':
                    case 'value_1':
                        //得到 需要查询节点的key
                        if( $data_value['key']!=null) {
                            $keys[] = $data_value['key'];
                            $sub_system_data[$system['id']][$data_value['key']] = null;
                        }
//                    if($data_value['img']=="/uploads/pic/摄像头1.jpg")
                        //判断是不是摄像头的点
                        if(isset($data_value['img']))
                            if(
                                $data_value['img']=="/uploads/pic/摄像头1.jpg" || $data_value['img']=="/uploads/pic/摄像头2.jpg" ||
                                $data_value['img']=="/uploads/pic/摄像头3.jpg" || $data_value['img']=="/uploads/pic/摄像头4.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头5.jpg"||$data_value['img']=="/uploads/pic/摄像头6.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头7.jpg"||$data_value['img']=="/uploads/pic/摄像头8.jpg"
                            ) {
                                $camera_key[] =$data_value['key'];
                            }
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //根据子系统id 和key 找到需要查询的点位集合
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();
            $type='points';

            foreach ($configs as $key=>$configs_value) {
                $data=json_decode($configs_value['config'],1)['data'];
                if(isset($data['points'])){
                    $type='points';
                    $points_info=$data['points'];
                }
                if(isset($data['events'])) {
                    $type='events';
                    $points_info = $data['events'];

                }
                if(isset($data['point_event'])) {
                    $type='point_event';
                    $points_info = $data['point_event'];

                }
//                echo '<pre>';
//                print_r($data);
//                die;
                //获取的data信息中  points 可能为点位id  可能为数组信息[id1=>name1,id2=>name2,...]
                if($points_info!="") {

                    if (!is_array($points_info)) {
                        $points[] = $points_info;
                        $point_keys[]=$configs_value['element_id'];
                        $sub_system_data[$system['id']][$configs_value['element_id']] = ['id' => $points_info];
                        if(!empty($camera_key)){
                            $camera_points[]= $points_info;
                        }
                    } else {
//                        echo '<pre>';
//                        print_r($points_info);

                        foreach ($points_info as $point_id => $point_name) {
                            if($type=='events' || $type=='point_event')  {
                                if($point_id!='name'){
                                    $events[]=$point_id;
                                    $events_keys[]=$configs_value['element_id'];
                                }
                            }
                            if($type=='points')  {$points[] = $point_id; $point_keys[]=$configs_value['element_id'];}
                            $sub_system_data[$system['id']][$configs_value['element_id']][] = ['id' => $point_id, 'name' => $point_name];

                        }

                    }
                }

            }
        }

        if(!empty($sub_system_data )){
            foreach($sub_system_data as $sub_system_id=>$sub_system){
                foreach($sub_system as $key=>$value){
                    if(empty($value))
                        unset($sub_system_data[$sub_system_id][$key]);
                }
            }
        }
        return [
            'data'=>$sub_system_data,
            'points'=>array_unique($points),'points_key'=>array_unique($point_keys),
            'camera_points'=>array_unique($camera_points),
            'events'=>array_unique($events),'events_key'=>array_unique($events_keys)
        ];
    }
    //得到 points 点位 写入
    public function SavePointCommand($points){
        $user = Yii::$app->user;
        $save_state='save';
        if(empty($points)){
            return 'points empty';
        }
        else {
            foreach ($points as $key => $point) {
                $command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
                $time = date('Y-m-d H:i:s', time());
                $data['CommandLog'] = [
                    'point_id' => $point,
                    'type' => 1,
                    'value' => '-1',
                    'timestamp' => $time,
                    'user_id' => $user->id,
                    'status' => 1
                ];
                $command_log->load($data);
                $command_log->save();
                if(!empty($command_log->errors)){
                    echo '<pre>';
                    print_r($command_log->errors);
                    die;
                    $save_state='save_error';
                }
            }
            return $save_state;
        }


    }
    public function actionView($id){
//
        //id=318   rgb(29,27,29)
        $model =SubSystem::findModel($id);

        $sub_system=ImageGroup::find()->where(['location_id'=>$model->location_id])->asArray()->all();
        $location_info=Location::findOne($model->location_id);
        $url=Image::findModel($location_info->image_id);
        $sub_system_data=self::getSubSystemPoint($sub_system);
        $save_state=self::SavePointCommand($sub_system_data['points']);
        $sub_system_data=json_encode($sub_system_data);
        if(empty($url->default)){
            $base_map='uploads/pic/map1.jpg';
        }
        $user_id=Yii::$app->user->id;
        $user=User::findOne($user_id);
        if(strlen($user->is_control)!=0)
            $control=$user->is_control;
        else $control=0;



        //318
        if($id=='000'){
            echo '111';
            die;
            return $this->render('subsystem1',[
                'control'=>$control,
                'user_id'=>$user->$user_id,
                'model'=>$model,
                'base_map'=>$base_map,
                'id'=>$id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
        else {
            return $this->render('subsystem', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'id' => $id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
    }
    /*
     *
     * 模式 配置页面
     * $type为               'subsystem'  'points'
     * $related_id 对应为     子系统id      点位id
     *
    */
    public function actionConfig($related_id,$type){

        if($type=='subsystem') {
            //得到本子系统信息
            $subsystem = SubSystem::findOne($related_id);
            //处理得到本子系统内的点位信息
            $subsystem_points = json_decode($subsystem->points, 1)['data'];
            $subsystem_points = isset($subsystem_points['points']) ? $subsystem_points['points'] : null;
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $subsystem_points])
                ->asArray()->all();
        }
        if($type=='points'){
            $subsystem=null;
            $points=$related_id;
            $points=substr($points,1,strlen($points)-2);
            $points=explode(',',$points);
            //处理字符串为整数
            foreach($points as $key=>$value){
                $value=substr($value,1,strlen($value)-2);
                $points[$key]=(int)$value;
            }
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $points])
                ->asArray()->all();
        }


        //控制模式信息
        $schemaControl=SchemaControl::find()->indexBy('id')
            ->asArray()->all();

        foreach ($schemaControl as $controlId=>$controlValue) {
            $schemaControl[$controlId]['control_info']=json_decode($schemaControl[$controlId]['control_info'],1);
        }
        $schemaControlName=MyFunc::_map($schemaControl,'id','name');
        $schemaControlInfo=MyFunc::map($schemaControl,'id','control_info');
        //时间模式信息
        $timeModelInfo=schemaManage::find()->asArray()->all();

        $timeModelInfo=MyFunc::_map($timeModelInfo,'id','name');
        $pointsInfo=MyFunc::map($points,'id','cn');


        return $this->render('config',[
            'subsystem'=>$subsystem,
            'point'=>$pointsInfo,
            'type'=>$type,
            'related_id'=>json_encode($related_id),
            //模式信息
            'schemaControlInfo'=>$schemaControlInfo,
            //模式下拉列表信息
            'schemaControlName'=>$schemaControlName,
            'timeModelInfo'=>$timeModelInfo
        ]);
    }
    //点位模式 配置页面
    public function actionConfigPoint(){


        return $this->render('config_point',[
        ]);
    }
    //config信息保存
    public function actionAjaxConfigSave(){
        $param=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($param);
//        die;
        $type=$param['type'];
        //类型是2 则表示关联着子系统
        if($type==2){
            $subsystem_id=$param['related_id'];
            $subsystem=SubSystem::findone($subsystem_id);

            $name=$subsystem->name;
            $related_id='{'.$param['related_id'].'}';

        }
        //类型是1 则表示关联着 自定义点位
        if($type==1){
            $related_id=json_decode($param['related_id']);
            $saveId=null;
            foreach ($related_id as $key=>$value) {
                $saveId=$saveId.','.$value;
            }

            $name=[
                'data_type'=>'description',
                'data'=>[
                    'zh-cn'=>$param['name'],
                    'en-us'=>$param['name']
                ]
            ];
            $related_id='{'.substr($saveId,1,strlen($saveId)-1).'}';

        }
            $success=false;
            foreach($param['data'] as $key=>$param_data) {
                    if(!empty($param_data)){
                        $model=new ControlInfo();
                        $data['ControlInfo']=[
                            'name'=>json_encode($name),
                            'type'=>$type,
                            'related_id'=>$related_id,
                            'time_mode'=>$param_data['timeId'],
                            'data'=>json_encode([
                                'data_type'=>'control_info',
                                'data'=>$param_data['controlInfo']['data']
                                ]
                            )
                        ];
                        $model->load($data);
                        $model->save();
                        $result=$model->errors;
                        if(!empty($result)){
                            echo '<pre>';
                            print_r($result);
                            die;
                        }
                    }
            }
    }
}
