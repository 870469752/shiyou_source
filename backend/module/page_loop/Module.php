<?php

namespace backend\module\page_loop;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\page_loop\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
