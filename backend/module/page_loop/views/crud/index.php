<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

?>



<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark"
				 data-widget-deletebutton="false"
				 data-widget-editbutton="false"
				 data-widget-colorbutton="false"
				 data-widget-sortable="false">
				<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
					<h2><?= Html::encode($this->title) ?></h2>
				</header>
				<!-- widget div-->
				<div>
                    <div class = 'row'>
                    <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                        <div class="jarviswidget jarviswidget-color-blueDark"
                             data-widget-deletebutton="false"
                             data-widget-editbutton="false"
                             data-widget-colorbutton="false"
                             data-widget-sortable="false"
                             data-widget-Collapse="false"
                             data-widget-custom="false"
                             data-widget-fullscreenbutton = 'false'
                             data-widget-togglebutton="false" id = '_jarviswidget_1'>
                            <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                                <h2>时间配置</h2>
                            </header>
                            <div>
                                <div style="height: 80px">
                                    <div style="width: 40%;float: left">
                                        <label>开始时间</label>
                                        <div class="input-group">
                                            <input id="start_time" class="form-control clockpicker"   type="text" placeholder="Select time" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div style="width: 40%;float:right;">
                                        <label>结束时间</label>
                                        <div class="input-group">
                                            <input id="end_time" class="form-control clockpicker"   type="text" placeholder="Select time" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                </div>


                                <div style="float:right;">
                                    <div class="input-group">
                                        <?=Html::button('添加',['id'=>'add','class'=>'btn bg-color-blue'])?>
                                    </div>
                                </div>
                                <div style="margin-right:10px;float:right;">
                                    <div class="input-group">
                                        <?=Html::button('删除',['id'=>'delete','class'=>'btn bg-color-blue'])?>
                                    </div>
                                </div>

                            </div>
                            <div id="timecontent" style="height: 100px">
                                <div id="tem_content"></div>
                            </div>
                        </div>

                    </article>
                    </div>
					<div class = 'row'>
						<article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
							<div class="jarviswidget jarviswidget-color-blueDark"
								 data-widget-deletebutton="false"
								 data-widget-editbutton="false"
								 data-widget-colorbutton="false"
								 data-widget-sortable="false"
								 data-widget-Collapse="false"
								 data-widget-custom="false"
								 data-widget-togglebutton="false" id = '_jarviswidget_2'>
								<header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
									<h2>子系统</h2>
								</header>

                                <div id="others">
                                    <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand" "><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>

                                        </tr>
                                        </thead>
                                    </table>
                                </div>

							</div>
						</article>
						<!-------     饼图内容 end-->
						<!-- 总用量 start-->
						<article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
							<div class="jarviswidget jarviswidget-color-blueDark"
								 data-widget-deletebutton="false"
								 data-widget-editbutton="false"
								 data-widget-colorbutton="false"
								 data-widget-sortable="false"
								 data-widget-Collapse="false"
								 data-widget-custom="false"
								 data-widget-fullscreenbutton = 'false'
								 data-widget-togglebutton="false" id = '_jarviswidget_3'>
								<header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
									<h2>配置</h2>
								</header>
								<div style="height:600px;">

								</div>
							</div>

						</article>
						<!-- 总用量 end-->
					</div>
					<div class="form-actions">
						<?= Html::submitButton('提交',['id'=>'_submit','class'=>'btn btn-primary']) ?>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>

<?php

$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/clockpicker/clockpicker.min.js', ['depends' => 'yii\web\JqueryAsset']);


?>

<script>
    var  selectData=new Array();
    var  timeData=new Array();
    timeData=<?=json_encode($times)?>;
    selectData=<?=json_encode($subsystems)?>;
    console.log(timeData);
    console.log(selectData);
    //去掉数组中选定的值
    function array_remove(array,type,element){
        var result=[];
        for(var key in array){
            if(type=="key"){
                if(key!=element)
                result[key]=array[key];
            }
            if(type="value"){
                if(array[key]!=element)
                    result[key]=array[key];
            }
        }
        return result;
    }

    //第三位参数为类型  判断是 key还是value
    function in_array(array,type,element){
        for(var i in array){
            if(type=='value') {
                if (array[i] == element)
                    return 1;
            }
            if(type=='key') {
                if (i == element)
                    return 1;
            }
        }
        return 0;
    }
    //checkbox操作
    window.select_=function(data){
        var checked=$(data).prop("checked");
        if(checked==true){
            //添加
            selectData.push(parseInt($(data).val()));
            console.log(selectData);
        }
        if(checked==false){
            //去掉
            selectData=array_remove(selectData,'value',$(data).val());

            console.log(selectData);
        }
    };
	window.onload = function() {
        showTime();
		//提交时 保存配置信息
		$("#_submit").click(function(){
			//时间间隔
			var time_interval=$("#time_interval").val();
            var systems=[];
			//选取的子系统
			$("#systems").find($("input:checkbox:checked")).each(function () {
                 systems.push($(this).val());
			});
            //提交信息处理
            $.ajax({
                type: "POST",
                url: "/page-loop/crud/save",
                data: {
                    id:0,
                    name:'defaultName',
                    time_data: timeData,
                    selectData:selectData
                },
                success: function (msg) {
                    console.log('msg');
                    //如果处理成功跳转到配置页面
                    var url="/page-loop/crud";
                    window.location.href=url;
                }
            });
		});

        initTable('datatable_others','oTable_others','tool_others',0,'point');
        /*************************************table start************************************/
        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable,tool,protocol_id,type) {
            /* TABLETOOLS */
            table = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'" +
                "<'col-xs-12 col-sm-2'f>" +
                "<'col-xs-12 col-sm-4 "+tool+"'>" +
                "<'col-sm-6 col-xs-4 hidden-xs'TC>r>" +
                "t" +
                "<'dt-toolbar-footer'" +
                "<'col-sm-6 col-xs-12 hidden-xs'li>" +
                "<'col-sm-6 col-xs-12'p>" +
                ">",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        //"csv",
                        "xls",
                        {
                            "sExtends": "pdf",
                            "name": "测试",
                            "sTitle": "点位管理",
                            "sPdfMessage": "pdf_test",
                            "sPdfSize": "letter"
                        }
                        ,
                        {
                            "sExtends": "print",
                            "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                        }
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,10,15, 16, 20, 25, 30, -1], [5,10,15, 16, 20, 25, 30, "All"]],
                "iDisplayLength": 10,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "ajax": {
                    "url": "/page-loop/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if(in_array(selectData,"value",sData)==1)
                            $(nTd).html("<input type='checkbox' name='checkList' onclick='javascript:select_(this)' checked='checked' value='" + sData + "' >");
                            else
                            $(nTd).html("<input type='checkbox' name='checkList' onclick='javascript:select_(this)' value='" + sData + "' >");
                        }
                    },
                    {"mDataProp": "id"},
                    {"mDataProp": "cn"}
//                    ,
//                    {
//                        "mDataProp": "id",
//                        "bSortable": false
//                    }
                ],
                "fnInitComplete": function (oSettings, json) {

                },
                "order": [1, 'asc']
            });
            /* END TABLETOOLS */
            return table;
        }

        //点击checkbox时  用selectData记录选取的值
        $("#checkAll_others").click(function () {
            var checkAll=$("#checkAll_others").prop("checked");
            alert(checkAll);
            $("#datatable_others").find($("input[name='checkList']")).each(function () {
                var checkValue=parseInt($(this).val());
                //全选
                if(checkAll==true) {
                    $(this).prop("checked", true);
                    if(in_array(selectData,'value',checkValue)==0){
                        selectData.push(checkValue);
                    }
                }
                //全取消
                if(checkAll==false) {
                    $(this).prop("checked", false);
                    //如果取消的值在数组中则去掉
                    if (in_array(selectData, 'value', checkValue) == 1) {
                        selectData = array_remove(selectData, 'value', checkValue);
                    }
                }
            });
        });


        /*************************************table end************************************/


        /*************************************time start************************************/
        //时间初始化
        $('.clockpicker').clockpicker({
            placement: 'top',
            donetext: 'Done'
        });
        //时间添加
        $("#add").click(function(){
            var start_time=$("#start_time").val();
            var end_time=$("#end_time").val();
            var is_legal=true;
                        //比较开始时间结束时间是否合法
            if(start_time=='')      alert("开始时间为空");
            if(start_time=='')      alert("结束时间为空");
            if(end_time<start_time) alert('结束时间不合法');
            //比较时间区间是否已经存在
            if(timeData.length!=0){
                for(var i in timeData){
                    if(start_time==timeData[i]['start_time']&&end_time==timeData[i]['end_time'])
                    {alert("时间区间已经存在");is_legal=false; }
                    if(start_time<timeData[i]['start_time']&&end_time>timeData[i]['start_time'])
                    {alert("时间区间重复");is_legal=false; }
                    if(start_time<timeData[i]['end_time']&&end_time>timeData[i]['end_time'])
                    {alert("时间区间重复");is_legal=false;}
                }
            }
            //时间合法就把时间加入时间集合内  并生成时间标签展示
           if(is_legal){
                var time={
                    'start_time':start_time,
                    'end_time':end_time
                };
                console.log(time);
                timeData.push(time);
                console.log(timeData);
                showTime();
            }

        });
        //时间删除
        $("#delete").click(function(){
            var start_time=$("#start_time").val();
            var end_time=$("#end_time").val();
            var data=new Array();
            //比较时间区间是否已经存在
            if(timeData.length!=0){
                for(var i in timeData){
                    if(start_time==timeData[i]['start_time']&&end_time==timeData[i]['end_time']) {}
                    else{data.puhs(timeData[i]);}
                }
                timeData=data;
            }
            showTime();
        });
        //时间标签展示  先清空时间标签 在重新绘制
        function showTime(){
            //清空时间标签
                $("#tem_content").remove();
            var timeHtml='<div id="tem_content">';
            if(timeData.length!=0)
            {
                for(var key in timeData){
                    var value=timeData[key]['start_time']+"-"+timeData[key]['end_time'];
                    timeHtml+='<button class="btn btn-primary time-button" style="float: left;margin-right: 5px">'+value+'</button>';
                }
                timeHtml+='</div>';
            }
            $("#timecontent").append(timeHtml);
            //按钮点击显示按钮时间
            $(".time-button").click(function(){
                var Stime=$(this).text();
                var times=Stime.split("-");
                $("#start_time").val(times[0]);
                $("#end_time").val(times[1]);
            })
        }
        /*************************************time end************************************/
    }
</script>