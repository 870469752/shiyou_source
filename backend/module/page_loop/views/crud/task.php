<?php

use common\library\MyHtml;
use backend\assets\TableAsset;

use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
//echo '<pre>';
//print_r($data);
//die;
TableAsset::register ( $this );
$this->title = $title;
?>

<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                    <div class="widget-toolbar smart-form" data-toggle="buttons">

                        <button class="btn btn-xs btn-primary" id="submit_button"  >
                            提交
                        </button>

                    </div>
                    <div class="widget-toolbar smart-form">

                        <label class="input"> <i class="icon-append fa fa-question-circle"></i>
                            <input type="text" name="time" id="time" placeholder="Time">
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Some helpful information</b>
                        </label>

                    </div>
                </header>
                <div>
                    <?php $form = MyActiveForm::begin(['method' => 'post','id'=>'testform']) ?>

                    <?php MyActiveForm::end(); ?>
                    <table id="datatable" class="table table-striped table-bordered table-hover" style="width:100%; text-align: center">
                        <thead>
                        <tr>
                            <th data-hide="phone" style="width: 12.5%;text-align: center">任务</th>
                            <th data-hide="phone" style="width: 12.5%;text-align: center">周日</br><?=$time[0]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周一</br><?=$time[1]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周二</br><?=$time[2]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周三</br><?=$time[3]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周四</br><?=$time[4]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周五</br><?=$time[5]?></th>
                            <th data-class="expand" style="width: 12.5%;text-align: center">周六</br><?=$time[6]?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($data as $k => $v){?>
                            <tr>
                                <td><?=$k?></td>
                                <td><?=$v[0]?></td>
                                <td><?=$v[1]?></td>
                                <td><?=$v[2]?></td>
                                <td><?=$v[3]?></td>
                                <td><?=$v[4]?></td>
                                <td><?=$v[5]?></td>
                                <td><?=$v[6]?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>


            </div>
        </article>
    </div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


?>

<script>
    window.onload = function() {

        $(document).ready(function() {
            //初始化时间选择框
            timeTypeChange('day', $('#time'));
            $("#time").val('<?=$selected_day?>');
            $("#submit_button").click(function(){
                $("#testform").append($("#time"));
                $("#testform").submit();
            })
            /* TABLETOOLS */
            $('#datatable').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4'><'col-sm-6 col-xs-6 hidden-xs'T>r>"+"t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        "xls",
                        {
                            "sExtends": "print",
                            "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                        }
                    ],
                    "sSwfPath": "../../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth" : true,
                "lengthMenu": [[15,16,20,25,30, -1], [15,16,20,25,30, "All"]],
                "iDisplayLength":15,
                "language": {
                    "sProcessing": "处理中...",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": false,

            });
            /* END TABLETOOLS */
        });


    }

</script>
