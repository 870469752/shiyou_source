<?php

namespace backend\module\page_loop\controllers;

use backend\models\LoopConfig;
use backend\models\LoopRecord;
use backend\models\ScheduleTask;
use backend\models\SubSystem;
use common\library\MyFunc;
use Yii;
use backend\models\search\PicturesCategorySearch;
use backend\controllers\MyController;
use yii\filters\VerbFilter;
use backend\models\SubSystemManage;
use backend\models\ScheduleTaskLog;
use common\models\User;
use common\library\Ssp;
/**
 * BatchCrudController implements the CRUD actions for PicturesCategory model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * 页面轮巡配置
     * @return mixed
     */
    public function actionIndex()
    {
//        $user_id=Yii::$app->user->id;
//        $data_=Yii::$app->redis->get("TASK_INFO65");
//        $data_=msgpack_unpack($data_);
//        echo '<pre>';
//        print_r($data_);
//        die;
        $model=LoopConfig::findModel(0);
        $times=json_decode($model->times,1)['data'];
        $subsystems=json_decode($model->subsystems,1)['data'];
        return $this->render('index',[
                'times'=>$times,
                'subsystems'=>$subsystems

        ]);
    }
    public function actionTask(){

        $param=Yii::$app->request->post();

        //默认时间为当天
        $selected_day=isset($param['time'])?strtotime($param['time']):time();
        $userInfo=User::find()->select(["id","username"])->asArray()->all();
        $userInfo=MyFunc::map($userInfo,"id","username");
        $statu=[
            1=>"未开始",2=>"可开始",3=>"正在进行中",4=>"已完成",5=>"未完成"
        ];
        //得到选择时间是周几
        $week=date('w',$selected_day);
        //得到选择时间的    周日  周六
        $sunDay=date('Y-m-d',strtotime("-".$week." day",$selected_day)).' 00:00:00';
        $saturDay=date('Y-m-d',strtotime("+".(6-$week)." day",$selected_day)).' 23:59:59';
        $data=ScheduleTaskLog::getTaskByTime($sunDay,$saturDay);

        //如果这段时间内y有数据
        if(!empty($data)) {
            $init_week = [0 => null, 1 => null, 2 => null, 3 => null, 4 => null, 5 => null, 6 => null];
            $ScheduleTask_id=[];
            foreach ($data as $key => $value) {
                $ScheduleTask_id[]=$value['schedule_task_id'];
            }
            $ScheduleTask_info=ScheduleTask::find()->where(['id'=>$ScheduleTask_id])->asArray()->all();
            $ScheduleTask_info=MyFunc::_map($ScheduleTask_info,'id','name');
            foreach ($data as $key => $value) {
                $start_time = substr($value['start_timestamp'], 11, 8);
                $end_time = substr($value['end_timestamp'], 11, 8);
                $time = $ScheduleTask_info[$value['schedule_task_id']] . '-' . $start_time . '-' . $end_time;
                $week_data[$time] = $init_week;
            }

            foreach ($data as $key => $value) {
                $task_day = substr($value['start_timestamp'], 0, 10);
                //得到任务是在周几
                $task_week = date('w', strtotime($task_day));
                $start_time = substr($value['start_timestamp'], 11, 8);
                $end_time = substr($value['end_timestamp'], 11, 8);
                $time = $ScheduleTask_info[$value['schedule_task_id']] . '-' . $start_time . '-' . $end_time;


                        $performer=$userInfo[$value['performer']];
                if($value['status']==1)
                    $week_data[$time][$task_week]=$statu[1];
                else $week_data[$time][$task_week] =$performer.' '.$statu[$value['status']];
            }
        }
        //如果无数据
        else $week_data=[];

        for($i=0;$i<7;$i++){
            $data_time[$i]=date('Y-m-d',strtotime("+".$i." day",strtotime($sunDay)));
        }
        return $this->render('task', [
            'data' => $week_data,
            'title'=>'周任务统计',
            'time'=>$data_time,
            'selected_day'=>date('Y-m-d',$selected_day)
        ]);
    }

    public function actionShow(){
        //得到页面轮巡完成后 新的$data值
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
//        Yii::$app->cache->set('page_loop_data', []);
//        $page_loop_data=Yii::$app->cache->get('page_loop_data');
        if(empty($page_loop_data)) {
            //查询$data的值 并写入session
            $model=LoopConfig::findModel(0);
            $subsystems=json_decode($model->subsystems,1);
            $times=json_decode($model->times,1)['data'];
            $data=SubSystem::find()
                ->select(['id','name'])
                ->where(['id'=>$subsystems['data']])
                ->indexBy('id')->asArray()->all();
            ksort($data);
            //取得第一个元素键值
            $now=current($data)['id'];
            foreach ($data as $key=>$value) {

                $name=$data[$key]['name'];
                $data[$key]['name']=json_decode($name,1)['data']['zh-cn'];
                unset($data[$key]['id']);
                $data[$key]['config']=false;
            }

//            $data = [
//                61 => [
//                    'name' => '测试页面1',
//                    'config' => true
//                ],
//                65 => [
//                    'name' => '测试页面2',
//                    'config' => false
//                ],
//                68 => [
//                    'name' => '测试页面3',
//                    'config' => false
//                ]
//            ];
            $page_loop_data =[
                'data'=>$data,
                'now'=>$now,
                'time'=>$times
            ];
            Yii::$app->cache->set('page_loop_data', $page_loop_data);
        }

            $id=$page_loop_data['now'];

            //所有的本楼层的子系统
            $sub_system=SubSystem::find()->where(['id'=>$id])->asArray()->all();

            $camera_points=[
                "A45"=>[360,361,362,363],
                "A12"=>[361,362,337,338],
                "A16"=>[339,340,341,346],
                "A29"=>[11635,11636,11637,11639],
                "A34"=>[8985445,8985446,8985447,8985448],
                "A42"=>[8985302,8985293,8985294,8985292]
            ];
            $name=json_decode($sub_system[0]['name'],1)['data']['zh-cn'];
            $points=[];
            foreach($camera_points as $key=>$value){
                    if(strpos($name,$key)===0){
                        $points=$camera_points[$key];
                    }
            }
            $points_string=null;
            foreach($points as $_key=>$id){
                if($points_string==null) $points_string=$id;
                else  $points_string=$points_string.",".$id;
            }


        //发布键名为CameraPoint的 摄像头点位字符串
            Yii::$app->redis->publish("CameraPoint",$points_string);
//        Yii::$app->redis->publish("Alarm:1",1);
            $id=$sub_system[0]['id'];
            $model =SubSystem::findModel($id);

            $location_info=SubsystemManage::findOne($model->location_id);
            $sub_system_data=SubSystem::getSubSystemPoint($sub_system);
            $sub_system_data=json_encode($sub_system_data);
            $base_map='uploads/pic/map1.jpg';

            $user_id=Yii::$app->user->id;
            $user=User::findOne($user_id);
            $control=0;
            /*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/

            return $this->render('page_show', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'id' => $id,
                'sub_system_data'=>$sub_system_data,
                'page_loop_data'=>$page_loop_data,
                'camera'=>$points_string
            ]);
    }
    public function actionAjaxUpdateData(){
        $param=Yii::$app->request->post();
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
        $backward=null;
        $now=null;
//        $param['id']=65;
        foreach ($page_loop_data['data'] as $key=>$value) {
            $backward=$now;
            $now=$key;
            if($now==$param['id']){
                $page_loop_data['data'][$key]['config']=true;
                if($param['type']=='backward'){
                    if($backward==null)$page_loop_data['now']=$now;
                    else $page_loop_data['now']=$backward;
                }
                if($param['type']=='forward'){
                    $page_loop_data['now']=$now;
                }
            }
            if($backward==$param['id']){
                if($param['type']=='forward'){
                    $page_loop_data['now']=$now;
                }
            }
        }

        Yii::$app->cache->set('page_loop_data', $page_loop_data);
    }

    public function actionAjaxLoopStatistics(){
        $param=Yii::$app->request->post();
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
        $data=$page_loop_data['data'];
        $loop_state=1;
        foreach ($data as $key=>$value) {
            if($value['config']==false){
                $loop_state=0;
            }
            $value['config']=false;
            //重置config字段组成新的data字段
            $reset_data[]=$value;
        }

        $model=new LoopRecord();
        $load_data['LoopRecord']=[
            'loop_info'=>json_encode([
                'data_type'=>'loop_info',
                'data'=>$page_loop_data['data']
            ]),
            'loop_state'=>$loop_state,
            'start_time'=>$param['result']['start'],
            'end_time'=>$param['result']['end']
        ];
        //重置data值为新状态
        $page_loop_data['data']=$reset_data;
        $model->load($load_data);
        $model->save();
        $result=$model->errors;
        if(!empty($result)) {
            echo '<pre>';
            print_r($load_data);
            print_r($result);
            die;
        }
        //把重置状态的page_loop_data写入cache
        Yii::$app->cache->set('page_loop_data', $page_loop_data);
    }

    public function actionSave(){
        $param=Yii::$app->request->post();
        $model=LoopConfig::findModel($param['id']);

        $loaddata['LoopConfig']=[
            'name'=>json_encode([
                'data_type'=>'description',
                'data'=>[
                    'zh-cn'=>$param['name'],
                    'en-us'=>$param['name']
                ]
            ]),
            'times'=>json_encode([
                    'data_type'=>'times',
                'data'=>$param['time_data']
                    ]),
            'subsystems'=>json_encode([
                'data_type'=>'subsystems',
                'data'=>array_filter($param['selectData'])
            ])
        ];
        $model->load($loaddata);
        $model->save();
        $result=$model->errors;
        if(empty($result)){
            return 'success';
        }
        else return json_encode($result);
    }

    public function actionGetData()
    {

//        if($type == 'point'){
//            $con = " is_shield = false ";
//        }else{
//            $con = " is_shield = true ";
//        }

//        if($protocol_id){
//            $con .= " and protocol_id = $protocol_id ";
//        }else{
//            if($type == 'point'){
//                $con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
//            }
//        }
        //$db = Yii::$app->db;
        //echo "<pre>";print_r($con);die;
        $con="id is not null ";
        $table = 'core.sub_system';
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
//            array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),
//            array( 'db' => 'is_shield',   'dt' => 'is_shield' ,'dj'=>"is_shield" ),
//            array( 'db' => 'unit',   'dt' => 'unit' ,'dj'=>"unit" ),
//            array( 'db' => 'update_timestamp',   'dt' => 'update_timestamp' ,'dj'=>"update_timestamp" ),
//            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
        );
        $result = Ssp::simple_jia( $_POST, $table, $primaryKey, $columns, $con );

        echo json_encode( $result);
    }

    //查询当前用户今天的 任务进度等信息
    public function actionAjaxTaskData(){
        $user_id=Yii::$app->user->id;
        //当前时间
        $now_date = date("Y-m-d H:i:s", time());
        //得到当前用户   当天的计划任务
//        $data_=Yii::$app->cache->get("TodayTask");
        $data_=Yii::$app->redis->get("TASK_INFO:".$user_id);
        $data_=msgpack_unpack($data_);
//        echo '<pre>';
//        print_r($data_);
//        die;
        if(!empty($data_)){

            $today_task=$data_['tasks'];
            $tasks_info=$data_['tasks_name'];

            //处理当天任务的名称
            //状态 完成程度 为数据数组 待完成

            foreach ($today_task as $key=>$value) {
                $actions=$value['action'];
                //确定任务完成进度
                $total_action=0;$complete=0;
                foreach ($actions as $num=>$act) {
                    $total_action++;
                    if($act==true)
                        $complete++;
                }
                //拿到的的都是可执行的先写值为2
                $status=2;
                //确认任务状态
//                if($value['start_timestamp']>$now_date) {
//                    $status = 1;//还未开始
//                    $complete=0;
//                }
//                if($value['start_timestamp']<$now_date && $now_date<$value['end_timestamp']){
                    if($complete==0) $status=2;//可执行但是未开始
                    else $status=3;//可执行并已经开始
//                }
//                if($now_date > $value['end_timestamp']){
//                    if($complete==$total_action)$status=4;//已结束并完成
//                    else $status=5;//已结束但是未完成
//                }
    //            $start_time=substr($value['start_timestamp'],11,8);
    //            $end_time=substr($value['end_timestamp'],11,8);
                $start_time=$value['start_timestamp'];
                $end_time=$value['end_timestamp'];
                $temp_data=[
                    'name'=>$tasks_info[$value['schedule_task_id']],
                    'start_time'=>$start_time,
                    'end_time'=>$end_time,
                    'state'=>$status,
                    'complete_value'=>$complete.'/'.$total_action,
                    'complete_percent'=>($complete==0)?'0%':($complete/$total_action*100).'%'
                ];
                $data[]=$temp_data;
            }
        }else {
//            echo 111;
//            die;
            $data=[];
        }
        return json_encode($data);
    }


    public function actionUpdateTaskInfo(){
        $user_id=Yii::$app->user->id;
        $data=Yii::$app->request->post();
        $init_state=[
            "A12" => false, "A16" => false, "A29" => false,
            "A34" => false, "A42" => false, "A45" => false
        ];
//        $scheduletask_ids=isset($param['data'])?$param['data']:[];
        $scheduletask_ids=$data['task'];
        //得到任务长度
        $num=count($scheduletask_ids);
//        $old_task_info=Yii::$app->cache->get("TASK_INFO");
        $old_task_info=Yii::$app->redis->get("TASK_INFO:".$user_id);
        $old_task_info=msgpack_unpack($old_task_info);
        //新的当前任务信息
        $new_task=ScheduleTaskLog::find()
            ->where(['id'=>$scheduletask_ids])
            ->indexBy('id')
            ->asArray()->all();
        $old_tasks_data=[];
        //更新前记下以前的任务执行程度1/6
        if(!empty($old_task_info)){
            $tasks_old=$old_task_info['tasks'];
            foreach ($tasks_old as $key=>$value) {
                $old_tasks_data[$value['id']]=$value['action'];
            }
        }

        //给当前任务  加上执行程度信息
        foreach ($new_task as $key=>$value ) {
            $tasks[]=$value['schedule_task_id'];
            $new_task[$key]['action']=isset($tasks_old_data[$value['id']])?$tasks_old_data[$value['id']]:$init_state;
        }
        $tasks_info=ScheduleTask::find()->where(['id'=>$tasks])->asArray()->all();
        $tasks_info=MyFunc::_map($tasks_info,'id','name');

        Yii::$app->redis->set("TASK_INFO:".$user_id,msgpack_pack([
            'task_num'=>$num,
            'tasks'=>$new_task,
            'tasks_name'=>$tasks_info
        ]));
        //返回给页面更新 任务信息
        return json_encode([
            'tasks'=>$new_task,
            'task_num'=>$num,
            'tasks_name'=>$tasks_info
        ]);

    }

}
