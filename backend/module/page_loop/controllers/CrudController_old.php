<?php

namespace backend\module\page_loop\controllers;

use backend\models\LoopConfig;
use backend\models\LoopRecord;
use backend\models\SubSystem;
use common\library\MyFunc;
use Yii;
use backend\models\search\PicturesCategorySearch;
use backend\controllers\MyController;
use yii\filters\VerbFilter;
use backend\models\SubSystemManage;
use common\models\User;
use common\library\Ssp;
/**
 * BatchCrudController implements the CRUD actions for PicturesCategory model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * 页面轮巡配置
     * @return mixed
     */
    public function actionIndex()
    {
        $model=LoopConfig::findModel(0);
        $times=json_decode($model->times,1)['data'];
        $subsystems=json_decode($model->subsystems,1)['data'];
        return $this->render('index',[
                'times'=>$times,
                'subsystems'=>$subsystems

        ]);
    }
    public function actionTest(){
        return $this->render('index',[

        ]);
    }

    public function actionShow(){
        //得到页面轮巡完成后 新的$data值
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
//        Yii::$app->cache->set('page_loop_data', []);
//        $page_loop_data=Yii::$app->cache->get('page_loop_data');
        if(empty($page_loop_data)) {
            //查询$data的值 并写入session
            $model=LoopConfig::findModel(0);
            $subsystems=json_decode($model->subsystems,1);
            $times=json_decode($model->times,1)['data'];
            $data=SubSystem::find()
                ->select(['id','name'])
                ->where(['id'=>$subsystems['data']])
                ->indexBy('id')->asArray()->all();
            ksort($data);
            //取得第一个元素键值
            $now=current($data)['id'];
            foreach ($data as $key=>$value) {

                $name=$data[$key]['name'];
                $data[$key]['name']=json_decode($name,1)['data']['zh-cn'];
                unset($data[$key]['id']);
                $data[$key]['config']=false;
            }

//            $data = [
//                61 => [
//                    'name' => '测试页面1',
//                    'config' => true
//                ],
//                65 => [
//                    'name' => '测试页面2',
//                    'config' => false
//                ],
//                68 => [
//                    'name' => '测试页面3',
//                    'config' => false
//                ]
//            ];
            $page_loop_data =[
                'data'=>$data,
                'now'=>$now,
                'time'=>$times
            ];
            Yii::$app->cache->set('page_loop_data', $page_loop_data);
        }

        $id=$page_loop_data['now'];

        //所有的本楼层的子系统
        $sub_system=SubSystem::find()->where(['id'=>$id])->asArray()->all();

            $id=$sub_system[0]['id'];
            $model =SubSystem::findModel($id);
             Yii::$app->redis->publish("Alarm:1",1);
            $location_info=SubsystemManage::findOne($model->location_id);
            $sub_system_data=SubSystem::getSubSystemPoint($sub_system);
            $sub_system_data=json_encode($sub_system_data);
            $base_map='uploads/pic/map1.jpg';

            $user_id=Yii::$app->user->id;
            $user=User::findOne($user_id);
            $control=0;
            /*if(strlen($user->is_control)!=0)
                $control=$user->is_control;
            else $control=0;*/

            return $this->render('page_show', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'id' => $id,
                'sub_system_data'=>$sub_system_data,
                'page_loop_data'=>$page_loop_data
            ]);
    }
    public function actionAjaxUpdateData(){
        $param=Yii::$app->request->post();
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
        $backward=null;
        $now=null;
//        $param['id']=65;
        foreach ($page_loop_data['data'] as $key=>$value) {
            $backward=$now;
            $now=$key;
            if($now==$param['id']){
                $page_loop_data['data'][$key]['config']=true;
                if($param['type']=='backward'){
                    if($backward==null)$page_loop_data['now']=$now;
                    else $page_loop_data['now']=$backward;
                }
                if($param['type']=='forward'){
                    $page_loop_data['now']=$now;
                }
            }
            if($backward==$param['id']){
                if($param['type']=='forward'){
                    $page_loop_data['now']=$now;
                }
            }
        }

        Yii::$app->cache->set('page_loop_data', $page_loop_data);
    }

    public function actionAjaxLoopStatistics(){
        $param=Yii::$app->request->post();
        $page_loop_data=Yii::$app->cache->get('page_loop_data');
//        echo '<pre>';
//        print_r($param);
//        print_r($page_loop_data);
//        die;
        $model=new LoopRecord();
        $load_data['LoopRecord']=[
            'loop_info'=>json_encode([
                'data_type'=>'loop_info',
                'data'=>$page_loop_data['data']
            ]),
            'loop_state'=>0,
            'start_time'=>$param['result']['start'],
            'end_time'=>$param['result']['end']
        ];
        $model->load($load_data);
        $model->save();
        $result=$model->errors;
        if(!empty($result)) {
            echo '<pre>';
            print_r($load_data);
            print_r($result);
            die;
        }
        //需要重新还原page_loop_data为false

        Yii::$app->cache->set('page_loop_data', $page_loop_data);
    }

    public function actionSave(){
        $param=Yii::$app->request->post();
        $model=LoopConfig::findModel($param['id']);
        $loaddata['LoopConfig']=[
            'name'=>json_encode([
                'data_type'=>'description',
                'data'=>[
                    'zh-cn'=>$param['name'],
                    'en-us'=>$param['name']
                ]
            ]),
            'times'=>json_encode([
                    'data_type'=>'times',
                'data'=>$param['time_data']
                    ]),
            'subsystems'=>json_encode([
                'data_type'=>'subsystems',
                'data'=>$param['selectData']
            ])
        ];
        $model->load($loaddata);
        $model->save();
        $result=$model->errors;
        if(empty($result)){
            return 'success';
        }
        else return json_encode($result);
    }

    public function actionGetData()
    {
        $protocol_id = Yii::$app->request->get('protocol_id');
        $type = Yii::$app->request->get('type');
//        if($type == 'point'){
//            $con = " is_shield = false ";
//        }else{
//            $con = " is_shield = true ";
//        }

//        if($protocol_id){
//            $con .= " and protocol_id = $protocol_id ";
//        }else{
//            if($type == 'point'){
//                $con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
//            }
//        }
        //$db = Yii::$app->db;
        //echo "<pre>";print_r($con);die;
        $con="id is not null ";
        $table = 'core.sub_system';
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
//            array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),
//            array( 'db' => 'is_shield',   'dt' => 'is_shield' ,'dj'=>"is_shield" ),
//            array( 'db' => 'unit',   'dt' => 'unit' ,'dj'=>"unit" ),
//            array( 'db' => 'update_timestamp',   'dt' => 'update_timestamp' ,'dj'=>"update_timestamp" ),
//            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
        );
        $result = Ssp::simple_jia( $_POST, $table, $primaryKey, $columns, $con );

        echo json_encode( $result);
    }

}
