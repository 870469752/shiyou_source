<?php

namespace backend\module\page_loop\controllers;

use backend\controllers\MyController;
use backend\models\Pictures;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        $pictures = Pictures::find()->all();
        return $this->render('index',[
            'pictures' => $pictures
        ]);
    }
}
