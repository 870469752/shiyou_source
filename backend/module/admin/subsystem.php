<?php

namespace backend\module\admin;

class subsystem extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\admin\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
