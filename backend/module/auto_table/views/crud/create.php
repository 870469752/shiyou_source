<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CustomReport $model
 */
$this->title = Yii::t('auto_table', 'Create {modelClass}', [
  'modelClass' => 'Auto Table',
]);
?>
    <?= $this->render('_form', [
    'model' => $model,
    'unit' => $unit,
    'category_tree' => $category_tree,
    'points' => $points,
    'formula' => $formula
    ]) ?>
