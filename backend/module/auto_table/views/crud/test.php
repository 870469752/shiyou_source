<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;

?>
<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset {
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }

    table td{width: 12%}
    table tr{border:dotted ; border-width:1px 0px 0px 0px;}
    table{border:dotted ; border-width:1px 1px 1px 1px;}

</style>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
                        <span class="widget-icon">
                            <i class="fa fa-edit"></i>
                        </span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
                        <?= Html::hiddenInput('AutoTable[table_task]', '', ['id' => 'table_task'])?>
                        <div id="oneset">
                            <fieldset>
                                <section>
                                    <div class="form-group field-scheduledtask-export_title has-success">
                                        <label class="label">报表标题</label>
                                        <input type="text" id="fieldset_title" class="form-control" name="fieldset_title" value="">
                                    </div>
                                </section>
                            </fieldset>
                            <!--<fieldset  id="Backup_date">
                                <label class="label"><?/*=Yii::t('app', 'Backup Date')*/?> :</label>
                                <div id = '_cron'  class = 'time_mode'>
                                    <span id='selector'></span>
                                </div>
                            </fieldset>-->
                            <!--报表类型-->
                            <fieldset>
                                <label class="label">报表类型</label>
                                <label class="radio state-success"><input type="radio" id="radio_day" name="radio_date" value="day" checked><i></i>日报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_week" name="radio_date" value="week"><i></i>周报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_month" name="radio_date" value="month"><i></i>月报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_year" name="radio_date" value="year"><i></i>年报表</label>
                                <label class="radio state-success"  style="display: none"><input type="radio" id="radio_interval" name="radio_date" value="interval"><i></i>间隔类报表</label>
                            </fieldset>
                            <!--开始时间-->
                            <fieldset style="display: none">
                                <label class="label">开始时间</label>
                                <label class="select state-success">
                                    <select id="select_backup_year" style="display: none">
                                        <option value="1" selected>1月</option>
                                        <?php for($i=2;$i<13;$i++){?>
                                            <option value="<?=$i?>"><?=$i?>月</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                                <label class="select state-success">
                                    <select id="select_backup_month" style="display: none">
                                        <option value="1" selected>1号</option>
                                        <?php for($i=2;$i<28;$i++){?>
                                            <option value="<?=$i?>"><?=$i?>号</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                                <label class="select state-success">
                                    <select id="select_backup_week" style="display: none">
                                        <option value="7" selected>周日</option>
                                        <option value="1" selected>周一</option>
                                        <option value="2" selected>周二</option>
                                        <option value="3" selected>周三</option>
                                        <option value="4" selected>周四</option>
                                        <option value="5" selected>周五</option>
                                        <option value="6" selected>周六</option>
                                    </select><i></i>
                                </label>
                                <label class="select state-success">
                                    <select id="select_backup_day">
                                        <option value="0" selected>0点</option>
                                        <?php  for($i=1;$i<24;$i++){ ?>
                                            <option value="<?=$i?>"><?=$i?>点</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                                </label>
                            </fieldset>
                            <fieldset id="fieldset_day">
                                <label class="label">天范围</label>
                                <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="all" checked><i></i>全部</label>
                                <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="work_day"><i></i>工作日</label>
                                <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="week_end"><i></i>周末</label>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_week">
                                <label class="label">天范围</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="7" checked><i></i>星期日</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="1" checked><i></i>星期一</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="2" checked><i></i>星期二</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="3" checked><i></i>星期三</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="4" checked><i></i>星期四</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="5" checked><i></i>星期五</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="6" checked><i></i>星期六</label>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_year">
                                <label class="label">月范围</label>
                                <?php for($i=1;$i<13;$i++){?>
                                    <label class="checkbox state-success"><input type="checkbox" name="fieldset_year_checkbox" value="<?=$i?>" checked><i></i><?=$i?>月</label>
                                <?php }?>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_month">
                                <label class="label">天范围</label>
                                <?php for($i=1;$i<32;$i++){?>
                                    <label class="checkbox state-success"><input type="checkbox" name="fieldset_month_checkbox" value="<?=$i?>" checked><i></i><?=$i?>号</label>
                                <?php }?>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_interval">
                                <div id = '_static'>
                                    <label class="label">间隔</label>
                                    <a href="#" id="static" data-type="timec" data-pk="1" data-title="Please, fill time" class="editable editable-click">empty</a>
                                    <input type="hidden" id="static_time" name="ScheduledTask[statistic_time]" value="Empty">
                                </div>
                            </fieldset>
                            <!--数据生成时间间隔-->
                            <fieldset>
                                <label class="label">时范围</label>
                                <label class="radio state-success" id="every_hour"><input type="radio"  name="data_interval_time" value="hour" checked><i></i>每小时</label>
                                <label class="radio state-success" id="every_auto" style="width: auto"><input type="radio" name="data_interval_time" value="auto"><i></i>自定义</label>
                                <label class="select state-success" id="lable_select_data_interval" style="margin-left:15px;display: none">
                                    <select id="select_data_interval" >
                                        <option value='hour'>小时区间</option>
                                    </select><i></i>
                                </label>
                            </fieldset>

                            <fieldset  style="display: none" id="fieldset_day_interval">
                                <label class="label">时间区间</label>
                                <div class="col-md-12">
                                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                        <div class="panel panel-primary" style="padding: 15px;display: block">
                                            <div class="panel-body" ng-transclude>
                                                <table id = "action" class="table table-bordered table-condensed ng-scope">
                                                    <thead>
                                                    <input type="hidden" id="hour_time_mode" style="width: 800px"/>
                                                    <tr style="font-weight: bold">
                                                        <th style = "10%"><input type = "checkbox" id = "set_checked" onclick="check_all()"></th>
                                                        <th style="width:30%">名称</th>
                                                        <th style="width:30%">开始时间</th>
                                                        <th style="width:30%">结束时间</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <footer style="background-color: lightgoldenrodyellow">
                                                    <button id = "_delete" type="button" onclick="deletetimearea()" class="btn btn-primary">Delete</button>
                                                    <button id = "_add" type="button" onclick="addtimearea()" class="btn btn-default" data-toggle="modal" data-target="#myModal">Add row</button>
                                                </footer>
                                            </div>
                                        </div>
                                    </panel>
                                </div>
                            </fieldset>
                            <div class="form-actions">
                                <button type="button" id="" class="create btn-success" onclick="displayset('oneset','twoset')">下一步</button>
                            </div>
                        </div>

                        <div id = "twoset" style="display: none">
                            <fieldset>
                                <label class="label">数据类型</label>
                                <label class="radio state-success"><input type="radio" id="radio_point" name="radio_point_type" value="point" checked><i></i>点位</label>
                                <label class="radio state-success"><input type="radio" id="radio_equiment" name="radio_point_type" value="equiment"><i></i>自定义</label>
                            </fieldset>
                            <fieldset>
                                <label class="label">点位选择</label>
                                <?=Html::dropDownList('dropDownList_point', null, $point_info,  ['id' => 'point', 'class' => 'select2 point_info', 'multiple' => 'multiple', 'placeholder' => '点位选择'])?>
                            </fieldset>
                            <fieldset style="display: none">
                                <label class="label">点位选择</label>
                                <?php foreach($point_info as $k=>$value) {?>
                                    <label class="checkbox state-success"><input type="checkbox" name="point" value="<?=$k.'-'.$value;?>" checked><i></i><?=$value;?></label>
                                <?php } ?>
                            </fieldset>
                            <fieldset id = "point_static" >
                                <label class="label">点位统计类型及命名</label>
                                <label class="label">
                                    <table style="width: 100%;text-align: center">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </label>
                            </fieldset>

                            <div class="form-actions">
                                <button type="button" id="" class="create btn-success" onclick="displayset('twoset','oneset')">上一步</button>&nbsp;&nbsp;
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>


</section>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);

?>
<script>
    window.onload = function() {

    }

</script>                                                                                                                                                                                                                                          