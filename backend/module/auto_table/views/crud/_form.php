<?php

use yii\helpers\Html;
use common\library\MyHtml;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;

?>
<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset {
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }

    table td{width: 12%}
    table tr{border:dotted ; border-width:1px 0px 0px 0px;}
    table{border:dotted ; border-width:1px 1px 1px 1px;}

</style>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
                        <span class="widget-icon">
                            <i class="fa fa-edit"></i>
                        </span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
                        <?= Html::hiddenInput('AutoTable[table_task]', '', ['id' => 'table_task'])?>
                        <div id="oneset">
                            <fieldset>
                                <section>
                                    <div class="form-group field-scheduledtask-export_title has-success">
                                        <label class="label">报表标题</label>
                                        <input type="text" id="fieldset_title" class="form-control" name="fieldset_title" value="">
                                    </div>
                                </section>
                            </fieldset>
                            <!--报表类型-->
                            <fieldset>
                                <label class="label">报表类型</label>
                                <label class="radio state-success"><input type="radio" id="radio_day" name="radio_date" value="day" checked><i></i>日报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_week" name="radio_date" value="week"><i></i>周报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_month" name="radio_date" value="month"><i></i>月报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_year" name="radio_date" value="year"><i></i>年报表</label>
                            </fieldset>

                            <fieldset style="display: none" id="fieldset_week">
                                <label class="label">天范围</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="7" checked><i></i>星期日</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="1" checked><i></i>星期一</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="2" checked><i></i>星期二</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="3" checked><i></i>星期三</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="4" checked><i></i>星期四</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="5" checked><i></i>星期五</label>
                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="6" checked><i></i>星期六</label>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_year">
                                <label class="label">月范围</label>
                                <?php for($i=1;$i<13;$i++){?>
                                    <label class="checkbox state-success"><input type="checkbox" name="fieldset_year_checkbox" value="<?=$i?>" checked><i></i><?=$i?>月</label>
                                <?php }?>
                            </fieldset>
                            <fieldset style="display: none" id="fieldset_month">
                                <label class="label">天范围</label>
                                <?php for($i=1;$i<32;$i++){?>
                                    <label class="checkbox state-success"><input type="checkbox" name="fieldset_month_checkbox" value="<?=$i?>" checked><i></i><?=$i?>号</label>
                                <?php }?>
                            </fieldset>
                            <!--开始时间-->
                            <fieldset id="fieldset_begintime">
                                <?= Html::hiddenInput('CalculatePoint[formula]',$model->isNewRecord ? '' : $model->formula , ['id' => '_calculate'])?>
                                <label class="label">开始时间</label>
                                <label class="select state-success">
                                    <select id="select_backup_day">
                                        <option value="0" selected>0点</option>
                                        <?php  for($i=1;$i<24;$i++){ ?>
                                            <option value="<?=$i?>"><?=$i?>点</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                                </label>
                            </fieldset>
                            <!--数据生成时间间隔-->
                            <fieldset id="fieldset_interval">
                                <label class="label">时范围</label>
                                <div id="data_interval_time">
                                    <?=Html::dropDownList('data_interval_time', '', [
                                        '无' => '无',
                                        'hour' => '每小时',
                                        'auto' => '自定义'
                                    ], ['class' => 'select2', 'placeholder' => ''])?>
                                </div>

                                <label class="select state-success" id="lable_select_data_interval" style="margin-left:15px;display: none">
                                    <select id="select_data_interval" >
                                        <option value='hour'>小时区间</option>
                                    </select><i></i>
                                </label>
                            </fieldset>

                            <fieldset  style="display: none" id="fieldset_day_interval">
                                <label class="label">时间区间</label>
                                <div class="col-md-12">
                                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                        <div class="panel panel-primary" style="padding: 15px;display: block">
                                            <div class="panel-body" ng-transclude>
                                                <table id = "action" class="table table-bordered table-condensed ng-scope">
                                                    <thead>
                                                    <input type="hidden" id="hour_time_mode" style="width: 800px"/>
                                                    <tr style="font-weight: bold">
                                                        <th style = "10%"><input type = "checkbox" id = "interval_set_checked"></th>
                                                        <th style="width:30%">名称</th>
                                                        <th style="width:30%">开始时间</th>
                                                        <th style="width:30%">结束时间</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <footer style="background-color: lightgoldenrodyellow">
                                                    <button id = "_delete1" type="button"  class="btn btn-primary">Delete</button>
                                                    <button id = "_add" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Add row</button>
                                                </footer>
                                            </div>
                                        </div>
                                    </panel>
                                </div>
                            </fieldset>
                            <div class="form-actions">
                                <button type="button" id="" class="create btn-success" onclick="displayset('oneset','twoset')">下一步</button>
                            </div>
                        </div>

                        <div id = "twoset" style="display: none">
                            <fieldset>
                                <div class="col-md-12">
                                    <label class="label">公式名称</label>
                                    <input type="text" id="formula_title" class="form-control" name="formula_title" value="">
                                </div>

                                <div class="col-md-12" style="margin-top: 10px">
                                    <label class="label">公式编辑</label>
                                </div>

                                <div class="input-group" style="margin-top: 10px" id = '_formula'>
                                    <div class = 'select2-container select2-container-multi' style = 'margin-top:2px;width: 100%;'>
                                        <ul class="select2-choices">
                                            <li class="select2-search-choice" data-status = '0' data-type = 'plus' style = 'background-color:#AFEDF5;border:none;'>
                                                <div><i class="fa fa-plus"></i></div>
                                                <a style = 'display:none' href="#" class="select2-search-choice-close _delete" tabindex="-1"></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="input-group-btn" style="margin-top: 10px" id = '_del'>
                                        <button class="btn create btn-primary" type="button">
                                            <i class="fa fa-mail-reply-all"></i> Clear
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px">
                                    <label class="label">单位选择</label>
                                    <?=Html::dropDownList('test', '', $unit, ['class' => 'select2', 'placeholder' => '请选择点位'])?>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px">
                                    <label class="label">最大过滤值</label>
                                    <input type="text" id="formula_max_fie" class="form-control" name="formula_max_fie" value="0">
                                </div>
                                <div class="col-md-12" style="margin-top: 10px">
                                    <label class="label">最小过滤值</label>
                                    <input type="text" id="formula_min_fie" class="form-control" name="formula_min_fie" value="0">
                                </div>
                                <div class="col-md-12" style="margin-top: 10px" id = 'add_point'>
                                    <button class="btn create btn-primary" type="button">
                                        添加点位
                                    </button>
                                </div>
                            </fieldset>


                            <fieldset id="formula_static">
                                <div class="col-md-12">
                                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                        <div class="panel panel-primary" style="padding: 15px;display: block">
                                            <div class="panel-body" ng-transclude>
                                                <table id = "formula" class="table table-bordered table-condensed ng-scope">
                                                    <thead>
                                                    <tr style="font-weight: bold">
                                                        <th style = "10%"><input type = "checkbox" id = "formula_set_checked" ></th>
                                                        <th style="width:18%">公式名称</th>
                                                        <th style="width:18%">计算公式</th>
                                                        <th style="width:18%">单位</th>
                                                        <th style="width:18%">最大过滤值</th>
                                                        <th style="width:18%">最小过滤值</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <footer style="background-color: lightgoldenrodyellow">
                                                    <button id = "_delete2" type="button" class="btn btn-primary">Delete</button>
                                                </footer>
                                            </div>
                                        </div>
                                    </panel>
                                </div>
                            </fieldset>

                            <div class="form-actions">
                                <button type="button" id="" class="create btn-success" onclick="displayset('twoset','oneset')">上一步</button>&nbsp;&nbsp;
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>

    <!-- Modal -->
    <div class="modal fade" style="padding-top: 200px" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 400px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel">添加</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" >时间段名
                                <input type="text" id = "time_name" style="width: 50%">
                            </div>
                            <div class="form-group">开始时间
                                <select id="select_begin_time" style="width: 50%">
                                    <option value="null" selected>无</option>
                                </select>
                            </div>
                            <div class="form-group">结束时间
                                <select id="select_end_time" style="width: 50%">
                                    <option value="null" selected>无</option>

                                </select>
                            </div>
                            <div id = "error_message" style = "display:none" class = "form-group">
                                <i class="fa-fw fa fa-warning"></i>
                                <span style = "color:red" id = "_message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id = "add_action" type="button" class="btn btn-primary">
                        Add
                    </button>
                    <button type="button" id = "_cancel" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- 自定义公式模板 -->
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_ate'  style="display: none;">
        <div class="arrow"></div>
        <h3 class="popover-title">选择点位</h3>
        <div class="popover-content">
            <div class="editableform-loading" style="display: none;"></div>
            <form class="form-inline editableform" style="">
                <div class="control-group form-group"><div>
                        <div class="editable-input">
                            <div class="editable-address">
                                <?=Html::dropDownList('gs', '', [
                                    '' => '',
                                    '_fh' => '运算符',
                                    '_hs' => '函数',
                                    '_sz' => '数字',
                                    '_dw' => '点位',
                                    '_mb' => '模板',
                                ], ['class' => 'select2', 'placeholder' => '请选择公式组成元素', 'id' => '_ele'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_fh' style = 'display:none'>
                                <?=Html::dropDownList('_fh', '', [
                                    '' => '',
                                    '＋' => '＋',
                                    '－' => '－',
                                    '*' => '×',
                                    '/' => '÷',
                                    '(' => '(',
                                    ')' => ')'
                                ], ['class' => 'select2', 'placeholder' => '请选择运算符号'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_hs' style = 'display:none'>
                                <?=Html::dropDownList('_hs', '', [
                                    '' => '',
                                    'sin(' => 'sin',
                                    'cos(' => 'cos',
                                    'max(' => 'max',
                                    'min(' => 'min',
                                    'sum(' => 'sum',
                                    'avg(' => 'avg'
                                ], ['class' => 'select2', 'placeholder' => '请选择函数'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_sz' style = 'display:none'>
                                <input type = 'text' name = '_sz' style = 'width:100%' placeholder = '请填写数字'>
                            </div>
                            <div class="editable-address _calculate" id  = '_dw' style = 'display:none'>
                                <?=Html::dropDownList('_dw', '', $points, ['class' => 'select2', 'placeholder' => '请选择点位'])?>
                            </div>

                            <div class="editable-address _calculate" id = '_error_log' style = '    display:none;color:indianred'></div>
                        </div>
                        <div class="editable-buttons">
                            <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                            <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                        </div></div>
                    <div class="editable-error-block help-block" style="display: none;"></div>
                </div>
            </form>
        </div>
    </div>
    <!-- 自定义公式模板 END -->
</section>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);

?>
<script>
    window.onload = function() {
        var $formula = $('#_calculate').val().split(" ");//自定义公式模板变量
        var $static = $('#static_time').val();
        $("input:radio[name='radio_date']").change(function () {
            var $temp = $("input[name='radio_date']:checked").val();
            $("fieldset[id^='fieldset_']").hide();//隐藏“报表数据范围里的所有选项”
            $("fieldset[id ='fieldset_" + $temp + "']").show();

            $("#lable_select_data_interval").hide();

            if ($temp == 'day') {
                $("#fieldset_begintime").show();
                $("#fieldset_interval").show();
            } else if ($temp == 'week') {
                $("#fieldset_begintime").show();
                $("#fieldset_interval").show();
            } else if ($temp == 'month') {
                $("#fieldset_begintime").show();
                $("#fieldset_interval").show();
            } else if ($temp == 'year') {
                $("#fieldset_begintime").hide();
                $("#fieldset_interval").hide();
            } else {
            }
        })

        $("#data_interval_time").change(function () {
            var $temp1 = $("select[name='data_interval_time']").val();
            if ($temp1 == 'auto') {
                $("#lable_select_day_interval").show();
                $("#fieldset_day_interval").show();
            } else {
                $("#lable_select_data_interval").hide();
                $("#fieldset_day_interval").hide();
            }
        })

        $("#select_begin_time").change(function () {
            var $select_begin_time = parseInt($("#select_begin_time").val());
            var $begin_time=parseInt($('#select_backup_day').val());//获取开始时间值
            $("#select_end_time").empty();
            $("#select_end_time").append("<option value='null'>无</option>");
            for (var $i = $select_begin_time; $i <  $begin_time+24; $i++) {
                if($i>23){
                    var $tmp_i=$i-24;
                    $("#select_end_time").append("<option value='" + $i + "'>次日" + $tmp_i + "点</option>");
                }else{
                    $("#select_end_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                }
            }
        })

        function add_row($timename, $begintime, $endtime) {
            var $arr_two = new Array;
            var $time_num = $("#hour_time_mode").val();
            //时间段排序
            if ($time_num == '') {
                $("#hour_time_mode").prop("value", $timename + "-" + $begintime + "-" + $endtime);
                if($begintime>=24){
                    $begintime='次日'+($begintime-24);
                    $endtime='次日'+($endtime-24);
                }
                if($endtime>24){ $endtime='次日'+($endtime-24);}
                $('#action tbody').append(
                    '<tr ng-repeat="user in users" class="ng-scope" data-time_name="' + $timename + '" data-begin_time="' + $begintime + '"data-end_time="' + $endtime + '">' +
                    '<td><input class = "_check1" type = "checkbox"></td>' +
                    '<td>' + $timename + '</td>' +
                    '<td>' + $begintime + '点</td>' +
                    '<td>' + $endtime + '点</td>' +
                    '</tr>');
            } else {
                $time_num += ";" + $timename + "-" + $begintime + "-" + $endtime;
                var $arr_one = $time_num.split(";");
                for ($i = 0; $i < $arr_one.length; $i++) {
                    $arr_two[$i] = $arr_one[$i].split('-')
                }
                /*$arr_two.sort(function (x, y) {
                    return x[1] - y[1];
                });*/
                var $str = '';
                for ($j = 0; $j < $arr_two.length; $j++) {
                    if ($j == 0) {
                        $str += $arr_two[$j][0] + "-" + $arr_two[$j][1] + "-" + $arr_two[$j][2];
                    } else {
                        $str += ";" + $arr_two[$j][0] + "-" + $arr_two[$j][1] + "-" + $arr_two[$j][2];
                    }
                }
                $("#hour_time_mode").prop("value", $str);

                $('#action tbody').empty();
                for ($i = 0; $i < $arr_two.length; $i++) {
                    if( $arr_two[$i][1]>=24){
                        $arr_two[$i][1]='次日'+($arr_two[$i][1]-24);
                        $arr_two[$i][2]='次日'+($arr_two[$i][2]-24);
                    }
                    if($arr_two[$i][2]>=24){ $arr_two[$i][2]='次日'+($arr_two[$i][2]-24);}
                    $('#action tbody').append(
                        '<tr ng-repeat="user in users" class="ng-scope" data-time_name="' + $arr_two[$i][0] + '" data-begin_time="' + $arr_two[$i][1] + '"data-end_time="' + $arr_two[$i][2] + '">' +
                        '<td><input class = "_check1" type = "checkbox"></td>' +
                        '<td>' + $arr_two[$i][0] + '</td>' +
                        '<td>' + $arr_two[$i][1] + '点</td>' +
                        '<td>' + $arr_two[$i][2] + '点</td>' +
                        '</tr>');
                }
            }
        }

        /*------------------------------------------------------------------------------------------------------------*/

        /************  action table click event  * start ********************/

        $('#add_action').click(function () {                                                                              //弹出框 add点击事件
            var time_name = $('#time_name').val().trim();
            var begin = $('#select_begin_time').val();
            var end = $('#select_end_time').val();

            if (time_name == '') {
                $('#error_message').show();
                $('#_message').text('请填写时间段名!');
                return false;
            }
            if (begin == 'null') {
                $('#error_message').show();
                $('#_message').text('请选择开始时间！');
                return false;
            }
            if (end == 'null') {
                $('#error_message').show();
                $('#_message').text('请选择结束时间!');
                return false;
            }
            add_row(time_name, begin, end);
            my_overload();
            $('#_cancel').trigger('click');
        });

        $('#_add').click(function () {                                                                                    //页面添加row按钮点击事件
            my_overload();
        })

        $('#add_point').click(function(){
            var formula_title=$('#formula_title').val();//公式名称
            var formula = ele2Formula();//公式
            var formula_type = $("select[name='test']").val();//单位
            var formula_type_text = $("#select2-chosen-4").text();//单位
            var formula_max_fie = $('#formula_max_fie').val();//最大过滤值
            var formula_min_fie = $('#formula_min_fie').val();//最小过滤值

            $('#formula tbody').append(
                '<tr ng-repeat="user in users" class="ng-scope" data-formula_title="' +formula_title + '" data-formula_type="' + formula_type + '"data-formula_max_fie="' + formula_max_fie +'"data-formula_min_fie="' + formula_min_fie +'"data-formula="' + formula + '">' +
                '<td><input class = "_check2" type = "checkbox"></td>' +
                '<td>' + formula_title + '</td>' +
                '<td>' + formula + '</td>' +
                '<td>' + formula_type_text + '</td>' +
                '<td>' + formula_max_fie + '</td>' +
                '<td>' + formula_min_fie + '</td>' +
                '</tr>');

            $('#formula_title').val('');//初始化公式名称
            $('#formula_max_fie').val(0);//初始化最大过滤值
            $('#formula_min_fie').val(0);//初始化最小过滤值
            $('.select2-choices li').each(function(){//初始化公式
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })

            /*alert(formula_title+'*****'+formula_type_text+'*****'+formula_max_fie+'*****'+formula_min_fie+'*****'+formula);*/
        })

        function my_overload() {
            //初始化hour时间段模块内容
            $('#time_name').val("");
            $("#select_begin_time").empty();
            $("#select_begin_time").append("<option value='null'>无</option>");
            $("#select_end_time").empty();
            $("#select_end_time").append("<option value='null'>无</option>");
            $('#_message').text('');
            $('#error_message').hide();

            var $begin_time=parseInt($('#select_backup_day').val());//获取开始时间值
            for (var $i = $begin_time; $i < $begin_time+24; $i++) {
                if($i>23){
                    var $tmp_i=$i-24;
                    $("#select_begin_time").append("<option value='" + $i + "'>次日" + $tmp_i + "点</option>");
                }else{
                    $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                }
            }
        }

        $('#_delete1').click(function () {                                                                                 //页面delete按钮删除事件
            $('._check1').each(function () {
                if ($(this).prop('checked')) {
                    $(this).parent().parent().remove();
                }
            })
            var $info = '';
            $('#action tbody tr').each(function(k){
                $info +=  $(this).data('time_name') + '-' + $(this).data('begin_time')+ '-' +$(this).data('end_time') + ';';
            })
            $info=$info.substr(0,$info.length-1);
            $("#hour_time_mode").val($info);
        })

        $('#_delete2').click(function () {                                                                                 //页面delete按钮删除事件
            $('._check2').each(function () {
                if ($(this).prop('checked')) {
                    $(this).parent().parent().remove();
                }
            })
        })

        $('#interval_set_checked').click(function () {                                                                               //checkbox全选事件
            var $check_all = $(this);
            $('._check1').each(function () {
                $(this).prop('checked', $check_all.prop('checked'));
            })
        })

        $('#formula_set_checked').click(function () {                                                                               //checkbox全选事件
            var $check_all = $(this);
            $('._check2').each(function () {
                $(this).prop('checked', $check_all.prop('checked'));
            })
        })


        /********************Submit data processing  * start ************************************/
        $('#_submit').click(function (event) {
            var $table_task = '';
            var $table_title = $("#fieldset_title").val();   //报表标题
            var $table_type = $("input[name='radio_date']:checked").val();  //报表类型（日,周,月,年）
            var $table_day_area = '';//报表日范围
            var $table_hour_area = ''//报表小时范围
            var $table_point_info = ''//报表点位信息

            if (!$table_title) {
                alert("请填写报表标题！");
                return false;
                event.preventDefault();
            }

            //获取日范围数据
            if ($table_type == 'day') {
                $table_day_area = $("input[name='fieldset_day_radio']:checked").val();
            } else if ($table_type == 'week') {
                $("input[name='fieldset_week_checkbox']:checked").each(function () {
                    $table_day_area += $(this).val() + '-';
                })
                $table_day_area = $table_day_area.substr(0, $table_day_area.length - 1);
            } else if ($table_type == 'month') {
                $("input[name='fieldset_month_checkbox']:checked").each(function () {
                    $table_day_area += $(this).val() + '-';
                })
                $table_day_area = $table_day_area.substr(0, $table_day_area.length - 1);
            } else if ($table_type == 'year') {
                $("input[name='fieldset_year_checkbox']:checked").each(function () {
                    $table_day_area += $(this).val() + '-';
                })
                $table_day_area = put_off_last_letter($table_day_area);
                $table_day_area += ';'
                $("input[name='fieldset_month_checkbox']:checked").each(function () {
                    $table_day_area += $(this).val() + '-';
                })
                $table_day_area = put_off_last_letter($table_day_area);
            }

            //获取小时范围数据
            var $data_interval_time = $("input[name='data_interval_time']:checked").val();//获取小时类型（每小时/小时区间）
            if ($data_interval_time == 'hour') {
                $table_hour_area = $data_interval_time;
            } else if ($data_interval_time == 'auto') {
                var $time_num = $("#hour_time_mode").val();
                if ($time_num != '') {
                    $table_hour_area = $time_num;
                } else {
                    $table_hour_area = $data_interval_time;
                }
            } else {
            }

            //获取报表点位信息 $table_point_info
            var $point_type = $("input[name='radio_point_type']:checked").val();
            if ($point_type == 'point') {
                var $point_num = new Array;
                var $param = $("#point").val();
                for ($j = 0; $j < $param.length; $j++) {

                }
                if (!$param.length) {
                    alert("请进行点位选择！");
                    return false;
                    event.preventDefault();
                } else {
                    for ($i = 0; $i < $point_num.length; $i++) {
                        var $point_child = $point_num[$i].split("-");
                        var $point_key = $point_child[0];//点位主见
                        var $point_name = $point_child[1];//点位名称
                        var $point_static = '';//点位统计类型（+号连接）
                        var $point_newname = $("#name_" + $point_key).val();
                        if ($point_newname) {
                            $point_name = $point_newname;
                        }
                        var $static = $("input[name='point_" + $i + "']:checked");//被选中的统计类型
                        var $static_num = $static.length;//选中统计类型个数
                        if (!$static_num) {
                            alert("请对" + $point_name + "的统计类型进行选择！");
                            return false;
                            event.preventDefault();
                        } else {
                            for ($j = 0; $j < $static_num; $j++) {
                                $point_static += $static.eq($j).val() + "+";
                            }
                            $point_static = put_off_last_letter($point_static);
                        }
                        $table_point_info += $point_key + '-' + $point_name + '-' + $point_static + ';';
                    }
                    $table_point_info = put_off_last_letter($table_point_info);
                }
            } else if ($point_type == 'equiment') {
            } else {
            }

            $table_task = $table_title + "&&" + $table_type + "&&" + $table_day_area + "&&" + $table_hour_area + "&&" + $table_point_info;
            $('#table_task').val($table_task);
        })

        function put_off_last_letter($v) {
            return $v.substr(0, $v.length - 1);
        }
        /*******************************自定义公式 js***************************************************/

        //清空所有公式元素块儿的状态
        function _clear(){
            $('.select2-choices li').each(function(){
                $(this).data('status', 0);
            });
            $('#_error_log').text('');
        }

        //从所有公式元素快中找出当前选择的li
        function _find()
        {
            var $this = null;
            $('.select2-choices li').each(function(){
                if($(this).data('status') == 1){
                    $this = $(this);
                }
            });
            return $this;
        }
        //添加一个公式元素块li

        /********************************公式元素li块点击事件  ****************************************/
        $('.select2-choices').on('click', 'li', function(event){
            _clear();
            var $_p_left = $('#_formula').position().left;
            var $this = $(this);
            $this.data('status', 1);
            var $cha = - Math.round($('#_ate').width()/2) + 33;
            var $_X = $cha + $(this).position().left;
            var $_Y = $(this).parent().parent().parent().position().top + $(this).position().top;
            //设置弹出层在点击的li元素上方
            $('#_ate').show();
            $('#_ate').css({'left': $_X, 'top': $_Y})

        });

        /*******************************根据所选择的公式元素 进行联动 ************************/
        $("#_ele").change(function(){
            $('._calculate').hide();
            var $_chose = $(this).val();
            $('#'+$_chose).show();
        });

        /***************************** 公式表单的各个模块的处理 **********************************/
        var $ele_val = '';
        var $ele_text = '';
        var $select_type = '';
        $('#_ele').change(function(){
            $ele_val = '';
            $ele_text = '';
            $select_type = 'sz';
        })
        $('#_fh').change(function(){
            $ele_val = $("#_fh :selected").val();
            $ele_text = $("#_fh :selected").text();
            $select_type = 'fh';
        });
        $('#_hs').change(function(){
            $ele_text = $ele_val = $("#_hs :selected").val();
            $select_type = 'hs';
        });
        $('#_dw').change(function(){
            $ele_val = 'p' + $("#_dw :selected").val();
            $ele_text = $("#_dw :selected").text();
            $select_type = 'dw';
        });
        $('#_mb').change(function(){
            $ele_val = $ele_text = $("#_mb :selected").val();
            $select_type = 'mb';
        })
        $('input[name=_sz]').blur(function(){
            $ele_val = $ele_text = $(this).val();
        })
        /************************** 确定选择 按钮 *******************************************/
        $('.editable-container').on('click', '.editable-submit', function(){
            var $_obj = _find();
            if($_obj == null){
                $('#_ate').hide();
                return false;
            }
            var $_type = $_obj.data('type');
            if(!$ele_val){
                $('#_error_log').show();
                $('#_error_log').text('元素不能为空');
                return false;
            }

            if($select_type == 'mb'){
                var $_html = '';
                var $_formula_arr = $ele_text.split(' ');
                alert($_formula_arr);
                //将模板中的 元素以li的形式加入到 公式列表中
                $.each($_formula_arr, function(k , v){
                    if(v.indexOf('[') !== -1 || v.indexOf('{') !== -1){
                        //[ ] | { }
                        $_html += "<li class='select2-search-choice' data-type = 'mb' data-status = '0' style = 'background-color:#E7F2FC;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                        "<div>" + v + "</div>" +
                        "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                        "</li>";
                    }else{
                        $_html += "<li class='select2-search-choice' data-value = "+v+" data-type = 'ele' data-status = '0' style = 'background-color:#FFF;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                        "<div>" + v + "</div>" +
                        "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                        "</li>";
                    }
                });
                $_obj.after($_html);
                $_html = '';
                $_obj.remove();
            }
            else{

                $_obj.find('div').text($ele_text);
                $_obj.data('value', $ele_val);
                $_obj.removeClass('_temps');
                $_obj.data('type', 'ele');
                //已确认块的样式
                $_obj.css({'background-color':'#FFF','color':'#000','border':'none','padding':'1px 8px 1px 1px'});
            }
            $('#_'+$select_type + ' .select2').select2('val', '');
            $('#_'+$select_type).hide();
            $("#_ele").select2('val', '');
            $('#_ate').hide();
            if($_type != 'plus' && $select_type != 'mb'){
                return false;
            }
            if($('#_formula li').last().data('type') == 'plus'){
                return false;
            }
            //判断如果当前选中的元素是＋才会执行下面的clone
            //自动生成一个li
            var $clone = $_obj.clone(true);
            $clone.data('value', '');
            $clone.data('type', 'plus');
            $clone.find('div').html('<i class="fa fa-plus"></i>');
            $clone.css({'background-color':'#AFEDF5', 'color':'#FFF', 'border': 'none','padding':'1px 14px 1px 8px'});

            $('#_formula ul').append($clone);
        })

        /************************* 关闭 按钮 *****************************************************/
        $('.editable-container').on('click', '.editable-cancel', function(){
            var $_obj = _find();
            if($_obj == null){}
            else if($_obj.data('type') == 'temp'){
                $_obj.remove();
            }
            $('#_ate').hide();
        })

        $('.select2-choices').on('click', '._delete', function(event){
            $(this).parent().remove();
            //在此 阻止事件冒泡
            event.stopPropagation();
        })

        $('#_formula').on('mouseout', function() {

            $('._temps').hide();
            $('._temps').mouseover(function(){
                $('._temps').show();
                $('._temps').click(function(){
                    $(this).removeClass('_temps');
                })
            })
        })

        //清空公式列表除了 'plus' 之外的 元素
        $('#_del').click(function(){
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        })
        /************************** 遍历 公式列表中的ele 元素 生成完整公式 ************************************/
        function ele2Formula()
        {
            var $_formula = '';
            $('.select2-choices li').each(function(){
                if($(this).data('type') == 'ele'){
                    $_formula += ' ' + $(this).data('value');
                }
            })
            return $_formula;
        }

        $('#_sub').click(function(){
            var $_formula = '';
            $_formula = ele2Formula();
            $('#_calculate').val($_formula);
        })
    }

    /*   function check_all(id){
        var $check_all = $("#"+id).prop("checked");
        $('._check').prop('checked',$check_all);
    }

    function deletetimearea() {
        var $info = '';
        $('._check').each(function(){
            if($(this).prop('checked')){
                $(this).parent().parent().remove();
            }
        })
        $('#action tbody tr').each(function(k){
            //console.log($(this));
            $info +=  $(this).data('time_name') + '-' + $(this).data('begin_time')+ '-' +$(this).data('end_time') + ';';
        })
        $info=$info.substr(0,$info.length-1);
        $("#hour_time_mode").val($info);
        $('._check').prop('checked',false);//设置全选按钮
    }*/
    //上一步，下一步控制
    function displayset(one,two){
        if(one =='oneset'){
            var $table_title = $("#fieldset_title").val();   //报表标题
            if(!$table_title){
                alert("请填写报表标题！");return false;
                //event.preventDefault();
            }
        }
        $("#"+one).hide();
        $("#"+two).show();
    }

</script>                                                                                                                                                                                                                                          