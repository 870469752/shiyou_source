    <?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use common\library\MyFunc;
    use Yii\web\View;
    use backend\models\Point;

    ?>
    <style>
        .create{
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
            border-radius: 2px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .popover-content:last-child {
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .popover-content {
            padding: 9px 14px;
        }
        .popover-title {
            margin: 0;
            padding: 8px 14px;
            font-size: 13px;
            font-weight: 400;
            line-height: 18px;
            background-color: #f7f7f7;
            border-bottom: 1px solid #ebebeb;
            border-radius: 2px 2px 0 0;
        }
        .editable-container.popover {
            width: auto;
        }
        .smart-form .col-4 {
            width: 100%;
        }
        .smart-form .state-success {
            width: 10%;
            float: left;
        }
        .smart-form fieldset {
            height: auto;
            display: block;
            padding-top: 10px;
            padding-bottom: 10px;
            border-bottom: 1px dashed rgba(0,0,0,.2);
            background: rgba(255,255,255,.9);
            position: relative;
        }

        table td{width: 12%}
        table tr{border:dotted ; border-width:1px 0px 0px 0px;}
        table{border:dotted ; border-width:1px 1px 1px 1px;}

    </style>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- START ROW -->
        <div class="row">

            <!-- NEW COL START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget"
                data-widget-deletebutton="false"
                data-widget-editbutton="false"
                data-widget-colorbutton="false"
                data-widget-sortable="false">
                    <header>
                        <span class="widget-icon">
                            <i class="fa fa-edit"></i>
                        </span>
                        <h2><?= Html::encode($this->title) ?></h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget content -->
.                        <div class="widget-body">
                            <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
                            <?= Html::hiddenInput('AutoTable[table_task]', '', ['id' => 'table_task'])?>
                            <div id="oneset">
                            <fieldset>
                                <section>
                                        <div class="form-group field-scheduledtask-export_title has-success">
                                            <label class="label">报表标题</label>
                                            <input type="text" id="fieldset_title" class="form-control" name="fieldset_title" value="">
                                        </div>
                                </section>
                            </fieldset>
                            <!--<fieldset  id="Backup_date">
                                <label class="label"><?/*=Yii::t('app', 'Backup Date')*/?> :</label>
                                <div id = '_cron'  class = 'time_mode'>
                                    <span id='selector'></span>
                                </div>
                            </fieldset>-->
                            <!--报表类型-->
                            <fieldset>
                                <label class="label">报表类型</label>
                                <label class="radio state-success"><input type="radio" id="radio_day" name="radio_date" value="day" checked><i></i>日报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_week" name="radio_date" value="week"><i></i>周报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_month" name="radio_date" value="month"><i></i>月报表</label>
                                <label class="radio state-success"><input type="radio" id="radio_year" name="radio_date" value="year"><i></i>年报表</label>
                                <label class="radio state-success"  style="display: none"><input type="radio" id="radio_interval" name="radio_date" value="interval"><i></i>间隔类报表</label>
                            </fieldset>
                            <!--开始时间-->
                                <fieldset style="display: none">
                                    <label class="label">开始时间</label>
                                    <label class="select state-success">
                                        <select id="select_backup_year" style="display: none">
                                            <option value="1" selected>1月</option>
                                            <?php for($i=2;$i<13;$i++){?>
                                                <option value="<?=$i?>"><?=$i?>月</option>
                                            <?php }?>
                                        </select><i></i>
                                    </label>
                                    <label class="select state-success">
                                        <select id="select_backup_month" style="display: none">
                                            <option value="1" selected>1号</option>
                                            <?php for($i=2;$i<28;$i++){?>
                                                <option value="<?=$i?>"><?=$i?>号</option>
                                            <?php }?>
                                        </select><i></i>
                                    </label>
                                    <label class="select state-success">
                                        <select id="select_backup_week" style="display: none">
                                            <option value="1" selected>周一</option>
                                            <option value="2" selected>周二</option>
                                            <option value="3" selected>周三</option>
                                            <option value="4" selected>周四</option>
                                            <option value="5" selected>周五</option>
                                            <option value="6" selected>周六</option>
                                            <option value="7" selected>周日</option>
                                        </select><i></i>
                                    </label>
                                    <label class="select state-success">
                                        <select id="select_backup_day">
                                            <option value="0" selected>0点</option>
                                            <?php  for($i=1;$i<24;$i++){ ?>
                                                <option value="<?=$i?>"><?=$i?>点</option>
                                            <?php }?>
                                        </select><i></i>
                                    </label>
                                    </label>
                            </fieldset>
                                <fieldset id="fieldset_day">
                                    <label class="label">天范围</label>
                                    <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="all" checked><i></i>全部</label>
                                    <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="work_day"><i></i>工作日</label>
                                    <label class="radio state-success"><input type="radio" name="fieldset_day_radio" value="week_end"><i></i>周末</label>
                            </fieldset>
                                <fieldset style="display: none" id="fieldset_week">
                                        <label class="label">天范围</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="7" checked><i></i>星期日</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="1" checked><i></i>星期一</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="2" checked><i></i>星期二</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="3" checked><i></i>星期三</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="4" checked><i></i>星期四</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="5" checked><i></i>星期五</label>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox"  value="6" checked><i></i>星期六</label>
                            </fieldset>
                                <fieldset style="display: none" id="fieldset_year">
                                    <label class="label">月范围</label>
                                    <?php for($i=1;$i<13;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="fieldset_year_checkbox" value="<?=$i?>" checked><i></i><?=$i?>月</label>
                                    <?php }?>
                                </fieldset>
                                <fieldset style="display: none" id="fieldset_month">
                                        <label class="label">天范围</label>
                                            <?php for($i=1;$i<32;$i++){?>
                                                <label class="checkbox state-success"><input type="checkbox" name="fieldset_month_checkbox" value="<?=$i?>" checked><i></i><?=$i?>号</label>
                                            <?php }?>
                                </fieldset>
                                <fieldset style="display: none" id="fieldset_interval">
                                    <div id = '_static'>
                                        <label class="label">间隔</label>
                                        <a href="#" id="static" data-type="timec" data-pk="1" data-title="Please, fill time" class="editable editable-click">empty</a>
                                        <input type="hidden" id="static_time" name="ScheduledTask[statistic_time]" value="Empty">
                                    </div>
                                </fieldset>
                                <!--数据生成时间间隔-->
                            <fieldset>
                                <label class="label">时范围</label>
                                <label class="radio state-success" id="every_hour"><input type="radio"  name="data_interval_time" value="hour" checked><i></i>每小时</label>
                                <label class="radio state-success" id="every_auto" style="width: auto"><input type="radio" name="data_interval_time" value="auto"><i></i>自定义</label>
                                <label class="select state-success" id="lable_select_data_interval" style="margin-left:15px;display: none">
                                    <select id="select_data_interval" >
                                        <option value='hour'>小时区间</option>
                                    </select><i></i>
                                </label>
                            </fieldset>

                            <fieldset  style="display: none" id="fieldset_day_interval">
                                <label class="label">时间区间</label>
                                <div class="col-md-12">
                                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                        <div class="panel panel-primary" style="padding: 15px;display: block">
                                            <div class="panel-body" ng-transclude>
                                                <table id = "action" class="table table-bordered table-condensed ng-scope">
                                                    <thead>
                                                    <input type="hidden" id="hour_time_mode" style="width: 800px"/>
                                                    <tr style="font-weight: bold">
                                                        <th style = "10%"><input type = "checkbox" id = "set_checked" onclick="check_all()"></th>
                                                        <th style="width:30%">名称</th>
                                                        <th style="width:30%">开始时间</th>
                                                        <th style="width:30%">结束时间</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <footer style="background-color: lightgoldenrodyellow">
                                                    <button id = "_delete" type="button" onclick="deletetimearea()" class="btn btn-primary">Delete</button>
                                                    <button id = "_add" type="button" onclick="addtimearea()" class="btn btn-default" data-toggle="modal" data-target="#myModal">Add row</button>
                                                </footer>
                                            </div>
                                        </div>
                                    </panel>
                                </div>
                            </fieldset>
                                <div class="form-actions">
                                    <button type="button" id="" class="create btn-success" onclick="displayset('oneset','twoset')">下一步</button>
                                </div>
                            </div>

                            <div id = "twoset" style="display: none">
                                <fieldset>
                                    <label class="label">数据类型</label>
                                    <label class="radio state-success"><input type="radio" id="radio_point" name="radio_point_type" value="point" checked><i></i>点位</label>
                                    <label class="radio state-success"><input type="radio" id="radio_equiment" name="radio_point_type" value="equiment"><i></i>自定义</label>
                                </fieldset>
                                <fieldset>
                                    <label class="label">点位选择</label>
                                    <?=Html::dropDownList('dropDownList_point', null, $point_info,  ['id' => 'point', 'class' => 'select2 point_info', 'multiple' => 'multiple', 'placeholder' => '点位选择'])?>
                                </fieldset>
                                <fieldset style="display: none">
                                    <label class="label">点位选择</label>
                                    <?php foreach($point_info as $k=>$value) {?>
                                        <label class="checkbox state-success"><input type="checkbox" name="point" value="<?=$k.'-'.$value;?>" checked><i></i><?=$value;?></label>
                                    <?php } ?>
                                </fieldset>
                                <fieldset id = "point_static" >
                                    <label class="label">点位统计类型及命名</label>
                                    <label class="label">
                                        <table style="width: 100%;text-align: center">
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </label>
                                </fieldset>

                                <div class="form-actions">
                                    <button type="button" id="" class="create btn-success" onclick="displayset('twoset','oneset')">上一步</button>&nbsp;&nbsp;
                                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

        <!-- Modal -->
        <div class="modal fade" style="padding-top: 200px" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 400px">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            ×
                        </button>
                        <h4 class="modal-title" id="myModalLabel">添加</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" >时间段名
                                    <input type="text" id = "time_name" style="width: 50%">
                                </div>
                                <div class="form-group">开始时间
                                        <select id="select_begin_time" style="width: 50%">
                                            <option value="null" selected>无</option>
                                            <?php /*for($i=0;$i<25;$i++){*/?><!--
                                                <option value="<?/*=$i*/?>"><?/*=$i*/?>点</option>
                                            --><?php /*}*/?>
                                        </select>
                                </div>
                                <div class="form-group">结束时间
                                    <select id="select_end_time" style="width: 50%">
                                        <option value="null" selected>无</option>

                                    </select>
                                </div>
                                <div id = "error_message" style = "display:none" class = "form-group">
                                    <i class="fa-fw fa fa-warning"></i>
                                    <span style = "color:red" id = "_message"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id = "add_action" type="button" class="btn btn-primary">
                            Add
                        </button>
                        <button type="button" id = "_cancel" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </section>
    <?php
    $this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
    $this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
    $this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
    //cron
    $this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
    $this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
    $this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);

    ?>
    <script>
        window.onload = function() {
            var $static =$('#static_time').val();
            /*var $cron = $('#selector').cron({
                //initial: '0 * * * *',//时
                initial: '0 0 * * *',//日
                //initial: '0 0 * * 0'//周
                //initial: '0 0 1 * *',//月
                //initial: '0 0 1 1 *',//年
                onChange: function() {
                    //$('#interval_time').val($(this).cron("value"))
                    var $temp_value =  $(this).cron("value");
                    var $tem_arr = $temp_value.split("*");
                    var $tem_arr2 = $temp_value.split(" ");
                    var $tem_num = $tem_arr.length-1;
                    var $tem_str = '';
                    if($tem_num == 3 ){
                        $tem_str = 'day'
                    }else if($tem_num == 1){
                        $tem_str = 'year'
                    }else if($tem_num == 2 && $tem_arr2[4] == '*'){
                        $tem_str = 'month'
                    }else{
                        $tem_str = 'week'
                    }
                    $("fieldset[id^='fieldset_']").hide();
                    $("#fieldset_"+$tem_str).show();
                    //alert($tem_str);
                },
                useGentleSelect: true
            });*/
           // $("select[id^='select_backup_']").show();
            $("input:radio[name='radio_date']").change(function() {
                var $temp = $("input[name='radio_date']:checked").val();
                $("fieldset[id^='fieldset_']").hide();//隐藏“报表数据范围里的所有选项”
                $("fieldset[id ='fieldset_"+$temp+"']").show();
                $("input:radio[name='data_interval_time']:eq(0)").prop('checked',true);//设置"数据生成时间间隔"初始化
                //$("#point_static label table tbody").empty();//初始化点位统计类型及命名

                $("#lable_select_data_interval").hide();

                if($temp=='day'){
                }else if($temp == 'week'){
                } else if($temp == 'month'){
                }else if($temp == 'year'){
                    $("#fieldset_month").show();
                }else{
                }
            })

            $("input:radio[name='data_interval_time']").change(function() {
                var $temp1 = $("input[name='data_interval_time']:checked").val();
                var $temp2 = $("input[name='radio_date']:checked").val();

                if($temp1 == 'auto'){
                    $("#lable_select_day_interval").show();
                    $("#fieldset_day_interval").show();
                }else{
                    $("#lable_select_data_interval").hide();
                    $("#fieldset_day_interval").hide();
                }
            })

            $("#select_begin_time").change(function() {//select_backup_day
                var $select_begin_time = parseInt($("#select_begin_time").val());
                var $time_num = $("#hour_time_mode").val();
                $("#select_end_time").empty();
                $("#select_end_time").append("<option value='null'>无</option>");
                //alert($select_begin_time);return false;
                if($time_num != ''){
                    var $arr_one = $time_num.split(";");
                    if($arr_one.length == 1){

                        var $arr_two = $arr_one[0].split("-");
                        if($select_begin_time<$arr_two[1]){
                            for(var $i=$select_begin_time+1;$i<(parseInt($arr_two[1])+1);$i++){
                                $("#select_end_time").append("<option value='"+$i+"'>"+$i+"点</option>");
                            }
                        }else{
                            for(var $i=$select_begin_time+1;$i<25;$i++){
                                $("#select_end_time").append("<option value='"+$i+"'>"+$i+"点</option>");
                            }
                        }
                    }else{
                        var $arr_two =new Array;
                        for($i=0;$i<$arr_one.length;$i++){
                            $arr_two[$i] = $arr_one[$i].split('-')
                        }
                        for($i=0;$i<$arr_two.length;$i++){
                            if($select_begin_time<parseInt($arr_two[0][1])){
                                for($z=$select_begin_time+1;$z<=parseInt($arr_two[0][1]);$z++){
                                    $("#select_end_time").append("<option value='" + $z + "'>" + $z + "点</option>");
                                }
                                break;
                            }
                            if($i == $arr_two.length-1){//alert("24");alert($select_begin_time);alert(parseInt($arr_two[$i][2]));alert(parseInt($arr_two[$i+1][1]));return false;
                                for($z=$select_begin_time+1;$z<25;$z++){
                                    $("#select_end_time").append("<option value='" + $z + "'>" + $z + "点</option>");
                                }
                                break;
                            }//alert($select_begin_time);alert(parseInt($arr_two[$i][2]));alert(parseInt($arr_two[$i+1][1]));
                            if($select_begin_time>=parseInt($arr_two[$i][2])&&$select_begin_time<parseInt($arr_two[$i+1][1])){//alert($select_begin_time);alert(parseInt($arr_two[$i][2]));alert(parseInt($arr_two[$i+1][1]));
                                for($z=$select_begin_time+1;$z<=parseInt($arr_two[$i+1][1]);$z++){
                                    $("#select_end_time").append("<option value='" + $z + "'>" + $z + "点</option>");
                                }
                                break;
                            }
                        }
                    }
                }else{
                    for(var $i=$select_begin_time+1;$i<25;$i++){
                        $("#select_end_time").append("<option value='"+$i+"'>"+$i+"点</option>");
                    }
                }
            })

            function add_row($timename, $begintime,$endtime){
                var $arr_two =new Array;
                var $time_num = $("#hour_time_mode").val();
                //时间段排序
                if($time_num==''){
                    $("#hour_time_mode").prop("value",$timename+"-"+$begintime+"-"+$endtime);
                    $('#action tbody').append(
                        '<tr ng-repeat="user in users" class="ng-scope" data-time_name="'+$timename+'" data-begin_time="'+$begintime+'"data-end_time="'+$endtime+'">' +
                        '<td><input class = "_check" type = "checkbox"></td>'+
                        '<td>'+$timename+'</td>'+
                        '<td>'+$begintime+'点</td>'+
                        '<td>'+$endtime+'点</td>'+
                        '</tr>');
                }else{
                    $time_num+=";"+$timename+"-"+$begintime+"-"+$endtime;
                    var $arr_one = $time_num.split(";");
                    for($i=0;$i<$arr_one.length;$i++){
                        $arr_two[$i] = $arr_one[$i].split('-')
                    }
                    $arr_two.sort(function( x , y){  return x[1] - y[1]; });
                    var $str = '';
                    for($j=0;$j<$arr_two.length;$j++){
                        if($j == 0){
                            $str +=  $arr_two[$j][0] + "-" + $arr_two[$j][1] + "-" + $arr_two[$j][2];
                        }else {
                            $str += ";" + $arr_two[$j][0] + "-" + $arr_two[$j][1] + "-" + $arr_two[$j][2];
                        }
                    }
                    $("#hour_time_mode").prop("value", $str);

                    $('#action tbody').empty();
                    for($i=0;$i<$arr_two.length;$i++){
                        $('#action tbody').append(
                            '<tr ng-repeat="user in users" class="ng-scope" data-time_name="'+$arr_two[$i][0]+'" data-begin_time="'+$arr_two[$i][1]+'"data-end_time="'+$arr_two[$i][2]+'">' +
                            '<td><input class = "_check" type = "checkbox"></td>'+
                            '<td>'+$arr_two[$i][0]+'</td>'+
                            '<td>'+$arr_two[$i][1]+'点</td>'+
                            '<td>'+$arr_two[$i][2]+'点</td>'+
                            '</tr>');
                    }
                }
            }

            /**************************function init * end ****************/

            /*------------------------------------------------------------------------------------------------------------*/

            /************  action table click event  * start ********************/

            $('#add_action').click(function(){                                                                              //弹出框 add点击事件
                var time_name = $('#time_name').val().trim();
                var begin = $('#select_begin_time').val();
                var end = $('#select_end_time').val();

                if(time_name == ''){
                    $('#error_message').show();
                    $('#_message').text('请填写时间段名!');
                    return false;
                }
                if(begin == 'null'){
                    $('#error_message').show();
                    $('#_message').text('请选择开始时间！');
                    return false;
                }
                if(end == 'null'){
                    $('#error_message').show();
                    $('#_message').text('请选择结束时间!');
                    return false;
                }
                add_row(time_name, begin,end);my_overload();
                $('#_cancel').trigger('click');
            });

            $('#_add').click(function(){                                                                                    //页面添加row按钮点击事件
                my_overload();
            })

            function my_overload(){
                //初始化hour时间段模块内容
                $('#time_name').val("");
                $("#select_begin_time").empty();
                $("#select_begin_time").append("<option value='null'>无</option>");
                $("#select_end_time").empty();
                $("#select_end_time").append("<option value='null'>无</option>");
                $('#_message').text('');
                $('#error_message').hide();

                var $time_num = $("#hour_time_mode").val();//获取隐藏框内容
                if ($time_num != '') {
                    var $arr_one = $time_num.split(";");
                    if($arr_one.length == 1){
                        var $arr_two = $arr_one[0].split('-');
                        for (var $i = 0; $i < $arr_two[1]; $i++) {
                            $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                        }
                        for (var $i = $arr_two[2]; $i < 24; $i++) {
                            $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                        }
                    }else{
                        var $arr_two =new Array;
                        for($i=0;$i<$arr_one.length;$i++){
                            $arr_two[$i] = $arr_one[$i].split('-')
                        }
                        for($i=0;$i<$arr_two.length;$i++){
                            if($i==0){
                                for($j=0;$j<parseInt($arr_two[$i][1]);$j++){
                                    $("#select_begin_time").append("<option value='" + $j + "'>" + $j + "点</option>");
                                }
                            }else{
                                for($j=parseInt($arr_two[$i-1][2]);$j<parseInt($arr_two[$i][1]);$j++){
                                    $("#select_begin_time").append("<option value='" + $j + "'>" + $j + "点</option>");
                                }
                            }
                            if($i==$arr_two.length-1){
                                for (var $k = ($arr_two[$i][2]); $k < 24; $k++) {
                                    $("#select_begin_time").append("<option value='" + $k + "'>" + $k + "点</option>");
                                }
                            }
                        }
                    }
                } else {
                    $("#select_begin_time").empty();
                    $("#select_begin_time").append("<option value='null'>无</option>");
                    for (var $i = 0; $i < 24; $i++) {
                        $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                    }
                }
            }

            $('#_delete').click(function(){                                                                                 //页面delete按钮删除事件
                $('._check').each(function(){
                    if($(this).prop('checked')){
                        $(this).parent().parent().remove();
                    }
                })
            })

            $('#check_all').click(function(){                                                                               //checkbox全选事件
                var $check_all = $(this);
                $('._check').each(function(){
                    $(this).prop('checked', $check_all.prop('checked'));
                })
            })

            $('.point_info').change(function(e) {
                var $arr_point = new Array;
                var $param = $(this).val();
                if($param === null) return true;
                for($j=0;$j<$param.length;$j++){
                    <?php foreach($point_info as $k=>$v){ ?>
                    if($param[$j] == <?=$k;?>) {
                        $arr_point[$j]='<?=$k."-".$v;?>';
                    }
                    <?php } ?>
                }
                for($i=0;$i<$arr_point.length;$i++){
                    $arr_point[$i]=$arr_point[$i].split('-');
                }
                //alert($arr_point);return false;
                if($arr_point === null){
                    $("#point_static").hide();
                    return true;
                }else{
                    $("#point_static").show();
                    $("#point_static label table tbody").empty();
                    for($i=0;$i<$arr_point.length;$i++){
                        $("#point_static label table tbody").append(
                            '<tr style="height:40px">'+
                            '<td style="width: 6%;border;border-right: 1px dotted">'+$arr_point[$i][1]+'</td>'+
                            '<td style="width: 6%;border;border-right: 1px dotted"><input type="text"  id="name_'+$arr_point[$i][0]+'" maxlength="255" placeholder="'+$arr_point[$i][1]+'"></td>'+
                            '<td><label class="checkbox state-success" style="margin-left: 20px"><input type="checkbox" name="point_'+$i+'" value="max" checked><i></i>最大</label></td>'+
                            '<td><label class="checkbox state-success" style="margin-left: 20px"><input type="checkbox" name="point_'+$i+'" value="min" checked><i></i>最小</label></td>'+
                            '<td><label class="checkbox state-success"><input type="checkbox" name="point_'+$i+'" value="avg" checked><i></i>平均</label></td>'+
                            '<td><label class="checkbox state-success"><input type="checkbox" name="point_'+$i+'" value="sum" checked><i></i>总和</label></td>'+
                            '</tr>'
                        )
                    }
                }
            })


            $("input:checkbox[name='point']").on('change', function(e) {
                var $arr_point = new Array;
                var $i=0;
                $("input[name='point']:checkbox").each(function(){
                    if($(this).prop("checked")){
                       $arr_point[$i]=$(this).val().split('-');
                        $arr_point[$i][2]=$(this).val();
                        $i++;
                    }
                })
                if($arr_point === null){
                    $("#point_static").hide();
                    return true;
                }else{
                    $("#point_static").show();
                    $("#point_static label table tbody").empty();
                    for($i=0;$i<$arr_point.length;$i++){
                        $("#point_static label table tbody").append(
                            '<tr style="height:40px">'+
                                '<td style="width: 6%;border;border-right: 1px dotted">'+$arr_point[$i][1]+'</td>'+
                                '<td style="width: 6%;border;border-right: 1px dotted"><input type="text"  id="name_'+$arr_point[$i][0]+'" maxlength="255" placeholder="'+$arr_point[$i][1]+'"></td>'+
                                '<td><label class="checkbox state-success" style="margin-left: 20px"><input type="checkbox" name="point_'+$i+'" value="max" checked><i></i>最大</label></td>'+
                                '<td><label class="checkbox state-success" style="margin-left: 20px"><input type="checkbox" name="point_'+$i+'" value="min" checked><i></i>最小</label></td>'+
                                '<td><label class="checkbox state-success"><input type="checkbox" name="point_'+$i+'" value="avg" checked><i></i>平均</label></td>'+
                                '<td><label class="checkbox state-success"><input type="checkbox" name="point_'+$i+'" value="sum" checked><i></i>总和</label></td>'+
                            '</tr>'
                        )
                    }
                }
            })

            /********************Submit data processing  * start ************************************/
            $('#_submit').click(function(event){
                var $table_task = '';
                var $table_title = $("#fieldset_title").val();   //报表标题
                var $table_type = $("input[name='radio_date']:checked").val();  //报表类型（日,周,月,年）
                var $table_day_area = '';//报表日范围
                var $table_hour_area = ''//报表小时范围
                var $table_point_info = ''//报表点位信息

                if(!$table_title){
                    alert("请填写报表标题！");return false;
                    event.preventDefault();
                }

                //获取日范围数据
                if($table_type=='day'){
                    $table_day_area = $("input[name='fieldset_day_radio']:checked").val();
                }else if($table_type == 'week'){
                    $("input[name='fieldset_week_checkbox']:checked").each(function(){
                        $table_day_area += $(this).val() + '-';
                    })
                    $table_day_area=$table_day_area.substr(0,$table_day_area.length-1);
                } else if($table_type == 'month'){
                    $("input[name='fieldset_month_checkbox']:checked").each(function(){
                        $table_day_area += $(this).val() + '-';
                    })
                    $table_day_area=$table_day_area.substr(0,$table_day_area.length-1);
                }else if($table_type == 'year'){
                    $("input[name='fieldset_year_checkbox']:checked").each(function(){
                        $table_day_area += $(this).val() + '-';
                    })
                    $table_day_area=put_off_last_letter($table_day_area);
                    $table_day_area += ';'
                    $("input[name='fieldset_month_checkbox']:checked").each(function(){
                        $table_day_area += $(this).val() + '-';
                    })
                    $table_day_area=put_off_last_letter($table_day_area);
                }

                //获取小时范围数据
                var $data_interval_time = $("input[name='data_interval_time']:checked").val();//获取小时类型（每小时/小时区间）
                if($data_interval_time == 'hour'){
                    $table_hour_area =$data_interval_time;
                }else if($data_interval_time == 'auto'){
                    var $time_num = $("#hour_time_mode").val();
                    if($time_num != ''){
                        $table_hour_area=$time_num;
                    }else{
                        $table_hour_area =$data_interval_time;
                    }
                }else{}

                //获取报表点位信息 $table_point_info
                var $point_type = $("input[name='radio_point_type']:checked").val();
                if($point_type=='point'){
                    var $point_num = new Array;
                    var $param = $("#point").val();
                    for($j=0;$j<$param.length;$j++){
                        <?php foreach($point_info as $k=>$v){ ?>
                        if($param[$j] == <?=$k;?>) {
                            $point_num[$j]='<?=$k."-".$v;?>';
                        }
                        <?php } ?>
                    }
                    if(!$param.length){
                        alert("请进行点位选择！");
                        return false;
                        event.preventDefault();
                    }else{
                        for($i = 0;$i < $point_num.length;$i++){
                            var $point_child = $point_num[$i].split("-");
                            var $point_key = $point_child[0];//点位主见
                            var $point_name =$point_child[1];//点位名称
                            var $point_static ='';//点位统计类型（+号连接）
                            var $point_newname = $("#name_"+$point_key).val();
                            if($point_newname){$point_name = $point_newname;}
                            var $static =  $("input[name='point_"+$i+"']:checked");//被选中的统计类型
                            var $static_num = $static.length;//选中统计类型个数
                            if(!$static_num){
                                alert("请对"+$point_name+"的统计类型进行选择！");
                                return false;
                                event.preventDefault();
                            }else{
                                for($j=0;$j<$static_num;$j++){
                                    $point_static += $static.eq($j).val()+"+";
                                }
                                $point_static=put_off_last_letter($point_static);
                            }
                            $table_point_info += $point_key+'-'+$point_name+'-'+$point_static+';';
                        }
                        $table_point_info=put_off_last_letter($table_point_info);
                    }
                }else if($point_type=='equiment'){
                }else{}

                $table_task = $table_title+"&&"+$table_type+"&&"+$table_day_area+"&&"+$table_hour_area+"&&"+$table_point_info;
                $('#table_task').val($table_task);
            })

            function put_off_last_letter($v){
               return $v.substr(0,$v.length-1);
            }

            //$("input:checkbox[name='point']").trigger('change');
            //$('.point_info').trigger('change');
            /********************Submit data processing  * end ************************************/
            /* x-editable 生成时间选择插件 */
            /*timeOfEditable( $('#static') );
            $('#static').text($static);
            $('#scheduledtask-mission_mode').val() === 'cron' ? $cron.cron('value', $interval) : '';*/
        }
        function addtimearea() {
            //初始化hour时间段模块内容
            $('#time_name').val("");
            $("#select_begin_time").empty();
            $("#select_begin_time").append("<option value='null'>无</option>");
            $("#select_end_time").empty();
            $("#select_end_time").append("<option value='null'>无</option>");
            $('#_message').text('');
            $('#error_message').hide();

            var $time_num = $("#hour_time_mode").val();//获取隐藏框内容
            if ($time_num != '') {
                var $arr_one = $time_num.split(";");
                if($arr_one.length == 1){
                    var $arr_two = $arr_one[0].split('-');
                    for (var $i = 0; $i < $arr_two[1]; $i++) {
                        $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                    }
                    for (var $i = $arr_two[2]; $i < 24; $i++) {
                        $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                    }
                }else{
                    var $arr_two =new Array;
                    for($i=0;$i<$arr_one.length;$i++){
                        $arr_two[$i] = $arr_one[$i].split('-')
                    }
                    for($i=0;$i<$arr_two.length;$i++){
                        if($i==0){
                            for($j=0;$j<parseInt($arr_two[$i][1]);$j++){
                                $("#select_begin_time").append("<option value='" + $j + "'>" + $j + "点</option>");
                            }
                        }else{
                            for($j=parseInt($arr_two[$i-1][2]);$j<parseInt($arr_two[$i][1]);$j++){
                                $("#select_begin_time").append("<option value='" + $j + "'>" + $j + "点</option>");
                            }
                        }
                        if($i==$arr_two.length-1){
                            for (var $k = ($arr_two[$i][2]); $k < 24; $k++) {
                                $("#select_begin_time").append("<option value='" + $k + "'>" + $k + "点</option>");
                            }
                        }
                    }
                }
            } else {
                $("#select_begin_time").empty();
                $("#select_begin_time").append("<option value='null'>无</option>");
                for (var $i = 0; $i < 24; $i++) {
                    $("#select_begin_time").append("<option value='" + $i + "'>" + $i + "点</option>");
                }
            }
        }
        function check_all(){
            var $check_all = $("#set_checked").prop("checked");
            $('._check').prop('checked',$check_all);
        }

        function deletetimearea() {
            var $info = '';
            $('._check').each(function(){
                if($(this).prop('checked')){
                    $(this).parent().parent().remove();
                }
            })
            $('#action tbody tr').each(function(k){
                //console.log($(this));
                $info +=  $(this).data('time_name') + '-' + $(this).data('begin_time')+ '-' +$(this).data('end_time') + ';';
            })
            $info=$info.substr(0,$info.length-1);
            $("#hour_time_mode").val($info);
            $('._check').prop('checked',false);//设置全选按钮
        }

        //上一步，下一步控制
        function displayset(one,two){
            $("#"+one).hide();
            $("#"+two).show();
        }

    </script>                                                                                                                                                                                                                                          