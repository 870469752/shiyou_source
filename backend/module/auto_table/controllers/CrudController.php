<?php

namespace backend\module\auto_table\controllers;
//yii 引用
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;
//模型 引用
use backend\models\Template;
use backend\models\Category;
use backend\models\AutoTable;
use backend\models\search\AutoTableSearch;
use backend\models\Point;
use backend\models\Unit;
use backend\models\search\PointDataSearch;
use yii\web\User;

class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function test(){
        return $this->render('test');
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutoTableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Deletes an existing CustomReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AutoTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function splitArr($data){
        $j=-1;$y=0;
        foreach($data as $key=>$value){
            if($key != 0){
                $j++;$i=$data[$key-1];
                if($i==($value-1)){
                    $str[$y][]=$value;
                }else{
                    $y++;
                    $str[$y][]=$value;
                }
            }else{
                $y++;
                $str[$y][]=$value;
            }
        }
        return $str;
    }

    /**
     * Creates a new CustomReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AutoTable();
        if ($model->load(Yii::$app->request->post(), '') && $model->AutoTableSave()) {
            return $this->redirect(['index']);
        } else {
            $points = MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getOfStandardUnit(), 'id', 'name'));
            $formula =Template::getFormula();
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            //echo "<pre>";
            //print_r($unit);die;
            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());
            return $this->render('create',[
                'model' => $model,
                'unit' => $unit,
                'category_tree' => $category_tree,
                'points' => $points,
                'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
            ]);
        }
    }

    /*public function actionCreate()
    {
        $model = new AutoTable();
        if ($model->load(Yii::$app->request->post(), '') && $model->AutoTableSave()) {
            return $this->redirect(['index']);
        } else {
            $point_info = MyFunc::_map(Point::getAvailablePoint(), 'id', 'name');
            return $this->render('create',[
                'point_info' => $point_info,
                'model' => $model
            ]);
        }
    }*/
}
