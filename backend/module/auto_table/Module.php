<?php

namespace backend\module\auto_table;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\auto_table\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
