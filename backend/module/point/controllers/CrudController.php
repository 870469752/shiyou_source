<?php

namespace backend\module\point\controllers;
//yii 引用
use backend\models\LocationCategory;
use backend\models\Protocol;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;
//模型 引用
use backend\models\Point;
use backend\models\CalculatePoint;
use backend\models\SimulatePoint;
use backend\models\BacnetPoint;
use backend\models\DemoPoint;
use backend\models\PointEvent;
use backend\models\search\PointSearch;
use backend\models\Unit;
use backend\models\Category;
use backend\models\Collector;
use backend\models\BacnetController;
use backend\models\PointData;
use backend\models\search\PointDataSearch;
use yii\helpers\BaseArrayHelper;
use common\library\Ssp;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        $unit=new Unit();
        $unit=BaseArrayHelper::map($unit->find()->orderBy('category_id asc, id asc')->asArray()->all(),'id','name');
        //$unit=json_encode($unit);
        //echo "<pre>";print_r($unit);die;
        return $this->render('index', [
            'active'=>'bacnet_show',
            'unit' => $unit
        ]);
    }

    public function actionNewIndex()
    {

        $treeData=self::getColDevTree();
        $unit=new Unit();
        $unit=BaseArrayHelper::map($unit->find()->orderBy('category_id asc, id asc')->asArray()->all(),'id','name');
//        echo '<pre>';
//        print_r($unit);die;
        // $unit=json_encode($unit);


        return $this->render('index_jia', [
            'treeData'=>$treeData,
            'active'=>'bacnet_show',
            'unit' => $unit

        ]);
    }
    public function actionGetData()
    {
        $protocol_id = Yii::$app->request->get('protocol_id');
        $type = Yii::$app->request->get('type');
        if($type == 'point'){
            $con = " is_shield = false ";
        }else{
            $con = " is_shield = true ";
        }

        if($protocol_id){
            $con .= " and protocol_id = $protocol_id ";
        }else{
            if($type == 'point'){
                $con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
            }
        }
        //$db = Yii::$app->db;
        //echo "<pre>";print_r($con);die;
        $table = 'core.point';
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),
            array( 'db' => 'is_shield',   'dt' => 'is_shield' ,'dj'=>"is_shield" ),
            array( 'db' => 'unit',   'dt' => 'unit' ,'dj'=>"unit" ),
            array( 'db' => 'update_timestamp',   'dt' => 'update_timestamp' ,'dj'=>"update_timestamp" ),
            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        $unit=new Unit();
        $unit=BaseArrayHelper::map($unit->find()->orderBy('category_id asc, id asc')->asArray()->all(),'id','name');
        for($i=0; $i<count($result['data']); $i++){
            foreach($unit as $k => $v){
                if($result['data'][$i]['unit'] == $k){
                    $result['data'][$i]['unit'] = $v;
                    break;
                }
            }
        }
        $p_e=new PointEvent();
        for($i=0; $i<count($result['data']); $i++){
            $h_e = $p_e->getEventAndRule($result['data'][$i]['id']);
            empty($h_e)?$result['data'][$i]['have_event']="NO":$result['data'][$i]['have_event']="YES";
        }
        echo json_encode( $result);
    }

    public function actionUpdatePoint(){
        $id = Yii::$app->request->post('id');
        $cn = Yii::$app->request->post('cn');
        $us = Yii::$app->request->post('us');
        $new_data = [
            'data_type' => 'description',
            'data' => ['zh-cn'=>$cn,'en-us'=>$us],
        ];
        $name = MyFunc::jsonTransfer($new_data);
        $model = Point::getById($id);
        $model->name = $name;
        return $model->save();
    }

    //批量更新点位单位
    public function actionUpdatePointUnit(){
        $ids = explode(",",Yii::$app->request->post('id'));
        $unit = Yii::$app->request->post('unit');

        //echo "<pre>";print_r($ids);die;
        foreach($ids as $k=>$v){
            $model = Point::getById($v);
            $model->save($model->unit=$unit);
        }
        return $model->save();
    }

    //查找点位事件及规则
    public function actionGetEventRule(){
        $point_id = Yii::$app->request->get('point_id');
        $p_e=new PointEvent();
        $result = $p_e->getEventAndRule($point_id);
        echo json_encode( $result);
    }

    //点位批量删除
    public function actionDeletePoint(){
        $ids = explode(",",Yii::$app->request->post('id'));
        foreach($ids as $k=>$v){
            $model = Point::getById($v);
            $model->save($model->is_shield=true);
        }
        return $model->save();
    }

    //点位批量回收
    public function actionRevePoint(){
        $ids = explode(",",Yii::$app->request->post('id'));
        foreach($ids as $k=>$v){
            $model = Point::getById($v);
            $model->save($model->is_shield=false);
        }
        return $model->save();
    }

    public function actionIndex_old()
    {
        $unit=new Unit();
        $protocol=new Protocol();
        $searchModel = new PointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $model=new Point();
        $data=$model->find()->asArray()->all();
        //处理$data 把名字的中英文分开
        foreach($data as $key=>$value){
            $name=json_decode($data[$key]['name'],1);
            if(array_key_exists('zh-cn',$name['data']))
            $data[$key]['zh-name']=$name['data']['zh-cn'];
            else  $data[$key]['zh-name']=null;
            if(array_key_exists('en-us',$name['data']))
            $data[$key]['en-name']=$name['data']['en-us'];
            else $data[$key]['en-name']=null;
        }
        $data=json_encode($data);
        $unit=BaseArrayHelper::map($unit->find()->asArray()->all(),'id','name');
        $protocol=BaseArrayHelper::map($protocol->find()->asArray()->all(),'id','name');
        $unit=json_encode($unit);
        $protocol=json_encode($protocol);
        return $this->render('index', [
            'data'=>$data,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'unit'=>$unit,
            'protocol'=>$protocol,
        ]);
    }


    public function actionAjax(){
        $data=$_POST['data'];

        //参数1强制转化为数组  否则为对象

        $data=json_decode($data,1);
        $data=['Point'=>$data];
        $data1=Array([
            'Point'=>[
                'is_shield'=>true,
            ]
        ]);
        //这两个不剔除  save 方法不能执行
        unset($data['Point']['is_shield']);
        unset($data['Point']['zh-name']);
        unset($data['Point']['en-name']);
        unset($data['Point']['is_upload']);
        $id=$data['Point']['id'];
        $model = $this->findModel($id);
        $model->load($data);
        $result=$model->save();
        echo $result;
    }
    /**
     * Displays a single Point model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Point model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new  Point;

        if ($model->load(Yii::$app->request->post()) && $result = $model->saveDemoPoint()) {
            /*操作日志*/
            $info = MyFunc::DisposeJSON($model->name);
            $op['zh'] = '创建统计点位';
            $op['en'] = 'Create demo points';
            @$this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            //获取所有标准单位
            $unit = Unit::getAllUnit();
            $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo());

            $tree_json = json_encode($tree_data);
            return $this->render('create', [
                'model' => $model,
                'unit' => $unit,
                'tree' => $tree_data,
                'tree_json' => $tree_json,
            ]);
        }
    }


    /**
     * Updates an existing Point model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $query_params=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($query_params);
//        die;
        if(!empty($query_params)) {
            $location_id = $query_params['Point']['category_id1'];
            $query_params['Point']['location_id'] = $location_id;
            unset($query_params['Point']['category_id1']);
        }
        if ($model->load($query_params) && $model->savePoint()) {
                return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo());
            $location_tree_data = MyFunc::get_fancyTree_data(LocationCategory::getCategoryInfo());
            $model->filter_max_value = MyFunc::JsonFindValue($model->value_filter, 'data.max');
            $model->filter_min_value =MyFunc::JsonFindValue($model->value_filter, 'data.min');

            $tree_json = json_encode($tree_data);
            $location_tree_json=json_encode($location_tree_data);
            return $this->render('update', [
                'model' => $model,
                'unit' => $unit,
                'tree' => $tree_data,
                'tree_json' => $tree_json,
                'location_tree_json'=>$location_tree_json
            ]);
        }
    }

    /**
     * 根据点位的模式找到对应点位表 放入点位回收站.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @param $protocol_id
     */
    public function actionShield($id, $protocol_id = '')
    {
        switch($protocol_id) {
            //4代表计算点位
            case '4':
                $m_point = 'backend\models\CalculatePoint';
                break;
            //1代表bacnet点位
            case '1':
                $m_point = 'backend\models\BacnetPoint';
                break;
            //7代表模拟点位
            case '7':
                $m_point = 'backend\models\SimulatePoint';
                break;
            //100代demo点位
            case '100':
                $m_point = 'backend\models\Point';
                break;
            default:
                $m_point = 'backend\models\Point';
        }
        //$m_point = 'backend\models\Point';
        new $m_point;
        $model = $this->typeModel($m_point,$id);
        $model->save($model->is_shield=true);
        return $this->redirect(['index']);
    }

    protected function typeModel($m_point,$id)
    {
        if (($model = $m_point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Point model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Point the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 根据点位的模式 对不同点为进行更新
     * @param $id
     * @param $protocol_id
     */
    public function actionPointUpdate($id, $protocol_id = '')
    {
        switch($protocol_id) {
            //4代表计算点位
            case '4':
                $url = '/calculate-point/crud/update';
                break;
            //1代表bacnet点位
            case '1':
                $url = '/point/bacnet-crud/update';
                break;
            //7代表模拟点位
            case '7':
                $url = '/simulate-point/crud/update';
                break;
            //100代demo点位
            case '100':
                $url = '/demo-point/crud/update';
                break;
            default:
                $url = 'point/crud/update';
        }
        return $this->redirect([Url::toRoute([$url] + Yii::$app->request->get())]);
    }


    /**
     * 点位查看 报表方法
     * @param $id
     * @return string
     */
    public function actionReportTable($id)
    {
        $point_data_info = self::getPointData(Yii::$app->request->getQueryParams());
        return $this->render('report_table_index', [
            'dataProvider' => $point_data_info['dataProvider'],
            'name' => $point_data_info['name'],
            'unit' => $point_data_info['unit'],
            'params' => $point_data_info['params']
        ]);
    }


    public function actionGetBacnetPointData()
    {
        $controller_id = Yii::$app->request->get('controller_id');
        $con = " controller_id = $controller_id ";
        $db = Yii::$app->db;

        $table = 'protocol.bacnet_point';
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                        return 'row_'.$d;
                    },
                'dj' =>'id'
            ),
            //
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
            // array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),

            // array( 'db' => "value_filter->'data'->>'min'as min", 'dt' => 'min' ,'dj'=>"value_filter->'data'->>'min'" ),
            //array( 'db' => "value_filter->'data'->>'max' as max", 'dt' => 'max' ,'dj'=>"value_filter->'data'->>'max'" ),
            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
            array( 'db' => 'value_timestamp',   'dt' => 'value_timestamp' ,'dj'=>"value_timestamp" ),
            array( 'db' => 'src_name',   'dt' => 'src_name' ,'dj'=>"src_name" ),
            array( 'db' => 'data_count',  'dt' => 'data_count',  'dj' => 'data_count' ),
            array( 'db' => 'is_shield',   'dt' => 'is_shield' ,'dj'=>"is_shield" ),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        //$unit=new Unit();
        //$unit=BaseArrayHelper::map($unit->find()->orderBy('category_id asc, id asc')->asArray()->all(),'id','name');
        // for($i=0; $i<count($result['data']); $i++){
        // foreach($unit as $k => $v){
        // if($result['data'][$i]['unit'] == $k){
        //  $result['data'][$i]['unit'] = $v;
        // break;
        // }
        // }
        // }
        // $p_e=new PointEvent();
        // for($i=0; $i<count($result['data']); $i++){
        // $h_e = $p_e->getEventAndRule($result['data'][$i]['id']);
        //empty($h_e)?$result['data'][$i]['have_event']="NO":$result['data'][$i]['have_event']="YES";
        // }
        echo json_encode( $result);
        //echo "<pre>";print_r($con);die;
    }

    /**
     * 获取数据方法
     * @param $id
     * @param $start_time
     * @param $end_time
     * @return \yii\data\ActiveDataProvider
     */
    public static function getPointData($params)
    {
        $id = $params['id'];
        $params['start_time'] = isset($params['start_time']) && !empty($params['start_time']) ? $params['start_time'] : date('Y-m-d 00:00');
        $params['end_time'] = isset($params['end_time']) && !empty($params['end_time']) ? $params['end_time'] : date('Y-m-d 23:59');
        $searchModel = new PointDataSearch;
        $dataProvider = $searchModel->_search($params['id'], $params['start_time'], $params['end_time']);
        //获取点位名称 点位单位
        $point_info = Point::getPointUnitById($id)[0];
        $name = MyFunc::DisposeJSON($point_info['name']);
        $unit = isset($point_info['unit']['name']) ? $point_info['unit']['name'] : '';
        return [
            'name' => $name,
            'unit' => $unit,
            'dataProvider' => $dataProvider,
            'params' => $params
        ];
    }

    /**
     * 使用自定义报表的查询
     * @return string
     */
    public function actionReportChart($id)
    {
        $params = Yii::$app->request->getQueryParams();
        $params['point_id'] = explode(',', $params['id']);
        $params['start_time'] = isset($params['start_time']) && !empty($params['start_time']) ? strtotime($params['start_time']) : strtotime(date('Y-m-d 00:00'));
        $params['end_time'] = isset($params['end_time']) && !empty($params['end_time']) ? strtotime($params['end_time']) : strtotime(date('Y-m-d 23:59'));

        $data_info = \backend\module\custom_report\controllers\CrudController::getDataByTimeRange($params);
        $data = json_decode($data_info, true);
        $params['start_time'] = date('Y-m-d', $data['param']['start_time']);
        $params['end_time'] = date('Y-m-d', $data['param']['end_time']);
        return $this->render('report_chart_index', [
            'point_data' => json_encode($data['chart']),
            'params' => $params,
            'point_id' => $id
        ]);
    }

    public function actionAjaxChangeChart()
    {
        $params = Yii::$app->request->getQueryParams();
        $params['point_id'] = explode(',', $params['point_id']);
        return \backend\module\custom_report\controllers\CrudController::getDataByTimeRange($params);
    }

    /**
     * 导出
     */
    public function actionExport()
    {
        $point_data_info = self::getPointData(Yii::$app->request->getQueryParams());
        $point_data = $point_data_info['dataProvider']->query->asArray()->all();
        $report_name = $point_data_info['name'].'数据报表(' .$point_data_info['params']['start_time'] .'——' .$point_data_info['params']['end_time'] .')';
        //设置php 运行时间
        set_time_limit(0);
        //设置缓存大小
        ini_set('memory_limit','2048M');
        MyExport::run($point_data,[['时间', '数值(' .$point_data_info['unit'] .')']], ['save_file' => ['file_name' => $report_name]]);
    }

//查询collector 以及device得到属性结构 来给backent点位分类
    public static function getColDevTree(){
        $collector=Collector::find()->select(['collector_id','name'])->asArray()->all();
        $collector=MyFunc::map($collector,'collector_id','name');
        $controller=BacnetController::find()->select(['id','name','src_eid','object_instance_number'])->asArray()->all();
        $treeData=[];
        foreach ($collector as $key=>$value) {
            $treeData[$key]['name']=$value;
            $treeData[$key]['type']='collector';
        }

        foreach ($controller as $key=>$value) {
            $value['name']=$value['object_instance_number'].'('.MyFunc::DisposeJSON($value['name']).')';
//            unset($value['name']);
            $value['type']='device';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children'][]=$value;
        }

        return json_encode($treeData);
    }
}
