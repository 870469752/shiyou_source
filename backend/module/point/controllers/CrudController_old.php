<?php

namespace backend\module\point\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\Point;
use backend\models\Category;
use backend\models\PointCategoryRel;
use backend\models\search\PointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Unit;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
//        $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());

        return $this->render(Yii::$app->request->get('view', 'index'), [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
//            'category_tree' => $category_tree,
        ]);
    }

    /**
     * 暂时还不可用
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Point;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Point model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // 不知是否可以用ORM关系做？
            PointCategoryRel::deleteAll(['point_id' => $id]); // 删除关联
            $PointCategoryRel = new PointCategoryRel();
            // 循环添加关联（代码待修改）
            foreach($model->category_id as $v)
            {
                $attributes['point_id'] = $id;
                $attributes['category_id'] = $v;
                $_PointCategoryRel = clone $PointCategoryRel;
                $_PointCategoryRel->setAttributes($attributes);
                $_PointCategoryRel->save();
            }
            // 跳转到相应链接
            $get = Yii::$app->request->get();
            unset($get['id']);
            return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
        } else {
            // 把分类关系表的分类ID和类型都载入进来
            $PointCategoryRel = PointCategoryRel::find()->where(['point_id' => $id])->all();
            $model->category_id = MyFunc::arrayTwoOne($PointCategoryRel, 'category_id');

            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());

            $unit = Unit::getAllUnit();
            return $this->render('update', [
                'model' => $model,
                'category_tree' => $category_tree,
                'unit' => $unit,
            ]);
        }
    }

    public function actionShield($id)
    {
        $model = $this->findModel($id);
        $model->is_shield = true;
        $model->save();
        $get = Yii::$app->request->get();
        unset($get['id']);
        // 跳转到相应连接中
        return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
    }

    /**
     * Finds the Point model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Point the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
