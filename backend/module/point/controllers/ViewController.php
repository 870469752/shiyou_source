<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-7-2
 * Time: 上午9:48
 * 此类继承自 自定义报表
 */

namespace backend\module\point\controllers;

//系统类引用
use Yii;
use backend\controllers\MyController;
use backend\module\custom_report\controllers\CrudController;

//模型类
use backend\models\Point;
use backend\models\CustomReport;

//帮助类
use common\library\MyFunc;
use common\library\MyExport;

class ViewController extends CrudController
{
    /**
     * 点位数据表格展示
     * @return string
     */
    public function actionPointView()
    {
        $point_info = MyFunc::_map(Point::getAvailablePoint(), 'id', 'name');
        return $this->render('point_view',[
                'point_info' => $point_info
            ]);
    }

    /**
     * 使用提交按钮来获取数据
     * @return bool
     */
    public function actionAjaxChartView()
    {
        $params = Yii::$app->request->getQueryParams();
        /* 时间处理包括 开始时间 和 结束时间*/
        $start_time = self::timeProcess($params, 'start_time');
        $end_time = self::timeProcess($params, 'end_time');
        $params['start_time'] = strtotime($start_time);
        $params['end_time'] = strtotime($end_time);
        //如果初试时间 大于 结束时间 那么就直接提示错误
        if($params['end_time'] - $params['start_time'] <= 0){
            return false;
        }
        return parent::getDataByTimeRange($params);
    }

    /**
     * 利用StockChart 的 afterSetExtremes 进行数据更新
     */
    public function actionAjaxChangeChart()
    {
        $params = Yii::$app->request->getQueryParams();
        $params['point_id'] = explode(',', $params['point_id']);
        return parent::getDataByTimeRange($params);
    }



    public function actionExportPointView()
    {
        $params = Yii::$app->request->getQueryParams();
        $result_data = $this->actionAjaxChangeChart();
        $table_data = json_decode($result_data, true)['table'];
        MyExport::run($table_data['tbody'],[$table_data['thead']], ['save_file' => ['file_name' => '点位查看报表']]);
    }

    /**
     * 保存为自定义报表
     */
    public function actionSaveToCustomReport()
    {
        $params = Yii::$app->request->getQueryParams();
        $params['left_point'] = $params['point_id']; unset($params['point_id']);
        $params['name'] = $params['custom_report_name']; unset($params['custom_report_name']);
        $model = new CustomReport();
        if($model->load($params, '') && $model->customReportSave()){
            return true;
        }
        return false;
    }
} 