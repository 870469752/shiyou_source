<?php

namespace backend\module\point\controllers;

use backend\models\Point;
use Yii;
use backend\models\BacnetPoint;
use backend\models\search\BacnetPointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\Category;
use backend\models\PointCategoryRel;
use backend\models\Unit;
use vendor\PHPExcel\PHPExcel\PHPExcel_IOFactory;
use yii\web\UploadedFile;

/**
 * BacnetCrudController implements the CRUD actions for BacnetPoint model.
 */
class BacnetCrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BacnetPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BacnetPointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $category_tree = MyFunc::TreeData(Category::find()->asArray()->all());

        return $this->render(Yii::$app->request->get('view', 'index'), [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'category_tree' => $category_tree,
        ]);
    }

    public function actionTranslate()
    {
        $this->_translate('translate.xls');
        return $this->redirect(['index'] + Yii::$app->request->get());
    }

    public function actionUploadTranslate()
    {
        $translate = UploadedFile::getInstanceByName('translate');
        $path = YII::$app->params['uploadPath'] . $translate->baseName . '.' . $translate->extension;
        if ($translate->error == 0) {
            $translate->saveAs($path);
        }
        $this->_translate($path);
        $get = Yii::$app->request->post();
        unset($get['_csrf']);
        return $this->redirect(['index'] + $get);
    }

    /**
     * 点位翻译方法
     * @param $path EXCEL文件路径
     */
    public function _translate($path)
    {
        $arr = PHPExcel_IOFactory::load($path)->getActiveSheet()->toArray();
        foreach($arr as $k => $v){
            if($bacnet_point = BacnetPoint::findOne(['object_name' => $v[0]])){
                $bacnet_point->name = json_encode([
                    'data_type' => 'description',
                    'data' => [
                        'zh-cn' => @$v[1],
                        'en-us' => @$v[2]
                    ],
                ], JSON_UNESCAPED_UNICODE);
                $bacnet_point->save();
            }
        }
    }

    /**
     * Updates an existing BacnetPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $protocol_id = '')
    {
        $model = $this->findModel($id);
        $params = Yii::$app->request->getBodyParams();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // 不知是否可以用ORM关系做？
            PointCategoryRel::deleteAll(['point_id' => $id]); // 删除关联
            $PointCategoryRel = new PointCategoryRel();
            // 循环添加关联（代码待修改）
            foreach($model->category_id as $v)
            {
                $attributes['point_id'] = $id;
                $attributes['category_id'] = $v;
                $_PointCategoryRel = clone $PointCategoryRel;
                $_PointCategoryRel->setAttributes($attributes);
                $_PointCategoryRel->save();
            }
            // 跳转到相应链接
            $get = Yii::$app->request->get();
            unset($get['id']);
            //判断当前修改是哪个控制器请求的 如果protocol_id不为空说明来自point/crud的请求在执行完毕后跳转回point/crud,否则跳到当前控制器
            if($params['protocol_id']) {
                return $this->redirect(['/point/crud']);
            } else {
                return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
            }
        } else {
            // 把分类关系表的分类ID和类型都载入进来
            $PointCategoryRel = PointCategoryRel::find()->where(['point_id' => $id])->all();
            $model->category_id = MyFunc::arrayTwoOne($PointCategoryRel, 'category_id');

            $category_tree = MyFunc::TreeData(Category::find()->asArray()->indexBy('id')->all());
            $allUnit = Unit::getAllUnit();
            return $this->render('update', [
                'protocol_id' => $protocol_id,
                'model' => $model,
                'category_tree' => $category_tree,
                'allUnit' => $allUnit
            ]);
        }
    }

    public function actionShield($id)
    {
        $model = $this->findModel($id);
        $model->is_shield = true;
        $model->save();
        $get = Yii::$app->request->get();
        unset($get['id']);
        // 跳转到相应连接中
        return $this->redirect(['/'.$this->module->id.'/'.$this->id] + $get);
    }

    /**
     * Finds the BacnetPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BacnetPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BacnetPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
