<?php

namespace backend\module\point\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\Point;
use backend\models\Category;
use backend\models\PointCategoryRel;
use backend\models\search\PointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for Point model.
 */
class RecoveryController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->query->where(['is_shield' => true, 'is_available' => true]);
        $category_tree = MyFunc::TreeData(Category::find()->asArray()->all());

        return $this->render(Yii::$app->request->get('view', 'index'), [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'category_tree' => $category_tree,
        ]);
    }

    public function actionRecovery($id)
    {
        $model = $this->findModel($id);
        $model->is_shield = false;
        $model->save();
        $get = Yii::$app->request->get();
        unset($get['id']);
        // 跳转到相应连接中
        return $this->redirect(['/point/recovery'] + $get);
    }

    /**
     * Finds the Point model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Point the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
