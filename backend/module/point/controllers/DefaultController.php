<?php

namespace backend\module\point\controllers;

use backend\models\Category;
use backend\models\CustomFeature;
use backend\models\form\PointDataForm;
use backend\models\Menu;
use common\library\MyArrayHelper;
use Yii;
use backend\controllers\MyController;
use backend\models\Point;
use backend\models\search\PointSearch;
use backend\models\PointData;
use backend\models\search\PointDataSearch;
use common\library\MyFunc;
use common\library\MyExport;
class DefaultController extends MyController
{
    public function actionIndex(){
        $this->layout = '/onlyContent';
        $searchModel = new PointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $category_tree = MyFunc::TreeData(Category::find()->asArray()->all());
        return $this->render(Yii::$app->request->get('view', 'index'), [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'category_tree' => $category_tree,
        ]);
    }

    public function actionCondition()
    {
        $model = new PointData();
        $model->validate();
        return $this->render('condition', [
            'model' => $model
        ]);
    }

    /**
     * 点位数据表格展示
     * @return string
     */
    public function actionView()
    {
        // 自定义功能页表单
        $model = new CustomFeature();
        // 选择点位和报表类型的表单
        $searchModel = new PointDataForm();
        // 把表单提交的数据载入
        $searchModel->load(Yii::$app->request->get());
        // 制造图形展示的多轴
        $axis_json = MyArrayHelper::highchartsMultiUnit(Point::findPointUnit($searchModel->point_id));
        return $this->render('view', [
            'axis_json' => $axis_json,
            // 菜单树数组
            'menu' => Menu::findTreeMenu(),
            'searchModel' => $searchModel,
            'model' => $model
        ]);
    }

    /**
     * 表格数据
     * @return string
     */
    public function actionTable()
    {
        $this->layout = '/onlyContent';
        // 点位数据搜索表单
        $searchModel = new PointDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('table', [
            'dataProvider' => $dataProvider,
            // 创建表格列名称
            'col' => $searchModel->createColumn(),
            'searchModel' => $searchModel
        ]);
    }

    /**
     * 导出
     */
    public function actionExportData()
    {
        $get = Yii::$app->request->get();
        $point_ids = explode(',',$get['point_id']);
        $pModel = new PointData();
        $data = $pModel -> ColRowTransition($point_ids,$get['start_time'],$get['end_time'],'','')->asArray()->all();
        $head[] = '时间';
        foreach($point_ids as $v){
            $pName = MyFunc::DisposeJSON(Point::getById($v)->name);
            $head[] = $pName;
        }
        $header = [$head];
        $options = [
            'save_file' => [
                'file_name' => '点位数据'.date('Ymd'),
                'file_type' => 'xls',
            ],
        ];
        MyExport::run($data,$header,$options);
    }
    /**
     * 缩放图形时异步载入
     */
    public function actionAjaxPointData()
    {
        $model = new PointDataForm;
        $get = Yii::$app->request->get();
        $model->load($get,'');
        $Point = $model->findChartData();
        foreach($Point as $k => $row){
            $point_name[$row['id']] = MyFunc::DisposeJSON($row['name']);
            $result[$row['id']] = $row['pointData'];
            switch($row['value_type']){
                case '1': // 如果是累计就区域图
                    $chart[$row['id']] = 'area';
                    break;
                case '2': // 如果是开关就栈图
                    $chart[$row['id']] = 'step';
                    break;
                default: // 默认折线图
                    $chart[$row['id']] = '';
            }
            // 如果页面上有设置过图形参数就按照页面的来
            $chart[$row['id']] = !empty($get['chart'][$row['id']])?$get['chart'][$row['id']]:$chart[$row['id']];
        }
        // 好像有自带JSON和JSONP跨域请求，但是不知道怎么用
        echo isset($result)?MyFunc::HighChartsData($result, $point_name, $chart, $get['axis_point']):'[]';
    }
}
