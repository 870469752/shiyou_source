<?php

use yii\helpers\Html;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 */

$this->title = Yii::t('point', 'Update {modelClass}: ', [
  'modelClass' => 'Point',
]) . MyFunc::DisposeJSON($model->name);
?>
    <?= $this->render('_form'.ucfirst(Yii::$app->request->get('view', '')), [
        'model' => $model,
        'category_tree' => $category_tree,
        'unit' => $unit,
    ]) ?>
