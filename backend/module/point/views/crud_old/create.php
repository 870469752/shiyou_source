<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 */
$this->title = Yii::t('point', 'Create {modelClass}', [
  'modelClass' => 'Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
