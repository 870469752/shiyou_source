<?php

use common\library\MyHtml;
use yii\grid\GridView;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row" >
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-colorbutton="false"
                 data-widget-editbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                <!-- widget div-->
                    <?=
                    GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover table-middle no-margin'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [

                            ['attribute' => 'name', 'format' => 'JSON'],
                            ['attribute' => 'interval', 'format' => 'TimeUnitTransition', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'last_update', 'format' => 'datetime', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'is_upload', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'category', 'format' => ['array', 'index' => 'name'], 'headerOptions' => ['data-hide' => 'phone']],

                            ['class' => 'common\library\MyActionColumn', 'template' => '{update} {shield}', 'headerOptions' => ['data-class' => 'expand']],
                        ],
                    ]); ?>

            </div>
    </div>
    </article>
    </div>
</section>
