<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Unit;
/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 * @var yii\widgets\ActiveForm $form
 */

?>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- START ROW -->
        <div class="row">

            <!-- NEW COL START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                        <h2><?= MyHtml::encode($this->title) ?></h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <?php $form = MyActiveForm::begin(); ?>
                            <fieldset>

                                <?php $name_data = ArrayHelper::getValue($model, 'name'); ?>
                                <?= $form->field($model, 'name')->textInput(['name' => 'null', 'value' => MyFunc::DisposeJSON($name_data)])?>
                                <?= MyHtml::hiddenInput($model->formName().'[name]', $name_data) ?>

                                <?= $form->field($model, 'base_value')->textInput() ?>

                                <?= $form->field($model, 'interval')->dropDownList([
                                    '60'=>'1 '.Yii::t('app', 'Minute'),
                                    '300'=>'5 '.Yii::t('app', 'Minute'),
                                    '600'=>'10 '.Yii::t('app', 'Minute'),
                                    '1800'=>'30 '.Yii::t('app', 'Minute'),
                                    '3600'=>'1 '.Yii::t('app', 'Hour'),
                                    '18000'=>'5 '.Yii::t('app', 'Hour'),
                                    '43200'=>'12 '.Yii::t('app', 'Hour'),
                                    '86400'=>'24 '.Yii::t('app', 'Hour'),
                                ],['class' => 'select2']) ?>

                                <?= $form->field($model, 'value_type')->dropDownList([
                                    '0'=> Yii::t('app', '实时'),
                                    '1'=> Yii::t('app', '累计'),
                                    '2'=> Yii::t('app', '开关'),
                                ],['class' => 'select2']) ?>

                                <!--在系统中选择 点位单位-->
                                <?=$form->field($model, 'unit')->dropDownList(
                                    Unit::getAllUnit(),
                                    ['class'=>'select2', 'style' => 'width:200px']
                                )?>


                                <?= $form->field($model, 'is_upload')->checkbox() ?>
                            </fieldset>
                            <div class="form-actions">
                                <?= MyHtml::submitButton($model->isNewRecord ? Yii::t('point', 'Create') : Yii::t('point', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>

                            <?php MyActiveForm::end(); ?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>
    </section>
<?php
$this->registerJs ( "edit_name('".$model->formName()."','".Yii::$app->language."');");
?>