<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 */

$this->title = MyFunc::DisposeJSON($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('point', 'Points'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('point', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('point', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('point', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'name:JSON',
            'interval:TimeUnitTransition',
            'last_update:date',
            'base_value',
            'is_shield:boolean',
            'is_available:boolean',
            'is_upload:boolean',
        ],
    ]) ?>

</div>
