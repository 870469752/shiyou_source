<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'PointData');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = ActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>对比参数设定</legend>
                <!--时间选择-->
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('id', Yii::$app->request->get('id'))?>
                    <div class="col-md-4">
                        <?=Html::textInput('start_time', isset($param['start_time'])?$param['start_time']:null,
                            ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Start Time'), 'id' => '_start_time', 'rel' => 'popover', 'data-placement' => 'top', 'data-original-title' => '开始时间不能大于结束时间'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::textInput('end_time', isset($param['end_time'])?$param['end_time']:null,  ['class' => 'form-control', 'placeholder' => Yii::t('app', 'End Time'), 'id' => '_end_time'])?>
                    </div>
                </div>
            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= (isset($name) ? $name : '') . '__点位报表查看' ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <div id="container_chart" style="height:500px;"></div>
                    <!-- 搜索表单和JS -->
                </div>
            </div>
        </article>
    </div>
</section>

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<script>
    window.onload = function() {
        var $start_time = <?= isset($params['start_time']) ? "'" .$params['start_time'] ."'" : "''"?>;
        var $end_time = <?= isset($params['end_time']) ? "'" .$params['end_time'] ."'" : "''"?>;
        var $change_url = '<?=Url::toRoute('ajax-change-chart')?>';
        var $point_id = <?=isset($point_id) ? $point_id : "''"?>;
        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
            }
            $("#_loop").slideToggle("slow");

        });

        highcharts_lang_date();
        function afterSetExtremes(e){
            $chart.showLoading('Loading data from server...');
            var $params = '?point_id=' + $point_id + '&start_time=' + Math.ceil(e.min/1000) + '&end_time=' + Math.ceil(e.max/1000);
            $.getJSON($change_url + $params,
                function (result) {
                    $data = result;
                    // 他必须得一组一组数据插入
                    for (var i = 0; i < result.chart.length; i++) {
                        $chart.series[i].setData(result.chart[i].data);
                    }
//                    ajaxGetTable($data.table);
//                    triggerAfterAjax($('#container_table'), $data.table, Math.ceil($data.table.total/$_page_num));
//                    $('#_save').attr('href', $export_url + $params);
                    $chart.hideLoading();
                });
        }


            $chart = new Highcharts.StockChart({
                chart: {
                    renderTo: 'container_chart',
                    borderWidth: 0,
                    borderRadius: 0,
                    type: 'spline',
                    zoomType: 'x'
                },
                title: {
                    text: '点位查看',
                    align: 'center',
                },
                credits : {
                    enabled : false
                },
                scrollbar: {
                    liveRedraw: false
                },
                legend: {
                    enabled: true,
                    align: 'center',
                    verticalAlign: 'bottom',
                    borderWidth: 2,
                    itemStyle: {
                        fontSize: '14px'
                    }
                },
                rangeSelector : {
                    enabled:false
                },
                navigator : {
                    adaptToUpdatedData: false,
                    series : <?=$point_data?>
                },
                xAxis : {
                    events : {
                        afterSetExtremes : afterSetExtremes
                    },
                    minRange: 3600 * 1000
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    },
                    opposite: false
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    valueDecimals: 2,
                    valueDecimals: 2,
                    shared : true,
                    style : {
                        //color : 'white',
                        padding : '7px',
                        fontsize : '9pt'
                    }
                },
                series: <?=$point_data?>
            });

        $('#_submit').click(function() {
            var $start_time = $('#_start_time').val();
            var $end_time = $('#_end_time').val();
            if($start_time >= $end_time) {
                $('#_start_time').trigger('click');
                return false
            }
            return true;
        });

        timeTypeChange('day', $('#_start_time'));
        timeTypeChange('day', $('#_end_time'));
        $('#_start_time').val($start_time);
        $('#_end_time').val($end_time);
    }
    </script>