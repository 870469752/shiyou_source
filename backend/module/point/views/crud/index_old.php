<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Points');
$this->params['breadcrumbs'][] = $this->title;

?>

<section id="widget-grid" class="">
	<div class="well well-sm well-light">
		<div id="tabs" >
			<ul>
				<li>
					<a href="#tabs-all1">所有点位</a>
				</li>
				<li>
					<a href="#tabs-modbus1">Modbus点位</a>
				</li>
				<li>
					<a href="#tabs-bacnet1">Bacnet点位</a>
				</li>
			</ul>

			<div id="tabs-all1" style="width: 100%;height: 800px">
				<table id="tabs-all" ></table>
				<div id="tabs-all2"></div>
			</div>
			<div id="tabs-modbus1">
				<table id="tabs-modbus"></table>
					<div id="tabs-modbus2"></div>
			</div >

			<div id="tabs-bacnet1">
				<table id="tabs-bacnet"></table>
					<div id="tabs-bacnet2"></div>
			</div>
		</div>
	</div>
</section>


<table id="tabs-test"></table>
<div id="tabs-test2"></div>

<!---->
<!--<table id="tabs-modbus"></table>-->
<!--<div id="pjqgrid-modbus"></div>-->
<!--<!-- widget grid -->
<!--<section id="widget-grid" class="">-->
<!---->
<!--	<!-- row -->
<!--	<div class="row">-->
<!---->
<!--		<!-- NEW WIDGET START -->
<!--		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<!---->
<!--			<table id="jqgrid"></table>-->
<!--			<div id="pjqgrid"></div>-->
<!---->
<!--<!--			<br>-->
<!--<!--			<a href="javascript:void(0)" id="m1">Get Selected id's</a><br>-->
<!--<!--			<a href="javascript:void(0)" id="m1s">Select(Unselect) row 13</a>-->
<!---->
<!--		</article>-->
<!--		<!-- WIDGET END -->
<!---->
<!--	</div>-->
<!---->
<!--	<!-- end row -->
<!---->
<!--</section>-->
<!-- end widget grid -->
<?php
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/jquery.jqGrid.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/grid.locale-en.min.js", ['backend\assets\AppAsset']);
?>

<script>
//	function zh_dejson(el, cellval, opts) {
//		//return el;
//	}
//	function zh_enjson(el, cellval, opts) {
//		return a;
//	}

	function isArray(obj) {
		return Object.prototype.toString.call(obj) === '[object Array]';
	}


	//自定义 编辑 保存  等按钮
	function editRow(element,cl){
		jQuery('#'+element).editRow(cl);
		//console.log(element);console.log(cl);
	}

	function saveRow(element,cl){
		//全部数据
//		console.log(jQuery('#' + element).jqGrid('getGridParam'));
		if(cl!=0) {
			jQuery('#' + element).saveRow(cl);
			var data = new Array();
			data = jQuery('#' + element).jqGrid('getRowData', cl);
			//console.log(data);
			//修改
			var zh_name=data['zh-name'];
			var en_name=data['en-name'];
			 var name= {
				"data_type": "description",
				"data": {
					"zh-cn": zh_name,
					"en-us": en_name
				}
			};
			data['name']=JSON.stringify(name);
			//console.log(data);
			data = JSON.stringify(data);
			//console.log(data);
			//调试用
			//window.location.href="crud/ajax?data="+data;
			$.ajax({
				type: "POST",
				url: "/point/crud/ajax",
				data: {data: data},
				//暂时未加判定是否成功插入
				success: function (msg) {
					if (msg == 1)
						console.log('id=' + cl + '保存成功');
					else console.log('id=' + cl + '保存失败');
				}
			});
		}

	}
	function restoreRow(element,cl){
		jQuery('#'+element).restoreRow(cl);
		console.log("restore");
	}
	function detaileRow(cl){
		window.location.href="/point/crud/update?id="+cl;

	}
	function dataEvent(){

	}

	window.onload = function(){
		$('#tabs').tabs();
		var lastsel2=0;
		var jqgrid_data =<?=$data?>;
		console.log(jqgrid_data);
		var unit_category=<?=$unit?>;
		var protocol=<?=$protocol?>;
		//处理得到modbus  bacnet 点位数据
		var bacnetdata=Array();
		var modbusdata=Array();
		//{1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate", 100: "demo"}
		//分类数据  按 协议类型分
		for(var data in jqgrid_data){
			//console.log(jqgrid_data[data].protocol_id);
			switch (jqgrid_data[data].protocol_id){
				case 1:bacnetdata.push(jqgrid_data[data]);break;
				case 2:modbusdata.push(jqgrid_data[data]);break;
			}
		}




		init('tabs-all',jqgrid_data);
		//隐藏不需要展示的列  		colname colnames初始化后不能改变，，
		$("#tabs-all").setGridParam().hideCol("en-name").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("base_value").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("base_history").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("is_upload").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("is_record").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("is_system").trigger("reloadGrid");
		$("#tabs-all").setGridParam().hideCol("value_filter").trigger("reloadGrid");

		init('tabs-modbus',modbusdata);
		//隐藏不需要展示的列  		colname colnames初始化后不能改变，
		$("#tabs-modbus").setGridParam().hideCol("en-name").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("protocol_id").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("base_value").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("base_history").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("is_upload").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("is_record").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("is_system").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("value_filter").trigger("reloadGrid");

		init('tabs-bacnet',bacnetdata);
//		//隐藏不需要展示的列  		colname colnames初始化后不能改变，，
		$("#tabs-bacnet").setGridParam().hideCol("en-name").trigger("reloadGrid");
		$("#tabs-modbus").setGridParam().hideCol("protocol_id").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("base_value").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("base_history").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("is_upload").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("is_record").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("is_system").trigger("reloadGrid");
		$("#tabs-mdubus").setGridParam().hideCol("value_filter").trigger("reloadGrid");


//		$("#tabs-170").setGridParam().hideCol("name").trigger("reloadGrid");
//		$("#tabs-170").setGridParam().hideCol("name").trigger("reloadGrid");
		var id_data=Array();
		var olddata;
		var newdata
		var top_id;
		function init(element,data) {
			jQuery("#" + element).jqGrid({
				data: data,
				datatype: "local",
				width: 'auto',
				height: $("#tabs-all1").height(),
				colNames: ['Inv No', '名称', 'Name', '采集间隔', '协议类型', '基础值', '历史基础值', '是否可用',
					'是否屏蔽', '是否上传', '是否记录', '是否系统', '最新数据', ' 单位', '值类型', '过滤值', 'Actions'],
				colModel: [
					{name: 'id', index: 'id'},
					{name: 'zh-name', index: '名称', editable: true},//,unformat:zh_enjson},
					{name: 'en-name', index: 'Name', editable: true},//,unformat:zh_enjson},
					{name: 'interval', index: '采集间隔', align: "right", editable: true},
					{
						name: 'protocol_id',
						index: '协议类型',
						align: "right",
						editoptions: {value: protocol},
						formatter: 'select'
					},
					{name: 'base_value', index: '基础值', align: "right", editable: true},
					{name: 'base_history', index: '历史基础值', sortable: false, editable: true},
					{
						name: 'is_available',
						index: '是否可用',
						edittype: 'select',
						editoptions: {value: {true: '是', false: '否'}},
						formatter: 'select'
					},
					{
						name: 'is_shield',
						index: '是否屏蔽',
						editable: true,
						edittype: 'select',
						editoptions: {value: {true: '是', false: '否'}},
						formatter: 'select'
					},
					{name: 'is_upload', index: '是否上传', editable: true},
					{name: 'is_record', index: '是否记录', editable: true},
					{name: 'is_system', index: '是否系统', editable: true},
					{name: 'value', index: '最新数据'},
					{
						name: 'unit',
						index: '单位',
						editable: true,
						edittype: 'select',
						editoptions: {value: unit_category},
						formatter: 'select'
					},
					{name: 'value_filter', index: '过滤值', editable: true},
					{name: 'value_type', index: '值类型', editable: true},
					{name: 'act', index: 'act', sortable: false},
				],

				rowNum: 20,
				rowList: [20, 40, 60],
				pager: "#" + element + '2',
				sortname: 'id',
				toolbarfilter: true,
				viewrecords: true,
				sortorder: "asc",
				gridComplete: function () {
					var ids = jQuery("#" + element).jqGrid('getDataIDs');
					for (var i = 0; i < ids.length; i++) {
						var cl = ids[i];
//						be = "<button class='btn btn-xs btn-default' data-original-title='Edit Row' onclick=\"editRow('" + elemet + "','" + cl + "');\"><i class='fa fa-pencil'></i></button>";
//						se = "<button class='btn btn-xs btn-default' data-original-title='Save Row' onclick=\"saveRow('" + elemet + "','" + cl + "');\"><i class='fa fa-save'></i></button>";
//						ca = "<button class='btn btn-xs btn-default' data-original-title='Cancel' onclick=\"restoreRow('" + elemet + "','" + cl + "');\"><i class='fa fa-times'></i></button>";
						ua = "<button class='btn btn-xs btn-default' data-original-title='Cancel' onclick=\"detaileRow('" + cl + "');\"><i class='fa fa-plus-square'></i></button>";
						//ce = "<button class='btn btn-xs btn-default' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
						//jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ce});
						jQuery("#" + element).jqGrid('setRowData', ids[i], {act: ua});
					}
				},
				editurl: "/point/crud/jqgrid",//action页面crud/jqgrid
				caption: "Point",
				multiselect: false,
				autowidth: true,
				onSelectRow: function (id) {
					if (id && id !== lastsel2) {
						jQuery("#" + element).saveRow(lastsel2);
						jQuery("#" + element).restoreRow(lastsel2);
						//得到修改行数据
						newdata=jQuery('#' + element).jqGrid('getRowData', lastsel2);
						if(lastsel2!=0) {
							//比较值是否改变 改变了则保存
							for(var key in olddata){
								 if(newdata[key]!=olddata[key])
								 {
									 saveRow(element, lastsel2);
									 break;
								 }
							}
						}
						olddata=jQuery('#' + element).jqGrid('getRowData', id);
						jQuery("#" + element).editRow(id, true);
						lastsel2 = id;
					}
				}

			});
			//保存最后一行数据
			var Save = function () {
				//当前行保存并退出编辑
				jQuery("#" + element).saveRow(lastsel2);
				jQuery("#" + element).restoreRow(lastsel2);
				saveRow(element, lastsel2);
			};
			jQuery("#" + element).jqGrid('navGrid', "#" + element + '2', {
				edit: false,
				add: false,
				del: false
			})
				//自定义save按钮
				.navButtonAdd("#" + element + '2',
				{
					caption: " ",
					buttonicon: "ui-icon-disk",
					onClickButton: Save,
					position: "first"
				});



//			//删除时ajax删除数据
//			var Del = function () {
//				var id;
//				//得到选取的行的id  多个为id数组
//				id = jQuery("#"+element).jqGrid('getGridParam', 'selarrrow');
//				//ajax删除
//				$.ajax({
//					type: "POST",
//					url: "/point/crud/ajax",
//					data: {id: id, imagegroupid: imagegroupid},
//					success: function (msg) {
//						alert(msg);
//					}
//				});
//
//				alert(id);
//				//确定删除后 移除对应行
//				jQuery("#"+element).jqGrid('delGridRow', id);
//				console.log('success delete');
//			};

//			jQuery("#"+element).jqGrid('navGrid',"#"+element+'2', {
//				edit: false,
//				add: true,
//				delfunc: Del
//			});


//			jQuery("#"+element).jqGrid('inlineNav', "#pjqgrid1");
//			/* Add tooltips */
//			$('.navtable .ui-pg-button').tooltip({
//				container: 'body'
//			});


//			jQuery("#m1").click(function () {
//				var id;
//				//得到选取的行的id  多个为id数组
//				id = jQuery("#"+element).jqGrid('getGridParam', 'selarrrow');
//				var data;
//				// id  column 单元格内容
//				//data = $("#jqgrid").jqGrid('getCell',10,'name');
//				//id行内容
//				data = jQuery("#"+element).jqGrid('getRowData', id);//id不能为数组
//				alert(id);
//				console.log(data);
//			});
//
//			jQuery("#m1s").click(function () {
//				//jQuery("#jqgrid").jqGrid('setSelection', "13");
//				var ids = jQuery("#"+element).jqGrid('getGridParam', 'selarrrow');
//
//				console.log(ids[0]);
//				console.log(ids);
//			});

			// remove classes
			$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
			$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
			$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
			$(".ui-jqgrid-pager").removeClass("ui-state-default");
			$(".ui-jqgrid").removeClass("ui-widget-content");

			// add classes
			$(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
			$(".ui-jqgrid-btable").addClass("table table-bordered table-striped");


			$(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
			$(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
			$(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
			$(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
			$(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
			$(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
			$(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
			$(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

			$(".ui-icon.ui-icon-seek-prev").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

			$(".ui-icon.ui-icon-seek-first").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

			$(".ui-icon.ui-icon-seek-next").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

			$(".ui-icon.ui-icon-seek-end").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");

			$(window).on('resize.jqGrid', function () {
				$("#"+element).jqGrid('setGridWidth', $("#content").width()-50);
			})
		}


//		var data2=
//			[
//				{"CustomerID":"ALFKI","CompanyName":"Alfreds Futterkiste","Phone":"030-0074321","PostalCode":"12209","City":"Berlin"},
//				{"CustomerID":"ANATR","CompanyName":"Ana Trujillo Emparedados y helados","Phone":"(5) 555-4729","PostalCode":"050216","City":"M\u00e9xico D.F."},
//				{"CustomerID":"BLAUS","CompanyName":"Blauer See Delikatessen -U","Phone":"0621-084608","PostalCode":"68306","City":"Mannheim"},
//				{"CustomerID":"BONAP","CompanyName":"Bon app'","Phone":"91.24.45.40","PostalCode":"13008","City":"Marseille777"},
//				{"CustomerID":"CACTU","CompanyName":"Cactus Comidas para llevar","Phone":"(1) 135-5555","PostalCode":"1010","City":"Buenos Aires"},
//				{"CustomerID":"CENTC","CompanyName":"Centro comercial Moctezuma","Phone":"(5) 555-3392","PostalCode":"05022","City":"M\u00e9xico D.F."},
//				{"CustomerID":"CHOPS","CompanyName":"Chop-suey Chinese","Phone":"0452-076545","PostalCode":"3012","City":"Bern"},
//				{"CustomerID":"COMMI","CompanyName":"Com\u00e9rcio Mineiro","Phone":"(11) 555-7647","PostalCode":"05432-043","City":"S\u00e3o Paulo"},
//				{"CustomerID":"CONSH","CompanyName":"Consolidated Holdings","Phone":"(171) 555-2282","PostalCode":"WX1 6LT","City":"London"},
//				{"CustomerID":"DRACD","CompanyName":"Drachenblut Delikatessen","Phone":"0241-039123","PostalCode":"52066","City":"Aachen"},
//				{"CustomerID":"DUMON","CompanyName":"Du monde entier","Phone":"40.67.88.88","PostalCode":"44000","City":"Nantes"},
//				{"CustomerID":"EASTC","CompanyName":"Eastern Connection","Phone":"(171) 555-0297","PostalCode":"WX3 6FW","City":"London"},
//				{"CustomerID":"ERNSH","CompanyName":"Ernst Handel","Phone":"7675-3425","PostalCode":"8010","City":"Graz"},
//				{"CustomerID":"FAMIA","CompanyName":"Familia Arquibaldo","Phone":"(11) 555-9857","PostalCode":"05442-030","City":"S\u00e3o Paulo"},
//				{"CustomerID":"FISSA","CompanyName":"FISSA Fabrica Inter. Salchichas S.A.","Phone":"(91) 555 94 44","PostalCode":"28034","City":"Madrid"},
//				{"CustomerID":"FOLIG","CompanyName":"Folies gourmandes","Phone":"20.16.10.16","PostalCode":"59000","City":"Lille"},
//				{"CustomerID":"FOLKO","CompanyName":"Folk och f\u00e4 HB","Phone":"0695-34 67 21","PostalCode":"S-844 67","City":"Br\u00e4cke"},
//				{"CustomerID":"FRANK","CompanyName":"Frankenversand","Phone":"089-0877310","PostalCode":"80805","City":"M\u00fcnchen"},
//				{"CustomerID":"FRANR","CompanyName":"France restauration","Phone":"40.32.21.21","PostalCode":"44000","City":"Nantes"},
//				{"CustomerID":"FRANS","CompanyName":"Franchi S.p.A.","Phone":"011-4988260","PostalCode":"10100","City":"Torino"},
//				{"CustomerID":"FURIB","CompanyName":"Furia Bacalhau e Frutos do Mar","Phone":"(1) 354-2534","PostalCode":"1675","City":"Lisboa"},
//				{"CustomerID":"GALED","CompanyName":"Galer\u00eda del gastr\u00f3nomo","Phone":"(93) 203 4560","PostalCode":"08022","City":"Barcelona"},
//				{"CustomerID":"GODOS","CompanyName":"Godos Cocina T\u00edpica","Phone":"(95) 555 82 82","PostalCode":"41101","City":"Sevilla"},
//				{"CustomerID":"GOURL","CompanyName":"Gourmet Lanchonetes","Phone":"(11) 555-9482","PostalCode":"04876-786","City":"Campinas"},
//				{"CustomerID":"GREAL","CompanyName":"Great Lakes Food Market","Phone":"(503) 555-7555","PostalCode":"97403","City":"Eugene"},
//				{"CustomerID":"GROSR","CompanyName":"GROSELLA-Restaurante","Phone":"(2) 283-2951","PostalCode":"1081","City":"Caracas"},
//				{"CustomerID":"HANAR","CompanyName":"Hanari Carnes","Phone":"(21) 555-0091","PostalCode":"05454-876","City":"Rio de Janeiro"},
//				{"CustomerID":"hh","CompanyName":"h","Phone":"h","PostalCode":"h","City":"h"},
//				{"CustomerID":"HILAA","CompanyName":"HILARI\u00d3N-Abastos","Phone":"(5) 555-1340","PostalCode":"5022","City":"San Crist\u00f3bal"},
//				{"CustomerID":"HUNGC","CompanyName":"Hungry Coyote Import Store","Phone":"(503) 555-6874","PostalCode":"97827","City":"Elgin"}
//			];
//
//		data2=eval(data2);
//		$("#tabs-test").jqGrid({
//			data:data2,
//			datatype: "local",
////			url: 'data.json',
//			// we set the changes to be made at client side using predefined word clientArray
//			editurl: 'clientArray',
//			colModel: [
//				{label: 'Customer ID',name: 'CustomerID',width: 75,key: true,editable: true,editrules : { required: true}},
//				{label: 'Company Name',name: 'CompanyName',width: 140,editable: true
//					// must set editable to true if you want to make the field editable
//				},
//				{label : 'Phone',name: 'Phone',width: 100,editable: true},
//				{label: 'Postal Code',name: 'PostalCode',width: 80,editable: true},
//				{label: 'City',name: 'City',width: 140,editable: true}
//			],
//			sortname: 'CustomerID',
//			sortorder : 'asc',
//			loadonce: true,
//			onSelectRow: editRow,
//			viewrecords: true,
//			width: 780,
//			height: 200,
//			rowNum: 10,
//			pager: "#tabs-test2"
//		});
//
//
//		$("#tabs-test").navGrid("#tabs-test2",
//			// the buttons to appear on the toolbar of the grid
//			{
//				edit: true,
//				add: true,
//				del: true,
//				search: false,
//				refresh: false,
//				view: false,
//				position: "left",
//				cloneToTop: false
//			},
//			// options for the Edit Dialog
//			{
//				editCaption: "The Edit Dialog",
//				recreateForm: true,
//				checkOnUpdate : true,
//				checkOnSubmit : true,
//				closeAfterEdit: true,
//				errorTextFormat: function (data) {
//					return 'Error: ' + data.responseText
//				}
//			},
//			// options for the Add Dialog
//			{
//				closeAfterAdd: true,
//				recreateForm: true,
//				errorTextFormat: function (data) {
//					return 'Error: ' + data.responseText
//				}
//			},
//			// options for the Delete Dailog
//			{
//				errorTextFormat: function (data) {
//					return 'Error: ' + data.responseText
//				}
//			}
//		);
//
//		var lastSelection;
//
//		function editRow(id) {
//			if (id && id !== lastSelection) {
//				var grid = $("#tabs-test");
//				grid.jqGrid('restoreRow',lastSelection);
//				grid.jqGrid('editRow',id, {keys: true} );
//				lastSelection = id;
//			}
//		}


}

</script>