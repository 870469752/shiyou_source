<?php
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
TableAsset::register ( $this );
?>

<section id="widget-grid" class="">
	<div class="well well-sm well-light">
		<div id="tabs" >
			<ul>
				<li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
				<li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                <li><a href="#calculate" id="calculate_show">计算点位</a></li>
                <li><a href="#others" id="others_show">其他点位</a></li>
                <li><a href="#reve" id="reve_show">点位回收站</a></li>
			</ul>

			<div id="bacnet">
                <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">是否屏蔽</th>
                        <th data-class="expand">单位</th>
                        <th data-class="expand">时间</th>
                        <th data-class="expand">当前值</th>
                    </tr>
                    </thead>
                </table>
			</div>

			<div id="modbus">
                <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">是否屏蔽</th>
                        <th data-class="expand">单位</th>
                        <th data-class="expand">时间</th>
                        <th data-class="expand">当前值</th>
                    </tr>
                    </thead>
                </table>
			</div >

            <div id="calculate">
                <table id="datatable_calculate" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_calculate' name="checkAll_calculate"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">是否屏蔽</th>
                        <th data-class="expand">单位</th>
                        <th data-class="expand">时间</th>
                        <th data-class="expand">当前值</th>
                    </tr>
                    </thead>
                </table>
            </div >

            <div id="reve">
                <table id="datatable_reve" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_reve' name="checkAll_reve"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">是否屏蔽</th>
                        <th data-class="expand">单位</th>
                        <th data-class="expand">时间</th>
                        <th data-class="expand">当前值</th>
                    </tr>
                    </thead>
                </table>
            </div >

			<div id="others">
				<table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
					<thead>
					<tr>
						<th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
						<th data-class="expand">ID</th>
						<th data-class="expand">中文名</th>
						<th data-class="expand">英文名</th>
						<th data-class="expand">是否屏蔽</th>
                        <th data-class="expand">单位</th>
						<th data-class="expand">时间</th>
						<th data-class="expand">当前值</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">信息</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="resForm" style="margin-left: 10px">
							<input type="hidden" id="objectId"/>
							<div class="form-group">
								<label class="control-label" for="inputName">中文：</label><input type="text" style="width: 70%" id="cn" placeholder="中文">
							</div>
							<div class="form-group">
								<label class="control-label" for="inputName">英文：</label><input type="text" style="width: 70%" id="us" placeholder="英文">
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" id="btnSave">确定</button>
						<button class="btn btn-primary" id="btnEdit">保存</button>
						<button class="btn btn-danger" data-dismiss="modal"
								aria-hidden="true">取消
						</button>
					</div>
				</div>
			</div>
        </div>

        <!-- Modal -->
        <div class="modal fade"  id="myUnitModal" tabindex="-1" role="dialog" aria-labelledby="myUnitModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">信息</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="resForm" style="margin-left: 10px">
                            <input type="hidden" id="objectUnitId"/>
                            <div class="form-group">
                                <label class="control-label">单位：</label>
                                <select id="s_unit" name = "s_unit" style=" width: 90%">
                                    <option value="0">请选择单</option>
                                    <?php foreach($unit as $k => $v){ ?>
                                    <option value="<?=$k; ?>"><?=$v; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="btnUnitSave">确定</button>
                        <button class="btn btn-danger" data-dismiss="modal"
                                aria-hidden="true">取消
                        </button>
                    </div>
                </div>
            </div>
        </div>

	</div>
</section>


<script>

	window.onload = function() {
		var oTable_others; var oTable_bacnet; var oTable_modbus; var oTable_reve; var oTable_calculate;
        var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0; var temp_reve = 0; var temp_calculate = 0;

        $("#bacnet_show").click(function(){
            if(!temp_bacnet){
                oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
                temp_bacnet = 1;
            }
        })

        $("#modbus_show").click(function(){
            if(!temp_modbus){
                oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
                temp_modbus = 1;
            }
        })

        $("#calculate_show").click(function(){
            if(!temp_calculate){
                oTable_calculate = initTable('datatable_calculate','oTable_calculate','tool_calculate',4,'point');
                temp_calculate = 1;
            }
        })

        $("#reve_show").click(function(){
            if(!temp_reve){
                oTable_reve = initTable('datatable_reve','oTable_reve','tool_reve',0,'reve');
                temp_reve = 1;
            }
        })

        $("#others_show").click(function(){
            if(!temp_others){
                oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
                temp_others = 1;
            }
        })



        $(document).ready(function () {
			initModal();
            initUnitModal();
			$('#tabs').tabs({
				activate: function(event, ui) {
					ttInstances = TableTools.fnGetMasters();
					for (i in ttInstances) {
						if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
					}
				}
			});
            $("#<?=$active ?>").click();
		});

        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
		function initTable(name,oTable,tool,protocol_id,type) {
			/* TABLETOOLS */
			table = $('#'+name).dataTable({
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
				//"sdom": "Bfrtip",
				"oTableTools": {
					"aButtons": [
						"copy",
						//"csv",
						"xls",
						{
							"sExtends": "pdf",
							"name": "测试",
							"sTitle": "点位管理",
							"sPdfMessage": "pdf_test",
							"sPdfSize": "letter"
						},
						{
							"sExtends": "print",
							"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
						}
					],
					"sRowSelect": "os",
					"sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
				},
				"autoWidth": true,
				"lengthMenu": [[14,15, 16, 20, 25, 30, -1], [14,15, 16, 20, 25, 30, "All"]],
				"iDisplayLength": 14,
				//"bSort": false,
				"language": {
					"sProcessing": "处理中...",
					"sClear":"test",
					"sLengthMenu": "显示 _MENU_ 项结果",
					"sZeroRecords": "没有匹配结果",
					"sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
					"sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
					"sInfoFiltered": "(由 _MAX_ 项结果过滤)",
					"sInfoPostFix": "",
					"sSearch": "搜索:",
					"sUrl": "",
					"sEmptyTable": "表中数据为空",
					"sLoadingRecords": "载入中...",
					"sInfoThousands": ",",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "上页",
						"sNext": "下页",
						"sLast": "末页"
					},
					"oAria": {
						"sSortAscending": ": 以升序排列此列",
						"sSortDescending": ": 以降序排列此列"
					}
				},
				"processing": false,
				"serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
				"ajax": {
					"url": "/point/crud/get-data?type="+type+"&protocol_id="+protocol_id,
					"type": "post",
					"error": function () {
						alert("服务器未正常响应，请重试");
					}
				},
				"aoColumns": [
					{
						"mDataProp": "id",
						"bSortable": false,
						"fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
							$(nTd).html("<input type='checkbox' name='checkList_"+name+"' value='" + sData + "' >");
						}
					},
					{"mDataProp": "id"},
					{"mDataProp": "cn"},
					{"mDataProp": "us"},
					{"mDataProp": "is_shield"},
                    {"mDataProp": "unit" },
					{"mDataProp": "update_timestamp"},
					{"mDataProp": "value"},
				],
                "fnInitComplete": function (oSettings, json) {
                    if(name == 'datatable_reve'){
                        $('<a href="#" class="btn btn-success" id="reveFun">点位回收</a>' + '&nbsp;').appendTo($('.'+tool));
                        $("#reveFun").click(function(){
                            _reveList(name,oTable);
                        });
                    }else if(name == 'datatable_calculate'){
                        $('<a href="/calculate-point/crud/create" class="btn btn-success" id="calculateAddFun">添加</a>' + '&nbsp;'+
                        '<a href="#" class="btn btn-danger" id="deleteFun">删除</a>' + '&nbsp;').appendTo($('.'+tool));
                        $("#deleteFun").click(function(){
                            _deleteList(name,oTable);
                        });
                    }else{
                        $('<a href="#" class="btn btn-primary" id="editFun'+name+'">名称修改</a> ' + '&nbsp;' +
                        '<a href="#" class="btn btn-primary" id="editUnitFun'+name+'">单位修改</a> ' + '&nbsp;' +
                        '<a href="#" class="btn btn-danger" id="deleteFun'+name+'">删除</a>' + '&nbsp;').appendTo($('.'+tool));//myBtnBox
                        $("#deleteFun"+name).click(function(){
                            _deleteList(name,oTable);
                        });
                        $("#editFun"+name).click(function(){
                            _value(name,oTable);
                        });
                        $("#editUnitFun"+name).click(function(){
                            _valueUnit(name,oTable);
                        });
                        $("#btnEdit").click(function(){
                            _editFunAjax(oTable);
                        });
                        $("#addFun").click(_init);
                    }
                },
				"order": [1, 'asc']
			});
			/* END TABLETOOLS */
			return table;
		}

		/*******************************************    全选 取消 * 按钮****************************************************************/

		function check_all(name){
			$("input[name='checkList_"+name+"']").each(function(){
				$(this).prop('checked',true);
			});
		}

		function check_none(name){
            $("input[name='checkList_"+name+"']").each(function(){
				$(this).filter(':checkbox').prop('checked',false);
			})
		}

		$('#checkAll_bacnet').click(function(){
			var $status = $(this).prop('checked');
			$status ? check_all('datatable_bacnet') : check_none('datatable_bacnet');
		})

        $('#checkAll_others').click(function(){
            var $status = $(this).prop('checked');
            $status ? check_all('datatable_others') : check_none('datatable_others');
        })

        $('#checkAll_modbus').click(function(){
            var $status = $(this).prop('checked');
            $status ? check_all('datatable_modbus') : check_none('datatable_modbus');
        })

        $('#checkAll_reve').click(function(){
            var $status = $(this).prop('checked');
            $status ? check_all('datatable_reve') : check_none('datatable_reve');
        })

        $('#checkAll_calculate').click(function(){
            var $status = $(this).prop('checked');
            $status ? check_all('datatable_calculate') : check_none('datatable_calculate');
        })



		/**
		 * 编辑数据
		 * @private
		 */
		function _editFunAjax(oTable) {
			var id = $("#objectId").val();
			var cn = $("#cn").val();
			var us = $("#us").val();
			var jsonData = {
				"id": id,
				"cn": cn,
				"us": us
			};
			$.ajax({
				type: 'POST',
				url: '/point/crud/update-point',
				data: jsonData,
				success: function (json) {
					if (json) {
						$("#myModal").modal("hide");
						resetFrom();
                        resetoTable(oTable);
					} else {
						alert("更新失败");
					}
				}
			});
		}

        $("#btnUnitSave").click(function(){
            var str = $("#objectUnitId").val();
            var ids = str.substr(0, str.length - 1);
            var unit = $("#s_unit").val();

            var jsonData = {
                "id": ids,
                "unit": unit
            };
            $.ajax({
                type: 'POST',
                url: '/point/crud/update-point-unit',
                data: jsonData,
                success: function (json) {
                    if (json) {
                        $("#myUnitModal").modal("hide");
                        alert("更新成功");
                    } else {
                        alert("更新失败");
                    }
                }
            });
        })

        function resetoTable(oTable){
            if(oTable == 'oTable_others'){
                oTable_others.fnReloadAjax(oTable_others.fnSettings());
            }
            if(oTable == 'oTable_bacnet'){
                oTable_bacnet.fnReloadAjax(oTable_bacnet.fnSettings());
            }
            if(oTable == 'oTable_modbus'){
                oTable_modbus.fnReloadAjax(oTable_modbus.fnSettings());
            }
            if(oTable == 'oTable_calculate'){
                oTable_calculate.fnReloadAjax(oTable_calculate.fnSettings());
            }
            if(oTable == 'oTable_reve'){
                oTable_reve.fnReloadAjax(oTable_reve.fnSettings());
            }
        }

		/**
		 * 删除
		 * @param id
		 * @private
		 */
		function _deleteFun(ids,oTable) {
			$.ajax({
				url: "/point/crud/delete-point",
				data: {"id": ids},
				type: "post",
				success: function (backdata) {
					if (backdata) {
                        resetoTable(oTable)
                        alert("删除成功");
					} else {
						alert("删除失败");
					}
				}, error: function (error) {
					console.log(error);
				}
			});
		}

        /**
         * 点位回收
         * @param id
         * @private
         */
        function _reveFun(ids,oTable) {
            $.ajax({
                url: "/point/crud/reve-point",
                data: {"id": ids},
                type: "post",
                success: function (backdata) {
                    if (backdata) {
                        resetoTable(oTable)
                        alert("回收成功");
                    } else {
                        alert("回收失败");
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        }

		/**
		 * 赋值
		 * @private
		 */
		function _value(name,oTable) {
            var note; var s; var str;
            var length = $("input[name='checkList_"+name+"']:checked").length;
            if (!length) {
                alert("至少选择一条记录操作");return false;
            }

            $("input[name='checkList_"+name+"']:checked").each(function (i, o) {
                note = $(this).parent().parent().get(0);
            });
            if(oTable == 'oTable_others'){
                s = oTable_others.fnGetData(note)
            }
            if(oTable == 'oTable_bacnet'){
                s = oTable_bacnet.fnGetData(note)
            }
            if(oTable == 'oTable_modbus'){
                s = oTable_modbus.fnGetData(note)
            }

			if (note) {
				$("#btnEdit").show();
				$("#cn").val(s.cn);
				$("#us").val(s.us);
				$("#objectId").val(s.id);

				$("#myModal").modal("show");
				$("#btnSave").hide();
			} else {
				alert('请点击选择一条记录后操作。');
			}
		}

        /**
         * 赋值
         * @private
         */
        function _valueUnit(name,oTable) {
            var length = $("input[name='checkList_"+name+"']:checked").length;
            if (!length) {
                alert("至少选择一条记录操作");return false;
            }else{
                var str = '';
                $("input[name='checkList_"+name+"']:checked").each(function (i, o) {
                    str += $(this).val();
                    str += ",";
                });
                $("#objectUnitId").val(str);
                $("#myUnitModal").modal("show");
            }
        }

		/**
		 * 编辑数据带出值
		 * @param id
		 * @param us
		 * @param cn
		 * @private
		 */
		function _editFun(id, cn, us) {
			$("#us").val(us);
			$("#cn").val(cn);
			$("#objectId").val(id);
			$("#myModal").modal("show");
			$("#btnSave").hide();
			$("#btnEdit").show();
		}
		/**
		 * 初始化
		 * @private
		 */
		function _init() {
			resetFrom();
			$("#btnEdit").hide();
			$("#btnSave").show();
		}

		/**
		 * 添加数据
		 * @private
		 */
		function _addFun() {
			alert("本功能还未开发！");return false;
			var jsonData = {
				'name': $("#inputName").val(),
				'job': $("#inputJob").val(),
				'note': $("#inputNote").val()
			};
			$.ajax({
				url: "http://dt.thxopen.com/example/resources/user_share/basic_curd/insertFun.php",
				data: jsonData,
				type: "post",
				success: function (backdata) {
					if (backdata == 1) {
						$("#myModal").modal("hide");
						resetFrom();
						oTable.fnReloadAjax(oTable.fnSettings());
					} else if (backdata == 0) {
						alert("插入失败");
					} else {
						alert("防止数据不断增长，会影响速度，请先删掉一些数据再做测试");
					}
				}, error: function (error) {
					console.log(error);
				}
			});
		}

        /**
         * 初始化弹出层
         */
        function initUnitModal() {
            $('#myUnitModal').on('show', function () {
                $('body', document).addClass('modal-open');
                $('<div class="modal-backdrop fade in"></div>').appendTo($('body', document));
            });
            $('#myUnitModal').on('hide', function () {
                $('body', document).removeClass('modal-open');
                $('div.modal-backdrop').remove();
            });
        }


		/**
		 * 初始化弹出层
		 */
		function initModal() {
			$('#myModal').on('show', function () {
				$('body', document).addClass('modal-open');
				$('<div class="modal-backdrop fade in"></div>').appendTo($('body', document));
			});
			$('#myModal').on('hide', function () {
				$('body', document).removeClass('modal-open');
				$('div.modal-backdrop').remove();
			});
		}

		/**
		 * 重置表单
		 */
		function resetFrom() {
			$('form').each(function (index) {
				$('form')[index].reset();
			});
		}



        /**
         * 批量回收
         * @private
         */
        function _reveList(name,oTable) {
            var str = '';
            $("input[name='checkList_"+name+"']:checked").each(function (i, o) {
                str += $(this).val();
                str += ",";
            });
            if (str.length > 0) {
                var IDS = str.substr(0, str.length - 1);
                if(confirm("你要回收的数据集id为" + IDS)){
                    _reveFun(IDS,oTable);
                }
            } else {
                alert("至少选择一条记录操作");
            }
        }

		/**
		 * 批量删除
		 * @private
		 */
		function _deleteList(name,oTable) {
			var str = '';
			$("input[name='checkList_"+name+"']:checked").each(function (i, o) {
				str += $(this).val();
				str += ",";
			});
			if (str.length > 0) {
				var IDS = str.substr(0, str.length - 1);
                if(confirm("你要删除的数据集id为" + IDS)){
                    _deleteFun(IDS,oTable);
                }
			} else {
				alert("至少选择一条记录操作");
			}
		}

        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }
	}



</script>
