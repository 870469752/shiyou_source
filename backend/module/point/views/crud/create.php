<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit'=>$unit,
        'tree' => $tree,
        'tree_json' => $tree_json,
    ]) ?>
