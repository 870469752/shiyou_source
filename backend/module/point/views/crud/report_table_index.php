<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'PointData');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = ActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>对比参数设定</legend>
                <!--时间选择-->
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('id', Yii::$app->request->get('id'))?>
                    <div class="col-md-4">
                        <?=Html::textInput('start_time', isset($param['start_time'])?$param['start_time']:null,
                            ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Start Time'), 'id' => '_start_time', 'rel' => 'popover', 'data-placement' => 'top', 'data-original-title' => '开始时间不能大于结束时间'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::textInput('end_time', isset($param['end_time'])?$param['end_time']:null,  ['class' => 'form-control', 'placeholder' => Yii::t('app', 'End Time'), 'id' => '_end_time'])?>
                    </div>
                </div>
            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <div class="jarviswidget-ctrls">
                        <a href = <?=Url::toRoute('export').'?'.http_build_query(Yii::$app->request->getQueryParams());?> id="aSave" class="button-icon" rel="tooltip" data-original-title="保存报表" data-placement="bottom">
                            <i class="fa fa-save"></i>
                        </a>
                    </div>
                    <h2><?= (isset($name) ? $name : '') . '__点位报表查看' ?></h2>
                </header>
                <!-- widget div-->
                <div>

                    <?= GridView::widget([
                            'formatter' => ['class' => 'common\library\MyFormatter'],
                            'dataProvider' => $dataProvider,
                            'options' => ['class' => 'widget-body no-padding'],
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover',
                                'width' => '100%',
                                'id' => 'datatable'
                            ],
                            'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                            'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

                                ['attribute' => '_timestamp', 'format' => 'TimeFormatter', 'headerOptions' => ['data-hide' => 'phone']],

                                ['attribute' => '_value','label' => 'Value('.$unit.')', 'headerOptions' => ['data-hide' => 'phone']],
                            ],
                        ]); ?>

                    <!-- 搜索表单和JS -->
                </div>
            </div>
        </article>
    </div>
</section>

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function() {
        var $start_time = <?= isset($params['start_time']) ? "'" .$params['start_time'] ."'" : "''"?>;
        var $end_time = <?= isset($params['end_time']) ? "'" .$params['end_time'] ."'" : "''"?>;

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
            }
            $("#_loop").slideToggle("slow");

        });


        $('#_submit').click(function() {
            var $start_time = $('#_start_time').val();
            var $end_time = $('#_end_time').val();
            if($start_time >= $end_time) {
                $('#_start_time').trigger('click');
                return false
            }
            return true;
        });

        timeTypeChange('day', $('#_start_time'));
        timeTypeChange('day', $('#_end_time'));
        $('#_start_time').val($start_time);
        $('#_end_time').val($end_time);
    }
    </script>