<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var backend\models\Point $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = MyActiveForm::begin(); ?>
						<fieldset>
						<?= $form->field($model, 'name')->textInput(['placeholder' => '点位名称']) ?>

                        <?= $form->field($model, 'base_value')->textInput(['placeholder' => '点位基础值']) ?>

                        <?= $form->field($model, 'filter_min_value')->textInput(['placeholder' => '最小过滤值']) ?>

                        <?= $form->field($model, 'filter_max_value')->textInput(['placeholder' => '最大过滤值']) ?>

                        <?= $form->field($model, 'value_type')->dropDownList([
                                ''=> '',
                                '0'=> Yii::t('app', '实时'),
                                '1'=> Yii::t('app', '累计'),
                                '2'=> Yii::t('app', '开关'),
                            ],['class' => 'select2', 'placeholder' => '请选择点位数值类型']) ?>

                        <?= $form->field($model, 'interval')->dropDownList([
                                ''=>'',
                                '60'=>'1 '.Yii::t('app', 'Minute'),
                                '300'=>'5 '.Yii::t('app', 'Minute'),
                                '600'=>'10 '.Yii::t('app', 'Minute'),
                                '1800'=>'30 '.Yii::t('app', 'Minute'),
                                '3600'=>'1 '.Yii::t('app', 'Hour'),
                                '18000'=>'5 '.Yii::t('app', 'Hour'),
                                '43200'=>'12 '.Yii::t('app', 'Hour'),
                                '86400'=>'24 '.Yii::t('app', 'Hour'),
                            ],['class' => 'select2', 'placeholder' => '请选择点位记录间隔']) ?>

                        <!--在系统中选择 点位单位-->
                            <?=$form->field($model, 'unit')->dropDownList(
                                $unit,
//                            ArrayHelper::map(Unit::getAllUnit(), 'id', 'unit'),
                                ['class'=>'select2', 'style' => 'width:200px']
                            )?>


                        <!-- 分类 隐藏框 -->
                        <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id'])?>

                        <!-- 点位分类 -->
                        <label>能源分类</label>
                        <!--使用Fancytree 来做分类的展示-->
                            <div class="input-group" id = 'category_tree'>
                                <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                    <ul id = 'select_category' class="select2-choices" style = 'min-height: 30px'>
                                    </ul>
                                </div>

                                <div class="input-group-btn">
                                    <button class="btn btn-default btn-primary" type="button"  id = '_clear'>
                                        <i class="fa fa-mail-reply-all"></i> Clear
                                    </button>
                                </div>
                            </div>
                            <div id = 'tree' style = 'display:none;position:absolute;width:100%'/></div><br />




                    <!-- 分类 隐藏框 -->
                    <?=Html::hiddenInput('Point[category_id1]', null, ['id' => 'category_id1'])?>
                    <label>地理位置分类</label>
                    <!--使用Fancytree 来做分类的展示-->
                        <div class="input-group" id = 'category_tree1' style="width: 100%;">
                            <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name1' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                <ul id = 'select_category1' class="select2-choices choices1" style = 'min-height: 30px'>
                                </ul>
                            </div>

                                                    <div class="input-group-btn">
                                                        <button class="btn btn-default btn-primary" type="button"  id = '_clear1'>
                                                            <i class="fa fa-mail-reply-all"></i> Clear
                                                        </button>
                                                    </div>
                        </div>
                        <div id = 'tree1' style = 'display:none;position:absolute;width:100%'></div></div>

                        <?= $form->field($model, 'is_shield')->checkbox() ?>

                        <?= $form->field($model, 'is_upload')->checkbox() ?>

						</fieldset>

						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php MyActiveForm::end(); ?>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- END COL -->
	</div>
</section>

<!-- 加载  fancyTree 所需要的css 和 js 文件-->
<?php

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);


?>

<script>
    window.onload = function(){
        //初始化能源分类 与 地理位置分类 框
        initfancytree(<?=$tree_json?>,'');
        initfancytree(<?=$location_tree_json?>,1);


        //初始化树形下拉列表框  data为树状数据源  id 为元素后缀名
        function initfancytree(data,id){
            //fancytree 树状多选框初始化
            var $current_keys = [];
            var $count = 0;

            function delete_all_category() {
                $('.choices'+id+' li').each(function(){
                    if($(this).data('type') != 'plus'){
                        $(this).remove();
                    }
                })
            }
            console.log("#tree"+id);

            $("#tree"+id).fancytree({
                extensions: ["glyph", "edit", "wide"],
                checkbox: true,
                selectMode: 1,
                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                glyph: {
                    map: {
                        doc: "glyphicon glyphicon-file",
                        docOpen: "glyphicon glyphicon-file",
                        checkbox: "glyphicon glyphicon-unchecked",
                        checkboxSelected: "glyphicon glyphicon-check",
                        checkboxUnknown: "glyphicon glyphicon-share",
                        error: "glyphicon glyphicon-warning-sign",
                        expanderClosed: "glyphicon glyphicon-plus-sign",
                        expanderLazy: "glyphicon glyphicon-plus-sign",
                        // expanderLazy: "glyphicon glyphicon-expand",
                        expanderOpen: "glyphicon glyphicon-minus-sign",
                        // expanderOpen: "glyphicon glyphicon-collapse-down",
                        folder: "glyphicon glyphicon-folder-close",
                        folderOpen: "glyphicon glyphicon-folder-open",
                        loading: "glyphicon glyphicon-refresh"
                        // loading: "icon-spinner icon-spin"
                    }
                },
                wide: {
                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                },
                source:data,
                select: function(event, data) {
                    // Get a list of all selected nodes, and convert to a key array:
                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                        return node.key;
                    });
                    console.log('当前所有分类' + '_________________________' + $sel_keys);
                    if ($sel_keys.length >= $current_keys.length) {
                        for (var i = 0; i < $sel_keys.length; i++) {

                            if($.inArray($sel_keys[i], $current_keys) === -1) {
                                console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                console.log('上次所选的分类 --- ' + $current_keys);
                                console.log('当前选择分类 --- ' + $sel_keys[i]);
                                var $current_key = $sel_keys[i];
                                var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                for (var j = 0; j < $_other_sibling.length; j++) {
                                    console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                    $_other_sibling[j].setSelected(false);
                                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                        return node.key;
                                    });
                                }
                            }

                        }
                    }

                    //获取所有选中的分类将其加入到选择框中
                    var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                        console.log(node.key + ' ---- ' + node.title);
                        var $_html = '';
                        $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                            "<div>" + node.title + "</div>" +
                            "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                        return $_html;
                    });
                    //将分类选择框中的分类全部清除
                    delete_all_category();
                    $('#select_category'+id).append($_html);
                    //将选择的分类id放入分类隐藏框中
                    $('#category_id'+id).val($sel_keys);
                    //将最新所选的分类集合跟新到$current_keys 里
                    $current_keys = $sel_keys;
                    console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                }
            });

            $('#_clear'+id).click(function () {
                delete_all_category();
                $("#tree").fancytree();
            });

            $('#category_tree'+id).click(function() {
                $('#tree'+id).toggle();
            });
            $('#tree'+id).mouseleave(function() {
                $('#tree'+id).hide();
            });
        }
        //提交验证
        $('#_sub').click(function() {

        })
    }
</script>