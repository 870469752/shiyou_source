<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
use yii\bootstrap\ActiveForm;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .border_style_pop {
        box-shadow: 10px 10px 5px #888888;
    }
    .border_style_error {
        box-shadow: 0px 0px 31px #F07878;
    }
</style>
<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' class = 'border_style_pop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
                    <?php $form = ActiveForm::begin(['method' => 'get', 'id' => '_view']) ?>
                <fieldset>

                    <legend>参数设定</legend>

                    <div class = 'form-group col-md-12'>
                        <div class = 'col-md-12'>
                            <?=Html::dropDownList('point_id', null, $point_info,
                                ['multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Choose Point'), 'title' => Yii::t('app', 'Choose Point'), 'class' => 'select2', 'id' => 'point_id'])?>
                        </div>
                    </div>

                    <!-- 开始时间 -->
                    <div class = "form-group col-md-12 alert-info">
                        <div class = "col-md-3">
                            <?=Html::dropDownList('start_time_type', isset($time_type)?$time_type:null, ['' => '',
                                 'absolute' => Yii::t('app', 'Absolute Time'),
                                 'relative' => Yii::t('app', 'Relative Time'),
                                ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Start Time Type'), 'title' => Yii::t('app', 'Start Time Type'),'id' => 'start_time_type'])?>
                        </div>

                        <!-- 开始时间 -->
                        <div id = '_start'></div>

                    </div>
                    <!-- 开始时间 -->

                    <!-- 结束时间 -->
                    <div class = "form-group col-md-12 alert-success">
                        <div class = "col-md-3">
                            <?=Html::dropDownList('end_time_type', isset($time_type)?$time_type:null, ['' => '',
                                    'absolute' => Yii::t('app', 'Absolute Time'),
                                    'relative' => Yii::t('app', 'Relative Time'),
                                ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'End Time Type'), 'title' => Yii::t('app', 'End Time Type'),'id' => 'end_time_type'])?>
                        </div>

                        <div id = '_end'></div>

                    </div>
                    <!-- 结束时间 -->

                </fieldset>
                <!--提交          考虑到该页面会时常刷新采用Ajax提交-->
                <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
                    <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
                    <?=Html::a('提交', null, ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                </div>
                    <?php ActiveForm::end(); ?>
            </div>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark _jarviswidget border_style_pop"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false"
        data-widget-fullscreenbutton="false">
        <header id = '_header'>
            <div class="jarviswidget-ctrls">
                <a id="aSave" class="button-icon" href="#" data-toggle="modal" data-target="#SaveMenu" rel="tooltip" data-original-title=<?=Yii::t('app', 'Save Custom Reports')?> data-placement="bottom">
                    <i class="fa fa-save"></i>
                </a>
                <a style = 'display:none;' href = 'javascript:void(0)' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>
                <a href="javascript:void(0);" id = '_full' class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                    <i class="fa fa-expand "></i>
                </a>
            </div>
            <span class="widget-icon">
                 <i class="fa fa-table"></i>
            </span>
            <h2><?=isset($category_name) ? $category_name : ' '?></h2>
        </header>
        <!-- widget div-->

        <div class="no-padding">

            <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                <div class="col-md-10" >
<!--                    <label class="radio radio-inline"  style = 'margin-top:0px'>-->
<!--                        <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>-->
<!--                        <span>曲线图</span>-->
<!--                    </label>-->
<!--                    <label class="radio radio-inline" style = 'margin-top:0px'>-->
<!--                        <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>-->
<!--                        <span>柱状图</span>-->
<!--                    </label>-->
<!--                    <label class="radio radio-inline" style = 'margin-top:0px'>-->
<!--                        <input type="radio" class="radiobox" name="style-0a" data-value = 'bar'>-->
<!--                        <span>柱状图_2</span>-->
<!--                    </label>-->
                </div>

        </div>

            <div id="container_chart" style="height:500px;"></div>

            <div id="container_table" style="min-height:500px;display:none">

            </div>

        </div>
    </div>
    <!-------     图表内容 end-->
</section>

<div class="modal fade" id="SaveMenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id = '_sa_mu'>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel"><?=Yii::t('app', 'Save Custom Reports')?></h4>
            </div>
            <?php $form = ActiveForm::begin(['action' => '#', 'id' => 'save_form', 'method' => 'get']) ?>
            <div class="modal-body">
                <div class="well" id="condition">
                    <?=Html::textInput('custom_report_name',null, ['id' => 'custom_report_name', 'class' => 'form-control', 'placeholder' => Yii::t('app', 'Custom report name'), 'title' => Yii::t('app', 'Custom report name')])?>
                </div>
            </div>
            <div class="modal-footer">
                <?= Html::button('取消', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'id' => '_dis']) ?>
                <?= Html::submitButton('保存', ['class' => 'btn btn-success', 'id' => 'custom_btn']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/animate.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/twbs-pagination/jquery.twbsPagination.min.js", ['yii\web\JqueryAsset']);
?>
<!-- js 脚本区域 start-->
<script>

window.onload = function(){
    var $form_info = '';                                            //参数提交按钮保存的表单信息
    var $start_time_type = '';
    var $end_time_type = '';
    var $point_ids = '';
    var $url = '<?=Url::toRoute('ajax-chart-view')?>';
    var $change_url = '<?=Url::toRoute('ajax-change-chart')?>';
    var $export_url = '<?=Url::toRoute('export-point-view')?>';
    var $save_custom_report_url = '<?=Url::toRoute('save-to-custom-report')?>';
    var $data = '';
    var $chart;
    var $_page_num = 10;

    $('#_cl').click(function(){
        $('#_loop').removeClass('animated shake border_style_error');
        if($("#_loop").css('display') == 'none'){
            $('#_line').hide();
        }else{
            $('#_line').show();
        }
        $("#_loop").slideToggle("slow");
    });

    /*若用户选择相对时间中的自定义 动态添加有关自定义的 dom元素*/
    function appendCustom($name) {
        $('#' + $name).parent().after(
                     '<div class="col-md-3">' +
                        '<div class = "input-group">' +
                            '<span class="input-group-addon"><?=Yii::t('app', 'Last')?></span>' +
                            '<input type="text" id= "custom_' + $name + '_num" class="form-control" name = "custom_' + $name + '_num" title="<?=Yii::t('app', 'Time number')?>" placeholder="<?=Yii::t('app', 'Time number')?>">' +
                        '</div>' +
                     '</div>' +
                     '<div class="col-md-3">' +
                        '<select id = "custom_' + $name + '_type" class="select2" name = "custom_' + $name + '_type" title="<?=Yii::t('app', 'Choose custom time type')?>" placeholder="<?=Yii::t('app', 'Choose custom time type')?>">' +
                            '<option value=""></option>' +
                            '<option value="day">日</option>' +
                            '<option value="month">月</option>' +
                            '<option value="year">年</option>' +
                        '</select>' +
                     '</div>'
        );
        $('#custom_' + $name + '_type').select2();
    }

    var $relative_value = '';
    function addChangeEvent($name) {
        $('#' + $name).change(function (){
            $relative_value = $(this).val();
            if($relative_value == 'custom') {
                appendCustom($name);
            } else {
                $('#' + $name).parent().find('~ div').remove(); // or $('#' + $name).parent().nextAll().remove();
            }
        })
    }

    /* 根据用户选择的时间类型进行联动 动态添加所需要的参数表单dom */
    function appendTime($time_type, $name, $obj) {
        $obj.empty();
        if($time_type == 'absolute') {
            //添加开始时间的 时间选择器
            $obj.append(
                        '<div class = "col-md-3">' +
                            '<input type="text" id=' + $name + ' class="form-control" name = ' + $name + '  title = <?=Yii::t('app', 'Choose Time')?> placeholder = <?=Yii::t('app', 'Choose Time')?>>' +
                        '</div>'
            );
            timeTypeChange('day', $('#' + $name));
        }else {
            $obj.append(
                '<div class = "col-md-3">' +
                    '<select id = ' + $name + ' class="select2" name=' + $name + ' title = <?=Yii::t('app', 'Choose Time')?> placeholder = <?=Yii::t('app', 'Choose Time')?>>' +
                        '<option></option>' +
                        '<option value="0,day"><?=Yii::t('app', 'Today')?></option>' +
                        '<option value="0,week"><?=Yii::t('app', 'This week')?></option>' +
                        '<option value="0,month"><?=Yii::t('app', 'This month')?></option>' +
                        '<option value="0,year"><?=Yii::t('app', 'This year')?></option>' +
                        '<option value="1,day"><?=Yii::t('app', 'Last day')?></option>' +
                        '<option value="2,day"><?=Yii::t('app', 'Last two days')?></option>' +
                        '<option value="1,week"><?=Yii::t('app', 'Last week')?></option>' +
                        '<option value="2,week"><?=Yii::t('app', 'Last two weeks')?></option>' +
                        '<option value="1,month"><?=Yii::t('app', 'Last month')?></option>' +
                        '<option value="custom"><?=Yii::t('app', 'Custom')?></option>' +
                    '</select>' +
                '</div>'
            );
            $('#' + $name).select2();
            addChangeEvent($name);
        }
    }

    $('#start_time_type').change(function() {
        $start_time_type = $(this).val();
        appendTime($start_time_type, 'start_time', $('#_start'));
    });

    $('#end_time_type').change(function() {
        $end_time_type = $(this).val();
        appendTime($end_time_type, 'end_time', $('#_end'));
    });

    /** ajax提交 **/
    $('#_submit').on('click', function(event) {
        $form_info = $('#_view').serializeArray();
        console.log('form_info:', $form_info);
        //在此进行表单验证
        if(validateFrom($form_info) != 0){return false;}
        $point_ids = $('#point_id').val();
        $.getJSON($url, $form_info, function(data) {
            $data = data;
            if(!data) {
                alert('时间不正确，请重新选择！');
            }else {
                ajaxGetHighChart(data.chart);
                ajaxGetTable(data.table);
                triggerAfterAjax($('#container_table'), data.table, Math.ceil(data.table.total/$_page_num));
                $('#_save').attr('href', $export_url + '?' + jsonToUrlString(data.param));
            }
        })
        event.stopPropagation();
        return false;
    });

    function jsonToUrlString($json_data) {
        var $url_params = '';
        $.each($json_data, function($key, $value) {
            $url_params += '&' + $key + '=' + $value;
        });
        return $url_params.substr(1);
    }
    /***************************************************************************************end ****************/

    timeTypeChange('day', $('#end_absolute_time'));
    // 图、表切换

    highcharts_lang_date();
//    $.ajaxSetup({async: false});         //设置全局ajax为 同步请求
    var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
    //chart 中保持不变的量
    var $point_info   = <?=isset($point_info) ? json_encode($point_info) : '{}'?>;


    function afterSetExtremes(e){
        $chart.showLoading('Loading data from server...');
        var $params = '?point_id=' + $point_ids + '&start_time=' + Math.ceil(e.min/1000) + '&end_time=' + Math.ceil(e.max/1000);
        $.getJSON($change_url + $params,
                    function (result) {
                        $data = result;
                        // 他必须得一组一组数据插入
                        for (var i = 0; i < result.chart.length; i++) {
                            $chart.series[i].setData(result.chart[i].data);
                        }
                        ajaxGetTable($data.table);
                        triggerAfterAjax($('#container_table'), $data.table, Math.ceil($data.table.total/$_page_num));
                        $('#_save').attr('href', $export_url + $params);
                        $chart.hideLoading();
                    });
    }

    //highchart 图形绘制
    function ajaxGetHighChart(chart_data){
        $chart = new Highcharts.StockChart({
                chart: {
                    renderTo: 'container_chart',
                    borderWidth: 0,
                    borderRadius: 0,
                    type: 'spline',
                    zoomType: 'x'
                },
                title: {
                    text: '点位查看',
                    align: 'center',
                },
                credits : {
                    enabled : false
                },
                scrollbar: {
                    liveRedraw: false
                },
                legend: {
                    enabled: true,
                    align: 'center',
                    verticalAlign: 'bottom',
                    borderWidth: 2,
                    itemStyle: {
                        fontSize: '14px'
                    }
                },
                rangeSelector : {
                   enabled:false
                },
                navigator : {
                    adaptToUpdatedData: false,
                    series : chart_data
                },
                xAxis : {
                    events : {
                        afterSetExtremes : afterSetExtremes
                    },
                    minRange: 3600 * 1000
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    },
                    opposite: false
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    valueDecimals: 2,
                    valueDecimals: 2,
                    shared : true,
                    style : {
                        //color : 'white',
                        padding : '7px',
                        fontsize : '9pt'
                    }
                },
                series: chart_data
            });
    }


    //当ajax获取分项插件后 给每一个分页按钮添加点击事件
    function triggerAfterAjax($table_obj, $data, $total_page){
        $table_obj.find('.pagination').twbsPagination({
            totalPages: $total_page,
            visiblePages: 7,
            onPageClick: function (event, page) {
                addTable($data, $table_obj, page);
            }
        });
    }

//    $("#_header").on('click', function() {
//        ajaxGetHighChart($data.chart);
//    })
    //table 表格生成
    function ajaxGetTable(table_data){
        $('#container_table').empty();
        var $thead = '<tr>';
        $.each(table_data.thead, function(thead_k, thead_v) {
            $thead += '<td>' + thead_v + '</td>';
        })
        $thead += '</tr>';
        $('#container_table').append(
            '<table class="table table-striped table-bordered table-hover table-middle" >' +
                '<thead>' + $thead + '</thead>' +
                '<tbody></tbody>' +
            '</table>'+
            "<div id ='pagination-pie' class = 'pagination'></div>"
        );
        addTable(table_data, $('#container_table'), 1);
    }
    //向table中添加当前页数的数据
    function addTable($data, $table_obj , $page){

        $table_obj.find('tbody').empty();
        //table
        var $_tbody = '';
        for(var $i = 0; $i < $data.tbody.length; $i++) {
                if( $_page_num * ($page - 1) <= $i && $i < $_page_num * ($page)){
                    //将数据集 分开 饼图 和 折现、柱状图数据格式不同
                    $_tbody += '<tr>';
                    $.each($data.thead, function(thead_k, thead_v) {
                        $_tbody += '<td>' + $data.tbody[$i][thead_k] + '</td>';
                    })
                    $_tbody += '</tr>';
                }
        }
//        console.log($data, $data.tbody.length, $_tbody);
        $table_obj.find('tbody').append($_tbody);
    }

    // 图、表切换
    $('#_chart_switch').on('click', function(){
        var $_chart_type = $(this).data('type');
        if($_chart_type == 'chart'){
            $('#container_chart').hide();
            $('#chart_type').hide();
            $('#container_table').show();
            $('#_save').show();
            $(this).data('type', 'table');

        }else{
            $('#container_chart').show();
            $('#chart_type').show();
            $('#container_table').hide();
            $('#_save').hide();
            $(this).data('type', 'chart');

        }
    });

    //保存为 自定义报表 ---本页面含有大量点位数据 为了不重新请求 依然ajax
    $('#custom_btn').on('click', function() {
        addErrorClass($('#_loop'), false);
        addErrorClass($('#_sa_mu'), false);
        //简单对参数表单进行验证
        if(! $form_info) {
            $('#_dis').trigger('click');
            $('#_loop').show();
            $('#_line').hide();
            addErrorClass($('#_loop'));
            return false;
        } else {
            var $custom_report_name = $('#custom_report_name').val().trim();
            if(! $custom_report_name) {
                addErrorClass($('#_sa_mu'));
                return false;
            }
            var $save_form = $('#save_form').serializeArray();
            $form_info = $.unique($form_info.concat($save_form));
        }
        //验证通过 ajax 将数据保存到自定义报表中
        $.get($save_custom_report_url, $form_info, function(data) {
            if(data) {
                alert('保存成功!');
                $('#_dis').trigger('click');
            }else{
                alert('保存失败，请重新保存!');
            }
        }).error(function() {
            alert('保存失败，请重新保存!');
        });
        return false;
    });


    //验证效果 添加 和 移除
    function addErrorClass($obj, $add) {
        $obj.removeClass('border_style_error animated shake');
        $add = (typeof(arguments[1])=="undefined")?true:arguments[1];
        if($add){
            $obj.addClass('border_style_error animated shake');
        }
    }

    //表单验证
    function validateFrom($form_info) {
        var $validate_sign = 0;
        //由于目前 serializeArray 不能获取空的（有时间可以追下代码） multiple select 元素 所以单独进行处理
        if(! $('#point_id').val()) {
            $validate_sign += 1;
            addErrorClass($('#point_id'));
        } else {
            addErrorClass($('#point_id'), false);
        }

        $.each($form_info, function (name, value) {
            if(value.value == '') {
                addErrorClass($('#' + value.name.replace('[]', '')));
                $validate_sign += 1;
            } else {
                addErrorClass($('#' + value.name.replace('[]', '')), false);
            }
        })
        return $validate_sign;
    }

    function vaildationSelect() {

    }
}
</script>
<!-- js 脚本区域 end-->