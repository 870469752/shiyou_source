<?php

use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="padding-10" style="background: #fafafa">
    <div>
        <?php $form = MyActiveForm::begin(['method' => 'get', 'options' => ['class' => 'form-inline']]); ?>
        <?= MyHtml::multiTree(
            $searchModel->formName().'[category_id]',
            $category_tree,
            @$searchModel->id,
            ['multiple' => true]
        ) ?>
        <?= $form->field($searchModel, 'name')->textInput(['style' => 'width:250px; height:34px']) ?>

        <div>
        <?= MyHtml::submitButton('筛选点位', ['class' => 'btn btn-success']) ?>
        <?= MyHtml::button('清除已选项', ['class' => 'btn btn-danger', 'id' => 'clear_selected']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>

</div>

<?= GridView::widget([
    'formatter' => ['class' => 'common\library\MyFormatter'],
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover table-middle'
    ],
    'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
    'columns' => [

        [
            'class' => 'common\library\MyCheckboxColumn',
            'name' => 'point_id',
            'checkboxOptions' => ['class' => 'table_checkbox'],
            'header_content' => '请选择'
        ],
        [
            'class' => 'common\library\SelectColumn', 'item' => [
            '' => '默认',
            'line' => '折线图',
            'column' => '柱状图',
            'area' => '面积图',
            'step' => '脉冲图'
        ],
            'label' => '图形样式',
            'selectOptions' => ['class' => 'table_select select2', 'disabled' => true]
        ],
        ['attribute' => 'name', 'format' => 'JSON'],
        ['attribute' => 'oneData', 'format' => ['arrayKey', 'value']],
        ['attribute' => 'unit', 'format' => ['arrayKey', 'name']],
    ],
]); ?>
<script>
    window.onload = function () {
        // 遍历本地存储 主要为了保留选择框和复选框
        for (var i = localStorage.length-1; i >= 0; i --) {
            var input_name = localStorage.key(i);
            var input_value = localStorage.getItem(input_name);
            var input_obj = $('[name="' + input_name + '"]');
            if (input_name.indexOf('point_id') != -1) {
                input_obj.attr('checked', true);
            } else if (input_name.indexOf('chart') != -1) {
                input_obj.attr('disabled', false);
                input_obj.val(input_value).trigger("change");
            }
        }
        // 本地存储操作
        $('.table_checkbox').change(function (e) {
            var chart_item = 'chart[' + this.value + ']';
            var chart_obj = $('[name="' + chart_item + '"]');
            if (this.checked == true) {
                chart_obj.attr('disabled', false); // 图形选择设为可用
                localStorage.setItem(this.name, this.value) // 设置点位ID本地存储
                localStorage.setItem(chart_item, ''); // 设置图形参数本地存储
            }
            else { // 删除点位和图形样式
                chart_obj.val('').trigger("change"); // 图形选择设默认
                chart_obj.attr('disabled', true); // 图形选择设为不可用
                localStorage.removeItem(this.name); // 删除点位ID本地存储
                localStorage.removeItem(chart_item); // 删除图形参数本地存储


            }
        })

        $('#clear_selected').click(function(){
            // 遍历删除localStorage里的point_id和chart
            for (var i = localStorage.length-1; i >= 0; i --) {
                var item = localStorage.key(i);
                if(item.indexOf('point_id') != -1 || item.indexOf('chart') != -1){
                    localStorage.removeItem(item);
                }
            }
            location.reload();
        })

        $('.table_select').change(function (e) {
            localStorage.setItem(this.name, this.value)
        })
    }
</script>