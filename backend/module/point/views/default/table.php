<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\grid\GridView;
use backend\assets\TableAsset;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('app', '点位数据表格');
$this->params['breadcrumbs'][] = $this->title;
?>
<?=
GridView::widget([
    'formatter' => ['class' => 'common\library\MyFormatter'],
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
    ],
    'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
    'columns' => $col,
]); ?>
<script>
    window.onload = function () {
        var options = {"button": [], "dom": "t"};
        table_config('datatable', options);
    }
</script>
