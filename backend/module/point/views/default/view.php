<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\grid\GridView;
use backend\assets\TableAsset;
use yii\helpers\Url;
use common\library\TimeHelper;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('app', '点位查看');
$this->params['breadcrumbs'][] = $this->title;
$PointDataForm = YII::$app->request->get('PointDataForm', []);
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                    <h2>点位图形数据</h2>
                    <div class="jarviswidget-ctrls">
                        <a id="aSave" class="button-icon" href="#" data-toggle="modal" data-target="#SaveMenu" rel="tooltip" data-original-title="保存报表" data-placement="bottom">
                            <i class="fa fa-save"></i>
                        </a>
                        <a id="aSearchPoint" class="button-icon" href="#" data-toggle="modal" data-target="#searchPoint" rel="tooltip" data-original-title="搜索点位" data-placement="bottom">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div class="no-padding">
                    <div id="PointChart"><h1 class="col-xs-12 text-align-center"><strong>请稍等...</strong></h1></div>
                </div>
            </div>
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2>点位表格数据</h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="<?=
                        '/point/default/export-data?point_id='
                        . @implode(',', $PointDataForm['point_id']).'&'.
                        http_build_query([
                            'file_type' => 'excel',
                            'date' => @$PointDataForm['date'],
                            'start_time' => @TimeHelper::StartEndTime($PointDataForm['date'],'start_time'),
                            'end_time' => @TimeHelper::StartEndTime($PointDataForm['date'],'end_time'),
                        ])
                        ?>" rel="tooltip" data-original-title="导出" data-placement="bottom">
                            <i class="fa fa-file-excel-o fa-fw"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div class="no-padding">
                    <iframe id="iframeTableData" frameborder="0" scrolling="no" width="100%" src="<?= Url::toRoute(['/point/default/table'] + $PointDataForm) ?>" onload="this.height = this.contentWindow.document.body.offsetHeight"></iframe>
                </div>
            </div>
        </article>
    </div>
</section>
<div class="modal fade" id="SaveMenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">保存报表</h4>
            </div>
            <?php $form = MyActiveForm::begin(['action' => Url::toRoute('/custom-feature/crud/create')]) ?>
            <div class="modal-body">
                <div class="well" id="condition">
                    <?= $form->field($model, 'description')->textInput(['class' => 'form-control']) ?>

                    <?= MyHtml::dropDownList($model->formName().'[parent_id]', null,
                        ['' => ['id' => '', 'name' => '不保存到菜单', 'parent_id' => '']] + $menu,
                        ['class' => 'select2', 'tree' => true]
                    ) ?>
                    <?= $form->field($model, 'url')->hiddenInput(['value' => parse_url(Url::to(''))['path']]) ?>
                    <?= $form->field($model, 'param')->hiddenInput(['value' => @parse_url(Url::to(''))['query']]) ?>
                    <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id]) ?>
                </div>
            </div>
            <div class="modal-footer">
                <?= MyHtml::button('取消', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
                <?= MyHtml::submitButton('提交', ['class' => 'btn btn-success']) ?>
            </div>
            <?php MyActiveForm::end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="searchPoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div style="width:1000px" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">选择点位与报表类型</h4>
            </div>
            <?php $form = MyActiveForm::begin(['method' => 'get', 'options' => ['id' => $searchModel->formName()]]); ?>
            <div class="modal-body">
                <div class="well">
                    <iframe id="iframePointSearch" frameborder="0" scrolling="no" width="100%" src="/point" onload="this.height = this.contentWindow.document.body.offsetHeight"></iframe>
                    <?= $form->field($searchModel, 'report_type')->dropDownList([
                        '' => '整体趋势',
                        'hour' => '日报表',
                        'day' => '月报表',
                        'month' => '年报表',
                    ], ['class' => 'select2']) ?>
                    <?= $form->field($searchModel, 'date')->textInput() ?>
                    <?= $form->field($searchModel, 'fetch_value_method')->dropDownList([
                        '' => '默认值',
                        'max' => '最大值',
                        'min' => '最小值',
                        'avg' => '平均值',
                        'sum' => '累加值'
                    ], ['class' => 'select2']) ?>
                </div>

            </div>
            <div class="modal-footer">
                <?= MyHtml::button('取消', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
                <?= MyHtml::a('提交', null, ['class' => 'btn btn-success', 'id' => 'aSubmit']) ?>
            </div>
            <?php MyActiveForm::end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css");
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    // 每次载入都清空本地存储，然后按链接重建本地存储
    for (var i = localStorage.length-1; i >= 0; i --) {
        var k = localStorage.key(i);
        if (k.indexOf('point_id') != -1 || k.indexOf('chart') != -1) {
            localStorage.removeItem(k);
        }
    }
    // 按链接参数重建本地存储
    var Point_data_form = JSON.parse('<?= isset($PointDataForm)?json_encode($PointDataForm, JSON_UNESCAPED_UNICODE):'[]' ?>');
    for(var i in Point_data_form.point_id){
        localStorage.setItem('point_id['+ Point_data_form.point_id[i] +']', Point_data_form.point_id[i]);
        localStorage.setItem('chart['+ Point_data_form.point_id[i] +']', Point_data_form.chart[i]);
    }

    window.onload = function () {

        // 图形
        highcharts_lang_date();

        // 传入所有参数
        var ajax_url = '<?= Url::toRoute(
            ['/point/default/ajax-point-data']+
            $PointDataForm +
            $axis_json
        ) ?>';

        function afterSetExtremes(e) {
            var chart = $('#PointChart').highcharts();
            chart.showLoading('加载中，请稍等...');
            $.getJSON(ajax_url +
                '&start_time=' + TimestampToDate(e.min) +
                '&end_time=' + TimestampToDate(e.max),
                function (result) {
                    // 他必须得一组一组数据插入
                    for (var i = 0; i < result.length; i++) {
                        chart.series[i].setData(result[i].data);
                    }
                    chart.hideLoading();
                });
        }

        $.getJSON(ajax_url, function (data) {
            $('#PointChart').highcharts('StockChart', {
                chart: {
                    zoomType: 'x'
                },
                navigator: {
                    adaptToUpdatedData: false,
                    series: data
                },
                legend: {
                    enabled: true,
                    align: '',
                    verticalAlign: 'top'
                },
                scrollbar: {
                    liveRedraw: false
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                rangeSelector: {
                    enabled: false
                },
                tooltip: {
                    valueDecimals: 2,
                    shared: true
                },
                plotOptions: {
                    column:{
                        dataGrouping: {
                            enabled:false
                        }
                    }
                    <?php if(Yii::$app->request->get('stack')): ?>, series: {
                        stacking: 'normal' // 堆叠
                    }
                    <?php endif ?>
                },
                yAxis: <?= $axis_json['axis'] ?>, //多轴

                xAxis: {
                    events: {
                        afterSetExtremes: afterSetExtremes
                    },
                    minRange: 3600 * 1000
                },
                series: data
            });
        });

        // 如果报表类型为整体趋势，那么时间和取值都隐藏
        var input_date = $('[name="<?= $searchModel->formName() ?>[date]"]');
        var input_group = $('[name="<?= $searchModel->formName() ?>[report_type]"]');
        var method = $('.field-<?= strtolower($searchModel->formName()) ?>-fetch_value_method');
        var date = $('.field-<?= strtolower($searchModel->formName()) ?>-date');
        if(input_group.val() == ''){
            method.hide();
            date.hide();
        }
        input_date.datetimepicker({
            format: 'yyyy-mm-dd',
            startView: 'month',
            minView: 'month',
            autoclose: true,
            startDate: '<?= $searchModel->time['min'] ?>',
            endDate: '<?= $searchModel->time['max'] ?>'
        });
        input_group.change(function(){
            // 当选择的报表不同时，时间插件的选择视图也不同
            switch(this.value){
                case '':
                    method.hide();
                    date.hide();
                    var date_format = ''; // 清空时间
                    break;
                case 'hour':
                    method.show();
                    date.show();
                    var date_format = 'yyyy-mm-dd';
                    var date_type = 'month'
                    break;
                case 'day':
                    method.show();
                    date.show();
                    var date_format = 'yyyy-mm';
                    var date_type = 'year'
                    break;
                case 'month':
                    method.show();
                    date.show();
                    var date_format = 'yyyy';
                    var date_type = 'decade'
                    break;
            }
            input_date.datetimepicker('remove');
            input_date.datetimepicker({
                format: date_format,
                startView: date_type,
                minView: date_type,
                startDate: '<?= $searchModel->time['min'] ?>',
                endDate: '<?= $searchModel->time['max'] ?>',
                autoclose: true
            });
            input_date.val(('<?= $searchModel->time['max'] ?>'.substr(0, date_format.length)));
        })

        // 点击提交时验证
        $('#aSubmit').click(function(){
            var form_obj = $('#'+'<?= $searchModel->formName() ?>');
            for (var i = localStorage.length-1; i >= 0; i --) {
                var name = '<?= $searchModel->formName() ?>[' + /\w+/.exec(localStorage.key(i)) + ']' + /\[\d+\]/.exec(localStorage.key(i));
                var value = localStorage.getItem(localStorage.key(i));
                if (name.indexOf('point_id') != -1 || name.indexOf('chart') != -1) {
                    form_obj.append('<input class="data" type="hidden" name="'+name+'" value="'+value+'">');
                }
            }
            if($('.data').length){
                // 不知道为什么要提交两下
                form_obj.submit().submit();
            }else{
                alert("请勾选点位");
                return false;
            }


        })

        $('#aSearchPoint').click(function(){
            setTimeout( function(){
                document.getElementById('iframePointSearch').height = document.getElementById('iframePointSearch').contentWindow.document.body.offsetHeight;
            }, 200)
        })
        <?php if(empty($PointDataForm)): // 如果没有查询参数就弹出?>
        $('#aSearchPoint').click();
        <?php endif ?>
    };
</script>
