<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', '选择查看条件');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div class="widget-body">
                    <?php $form = MyActiveForm::begin(['method' => 'get', 'action' => Url::toRoute('/point/default/view')]); ?>

                    <?= $form->field($model, 'group')->dropDownList([
                        '' => '整体趋势',
                        'hour' => '日报表',
                        'day' => '月报表',
                        'month' => '年报表',
                    ], ['class' => 'select2']) ?>
                    <?= $form->field($model, 'date')->textInput() ?>
                    <?= $form->field($model, 'method')->dropDownList([
                        '' => '默认值',
                        'max' => '最大值',
                        'min' => '最小值',
                        'avg' => '平均值',
                        'sum' => '累加值'
                    ], ['class' => 'select2']) ?>

                    <!--
                    <div class="form-group field-spindle required">
                        <label class="control-label" for="field-spindle">图形是否多轴</label>
                        <?= MyHtml::checkbox('spindle') ?>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group field-stack required">
                        <label class="control-label" for="field-stack">图形是否堆叠</label>
                        <?= MyHtml::checkbox('stack') ?>
                        <div class="help-block"></div>
                    </div>
                    -->

                    <!-- 生成隐藏框 -->
                    <?php foreach(Yii::$app->request->get('point_id', []) as $k => $v):?>
                        <?= MyHtml::hiddenInput($model->formName().'[point_id]['.$k.']', $v) ?>
                    <?php endforeach ?>
                    <?php foreach(Yii::$app->request->get('chart', []) as $k => $v):?>
                        <?= MyHtml::hiddenInput('chart['.$k.']',$v) ?>
                    <?php endforeach ?>
                    <?php if($id = Yii::$app->request->get('id')):?>
                    <?= MyHtml::hiddenInput($model->formName().'[point_id]['.$id.']', $id);?>
                    <?php endif ?>
                    <!-- 生成隐藏框结束 -->

                    <?= MyHtml::submitButton('完成', ['class' => 'btn btn-success']) ?>

                    <?php MyActiveForm::end(); ?>
                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css");
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function () {
        // 如果报表类型为整体趋势，那么时间和取值都隐藏
        var input_date = $('[name="<?= $model->formName() ?>[date]"]');
        var input_group = $('[name="<?= $model->formName() ?>[group]"]');
        var method = $('.field-pointdata-method');
        var date = $('.field-pointdata-date');
        if(input_group.val() == ''){
            method.hide();
            date.hide();
        }
        input_date.datetimepicker({
            format: 'yyyy-mm-dd',
            startView: 'month',
            minView: 'month',
            autoclose: true,
            startDate: '<?= $model->getTime('min') ?>',
            endDate: '<?= $model->getTime('max') ?>'
        });
        input_group.change(function(){
            // 当选择的报表不同时，时间插件的选择视图也不同
            switch(this.value){
                case '':
                    method.hide();
                    date.hide();
                    var date_format = ''; // 清空时间
                    break;
                case 'hour':
                    method.show();
                    date.show();
                    var date_format = 'yyyy-mm-dd';
                    var date_type = 'month'
                    break;
                case 'day':
                    method.show();
                    date.show();
                    var date_format = 'yyyy-mm';
                    var date_type = 'year'
                    break;
                case 'month':
                    method.show();
                    date.show();
                    var date_format = 'yyyy';
                    var date_type = 'decade'
                    break;
            }
            input_date.datetimepicker('remove');
            input_date.datetimepicker({
                format: date_format,
                startView: date_type,
                minView: date_type,
                startDate: '<?= $model->getTime('min') ?>',
                endDate: '<?= $model->getTime('max') ?>',
                autoclose: true
            });
            input_date.val(('<?= $model->getTime() ?>'.substr(0, date_format.length)));
        })

    }
</script>