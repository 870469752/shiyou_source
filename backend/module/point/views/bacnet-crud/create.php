<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\BacnetPoint $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Bacnet Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit' => $unit,
    ]) ?>
