<?php

use common\library\MyHtml;
use yii\grid\GridView;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\BacnetPointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'Bacnet Points');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div class="no-padding">
                    <div class="padding-10" style="background: #fafafa">
                        <div>
                            <?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                            <?= MyHtml::hiddenInput('view', 'notCategory') ?>
                            <?= $form->field($searchModel, 'name')->textInput() ?>

                            <?= MyHtml::submitButton('筛选点位', ['class' => 'btn btn-success']) ?>
                            <?= MyHtml::a('一键翻译', Url::toRoute(['/point/bacnet-crud/translate'] + Yii::$app->request->get()), ['class' => 'btn btn-primary']) ?>
                            <?= MyHtml::a('下载模板', Url::toRoute('/translate.xls'), ['class' => 'btn btn-primary']) ?>
                            <?= MyHtml::Button('上传翻译', ['type' => 'button', 'class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#translate']) ?>
                            <?php MyActiveForm::end(); ?>
                        </div>
                    </div>

                    <?= GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover table-middle no-margin'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [

                            ['attribute' => 'object_name', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'object_type', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'object_instance_number', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'interval'],
                            ['attribute' => 'base_value', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'unit', 'format' => ['arrayKey', 'name'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'is_upload', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'category', 'format' => ['array', 'index' => 'name'], 'headerOptions' => ['data-hide' => 'phone']],

                            ['class' => 'common\library\MyActionColumn', 'template' => '{update} {shield}', 'headerOptions' => ['data-class' => 'expand']],
                        ],
                    ]); ?>

                    <?php
                    $this->registerJs ("table_config('datatable', {\"button\":[]});");
                    ?>

                </div>
            </div>
        </article>
    </div>
</section>
<div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">翻译EXCEL上传</h4>
            </div>
            <?php $form = MyActiveForm::begin(['action' => '/point/bacnet-crud/upload-translate' ,'options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="modal-body">
                <div class="well" id="condition">
                    <?= MyHtml::hiddenInput('view', Yii::$app->request->get('view')) ?>
                    <?= MyHtml::fileInput('translate') ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    取消
                </button>
                <button type="submit" class="btn btn-primary">
                    提交
                </button>
            </div>
            <?php MyActiveForm::end(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
