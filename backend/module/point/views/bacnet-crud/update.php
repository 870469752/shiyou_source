<?php

use yii\helpers\Html;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\BacnetPoint $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Bacnet Point',
]) . MyFunc::DisposeJSON($model->name);
?>
    <?= $this->render('_form'.ucfirst(Yii::$app->request->get('view', '')), [
        'protocol_id' => $protocol_id,
        'model' => $model,
        'category_tree' => $category_tree,
        'allUnit' => $allUnit
    ]) ?>
