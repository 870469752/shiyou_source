<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Simulate Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit' => $unit,
    ]) ?>
