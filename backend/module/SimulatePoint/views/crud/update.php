<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Simulate Point',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'protocol_id' => $protocol_id,
        'model' => $model,
        'unit' => $unit,
    ]) ?>
