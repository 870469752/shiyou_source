<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
/**
 * @var yii\web\View $this
 * @var backend\models\SimulatePoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>

						<?= $form->field($model, 'name')->textInput(['disabled' => 'true']) ?>

                        <?= $form->field($model, 'interval')->dropDownList([5*60 => '5 '.Yii::t('app', 'Minutes'),
                                                                            10*60 => '10 '.Yii::t('app', 'Minutes'),
                                                                            15*60 => '15 '.Yii::t('app', 'Minutes'),
                                                                            60*60 => '60 '.Yii::t('app', 'Minutes')],['class'=>'select2','style' => 'width:200px']); ?>

                        <?= Html::hiddenInput('SimulatePoint[time_type]', 'day', ['id' => 'simulatepoint-time_type']); ?>

                        <!--在系统中选择 点位单位-->
<!--                        --?//= $form->field($model, 'unit')->dropDownList(
//                            $unit,
//                            ['class'=>'select2', 'style' => 'width:200px']
//                        )?-->
                        <?= Html::hiddenInput("SimulatePoint[name]", 'System-ectricity_prices'); ?>
                        <?= Html::hiddenInput("SimulatePoint[is_system]", 1); ?>
                        <?= Html::hiddenInput("SimulatePoint[value_pattern]"); ?>
                        <!--开始 日历插件-->
                        <div class="jarviswidget jarviswidget-color-magenta"
                             data-widget-deletebutton="false"
                             data-widget-editbutton="false"
                             data-widget-colorbutton="false"
                             data-widget-sortable="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                                <h2> <?= Yii::t('app', 'Select Time')?> </h2>
                            </header>
                            <!-- widget div-->
                            <div>
                                <div class="widget-body no-padding">
                                    <!-- content goes here -->
                                    <div class="widget-body-toolbar">
                                        <input type="text" style = "width:110px;display:none" id ="timepick" name="mydate" readonly placeholder="Select a date" class="form_datetime"/>
                                        <div id="calendar-buttons">
                                            <!-- add: non-hidden - to disable auto hide -->
                                            <div class="btn-group" id = 'select_btn'>
                                                <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                                    Showing <i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu js-status-update pull-right">
                                                    <li>
                                                        <a href="javascript:void(0);" id="mt">Month</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" id="ag">Agenda</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" id="td">Today</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                   id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                   id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="calendar"></div>
                                    <!-- end content -->
                                </div>
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!--结束  日历插件-->

                        <?= $form->field($model, 'is_shield')->checkbox() ?>

                        <?= $form->field($model, 'is_upload')->checkbox() ?>

						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/moment.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/qtip/jquery.qtip-1.0.0-rc3.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/bootbox/bootbox.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/my_js/calendar.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset'])
?>

<script>

window.onload = function () {
    var date = new Date();
    var str = '';
    var up_type = 'year';
    var _data = '';
    /*初始化日历事件  获取所有event 并展示到日历上*/
    <?php if ($value_pattern = $model->value_pattern): ?>
    str = <?=$value_pattern;?>;
    $.each(str, function (key, value) {
        if (key == 'data') {
            $.each(value, function (k1, v1) {
                if (k1 == 'cycle') {
                    up_type = v1;
                }
                else if (k1 == 'value') {
                    _data = '['
                    $.each(v1, function (k2, v2) {
                        var curent_day = new Date(v2['start']);
                        _data += '{';
                        _data += "start:$.fullCalendar.parseDate('" + v2['start'] + "') ,";
                        _data += "end:$.fullCalendar.parseDate('" + v2['end'] + "')  ,";
                        _data += "title: 'value is " + v2['formula'] + "',";
                        _data += 'description:' + "'<span><?=Yii::t('app', 'Prompt')?></span>:<li><?=Yii::t('app', 'Click delete this time')?></li><li><?=Yii::t('app', 'Drag the right margin change the time')?></li>',";
                        _data += 'className:' + "['event', 'bg-color-darken'],";
                        if(curent_day.getHours() || curent_day.getMinutes()){
                            _data += 'allDay: false';
                        }
                        _data += '},';
                    })

                }
            })
        }

    })
    _data = _data.substring(0, _data.length - 1);
    _data += ']';
    $('#calendar').fullCalendar('addEventSource', eval("(" + _data + ")"));
    <?php endif;?>
    /*                                                                                      结束*/


    /*添加自定义按钮 点击事件*/
    $('#calendar-buttons #btn-prev').click(function () {
        $('#calendar').fullCalendar('prev');
        return false;
    });
    $('#calendar-buttons #btn-next').click(function () {
        $('#calendar').fullCalendar('next');
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });
    /* 隐藏 日历切换按钮 */
    function hide_buttons() {
        $('.fc-header-right, .fc-header-center').hide();
        $('#calendar-buttons #btn-prev').hide();
        $('#calendar-buttons #btn-next').hide();
        $('#timepick').hide();
    }

    /* 显示 日历切换按钮 */
    function show_buttons() {
        $('.fc-header-right, .fc-header-center').show();
        $('#calendar-buttons #btn-prev').show();
        $('#calendar-buttons #btn-next').show();
        $('#timepick').show();
    }

    $('header').click(function () {
        $('#popover').hide();
    })

    /*                                                                                 对默认类型 下拉菜单 进行监听*/
    $('#simulatepoint-time_type').change(function () {
        var _type = 'day';

        $('#select_btn').hide();
        $('#popover').hide();
        if (up_type != _type) {
            $('#calendar').fullCalendar('removeEvents');
        }
        if (_type == 'year') {
            timeTypeChange('month');
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('today');
            show_buttons();
        }
        else if (_type == 'month') {
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if (_type == 'week') {

            $('#calendar').fullCalendar('changeView', 'agendaWeek');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if (_type == 'day') {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if(_type == 'custom') {
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('today');
            show_buttons();
            $('#select_btn').show();
            $('#mt').click(function () {
                timeTypeChange('month');
                $('#popover').hide();
                $('#calendar').fullCalendar('changeView', 'month');
            });

            $('#ag').click(function () {
                timeTypeChange('day');
                $('#popover').hide();
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
            });

            $('#td').click(function () {
                timeTypeChange('day');
                $('#popover').hide();
                $('#calendar').fullCalendar('changeView', 'agendaDay');
            });
        }
    });

    function timeTypeChange(time_type)
    {
        $(".form_datetime").datetimepicker('remove');
        $(".form_datetime").val('');
        if(time_type == 'month')
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm",
                startView: 'year',
                minView: 'year',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01');
        }
        else
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm-dd",
                startView: 'month',
                minView: 'month',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01-01');
        }
    }

    /*时间选择框中的 值 变化时 对应的 fullcalendar 也发生变化*/
    $('.form_datetime').datetimepicker().on('changeDate', function(ev){
        var time_data = new Date($(".form_datetime").val());
        $("#calendar").fullCalendar('gotoDate', time_data.getFullYear(),time_data.getMonth(),time_data.getDate());
    });

    /*                                                                                                  结束*/
    $('#simulatepoint-time_type').val(up_type);
    $('#simulatepoint-time_type').trigger('change');
    $('.jarviswidget-ctrls').hide();
    /*点击提交  收集日历上所有的事件 并将时间值赋给 value_pattern input框*/
    $('.btn').click(function () {

        var _datas = '';

        var data = $('#calendar').fullCalendar('clientEvents');

        $.each(data, function (key, value) {
            $.each(value, function (k, v) {
                if (k == 'start' || k == 'end') {
                    _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm") + ',';
                    if(k == 'end'){
                        _datas += value['title'].replace(/[A-Za-z ]|[^u4E00-u9FA5|^\.]/g,'');
                    }
                }

            })

            _datas += ';';
        })

        $("input[name='SimulatePoint[value_pattern]']").val(_datas);
    })
}
</script>