<?php

namespace backend\module\simulatepoint;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\simulatepoint\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
