<?php

namespace backend\module\SimulatePoint\controllers;

use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
