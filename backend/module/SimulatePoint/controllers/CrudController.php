<?php

namespace backend\module\simulatepoint\controllers;

use Yii;
use backend\models\SimulatePoint;
use backend\models\search\SimulatePointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\Unit;
/**
 * CrudController implements the CRUD actions for SimulatePoint model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SimulatePoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SimulatePointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SimulatePoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SimulatePoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SimulatePoint;
        if ($model->load(Yii::$app->request->post()) && $model->SimulateSave()) {
            return $this->redirect(['index']);
        } else {
            $unit = Unit::getAllUnit();
            return $this->render('create', [
                'model' => $model,
                'unit' => $unit,
            ]);
        }
    }

    /**
     * Updates an existing SimulatePoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $protocol_id = '')
    {
        $model = $this->findModel($id);
        $params = Yii::$app->request->getBodyParams();
        if ($model->load(Yii::$app->request->post()) && $model->SimulateSave()) {
            //判断当前修改是哪个控制器请求的 如果protocol_id不为空说明来自point/crud的请求在执行完毕后跳转回point/crud,否则跳到当前控制器
            if($params['protocol_id']) {
                return $this->redirect(['/point/crud']);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            $unit = Unit::getAllUnit();
            $model->name = Myfunc::DisposeJSON($model->name);
            $model->filter_max_value = MyFunc::JsonFindValue($model->value_filter, 'data.max');
            $model->filter_min_value = MyFunc::JsonFindValue($model->value_filter, 'data.min');
            $model->interval = MyFunc::SecondChangeToTime($model->interval);
            return $this->render('update', [
                'protocol_id' => $protocol_id,
                'model' => $model,
                'unit' => $unit,
            ]);
        }
    }

    /**
     * Deletes an existing SimulatePoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SimulatePoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SimulatePoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SimulatePoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 为电价做一个表单
     */
    public function actionElePrice()
    {
        $model = SimulatePoint::getSystemPointByName('System-ectricity_prices');
        if ($model->load(Yii::$app->request->post()) && $model->SimulateSave()) {
            return $this->redirect(['/simulate-point/crud/ele-price']);
        } else {
            /*最终电价也是一个模拟点位为了和普通的模拟点位区分，固定电价模拟点位的名称*/
            $model->name = '{"data_type":"description","data":{"zh-cn":"电价","en-us":"Electricity prices"}}';
            $model->time_type = 'day';
            $model->name = Myfunc::DisposeJSON($model->name);
            return $this->render('ele_price', [
                    'model' => $model,
                ]);
        }
    }
}
