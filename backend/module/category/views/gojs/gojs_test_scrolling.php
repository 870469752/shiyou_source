


<div id="sample">
    <div id="myDiagram" style="border: solid 1px black; width:100%; height:600px"></div>

    <br/>
    The "AutoRepeatButton" Panel is also defined in that file.
</div>

<input type="button" id="save" value="save">
<!--<input type="button" id="update" value="update">-->
<textarea id="mySavedModel" style="width:100%;height:300px;display: '';">

  </textarea>
<?php
//$this->registerJsFile("js/gojs/go.js");


$this->registerJsFile("js/gojs/go.min.js"  );

$this->registerJsFile("js/gojs/ScrollingTable.js" );
//$this->registerJsFile("js/gojs/go-debug.js" );
?>
<script>

    window.onload = function () {
        init();
        $("#save").click(function () {
            document.getElementById("mySavedModel").value =myDiagram.model.toJson();

        })
        function init() {
//            if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;
            //ScrollingTable ��ʼ��
            InitScrollingTable();


            myDiagram =
                $(go.Diagram, "myDiagram",
                    {
                        initialContentAlignment: go.Spot.Center,
                        validCycle: go.Diagram.CycleNotDirected,  // don't allow loops
                        "undoManager.isEnabled": true
                    });

            var fieldTemplate =
                $(go.Panel, "TableRow",  // this Panel is a row in the containing Table
                    new go.Binding("portId", "name"),  // this Panel is a "port"
                    {
                        background: "transparent",  // so this port's background can be picked by the mouse
                        fromSpot: go.Spot.Right,  // links only go from the right side to the left side
                        toSpot: go.Spot.Left,
                        // allow drawing links from or to this port:
                        fromLinkable: true, toLinkable: true
                    },
                    { // allow the user to select items -- the background color indicates whether "selected"
                        //?? maybe this should be more sophisticated than simple toggling of selection
                        click: function(e, item) {
                            // assume "transparent" means not "selected", for items
                            var oldskips = item.diagram.skipsUndoManager;
                            item.diagram.skipsUndoManager = true;
                            if (item.background === "transparent") {
                                item.background = "dodgerblue";
                            } else {
                                item.background = "transparent";
                            }
                            item.diagram.skipsUndoManager = oldskips;
                        }
                    },
                    $(go.Shape,
                        { width: 12, height: 12, column: 0, strokeWidth: 2, margin: 4,
                            // but disallow drawing links from or to this shape:
                            fromLinkable: false, toLinkable: false },
                        new go.Binding("figure", "figure"),
                        new go.Binding("fill", "color")),
                    $(go.TextBlock,
                        { margin: new go.Margin(0, 2), column: 1, font: "bold 13px sans-serif",
                            // and disallow drawing links from or to this text:
                            fromLinkable: false, toLinkable: false },
                        new go.Binding("text", "name")),
                    $(go.TextBlock,
                        { margin: new go.Margin(0, 2), column: 2, font: "13px sans-serif" },
                        new go.Binding("text", "info"))
                );


            myDiagram.nodeTemplate =
                $(go.Node, "Vertical",
                    {
                        selectionObjectName: "SCROLLING",
                        resizable: true, resizeObjectName: "SCROLLING"
                    },
                    $(go.TextBlock,
                        { font: "bold 14px sans-serif" },
                        new go.Binding("text", "key")),
                    $(go.Panel, "Auto",
                        $(go.Shape, { fill: "white" }),
                        $("ScrollingTable","TABLE",
                            new go.Binding("TABLE.itemArray", "items"),
                            {
                                name: "SCROLLING",
                                desiredSize: new go.Size(NaN, 60),
                                "TABLE.itemTemplate": fieldTemplate,
//                                "TABLE.itemTemplate":
//                                    $(go.Panel, "TableRow",
//                                        $(go.Picture,"/uploads/pic/test.png" , { column: 0 }),
//                                        $(go.Picture,"/uploads/pic/test.png" , { column: 1 }),
//                                        $(go.TextBlock, { editable: true },{ column: 0 ,margin: new go.Margin(0, 0, 0, 0)}, new go.Binding("text", "name")),
//                                        $(go.TextBlock, { editable: true },{ column: 1 ,margin: new go.Margin(0, 0, 0, 0)}, new go.Binding("text", "value"))
//                                    ),
                                "TABLE.defaultColumnSeparatorStroke": "gray",
                                "TABLE.defaultColumnSeparatorStrokeWidth": 0.5,
                                "TABLE.defaultRowSeparatorStroke": "gray",
                                "TABLE.defaultRowSeparatorStrokeWidth": 0.5,
//                                "TABLE.defaultSeparatorPadding": new go.Margin(1, 3, 0, 3)
                            }
                        )
                    )
                );


            myDiagram.model = new go.GraphLinksModel([
                { key: "Alpha",  items: [
                    { name: "field1", info: "", color: "#F7B84B", figure: "Ellipse" },
                    { name: "field2", info: "the second one", color: "#F25022", figure: "Ellipse" },
                    { name: "fieldThree", info: "3rd", color: "#00BCF2" }
                ],
                },
                {
                    key: "Beta", items: [
                    { name: "fieldA", info: "",       color: "#FFB900", figure: "Diamond" },
                    { name: "fieldB", info: "",       color: "#F25022", figure: "Rectangle" },
                    { name: "fieldC", info: "",       color: "#7FBA00", figure: "Diamond" },
                    { name: "fieldD", info: "fourth", color: "#00BCF2",  figure: "Rectangle" }
                ],
                },
                {
                    key: "One item",items: [
                    { name: "fieldA", info: "",       color: "#FFB900", figure: "Diamond" },
                    { name: "fieldB", info: "",       color: "#F25022", figure: "Rectangle" },
                    { name: "fieldC", info: "",       color: "#7FBA00", figure: "Diamond" },
                    { name: "fieldD", info: "fourth", color: "#00BCF2",  figure: "Rectangle" }
                ],
                },
                {
                    key: "No items", items: [
                    { name: "fieldA", info: "",       color: "#FFB900", figure: "Diamond" },
                    { name: "fieldB", info: "",       color: "#F25022", figure: "Rectangle" },
                    { name: "fieldC", info: "",       color: "#7FBA00", figure: "Diamond" },
                    { name: "fieldD", info: "fourth", color: "#00BCF2",  figure: "Rectangle" }
                ],
                }
            ]);
        }

    }
</script>