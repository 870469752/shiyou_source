<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Category $model
 */
$this->title = Yii::t('category', 'Create {modelClass}', [
  'modelClass' => 'Category',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
