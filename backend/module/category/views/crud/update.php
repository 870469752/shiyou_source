<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Category $model
 */

$this->title = Yii::t('category', 'Update {modelClass}: ', [
  'modelClass' => 'Category',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
