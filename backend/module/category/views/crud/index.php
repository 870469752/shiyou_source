<?php
use common\library\MyHtml;
use Yii\web\View;
use common\library\MyFunc;
use yii\helpers\Url;
//echo '<pre>';
//print_r($tree);
//die;
$this->title = Yii::t('category', 'Category');
?>
    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
                        <span class="widget-icon">
                            <i class="fa fa-table"></i>
                        </span>

                        <h2><?= MyHtml::encode($this->title) ?></h2>
                    </header>

                    <!-- widget content -->
                    <div class="widget-body">

                        <div id="nestable-menu">
                            <!--<button type="button" class="btn btn-default"
                                    data-action="expand-all">
                                <?/*= Yii::t('category', 'Expand All') */?>
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-action="collapse-all">
                                <?/*= Yii::t('category', 'Collapse All') */?>
                            </button>-->
                            <button type="button" class="btn btn-default"
                                    data-action="add">
                                <?= Yii::t('category', 'Add Category') ?>
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-action="default">
                                <?= Yii::t('category', 'Restore Modify') ?>
                            </button>
                            <button type="button" class="btn btn-default" data-action="save">
                                <?= Yii::t('category', 'Save Modify') ?>
                            </button>
                        </div>
                        <div class="dd" id="nestable">
                            <?php echo MyHtml::EnergyTreeRecursion($tree); ?>
                        </div>


                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget -->
            </article>
        </div>
    </section>
    <div class="modal fade" id="pointSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">点位批量关联</h4>
                </div>
                <div class="modal-body">
                    <div class="well" id="condition">
                        功能正在开发中，下个版本将看到惊喜
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-primary">
                        提交
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


<?php
$this->registerJsFile("js/color_picker/js/evol.colorpicker.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("js/color_picker/css/evol.colorpicker.min.css", ['backend\assets\AppAsset']);
?>

    <script>
        window.onload = function () {

            $('.noIndColor').colorpicker({
                displayIndicator: false
            });


            $('.dd').nestable('collapseAll');
            var GenerateComputePoint = <?=YII::$app->request->get('GenerateState',0)?>;
            if(GenerateComputePoint > 0){
                
            }

            // 子分类标签模板
            var li = '\
        <li class="dd-item dd3-item" data-id="">\
            <div class="dd-handle dd3-handle">Drag</div>\
		    <div class="dd3-content">\
		        <span class="name">请单击修改名称</span>\
		        <em class="close pull-right " style="padding-left:10px;">×</em>\
                <a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" title="" data-placement="top" data-original-title="添加子类">\
                    <i class="fa fa-sitemap fa-fw"></i>\
		        </a>\
		    </div>\
		</li>';

            /**
             * 分类中的图标事件
             */
            $('#nestable').on('click', '.close',function (e) {
                if (confirm("是否删除") == true)
                    $(this).parent().parent().remove();
            }).on('click', '.add',function (e) { // 添加子分类
                var ol = $('<ol>', {"class": "dd-list"});
                var ol_object = $(this).parent().parent().find('ol').first();
                if (ol_object.length <= 0) {
                    $(this).parent().parent().append(ol);
                    ol_object = ol;
                }
                ol_object.append(li);
            }).on('click', '.name',function (e) { // 点击名称弹出文本框
                var input = $('<input>', {"class": "text-name", "height": "20px", "type": "text", "value": $(this).text()});
                $(this).html(input);
                input.focus();
            }).on('blur', '.text-name',function (e) { // 失去焦点保存文本
                edit_text($(this));
            }).on('keydown', '.text-name', function (e) { // 按回车保存文本
                if (e.keyCode == 13) {
                    edit_text($(this));
                }
            });

            /**
             * 编辑分类
             */
            function edit_text(object) {
                var name_value = object.val() ? object.val() : '请单击修改名称'; // 如果没有就显示默认值
//                alert(name_value);
                var zh=object.parent().parent().parent().attr('data-zh');// 获取中文名称
                var en=object.parent().parent().parent().attr('data-en');// 获取英文名称;
                if('<?= Yii::$app->language ?>'=='zh-cn'){
                    object.parent().parent().parent().attr('data-zh', name_value);
                }else{
                    object.parent().parent().parent().attr('data-en', name_value);
                }
                object.parent().html(name_value);
            }

            /**
             * 最顶上的菜单按钮
             */
            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target), action = target.data('action');
                if (action === 'expand-all') { // 展开所有
                    $('.dd').nestable('expandAll');
                }
                else if (action === 'collapse-all') { // 合并所有
                    $('.dd').nestable('collapseAll');
                }
                else if (action === 'add') { // 添加分类
                    var ol = $('<ol>', {"class": "dd-list"});
                    var ol_object = $("#nestable ol").first();
                    if (ol_object.length <= 0) {
                        $("#nestable").append(ol);
                        ol_object = ol;
                    }
                    ol_object.append(li);
                }
                else if (action === 'default') { // 恢复默认
                    location.reload();
                }
                else if (action === 'save') { // 保存数据
                    //加入color数据到表单数据
                    $('.noIndColor').each(function(){
                        var list=$(this).parent().parent().parent().parent();
                        var color=$(this).next().css("background-color");
                        list.attr('data-rgba',color);
                    });

                    console.log($('#nestable').nestable('serialize'));
                    $.post("<?= Url::toRoute('/category/crud/update') ?>", {
                            "data": $('#nestable').nestable('serialize')
                        },
                        function (link) {
//                            console.log(link);
                            location.href = link;
                        });
                }
            });
        }
    </script>
<?php
$this->registerJs("$('#nestable').nestable();", View::POS_READY);
$this->registerJsFile('/js/plugin/jquery-nestable/jquery.nestable.min.js', [
    'depends' => 'yii\web\JqueryAsset'
]);
?>