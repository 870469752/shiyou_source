<?php
use yii\helpers\Html;
use common\library\MyHtml;
use yii\helpers\BaseHtml;
use common\library\MyFunc;
use common\library\MyActiveForm;
use yii\bootstrap;
use yii\bootstrap\ActiveForm;
//echo '<pre>';
//print_r($all_sub_system);
//die;

?>

<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }

    #infoBoxHolder {
        z-index: 300;
        position: absolute;
        left: 5px;
    }

    #infoBox {
        border: 1px solid #999;
        padding: 8px;
        background-color: whitesmoke;
        opacity:0.9;
        position: relative;
        width: 250px;
    //height: 60px;
        font-family: arial, helvetica, sans-serif;
        font-weight: bold;
        font-size: 11px;
    }

    /* this is known as the "clearfix" hack to allow
       floated objects to add to the height of a div */
    #infoBox:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }

    div.infoTitle {
        width: 50px;
        font-weight: normal;
        color:  #787878;
        float: left;
        margin-left: 4px;
    }

    div.infoValues {
        width: 150px;
        text-align: left;
        float: right;
    }

</style>

<div id="contextMenu">
    <ul>
        <li><a href="#" id="menu1" onclick="AtomicEvent()">原子事件</a></li>
        <li><a href="#" id="menu5" onclick="">属性</a></li>
        <li><a href="#" id="menu3" onclick=" ">持续时间</a></li>
        <li><a href="#" id="font-color" onclick=" ">更新字体/颜色</a></li>
    </ul>
</div>



<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>


                    <div class="widget-toolbar smart-form" data-toggle="buttons">
                        <button class="btn btn-xs btn-primary" id="add_subsystem"  >
                            添加子系统
                        </button>

                        <button class="btn btn-xs btn-primary" id="save"  >
                            保存
                        </button>
                        <button class="btn btn-xs btn-primary" id="add_link"  >
                            链接
                        </button>
                        <button class="btn btn-xs btn-primary" id="font_color"  >
                            字体/颜色
                        </button>
                        <button class="btn btn-xs btn-primary" id="remove_attribute"  >
                            属性框
                        </button>

                    </div>



                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>楼层子系统 </h2>

                </header>



                <div>
                        <span style="display: inline-block; vertical-align: top; padding: 5px">

                              <div>
                                  <div id="image_preview_parent">
                                      <div id="image_preview" style="border: solid 1px black;width: 150px; height: 100px"></div>
                                  </div>
                                  <div id="myPaletteSmall" style="border: solid 1px black;width: 150px; height: 600px">
                                      <!-- menu start -->
                                      <div id="accordion" style="width: 150px;">
                                          <h3>图1</h3>
                                          <div id="sample1" style="max-height: 450px;">
                                          </div>
                                          <h3>摄像头图</h3>
                                          <div style="max-height: 450px;">
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头1.jpg">摄像头1</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头2.jpg">摄像头2</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头3.jpg">摄像头3</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头4.jpg">摄像头4</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头5.jpg">摄像头5</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头6.jpg">摄像头6</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头7.jpg">摄像头7</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头8.jpg">摄像头8</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/摄像头9.jpg">摄像头9</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="main" url="/uploads/pic/绘图1.svg">摄像头10</a>
                                              <a  href="javascript:void(0);"class="btn btn-default btn-xs btn-block" category="state" url="/uploads/pic/圆红.svg"
                                                  config="{1:'/uploads/pic/圆红.svg',2:'/uploads/pic/圆绿.svg',3:'/uploads/pic/圆黑.svg'}"                             >红</a>

                                          </div>
                                          <h3>编辑框</h3>
                                          <div style="max-height: 450px;">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value" url="/uploads/pic/按钮6.png">值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="value" url="/uploads/pic/按钮3.png">值2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="only_value" url="">值3</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="edit">编辑</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="onoffvalue" url="/uploads/pic/按钮3.png">开关值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="onoffvalue" url="/uploads/pic/按钮6.png">开关值2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="alarmvalue" url="/uploads/pic/按钮3.png">警报值1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="alarmvalue" url="/uploads/pic/按钮6.png">警报值2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="table">表格</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="group">分组</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="main_edit" url="/uploads/pic/按钮6.png">main_edit</a>
                                          </div>
                                          <h3>按钮</h3>
                                          <div style="max-height: 450px;">
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮1.png">按钮2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮2.png">按钮3</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮3.png">按钮4</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="test" url="/uploads/pic/按钮5.png">按钮5</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="line1">线1</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="line2">线2</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="Inductor">电容</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="Ground">接地</a>
                                              <a href="javascript:void(0);" class="btn btn-default btn-xs btn-block" category="figure" url="line1">线5</a>
                                          </div>

                                      </div>
                                      <!-- end-->
                                  </div>


                              </div>
                 </span>

                     <span style="display: inline-block; vertical-align: top; padding: 5px; width:85%">
                    <div id="tabs2">
                        <ul>
                            <!--            隐藏初始化页面                -->
                            <li style="display:'';">
                                <a href="#tabs-0">sample</a>
                            </li>
                        </ul>
                        <div id="tabs-0">
                            <!--         隐藏初始化页面                  -->
                            <div id="myDiagramX" style="display:'';border: solid 1px black; width:100%; height:650px;background: black"></div>
                        </div>
                    </div>
                     </span>
                    <div id="infoBoxHolder">
                        <!-- Initially Empty, it is populated when updateInfoBox is called -->
                    </div>
                </div>




            </div>
        </article>
    </div>
</section>
<!--add link-->
<div id="link_update" title="子系统" style="display:none;width: auto">

    <form>
        <fieldset>
            <div class="form-group">
                <label>名称</label>

                <input class="form-control" id="node_name" value="" placeholder="Text field" type="text">

            </div>


            <div class="form-group">
                <label>节点链接</label>

                <input class="form-control" id="node_link" value="" placeholder="Text field" type="text">

            </div>





        </fieldset>

    </form>

</div>

<!-- font  color-->
<div id="update_font_color" title="字体/颜色" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体类型',null,  ['class' => 'control-label',   'id' => '_name'])?>
        <select id="font_family" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="微软雅黑">微软雅黑</option>
            <option   value="华文细黑">华文细黑</option>
            <option   value="中易宋体">中易宋体</option>
            <option   value="sans-serif">sans-serif</option>
            <option   value="Arial">Arial</option>
        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体变形',null,  ['class' => 'control-label',   'id' => '_name'])?>
        <select id="font_variant" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="normal">正常</option>
            <option   value="small-caps">小型</option>
            <option   value="inherit">继承型</option>

        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体粗细',null,  ['class' => 'control-label',   'id' => '_name'])?>
        <select id="font_weight" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="normal">正常</option>
            <option   value="bold">粗体</option>
            <option   value="bolder">更粗</option>
            <option   value="lighter">更细</option>
        </select>
    </div>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体大小',null,  ['class' => 'control-label',   'id' => '_name'])?>
        <select id="font_size" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="xx-small">最小</option>
            <option   value="x-small">较小</option>
            <option   value="small">小</option>
            <option   value="medium">正常</option>
            <option   value="large">大</option>
            <option   value="x-large">较大</option>
            <option   value="xx-large">最大</option>
        </select>
    </div>

    <div style="margin: 5px;width: 300px;">
        <?=Html::label('字体颜色',null,  ['class' => 'control-label',   'id' => '_name'])?>
        <select id="font_color" class= "chosen_select"   style="width: 100%" name="point_id" placeholder="请选择相应点位">
            <option   value="red">红色</option>
            <option   value="black">黑色</option>
            <option   value="white">白色</option>
            <option   value="blue">蓝色</option>
        </select>
    </div>

</div>


<!-- add tab Demo -->
<div id="addtab" title="子系统" style="display:none;width: auto">

    <form>

        <fieldset>
            <input name="authenticity_token" type="hidden">
            <div class="form-group">
                <label>子系统名称</label>
                <input class="form-control" id="sub_system_name" value="" placeholder="Text field" type="text">
            </div>

            <div class="form-group">
                <label>有无底图</label>
                <?=HTML::dropDownList('image_base',null,[0=>'无',1=>'有'],['class'=>'select2','id'=>'image_base'])?>

            </div>
            <div class="form-group">
                <label>子系统类型</label>
                <!--                <select id="sub_system_category" class="select2" name="sub_system_category">-->
                <!--                    <option value="3">&nbsp;给排水</option>-->
                <!--                    <option value="4">排风机</option>-->
                <!--                </select>-->
                <?=HTML::dropDownList('sub_system_category',null,$sub_system_in,['class'=>'select2','id'=>'sub_system_category'])?>

            </div>


        </fieldset>

    </form>

</div>

<!--属性面板-->
<div id="AttributeMenu" style="width: 400px;position:relative;z-index:10;display: ">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                <?=Html::hiddenInput('image_group_id',$sub_system_model->id,['id'=>'image_group_id'])?>
                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('sub_system_id',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('sub_system_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'sub_system_id'])?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                </div>

                                <div class="col-md-1"></div>
                                <div class="help-block"></div>
                                <?php echo'<div class="row"><div class="col-md-9" style="margin: 5px;">';?>
                                <?=Html::label('点位',null,  ['class' => 'control-label',   'id' => '_name'])?>
                                <?php echo '<select id= "select_point"   style="width: 100%" name="point_id" placeholder="请选择相应点位">';
                                foreach($num as $key=>$value)
                                    echo '<option   value='.$key.'>'.$value.'</option>';
                                echo '</select></div></div>';
                                ?>
                                <div class="help-block"></div>


                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                                    </div>
                                </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>

<!--<input type="button" id="save" value="save">-->
<!--<input type="button" id="update" value="update">-->
<textarea id="mySavedModel" style="width:100%;height:300px;display: ;">

  </textarea>
<p id="diagramEventsMsg" style="display: none;">Msg</p>


<!-- html 点位搜索弹出框 start-->
<section id="point_search" class="" style="display:none;width: auto">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?=Yii::t('app', 'Search Point')?></h2>
            <div class="jarviswidget-ctrls">

            </div>
        </header>
        <!-- 点位 搜索展示框 start-->
        <!--         Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2><?=Yii::t('point', '点位')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <table id="selected_datatable_tabletools1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>

                        </tr>
                        </thead>
                        <tbody id="tbodyData_one">

                        </tbody>
                    </table>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <div id="_loop1" class="form-actions">
            <?= Html::submitButton(Yii::t('app', '搜索添加点位') , ['class' => 'btn btn-primary', 'id' => 'sub_prev']) ?>
            <?= Html::submitButton(Yii::t('app', '绑定点位') , ['class' => 'btn btn-primary', 'id' => 'sub_next']) ?>
        </div>
        <!-- end widget -->
        <!-- 点位 搜索展示框 end-->
        <!-- 参数主体 start-->
        <!-- 点位搜索框 start      -->
        <div id = '_loop' style = 'border:2px solid #D5D5D5;padding-bottom:15px'>
            <div style = 'padding:0px 7px 15px 7px'>
                <?php $form = MyActiveForm::begin(['method' => 'get', 'id' => '_ws']) ?>
                <fieldset>

                    <div class="form-group">
                        <div class="col-md-2" style="margin-left:20px;">
                            <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Point Name'), 'class' => 'form-control'])?>
                        </div>
                        <!--                        <div class="col-md-1"></div>-->
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('energy_category_id', 0, $category_tree
                                , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('location_category_id', 0, $location_tree
                                , ['placeholder' => Yii::t('app', 'Choose Location'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
                        <!--                        <div class="col-md-1"></div>-->

                        <div class="col-md-2" style="display:none;">
                            <?=Html::dropDownList('protocol_id', isset($protocol_id)?$protocol_id:null, ['' => '',
                                ''=>'所有点位',
                                1 => Yii::t('app', 'BACnet Point'),
                                4 => Yii::t('app', 'Calculate Point'),
                                7 => Yii::t('app', 'Simulation Point'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Point Type'), 'id' => 'point_location_demo'])?>
                        </div>

                    </div>
                    <!--提交-->
                    <div class="form-actions" style = 'float:right;padding:0px;margin:-19px 130px -20px 39px;'>
                        <a id="point_search_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                        <?=Yii::t('app', 'Search Point')?>
                        </a>
                    </div>
                </fieldset>
            </div>

            <?php MyActiveForm::end(); ?>
        </div>


        <div id= 'point_search_div' class="widget-body"  ">


        <?= Html::hiddenInput('point_ids', '', ['id' => 'point_selected'])?>

        <fieldset style="display:none;">

            <!-- widget div-->
            <div class = 'form-group'>
                <label class="col-sm-1 control-label"><?=Yii::t('app', 'Bacth Filter')?></label>
                <div class = 'col-sm-6' >
                    <?=Html::dropDownList('point_ids', '', ['' => '',
                    ],
                        ['id' => 'point_select', 'multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Point Type')])?>
                </div>
            </div>

            <!--    批量修改 点位的一些属性      -->
            <div class = 'form-group' style = 'margin-top:230px'>

            </div>


            <div style="display: inline-block;">
            </div>

        </fieldset>



        <!-- Widget ID (each widget will need unique ID)-->
        <div  class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false" data-widget-deletebutton="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2><?=Yii::t('point', 'Point List')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyData">

                        </tbody>
                    </table>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->





        <!-- Widget ID (each widget will need unique ID)-->
        <div style="display: none" class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2><?=Yii::t('point', 'Selected Point')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <table id="selected_datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                        </tr>
                        </thead>
                        <tbody id="tbodySelected">

                        </tbody>
                    </table>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->



        <div class="form-actions">

            <?= Html::submitButton(Yii::t('app', '下一步') , ['class' => 'btn btn-primary', 'id' => '_sub']) ?>

            <div style="">
                <div style = 'padding-left:12px;margin-right:30px;margin-bottom:30px;color:red;text-align: left; display:none' id = '_error_log'></div>
            </div>

        </div>

        <!-- <?php //ActiveForm::end(); ?> -->
        <!--            --><?php //ActiveForm::end(); ?>
        <!--            <button id="checkAllBtn" class="button">CheckAll</button>-->
    </div>

    <!-- 点位搜索框 end      -->
    </div>
    <!-- 图表内容 end-->
</section>
<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/ScrollingTable.js" );

$this->registerJsFile("js/gojs/node_template_edit_special.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);


$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<script>
    var sub_system_data=new Array();
    var link=null;
    var link_name=null;
    var button=null;
    var button_key=null;
    var diagram_now=null;

    var GlobalData=[];
    var selectedPointContainer = [];

    function resize() {
        for(var key in sub_system_data) {
            var diagram = sub_system_data[key];
            diagram.requestUpdate();
//             diagram.rebuildParts();
        }

    }
    //修改点位名称后更改selectedPointContainer 内的值
    function update_selected(id,value){
        for(var key in selectedPointContainer){
            if(selectedPointContainer[key]['pointId']== id)
                selectedPointContainer[key]['pointName']= value;
        }
    }
    window.onload = function () {
        $("#font_color").click(function(){
            font_color_dialog.dialog('open');
        })


        $("#menu5").click(function(){
            //得到子系统id  元素key
            var sub_system_id=$("#sub_system_id").val();
            var element_id=$("#element_id").val();

            //ajax 查询表格绑定的点位信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-get-element",
                data: {sub_system_id:sub_system_id, element_id:element_id},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
                    msg=eval('['+msg+']');
                    var data=msg[0];
//                    var binding_id=data['binding_id'];
//                    binding_id=eval(binding_id);
//                    console.log(typeof (binding_id));
                    //更新 绑定点位信息

                    selectedPointContainer=data;
                    console.log(selectedPointContainer);
                    regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');
                }
            });

            //显示点位展示页面
            $("#_loop").hide();
            $("#point_search_div").hide();
            $("#wid-id-1").show();
            $("#_loop1").show();

            point_search_dialog.dialog("open");
        })
        //字体颜色改变
        $("#font-color").click(function(){
            fontupdate();
        })

//        init_node_template_edit();
        //把image加入到左侧 可预览的列表中
        var html='';
        <?php foreach ($all_image as $value) {?>
        html=html+'<a  href="javascript:void(0);"   class="btn btn-default btn-xs btn-block" category="main" url="/'+'<?=$value['url']?>'+'">'+'<?=MyFunc::DisposeJSON($value['name'])?>'+'</a>';
        <?php } ?>
        $("#sample1").append(html);


        //标签绑定点击事件 生成预览图
        $("a").click(function(){
            preview_test(this);
        });

//      点击按钮预览节点
        function preview_test(obj){
            var url=obj.getAttribute("url");
            var config=obj.getAttribute("config");
            config=eval("["+config+"]")[0] ;
            var category=obj.getAttribute("category");
//            console.log(url);
            var value={category: 'error'};
            switch (category) {
                case 'main'         :var value = { category: 'main',size: '75 75',img: url};                                    break;
                case 'state'        :var value = { category: 'state', size : '75 75',img:url,text:config};                      break;
                case 'value'        :var value = {category: 'value',img:url};                                                   break;
                case 'edit'         :var value = {category: 'edit'};                                                            break;
                case 'onoffvalue'   :var value = {category: 'onoffvalue',img:url};                                              break;
                case 'alarmvalue'   :var value = {category: 'alarmvalue',img:url};                                              break;
                case 'button'       :var value = {category: 'button',img:'/uploads/pic/按钮1.jpg',link:'',text:'按钮1'};         break;
                case 'test'         :var value = {category: 'test',img:url,link:'',text:'按钮'};                                 break;
                case 'gif'          :var value = {category: 'gif',size: '75 75',img:'/uploads/pic/图1.jpg'};                     break;
                case 'group'        :var value=  {text: "Lane1", isGroup: true, color: "lightblue"};                            break;
                case 'figure'       :var value=  {category:"figure",figure:url};                                                break;
                case 'main_edit'    :var value=  {category:"main_edit"};                                                         break;
                case 'only_value'   :var value=  {category:"only_value"};                                                        break;
                case 'table'        :var value = {
                    category: 'table',
                    items:
                        [
                            { name: "示例1", value: 1 },
                            { name: "示例2", value: 2 }
                        ]
                };break;
            }
            $("#image_preview").remove();
            $("#image_preview_parent").append('<div id="image_preview" style="border: solid 1px black;width: 150; height: 100px"></div>');
            preview(value);
        }
        function preview(value){
            var $ = go.GraphObject.make;
            $(go.Palette,"image_preview",  // must name or refer to the DIV HTML element
                {
                    maxSelectionCount: 1,
                    nodeTemplateMap: myDiagram2.nodeTemplateMap,  // share the templates used by myDiagram
                    model: new go.GraphLinksModel([  // specify the contents of the Palette
                        value
                    ])
                });
        }
        $( "#accordion" ).accordion();
        //属性窗口可拖动
        $('#AttributeMenu').draggable();
        $("#remove_attribute").click(function () {
            var display=document.getElementById("AttributeMenu").style.display
            if(display=='none') document.getElementById("AttributeMenu").style.display='';
            else document.getElementById("AttributeMenu").style.display='none';
        })
        //初始化下拉框
        $("#select_point option[value='"+'en'+"']").attr("selected","selected");
        $("#select_point").chosen();
        $(".chosen_select option[value='"+'en'+"']").attr("selected","selected");
        $(".chosen_select").chosen();
        document.getElementById("AttributeMenu").style.display='none';
        //提供 图id与 设施key  ajax得到设施信息
        function ajaxGetAttribute(sub_system_id,key){
            var id=$("#image_group_id").val();
            //ajax获取信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-get-element",
                data: {sub_system_id:sub_system_id, element_id:key},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
//                    msg=eval('['+msg+']');
//                    var data=msg[0];
//                    var binding_id=data['binding_id'];
//                    binding_id=eval(binding_id);
//                    console.log(typeof (binding_id));
                    //更新 绑定点位信息
                    update(msg);
                }
            });
        }
        $("#update").click(function(){
            updatevalue();
        })
        setInterval(updatevalue,30000);
//        setInterval(gif,500);
        //动态图测试
        function gif(){
            for(var key in sub_system_data){
                //tabs_id 对应div的value值为
                var tabs_id = "tabs-myDiagram-" + key;
                var sub_system_id=$("#"+tabs_id).attr("value");
                //加入指定的node
                var diagram=sub_system_data[key];
                var temp = diagram.model.toJson();
                var data = eval('(' + temp + ')');
                //得到节点对象
                var nodeDataArray = data['nodeDataArray'];
                var flag=false;
                //把查询返回的值重新写入节点数组中
                for(var nodeDatas in nodeDataArray){
                    var nodecategory=nodeDataArray[nodeDatas]['category'];
                    //找到value节点实时更新点位的值
                    if(nodecategory=='gif'){

                        var nodekey=nodeDataArray[nodeDatas]['key'];
                        //console.log(key_value);
                        //如果 value值改变了 则 更新 值 并使flag=true
                        if(nodeDataArray[nodeDatas]['img'] =='/uploads/pic/图1.jpg')
                        {
                            nodeDataArray[nodeDatas]['img']='/uploads/pic/双风机.jpg';
                        }
                        else if(nodeDataArray[nodeDatas]['img'] =='/uploads/pic/双风机.jpg' )
                        {
                            nodeDataArray[nodeDatas]['img']='/uploads/pic/图1.jpg';
                        }

                    }
                }
                diagram.model.startTransaction("flash");
                diagram.model.nodeDataArray=nodeDataArray;
                diagram.model.commitTransaction("flash");
            }
        }

        //字体以及颜色更新
        function fontupdate(){
            var diagram=diagram_now;
            //得到 字体字段
            var font_css=$("#font_css").val();
            var font_color=$("#font_color").val();
            var temp = diagram.model.toJson();
            var data = eval('(' + temp + ')');
            //得到节点对象
            var nodeDataArray = data['nodeDataArray'];
            //得到此节点key
            var key=$("#element_id").val();
            for(var nodeDatas in nodeDataArray){
                var nodekey=nodeDataArray[nodeDatas]['key'];
                //找到value节点实时更新点位的值
                if(nodekey==key){
                    nodeDataArray[nodeDatas]['font']=font_css;
                    nodeDataArray[nodeDatas]['color']=font_color;
                }
            }
            diagram.model.startTransaction("flash");
            diagram.model.nodeDataArray=nodeDataArray;
            diagram.model.commitTransaction("flash");
        }

        //ajax更新所有value节点的值
        function updatevalue() {
            var location_system=new Array();
            for(var key in sub_system_data){
                //tabs_id 对应div的value值为
                var tabs_id = "tabs-myDiagram-" + key;
                var sub_system_id=$("#"+tabs_id).attr("value");
                //加入指定的node
                var diagram=sub_system_data[key];
                var temp = diagram.model.toJson();
                var data = eval('(' + temp + ')');
                //得到节点对象
                var nodeDataArray = data['nodeDataArray'];
//                console.log(nodeDataArray);
                //找到key所表示的节点更新text为value
                var element_ids=new Array();

                for(var nodeDatas in nodeDataArray){
//                    console.log(nodeDatas);
                    var nodecategory=nodeDataArray[nodeDatas]['category'];
                    //找到value节点实时更新点位的值
//                    console.log(nodecategory=='value');
                    if( nodecategory=='value' ||
                        nodecategory=='onoffvalue' ||
                        nodecategory=='alarmvalue' ||
                        nodecategory=='table'      ||
                        nodecategory=='main_edit'  ||
                        nodecategory=='only_value' ||
                        nodecategory=='state'
                    )
                    {
                        //得到value onoffvalue alarmvalue 节点的key
                        var nodekey=nodeDataArray[nodeDatas]['key'];
//                        console.log(nodeDatas);
                        element_ids.push(nodekey);
                    }
                }
                location_system[sub_system_id]=sub_system_id;
                location_system[sub_system_id]=element_ids;
            }

            console.log(location_system);

            //ajax 查询点位信息后更新
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-update-value",
                data: { data:location_system},
                success: function (msg) {

                    //返回value绑定点位的值
//                    console.log(msg);
                    msg = eval('(' + msg + ')');

//                    console.log(msg);
                    for (var key in sub_system_data) {
                        var tabs_id = "tabs-myDiagram-" + key;
                        var sub_system_id = $("#" + tabs_id).attr("value");
                        //加入指定的node
                        var diagram = sub_system_data[key];
                        var temp = diagram.model.toJson();
                        var data = eval('(' + temp + ')');
                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        var key_value = msg[sub_system_id];
//                        console.log(key_value);
                        if (key_value!=undefined) {
                            //标记此图的value是否改变
                            var flag=false;
                            //把查询返回的值重新写入节点数组中
                            for(var nodeDatas in nodeDataArray){
                                var nodecategory=nodeDataArray[nodeDatas]['category'];
                                var nodekey=nodeDataArray[nodeDatas]['key'];
                                var node_value=key_value[nodekey];
//                                if(node_value!=undefined)
//                                    node_value=node_value['value'];

                                switch (nodecategory){
                                    case 'value':
                                        flag=true;
                                        switch (node_value['value']){
                                            case undefined:
                                                nodeDataArray[nodeDatas]['text'] = '?????';
                                                break;
                                            default :
                                                nodeDataArray[nodeDatas]['text'] = node_value['value'];
//                                                console.log(node_value['value']);
                                                break;
                                        }
                                        break;
                                    case 'onoffvalue':
                                        flag=true;
                                        switch (node_value['value']){
                                            case undefined:
                                                nodeDataArray[nodeDatas]['text'] = '?????';
                                                break;
                                            case '0':
                                                nodeDataArray[nodeDatas]['text'] = '开';
                                                break;
                                            case '1':
                                                nodeDataArray[nodeDatas]['text'] = '关';
                                                break;
                                            default :
                                                nodeDataArray[nodeDatas]['text'] = node_value['value'];
                                                break;
                                        }
                                        break;
                                    case 'alarmvalue':
                                        flag=true;
                                        switch (node_value['value']){
                                            case undefined:
                                                nodeDataArray[nodeDatas]['text'] = '?????';
                                                break;
                                            case '0':
                                                nodeDataArray[nodeDatas]['text'] = '正常';
                                                break;
                                            case '1':
                                                nodeDataArray[nodeDatas]['text'] = '警报';
                                                break;
                                            default :
                                                nodeDataArray[nodeDatas]['text'] = node_value['value'];
                                                break;
                                        }
                                        break;
                                    case 'state':
                                        flag=true;
                                        switch (node_value['value']){
                                            case undefined:
                                                nodeDataArray[nodeDatas]['img'] =nodeDataArray[nodeDatas]['text'][3];
                                                break;
                                            case '0':
                                                nodeDataArray[nodeDatas]['img'] =nodeDataArray[nodeDatas]['text'][2];
                                                break;
                                            case '1':
                                                nodeDataArray[nodeDatas]['img'] =nodeDataArray[nodeDatas]['text'][1];
                                                break;
                                            default :
                                                nodeDataArray[nodeDatas]['img'] =nodeDataArray[nodeDatas]['text'][2];
                                                break;
                                        }
                                        break;
                                    case 'only_value':
                                        flag=true;
                                        switch (node_value['value']){
                                            case undefined:
                                                nodeDataArray[nodeDatas]['text'] = '?????';
                                                break;
                                            default :
                                                nodeDataArray[nodeDatas]['text'] = node_value['value'];
//                                                console.log(node_value['value']);
                                                break;
                                        }
                                        break;
                                    case 'main_edit':
                                        flag=true;
//                                        switch (node_value){
//                                            case undefined:
//                                                nodeDataArray[nodeDatas]['text'] = '?????';
//                                                break;
//                                            case '0':
//                                                nodeDataArray[nodeDatas]['text'] = '正常';
//                                                break;
//                                            case '1':
//                                                nodeDataArray[nodeDatas]['text'] = '警报';
//                                                break;
//                                            default :
//                                                nodeDataArray[nodeDatas]['text'] = node_value['value'];
//                                                break;
//                                        }
                                        if(nodeDataArray[nodeDatas]['name']==undefined ||
                                            nodeDataArray[nodeDatas]['name']=='0'
                                        )
                                        {
                                            nodeDataArray[nodeDatas]['name'] = key_value[nodekey]['name'];
                                        }
                                        else {
                                            console.log(nodeDataArray[nodeDatas]['name']);
                                        }
                                        nodeDataArray[nodeDatas]['value'] = key_value[nodekey]['value'];
                                        break;
                                    case 'table':
                                        flag=true;
                                        nodeDataArray[nodeDatas]['items'] = key_value[nodekey];
                                        break;
                                }
                            }
                            //更新 nodedataarray 更新diagram
                            if(flag){
//                                console.log(nodeDataArray);
                                diagram.model.startTransaction("flash");
                                diagram.model.nodeDataArray=nodeDataArray;
                                diagram.model.commitTransaction("flash");
                            }
                        }
                    }
                }
            });
        }

        //更新属性框内的点位值
        function update(value){
            //chosen 先设置值在执行更新函数
            $("#select_point").val(value);
            $("#select_point").trigger("chosen:updated");
        }

        //点击保存绑定信息
        $('#_submit').click(function(){
            var id=$("#image_group_id").val();
            var sub_system_id=$("#sub_system_id").val();
            var element_id=$("#element_id").val();
            var point_ids=$("#select_point").val();
            //ajax保存信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save-element",
                data: {sub_system_id:sub_system_id, element_id:element_id,binding_id:point_ids},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
                    msg=eval('['+msg+']');
//                        console.log(msg);
                }
            });

        })

        //跟着窗口滚动
        function scroll(){
            $(window).scroll(function(){
                var oParent = document.getElementById('AttributeMenu');
                var x =oParent.offsetLeft;
                var y = oParent.offsetTop;
                var yy = $(this).scrollTop();//获得滚动条top值
                if ($(this).scrollTop() < 30) {
                    $("#AttributeMenu").css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位

                }else{
                    $("#AttributeMenu").css({"position":"absolute",top:yy+"px",left:x+"px"});
                }
            });
        }

        //s为所选节点 id为此节点所在的 myDiagram所在数组sub_system_data中的键值
        function showMessage(s,id) {

            var tabs_id = "tabs-myDiagram-" + id;
//            console.log(s.key +"   "+tabs_id);
            //节点信息
            document.getElementById("diagramEventsMsg").textContent = s;
            //所属的子系统id
            var sub_system_id = $("#" + tabs_id).attr("value");

            document.getElementById("sub_system_id").value = sub_system_id;
            document.getElementById("image_group_id").value = sub_system_id;
            document.getElementById("element_id").value = s.key;

            ajaxGetAttribute(sub_system_id, s.key);
            update('zh');
            //document.getElementById("AttributeMenu").style.display='';

//            //可拖动
//            drag();
        }

        scroll();
        var image_url='<?=$image_base?>';
        var location_id=<?=$location_id?>;
        //所有的分类
        var all_sub_system=<?=json_encode($all_sub_system)?>;
        var tab_num=1;
        //console.log(all_sub_system);
        init_blank('myDiagramX');

        $('#tabs').tabs();
        $('#tabs2').tabs();
        var image_in=new Array();
        var data_name=new Array();


        //数组remove指定键值元素 数据
        function remove(a,id){
            var result=new Array();
            for(var key in a){
                if(key!=id)
                    result[key]=a[key];
            }
            return result;
        }
        // Dynamic tabs
        var tabTitle = $("#tab_title");
        var tabContent = $("#tab_content");
        var tabTemplate =   "<li style='position:relative;'> "+
            "<span class='air air-top-left delete-tab' style='top:7px; left:7px;'>"+
            "<button class='btn btn-xs font-xs btn-default hover-transparent'>"+
            "<i class='fa fa-times'>"+
            "</i>" +
            "</button>" +
            "</span>" +
            "</span>" +
            "<a onclick='resize();' href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}" +
            "</a>" +
            "</li>";

        var id = "sub_system";
        var tabs = $("#tabs2").tabs();

        //弹出框内三个值
        var sub_system_name=$("#sub_system_name");
        var image_base=$("#image_base");
        var sub_system_category=$("#sub_system_category");




        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-danger",
                click : function() {
                    //验证弹出框内数据完整性
//                    var sub_system_name=$("#sub_system_name").val();
//                    var image_base=$("#image_base").find("option:checked").text();
//                    var sub_system_category=$("#sub_system_category").find("option:checked").text();
//                      alert(sub_system_category.find("option:checked").val())
                    if(sub_system_name.val()==''){
                        alert('请输入子系统名称');
                    }
                    else {
                        var name = sub_system_category.find("option:checked").text()+'_'+sub_system_name.val();
                        var id = sub_system_category.find("option:checked").val();
                        var with_image=image_base.find("option:checked").val();
                        //如果分类还有则 继续所属分类的标签
                        if(name!='_'+sub_system_name.val()) {
                            image_in[tab_num]=with_image;

                            //添加标签页
                            addTab(name,tab_num,0);
                            //初始化  myDiagram  有底图和无底图
                            if(with_image==0) init2('myDiagram'+tab_num,tab_num);
                            else init1('myDiagram'+tab_num,tab_num);

                            tab_num++;
                            //去掉添加了的子系统类型    [又不需要去掉了]
//                               $("#sub_system_category option[value=" + id + "]").remove();
//                               //改变选择值为第一个
//                               $("#sub_system_category option:first").prop("selected", 'selected');
//                               var text = $("#sub_system_category option:first").text();
//                               $("#select2-chosen-2").html(text);
                        }
                        //如果分类没有了 则提示
                        else alert('分类用完了');
                    }
                    $(this).dialog("close");
                }
            }]
        });

        $("#add_link").click(function(){
            linkdialog.dialog("open");
        })
        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var linkdialog = $("#link_update").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            },
                {

                    html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                    "class" : "btn btn-danger",
                    click : function() {
                        link=$("#node_link").val();
                        link_name=$("#node_name").val();

                        var diagram=button;
                        var temp = diagram.model.toJson();

                        var data = eval('(' + temp + ')');

                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        for(var nodeDatas in nodeDataArray){
                            var nodekey=nodeDataArray[nodeDatas]['key'];
                            if(nodekey==button_key){
                                nodeDataArray[nodeDatas]['link']=link;
                                nodeDataArray[nodeDatas]['text']=link_name;
                            }
                        }
                        diagram.model = go.Model.fromJson(data);
                        $(this).dialog("close");
                        $("#node_link").val('');
                        $("#node_name").val('');
                    }
                }]
        });



        var font_color_dialog = $("#update_font_color").dialog({
            autoOpen : false,
            width : 400,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
                    //获取字体信息 组成font样式
                    var font_family=$("#font_family").val();
                    var font_variant=$("#font_variant").val();
                    var font_weight=$("#font_weight").val();
                    var font_size=$("#font_size").val();
                    var font_color=$("#font_color").val();
                    var font=font_variant+' '+font_weight+' '+font_size+' '+font_family;
                    $("#font_css").val(font);
                    $(this).dialog("close");
                }
            }
            ]
        });



        var point_search_dialog = $("#point_search").dialog({
            autoOpen : false,
            width : 800,
            resizable : false,
            modal : true,
//            buttons : [{
////                html : "<i class='fa fa-times'></i>&nbsp; 取消",
//                "class" : "btn btn-default",
//                click : function() {
//                    $(this).dialog("close");
//
//                }
//            }, {
//
////                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
//                "class" : "btn btn-danger",
//                click : function() {
//
//                    $(this).dialog("close");
//                }
//            }
//            ]
        });
//        point_search_dialog.dialog("open");
        // actual addTab function: adds new tab using the input from the form above
        function addTab(name,id,sub_system_id) {
//            console.log(id);
            data_name[id]=name;
            var tabs_id = "tabs-myDiagram-" + id;
            var label = name || tabContent;
            var li = $(tabTemplate.replace(/#\{href\}/g, "#" + tabs_id).replace(/#\{label\}/g, label));
            var tabContentHtml='<div id="myDiagram'+id+'"style="border: solid 1px black; width:100%; height:650px;background:rgb(29,27,29)" "></div>';
            tabs.find(".ui-tabs-nav").append(li);
            tabs.append("<div id='" + tabs_id + "' value='"+sub_system_id+"'> " + tabContentHtml + " </div>");
            tabs.tabs("refresh");
            // clear fields
            $("#sub_system_name").val("");

        }



        // 增加子系统 并删除已经增加的子系统
        $("#add_subsystem").button().click(function() {
            dialog.dialog("open");
        });
        //删除标签
        // close icon: removing the tab on click
        $("#tabs2").on("click", 'span.delete-tab', function() {
            if (!confirm("您确定要删除吗？"))
            { return false;}
            else
            {
                //移除标签li   以及div
                var panelId = $(this).closest("li").remove().attr("aria-controls");


                //得到要删除的标签id  并截取对应的类型id位
                var tab_id=$("#" + panelId).attr("id");
                console.log(tab_id)
                var id=$("#" + panelId).attr("value");
                var category_id=tab_id.substring(15,tab_id.length);
                $("#" + panelId).remove();

                //ajax  delete删除对应的子系统
                $.ajax({
                    type: "POST",
                    url: "/category/crud/ajax-delete",
                    data: {id: id},
                    //
                    success: function (msg) {
                    }
                });
                //在数组内移除对应的子系统信息
                data_name=remove(data_name,category_id);
                image_in=remove(image_in,category_id);
                sub_system_data=remove(sub_system_data,category_id);

                tabs.tabs("refresh");
                //删除标签 并把分类再加入分类框内
                $("#sub_system_category").append("<option value='"+category_id+"'>"+all_sub_system[category_id]+"</option>");
            }
        });

        $("#save").click(function(){
//            console.log(data_name);
//            console.log(image_in);
//            console.log(sub_system_data);
            document.getElementById("mySavedModel").value = null;
            //var data1=myDiagram1.model.toJson();
            var data=new Array();

            for( var key in sub_system_data){
                console.log('key= '+key);
                var id_tmep=document.getElementById("tabs-myDiagram-"+key).getAttribute('value');
                //key=key.toString();
//                console.log("tabs-myDiagram-"+key);
//                console.log(id_tmep);
                var data_temp={
                    'name':data_name[key],
                    'category_id':key,
                    'id':id_tmep,
                    'data':{'image_base':image_in[key],'data':sub_system_data[key].model.toJson()},
                };
                document.getElementById("mySavedModel").value = document.getElementById("mySavedModel").value +' '+data_name[key]+' '+key+sub_system_data[key].model.toJson();
                data.push(data_temp);

            }
            //console.log(data);
            var data=JSON.stringify(data);
            //console.log(data);
//            ajax save
//            sub_system Info  [name data category_id location_id]
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save",
                data: {data: data,location_id:location_id},
                //暂时未加判定是否成功插入
                success: function (msg) {
                    if(msg==1)
                        console.log('保存成功');
                    else
                        alert('保存失败,请重试');
                }
            });

        })
        function doMouseOver(e) {
//            console.log(e);

            if (e === undefined) e = myDiagram.lastInput;
            var doc = e.documentPoint;

//             find all Nodes that are within 100 units
            var list = myDiagram.findObjectsNear(doc, 1, null, function(x) { return x instanceof go.Node; });
            // now find the one that is closest to e.documentPoint
            var closest = null;
            var closestDist = 999999999;

            list.each(function(node) {
                var dist = doc.distanceSquaredPoint(node.getDocumentPoint(go.Spot.Center));
                if (dist < closestDist) {
                    closestDist = dist;
                    closest = node;
                }
            });


            highlightNode(e, closest);
        }

        // Make sure the infoBox is momentarily hidden if the user tries to mouse over it
        var infoBoxH = document.getElementById("infoBoxHolder");
        infoBoxH.addEventListener("mousemove", function() {
            var box = document.getElementById("infoBoxHolder");
            box.style.left = parseInt(box.style.left) + "px";
            box.style.top = parseInt(box.style.top)+10 + "px";

        }, false);


        // Called with a Node (or null) that the mouse is over or near
        function highlightNode(e, node) {
            //到节点 并且 节点不为table
            if (node !== null && node.data['category']!='table' && node.data['category']!='figure') {
//                var shape = node.findObject("SHAPE");
//                shape.stroke = "white";
//                if (lastStroked !== null && lastStroked !== shape) lastStroked.stroke = null;
//                lastStroked = shape;


                //显示   InfoBox提示框 编辑页面先屏蔽
//                updateInfoBox(e.viewPoint, node.data);

            } else {
//                if (lastStroked !== null) lastStroked.stroke = null;
//                lastStroked = null;
                document.getElementById("infoBoxHolder").innerHTML = "";
            }
        }

        // This function is called to update the tooltip information
        // depending on the bound data of the Node that is closest to the pointer.
        function updateInfoBox(mousePt, data) {
            var x =
                "<div id='infoBox'>" +
                "<div>Info</div>" +
                "<div class='infoTitle'>属性名称</div>" +
                "<div class='infoValues'>值</div>"
            for(var key in data){
                var value='无';
                x=x+  "<div class='infoTitle'>"+key+"</div>";
                if(data[key]!='') {
                    value=data[key];
                }
                x=x+   "<div class='infoValues'>" +value + "</div> ";
            }
            x=x+"</div>"
            var box = document.getElementById("infoBoxHolder");

            box.innerHTML = x;
            box.style.left = mousePt.x+120 + "px";
            box.style.top = mousePt.y+20 + "px";
        }

        function relayoutDiagram() {
            myDiagram2.selection.each(function(n) { n.invalidateLayout(); });
            myDiagram2.layoutDiagram();
        }

        function init_blank(element) {
            if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates

            var cellSize = new go.Size(10, 10);
            myDiagram2 =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {
                                gridCellSize: cellSize
                            }
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {


                        // allow nodes to be dragged to the diagram's background,
                        // to be removed from any group that they were in
                        mouseDrop: function(e) {
                            var ok = e.diagram.commandHandler.addTopLevelParts(e.diagram.selection, true);
                            if (!ok) e.diagram.currentTool.doCancel();
                        },
                        // a clipboard copied node is pasted into the original node's group (i.e. lane).
                        "commandHandler.copiesGroupKey": true,
                        // automatically re-layout the swim lanes after dragging the selection
                        "SelectionMoved": relayoutDiagram,  // this DiagramEvent listener is
                        "SelectionCopied": relayoutDiagram, // defined above
                        "animationManager.isEnabled": false,
                        // enable undo & redo
                        "undoManager.isEnabled": true,


                        mouseOver: doMouseOver,
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        "resizingTool.isGridSnapEnabled": true,
                        "undoManager.isEnabled": true,
                        allowDrop: true,// must be true to accept drops from the Palette
                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.Center,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );
            //点击事件
            myDiagram2.addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if(part.data.category=='button') {
                            button = myDiagram2;
                            button_key = part.data.key;
                            document.getElementById("node_link").value = part.data.link;
                            document.getElementById("node_name").value = part.data.text;
//                            $("#node_link").val(part.data.link);
//                            $("#node_name").val(part.data.text);
                            console.log('button_key');
                            console.log(part);
                            console.log(button_key);
                            linkdialog.dialog("open");
                        }
                    }

                    //showMessage(part.data,num);
                });


            var node_template=init_node_template_edit();
            init_context_menu("contextMenu",myDiagram2);
            //
            myDiagram2.nodeTemplateMap.add("main",node_template.main);
            myDiagram2.nodeTemplateMap.add("edit",node_template.edit);
            myDiagram2.nodeTemplateMap.add("value",node_template.value);
            myDiagram2.nodeTemplateMap.add("onoffvalue",node_template.onoff);
            myDiagram2.nodeTemplateMap.add("alarmvalue",node_template.alarm);
            myDiagram2.nodeTemplateMap.add("button", node_template.button);
            myDiagram2.nodeTemplateMap.add("test", node_template.test);
            myDiagram2.nodeTemplateMap.add("test", node_template.gif);
            myDiagram2.nodeTemplateMap.add("table", node_template.table);
            myDiagram2.nodeTemplateMap.add("figure", node_template.figure);
            myDiagram2.nodeTemplateMap.add("main_edit", node_template.main_edit);
            myDiagram2.nodeTemplateMap.add("only_value", node_template.only_value);
            myDiagram2.nodeTemplateMap.add("state", node_template.state);
            myDiagram2.groupTemplate=node_template.group;



            myDiagram2.model = new go.GraphLinksModel(
                [ // node data
                    { key: "Lane1", text: "Lane1", isGroup: true, color: "lightblue" },
                    { key: "Lane2", text: "Lane1", isGroup: true, color: "lightblue", group: "Lane1"},
                    { key: "11", category:"edit",text: "text for oneA", group: "Lane1" },
                ]);  // no link data
        }


        //初始化 有底图的
        function init1(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {
                        mouseOver: doMouseOver,
                        allowVerticalScroll:true,
                        allowDrop: true,// must be true to accept drops from the Palette
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );


            sub_system_data[num] = myDiagram;

            //单击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function (e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if (part.data.category == 'button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
                        }
                    }
                });
            //右键点击时间
            sub_system_data[num].addDiagramListener("ObjectContextClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if (part.data.category == 'table'||
                            part.data.category == 'edit' ||
                            part.data.category == 'value'
                        )
                        {
                            showMessage(part.data, num);
                        }
                    }

                });

            // the background image, a floor plan
            sub_system_data[num].add(
                $(go.Part,  // this Part is not bound to any model data
                    {
//                        layerName: "Background", position: new go.Point(0, 0),
                        selectable: false, pickable: false
                    },
                    $(go.Picture, "/" + image_url)
                ));

            var node_template = init_node_template_edit();
            init_context_menu("contextMenu",sub_system_data[num]);
//���nodeģ��
            sub_system_data[num].nodeTemplateMap.add("main", node_template.main);
            sub_system_data[num].nodeTemplateMap.add("edit", node_template.edit);
            sub_system_data[num].nodeTemplateMap.add("value", node_template.value);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue", node_template.onoff);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue", node_template.alarm);
            sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.gif);
            sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
            sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
            sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
            sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
            sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
            sub_system_data[num].groupTemplate=node_template.group;
        }
        //初始化 无底图的
        function init2(element,num) {
            //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            var cellSize = new go.Size(10, 10);
            myDiagram =
                $(go.Diagram, element,  // must name or refer to the DIV HTML element
                    {
                        grid: $(go.Panel, "Grid",
                            {gridCellSize: cellSize}
//                            ,
//                            $(go.Shape, "LineH", {stroke: "lightgray"}),
//                            $(go.Shape, "LineV", {stroke: "lightgray"})
                        ),
                    }
                    ,
                    {
                        mouseOver: doMouseOver,
                        allowVerticalScroll:true,
                        allowDrop: true,// must be true to accept drops from the Palette
                        "draggingTool.isGridSnapEnabled": true,
                        "draggingTool.gridSnapCellSpot": go.Spot.Center,
                        contentAlignment:go.Spot.Center,
//                        initialContentAlignment: go.Spot.TopLeft,
//                        initialContentAlignment: go.Spot.Center,
//                        initialDocumentSpot: go.Spot.TopCenter,
//                        initialViewportSpot: go.Spot.TopCenter,
                        //isReadOnly: true,  // allow selection but not moving or copying or deleting
                        "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                    }
                );

            //把myDiagram 加入sub_system_data内
            sub_system_data[num]=myDiagram;
            //单击事件
            sub_system_data[num].addDiagramListener("ObjectSingleClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)) {
                        if(part.data.category=='button') {
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            linkdialog.dialog("open");
                        }
                        else {
                            showMessage(part.data, num);
                        }
                    };
                });
            //右键事件
            sub_system_data[num].addDiagramListener("ObjectContextClicked",
                function(e) {
                    var part = e.subject.part;
                    if (!(part instanceof go.Link)){
                        if (part.data.category == 'table'||
                            part.data.category == 'edit' ||
                            part.data.category == 'value'
                        )
                        {
                            diagram_now=sub_system_data[num];
                            showMessage(part.data, num);
                        }
                    }

                });

            var node_template=init_node_template_edit();
            init_context_menu("contextMenu",sub_system_data[num]);
            //
            sub_system_data[num].nodeTemplateMap.add("main",node_template.main);
            sub_system_data[num].nodeTemplateMap.add("edit",node_template.edit);
            sub_system_data[num].nodeTemplateMap.add("value",node_template.value);
            sub_system_data[num].nodeTemplateMap.add("onoffvalue",node_template.onoff);
            sub_system_data[num].nodeTemplateMap.add("alarmvalue",node_template.alarm);
            sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
            sub_system_data[num].nodeTemplateMap.add("test", node_template.gif);
            sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
            sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
            sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
            sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
            sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
            sub_system_data[num].groupTemplate=node_template.group;
        }


        //根据查询信息加载页面
        <?php  foreach ($sub_system as $key=>$value) { ?>
        var name='<?=MyFunc::DisposeJSON($value['name'])?>';
        var category_id=tab_num;//<?=$value['sub_system_type']?>;
        var data=<?=$value['data']?>;
        var sub_system_id=<?=$value['id']?>;
        //console.log(name);
        //console.log(data['data']);
        //console.log(eval(data));
        //加标签页
        addTab(name,category_id,sub_system_id);
        //初始化标签页  myDiagram
//                        console.log(data);
        if(data['image_base']==0) { image_in[category_id]=0;init2('myDiagram'+category_id,category_id);}
        else  {image_in[category_id]=1;init1('myDiagram'+category_id,category_id);}
        sub_system_data[category_id].model = go.Model.fromJson(data['data']);
        //tab_num=tab_num>=category_id?tab_num:category_id;
        tab_num++;
        $('#tabs2').tabs({active:1});
        <?php  }  ?>





        /*****************************************POINT SEARCH**************************************************************/



        var temp = new Array();

        var $__type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;


        var $current_keys = [];
        var $count = 0;

        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet : 1024,
            phone : 480
        };

        /* TABLETOOLS */
        var datable = $('#datatable_tabletools').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
//            "bLengthChange":true,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });

        var datable2 = $('#selected_datatable_tabletools').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });
        var datable3 = $('#selected_datatable_tabletools1').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools1'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });


        $('#point_select').multiSelect({
            selectableHeader: "<div class='custom-header'>搜索结果</div>",
            selectionHeader: "<div class='custom-header'>已选点位</div>",
            keepOrder: true
        });
        $('#point_select').multiSelect('deselect_all');

        $("#checkAllBtn").click(function() {
            console.log(111);
            $("#point_select").multiSelect("select_all");
        });
        //ajax提交
        $('#all_select').click(function(){
            console.log(111);
            $('#point_select').multiSelect('select_all');
        });




        //        timeTypeChange('year', $('#_time'));
        function regenerateTable(data, id, exeparam)
        {
            //动态添加元素
            // $('#point_select').multiSelect('addOption', data);
            var trstr = '<tbody id="'+id+'">';
            for (var i in data) {
                switch (data[i]['value_type']) {
                    case 0:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }


                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></button></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                    case 1:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 2:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 3:

                        if (data[i]['unit'] == null) {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }

                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'" ><span class="glyphicon glyphicon-ok"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                }

            }

            trstr += '</table>';

            switch (exeparam){
                case 'exeselect':
                    //clear the table tbody content and set the new data
                    datable.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    break;
                case 'exedel':
                    //clear the table tbody content and set the new data
                    datable2.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    $('#selected_datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

                    break;
            }

        }
        function regenerateTable_with_editable_name(data, id, exeparam)
        {
            //动态添加元素
            // $('#point_select').multiSelect('addOption', data);
            var trstr = '<tbody id="'+id+'">';
            for (var i in data) {
                switch (data[i]['value_type']) {
                    case 0:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }


                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                    case 1:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 2:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 3:

                        if (data[i]['unit'] == null) {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }

                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td><input type="text" onchange="update_selected(this.name,this.value)" name="'+data[i]['pointId']+'" value="'+data[i]['pointName']+'"/></td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td></tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                }

            }

            trstr += '</table>';

            switch (exeparam){
                case 'exeselect':
                    //clear the table tbody content and set the new data
                    datable.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    $('#datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

                    break;
                case 'exedel':
                    //clear the table tbody content and set the new data
                    datable2.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    $('#selected_datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});

                    break;
            }

        }
        $('#point_search_submit').click(function() {

            //clear the table tbody content and set the new data
            datable.fnClearTable();


            //recall the method dataTable() to regenerate the table
            $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});



            var energy_id = $('select[name=energy_category_id] option:checked').val();
            var location_id = $('select[name=location_category_id] option:checked').val();
            if (energy_id == '') {
                alert('请选择能源分类');
                return false;
            }

            if (location_id == '') {
                alert('请选择地理位置分类');
                return false;
            }


            var $url = '/point-batch/crud/ajax-find-point-specific?' + $('#_ws').serialize();
            console.log($url);

            $.get($url, function(data) {
                GlobalData = data;

                if(data) {
                    //先从server返回的json里面删除已选择的点位
                    //动态添加表格数据
                    regenerateTable(data, 'tbodyData', 'exeselect');

                }


            }, 'json');
        });



        window.exedel = function(data){
            for (var i in selectedPointContainer) {
                if (selectedPointContainer[i]['pointId'] == data.id) {
                    GlobalData.push(selectedPointContainer[i]);
                    selectedPointContainer.splice(i, 1);
                }
            }
            //判断selectedPointContainer内的点位数据名字是否改变
            //若名字改变则要保留改变的名字信息

            regenerateTable(GlobalData, 'tbodyData', 'exeselect');
            regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');
            regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');

        };


        window.exeselect = function(data){
            console.log(selectedPointContainer);

            if (selectedPointContainer.length != 0) {
                for (var i in selectedPointContainer) {
                    if (data.id == selectedPointContainer[i]['pointId']) {
                        alert('已经存在该点位');
                        return false;
//
                    }
                }
            }

//                //判断是否已经添加点位
//                if (window.IsEmpty())

            for (var i in GlobalData) {
                if (data.id == GlobalData[i]['pointId']) {
                    selectedPointContainer.push(GlobalData[i]);
                    GlobalData.splice(i, 1);
                }
            }
            regenerateTable(GlobalData, 'tbodyData', 'exeselect');
            regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');
            regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');
        };



        //初始化tabs窗口
        $('#tabs').tabs();
        //初始化accordion窗口
        var accordionIcons = {
            header: "fa fa-plus",    // custom icon class
            activeHeader: "fa fa-minus" // custom icon class
        };
        $(".accordion").accordion({
            autoHeight: false,
            heightStyle: "content",
            collapsible: true,
            animate: 300,
            icons: accordionIcons,
            header: "h4",
        })

        //提交时候，对表单进行验证
        $("#_sub").click(function ()   {
            //获取所有 需要选择的参数
            var time_type = $('#time_type').val();
            var time = $('#_time').val();
            var data_value = [];
            var point_ids = [];
            //            console.log($('#point_select option:checked'));
            $('#point_select option:checked').each(function () {
                //                console.log('dede');
                data_value.push($(this).attr('value'));
            });

            $('#selected_datatable_tabletools button').each(function (i, n) {
                // console.log('#tbodySelected');
                var point={
                    id: n.id,
                    name: n.name
                };
//                console.log(i);
//                console.log(n.id);
//                console.log(n);
                point_ids.push(point);

            })
            //隐藏搜索表格
            $("#_loop").hide();
            $("#point_search_div").hide();
            //显示点位展示框
            $("#wid-id-1").show();
            $("#_loop1").show();

            regenerateTable_with_editable_name(selectedPointContainer, 'tbodyData_one', 'exedel');

            // return false;
//            console.log(data_value);
//            // return false;
//            var $error_infor = '';
//            var $error_signal = 0;
//
//            if (point_ids.length == 0) {
//                $error_infor = '请选择点位';
//                $error_signal = 1;
//            }
            $('#point_selected').val(point_ids);
            var points = [];
            for (var i in selectedPointContainer) {
                points.push(selectedPointContainer[i]['pointId']);
            }

//            console.log(points);

            $('#point_selected').val(points);
            //points 即为选择的点位
            //ajax获取信息
//            $.ajax({
//                type: "POST",
//                url: "/category/crud/ajax-get-element",
//                data: {sub_system_id:sub_system_id, element_id:key},
//                success: function (msg) {
//
//                }
//            });



        });
        $("#sub_prev").click(function(){
            //隐藏搜索表格
            $("#_loop").show();
            $("#point_search_div").show();
            //显示点位展示框
            $("#wid-id-1").hide();
            $("#_loop1").hide();
            //把已经更改的名字信息保存在 selectedPointContainer  内
            $('#selected_datatable_tabletools1 input').each(function (i, n) {

                for(var key in selectedPointContainer){
                    console.log(n.mame);
                    if(selectedPointContainer[key]['pointId']== n.name)
                        selectedPointContainer[key]['pointName']= n.value;
                }
            });
        });
        //table 信息绑定
        $("#sub_next").click(function(){

            console.log('提交名称和点位id');
            var binding_points=[];
            for(var key in selectedPointContainer){
                var point={
                    id:  selectedPointContainer[key]['pointId'],
                    name: selectedPointContainer[key]['pointName']
                };
                binding_points.push(point);
            }
            var sub_system_id=$("#sub_system_id").val();
            var element_id=$("#element_id").val();
//            console.log(sub_system_id);
//            console.log(element_id);
            //ajax保存表格配置信息
            $.ajax({
                type: "POST",
                url: "/category/crud/ajax-save-table-element",
                data: {sub_system_id:sub_system_id, element_id:element_id,binding_id:binding_points},
                success: function (msg) {

                }
            });
        });

        $("#time_type").change(function () {
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });
        $("#time_type").trigger('change');
        $('#_time').val($_date_time);

        $('#tabs').tabs();
        $('#tabs2').tabs();





    }
</script>