<?php

namespace backend\module\category;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\category\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
