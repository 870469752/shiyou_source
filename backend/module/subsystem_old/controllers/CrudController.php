<?php

namespace backend\module\subsystem\controllers;

use backend\models\Image;
use backend\models\Location;
use Yii;
use backend\models\InfoGroup;
use backend\models\search\InfoGroupSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use backend\models\ImageGroup;
use common\models\User;
/**
 * CrudController implements the CRUD actions for InfoGroup model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfoGroupSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    //$sub_system Ϊ��ϵͳ�����ɵ�����
    public function getSubSystemPoint($sub_system){
        //�õ���ϵͳ�����а󶨵ĵ�λ �������?
        $points=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];
            $keys=[];
            $sub_system_data=[];
//            echo '<pre>';
//            print_r($data);
//            die;
            foreach ($data as $data_key=>$data_value) {
                //group ������category�ֶ�
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'value_1':
                    case 'state':
                    case 'gif':
                    case 'control':
                    case 'state_value':
                        //�õ� ��Ҫ��ѯ�ڵ��key
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //�����ϵͳid ��key �ҵ���Ҫ��ѯ�ĵ�λ����
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();

            foreach ($configs as $key=>$configs_value) {
                $points_info=json_decode($configs_value['config'],1)['data']['points'];
                //��ȡ��data��Ϣ��  points ����Ϊ��λid  ����Ϊ������Ϣ[id1=>name1,id2=>name2,...]
                if(!is_array($points_info))
                {
                    $points[]=$points_info;
                    $sub_system_data[$system['id']][$configs_value['element_id']]=['id'=>$points_info];
                }
                else {
                    foreach ($points_info as $point_id=>$point_name) {
                        $points[]=$point_id;
                        $sub_system_data[$system['id']][$configs_value['element_id']][]=['id'=>$point_id,'name'=>$point_name];
                    }

                }
            }
        }
        if(!empty($sub_system_data )){
            foreach($sub_system_data as $sub_system_id=>$sub_system){
                foreach($sub_system as $key=>$value){
                    if(empty($value))
                        unset($sub_system_data[$sub_system_id][$key]);
                }
            }
        }
        return ['data'=>$sub_system_data,'points'=>$points];
    }
    public function actionView($id){
        //id=318ʱ  �ѱ�����Ϊ  ��ɫrgb(29,27,29)
        $model =SubSystem::findModel($id);
    //���еı�¥�����ϵ�?
        $sub_system=ImageGroup::find()->where(['location_id'=>$model->location_id])->asArray()->all();
        $location_info=Location::findOne($model->location_id);
        $url=Image::findModel($location_info->image_id);
        $sub_system_data=self::getSubSystemPoint($sub_system);
        $sub_system_data=json_encode($sub_system_data);
        if(empty($url->default)){
            $base_map='uploads/pic/map1.jpg';
        }
        $user_id=Yii::$app->user->id;
        $user=User::findOne($user_id);
        if(strlen($user->is_control)!=0)
            $control=$user->is_control;
        else $control=0;
        //318
        if($id=='000'){
            echo '111';
            die;
            return $this->render('subsystem1',[
                'control'=>$control,
                'user_id'=>$user->$user_id,
                'model'=>$model,
                'base_map'=>$base_map,
                'id'=>$id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
        else {
            return $this->render('subsystem', [
                'control'=>$control,
                'user_id'=>$user_id,
                'model' => $model,
                'base_map' => $base_map,
                'id' => $id,
                'sub_system_data'=>$sub_system_data
            ]);
        }
    }


}
