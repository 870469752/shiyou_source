<?php

namespace backend\module\scheduledtasklog\controllers;

use backend\models\ScheduleTaskLog;
use common\library\MyFunc;
use Yii;
use backend\models\ScheduledTask;
use backend\models\search\ScheduledTaskSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for ScheduledTask model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

//    public function actionUpdateTaskInfo(){
//        $param=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($param);
//        die;
//        $init_state=[
//            "A12" => false, "A16" => false, "A29" => false,
//            "A34" => false, "A42" => false, "A45" => false
//        ];
//        $scheduletask_ids=isset($param['data'])?$param['data']:[];
//        $old_task_info=Yii::$app->cache->get("TASK_INFO");
//        //新的当前任务信息
//        $new_task=ScheduleTaskLog::find()->where(['id'=>$scheduletask_ids])->asArray()->all();
//        $old_tasks_data=[];
//        //更新前记下以前的任务执行程度1/6
//        if(!empty($old_task_info)){
//            $tasks_old=$old_task_info['tasks'];
//            foreach ($tasks_old as $key=>$value) {
//                $old_tasks_data[$value['id']]=$value['action'];
//            }
//        }
//        //给当前任务  加上执行程度信息
//        foreach ($new_task as $key=>$value ) {
//            $tasks[]=$value['id'];
//            $new_task[$key]['action']=isset($tasks_old_data[$value['id']])?$tasks_old_data[$value['id']]:$init_state;
//        }
//        $tasks_info=MyFunc::_map($new_task,'id','name');
//        Yii::$app->redis->set("TASK_INFO",[
//            'tasks'=>$new_task,
//            'tasks_name'=>$tasks_info
//        ]);
//        //返回给页面更新 任务信息
//        return json_encode([
//            'tasks'=>$new_task,
//            'tasks_name'=>$tasks_info
//        ]);
//
//    }
    protected function findModel($id)
    {
        if (($model = ScheduledTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
