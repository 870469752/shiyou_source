<?php

namespace backend\module\scheduledtasklog;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\scheduledtasklog\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
