<?php

namespace backend\module\bacnet_config;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\bacnet_config\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
