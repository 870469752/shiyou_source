<?php

namespace backend\module\bacnet_config\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\BacnetConfig;
use backend\models\search\BacnetConfigSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for BacnetConfig model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BacnetConfig models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BacnetConfigSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single BacnetConfig model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BacnetConfig model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BacnetConfig;
        if ($model->load(Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $op['zh'] = '创建bacnet配置';
            $op['en'] = 'Create bacnet config';
            @$this->getLogOperation($op,'',$result);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BacnetConfig model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $result = $model->save()) {
            /*操作日志*/
            $op['zh'] = '修改bacnet配置';
            $op['en'] = 'Update bacnet config';
            @$this->getLogOperation($op,'',$result);
            return $this->redirect(['index']);
        } else {
            $model->data_query_interval = MyFunc::SecondChangeToTime($model->data_query_interval);
            $model->info_update_interval = MyFunc::SecondChangeToTime($model->info_update_interval);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BacnetConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '删除bacnet配置';
        $op['en'] = 'Delete bacnet config';
        @$this->getLogOperation($op,'','');
        return $this->redirect(['index']);
    }

    /**
     * Finds the BacnetConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BacnetConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BacnetConfig::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
