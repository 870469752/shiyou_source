<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\BacnetConfig $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Bacnet Config',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
