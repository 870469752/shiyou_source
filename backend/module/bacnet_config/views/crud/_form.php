<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\BacnetConfig $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <!--数据读取间隔 -->
                            <lable><?=Yii::t('app', 'Data Query Interval')?> :</lable>
                            <a href="#"  id="query_interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                            <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框-->
                            <?= Html::hiddenInput('BacnetConfig[data_query_interval]', $model->data_query_interval ? $model->data_query_interval : 'Empty', ['id' => 'query_interval_time'])?>

                            <hr />

                        <!-- 设备更新间隔 -->

                            <lable><?=Yii::t('app', 'Info Update Interval')?> :</lable>
                            <a href="#"  id="update_interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                            <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框-->
                            <?= Html::hiddenInput('BacnetConfig[info_update_interval]', $model->info_update_interval ? $model->info_update_interval : 'Empty', ['id' => 'update_interval_time'])?>

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>

<?php
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
?>

<script>
    window.onload = function(){
        var $query_interval = $('#query_interval_time').val();
        var $update_interval = $('#update_interval_time').val();

        timeOfEditable( $('#query_interval') );
        timeOfEditable( $('#update_interval') );
        $('#query_interval').text($query_interval);
        $('#update_interval').text($update_interval);
    }
    </script>