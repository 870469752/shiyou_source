<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\BacnetConfig $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Bacnet Config',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
