<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

TableAsset::register ( $this );
$this->title = Yii::t('app', 'Schedule_Report_List');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" data-target="#SaveMenu" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div>

                    <?= GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'options' => ['class' => 'widget-body no-padding'],
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover',
                            'width' => '100%',
                            'id' => 'datatable'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'name', 'value' => 'report.name', 'format' => 'JSON'],
                            ['attribute' => 'start_timestamp', 'format' => 'TimeFormatter','headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'end_timestamp', 'format' => 'TimeFormatter', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['class' => 'common\library\MyActionColumn', 'template' => '{download-excel} {report-table}', 'header' => Yii::t('app', 'Action'), 'headerOptions' => ['data-class' => 'expand']],
                        ],
                    ]); ?>
                    <?php

                    // 添加两个按钮和初始化表格
                    $js_content = <<<JAVASCRIPT
                    var options = {"button":[]};
                    table_config('datatable', options);
                    $('.hidden-xs').remove();
JAVASCRIPT;
                    $this->registerJs ( $js_content, View::POS_READY);
                    ?>
                </div>
            </div>
        </article>
    </div>
</section>
<script>
    window.onload=function(){
        $('a').click(function(){
            var flag=false;
            var href=$(this).attr("href");
            if(href.indexOf("excel")>-1){
                var k=href.indexOf("download-excel");
                var sub=href.substring(k,href.length);
                var n=sub.indexOf("=");
                var id=sub.substring(n+1,sub.length);
                $.ajax({
                    type: "GET",
                    async : false,
                    url: "/scheduled-report/crud/isdownloadexcel",
                    data: {'id':id},
                    success: function (msg) {
                        if(msg=="0"){
                            flag=true;
                        }else if(msg=="1"){
                            alert("excel文件导出失败！");
                            flag= false;
                        }else if(msg=="2"){
                            alert("该excel文件不允许导出！");
                            flag= false;
                        }
                    }
                });
            }else if(href.indexOf("pdf")>-1){
                var k=href.indexOf("download-pdf");
                var sub=href.substring(k,href.length);
                var n=sub.indexOf("=");
                var id=sub.substring(n+1,sub.length);
                $.ajax({
                    type: "GET",
                    async : false,
                    url: "/scheduled-report/crud/isdownloadpdf",
                    data: {'id':id},
                    success: function (msg) {
                        if(msg=="0"){
                            flag=true;
                        }else if(msg=="1"){
                            alert("pdf文件导出失败！");
                            flag= false;
                        }else if(msg=="2"){
                            alert("该pdf文件不允许导出！");
                            flag= false;
                        }
                    }
                });
            }else{
                flag=true;
            }
            return flag;
        })
    }
</script>