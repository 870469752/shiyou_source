<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CustomReport $model
 */
$this->title = Yii::t('Scheduled_Report', 'Create {modelClass}', [
  'modelClass' => 'Scheduled Report',
]);
?>
    <?= $this->render('_form', [
    'model' => $model,
    'unit' => $unit,
    'active'=>$active,
    'users_array'=>$users_array,
    'now_user_id'=>Yii::$app->user->identity->id,
    'formula' => $formula
    ]) ?>
