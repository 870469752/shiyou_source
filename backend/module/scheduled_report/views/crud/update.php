<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/7/5
 * Time: 14:48
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use common\library\MyHtml;
?>
<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset{
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }
    table td{width: auto}
    table tr{border:dotted ; border-width:1px 0px 0px 0px;}
    table{border:dotted ; border-width:1px 1px 1px 1px;}

</style>
<section id="widget-grid" class="">
    <div class="row">
        <div id="oneset">
            <div class="col-sm-12 col-md-12 col-lg-3" >
                <div class="jarviswidget jarviswidget-color-blueDark">
                    <header>
                        <h2>报表设置 </h2>
                    </header>
                    <div>
                        <div class="widget-body">
                            <form id="add-event-form">
                                <fieldset>
                                    <div class="form-group">
                                        <label>报表名称</label>
                                        <input class="form-control"  id="report_title" name="report_title" maxlength="40" type="text" placeholder="报表名称">
                                        <label id="err1" style="font-size: 13px; color: red;margin-top: 5px;display: none;"></label>
                                        <br>
                                        <label >报表类型</label><br>
                                        <label ><input type="radio" id="radio_day" name="radio_date" value="day" ><i></i>日报表</label>
                                        <label ><input type="radio" id="radio_day" name="radio_date" value="week" ><i></i>周报表</label>
                                        <label ><input type="radio" id="radio_month" name="radio_date" value="month"><i></i>月报表</label>
                                        <label ><input type="radio" id="radio_year" name="radio_date" value="year" checked><i></i>年报表</label>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="well well-sm" id="event-container" >
                            <form class="smart-form">
                                <fieldset>
                                    <legend>
                                        时间段配置
                                    </legend>
                                    <ul id='external-events' class="list-unstyled" >
                                        <li style="display: none">
                                            <span data-category="0"  class="bg-color-red txt-color-white" data-description="" data-icon="fa-time" >自定义</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="1" class="bg-color-orange txt-color-white" data-description="" data-icon="fa-alert" >月配置</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="2" class="bg-color-darken txt-color-white" data-description="" data-icon="fa-alert" >周配置</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="3" class="bg-color-red txt-color-white" data-description="" data-icon="fa-pie" >自定义</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="4" class="bg-color-orange txt-color-white" data-description="" data-icon="fa-alert" >日配置</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="5" class="bg-color-darken txt-color-white" data-description="" data-icon="fa-pie" >周配置</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="6" class="bg-color-red txt-color-white" data-description="" data-icon="fa-alert" >自定义</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="7" class="bg-color-greenLight txt-color-white" data-description="" data-icon="fa-alert" >周配置</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="8" class="bg-color-red txt-color-white" data-description="" data-icon="fa-alert" >自定义</span>
                                        </li>
                                        <li style="display: none">
                                            <span data-category="9" class="bg-color-orange txt-color-white" data-description="" data-icon="fa-alert" >时配置</span>
                                        </li>
                                    </ul>
                                </fieldset>
                                <fieldset  id="time_range" style="display: none">
                                    <label class="label">时间总体范围</label>
                                    <div style="float:left; ">
                                        <label >开始时间</label>
                                        <input type="text" id="time_range_start" class="datetimepicker" style="color:black;">
                                    </div>
                                    <div style="float:left; ">
                                        <label >结束时间</label>
                                        <input type="text" id="time_range_end" class="datetimepicker" style="color:black;">
                                    </div>
                                </fieldset>
                                <fieldset  id="custom_year" style="display: none">
                                    <label class="label">时间范围</label>
                                    <div style="float:left;margin-bottom: 2px;">
                                        <label >时间名称</label>
                                        <input type="text" id="custom_year_time_name"  style="color:black;width:160px">
                                        <label id="err2" style="font-size: 13px; color: red;margin-top: 5px;display: none"></label>
                                    </div>
                                    <div style="float:left; ">
                                        <label >开始时间</label>
                                        <input type="text" name="custom_year_time_start" class="datetimepicker" style="color:black;">
                                    </div>
                                    <div style="float:left; ">
                                        <label >结束时间</label>
                                        <input type="text" name="custom_year_time_end" class="datetimepicker" style="color:black;">
                                    </div>
                                </fieldset>
                                <fieldset  id="simple_year" style="display: none">
                                    <label class="label">月范围</label>
                                    <?php for($i=1;$i<13;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_year_month_checkbox" value="<?=$i?>" ><i></i><?=$i?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset  id="simple_year_week" style="display: none">
                                    <label class="label">周范围</label>
                                    <?php for($i=1;$i<54;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_year_week_checkbox" value="<?=$i?>" ><i></i><?=$i?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset  id="custom_month" style="display: none">
                                    <label class="label">日范围</label>
                                    <div class="form-group">
                                        <label >时间名称</label>
                                        <div>
                                            <input type="text" id="custom_month_time_name"  style="color:black;width:230px;">
                                            <label id="err3" style="font-size: 13px; color: red;margin-top: 5px;display: none"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>开始日期</label>
                                        <input class="form-control"  class="spinner-decimal" id="spinner-decimal1" name="custom_month_time_start" value="1">
                                    </div>
                                    <div class="form-group">
                                        <label>结束日期</label>
                                        <input class="form-control"  class="spinner-decimal" id="spinner-decimal2" name="custom_month_time_end" value="1">
                                    </div>
                                </fieldset>
                                <fieldset  id="simple_month_day" style="display: none">
                                    <label class="label">日范围</label>
                                    <?php for($i=1;$i<32;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_month_day_checkbox" value="<?=$i?>" ><i></i><?=$i?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset  id="simple_month_week" style="display: none">
                                    <label class="label">周范围</label>
                                    <?php for($i=1;$i<6;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_month_week_checkbox" value="<?=$i?>" ><i></i><?=$i?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset  id="custom_week" style="display: none">
                                    <label class="label">周范围</label>
                                    <div style="float:left;margin-bottom: 2px;">
                                        <label >时间名称</label>
                                        <input type="text" id="custom_week_time_name"  style="color:black;width:228px;height:25px;">
                                        <label id="err4" style="font-size: 13px; color: red;margin-top: 5px;display: none"></label>
                                    </div>
                                    <div style="float:left; margin-top: 7px">
                                        <label >开始时间</label>
                                        <?=Html::dropDownList('custom_week_time_start','',  [
                                            '1' => '周一',
                                            '2' => '周二',
                                            '3' => '周三',
                                            '4' => '周四',
                                            '5' => '周五',
                                            '6' => '周六',
                                            '7' => '周日',
                                        ],['id'=>'mycheck1','class' => 'mycheck1', 'placeholder' => '请选择开始时间'])?>
                                    </div>
                                    <div style="float:left; margin-top: 7px ">
                                        <label >结束时间</label>
                                        <?=Html::dropDownList('custom_week_time_start','',  [
                                            '1' => '周一',
                                            '2' => '周二',
                                            '3' => '周三',
                                            '4' => '周四',
                                            '5' => '周五',
                                            '6' => '周六',
                                            '7' => '周日',
                                        ],['id'=>'mycheck2','class' => 'mycheck2', 'placeholder' => '请选择结束时间'])?>
                                    </div>
                                </fieldset>
                                <fieldset  id="simple_week" style="display: none">
                                    <label class="label">周范围</label>
                                    <?php for($i=1;$i<8;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_week_day_checkbox" value="<?=$i?>" ><i></i><?php if($i==7){echo '日';} else if($i==6) {echo '六';} else if($i==5) {echo '五';} else if($i==4) {echo '四';} else if($i==3) {echo '三';} else if($i==2) {echo '二';} else if($i==1) {echo '一';}?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset  id="custom_day" style="display: none">
                                    <label class="label">时范围</label>
                                    <div style="float:left;margin-bottom: 2px;">
                                        <label >时间名称</label>
                                        <input type="text" id="custom_day_time_name"  style="color:black;width:228px;height:25px;">
                                        <label id="err5" style="font-size: 13px; color: red;margin-top: 5px;display: none"></label>
                                    </div>
                                    <div style="float:left; ">
                                        <label >开始时间</label>
                                        <div class="input-group" >
                                            <input class="form-control" name="custom_day_time_start" id="clockpicker1" type="text" placeholder="Select time" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div style="float:left; ">
                                        <label >结束时间</label>
                                        <div class="input-group" >
                                            <input class="form-control" name="custom_day_time_end" id="clockpicker2" type="text" placeholder="Select time" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset  id="simple_day_hour" style="display: none">
                                    <label class="label">时范围</label>
                                    <?php for($i=0;$i<24;$i++){?>
                                        <label class="checkbox state-success"><input type="checkbox" name="simple_day_hour_checkbox" value="<?=$i?>" ><i></i><?=$i?></label>
                                    <?php }?>
                                </fieldset>
                                <fieldset>
                                    <div class="form-group" style="float: right;">
                                        <button  type="button" id="submit_custom_year" class="create btn-success">添加</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9">
                <div class="jarviswidget jarviswidget-color-blueDark" data-widget-collapsed="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                        <h2> 事件设置 </h2>
                        <div class="widget-toolbar">
                            <!-- add: non-hidden - to disable auto hide -->
                            <div class="btn-group">
                                <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                    视图选择 <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu js-status-update pull-right">
                                    <li>
                                        <a href="javascript:void(0);" id="mt">月视图</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" id="ag">周视图</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" id="td">日视图</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="widget-body-toolbar">
                                <div id="calendar-buttons" >
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-today">today</i></a>
                                        <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div id="calendar"></div>
                            <button type="button"  id="next" style="float:right" class="create btn-success" onclick="displayset('oneset','twoset')">下一步</button>
                            <button type="button"  style="float:right;margin-right:5px" class="create btn-success" id="btn_timeshow">时间段预览</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="timeshow" title="时间表" style="display:none;width: auto">
                <div class="col-md-12">
                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                        <div class="panel-body" ng-transclude style="overflow-x: auto; overflow-y: auto; height:300px;" >
                            <table id = "pre_table_timeshow" class="table table-bordered table-condensed ng-scope">
                                <thead style="text-align: center">
                                <th>行号</th><th>事件名称</th><th>时间范围</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </panel>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
        <?= Html::hiddenInput("config")?>
        <?= Html::hiddenInput("fromStart"); ?>
        <?= Html::hiddenInput("toEnd"); ?>
        <div id = "twoset" style="display: none">
            <fieldset>
                <div class="col-md-12">
                    <label class="label"  style="color:black">公式名称</label>
                    <input type="text" id="formula_title" class="form-control" name="formula_title" value="">
                </div>

                <div class="col-md-12" style="margin-top: 10px">
                    <label class="label" style="color:black">公式编辑</label>
                </div>
                <div class="input-group" style="margin-top: 10px" id = '_formula'>
                    <div class = 'select2-container select2-container-multi' style = 'margin-top:2px;width: 100%;'>
                        <ul class="select2-choices">
                            <li class="select2-search-choice" data-status = '0' data-type = 'plus' style = 'background-color:#AFEDF5;border:none;'>
                                <div><i class="fa fa-plus"></i></div>
                                <a style = 'display:none' href="#" class="select2-search-choice-close _delete" tabindex="-1"></a>
                            </li>
                        </ul>
                    </div>

                    <div class="input-group-btn" style="margin-top: 10px" id = '_del'>
                        <button class="btn create btn-primary" type="button">
                            <i class="fa fa-mail-reply-all"></i> Clear
                        </button>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 10px">
                    <label class="label" style="color:black">单位选择</label>
                    <?=Html::dropDownList('test', '', $unit, ['class' => 'select2', 'placeholder' => '无'])?>
                </div>
                <div class="col-md-12" style="margin-top: 10px" >
                    <button class="btn create btn-primary" id = 'add_point' type="button">
                        添加公式
                    </button>
                    <label id="point_prompt" style="font-size: 13px; color: red; margin-left: 10px"></label>
                </div>
            </fieldset>
            <fieldset id="formula_static">
                <div class="col-md-12">
                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                        <div class="panel panel-primary" style="padding: 15px;display: block">
                            <div class="panel-body" ng-transclude>
                                <table id = "formula" class="table table-bordered table-condensed ng-scope">
                                    <thead>
                                    <tr style="font-weight: bold">
                                        <th style = "10%"><input type = "checkbox" id = "formula_set_checked" ></th>
                                        <th style="width:auto">公式名称</th>
                                        <th style="width:auto">计算公式</th>
                                        <th style="width:auto">单位</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <footer style="background-color: lightgoldenrodyellow">
                                    <button id = "_delete2" type="button" class="btn btn-primary">Delete</button>
                                </footer>
                            </div>
                        </div>
                    </panel>
                </div>
            </fieldset>

            <fieldset id="preview" style="display: none">
                <div class="col-md-12">
                    <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                        <div class="panel panel-primary" style="padding: 15px;display: block">
                            <div class="panel-body" ng-transclude>
                                <table id = "pre_table" class="table table-bordered table-condensed ng-scope">
                                    <thead style="text-align: center">

                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <footer style="background-color: lightgoldenrodyellow">
                                    <button id = "close_pre_table" type="button" class="btn btn-primary">close</button>
                                </footer>
                            </div>
                        </div>
                    </panel>
                </div>
                <div id="pre_table"></div>
            </fieldset>
            <?= Html::hiddenInput("detail"); ?>
            <?= Html::hiddenInput("report_name"); ?>
            <div class="form-actions">
                <button type="button" id="pre_view" class="create btn-success">预览</button>&nbsp;&nbsp;
                <button type="button" id="" class="create btn-success" onclick="displayset('twoset','oneset')">上一步</button>&nbsp;&nbsp;
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_ate'  style="display: none;">
        <div class="arrow"></div>
        <h3 class="popover-title">选择点位</h3>
        <div class="popover-content">
            <div class="editableform-loading" style="display: none;"></div>
            <form class="form-inline editableform" style="">
                <div class="control-group form-group"><div>
                        <div class="editable-input">
                            <div class="editable-address">
                                <?=Html::dropDownList('gs', '', [
                                    '' => '',
                                    '_fh' => '运算符',
                                    '_hs' => '函数',
                                    '_sz' => '数字',
                                    '_dw' => '点位',
                                    '_mb' => '模板',
                                ], ['class' => 'select2', 'placeholder' => '请选择公式组成元素', 'id' => '_ele'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_fh' style = 'display:none'>
                                <?=Html::dropDownList('_fh', '', [
                                    '' => '',
                                    '＋' => '＋',
                                    '－' => '－',
                                    '*' => '×',
                                    '/' => '÷',
                                    '(' => '(',
                                    ')' => ')'
                                ], ['class' => 'select2', 'placeholder' => '请选择运算符号'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_hs' style = 'display:none'>
                                <?=Html::dropDownList('_hs', '', [
                                    '' => '',
                                    'sin(' => 'sin',
                                    'cos(' => 'cos',
                                    'max(' => 'max',
                                    'min(' => 'min',
                                    'sum(' => 'sum',
                                    'avg(' => 'avg'
                                ], ['class' => 'select2', 'placeholder' => '请选择函数'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_sz' style = 'display:none'>
                                <input type = 'text' name = '_sz' style = 'width:100%' placeholder = '请填写数字'>
                            </div>
                            <div class="editable-address _calculate" id  = '_dw' style = 'display:none'>
                                <input type = 'text' id = '_pointname' style = 'width:100%' placeholder = '点位名称'>
                            </div>
                            <div class="editable-address _calculate" id  = '_mb' style = 'display:none'>
                                <?=Html::dropDownList('_mb', '', $formula, ['class' => 'select2', 'placeholder' => '请选择模板公式'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_error_log' style = '    display:none;color:#50cd42'></div>
                        </div>
                        <div class="editable-buttons">
                            <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                            <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                        </div>
                    </div>
                    <div class="editable-error-block help-block" style="display: none;"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_point_search'  style="display: none;width:900px">
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
             data-widget-Collapse="false"
             data-widget-custom="false"
             data-widget-togglebutton="false" id = '_jarviswidget' >
            <header>
                <span class="widget-icon"><i class="fa fa-table"></i></span>
                <h2><?=Yii::t('app', 'Point Batch')?></h2>
                <div class="jarviswidget-ctrls"></div>
            </header>
            <!-- 参数主体 start-->
            <div class="widget-body" >
                <!-- Widget ID (each widget will need unique ID)-->
                <div>
                    <!-- widget edit box -->
                    <div class="widget-body no-padding">
                        <div class="well well-sm well-light">
                            <div id="tabs" >
                                <ul>
                                    <li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
                                    <li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                                    <li><a href="#others" id="others_show">其他点位</a></li>
                                </ul>
                                <div id="bacnet">
                                    <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="modbus">
                                    <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div >
                                <div id="others">
                                    <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end widget content -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php

$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/clockpicker/clockpicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
?>
<script>
    var data=[];
    var year_month=[];   //年报表月份重复
    var year_week=[];    //年报表星期重复
    var month_day=[];    //月报表天重复
    var row=1;
    var flag=0;
    var tem={
        "category":'',
        "title":'',
        "start":'',
        "end":'',
        "allDay":true,
        "description":'',
        "className":'',
        "row":1,
        "_id":1,
        "specical":''
    };
    var _div = '<div class = "rows"><div  class="col-md-4 col-sm-6 popover fade top in alert alert-info fade in" id="popover" style="display: none;">' +
        '<div class="arrow"></div>' +
        '<h3 class="">任务编辑</h3>' +
        '任务：<input type="text" style = "width: 150px" id="num">' +
        '<div class="arrow"></div><hr />' +
        '&nbsp;&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "del" class = "btn bg-color-purple txt-color-white" style="ime-mode:disabled " >删除</a>' +
        '&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "update" class = "btn btn-info">修改</a>' +
        '&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "cancel" class = "btn btn-info">取消</a>' +
        '</div></div>';
    window.onload = function()
    {
        $(document.body).append(_div);
        "use strict";
        $(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
        $(".datetimepicker").datetimepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            minView: 'month',
            pickerPosition: "bottom-left",
            autoclose: true,
            timepicker: false
        });
        Init();
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var dialog = $("#timeshow").dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    //  $("#pre_table_timeshow thead").text("");
                    $("#pre_table_timeshow tbody").text("");
                    $(this).dialog("close");

                }
            }, {
                html : "<i class='fa fa-plus'></i>&nbsp; 更新",
                "class" : "btn btn-danger",
                click : function() {
                    if(true){
                    }
                    else {
                    }
                    //     $("#pre_table_timeshow thead").text("");
                    $("#pre_table_timeshow tbody").text("");
                    $(this).dialog("close");
                }
            }]
        });
        $("#spinner-decimal1").spinner({
            min: 1,
            max: 31,
            step: 1,
            numberFormat: "n"
        });
        $("#spinner-decimal2").spinner({
            min: 1,
            max: 31,
            step: 1,
            numberFormat: "n"
        });
        $('#clockpicker1').clockpicker({
            placement: 'top',
            donetext: 'Done',
            'default': 'now'
        });
        $('#clockpicker2').clockpicker({
            placement: 'top',
            donetext: 'Done',
            'default': 'now'
        });
        var hdr = {
            left: 'title',
            center: 'month,basicWeek,agendaDay',
            right: 'prev,today,next'
        };
        var initDrag = function (e) {
            var eventObject = {
                title: $.trim(e.children().text()), // use the element's text as the event title
                description: $.trim(e.children('span').attr('data-description')),
                icon: $.trim(e.children('span').attr('data-icon')),
                className: $.trim(e.children('span').attr('class')) // use the element's children as the event class
            };
            // store the Event Object in the DOM element so we can get to it later
            e.data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            e.draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0 //  original position after the drag
            });
        };
        //添加事件
        var addEvent = function (title, priority, description, icon) {
            title = title.length === 0 ? "Untitled Event" : title;
            description = description.length === 0 ? "No Description" : description;
            icon = icon.length === 0 ? " " : icon;
            priority = priority.length === 0 ? "label label-default" : priority;
            var html = $('<li><span class="' + priority + '" data-description="' + description + '" data-icon="' +
                icon + '">' + title + '</span></li>').prependTo('ul#external-events').hide().fadeIn();
            $("#event-container").effect("highlight", 800);
            initDrag(html);
        };
        $('#external-events > li').each(function () {
            $(this).hide();
            initDrag($(this));
        });
        //左侧时间标签点击事件
        $('#external-events > li').click(function () {
            var category=$(this).children('span').attr('data-category');
            if(category=='0'){
                $("#custom_year").show();
                $("#simple_year_week").hide();
                $("#simple_year").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=0;
                $("#submit_custom_year").text("添加");
            }else if(category=='1'){
                $("#custom_year").hide();
                $("#simple_year").show();
                $("#simple_year_week").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=1;
                $("#submit_custom_year").text("更新");
            }else if(category=='2'){
                $("#custom_year").hide();
                $("#simple_year_week").show();
                $("#simple_year").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=2;
                $("#submit_custom_year").text("更新");
            }else if(category=='3'){
                $("#custom_month").show();
                $("#simple_month_week").hide();
                $("#simple_month_day").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=3;
                $("#submit_custom_year").text("添加");
            }else if(category=='4'){
                $("#custom_month").hide();
                $("#simple_month_week").hide();
                $("#simple_month_day").show();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=4;
                $("#submit_custom_year").text("更新");
            }else if(category=='5'){
                $("#custom_month").hide();
                $("#simple_month_week").show();
                $("#simple_month_day").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=5;
                $("#submit_custom_year").text("更新");
            }else if(category=='6'){
                $("#simple_week").hide();
                $("#custom_week").show();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=6;
                $("#submit_custom_year").text("添加");
            }else if(category=='7'){
                $("#simple_week").show();
                $("#custom_week").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=7;
                $("#submit_custom_year").text("更新");
            }else if(category=='8'){
                $("#custom_day").show();
                $("#simple_day_hour").hide();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=8;
                $("#submit_custom_year").text("添加");
            }else if(category=='9'){
                $("#custom_day").hide();
                $("#simple_day_hour").show();
                tem['title']=$(this).children().text();
                tem['category']=$(this).children('span').attr('data-category');
                tem['description']=$(this).children('span').attr('data-description');
                tem['className']=$(this).children('span').attr('class');
                flag=9;
                $("#submit_custom_year").text("更新");
            }
        });

        //自定义年报表确定按钮
        $("#submit_custom_year").click(function(){
            if(flag==0){
                var custom_year_start=$("input[name='custom_year_time_start']").val();
                var custom_year_end=$("input[name='custom_year_time_end']").val();
                custom_year_start='2007'+custom_year_start.substring(4,10);
                custom_year_end='2007'+custom_year_end.substring(4,10);
                if(custom_year_start>custom_year_end){
                    $("#err2").text("起始时间不能大于结束时间！");
                    $("#err2").show();
                    return false;
                }else{
                    $("#err2").text("");
                    $("#err2").hide();
                }
                var str=$("#custom_year_time_name").val();
                if(str=='' || str==null){
                    $("#err2").text("时间名称不能为空！");
                    $("#err2").show();
                    return false;
                }else{
                    $("#err2").text("");
                    $("#err2").hide();
                }
                tem['start']=custom_year_start;
                tem['end']=custom_year_end;
                tem['row']=row;
                tem['_id']=row;
                tem['title']=str;
                tem['specical']=0;
                row++;
                data.push(tem); console.log(tem);
                $('#calendar').fullCalendar('addEventSource', [{
                    "category":tem['category'],
                    "title":tem['title'],
                    "start":tem['start'],
                    "end":tem['end'],
                    "allDay":true,
                    "description":tem['description'],
                    "className":tem['className'],
                    "row":tem['row'],
                    "_id":tem['_id'],
                    "specical":tem['specical']
                }]);
                var date1=new Date();
                date1.setMonth(0);
                date1.setDate(1);
                var date2=new Date();
                date2.setMonth(11);
                date2.setDate(31);
                $("#time_range_start").val(date1.format("yyyy-MM-dd"));
                $("#time_range_end").val(date2.format("yyyy-MM-dd"));
            }else if(flag==1){
                var months=$("input:checkbox[name='simple_year_month_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='1'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                months.each(function() {
                    if(arrnow.indexOf(parseInt($(this).val()))>-1){
                    }else{
                        var startdate=new Date();
                        startdate.setFullYear(2007);
                        startdate.setMonth(parseInt($(this).val())-1);
                        startdate.setDate(1);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                        var enddate=new Date();
                        enddate.setFullYear(2007);
                        enddate.setMonth(parseInt($(this).val())-1);
                        enddate.setDate(getdays(enddate.getFullYear(),parseInt($(this).val())));enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                        tem['start']=startdate;
                        tem['end']=enddate;
                        tem['row']=row;
                        tem['_id']=row;
                        tem['specical']=parseInt($(this).val());
                        tem['title']=parseInt($(this).val())+"月";
                        data.push(tem); console.log(tem);
                        $('#calendar').fullCalendar('addEventSource', [{
                            "category":tem['category'],
                            "title":tem['title'],
                            "start":tem['start'],
                            "end":tem['end'],
                            "allDay":true,
                            "description":tem['description'],
                            "className":tem['className'],
                            "row":tem['row'],
                            "_id":tem['_id'],
                            "specical":tem['specical']
                        }]);
                        row++;
                    }

                });
                var date1=new Date();
                date1.setMonth(0);
                date1.setDate(1);
                var date2=new Date();
                date2.setMonth(11);
                date2.setDate(31);
                $("#time_range_start").val(date1.format("yyyy-MM-dd"));
                $("#time_range_end").val(date2.format("yyyy-MM-dd"));
            }else if(flag==2){
                var weeks=$("input:checkbox[name='simple_year_week_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='2'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                weeks.each(function(){
                        if(arrnow.indexOf(parseInt($(this).val()))>-1){
                        }else{
                            var startdate=new Date();
                            var year=startdate.getFullYear();
                            var nowbegin=getXDate(2007,parseInt($(this).val()),1);
                            var nowend=getXDate(2007,parseInt($(this).val())+1,0);
                            tem['start']=nowbegin;
                            tem['end']=nowend;
                            tem['row']=row;
                            tem['_id']=row;
                            tem['title']="第"+parseInt($(this).val())+"周";
                            tem['specical']=parseInt($(this).val());
                            data.push(tem); console.log(tem);
                            $('#calendar').fullCalendar('addEventSource', [{
                                "category":tem['category'],
                                "title":tem['title'],
                                "start":tem['start'],
                                "end":tem['end'],
                                "allDay":true,
                                "description":tem['description'],
                                "className":tem['className'],
                                "row":tem['row'],
                                "_id":tem['_id'],
                                "specical":tem['specical']
                            }]);
                            row++;
                        }
                    }
                );
                var date1=new Date();
                date1.setMonth(0);
                date1.setDate(1);
                var date2=new Date();
                date2.setMonth(11);
                date2.setDate(31);
                $("#time_range_start").val(date1.format("yyyy-MM-dd"));
                $("#time_range_end").val(date2.format("yyyy-MM-dd"));
            }else if(flag==3){
                var custom_month_start=$("#spinner-decimal1").val();
                var custom_month_end=$("#spinner-decimal2").val();
                if(custom_month_start.length==1){
                    custom_month_start='0'+custom_month_start;
                }
                if(custom_month_end.length==1){
                    custom_month_end='0'+custom_month_end;
                }
                custom_month_start='2007-01-'+custom_month_start;
                custom_month_end='2007-01-'+custom_month_end;
                if(custom_month_start>custom_month_end){
                    $("#err3").text("起始时间不能大于结束时间！");
                    $("#err3").show();
                    return false;
                }else{
                    $("#err3").text("");
                    $("#err3").hide();
                }
                var str=$("#custom_month_time_name").val();
                if(str=='' || str==null){
                    $("#err3").text("时间名称不能为空！");
                    $("#err3").show();
                    return false;
                }else{
                    $("#err3").text("");
                    $("#err3").hide();
                }
                tem['start']=custom_month_start;
                tem['end']=custom_month_end;
                tem['row']=row;
                tem['_id']=row;
                tem['title']=str;
                tem['specical']=0;
                row++;
                data.push(tem); console.log(tem);
                $('#calendar').fullCalendar('addEventSource', [{
                    "category":tem['category'],
                    "title":tem['title'],
                    "start":tem['start'],
                    "end":tem['end'],
                    "allDay":true,
                    "description":tem['description'],
                    "className":tem['className'],
                    "row":tem['row'],
                    "_id":tem['_id'],
                    "specical":tem['specical']
                }]);
            }else if(flag==4){
                var days=$("input:checkbox[name='simple_month_day_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='4'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                days.each(function() {
                        if(arrnow.indexOf(parseInt($(this).val()))>-1){

                        }else{
                            var startdate=new Date();
                            startdate.setFullYear(2007);
                            startdate.setMonth(0);
                            startdate.setDate(parseInt($(this).val()));startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                            var enddate=new Date();
                            enddate.setFullYear(2007);
                            enddate.setMonth(0);
                            enddate.setDate(parseInt($(this).val()));enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            tem['start']=startdate;
                            tem['end']=enddate;
                            tem['row']=row;
                            tem['_id']=row;
                            tem['title']=parseInt($(this).val())+"日";
                            tem['specical']=parseInt($(this).val());
                            data.push(tem); console.log(tem);
                            $('#calendar').fullCalendar('addEventSource', [{
                                "category":tem['category'],
                                "title":tem['title'],
                                "start":tem['start'],
                                "end":tem['end'],
                                "allDay":true,
                                "description":tem['description'],
                                "className":tem['className'],
                                "row":tem['row'],
                                "_id":tem['_id'],
                                "specical":tem['specical']
                            }]);
                            row++;
                        }
                    }
                );
            }else if(flag==5){
                var days=$("input:checkbox[name='simple_month_week_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='5'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                days.each(function() {
                        if(arrnow.indexOf(parseInt($(this).val()))>-1){

                        }else{
                            if(parseInt($(this).val())==1){
                                var startdate=new Date();
                                startdate.setFullYear(2007);
                                startdate.setMonth(0);
                                startdate.setDate(1);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                                var enddate=new Date();
                                enddate.setFullYear(2007);
                                enddate.setMonth(0);
                                enddate.setDate(7);enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            }else if(parseInt($(this).val())==2){
                                var startdate=new Date();
                                startdate.setFullYear(2007);
                                startdate.setMonth(0);
                                startdate.setDate(8);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                                var enddate=new Date();
                                enddate.setFullYear(2007);
                                enddate.setMonth(0);
                                enddate.setDate(14);enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            }else if(parseInt($(this).val())==3){
                                var startdate=new Date();
                                startdate.setFullYear(2007);
                                startdate.setMonth(0);
                                startdate.setDate(15);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                                var enddate=new Date();
                                enddate.setFullYear(2007);
                                enddate.setMonth(0);
                                enddate.setDate(21);enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            }else if(parseInt($(this).val())==4){
                                var startdate=new Date();
                                startdate.setFullYear(2007);
                                startdate.setMonth(0);
                                startdate.setDate(22);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                                var enddate=new Date();
                                enddate.setFullYear(2007);
                                enddate.setMonth(0);
                                enddate.setDate(28);enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            }else if(parseInt($(this).val())==5){
                                var startdate=new Date();
                                startdate.setFullYear(2007);
                                startdate.setMonth(0);
                                startdate.setDate(29);startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                                var enddate=new Date();
                                enddate.setFullYear(2007);
                                enddate.setMonth(1);
                                enddate.setDate(4);enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                            }
                            tem['start']=startdate;
                            tem['end']=enddate;
                            tem['row']=row;
                            tem['_id']=row;
                            tem['title']="第"+parseInt($(this).val())+"周";
                            tem['specical']=parseInt($(this).val());
                            data.push(tem); console.log(tem);
                            $('#calendar').fullCalendar('addEventSource', [{
                                "category":tem['category'],
                                "title":tem['title'],
                                "start":tem['start'],
                                "end":tem['end'],
                                "allDay":true,
                                "description":tem['description'],
                                "className":tem['className'],
                                "row":tem['row'],
                                "_id":tem['_id'],
                                "specical":tem['specical']
                            }]);
                            row++;
                        }
                    }
                );
            }else if(flag==6){
                var custom_week_start=$("#mycheck1").val();
                var custom_week_end=$("#mycheck2").val();
                var nowdate=new Date();
                if(custom_week_start>custom_week_end){
                    $("#err4").text("起始时间不能大于结束时间！");
                    $("#err4").show();
                    return false;
                }else{
                    $("#err4").text("");
                    $("#err4").hide();
                }
                var str=$("#custom_week_time_name").val();
                if(str=='' || str==null){
                    $("#err4").text("时间名称不能为空！");
                    $("#err4").show();
                    return false;
                }else{
                    $("#err4").text("");
                    $("#err4").hide();
                }
                tem['start']=2007+'-'+1+'-'+custom_week_start+' '+'00:00:00';
                tem['end']=2007+'-'+1+'-'+custom_week_end+' '+'23:59:59';
                tem['row']=row;
                tem['_id']=row;
                tem['title']=str;
                tem['specical']=0;
                row++;
                data.push(tem); console.log(tem);
                $('#calendar').fullCalendar('addEventSource', [{
                    "category":tem['category'],
                    "title":tem['title'],
                    "start":tem['start'],
                    "end":tem['end'],
                    "allDay":false,
                    "description":tem['description'],
                    "className":tem['className'],
                    "row":tem['row'],
                    "_id":tem['_id'],
                    "specical":tem['specical']
                }]);
            }else if(flag==7){
                var days=$("input:checkbox[name='simple_week_day_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='7'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                days.each(function() {
                    if(arrnow.indexOf(parseInt($(this).val()))>-1){
                    }else{
                        var startdate=new Date();
                        startdate.setFullYear(2007);
                        startdate.setMonth(0);
                        startdate.setDate(parseInt($(this).val()));startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
                        var enddate=new Date();
                        enddate.setFullYear(2007);
                        enddate.setMonth(0);
                        enddate.setDate(parseInt($(this).val()));enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
                        tem['start']=startdate;
                        tem['end']=enddate;
                        tem['row']=row;
                        tem['_id']=row;
                        tem['title']="周"+parseInt($(this).val());
                        tem['specical']=parseInt($(this).val());
                        data.push(tem); console.log(tem);
                        $('#calendar').fullCalendar('addEventSource', [{
                            "category":tem['category'],
                            "title":tem['title'],
                            "start":tem['start'],
                            "end":tem['end'],
                            "allDay":true,
                            "description":tem['description'],
                            "className":tem['className'],
                            "row":tem['row'],
                            "_id":tem['_id'],
                            "specical":tem['specical']
                        }]);
                        row++;
                    }

                });
            }else if(flag==8){
                var custom_day_start=$("input[name='custom_day_time_start']").val();
                var custom_day_end=$("input[name='custom_day_time_end']").val();
                var nowdate=new Date();
                if(custom_day_start>custom_day_end){
                    $("#err5").text("起始时间不能大于结束时间！");
                    $("#err5").show();
                    return false;
                }else{
                    $("#err5").text("");
                    $("#err5").hide();
                }
                var str=$("#custom_day_time_name").val();
                if(str=='' || str==null){
                    $("#err5").text("时间名称不能为空！");
                    $("#err5").show();
                    return false;
                }else{
                    $("#err5").text("");
                    $("#err5").hide();
                }
                tem['start']=2007+'-'+1+'-'+1+' '+custom_day_start+':00';
                tem['end']=2007+'-'+1+'-'+1+' '+custom_day_end+':59';
                tem['row']=row;
                tem['_id']=row;
                tem['title']=str;
                tem['specical']=0;
                row++;
                data.push(tem); console.log(tem);
                $('#calendar').fullCalendar('addEventSource', [{
                    "category":tem['category'],
                    "title":tem['title'],
                    "start":tem['start'],
                    "end":tem['end'],
                    "allDay":false,
                    "description":tem['description'],
                    "className":tem['className'],
                    "row":tem['row'],
                    "_id":tem['_id'],
                    "specical":tem['specical']
                }]);
            }else if(flag==9){
                var hours=$("input:checkbox[name='simple_day_hour_checkbox']:checked");
                var renderobject = $('#calendar').fullCalendar('clientEvents');
                var arrnow=[];
                $.each(renderobject, function (key, value) {
                    if(value['category']=='9'){
                        arrnow.push(parseInt(value['specical']));
                    }
                })
                hours.each(function() {
                    if(arrnow.indexOf(parseInt($(this).val()))>-1){

                    }else{
                        var nowdate=new Date();
                        tem['start']=2007+'-'+1+'-'+1+' '+parseInt($(this).val())+':00'+':00';
                        tem['end']=2007+'-'+1+'-'+1+' '+parseInt($(this).val())+':59'+':59';
                        tem['row']=row;
                        tem['_id']=row;
                        tem['title']=parseInt($(this).val())+"点";
                        tem['specical']=parseInt($(this).val());
                        data.push(tem); console.log(tem);
                        $('#calendar').fullCalendar('addEventSource', [{
                            "category":tem['category'],
                            "title":tem['title'],
                            "start":tem['start'],
                            "end":tem['end'],
                            "allDay":false,
                            "description":tem['description'],
                            "className":tem['className'],
                            "row":tem['row'],
                            "_id":tem['_id'],
                            "specical":tem['specical']
                        }]);
                        row++;
                    }
                });
            }
        });

        //报表名称检测是否为空
        $("#report_title").blur(function(){
            if($(this).val()=='' || $(this).val==null){
                $("#next").css({'background':'grey'});
                $("#err1").text("报表名称不能为空！");
                $("#err1").show();
            }else{
                $("#next").css({'background':'#739e73'});
                $("#err1").hide();
                $("#err1").text('');
            }
        });

        $('#add-event').click(function () {
            var title = $('#title').val(),
                priority = $('input:radio[name=priority]:checked').val(),
                description = $('#description').val(),
                icon = $('input:radio[name=iconselect]:checked').val();
            addEvent(title, priority, description, icon);
        });

        /* initialize the calendar
         -----------------------------------------------------------------*/

        $('#calendar').fullCalendar({
            header: hdr, //只有包含在header里面，才能触发翻页按钮
            //翻页按钮
            buttonText: {
                next: '<i class="fa fa-chevron-right"></i>',
                prev:'<i class="fa fa-chevron-left"></i>'
            },
            titleFormat:{
                month: 'MMMM',
                week: "",
                day: ''
            },
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            dayNames: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
            dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
            allDaySlot:false,
            firstDay:1,
            //     year:2007,
            //     month:0,
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            slotMinutes:30,
            axisFormat:'HH(:mm)',
            firstHour:0,
            drop: function (date, allDay) { // 往日历上面拖拽的时候调用
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.end=date;
                copiedEventObject.allDay = allDay;

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                console.log(copiedEventObject);
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            },
            //拉伸的时候触发事件
            eventResize:function( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
                console.log(event);
            },
            eventClick: function(event, jsEvent, view) {
                updatePopup( jsEvent, event, view);
            },

            //日程事件渲染时触发
            eventRender: function (event, element, icon) {
                if (!event.description == "") {
                    element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
                        "</span>");
                }
                if (!event.icon == "") {
                    element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
                        " '></i>");
                }
            },

            windowResize: function (event, ui) {
                $('#calendar').fullCalendar('render');
            }
        });
        $("#calendar").fullCalendar('gotoDate',2007,0);
        $('#calendar').fullCalendar('addEventSource', data);
        //初始化显示标签
        $("input:radio[name='radio_date']:checked").each(function() {
            var $temp = $(this).val();
            console.log($temp);
            if ($temp == 'day') {
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('changeView', 'agendaDay');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='8'&& category!='9' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'week') {
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('changeView', 'basicWeek');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='6'&& category!='7' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'month') {
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('changeView', 'month');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='3'&& category!='4'&& category!='5' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'year') {
                $("#calendar-buttons").show();
                $('#calendar').fullCalendar('changeView', 'month');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='0'&& category!='1'&& category!='2' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").hide();
            } else {
            }
        });
        /* hide default buttons */
        $('.fc-header-right, .fc-header-center').hide();

        $('#calendar-buttons #btn-prev').click(function () {
            $('.fc-button-prev').click();
            return false;
        });

        $('#calendar-buttons #btn-next').click(function () {
            $('.fc-button-next').click();
            return false;
        });

        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-button-today').click();
            return false;
        });


        //视图切换
        $('#mt').click(function () {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        $('#ag').click(function () {
            $('#calendar').fullCalendar('changeView', 'basicWeek');
        });

        $('#td').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });

        /*修改一个弹出层 设置*/
        function updatePopup(_jsEvent, cal, view)
        {
            var v = cal.title;
            $("#num").val(v);
            var _height = $("#popover").height();
            var _width = $("#popover").width();
            if(view.name == 'month')
            {
                $('#_times').text($.fullCalendar.formatDate(cal.start, "yyyy-MM-dd") + ' -- ' + $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd"))
            }
            else
            {
                $('#_times').text($.fullCalendar.formatDate(cal.start, "yyyy-MM-dd H:mm") + ' -- ' + $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd H:mm"))
            }
            $("#popover").css({
                'top' : _jsEvent.pageY - _height - 20,
                'left' : _jsEvent.pageX - _width/2
            })
            $("#popover").attr('_id',cal. _id);
            $("#popover").attr('start_time', cal.start);
            $("#popover").attr('row', cal.row);
            $("#popover").show();
        }
        //删除时间事件
        $("#del").click(function () {
            var _start_time = $(this).parent().attr('start_time');
            var _id=$(this).parent().attr('_id');
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if( cal._id==_id){
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                    row--;
                    $('#popover').hide();
                }
            })
        })
        //修改时间事件
        $("#update").click(function () {
            var _start_time = $(this).parent().attr('start_time');
            var _id=$(this).parent().attr('_id');
            var _value = $("#num").val();
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if( cal._id==_id){
                    cal.title = _value;
                    cal.editable = true;
                    //    $('#calendar').fullCalendar( 'rerenderEvents' );
                    $('#calendar').fullCalendar( 'updateEvent', cal );
                    console.log(cal);
                    $('#popover').hide();
                }
            })
        })
        //修改事件中的取消按钮
        $("#cancel").click(function () {
            $('#popover').hide();
        })
        //报表类型切换事件
        $("input:radio[name='radio_date']").change(function () {
            var $temp = $("input[name='radio_date']:checked").val();
            $("#custom_year").hide();
            $("#custom_month").hide();
            $("#custom_week").hide();
            $("#custom_day").hide();
            $("#simple_year").hide();
            $("#simple_year_week").hide();
            $("#simple_month_day").hide();
            $("#simple_month_week").hide();
            $("#simple_week").hide();
            $("#simple_day_hour").hide();
            if ($temp == 'day') {
                $("#calendar").fullCalendar('gotoDate',2007,0,1);
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('clientEvents', function ( cal ){
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                })
                row=1;
                data=[];
                $('#calendar').fullCalendar('changeView', 'agendaDay');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='8'&& category!='9' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'week') {
                $("#calendar").fullCalendar('gotoDate',2007,0);
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('clientEvents', function ( cal ){
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                })
                row=1;
                data=[];
                $('#calendar').fullCalendar('changeView', 'basicWeek');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='6'&& category!='7' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'month') {
                $("#calendar").fullCalendar('gotoDate',2007,0);
                $("#calendar-buttons").hide();
                $('#calendar').fullCalendar('clientEvents', function ( cal ){
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                })
                row=1;
                data=[];
                $('#calendar').fullCalendar('changeView', 'month');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='3'&& category!='4'&& category!='5' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").show();
            } else if ($temp == 'year') {
                $("#calendar").fullCalendar('gotoDate',2007,0);
                $("#calendar-buttons").show();
                $('#calendar').fullCalendar('clientEvents', function ( cal ){
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                })
                row=1;
                data=[];
                $('#calendar').fullCalendar('changeView', 'month');
                $('#external-events > li').each(function () {
                    var category=$(this).children('span').attr('data-category');
                    if(category!='0'&& category!='1'&& category!='2' ){
                        $(this).hide();
                    }else{
                        $(this).show();
                    }
                });
                $("#time_range").hide();
            } else {
            }
        })
        //时间段预览
        $("#btn_timeshow").button().click(function() {
            $("#pre_table_timeshow tbody").text("");
            setValueOneset();
            var texthtml='';
            var start;var end;var str;
            for(var i in data ){
                if(data[i][8]=='0'|| data[i][8]=='1'|| data[i][8]=='2'){
                    start= data[i][2].substring(5,10);
                    end=data[i][3].substring(5,10);
                    str=start+'---'+end;
                }else  if(data[i][8]=='3'|| data[i][8]=='4'|| data[i][8]=='5'){
                    start= data[i][2].substring(8,10);
                    end=data[i][3].substring(8,10);
                    str=start+'---'+end;
                }else if(data[i][8]=='6'|| data[i][8]=='7'){
                    start= data[i][2].substring(9,10);
                    end=data[i][3].substring(9,10);
                    if(start==end){
                        if(start=='1'){
                            str='周一';
                        }else if(start=='2'){
                            str='周二';
                        }else if(start=='3'){
                            str='周三';
                        }else if(start=='4'){
                            str='周四';
                        }else if(start=='5'){
                            str='周五';
                        }else if(start=='6'){
                            str='周六';
                        }else if(start=='7'){
                            str='周日';
                        }
                    }else{
                        if(start=='1'){
                            start='周一';
                        }else if(start=='2'){
                            start='周二';
                        }else if(start=='3'){
                            start='周三';
                        }else if(start=='4'){
                            start='周四';
                        }else if(start=='5'){
                            start='周五';
                        }else if(start=='6'){
                            start='周六';
                        }else if(start=='7'){
                            start='周日';
                        }
                        if(end=='1'){
                            end='周一';
                        }else if(end=='2'){
                            end='周二';
                        }else if(end=='3'){
                            end='周三';
                        }else if(end=='4'){
                            end='周四';
                        }else if(end=='5'){
                            end='周五';
                        }else if(end=='6'){
                            end='周六';
                        }else if(end=='7'){
                            end='周日';
                        }
                        str=start+'至'+end;
                    }
                }else if(data[i][8]=='8'|| data[i][8]=='9'){
                    start= data[i][2].substring(11,19);
                    end=data[i][3].substring(11,19);
                    str=start+'---'+end;
                }
                texthtml='<tr><td>'+'<input type="text" style="width:60px" value='+data[i][7]+'></td><td>'+data[i][1]+'</td><td>'+str+'</td></tr>';
                $("#pre_table_timeshow tbody").append(texthtml);
            }
            dialog.dialog("open");
        });

        //第二页
        $('#_ate').draggable();   //拖拽公式编辑框
        $('#_point_search').draggable();   //拖拽点位搜索框

        //添加公式
        $('#add_point').click(function(){
            var formula_title=$('#formula_title').val();//公式名称
            var formula = ele2Formula();//公式
            var formula_type = $("select[name='test']").val();//单位
            var formula_type_text = $("#select2-chosen-2").text();//单位
            $("#point_prompt").text('');
            if(formula_title == ''){$("#point_prompt").text('公式名称不能为空，请填写公式名称！');return false;}
            if(formula == ''){$("#point_prompt").text('公式不能为空，请编辑公式！');return false;}
            if(formula_type_text == ''){$("#point_prompt").text('单位不能为空，请选择单位！');return false;}

            $('#formula tbody').append(
                '<tr ng-repeat="user in users" class="ng-scope" data-formula_title="' +formula_title + '" data-formula_type="' + formula_type_text + '"data-formula="' + formula + '">' +
                '<td><input class = "_check2" type = "checkbox"></td>' +
                '<td>' + formula_title + '</td>' +
                '<td>' + formula + '</td>' +
                '<td>' + formula_type_text + '</td>' +
                '</tr>');

            $('#formula_title').val('');//初始化公式名称
            $('#formula_max_fie').val(0);//初始化最大过滤值
            $('#formula_min_fie').val(0);//初始化最小过滤值
            $('.select2-choices li').each(function(){//初始化公式
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })

            /*alert(formula_title+'*****'+formula_type_text+'*****'+formula_max_fie+'*****'+formula_min_fie+'*****'+formula);*/
        })

        //公式表格处的删除
        $('#_delete2').click(function () {                                                                                 //页面delete按钮删除事件
            $('._check2').each(function () {
                if ($(this).prop('checked')) {
                    $(this).parent().parent().remove();
                }
            })
        })



        $('#formula_set_checked').click(function () {                                                                               //checkbox全选事件
            var $check_all = $(this);
            $('._check2').each(function () {
                $(this).prop('checked', $check_all.prop('checked'));
            })
        })

        //提交按钮
        $('#_submit').click(function (event) {
            var table_title = $("input[name='report_name']").val();   //报表标题
            var table_type = $("input[name='radio_date']:checked").val();  //报表类型（日,月,年;day/month/year）
            var table_data_area = '';  //公式表格数据
            //获取表头点位信息
            $('#formula tbody tr').each(function(){
                table_data_area += $(this).data('formula_title')+'##'+$(this).data('formula')+'##'+$(this).data('formula_type')+';';
            })
            if (table_data_area.length == 0) {
                $("#point_prompt").text("请定义报表点位信息！");
                $("#formula_title").focus;
                return false;
                event.preventDefault();
            }else{
                table_data_area = put_off_last_letter(table_data_area);
            }
            var config = table_title + "&&" + table_type+ "&&"   + table_data_area;
            console.log(config);
            $("input[name='config']").val(config);
        })


        function put_off_last_letter($v) {
            return $v.substr(0, $v.length - 1);
        }
        /*******************************自定义公式 js***************************************************/

        //清空所有公式元素块儿的状态
        function _clear(){
            $('.select2-choices li').each(function(){
                $(this).data('status', 0);
            });
            $('#_error_log').text('');
        }

        //从所有公式元素快中找出当前选择的li
        function _find(){
            var $this = null;
            $('.select2-choices li').each(function(){
                if($(this).data('status') == 1){
                    $this = $(this);
                }
            });
            return $this;
        }
        //添加一个公式元素块li

        /********************************公式元素li块点击事件  ****************************************/
        $('.select2-choices').on('click', 'li', function(event){
            _clear();
            var $_p_left = $('#_formula').position().left;
            var $this = $(this);
            $this.data('status', 1);
            var $cha = - Math.round($('#_ate').width()/2) + 33;
            var $_X = $cha + $(this).position().left;
            var $_Y = $(this).parent().parent().parent().position().top + $(this).position().top;
            //设置弹出层在点击的li元素上方
            $('#_ate').show();
            //   $('#_pointname').hide();
            $('#_ate').css({'left': 100, 'top':20})

        });

        /*******************************根据所选择的公式元素 进行联动 ************************/
        $("#_ele").change(function(){
            $('._calculate').hide();
            $('#_point_search ').hide();
            var $_chose = $(this).val();
            if($_chose=='_dw'){
                $('#_point_search').show();
                $('#_point_search').css({'left': 300, 'top':20})
                $('#_point_search div').show();
                $('#modbus').hide();
                $('#others').hide();
                $('#_dw').show();
            }else{
                $('#_dw').hide();
                $('#'+$_chose).show();
            }
        });
        var $ele_val = '';
        var $ele_text = '';
        var $select_type = '';
        $('#_ele').change(function(){
            $ele_val = '';
            $ele_text = '';
            $select_type = 'sz';
        });
        $('#_fh').change(function(){
            $ele_val = $("#_fh :selected").val();
            $ele_text = $("#_fh :selected").text();
            $select_type = 'fh';
        });
        $('#_hs').change(function(){
            $ele_text = $ele_val = $("#_hs :selected").val();
            $select_type = 'hs';
        });
        /*     $('#_dw').change(function(){
         $ele_val = 'p' + $("#_dw :selected").val();
         $ele_text = $("#_dw :selected").text();
         alert('value= '+$ele_val+' '+'text= '+$ele_text);
         $select_type = 'dw';
         });*/
        $('#_mb').change(function(){
            $ele_val = $ele_text = $("#_mb :selected").val();
            $select_type = 'mb';
        })
        $('input[name=_sz]').blur(function(){
            $ele_val = $ele_text = $(this).val();
        })

        //选点
        /************************** 确定选择 按钮 *******************************************/
        $('.editable-container').on('click', '.editable-submit', function(){
            $('#_point_search').hide();
            $('#_dw').hide();
            var $_obj = _find();
            if($_obj == null){
                $('#_ate').hide();
                return false;
            }
            var $_type = $_obj.data('type');
            if(!$ele_val){
                $('#_error_log').show();
                $('#_error_log').text('元素不能为空');
                return false;
            }
            if($select_type == 'mb'){
                var $_html = '';
                var $_formula_arr = $ele_text.split(' ');
                //将模板中的 元素以li的形式加入到 公式列表中
                $.each($_formula_arr, function(k , v){
                    if(v.indexOf('[') !== -1 || v.indexOf('{') !== -1){
                        $_html += "<li class='select2-search-choice' data-type = 'mb' data-status = '0' style = 'background-color:#E7F2FC;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }else{
                        $_html += "<li class='select2-search-choice' data-value = "+v+" data-type = 'ele' data-status = '0' style = 'background-color:#FFF;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }
                });
                $_obj.after($_html);
                $_html = '';
                $_obj.remove();
            }
            else{
                $_obj.find('div').text($ele_text);
                $_obj.data('value', $ele_val);
                $_obj.removeClass('_temps');
                $_obj.data('type', 'ele');
                //已确认块的样式
                $_obj.css({'background-color':'#FFF','color':'#000','border':'none','padding':'1px 8px 1px 1px'});
            }
            $('#_'+$select_type + ' .select2').select2('val', '');
            $('#_'+$select_type).hide();
            $("#_ele").select2('val', '');
            $('#_ate').hide();
            $('#_pointname').val('');
            if($_type != 'plus' && $select_type != 'mb'){
                return false;
            }
            if($('#_formula li').last().data('type') == 'plus'){
                return false;
            }
            //判断如果当前选中的元素是＋才会执行下面的clone
            //自动生成一个li
            var $clone = $_obj.clone(true);
            $clone.data('value', '');
            $clone.data('type', 'plus');
            $clone.find('div').html('<i class="fa fa-plus"></i>');
            $clone.css({'background-color':'#AFEDF5', 'color':'#FFF', 'border': 'none','padding':'1px 14px 1px 8px'});

            $('#_formula ul').append($clone);
        })
        /************************* 关闭 按钮 *****************************************************/
        $('.editable-container').on('click', '.editable-cancel', function(){
            var $_obj = _find();
            if($_obj == null){}
            else if($_obj.data('type') == 'temp'){
                $_obj.remove();
            }
            $('#_ate').hide();
            $('#_point_search').hide();
            $('#_pointname').val('');
        })

        $('.select2-choices').on('click', '._delete', function(event){
            $(this).parent().remove();
            //在此 阻止事件冒泡
            event.stopPropagation();
        })

        $('#_formula').on('mouseout', function() {

            $('._temps').hide();
            $('._temps').mouseover(function(){
                $('._temps').show();
                $('._temps').click(function(){
                    $(this).removeClass('_temps');
                })
            })
        })
        //清空公式列表除了 'plus' 之外的 元素
        $('#_del').click(function(){
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        })
        /************************** 遍历 公式列表中的ele 元素 生成完整公式 ************************************/
        function ele2Formula(){
            var $_formula = '';
            $('.select2-choices li').each(function(){
                if($(this).data('type') == 'ele'){
                    $_formula += $(this).data('value');
                }
            })
            return $_formula;
        }

        $('#_sub').click(function(){
            var $_formula = '';
            $_formula = ele2Formula();
            $('#_calculate').val($_formula);
        })

        //预览报表格式
        $('#pre_view').click(function(){
            $("#preview").show();//显示预览区域
            $("#pre_table thead").text("");
            $("#pre_table tbody").text("");
            var table_type = $("input[name='radio_date']:checked").val();  //报表类型（日,周,月,年）
            var table_time_area = new Array();  //时间名称
            var table_data_area = new Array();  //公式名称
            //获取公式名称
            $('#formula tbody tr td:nth-child(2)').each(function(v){
                table_data_area.push($(this).text());
            })
            console.log(table_data_area);
            if (table_data_area.length == 0) {
                $("#point_prompt").text("请定义报表点位信息！");
                $("#formula_title").focus; return false;
            }
            //初始化表格头部head
            var headfor='';
            for(var j in table_data_area){
                headfor+='<td>'+table_data_area[j]+'</td>';
            }
            var headstr='<tr><td>行号</td><td>时间名称</td>'+headfor+'</tr>';
            console.log(headstr);
            $("#pre_table thead").append(headstr);
            //获取时间名称 充预览表格
            for(var i in data ){
                var gongshi='';
                for(var j in table_data_area){
                    gongshi+='<td>no data</td>';
                }
                var texthtml='<tr><td>'+data[i][7]+'</td><td>'+data[i][1]+'</td>'+gongshi+'</tr>';
                console.log(texthtml);
                $("#pre_table tbody").append(texthtml);
            }
        })

        $("#close_pre_table").click(function(){
            $("#preview").hide();//显示预览区域
            $("#pre_table thead").text("");
        })
        //关闭时间段预览
        $("#close_timeshow").click(function(){
            $("#timeshow").hide();//显示预览区域
            $("#pre_table_timeshow thead").text("");
            $("#pre_table_timeshow tbody").text("");
        })
        var oTable_others; var oTable_bacnet; var oTable_modbus;
        var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0;
        $("#bacnet_show").click(function(){
            $('#modbus').hide();
            $('#others').hide();
            if(!temp_bacnet){
                oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
                temp_bacnet = 1;
            }
        })

        $("#modbus_show").click(function(){
            $('#bacnet').hide();
            $('#others').hide();
            if(!temp_modbus){
                oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
                temp_modbus = 1;
            }
        })

        $("#others_show").click(function(){
            $('#bacnet').hide();
            $('#modbus').hide();
            if(!temp_others){
                oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
                temp_others = 1;
            }
        })

        $(document).ready(function () {
            $('#tabs').tabs({
                activate: function(event, ui) {
                    var ttInstances = TableTools.fnGetMasters();
                    for (var i in ttInstances) {
                        if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                    }
                }
            });
            //  setTimeout(function(){$("#bacnet_show").click();},1000);
            setTimeout(function(){$("#bacnet_show").click();},1000);

        });
        window.exedel = function(data){
            $ele_val='p'+data.id;
            $ele_text=data.name;
            $('#_pointname').val($ele_text);
            $('#_point_search').hide();
        };
        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable,tool,protocol_id,type) {
            /* TABLETOOLS */
            var table = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,6, 7, 8, 9, 10, -1], [5,6, 7, 8, 9, 10, "All"]],
                "iDisplayLength": 5,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "bFilter": true, //搜索功能
                "ajax": {
                    "url": "/scheduled-report/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {"mDataProp": "id",},
                    {"mDataProp": "cn"},
                    {"mDataProp": "src_name"},
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+sData+'" name="'+oData.cn+'"><span class="glyphicon glyphicon-ok"></span></button></label>');
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {

                },
                "order": [1, 'asc'],
                "rowCallback": function( row, data ) {

                }
            });
            /* END TABLETOOLS */
            return table;
        }

        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }
    }
    //初始化
    function Init()
    {
        var modelarr=<?=json_encode($modelarr)?>;
        console.log(modelarr);
        var name_json=eval("("+modelarr['name']+")");
        var config_json=eval("("+modelarr['config']+")");
        console.log(config_json);
        $("#report_title").val(name_json['data']['zh-cn']);
        $("#time_range_start").val(config_json['data']['start'].substring(0,10));
        $("#time_range_end").val(config_json['data']['end'].substring(0,10));
        $("input:radio[name='radio_date']").each(
            function() {
                if($(this).val()==config_json['data']['type']){
                    $(this).attr('checked',true);
                }
            }
        );
        for(var i=0;i<config_json['data']['row'].length;i++){
            row++;
            data.push(config_json['data']['row'][i]);
        }
        console.log(data);
    }
    function displayset(one,two){
        if(one=='oneset')
        {
            if($("#report_title").val()==''){
                $("#err1").text("报表名称不能为空！");
                $("#err1").show();
                $("#next").css({'background':'grey'});
                return false;
            }else{
                $("#err1").hide();
                $("#err1").text('');
                $("#next").css({'background':'#739e73'});
                row=1;
                setValueOneset();
            }
        }
        console.log(data);
        $("#"+one).hide();
        $("#"+two).show();
    }

    //遍历日历上面的事件，并且赋值
    function setValueOneset()
    {
        var sortdata=[];
        data=[];
        var datatem = $('#calendar').fullCalendar('clientEvents');
        console.log(datatem);
        var type = $("input[name='radio_date']:checked").val();
        $.each(datatem, function (key, value) {
            var _id;var title;var start='';var end='';var allDay=false;var description;var className;var row;var category;var specical;
            var one_value=new Array();
            $.each(value, function (k, v) {
                if(k=='_id'){
                    _id=value['_id'];
                } else if(k == 'title'){
                    title = value['title'];
                }else if (k == 'start') {
                    start = $.fullCalendar.formatDate(v, "yyyy-MM-dd HH:mm:ss") ;  //object转字符串
                }else if(k == 'end'){
                    if(v==''|| v==null){
                        v=value['start'];
                    }
                    if(type!="day"){
                        v.setHours(23);v.setMinutes(59);v.setSeconds(59);
                        end = $.fullCalendar.formatDate(v, "yyyy-MM-dd HH:mm:ss");
                    }else{
                        end = $.fullCalendar.formatDate(v, "yyyy-MM-dd HH:mm:ss");
                    }

                }else if(k=='allDay'){
                    allDay = value['allDay'] ;
                }else if(k=='description'){
                    description = value['description'] ;
                }else if(k=='className'){
                    className = value['className'] ;
                }else if(k=='row'){
                    row=value['row'];
                }else if(k=='category'){
                    category=value['category'];
                }else if(k=='specical'){
                    specical=value['specical'];
                }
            })
            one_value.push(_id); one_value.push(title); one_value.push(start);  one_value.push(end);
            one_value.push(allDay); one_value.push(description);one_value.push(className);one_value.push(row);one_value.push(category);one_value.push(specical);
            sortdata[row]=one_value;
            //   data.push(one_value);
        })
        console.log(sortdata);
        for(var i in sortdata){
            data.push(sortdata[i]);
        }
        console.log(data);
        for(var i in data){
            data[i][7]=parseInt(i)+1;
            data[i][0]=data[i][7];
        };
        console.log(data);
        $("input[name='detail']").val(JSON.stringify(data));
        $("input[name='report_name']").val($("#report_title").val());
        $("input[name='fromStart']").val($("#time_range_start").val()+' 00:00:00');
        $("input[name='toEnd']").val($("#time_range_end").val()+' 23:59:59');
    }
    //得到天数
    function getdays(year,monflag)
    {
        var isflag=false;
        if((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)){
            isflag=true;
        }else{
            isflag=false;
        }
        if(monflag==1 || monflag==3 || monflag==5|| monflag==7|| monflag==8|| monflag==10|| monflag==12 ){
            return 31;
        }else if(monflag==4|| monflag==6|| monflag==9|| monflag==11){
            return 30;
        }else {
            if(isflag){
                return 29;
            }else{
                return 28;
            }
        }
    }
    //这个方法将取得某年(year)第几周(weeks)的星期几(weekDay)的日期
    function getXDate(year,weeks,weekDay){
        //用指定的年构造一个日期对象，并将日期设置成这个年的1月1日
        //因为计算机中的月份是从0开始的,所以有如下的构造方法
        var date = new Date(year,"0","1")
        //取得这个日期对象 date 的长整形时间 time
        var time = date.getTime();
        //将这个长整形时间加上第N周的时间偏移
        //因为第一周就是当前周,所以有:weeks-1,以此类推
        //7*24*3600000 是一星期的时间毫秒数,(JS中的日期精确到毫秒)
        time+=(weeks-1)*7*24*3600000;
        //为日期对象 date 重新设置成时间 time
        date.setTime(time);
        return getNextDate(date,weekDay);
    }
    //这个方法将取得 某日期(nowDate) 所在周的星期几(weekDay)的日期
    function getNextDate(nowDate,weekDay){
        //0是星期日,1是星期一,...
        weekDay%=7;
        var day = nowDate.getDay();
        var time = nowDate.getTime();
        var sub = weekDay-day;
        time+=sub*24*3600000;
        nowDate.setTime(time);
        return nowDate;
    }

    //js 获取某年某月有几周，以及每周的周一和周末是几号到几号
    function getInfo(year,month,count) {
        console.log(count);
        var arr=[];
        var d = new Date();
        d.setFullYear(year, month-1, 1);
        var w1 = d.getDay();
        if (w1 == 0) w1 = 7;
        d.setFullYear(year, month, 0);
        var dd = d.getDate();
        var d1;
        // first Monday
        if (w1 != 1) d1 = 7 - w1 + 2;
        else d1 = 1;
        var week_count = Math.ceil((dd-d1+1)/7);
        console.log(week_count);
        for (var i = 0; i < week_count; i++) {
            if(i==count-1){
                var monday = d1+i*7;
                var sunday = monday + 6;
                var from = year+"-"+month+"-"+monday;
                var to;
                if (sunday <= dd) {
                    to = year+"-"+month+"-"+sunday;
                } else {
                    d.setFullYear(year, month-1, sunday);
                    to = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
                }
                arr.push(from);
                arr.push(to);
            }
        }
        return arr;
    }
</script>
