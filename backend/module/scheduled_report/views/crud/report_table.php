<?php
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
TableAsset::register ( $this );
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?=$name?>&nbsp;&nbsp;&nbsp;&nbsp;<?='('.$start.'-'.$end.')'?></h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" data-target="#SaveMenu" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div>
                    <div id="bacnet">
                        <table id="report" class="table table-striped table-bordered table-hover" style="width:100%;">
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<script>
    window.onload = function() {
        var header_str=<?=$header_c?>;
        var header=eval("("+header_str['header']+")");
        var max_row=header['data']['max_row'];
        var max_column=header['data']['max_column'];
        var start_x=header['data']['start_x'];
        var start_y=header['data']['start_y'];
        var head_data=header['data']['data'];
        var tbody_str='';
        var data=<?=json_encode($data)?>;
        console.log(data);
      for(var i=0;i<max_row;i++){
          var tr="";
          for(var j=0;j<max_column;j++){
                    if(i<start_x){
                        for(var k in head_data){
                            if(i==parseInt(head_data[k]["x"])&& j==parseInt(head_data[k]["y"])){
                                if(i==start_x-1){
                                    tr+='<td style="background-color:#E48E28;font-weight:bold" rowspan='+parseInt(head_data[k]["rowspan"])+' colspan='+parseInt(head_data[k]["colspan"])+'><a  style="color: black" href="/scheduled-report/crud/update-fomular?id='+"<?=$id?>"+'&column='+(j-start_y+1)+'">'+head_data[k]["text"]+'</a></td>';
                                }else{
                                    tr+='<td style="background-color:#E48E28;font-weight:bold" rowspan='+parseInt(head_data[k]["rowspan"])+' colspan='+parseInt(head_data[k]["colspan"])+'>'+head_data[k]["text"]+'</td>';
                                }
                            }
                        }
                    }else{
                        for(var k in head_data){
                            if(i==parseInt(head_data[k]["x"])&& j==parseInt(head_data[k]["y"])){
                                if(j<start_y){
                                    if(head_data[k]["text"]=='周1'){
                                        head_data[k]["text"]='周一';
                                    }else if(head_data[k]["text"]=='周2'){
                                        head_data[k]["text"]='周二';
                                    }else if(head_data[k]["text"]=='周3'){
                                        head_data[k]["text"]='周三';
                                    }else if(head_data[k]["text"]=='周4'){
                                        head_data[k]["text"]='周四';
                                    }else if(head_data[k]["text"]=='周5'){
                                        head_data[k]["text"]='周五';
                                    }else if(head_data[k]["text"]=='周6'){
                                        head_data[k]["text"]='周六';
                                    }else if(head_data[k]["text"]=='周7'){
                                        head_data[k]["text"]='周日';
                                    }
                                    tr+='<td style="font-weight:bold" rowspan='+parseInt(head_data[k]["rowspan"])+' colspan='+parseInt(head_data[k]["colspan"])+'>'+head_data[k]["text"]+'</td>';
                                }
                            }
                        }
                        for(var n in data){
                            if((i-start_x+1)==parseInt(data[n]['row']) ){
                                var tem=data[n]["data"];
                               for(var m in tem){
                                   if((j-start_y+1)==parseInt(tem[m]['column']))
                                   tr+='<td>'+tem[m]['value']+'</td>';
                               }
                            }
                        }
                    }
          }
          tbody_str+='<tr style="text-align:center;vertical-align:middle;">'+tr+'</tr>'
      }
       // console.log(tbody_str);
        $("#report tbody").append(tbody_str);

    }
</script>
