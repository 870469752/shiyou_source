<?php
use backend\assets\TableAsset;
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
TableAsset::register ( $this );
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2>定时报表</h2>
                </header>
                <!-- widget div-->
                <div>
                    <div id="bacnet">
                        <table id="datatable" class="table table-striped table-bordered table-hover" style="width:100%;">
                            <thead>
                            <tr>
                                <th data-class="expand">报表名称</th>
                                <th data-class="expand">报表类型</th>
                                <th data-class="expand">开始时间</th>
                                <th data-class="expand">结束时间</th>
                                <th data-class="expand">上次生成时间</th>
                                <th data-class="expand">下次生成时间</th>
                                <th data-class="expand">创建者</th>
                                <th data-class="expand">创建时间</th>
                                <th data-class="expand">查看</th>
                                <th data-class="expand">删除</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </article>
    </div>
</section>

<script>
    var user={};
    window.onload = function() {
        user=<?=json_encode($users_array)?>;
        var oTable_bacnet=initTable('datatable','oTable_bacnet');
        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable) {
            /* TABLETOOLS */
            table = $('#datatable').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+oTable+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[10,11, 12, 13, 14, 15, -1], [10,11, 12, 13, 14, 15, "All"]],
                "iDisplayLength": 10,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                "ajax": {
                    "url": "/scheduled-report/crud/get-report",
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },

                "aoColumns": [
                    {
                        "mDataProp": "cn",
                        "bSortable": true},
                    {
                        "mDataProp": "type",
                        "bSortable": true,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var temp_sData = '';
                        if(sData == 'day'){
                            temp_sData = '日报表';
                        }else if(sData == 'month'){
                            temp_sData = '月报表';
                        }else if(sData == 'week'){
                            temp_sData = '周报表';
                        }else {
                            temp_sData = '年报表';
                        }
                        $(nTd).html(temp_sData);
                    }
                    },
                    {"mDataProp": "start_timestamp","bSortable": true},
                    {"mDataProp": "end_timestamp","bSortable": true},
                    {"mDataProp": "last_generate_timestamp","bSortable": true},
                    {"mDataProp": "next_generate_timestamp","bSortable": true},
                    {
                        "mDataProp": "owner",
                        "bSortable": true,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html(user[sData]);
                    }},
                    {"mDataProp": "create_timestamp","bSortable": true},
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/scheduled-report/crud/view?id="+sData+"'>查看</a>");
                        }
                    },
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/scheduled-report/crud/delete1?id="+sData+"'>删除</a>");
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {
                    $('<a href="/scheduled-report/crud/create" class="btn btn-success" id="deleteFun">创建</a>' + '&nbsp;').appendTo($('.'+oTable));//myBtnBox
                    $("#deleteFun").click(function(){
                    });
                },
                "order": [0, 'asc']
            });
            /* END TABLETOOLS */
            return table;
        }

        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }


    }

</script>
