<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/7/5
 * Time: 14:48
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use common\library\MyHtml;
?>
<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset{
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }
    table td{width: auto}
    table tr{border:dotted ; border-width:1px 0px 0px 0px;}
    table{border:dotted ; border-width:1px 1px 1px 1px;}

</style>
<section id="widget-grid" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2>表头公式修改</h2>
                </header>
                <!-- widget div-->
                <div>
                    <?php $form = ActiveForm::begin(['action' => '/scheduled-report/crud/save-new-formula','options' => ['class' => 'smart-form']]); ?>
                    <?= Html::hiddenInput("config"); ?>
                    <div id = "twoset" >
                        <fieldset>
                            <div class="col-md-12">
                                <label class="label"  style="color:black">公式名称</label>
                                <input type="text" id="formula_title" class="form-control" name="formula_title" value="">
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <label class="label" style="color:black">公式编辑</label>
                            </div>
                            <div class="input-group" style="margin-top: 10px" id = '_formula'>
                                <div class = 'select2-container select2-container-multi' style = 'margin-top:2px;width: 100%;'>
                                    <ul class="select2-choices">
                                        <li class="select2-search-choice" data-status = '0' data-type = 'plus' style = 'background-color:#AFEDF5;border:none;'>
                                            <div><i class="fa fa-plus"></i></div>
                                            <a style = 'display:none' href="#" class="select2-search-choice-close _delete" tabindex="-1"></a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="input-group-btn" style="margin-top: 10px" id = '_del'>
                                    <button class="btn create btn-primary" type="button">
                                        <i class="fa fa-mail-reply-all"></i> Clear
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <label class="label" style="color:black">单位选择</label>
                                <?=Html::dropDownList('test', '', $unit, ['class' => 'select2', 'placeholder' => '无'])?>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px" >
                                <button class="btn create btn-primary" id = 'add_point' type="button">
                                    修改公式
                                </button>
                                <label id="point_prompt" style="font-size: 13px; color: red; margin-left: 10px"></label>
                            </div>
                        </fieldset>
                        <fieldset id="formula_static">
                            <label id="description" style="margin-bottom:5px">已有公式:</label>
                            <div class="col-md-12">
                                <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                    <div class="panel panel-primary" style="padding: 15px;display: block">
                                        <div class="panel-body" ng-transclude>
                                            <table id = "formula" class="table table-bordered table-condensed ng-scope">
                                                <thead>
                                                <tr style="font-weight: bold">
                                                    <th style = "width:auto">列号</th>
                                                    <th style="width:auto">公式名称</th>
                                                    <th style="width:auto">计算公式</th>
                                                    <th style="width:auto">单位</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </panel>
                            </div>
                        </fieldset>

                        <fieldset id="preview" style="display: none">
                            <div class="col-md-12">
                                <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                    <div class="panel panel-primary" style="padding: 15px;display: block">
                                        <div class="panel-body" ng-transclude>
                                            <table id = "pre_table" class="table table-bordered table-condensed ng-scope">
                                                <thead style="text-align: center">

                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <footer style="background-color: lightgoldenrodyellow">
                                                <button id = "close_pre_table" type="button" class="btn btn-primary">close</button>
                                            </footer>
                                        </div>
                                    </div>
                                </panel>
                            </div>
                            <div id="pre_table"></div>
                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </article>

    </div>
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_ate'  style="display: none;">
        <div class="arrow"></div>
        <h3 class="popover-title">选择点位</h3>
        <div class="popover-content">
            <div class="editableform-loading" style="display: none;"></div>
            <form class="form-inline editableform" style="">
                <div class="control-group form-group"><div>
                        <div class="editable-input">
                            <div class="editable-address">
                                <?=Html::dropDownList('gs', '', [
                                    '' => '',
                                    '_fh' => '运算符',
                                    '_hs' => '函数',
                                    '_sz' => '数字',
                                    '_dw' => '点位',
                                    '_mb' => '模板',
                                ], ['class' => 'select2', 'placeholder' => '请选择公式组成元素', 'id' => '_ele'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_fh' style = 'display:none'>
                                <?=Html::dropDownList('_fh', '', [
                                    '' => '',
                                    '＋' => '＋',
                                    '－' => '－',
                                    '*' => '×',
                                    '/' => '÷',
                                    '(' => '(',
                                    ')' => ')'
                                ], ['class' => 'select2', 'placeholder' => '请选择运算符号'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_hs' style = 'display:none'>
                                <?=Html::dropDownList('_hs', '', [
                                    '' => '',
                                    'sin(' => 'sin',
                                    'cos(' => 'cos',
                                    'max(' => 'max',
                                    'min(' => 'min',
                                    'sum(' => 'sum',
                                    'avg(' => 'avg'
                                ], ['class' => 'select2', 'placeholder' => '请选择函数'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_sz' style = 'display:none'>
                                <input type = 'text' name = '_sz' style = 'width:100%' placeholder = '请填写数字'>
                            </div>
                            <div class="editable-address _calculate" id  = '_dw' style = 'display:none'>
                                <input type = 'text' id = '_pointname' style = 'width:100%' placeholder = '点位名称'>
                            </div>
                            <div class="editable-address _calculate" id  = '_mb' style = 'display:none'>
                                <?=Html::dropDownList('_mb', '', $formula, ['class' => 'select2', 'placeholder' => '请选择模板公式'])?>
                            </div>
                            <div class="editable-address _calculate" id = '_error_log' style = '    display:none;color:#50cd42'></div>
                        </div>
                        <div class="editable-buttons">
                            <button type="button" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i></button>
                            <button type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>
                        </div>
                    </div>
                    <div class="editable-error-block help-block" style="display: none;"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="popover fade top in editable-container editable-popup" role="tooltip" id = '_point_search'  style="display: none;width:900px">
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
             data-widget-Collapse="false"
             data-widget-custom="false"
             data-widget-togglebutton="false" id = '_jarviswidget' >
            <header>
                <span class="widget-icon"><i class="fa fa-table"></i></span>
                <h2><?=Yii::t('app', 'Point Batch')?></h2>
                <div class="jarviswidget-ctrls"></div>
            </header>
            <!-- 参数主体 start-->
            <div class="widget-body" >
                <!-- Widget ID (each widget will need unique ID)-->
                <div>
                    <!-- widget edit box -->
                    <div class="widget-body no-padding">
                        <div class="well well-sm well-light">
                            <div id="tabs" >
                                <ul>
                                    <li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
                                    <li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                                    <li><a href="#others" id="others_show">其他点位</a></li>
                                </ul>
                                <div id="bacnet">
                                    <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="modbus">
                                    <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div >
                                <div id="others">
                                    <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">硬件名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end widget content -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php

$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/clockpicker/clockpicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
?>
<script>

   var  data={};//存放数据

    window.onload = function()
    {

      Init();

        //第二页
        $('#_ate').draggable();   //拖拽公式编辑框
        $('#_point_search').draggable();   //拖拽点位搜索框

        //添加公式
        $('#add_point').click(function(){
            var formula_title=$('#formula_title').val();//公式名称
            var formula = ele2Formula();//公式
            var formula_type = $("select[name='test']").val();//单位
            var formula_type_text = $("#select2-chosen-2").text();//单位
            var pos=data['column'];
            $("#point_prompt").text('');
            if(formula_title == ''){$("#point_prompt").text('公式名称不能为空，请填写公式名称！');return false;}
            if(formula == ''){$("#point_prompt").text('公式不能为空，请编辑公式！');return false;}
            if(formula_type_text == ''){$("#point_prompt").text('单位不能为空，请选择单位！');return false;}
            $('#formula tbody tr').each(function(){
                $(this).remove();
            });
            $('#formula tbody').append(
                '<tr ng-repeat="user in users" class="ng-scope" data-formula_title="' +formula_title + '" data-formula_type="' + formula_type_text + '"data-formula="' + formula + '"position="'+pos+'">' +
                '<td>'+ pos +'</td>' +
                '<td>' + formula_title + '</td>' +
                '<td>' + formula + '</td>' +
                '<td>' + formula_type_text + '</td>' +
                '</tr>');
            $('#formula_title').val('');//初始化公式名称
            $('#formula_max_fie').val(0);//初始化最大过滤值
            $('#formula_min_fie').val(0);//初始化最小过滤值
            $('.select2-choices li').each(function(){//初始化公式
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        })


        $('#formula_set_checked').click(function () {                                                                               //checkbox全选事件
            var $check_all = $(this);
            $('._check2').each(function () {
                $(this).prop('checked', $check_all.prop('checked'));
            })
        })

        //提交按钮
        $('#_submit').click(function (event) {
            $('#formula tbody tr').each(function(){
                console.log($(this).data('formula_type'));
                data['name']=$(this).data('formula_title');
                data['formula']=$(this).data('formula');
                data['column']=$(this).attr('position');
                data['unit']=$(this).data('formula_type');
            })
            $("input[name='config']").val(JSON.stringify(data));
            console.log(data);
        //    return false;
        })


        function put_off_last_letter($v) {
            return $v.substr(0, $v.length - 1);
        }
        /*******************************自定义公式 js***************************************************/

        //清空所有公式元素块儿的状态
        function _clear(){
            $('.select2-choices li').each(function(){
                $(this).data('status', 0);
            });
            $('#_error_log').text('');
        }

        //从所有公式元素快中找出当前选择的li
        function _find(){
            var $this = null;
            $('.select2-choices li').each(function(){
                if($(this).data('status') == 1){
                    $this = $(this);
                }
            });
            return $this;
        }
        //添加一个公式元素块li

        /********************************公式元素li块点击事件  ****************************************/
        $('.select2-choices').on('click', 'li', function(event){
            _clear();
            var $_p_left = $('#_formula').position().left;
            var $this = $(this);
            $this.data('status', 1);
            var $cha = - Math.round($('#_ate').width()/2) + 33;
            var $_X = $cha + $(this).position().left;
            var $_Y = $(this).parent().parent().parent().position().top + $(this).position().top;
            //设置弹出层在点击的li元素上方
            $('#_ate').show();
            //   $('#_pointname').hide();
            $('#_ate').css({'left': 100, 'top':20})

        });

        /*******************************根据所选择的公式元素 进行联动 ************************/
        $("#_ele").change(function(){
            $('._calculate').hide();
            $('#_point_search ').hide();
            var $_chose = $(this).val();
            if($_chose=='_dw'){
                $('#_point_search').show();
                $('#_point_search').css({'left': 300, 'top':20})
                $('#_point_search div').show();
                $('#modbus').hide();
                $('#others').hide();
                $('#_dw').show();
            }else{
                $('#_dw').hide();
                $('#'+$_chose).show();
            }
        });
        var $ele_val = '';
        var $ele_text = '';
        var $select_type = '';
        $('#_ele').change(function(){
            $ele_val = '';
            $ele_text = '';
            $select_type = 'sz';
        });
        $('#_fh').change(function(){
            $ele_val = $("#_fh :selected").val();
            $ele_text = $("#_fh :selected").text();
            $select_type = 'fh';
        });
        $('#_hs').change(function(){
            $ele_text = $ele_val = $("#_hs :selected").val();
            $select_type = 'hs';
        });
        /*     $('#_dw').change(function(){
         $ele_val = 'p' + $("#_dw :selected").val();
         $ele_text = $("#_dw :selected").text();
         alert('value= '+$ele_val+' '+'text= '+$ele_text);
         $select_type = 'dw';
         });*/
        $('#_mb').change(function(){
            $ele_val = $ele_text = $("#_mb :selected").val();
            $select_type = 'mb';
        })
        $('input[name=_sz]').blur(function(){
            $ele_val = $ele_text = $(this).val();
        })

        //选点
        /************************** 确定选择 按钮 *******************************************/
        $('.editable-container').on('click', '.editable-submit', function(){
            $('#_point_search').hide();
            $('#_dw').hide();
            var $_obj = _find();
            if($_obj == null){
                $('#_ate').hide();
                return false;
            }
            var $_type = $_obj.data('type');
            if(!$ele_val){
                $('#_error_log').show();
                $('#_error_log').text('元素不能为空');
                return false;
            }
            if($select_type == 'mb'){
                var $_html = '';
                var $_formula_arr = $ele_text.split(' ');
                //将模板中的 元素以li的形式加入到 公式列表中
                $.each($_formula_arr, function(k , v){
                    if(v.indexOf('[') !== -1 || v.indexOf('{') !== -1){
                        $_html += "<li class='select2-search-choice' data-type = 'mb' data-status = '0' style = 'background-color:#E7F2FC;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }else{
                        $_html += "<li class='select2-search-choice' data-value = "+v+" data-type = 'ele' data-status = '0' style = 'background-color:#FFF;color:#000;border:none;margin-left:1px;padding:1px 8px 1px 1px' >" +
                            "<div>" + v + "</div>" +
                            "<a href='#' style = 'display:none' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                    }
                });
                $_obj.after($_html);
                $_html = '';
                $_obj.remove();
            }
            else{
                $_obj.find('div').text($ele_text);
                $_obj.data('value', $ele_val);
                $_obj.removeClass('_temps');
                $_obj.data('type', 'ele');
                //已确认块的样式
                $_obj.css({'background-color':'#FFF','color':'#000','border':'none','padding':'1px 8px 1px 1px'});
            }
            $('#_'+$select_type + ' .select2').select2('val', '');
            $('#_'+$select_type).hide();
            $("#_ele").select2('val', '');
            $('#_ate').hide();
            $('#_pointname').val('');
            if($_type != 'plus' && $select_type != 'mb'){
                return false;
            }
            if($('#_formula li').last().data('type') == 'plus'){
                return false;
            }
            //判断如果当前选中的元素是＋才会执行下面的clone
            //自动生成一个li
            var $clone = $_obj.clone(true);
            $clone.data('value', '');
            $clone.data('type', 'plus');
            $clone.find('div').html('<i class="fa fa-plus"></i>');
            $clone.css({'background-color':'#AFEDF5', 'color':'#FFF', 'border': 'none','padding':'1px 14px 1px 8px'});

            $('#_formula ul').append($clone);
        })
        /************************* 关闭 按钮 *****************************************************/
        $('.editable-container').on('click', '.editable-cancel', function(){
            var $_obj = _find();
            if($_obj == null){}
            else if($_obj.data('type') == 'temp'){
                $_obj.remove();
            }
            $('#_ate').hide();
            $('#_point_search').hide();
            $('#_pointname').val('');
        })

        $('.select2-choices').on('click', '._delete', function(event){
            $(this).parent().remove();
            //在此 阻止事件冒泡
            event.stopPropagation();
        })

        $('#_formula').on('mouseout', function() {

            $('._temps').hide();
            $('._temps').mouseover(function(){
                $('._temps').show();
                $('._temps').click(function(){
                    $(this).removeClass('_temps');
                })
            })
        })
        //清空公式列表除了 'plus' 之外的 元素
        $('#_del').click(function(){
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        })
        /************************** 遍历 公式列表中的ele 元素 生成完整公式 ************************************/
        function ele2Formula(){
            var $_formula = '';
            $('.select2-choices li').each(function(){
                if($(this).data('type') == 'ele'){
                    $_formula += $(this).data('value');
                }
            })
            return $_formula;
        }

        $('#_sub').click(function(){
            var $_formula = '';
            $_formula = ele2Formula();
            $('#_calculate').val($_formula);
        })

        //预览报表格式
        $('#pre_view').click(function(){
            $("#preview").show();//显示预览区域
            $("#pre_table thead").text("");
            $("#pre_table tbody").text("");
            var table_type = $("input[name='radio_date']:checked").val();  //报表类型（日,周,月,年）
            var table_time_area = new Array();  //时间名称
            var table_data_area = new Array();  //公式名称
            //获取公式名称
            $('#formula tbody tr td:nth-child(2)').each(function(v){
                table_data_area.push($(this).text());
            })
            console.log(table_data_area);
            if (table_data_area.length == 0) {
                $("#point_prompt").text("请定义报表点位信息！");
                $("#formula_title").focus; return false;
            }
            //初始化表格头部head
            var headfor='';
            for(var j in table_data_area){
                headfor+='<td>'+table_data_area[j]+'</td>';
            }
            var headstr='<tr><td>行号</td><td>时间名称</td>'+headfor+'</tr>';
            console.log(headstr);
            $("#pre_table thead").append(headstr);
            //获取时间名称 充预览表格
            for(var i in data ){
                var gongshi='';
                for(var j in table_data_area){
                    gongshi+='<td>no data</td>';
                }
                var texthtml='<tr><td>'+data[i][7]+'</td><td>'+data[i][1]+'</td>'+gongshi+'</tr>';
                console.log(texthtml);
                $("#pre_table tbody").append(texthtml);
            }
        })

        $("#close_pre_table").click(function(){
            $("#preview").hide();//显示预览区域
            $("#pre_table thead").text("");
        })
        //关闭时间段预览
        $("#close_timeshow").click(function(){
            $("#timeshow").hide();//显示预览区域
            $("#pre_table_timeshow thead").text("");
            $("#pre_table_timeshow tbody").text("");
        })
        var oTable_others; var oTable_bacnet; var oTable_modbus;
        var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0;
        $("#bacnet_show").click(function(){
            $('#modbus').hide();
            $('#others').hide();
            if(!temp_bacnet){
                oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
                temp_bacnet = 1;
            }
        })

        $("#modbus_show").click(function(){
            $('#bacnet').hide();
            $('#others').hide();
            if(!temp_modbus){
                oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
                temp_modbus = 1;
            }
        })

        $("#others_show").click(function(){
            $('#bacnet').hide();
            $('#modbus').hide();
            if(!temp_others){
                oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
                temp_others = 1;
            }
        })

        $(document).ready(function () {
            $('#tabs').tabs({
                activate: function(event, ui) {
                    var ttInstances = TableTools.fnGetMasters();
                    for (var i in ttInstances) {
                        if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                    }
                }
            });
            //  setTimeout(function(){$("#bacnet_show").click();},1000);
            setTimeout(function(){$("#bacnet_show").click();},1000);

        });
        window.exedel = function(data){
            $ele_val='p'+data.id;
            $ele_text=data.name;
            $('#_pointname').val($ele_text);
            $('#_point_search').hide();
        };
        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable,tool,protocol_id,type) {
            /* TABLETOOLS */
            var table = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,6, 7, 8, 9, 10, -1], [5,6, 7, 8, 9, 10, "All"]],
                "iDisplayLength": 5,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "bFilter": true, //搜索功能
                "ajax": {
                    "url": "/scheduled-report/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {"mDataProp": "id",},
                    {"mDataProp": "cn"},
                    {"mDataProp": "src_name"},
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html('<label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+sData+'" name="'+oData.cn+'"><span class="glyphicon glyphicon-ok"></span></button></label>');
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {

                },
                "order": [1, 'asc'],
                "rowCallback": function( row, data ) {

                }
            });
            /* END TABLETOOLS */
            return table;
        }

        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }
    }
    //初始化
    function Init()
    {
            var tem=<?=json_encode($data)?>;
            data=tem;
            var formula_title=data['name'];//公式名称
            var formula = data['formula'];//公式
            var formula_type_text = data['unit'];//单位
            var pos=data['column'];//列号
          $('#formula tbody').append(
            '<tr ng-repeat="user in users" class="ng-scope" data-formula_title="' +formula_title + '" data-formula_type="' + formula_type_text + '"data-formula="' + formula + '"position="'+pos+'">' +
            '<td>'+pos+'</td>' +
            '<td>' + formula_title + '</td>' +
            '<td>' + formula + '</td>' +
            '<td>' + formula_type_text + '</td>' +
            '</tr>');
            console.log(data);
    }




</script>
