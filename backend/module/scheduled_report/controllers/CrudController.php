<?php

namespace backend\module\scheduled_report\controllers;
//yii 引用
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;
//模型 引用
use backend\models\Template;
use backend\models\ScheduledReport;
use backend\models\search\ScheduledReportSearch;
use backend\models\search\ScheduledReportLogSearch;
use backend\models\ScheduledReportLog;
use backend\models\Unit;
use backend\models\EnergyCategory;
use backend\models\Location;
use common\models\User;
use backend\models\Point;
use common\library\Ssp;
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionTest(){
        return $this->render('gridly');
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex1()
    {
        $searchModel = new ScheduledReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        Yii::$app->session->set('manage_report_id', '');
        Yii::$app->session->set('report_rel', '');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    //
    public function actionIndex()
    {
        $usermodel=new User();
        $users=$usermodel->getUser();
        $users_array=array();
        for($i=0;$i<count($users);$i++)
        {
            $users_array[$users[$i]['id']]=$users[$i]['username'];
        }
        return $this->render('index_datatable', [
            'users_array'=>$users_array,
        ]);
    }

    //获取该用户report信息
    public function actionGetReport()
    {
        //db用于select后面的字段，dt用于返回数组的键值，dj查询条件字段（如果不想该字段用于查询，则不加dj属性即可)
        $table = 'core.schedule_report';
        $primaryKey = 'id';
        $con = Yii::$app->user->identity->id ."= ANY (auth_users) ";
        $columns = array(
            array( 'db' => "name->'data'->>'zh-cn'as cn", 'dt' => 'cn', 'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "config->'data'->>'type'as type", 'dt' => 'type', 'dj'=>"config->'data'->>'type'"),
            array( 'db' => "start_timestamp", 'dt' => 'start_timestamp', 'dj'=>"start_timestamp"),
            array( 'db' => "end_timestamp", 'dt' => 'end_timestamp', 'dj'=>"end_timestamp"),
            array( 'db' => "last_generate_timestamp", 'dt' => 'last_generate_timestamp', 'dj'=>"last_generate_timestamp"),
            array( 'db' => "next_generate_timestamp", 'dt' => 'next_generate_timestamp', 'dj'=>"next_generate_timestamp"),
            array( 'db' => "owner", 'dt' => 'owner', 'dj'=>"owner"),
            array( 'db' => "create_timestamp", 'dt' => 'create_timestamp', 'dj'=>"create_timestamp"),
            array( 'db' => "id", 'dt' => 'id', 'dj'=>"id")
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        foreach($result['data'] as $k => $v){
            $result['data'][$k]['start_timestamp']=substr($v['start_timestamp'],0,10);
            $result['data'][$k]['end_timestamp']=substr($v['end_timestamp'],0,10);
            $result['data'][$k]['last_generate_timestamp']=substr($v['last_generate_timestamp'],0,19);
            $result['data'][$k]['next_generate_timestamp']=substr($v['next_generate_timestamp'],0,19);
            $result['data'][$k]['create_timestamp']=substr($v['create_timestamp'],0,19);
        }
        echo json_encode( $result);
    }
    /**
     * Deletes an existing CustomReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete1($id)
    {
        $this->actionDelete($id);
    }
    public function actionDelete($id)
    {
        ScheduledReportLog::deleteAll(['report_id'=>$id]);
        $this->findModel($id)->delete();
        return $this->redirect(['index']);

    }


    /**
     * Finds the ScheduledRepor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduledReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function splitArr($data){
        $j=-1;$y=0;
        foreach($data as $key=>$value){
            if($key != 0){
                $j++;$i=$data[$key-1];
                if($i==($value-1)){
                    $str[$y][]=$value;
                }else{
                    $y++;
                    $str[$y][]=$value;
                }
            }else{
                $y++;
                $str[$y][]=$value;
            }
        }
        return $str;
    }


    public function actionCreate()
    {
        $model = new ScheduledReport();
        //查询所有用户信息
        $usermodel=new User();
        $users=$usermodel->getUser();
        $users_array=array();
        for($i=0;$i<count($users);$i++)
        {
            $users_array[$users[$i]['id']]=$users[$i]['username'];
        }
        if($data = Yii::$app->request->post()){
            $detail=json_decode($data['detail'],1);
            $merge=$data['config'];
            $header_config=json_decode($data['header']);
            $arr = explode("&&",$merge);
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$arr[0],
                    'en-cn'=>null
                )
            );
            $header=array(
                'data_type'=>'schedule_report_header',
                'data'=>array(
                     "max_row"=>$data['max_row'],
                     "max_column"=>$data['max_column'],
                     "start_x"=>$data['start_x'],
                     "start_y"=>$data['start_y'],
                     "data"=>array(

                     )
                )
            );
            $model->name=json_encode($name);
            $model->create_timestamp = date('Y-m-d H:i:s');
            $model->owner = Yii::$app->user->identity->id.'';
            $model->start_timestamp=$data['fromStart'];
            $model->end_timestamp=$data['toEnd'];
            $config=array(
                'data_type'=>'schedule_report_config',
                'data'=>array(
                    'type'=>$arr[1],
                    'row'=>array(),
                    'column'=>array()
                )
            );
            //遍历时间
            for($i=0;$i<count($detail);$i++)
            {
                $row=array(
                    '_id'=>$detail[$i][0],
                    'title'=>$detail[$i][1],
                    'start'=>$detail[$i][2],
                    'end'=>$detail[$i][3],
                    'allDay'=>$detail[$i][4],
                    'description'=>$detail[$i][5],
                    'className'=>array($detail[$i][6][0],$detail[$i][6][1]),
                    'row'=>$detail[$i][7],
                    'category'=>$detail[$i][8],
                    'specical'=>$detail[$i][9],
                );
                $config['data']['row'][$i]=$row;
            }
            //遍历表头配置信息
            foreach($header_config as $k=>$v){
                $header['data']['data'][]=$v;
            }
            $arr_column = explode(";",$arr[2]);
            //遍历公式
            for($i=0;$i<count($arr_column);$i++)
            {
                $one_column=explode("##",$arr_column[$i]);
                $column=array(
                    'name'=>$one_column[0],
                    'formula'=>$one_column[1],
                    'unit'=>$one_column[2],
                    'column'=>$one_column[3],
                );
                $config['data']['column'][$i]=$column;
            }
            $model->config=json_encode($config);
            $model->header=json_encode($header);
            //给auth_users赋值
            if($data['auth_users']=="" || $data['auth_users']==null) {
                $model->auth_users='{'.Yii::$app->user->identity->id.'}';
            }else{
                $model->auth_users='{'.$data['auth_users'].'}';
            }

            if (!$model->save()) {
                var_dump($model->errors);die;
            }else{
                $model->save();
            }
            return $this->redirect(['index']);
        }else{
            $formula =Template::getFormula();
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            return $this->render('create',[
                'model' => $model,
                'unit' => $unit,
                'active'=>'bacnet_show',
                'users_array'=>$users_array,
                'now_user_id'=>Yii::$app->user->identity->id,
                'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelarr = ScheduledReport::find()->where(['id'=>$id])->asArray()->one();
        if($data = Yii::$app->request->post()){
            $detail=json_decode($data['detail'],1);
            $merge=$data['config'];
            $arr = explode("&&",$merge);
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$arr[0],
                    'en-cn'=>null
                )
            );
            $model->name=json_encode($name);
            $model->create_timestamp = date('Y-m-d H:i:s');
            $model->owner = Yii::$app->user->identity->id.'';
            $config=array(
                'data_type'=>'schedule_report_config',
                'data'=>array(
                    'start'=>$data['fromStart'],
                    'end'=>$data['toEnd'],
                    'type'=>$arr[1],
                    'row'=>array(),
                    'column'=>array()
                )
            );
            //遍历时间
            for($i=0;$i<count($detail);$i++)
            {
                $row=array(
                    '_id'=>$detail[$i][0],
                    'title'=>$detail[$i][1],
                    'start'=>$detail[$i][2],
                    'end'=>$detail[$i][3],
                    'allDay'=>$detail[$i][4],
                    'description'=>$detail[$i][5],
                    'className'=>array($detail[$i][6][0],$detail[$i][6][1]),
                    'row'=>$detail[$i][7],
                    'category'=>$detail[$i][8],
                    'specical'=>$detail[$i][9],
                );
                $config['data']['row'][$i]=$row;
            }

            $arr_column = explode(";",$arr[2]);
            //遍历公式
            for($i=0;$i<count($arr_column);$i++)
            {
                $one_column=explode("##",$arr_column[$i]);
                $column=array(
                    'name'=>$one_column[0],
                    'formula'=>$one_column[1],
                    'unit'=>$one_column[2],
                    'column'=>$one_column[3],
                );
                $config['data']['column'][$i]=$column;
            }
            $model->config=json_encode($config);
            if (!$model->save()) {
                var_dump($model->errors);die;
            }else{
                $model->save();
            }
            return $this->redirect(['index']);
        }else{
            $formula =Template::getFormula();
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            return $this->render('update', [
                'model' => $model,
                'modelarr'=>$modelarr,
                'unit' => $unit,
                'active'=>'bacnet_show',
                'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
            ]);
        }
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelLog = new ScheduledReportLogSearch();
        $dataProvider = $searchModelLog->search(Yii::$app->request->getQueryParams(),$id);
        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModelLog,
        ]);
    }

    /**
     * Lists all Data.
     * @return mixed
     */
    public function actionReportTable($id)
    {
        $model_log =new ScheduledReportLog();
        $report_log=$model_log->find()->where(['id'=>$id])->one();
        $t=json_decode($report_log->data,1);
        $model =new ScheduledReportLog();
        $schedule_report=new ScheduledReport();
        $report_header=$schedule_report->find()->select(['header'])->where(['id'=>$report_log->report_id])->asArray()->one();
        $report=$schedule_report->find()->select(['name','start_timestamp','end_timestamp'])
            ->where(['id'=>$report_log->report_id])->asArray()->one();
        $name=json_decode($report['name'],1);
        $datainfo=$model->getData($id);
        $datainfo=$datainfo['data'];
        $datainfo=json_decode($datainfo,1);
        $datainfo=$datainfo['data']; //数据整体部分
        $data=$datainfo['data'];  //报表数据部分
        return $this->render('report_table', [
            'data' => $data,
            'header_c'=>json_encode($report_header),
            'id'=>$id,
            'name'=>$name['data']['zh-cn'],
            'start'=>substr($t['data']['start'],0,10),
            'end'=>substr($t['data']['end'],0,10)
       ]);
    }

    public function actionGetName($point_id)
    {
        $point=Point::find()->select(['name'])->Where(['id' => $point_id])->one();
        $name= MyFunc::DisposeJSON($point->name);
        return $name;
    }

    //获取所有点数据
    public function actionGetData()
    {
        $protocol_id = Yii::$app->request->get('protocol_id');
        $type = Yii::$app->request->get('type');
        if($type == 'point'){
            $con = " is_shield = false ";
        }else{
            $con = " is_shield = true ";
        }

        if($protocol_id){
            $con .= " and protocol_id = $protocol_id ";
        }else{
            if($type == 'point'){
                $con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
            }
        }
       //db用于select后面的字段，dt用于返回数组的键值，dj查询条件字段（如果不想该字段用于查询，则不加dj属性即可)
        $table = 'core.point';
        $primaryKey = 'id';
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "src_name",  'dt' => "src_name" ,'dj'=>"src_name"),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );

        echo json_encode( $result);
    }

    //获取报表数据
    public function actionGetReportData($id)
    {
        //datatable数据格式
        $datatable_data=array(
            'draw'=>1,
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
            'data'=>array(

            )
        );
        //根据report_id获取reportlog表的数据
        $model =new ScheduledReportLog();
        $datainfo=$model->getData($id);
        $datainfo=$datainfo['data'];
        $datainfo=json_decode($datainfo,1);
        $datainfo=$datainfo['data']; //数据整体部分
        $data=$datainfo['data'];  //报表数据部分
        $columnlength=count($data[0]['data']);
        $formulaName=array();

        foreach($data as $k=>$v){
            $value=array();
            for($i=0;$i<$columnlength;$i++){
                $value[]=$v['data'][$i]['value'];
            }
            $tem=array();
            $tem[]=$v['row'];$tem[]=$v['name'];$tem[]=$v['start'];$tem[]=$v['end'];
            foreach($value as $k=>$v){
                $tem[]=$v;
            }
            $datatable_data['data'][]=$tem;
        }
        return json_encode($datatable_data);

    }

    //excel下载
    public function actionDownloadExcel($id)
    {
        $model =new ScheduledReportLog();
        $report=$model->find()->where(['id'=>$id])->one();
        header("Content-type:text/html;charset=utf-8");
        $file_name="report_log".$id.".xlsx";
   //用以解决中文不能显示出来的问题
        $file_name=iconv("utf-8","gb2312",$file_name);
        $file_sub_path=$_SERVER['DOCUMENT_ROOT']."/export"."/report".$report->report_id."/";
        $file_path=$file_sub_path.$file_name;
     //首先要判断给定的文件存在与否
        if(!file_exists($file_path)){
            echo $file_sub_path;
            echo "没有该文件";
            return ;
        }
        $fp=fopen($file_path,"r");
        $file_size=filesize($file_path);
   //下载文件需要用到的头
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition: attachment; filename=".$file_name);
        $buffer=1024;
        $file_count=0;
     //向浏览器返回数据
        while(!feof($fp) && $file_count<$file_size){
            $file_con=fread($fp,$buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        fclose($fp);
    }

    //pdf下载
    public function actionDownloadPdf($id)
    {
        $model =new ScheduledReportLog();
        $report=$model->find()->where(['id'=>$id])->one();
        header("Content-type:text/html;charset=utf-8");
        $file_name="report_log".$id.".pdf";
        //用以解决中文不能显示出来的问题
        $file_name=iconv("utf-8","gb2312",$file_name);
        $file_sub_path=$_SERVER['DOCUMENT_ROOT']."/export"."/report".$report->report_id."/";
        $file_path=$file_sub_path.$file_name;
        //首先要判断给定的文件存在与否
        if(!file_exists($file_path)){
            echo $file_sub_path;
            echo "没有该文件";
            return ;
        }
        $fp=fopen($file_path,"r");
        $file_size=filesize($file_path);
        //下载文件需要用到的头
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition: attachment; filename=".$file_name);
        $buffer=1024;
        $file_count=0;
        //向浏览器返回数据
        while(!feof($fp) && $file_count<$file_size){
            $file_con=fread($fp,$buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        fclose($fp);
    }

    //是否能下载
    public function actionIsdownloadexcel($id){
        $model =new ScheduledReportLog();
        $report_log=$model->find()->where(['id'=>$id])->one();
        if($report_log->xls_export_status=='' || $report_log->xls_export_status==null){
            return "2";
        }else{
            if($report_log->xls_export_status){
                return "0";
            }else{
                return "1";
            }
        }
    }
    //是否能下载
    public function actionIsdownloadpdf($id){
        $model =new ScheduledReportLog();
        $report_log=$model->find()->where(['id'=>$id])->one();
        if(empty($report_log->pdf_export_status)){
            return "2";
        }else{
            if($report_log->pdf_export_status){
                return "0";
            }else{
                return "1";
            }
        }
    }

    //点击报表头部修改对应列的公式
    public function actionUpdateFomular($id,$column){
        $model_log =new ScheduledReportLog();
        $report_id=$model_log->find()->select(['report_id'])->where(['id'=>$id])->asArray()->one();
        $model = $this->findModel($report_id['report_id']);
        $tem=json_decode($model->config,1);
        $column_arr=$tem['data']['column'];
        $data=array(
            "report_id"=>$report_id['report_id'],
            "name"=>'',
            "unit"=>'',
            "column"=>'',
            "formula"=>''
        );
        foreach ($column_arr as $k=>$v ) {
            if($v['column']==$column){
                $data['name']=$v['name'];
                $data['unit']=$v['unit'];
                $data['column']=$v['column'];
                $data['formula']=$v['formula'];

            }
        }
        $formula =Template::getFormula();
        $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
        return $this->render('update_fomular', [
            'id'=>$id,
            'model' => $model,
            'data' => $data,
            'unit' => $unit,
            'formula' =>  MyFunc::SelectDataAddArrayTop($formula['formula'])
        ]);
    }

    //点击表头之后保存新的公式信息
    public function actionSaveNewFormula()
    {
        if($data = Yii::$app->request->post()){
            $config_new=json_decode($data['config'],1);
            $model = $this->findModel($config_new['report_id']);
            $config=json_decode($model->config,1);
            foreach($config['data']['column'] as $k=>$v){
                if($v['column']==$config_new['column']){
                    $config['data']['column'][$k]['name']=$config_new['name'];
                    $config['data']['column'][$k]['unit']=$config_new['unit'];
                    $config['data']['column'][$k]['formula']=$config_new['formula'];
                }
            }
            //修改report中config字段
            $model->config=json_encode($config);
            //修改report中header字段
            $header=json_decode($model->header,1);
            $x=$header['data']['start_x'];
            $y=$header['data']['start_y'];

            foreach($header['data']['data'] as $k=>$v){
                if($v['x']-'0'==(($x-'0')-1)){
                    if(($v["y"]-'0')==(($config_new['column']-'0')-1+($y-'0'))){
                        $header['data']['data'][$k]['text']=$config_new['name'].'('.$config_new['unit'].')';
                    }
                }
            }
            $model->header=json_encode($header);
            $model->save();
            return $this->redirect(['index']);

        }else{
            return $this->redirect(['index']);
        }
    }
}
