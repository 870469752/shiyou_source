<?php

namespace backend\module\scheduled_report;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\scheduled_report\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
