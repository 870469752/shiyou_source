<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
$testdata=array(['Firefox',45.0],
    ['IE',26.8],
    ['Chrome',12.8],
    ['Safari',8.5],
    ['Opera',6.2],
    ['Others',0.7],
);

//echo '<pre>';
//print_r($top);
//die;
?>

<section id="widget-grid" class="">

    <!-- row -->
    <div class="row"  >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >

                <header>


                    <div class="widget-toolbar smart-form" data-toggle="buttons">

                        <button class="btn btn-xs btn-primary" id="submit_button"  >
                            提交
                        </button>

                    </div>



                    <div class="widget-toolbar smart-form" style="width: 300px">

                        <label class="input"> <i class="icon-append fa fa-question-circle"></i>
                            <!-- 分类 隐藏框 -->
                            <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id1'])?>
                            <div class="input-group" id = 'category_tree1' style="width: 100%;">
                                <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name1' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                    <ul id = 'select_category1' class="select2-choices choices1" style = 'min-height: 24px'>
                                    </ul>
                                </div>

                            </div>
                            <div id = 'tree1' style = 'display:none; width:100%'></div>
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Some helpful information</b>
                        </label>

                    </div>


                    <div class="widget-toolbar smart-form">

                        <label class="input"> <i class="icon-append fa fa-question-circle"></i>
                            <input type="text" name="date_time" id="time1" placeholder="Time">
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Some helpful information</b>
                        </label>

                    </div>

                    <!--       dropdown list             -->
                    <div class="widget-toolbar">
                        <?=Html::hiddenInput('time_type', null, ['id' => 'time_type1'])?>
                        <!-- add: non-hidden - to disable auto hide -->
                        <div class="btn-group">
                            <button class="btn dropdown-toggle btn-xs btn-default" id="time_button" data-toggle="dropdown">
                                时间类型 <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu js-status-update pull-right">
                                <li>
                                    <a href="javascript:void(0);" id="mt">Year</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="ag">Month</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="td">Day</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>能源分类 </h2>

                </header>
                <div id="error" style="display: none;color: red;">
                    <h4 align="center">时间不能为空 </h4>
                </div>
                <!--    add  null form  with  post element 建立空表单 之后加入要提交的信息          -->
                <?php $form = MyActiveForm::begin(['method' => 'get','id'=>'testform']) ?>

                <?php MyActiveForm::end(); ?>




                <div id="tabs"  class="col-lg-6 " style="width:100% ">

                    <ul>
                        <?php foreach ($top as $key=>$value) {
                            echo '<li>'
                                .'<a  href="#tabs-'.$key.'">'.$value.'</a>'
                                .'</li>';

                        } ?>
                    </ul>

                    <?php foreach ($top as $key=>$value) {
                        echo  '<div id="tabs-'.$key.'" style="height: 450px" >'

                            .'<div class="tabs-right">'
                            .'<ul class="nav nav-tabs">'
                            .'<li class="active">'
                            .'<a href="#tab1'.$key.'" data-toggle="tab"> <span class="badge bg-color-blue txt-color-white">饼图数据</span></a>'
                            .'</li>'
                            .'<li>'
                            .'<a href="#tab2'.$key.'" data-toggle="tab"> <span class="badge bg-color-blueDark txt-color-white">详细数据</span></a>'
                            .'</li>'
                            .'</ul>'
                            .'<div class="tab-content">'
                            .'<div class="tab-pane active" id="tab1'.$key.'">'
                            .'<div       style="height:350px;width:45%;float:left;">'
                            .'<div id="tabs-'.$key.'-pie"    >'
                            .'</div>'
                            .'</div>'
                            .'<div       style="height:350px;width:45%;float:left;">'
                            .'<div id="tabs-'.$key.'-next-pie"  >'
                            .'</div>'
                            .'</div>'

                            .'</div>'

                            .'<div class="tab-pane" id="tab2'.$key.'">'
                            //.'<div       style="height:350px;width:45%;float:left;">'
                            .'<div id="tabs-'.$key.'-chart"      style="height:350px;float:left;">'
                            .'</div>'
                            //.'</div>'

                            //.'<div       style="height:350px;width:45%;float:left;">'
                            .'<div id="tabs-'.$key.'-next-chart" style="height:350px;float:left;">'
                            .'</div>'
                            //.'</div>'

                            .'</div>'

                            .'</div>'
                            .'</div>'
                            .'</div>';
                    } ?>

                </div>
                <div id="container" style="min-width:700px;height:400px"></div>

            </div>


            <!-- end widget -->
        </article>



    </div>

</section>
<!--</section>-->





<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);



$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);


?>
<script>
    window.onload = function(){
        var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        var _categories = <?= isset($categories) ? $categories : "''"?>;
        var re_query_param=<?= isset($re_query_param) ? $re_query_param : "''"?>;
        var $__type = '日';
        switch(re_query_param['time_type']) {
            case 'day':
                $__type = '日';
                break;
            case 'month':
                $__type = '月';
                break;
            case 'year':
                $__type = '年';
                break;
        }

        if(re_query_param.length!=0){
            $("#time_type1").val(re_query_param['time_type']);
            document.getElementById('time_button').innerHTML=$__type+'<i class="fa fa-caret-down"></i>';
            $("#time1").val(re_query_param['date_time']);
        }

        //初始化tabs窗口
        $('#tabs').tabs();

        initcolumn(170);

        //验证表单  并 提交
        $("#submit_button").click(function(){
            //获取表单信息
            var time_type=$("#time_type1").val();
            var time=$("#time1").val();
            var category_id=$("#category_id1").val();
            //验证表单信息
            console.log(time_type== '');
            console.log(time=='');
            if(time_type!= '' && time!='') {
                //把要传递的信息都加入form表单后提交
                $("#testform").append($("#time_type1"));
                $("#testform").append($("#time1"));
                $("#testform").append($("#category_id1"));

                $("#testform").submit();
            }
            else {
                $("#error").show();
                setTimeout('$("#error").hide()',1000)
                return false;
            }
        })








// calendar month
        $('#mt').click(function() {
            var temp=document.getElementById('time_button').innerHTML;
            console.log(temp);
            $("#time_type1").val('year');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='年  <i class="fa fa-caret-down"></i>';
            timeTypeChange('year', $('#time1'));
        });

        // calendar agenda week
        $('#ag').click(function() {
            $("#time_type1").val('month');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='月  <i class="fa fa-caret-down"></i>';
            timeTypeChange('month', $('#time1'));
        });

        // calendar agenda day
        $('#td').click(function() {
            $("#time_type1").val('day');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='日   <i class="fa fa-caret-down"></i>';
            timeTypeChange('day', $('#time1'));
        });




        /********************************柱状图-start************************************************/

        //highChart('column');
        //highChart('column',[0,1],[0,133]);

        //ajax_data(170);
        //ajax根据分类的id来查找下属数据组成
        function ajax_data(id){
            console.log(id);
            var query_param=JSON.stringify(<?=json_encode($query_param)?>)
            //window.location.href="/category-data/crud/columndata?id="+id+"&query_param="+query_param;
            $.ajax({
                type: "POST",
                url: "/category-data/crud/columndata",
                data: {id:id,query_param:query_param},
                success: function (msg) {
                    console.log(msg);
                    var data=JSON.parse(msg)
                    console.log(data['chart_data']);
                    initcolumn(data);

                }
            });
        }


        function initcolumn(data) {
            console.log('柱状图数据');
            console.log(data);
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ' '
                },
                xAxis: {
                    categories: data['_category']
//                    ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                    //测试数据
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ' '
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                plotOptions: {
                    column: {
                        stacking: 'normal'

                    }
                },
                series: data['chart_data']
            });
        }

        // highchart 代码
        function highChart(element,type,id){
            var data=<?=json_encode($data)?>;
            var _categories = [];
            var chart_data = [];
            console.log(data);
            console.log(id);
            console.log(data[id]);
            console.log(data[id]['detail']);
            if(data[id]['detail'].length!=0) {
                _categories = data[id]['detail']['categories'];
                chart_data = data[id]['detail']['chart_data'];
            }
            console.log(_categories);
            console.log()
            $('#'+element).highcharts({
                chart: {
                    type: type,
                    zoomType: 'x'
                },
                title: {
                    text: $category_name
                },
                subtitle: {
                    text: $_date_time + ' ' + '<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>' + '累计量'
                },
                xAxis: {
                    categories: _categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },

                legend: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1
//                shadow: false
                },

                series: [{
                    name: $category_name,
                    data: chart_data,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }]
            });

        }




        //图形样式切换
        $('.radiobox').click(function(){
            var $_chart_type = $(this).data('value');
            if($_chart_type){
                highChart($_chart_type,_categories,chart_data);
            }
        })


        /********************************柱状图-end************************************************/
        /********************************表格-start************************************************/
//初始化表格
// id为 string id
// 或    数组data数据
        function initGridview_new(element,data){
            chartdata=data[0]['children'];


//    console.log(111);
//        console.log(id);
            var html= '<div id="'+element+'" style="height:350px;width:45%;float:left;" > <table class="table table-bordered table-striped"><thead><tr><th>name</th> <th>value</th> </tr> </thead>'
                +'<tbody>';
            //循环写入
            for(var key in chartdata) {
                //console.log(key+ '  '+chartdata[key])
                html=html+'<tr>'
                    + '<td>'+chartdata[key]['name']+'</td>'
                    + '<td>'+chartdata[key]['value']+'</td>'
                    + '</tr>';
            }
            html=html+'</tbody>'
                +'</table></div>';
            $('#'+element).replaceWith(html);
            $('#tabs').tabs();
        }
        //初始化表格
        // id为 string id
        // 或    数组data数据
        function initGridview(element,id){
            var chartdata;
            if(typeof(id)=='string'){
                var data=<?=json_encode($data)?>;
                chartdata=data[id]['children'];
            }
            else {
                var chartdata=id;
            }

//    console.log(111);
//        console.log(id);
            var html= '<div id="'+element+'" style="height:350px;width:45%;float:left;" > <table class="table table-bordered table-striped"><thead><tr><th>name</th> <th>value</th> </tr> </thead>'
                +'<tbody>';
            //循环写入
            for(var key in chartdata) {
                //console.log(key+ '  '+chartdata[key])
                html=html+'<tr>'
                    + '<td>'+chartdata[key]['name']+'</td>'
                    + '<td>'+chartdata[key]['value']+'</td>'
                    + '</tr>';
            }
            html=html+'</tbody>'
                +'</table></div>';
            $('#'+element).replaceWith(html);
            $('#tabs').tabs();
        }

        /*********************************表格-end***************************************************/




        /******************************饼图-start**************************************/
        var data=<?=json_encode($data)?>;
        console.log(data);
        //ajax_pie(37);

        //ajax_pie('tabs-29',29);
        //ajax_pie('tabs-29-next',37);
        function ajax_pie(element,id){
            //ajax 查询后再显示
            $.ajax({
                type: "POST",
                url: "/category-data/crud/getpoint",
                data: {id: id,data:JSON.stringify(<?=json_encode($data)?>)},
                success: function (msg) {
                    var data=eval("["+msg+"]")[0];
                    console.log(data);
                    //返回false则有下级显示下级
                    if (data['result'] == 'false') {
                        data=data['data'];
                        initGridview_new(element+"-chart",data);
                        var test=piedata_new(data);
                        console.log(test);
                        pie1_new(test,element);
                    }
                    else {
                        //无下级 显示点位信息
                        console.log(111);
                    }
                }
            });

        }

        function piedata_new (data) {
            var colors = Highcharts.getOptions().colors;
            var i=0;
            var rdata = new Array();
            //console.log(data);


            var total = data[0]['value'];
            var twodata = data[0]['children'];

            //判断如果数据都为0 则返回空数组
            if(total==0){
                rdata=[];
            }
            else {
                for (var pie_key in twodata) {
                    var name1 = twodata[pie_key]['name'];
                    a = {
                        name: name1,
                        y: parseFloat(((twodata[pie_key]['value'] / total) * 100).toFixed(2)),
                        id: pie_key,
                        colors: colors[i]
                    }
                    rdata.push(a);
                    i++;
                }
            }

            return rdata;
        }



//  处理相应的id  data[id]数据为一层饼图数据  data为整个能源分类树状数据
        function pie1data(data,id) {

            var colors = Highcharts.getOptions().colors;
            var i=0;
            var rdata = new Array();
//        console.log(id);
            //console.log(data[id]);

            var total = data[id]['value'];
            var twodata = data[id]['children'];

            //判断如果数据都为0 则返回空数组
            if(total==0){
                rdata=[];
            }
            else {
                for (var pie_key in twodata) {
                    var name1 = twodata[pie_key]['name'];
                    a = {
                        name: name1,
                        y: parseFloat(((twodata[pie_key]['value'] / total) * 100).toFixed(2)),
                        id: pie_key,
                        colors: colors[i]
                    }
                    rdata.push(a);
                    i++;
                }
            }

            return rdata;
        }

        // 单层饼图 by id
        function pie1_new(data,element1) {
            var text;
            if(data.length==0){
                text='无数据'
            }

            else text =' ';
            $('#'+element1+"-pie").highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: ' ',
                                onclick: function () {
                                    //console.log('详细信息');
                                    //initview(element1,id);
                                }
                            }, {
                                separator: true
                            }]
                                //自带的到处等功能按钮
                                .concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: text
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true

                    }
                    ,
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                console.log(element1+"-next");
                                console.log(e.point.id);
                                ajax_pie(element1+"-next",e.point.id);
                                ajax_data( e.point.id);
                            }
                        }
                    }
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data: data
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
                }]
            });
        }

        // 单层饼图 by id
        function pie1(data,element1,id) {
            //console.log(data);
            //console.log(element1);
            //console.log(id);
            var id_link=Array();
            id_link.push(id);
            var data=pie1data(data,id);
            //console.log(data);
            var text;
            if(pie1data(data,id).length==0){
                text='无数据'
            }

            else text =' ';
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: ' ',
                                onclick: function () {
                                    console.log('详细信息');
                                    initview(element1,id);
                                }
                            }, {
                                separator: true
                            }]
                                //自带的到处等功能按钮
                                .concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: text
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
//                        dataLabels: {
//                            distance: 40,//数据标签距离边缘的距离值 为负数就越靠近饼图中心
//                            rotation: 0,//数据标签旋转角度
//                            enabled: true,
//                            format: '{point.name}'
//                        }
//                    ,
//                    events: {
//                        click: function(event) {
//                            alert(this.name +' clicked\n'+'highcharts');
//                        }
//                    }
                    }
                    ,
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                //调试用
                                //window.location.href="/category-data/crud/getpoint?id="+e.point.id;
                                //ajax 查询后再显示
                                $.ajax({
                                    type: "POST",
                                    url: "/category-data/crud/getpoint",
                                    data: {id: e.point.id,data:JSON.stringify(<?=json_encode($data)?>)},
                                    success: function (msg) {
                                        console.log(msg);
                                        console.log( e.point.id);
                                        //返回false则有下级显示下级
                                        if (msg == 'false') {
                                            console.log('下级');
                                            //pie1("container12", e.point.id);
                                            pie1(element1+"-next", e.point.id);
                                            //tabs-'.$key.'-chart",
                                            initGridview("tabs-"+id+"-chart"+"-next", pie1data(data,e.point.id));

                                            //initGridview("container21", pie1data(e.point.id));
                                            ajax_data( e.point.id);
                                        }
                                        else {
                                            //无下级 显示点位信息
                                            msg = '[' + msg + ']';
                                            var a = eval(msg);
//                                                 console.log(a[0]);
                                            $('#container12').show;
                                            pie2("container12", pie1data2(a[0]));
                                            pie2(element1+"-next", pie1data2(a[0]));
                                            initGridview("tabs-"+id+"-chart"+"-next",a[0]);
                                            //initGridview(element1+"-next", a[0]);

                                            initGridview("container21", a[0]);
                                            ajax_data( e.point.id);
                                        }
                                    }
                                });
//                                 }

                            }
                        }
                    }
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data: data
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
                }]
            });
        }

        //单层饼图 by data   显示分类下 点位信息
        function pie2(element1,data) {
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: ' ',
                                onclick: function () {
                                    console.log('详细信息');
                                }
                            }, {
                                separator: true
                            }]
                                //自带的到处等功能按钮
                                .concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: ' '
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
//                    dataLabels: {
//                        distance: 40,//数据标签距离边缘的距离值 为负数就越靠近饼图中心
//                        rotation: 0,//数据标签旋转角度
//                        enabled: true,
//                        format: '{point.name}'
//                    }
                    }
                    ,
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                console.log(e.point.id);
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data:data
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
                }]
            });
        }
        /******************************************二层饼状图**********************************************************************/
        //二层饼状图
        function init(element1,id) {
            var data1=pie2data(id);
//            console.log(data1.categorys);
//            console.log(data1.data);
            //二层饼状图
            var colors = Highcharts.getOptions().colors,
            //categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
                categories = data1.categorys,
                name = 'Browser brands',
                data =data1.data;

            //console.log(data);
            // console.log(piedata());
            // Build the data arrays
            var browserData = [];
            var versionsData = [];
            for (var i = 0; i < data.length; i++) {

                // add browser data
                browserData.push({
                    name: categories[i],
                    y: data[i].y,
                    id:data[i].id,
                    color: data[i].color
                });

                // add version data
                for (var j = 0; j < data[i].drilldown.data.length; j++) {
                    var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
                    versionsData.push({
                        name: data[i].drilldown.categories[j],
                        y: data[i].drilldown.data[j],
                        color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                        id:data[i].drilldown.id[j]
                    });
                }
            }



            pie1("container12",178);
            //initGridview("container21",178);
            // Create the chart
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie'
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: ' ',
                                onclick: function () {
                                    console.log('详细信息');
                                    initview(element1,id);
                                }
                            }, {
                                separator: true
                            }]
                            //自带的导出等功能按钮
                            //.concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: ' '
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
                plotOptions: {
                    pie: {
                        shadow: false,
                        center: ['50%', '50%']
                    },
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                //console.log(111);
                                $('#container12').show;
                                console.log(e.point.id);
                                var data= <?=json_encode($data)?>;
                                console.log(e.point.id);
                                console.log(data);
                                //初始化单层饼图
                                pie1("container12",e.point.id);

                                var data= <?=json_encode($data)?>;
                                console.log(e.point.id);
                                //不太好 *****待解决//
                                var gridview_data=data[170]['children'][e.point.id]['children'];
                                initGridview("container21",gridview_data);



                            }
                        }
                    }
                },
                tooltip: {
                    valueSuffix: '%'
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                series: [{
                    name: 'Browsers',
                    data: browserData,
                    size: '60%',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
//                    dataLabels: {
//                        formatter: function () {
//                            return this.y > 5 ? this.point.name : null;
//                        },
//                        color: 'white',
//                        distance: -30
//                    }
                }, {
                    name: 'Versions',
                    data: versionsData,
                    size: '80%',
                    innerSize: '60%',
                    //图例
//                    dataLabels: {
//                        enabled: false
//                    },
//                    showInLegend: true,
                    dataLabels: {
                        formatter: function () {
                            // display only if larger than 1
                            return this.y > 5 ? this.point.name : null;
                            //return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
                        },
                        color: 'black',
                        distance: 30
                    }
                }]
            });

        }


        //二层结构的数据
        function pie2data(id){
            var data= <?=json_encode($data)?>;
            var colors = Highcharts.getOptions().colors;
            var ldata=new Array();
            var total= data[id]['value'];
            var i=0;
            var rdata=new Array();
            var categorys_data=new Array();
            //console.log(total);
            var twodata=data[id]['children'];
            for (var piekey in twodata) {


                var categorys = new Array();
                var data = new Array();
                var id = new Array();
                categorys_data.push(twodata[piekey]['name']);

                var child = twodata[piekey]['children'];
                for (var childkey in child) {
                    var name = child[childkey]['name'];
                    var value = child[childkey]['value'];
                    categorys.push(name);
                    data.push(value);
                    id.push(childkey);

                }
                var name1 = child[childkey]['name'];
                a = {
                    y: parseFloat(((twodata[piekey]['value'] / total) * 100).toFixed(2)),
                    color: colors[i],
                    id: piekey,
                    drilldown: {
                        name: name1,
                        id: id,
                        categories: categorys,
                        data: data,
                        color: colors[i]
                    }
                }
                rdata.push(a);
                i++;
            }
            return {
                data:rdata,
                categorys:categorys_data
            };
        }





        //处理多个  数据为  一层饼图数据
        function pie1data2(data){
            var colors = Highcharts.getOptions().colors;
            var i = 1;
            //合值
            var sum=0;
            var rdata=new Array();
            //console.log(data);
            for(var key in data){
                sum=sum+parseFloat(data[key]['value']);
            }
////            console.log(sum);
//            if(sum==0){
//                rdata=[];
//            }
//            else {
            for (var key in data) {
                a = {
                    name: data[key]['name'],
                    y: (data[key]['value'] / sum) * 100,
                    id: key,
                    colors: colors[i]
                }
                rdata.push(a);
                i++;
            }
//            }
            return rdata;
        }


        //id是否在data的一层数组键值中 返回ture+键值 false+null
        function Isin(id,data){
            var flag=false;
            var key=null;
            for(var key in data){
                if(key==id) {
                    flag = true || flag;
                    key=key;
                }
            }
            return {
                flag:falg,
                key:key
            };
        }

        /******************************双层饼图-end*******************************************/


        /******************************饼图-end*******************************************/




        //初始化各个tabs-id div
        //打印 执行函数 0-0
        //170电 为双层饼图初始化init()
        //其他  为单层饼图初始化pie1()
        //initGridview() 表格===待替换
        //highChart()


        <?php foreach($top as $key=>$value){
        //绑定点击事件
            //echo'ajax_pie("tabs-'.$key.'-pie","'.$key.'");';
                echo'ajax_pie("tabs-'.$key.'","'.$key.'");';
    //
    //                 echo'pie1('.json_encode($data).',"tabs-'.$key.'-pie","'.$key.'");';
    //                echo'initGridview("tabs-'.$key.'-chart","'.$key.'");';
    //                 foreach ($data[$key]['children'] as $data_children_key=>$child_value) {
    //                        $id=$data_children_key;
    //                        break;
    //                 }
    //                  echo'pie1('.json_encode($data).',"tabs-'.$key.'-pie-next","'.$id.'");';


        }
        ?>
        //ajax_pie
        //highChart('container11','column',170);
//    pie2("test",pie1data(111));



        /*****************************************************************************************/
        //TODO  找树状图路径  ***待完成
        /*****************************************************************************************/




        initfancytree(<?=$tree_json?>,'');

        initfancytree(<?=$tree_json?>,'1');
        //初始化树形下拉列表框  data为树状数据源  id 为元素后缀名
        function initfancytree(data,id){
            //fancytree 树状多选框初始化
            var $current_keys = [];
            var $count = 0;

            function delete_all_category() {
                $('.choices'+id+' li').each(function(){
                    if($(this).data('type') != 'plus'){
                        $(this).remove();
                    }
                })
            }
            console.log("#tree"+id);

            $("#tree"+id).fancytree({
                extensions: ["glyph", "edit", "wide"],
                checkbox: true,
                selectMode: 1,
                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                glyph: {
                    map: {
                        doc: "glyphicon glyphicon-file",
                        docOpen: "glyphicon glyphicon-file",
                        checkbox: "glyphicon glyphicon-unchecked",
                        checkboxSelected: "glyphicon glyphicon-check",
                        checkboxUnknown: "glyphicon glyphicon-share",
                        error: "glyphicon glyphicon-warning-sign",
                        expanderClosed: "glyphicon glyphicon-plus-sign",
                        expanderLazy: "glyphicon glyphicon-plus-sign",
                        // expanderLazy: "glyphicon glyphicon-expand",
                        expanderOpen: "glyphicon glyphicon-minus-sign",
                        // expanderOpen: "glyphicon glyphicon-collapse-down",
                        folder: "glyphicon glyphicon-folder-close",
                        folderOpen: "glyphicon glyphicon-folder-open",
                        loading: "glyphicon glyphicon-refresh"
                        // loading: "icon-spinner icon-spin"
                    }
                },
                wide: {
                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                },
                source:data,
                select: function(event, data) {
                    // Get a list of all selected nodes, and convert to a key array:
                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                        return node.key;
                    });
                    console.log('当前所有分类' + '_________________________' + $sel_keys);
                    if ($sel_keys.length >= $current_keys.length) {
                        for (var i = 0; i < $sel_keys.length; i++) {

                            if($.inArray($sel_keys[i], $current_keys) === -1) {
                                console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                console.log('上次所选的分类 --- ' + $current_keys);
                                console.log('当前选择分类 --- ' + $sel_keys[i]);
                                var $current_key = $sel_keys[i];
                                var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                for (var j = 0; j < $_other_sibling.length; j++) {
                                    console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                    $_other_sibling[j].setSelected(false);
                                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                        return node.key;
                                    });
                                }
                            }

                        }
                    }

                    //获取所有选中的分类将其加入到选择框中
                    var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                        console.log(node.key + ' ---- ' + node.title);
                        var $_html = '';
                        $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                            "<div>" + node.title + "</div>" +
                            "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                        return $_html;
                    });
                    //将分类选择框中的分类全部清除
                    delete_all_category();
                    $('#select_category'+id).append($_html);
                    //将选择的分类id放入分类隐藏框中
                    $('#category_id'+id).val($sel_keys);
                    //将最新所选的分类集合跟新到$current_keys 里
                    $current_keys = $sel_keys;
                    console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                }
            });

            $('#_clear'+id).click(function () {
                delete_all_category();
                $("#tree").fancytree();
            });

            $('#category_tree'+id).click(function() {
                $('#tree'+id).toggle();
            });
            $('#tree'+id).mouseleave(function() {
                $('#tree'+id).hide();
            });
        }
        //提交验证
        $('#_sub').click(function() {

        });
        $('#energy_cateogry').click(function() {
            $('#tree1').toggle();
        });

//    $('#tree1').mouseleave(function() {
//        $('#tree1').hide();
//    });
    }
</script>