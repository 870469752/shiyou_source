<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:36
 */

use yii\helpers\Html;
use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .ms-container{
        background: transparent url('/img/multi-select/switch.png') no-repeat 50% 50%;
        width: 900px;
    }
    .custom-header{
        text-align: center;
        padding: 3px;
        background: #000;
        color: #fff;
    }
    ul.fancytree-container {
        max-height: 200px;
        width: 100%;
    }
</style>

<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>



    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?=Yii::t('app', 'Point Batch')?></h2>
            <div class="jarviswidget-ctrls">

            </div>
        </header>



        <!-- 参数主体 start-->
        <div id = '_loop' style = 'border:2px solid #D5D5D5;padding-bottom:15px'>
            <div style = 'padding:0px 7px 15px 7px'>
                <?php $form = MyActiveForm::begin(['method' => 'get', 'id' => '_ws']) ?>
                <fieldset>
                    <legend>搜索条件</legend>
                    <div class="form-group">
                        <div class="col-md-2" style="margin-left:89px;">
                            <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Point Name'), 'class' => 'form-control'])?>
                        </div>
<!--                        <div class="col-md-1"></div>-->
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('energy_category_id', isset($category_id)?$category_id:null, $category_tree
                                , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('location_category_id', isset($category_id)?$category_id:null, $location_tree
                                , ['placeholder' => Yii::t('app', 'Choose Location'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
<!--                        <div class="col-md-1"></div>-->

                        <div class="col-md-2" style="display:none;">
                            <?=Html::dropDownList('protocol_id', isset($protocol_id)?$protocol_id:null, ['' => '',
                                ''=>'所有点位',
                                1 => Yii::t('app', 'BACnet Point'),
                                4 => Yii::t('app', 'Calculate Point'),
                                7 => Yii::t('app', 'Simulation Point'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Point Type'), 'id' => 'point_location_demo'])?>
                        </div>

                    </div>
                    <!--提交-->
                    <div class="form-actions" style = 'float:right;padding:0px;margin:-16px 130px -20px 39px;'>
                        <a id="_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                        <?=Yii::t('app', 'Search')?>
                        </a>
                    </div>
                </fieldset>
            </div>

            <?php MyActiveForm::end(); ?>
        </div>


        <div class="widget-body" style="display: inline-block;">

            <!-- <?php //$form = ActiveForm::begin(['method' => 'get']); ?> -->
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>

            <?= Html::hiddenInput('point_ids', '', ['id' => 'point_selected'])?>

                <fieldset style="display:none;">

                    <!-- widget div-->
                    <div class = 'form-group'>
                        <label class="col-sm-1 control-label"><?=Yii::t('app', 'Bacth Filter')?></label>
                        <div class = 'col-sm-6' >
                            <?=Html::dropDownList('point_ids', '', ['' => '',
                            ],
                                ['id' => 'point_select', 'multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Point Type')])?>
                        </div>
                    </div>

                    <!--    批量修改 点位的一些属性      -->
                    <div class = 'form-group' style = 'margin-top:230px'>

                    </div>


                    <div style="display: inline-block;">
                    </div>

                </fieldset>







    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
        <!-- widget options:
        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
    
        data-widget-colorbutton="false"
        data-widget-editbutton="false"
        data-widget-togglebutton="false"
        data-widget-deletebutton="false"
        data-widget-fullscreenbutton="false"
        data-widget-custombutton="false"
        data-widget-collapsed="true"
        data-widget-sortable="false"
    
        -->
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2><?=Yii::t('app', 'Point Search')?></h2>
    
        </header>
    
        <!-- widget div-->
        <div>
    
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
    
            </div>
            <!-- end widget edit box -->
    
            <!-- widget content -->
            <div class="widget-body no-padding">
    
                <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>                         
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyData">
                       
                    </tbody>
                </table>
    
            </div>
            <!-- end widget content -->
    
        </div>
        <!-- end widget div -->
    
    </div>
    <!-- end widget -->





    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
        <!-- widget options:
        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
    
        data-widget-colorbutton="false"
        data-widget-editbutton="false"
        data-widget-togglebutton="false"
        data-widget-deletebutton="false"
        data-widget-fullscreenbutton="false"
        data-widget-custombutton="false"
        data-widget-collapsed="true"
        data-widget-sortable="false"
    
        -->
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2><?=Yii::t('app', 'Point Search')?></h2>
    
        </header>

        <!-- widget div-->
        <div>
        
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
        
            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body no-padding">
            
                <table id="selected_datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>                         
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>
                        </tr>
                    </thead>
                    <tbody id="tbodySelected">
                       
                    </tbody>
                </table>
            
            </div>
            <!-- end widget content -->
            
    </div>
    <!-- end widget div -->

</div>
<!-- end widget -->

             

            <div class="form-actions">

                <?= Html::submitButton(Yii::t('app', 'Submit') , ['class' => 'btn btn-primary', 'id' => '_sub']) ?>

                <div style="">
                    <div style = 'padding-left:12px;margin-right:30px;margin-bottom:30px;color:red;text-align: left; display:none' id = '_error_log'></div>
                </div>

            </div>

            <!-- <?php //ActiveForm::end(); ?> -->
            <?php MyActiveForm::end(); ?>
<!--            <button id="checkAllBtn" class="button">CheckAll</button>-->
        </div>
    </div>
    <!-- 图表内容 end-->
</section>
<!-- html 区域 end-->
<?php



$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);




$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);

$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);




$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>


<!-- js 脚本区域 start-->
<script>
    window.onload = function(){

        $(document).ready(function(){

            var $__type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
            var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;


            var $current_keys = [];
            var $count = 0;

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            /* TABLETOOLS */
            var datable = $('#datatable_tabletools').dataTable({
                "sDom": "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "oTableTools": {
                },
                "autoWidth" : true,
                "bPaginate":true,
                "sPaginationType":"full_numbers",
                "iDisplay":10,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_tabletools) {
                        responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_tabletools.respond();
                }
            });

            var datable2 = $('#selected_datatable_tabletools').dataTable({
                "sDom": "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "oTableTools": {
                },
                "autoWidth" : true,
                "bPaginate":true,
                "sPaginationType":"full_numbers",
                "iDisplay":10,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_tabletools) {
                        responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_tabletools.respond();
                }
            });



            $('#point_select').multiSelect({
                selectableHeader: "<div class='custom-header'>搜索结果</div>",
                selectionHeader: "<div class='custom-header'>已选点位</div>",
                keepOrder: true
            });
            $('#point_select').multiSelect('deselect_all');

            $("#checkAllBtn").click(function() {
                console.log(111);
                $("#point_select").multiSelect("select_all");
            });
            //ajax提交
            $('#all_select').click(function(){
                console.log(111);
                $('#point_select').multiSelect('select_all');
            });

    //        timeTypeChange('year', $('#_time'));
            function regenerateTable(data, id, exeparam)
            {
                    //动态添加元素
                    // $('#point_select').multiSelect('addOption', data);
                    var trstr = '<tbody id="'+id+'">';
                    for (var i in data) {
                        switch (data[i]['value_type']) {
                            case 0:
                                if (data[i]['unit'] == null) {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }


                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                } else {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></button></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                }
                                
                                break;
                            case 1:
                                if (data[i]['unit'] == null) {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                } else {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                }
                                break;
                            case 2:
                                if (data[i]['unit'] == null) {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                } else {
                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                }
                                break;
                            case 3:

                                if (data[i]['unit'] == null) {

                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }

                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                } else {

                                    switch (exeparam){
                                        case 'exeselect':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                        case 'exedel':
                                            trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td><td><label class=""><button onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" ><span class=""></span></button></label></td></tr>';
                                        break;
                                    }
                                    // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                                }

                                break;
                        }
                       
                    }

                     trstr += '</table>';

                     switch (exeparam){
                        case 'exeselect':
                            //clear the table tbody content and set the new data
                            datable.fnClearTable();
                            id = '#'+id;
                            $(id).replaceWith(trstr);

                            //recall the method dataTable() to regenerate the table
                            $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});
                        break;
                        case 'exedel':
                            //clear the table tbody content and set the new data
                            datable2.fnClearTable();
                            id = '#'+id;
                            $(id).replaceWith(trstr);

                            //recall the method dataTable() to regenerate the table
                            $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});

                        break;
                     }

            }

            $('#_submit').click(function() {
                var $url = '/point-batch/crud/ajax-find-point-specific?' + $('#_ws').serialize();
                console.log($url);
                $.get($url, function(data) {
                    GlobalData = data;
                    console.log(data);
                    if(data) {
                        
                        //动态添加表格数据
                        regenerateTable(data, 'tbodyData', 'exeselect');

                    }
                }, 'json');
            });


            var selectedPointContainer = [];
            window.exedel = function(data){
                for (var i in selectedPointContainer) {
                    if (selectedPointContainer[i]['pointId'] == data.id) {
                        GlobalData.push(selectedPointContainer[i]);
                        selectedPointContainer.splice(i, 1);
                    }
                }
                regenerateTable(GlobalData, 'tbodyData', 'exeselect');
                regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');

            }


            window.exeselect = function(data){
                console.log(selectedPointContainer);

                if (selectedPointContainer.length != 0) {
                    for (var i in selectedPointContainer) {
                        if (data.id == selectedPointContainer[i]['pointId']) {
                            alert('已经存在该点位');
                            return false;
                        }
                    }
                }


                for (var i in GlobalData) {
                    if (data.id == GlobalData[i]['pointId']) {
                        selectedPointContainer.push(GlobalData[i]);
                        GlobalData.splice(i, 1);
                    }
                }
                regenerateTable(GlobalData, 'tbodyData', 'exeselect');
                regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');
            }



            //初始化tabs窗口
            $('#tabs').tabs();
            //初始化accordion窗口
            var accordionIcons = {
                header: "fa fa-plus",    // custom icon class
                activeHeader: "fa fa-minus" // custom icon class
            };
            $(".accordion").accordion({
                autoHeight: false,
                heightStyle: "content",
                collapsible: true,
                animate: 300,
                icons: accordionIcons,
                header: "h4",
            })

            //提交时候，对表单进行验证
            $("#_sub").click(function () {
                //获取所有 需要选择的参数
                var time_type = $('#time_type').val();
                var time = $('#_time').val();
                var data_value = [];
                var point_ids = [];
    //            console.log($('#point_select option:checked'));
                $('#point_select option:checked').each(function () {
    //                console.log('dede');
                    data_value.push($(this).attr('value'));
                });

                $('#selected_datatable_tabletools button').each(function (i, n) {
                    // console.log('#tbodySelected');
                    console.log(i);
                    console.log(n.id);
                    point_ids.push(n.id);
                })

                // return false;
               console.log(data_value);
               // return false;
                var $error_infor = '';
                var $error_signal = 0;

                if (point_ids.length == 0) {
                    $error_infor = '请选择点位';
                    $error_signal = 1;
                }

                // if (data_value.length == 0) {
                //     $error_infor = '请选择点位';
                //     $error_signal = 1;
                // }
                // if (!time) {
                //     $error_infor = '时间不能为空';
                //     $error_signal = 1;
                // }

                // if (!time_type) {
                //     $error_infor = '时间类型不能为空';
                //     $error_signal = 1;
                // }
                if ($error_signal) {
                    $('#_error_log').text($error_infor);
                    $('#_error_log').show();
                    return false;
                }

                $('#point_selected').val(point_ids);

            });

            $("#time_type").change(function () {
                _type = $(this).val();
                timeTypeChange(_type, $('#_time'));
            });
            $("#time_type").trigger('change');
            $('#_time').val($_date_time);

            $('#tabs').tabs();
            $('#tabs2').tabs();

        });


    }
</script>
<!-- js 脚本区域 end-->