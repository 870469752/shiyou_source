<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
$testdata=array(['Firefox',45.0],
    ['IE',26.8],
    ['Chrome',12.8],
    ['Safari',8.5],
    ['Opera',6.2],
    ['Others',0.7],
);

?>

<section id="widget-grid" class="">

    <article class="col-sm-12 col-md-12 col-lg-12" style="padding:0px !important">

        <!-- content  图表内容 start-->
        <div style="padding:0px !important" class="jarviswidget jarviswidget-color-blueDark _jarviswidget"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
             data-widget-Collapse="false"
             data-widget-custom="false"
             data-widget-togglebutton="false">

            <header>

                <div class="jarviswidget-ctrls">
                    <a style = 'display:none;' href = '<?//=Url::toRoute('export-energy-calculate').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
<!--                        <i class="fa fa-file-excel-o"></i>-->
                    </a>
                    <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                        <i class="fa fa-bar-chart-o"></i>
                    </a>

                </div>
                <span class="widget-icon">
                     <i class="fa fa-table"></i>
                </span>
                <h2><?=isset($category_name) ? $category_name : ' '?></h2>
                <h2>实时数据</h2>


            </header>
            <!--         widget div-->

            <div class="no-padding">

                <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                    <div class="" >
                        <label class="radio radio-inline"  style = 'margin-top:0px;margin-left:12px;'>
                            <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>
                            <span>曲线图</span>
                        </label>
                        <label class="radio radio-inline" style = 'margin-top:0px'>
                            <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>
                            <span>柱状图</span>
                        </label>
                        <label class="radio radio-inline" style = 'margin-top:0px'>
                            <input type="radio" class="radiobox" name="style-0a" data-value = 'area'>
                            <span>堆栈图</span>
                        </label>
                    </div>
                    <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
                    <?=Html::hiddenInput('compare_type','0')?>
                    <?=Html::hiddenInput('parent_category_ids', isset($parent_category_id) ? $parent_category_id : '1')?>

                    <div class="col-md-2" style="width:104px !important;margin-left:900px;">
                        <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                            ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                    </div>

                    <div class="col-md-3" style="width:130px;">
                        <?=Html::textInput('date_time', isset($date_time)?$date_time:date('Y-m',time()),  ['class' => 'form-control', 'placeholder' => $date_time, 'id' => '_time'])?>
                    </div>

                    <div class="col-md-3" style="width:230px;">
                        <!-- 分类 隐藏框 -->
                        <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id'])?>
                        <div class="input-group" id = 'category_tree' style="width:230px!important;">
                            <div rel="tooltip" placeholder="<?= isset($categoriesName)?json_decode($categoriesName):"能源分类选择(必选)";?>" id = 'category_name' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                <ul id = 'select_category' class="select2-choices" style = 'min-height: 30px'>
                                </ul>
                            </div>
                            <div id = "tree" style = 'display:none;position:absolute;width:100%'></div>
                        </div>
                    </div>

                    <div style="float:right;margin-bottom:2px;">
                        <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                    </div>

                    <?php MyActiveForm::end(); ?>

                </div>

                <div id="container_chart" style="height: 600px;"></div>

                <!--container_table start-->
                <div id="container_table" style="">
                    <div id="_table"></div>
                </div>
                <!--container_table end-->

            </div>

        </div>
        <!-------     图表内容 end-->

    </article>

</section>
<!--</section>-->

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>
<script>
    window.onload = function(){
        function json2array(json){
            var temp = [];
            var keys = Object.keys(json);
            keys.forEach(function(key){
                temp.push([key,json[key]]);
            });
            return temp;
        }

        function getArray(object){
            var array = [];
            for(var key in object){
                var item = object[key];
                array[parseInt(key)] = (typeof(item) == "object")?getArray(item):item;
            }
            return array;
        }

        var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
        var categoriesName = <?= isset($categoriesName) ? $categoriesName : []?>;
        var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
        var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
        var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var count = <?= isset($count) ?$count :null?>;
        var $display_type = <?= isset($display_type) ? "'" .$display_type ."'" : "''"?>;
        var currentTotal = <?= isset($current_total)?$current_total:0?>;
        var versusTotal = <?= isset($versus_total)?$versus_total:0?>;
        var chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        var query_param = <?= isset($query_param) ? json_encode($query_param) : "''"?>;
        var _categories = <?= isset($categories) ? $categories : "''"?>;

        var $__type = '日';
        switch($_type) {
            case 'day':
                $__type = '日';
                break;
            case 'month':
                $__type = '月';
                break;
            case 'year':
                $__type = '年';
                break;
        }
        //初始化tabs窗口
        $('#tabs').tabs();
        //初始化accordion窗口
        var accordionIcons = {
            header: "fa fa-plus",    // custom icon class
            activeHeader: "fa fa-minus" // custom icon class
        };
        $(".accordion").accordion({
            autoHeight : false,
            heightStyle : "content",
            collapsible : true,
            animate : 300,
            icons: accordionIcons,
            header : "h4"
        });

        //对表单进行验证
        $("#_submit").click(function(){
            //获取所有 需要选择的参数
            var time_type = $('#time_type').val();
            var time = $('#_time').val();
            var data_value = $('#category_id').attr('value');
//            alert(data_value);
            var $error_infor = '';
            var $error_signal = 0;
            if(!data_value){
                $error_infor = '请选择分类类型';
                $error_signal = 1;
            }
            if(!time){
                $error_infor = '时间不能为空';
                $error_signal = 1;
            }

            if(!time_type){
                $error_infor = '时间类型不能为空';
                $error_signal = 1;
            }
            if($error_signal){
                $('#_error_log').text($error_infor);
                $('#_error_log').show();
                return false;
            }

        });

        $("#time_type").change(function (){
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });
        $("#time_type").trigger('change');
        $('#_time').val($_date_time);

        $('#tabs').tabs();
        $('#tabs2').tabs();

        /********************************表格-start************************************************/
        //初始化表格
        // id为 string id
        // 或    数组data数据
        function initGridview(element,id){
            var chartdata;
            if(typeof(id)=='string'){
                var data=<?=json_encode($data)?>;
                chartdata=data[id]['children'];
            }
            else {
                var chartdata=id;
            }

            var html= '<div id="'+element+'" style="height: 400px" > <table class="table table-bordered table-striped"><thead><tr><th>name</th> <th>value</th> </tr> </thead>'
                +'<tbody>';
            //循环写入
            for(var key in chartdata) {
                //console.log(key+ '  '+chartdata[key])
                html=html+'<tr>'
                    + '<td>'+chartdata[key]['name']+'</td>'
                    + '<td>'+chartdata[key]['value']+'</td>'
                    + '</tr>';
            }
            html=html+'</tbody>'
                +'</table></div>';
            $('#'+element).replaceWith(html);
            $('#tabs').tabs();
        }

        /*********************************表格-end***************************************************/

        /*****************************************************************************************/
        //TODO  找树状图路径  ***待完成
        function Findpath(id) {
            var data = <?=json_encode($data)?>;
            var path = new Array();
            var flag = Isin(id, data);
            //如果不在 往下遍历
            if (!Isin(id, data).flag) {
                for (var key in data) {
                    //记录路径
                    path.push(key);
                    if (!Isin(id, data[key]).flag) {

                    }

                }

            }
        }
        /*****************************************************************************************/

        //fancytree 树状多选框初始化
        var $current_keys = [];
        var $count = 0;

        function delete_all_category() {
            $('#select_category li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        }

        function delete_all_location() {
            $('#select_location li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        }

        /**
         * select the energy type according to the energy
         * pzzrudlf pzzrudlf@gmail.com
         */
        $("#tree").fancytree({
            extensions: ["glyph", "edit", "wide"],
            checkbox: true,
            selectMode: 1,
            toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
            glyph: {
                map: {
                    doc: "glyphicon glyphicon-file",
                    docOpen: "glyphicon glyphicon-file",
                    checkbox: "glyphicon glyphicon-unchecked",
                    checkboxSelected: "glyphicon glyphicon-check",
                    checkboxUnknown: "glyphicon glyphicon-share",
                    error: "glyphicon glyphicon-warning-sign",
                    expanderClosed: "glyphicon glyphicon-plus-sign",
                    expanderLazy: "glyphicon glyphicon-plus-sign",
                    // expanderLazy: "glyphicon glyphicon-expand",
                    expanderOpen: "glyphicon glyphicon-minus-sign",
                    // expanderOpen: "glyphicon glyphicon-collapse-down",
                    folder: "glyphicon glyphicon-folder-close",
                    folderOpen: "glyphicon glyphicon-folder-open",
                    loading: "glyphicon glyphicon-refresh"
                    // loading: "icon-spinner icon-spin"
                }
            },
            wide: {
                iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
            },
            source:<?=$tree_json;?>,
            select: function(event, data) {
                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                    return node.key;
                });
                console.log('sel_keys');
                console.log($sel_keys);
                console.log('sel_keys');
                if ($sel_keys.length >= $current_keys.length) {
                    for (var i = 0; i < $sel_keys.length; i++) {

                        if($.inArray($sel_keys[i], $current_keys) === -1) {
                            var $current_key = $sel_keys[i];
                            var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                            for (var j = 0; j < $_other_sibling.length; j++) {
                                $_other_sibling[j].setSelected(false);
                                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                    return node.key;
                                });
                            }
                        }

                    }
                }

                //获取所有选中的分类将其加入到选择框中
                var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                    var $_html = '';
                    $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                        "<div>" + node.title + "</div>" +
                        "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                        "</li>";
                    return $_html;
                });
                //将分类选择框中的分类全部清除
                delete_all_category();
                $('#select_category').append($_html);
                //将选择的分类id放入分类隐藏框中
                $('#category_id').val($sel_keys);
                //将最新所选的分类集合跟新到$current_keys 里
                $current_keys = $sel_keys;
//                console.log($current_keys + '@@@' + $count++);
            }
        });
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //pzzrudlf@gamil.com start
        //energy type
        $('#category_tree').click(function() {
            $('#tree').toggle();
        });
        $('#tree').mouseleave(function() {
            $(this).hide();
        });
        //提交验证
        $('#_sub').click(function() {

        });
        //pzzrudlf@gmail.com end

        //pzzrudlf@gmail.com start
        $xAix = [];
        if ($_type == 'year') {
            $xAix = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
        } else if ($_type == 'month') {
            for (var i=1; i<count+1; i++) {
                $xAix.push(i);
            }
        } else if ($_type == 'day') {
            $xAix = ['00:00','1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
        }

        var pie = [
            {
                type: 'pie',
                name: categoriesName+'总量',
                data: [{
                    name: query_param['date_time'],//对比年份
                    y: currentTotal,//年总量 || 月总量 || 日总量
                    color: Highcharts.getOptions().colors[0]//2015年的color
                }, {
                    name: query_param['compare_date_time'],//对比年份
                    y: versusTotal,//年总量 || 月总量 || 日总量
                    color: Highcharts.getOptions().colors[1]//2014年的color
                }],
                center: [60,50],
                size: 100,
                showInlegend: false,
                dataLabels: {
                    enabled: false
                }
            }
        ];

        //assemble the total array
        var total = chart_data.concat(pie);
        // 调用画图方法
        highChart('column', total);
        // highchart 代码
        function highChart(type, chart_data){

                                $('#container_chart').highcharts({

                                    chart: {
                                        type: type,
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: categoriesName
                                    },
                                    xAxis: {
                                        categories: $xAix//分类名字
                                    },
                                    labels: {

                                        items: [{
                                            html:'总量占比',
                                            style: {
                                                left: '55px',
                                                top: '0px',
                                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                            }
                                        }]
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: '使用量'
                                        },
                                        stackLabels: {
                                            enabled: false,
                                            style: {
                                                fontWeight: 'bold',
                                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                            }
                                        }
                                    },
                                    plotOptions: {
                                        area: {
                                            stacking: 'normal',
                                            lineColor: '#666666',
                                            lineWidth: 1,
                                            marker: {
                                                lineWidth: 1,
                                                lineColor: '#666666'
                                            }
                                        }
                                    },
                                    legend: {
                                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                        borderColor: '#CCC',
                                        borderWidth: 1
                                    },
                                    credits: false,
                                    series:chart_data
                                })
        }

        //pzzrudlf@gmail.com end

        //图标样式切换 开始
        $('.radiobox').click(function(){
            var $_chart_type = $(this).data('value');
            if($_chart_type == 'area'){
                var chart_data_temp = new Array();
                chart_data_temp = [
                    {color:total[0]['color'],data:[],name:total[0]['name']},
                    {color:total[1]['color'],data:[],name:total[1]['name']},
                    total[2]
                ];
                var temp_total = 0;
                for (var i in total[0]['data']) {
                    var mid = total[0]['data'][i];
                    temp_total = temp_total + mid;
                    chart_data_temp[0]['data'][i] = temp_total;
                }
                temp_total = 0;
                for (var i in total[1]['data']) {
                    var mid = total[1]['data'][i];
                    temp_total = temp_total + mid;
                    chart_data_temp[1]['data'][i] = temp_total;
                }
                highChart($_chart_type, chart_data_temp);
            } else {
                highChart($_chart_type, total);
            }
        })

        //图标样式切换 结束

        //pzzrudlf@gmail.com start
        // 图、表切换

        $('#_chart_switch').click(function(){
            var $_chart_type = $(this).data('type');
            if($_chart_type == 'chart'){
                $('#_loop').hide();
                $('#chart_type').hide();
                $('#container_chart').hide();
                $('#container_table').show();
                $('#_save').show();
                showTable(total);
                $(this).data('type', 'table');
            }else{
                $('#_loop').show();
                $('#chart_type').show();
                $('#container_chart').show();
                $('#container_table').hide();
                $('#_save').hide();
                $('#_table').empty();
                $(this).data('type', 'chart');
            }
        });

        //pzzrudlf@gmail.com end


        //generate the table start

        function showTable(total) {
//            alert(total);
//            console.log(total);
            var html = '';

            var html = '<table class="table table-striped table=bordered table-hover table-middle"><caption>' +categoriesName+
                '</caption><thead><tr><th data-hide="phone,tablet">时间</th><th data-hide="phone,tablet">数值</th></tr></thead>'+
                '<tbody>';

            for (var i in total) {
                var _name = total[i]['name'];
                var temp = total[i]['data'];
                if (i == 0 ) {//current data
                    for (var j in temp) {

                        if ($_type == 'year') {
                            j++;
                            html += '<tr><td>'+_name+'-'+j+'</td>';
                            j--;
                            html +='<td>'+temp[j]+'</td></tr>';
                        } else if ($_type == 'month') {
                            j++;
                            html += '<tr><td>'+_name+'-'+j+'</td>';
                            j--;
                            html +='<td>'+temp[j]+'</td></tr>';
                        } else if ($_type == 'day') {
                            html += '<tr><td>'+_name+'-'+j+':00'+'</td><td>'+temp[j]+'</td></tr>';
                        }


                    }
                }

                if (i == 1 ) {//versus data
                    for (var j in temp) {

                        if ($_type == 'year') {
                            j++;
                            html += '<tr><td>'+_name+'-'+j+'</td>';
                            j--;
                            html +='<td>'+temp[j]+'</td></tr>';
                        } else if ($_type == 'month') {
                            j++;
                            html += '<tr><td>'+_name+'-'+j+'</td>';
                            j--;
                            html +='<td>'+temp[j]+'</td></tr>';
                        } else if ($_type == 'day') {
                            html += '<tr><td>'+_name+'-'+j+':00'+'</td><td>'+temp[j]+'</td></tr>';
                        }


                    }
                }

            }

            html += '</tbody></table>';

            console.log(html);

            $('#_table').append(html);
        }

        //generate the table end

    }
</script>