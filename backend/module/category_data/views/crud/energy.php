<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
?>


<div id="content" xmlns="http://www.w3.org/1999/html">




<section>
	<hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>
	<div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
		<div id="drag" style="position:absolute;z-index:110;width: 220px" draggable="true">
			<div class="jarviswidget jarviswidget-color-blueDark"
				 data-widget-deletebutton="false"
				 data-widget-editbutton="false"
				 data-widget-colorbutton="false"
				 data-widget-fullscreenbutton='false'>

			</div>
		</div>
		<?php $form = MyActiveForm::begin(['method' => 'get']) ?>

		<fieldset>
			<legend>参数设定</legend>
			<div class="form-group">
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
						'year' => Yii::t('app', 'Year'),
						'quarter'=>yii::t('app','Quarter'),
						'month' => Yii::t('app', 'Month'),
						'day' => Yii::t('app', 'Day'),
					],
						['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<?=Html::textInput('date_time', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'placeholder' => '选择时间', 'id' => '_time'])?>
				</div>
				<!-- 查看的 分类id-->
			</div>

		</fieldset>

	<!--提交-->
	<div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
		<div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
		<?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
	</div>
	<?php MyActiveForm::end(); ?>
	</div>
	<div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>




	<div class="jarviswidget jarviswidget-color-blueDark _jarviswidget"
		 data-widget-deletebutton="false"
		 data-widget-editbutton="false"
		 data-widget-colorbutton="false"
		 data-widget-sortable="false"
		 data-widget-Collapse="false"
		 data-widget-custom="false"
		 data-widget-togglebutton="false">
		<header>
			<div class="jarviswidget-ctrls">
				<a style = 'display:none;' href = '<?=Url::toRoute('export-statistic-category').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
					<i class="fa fa-bar-chart-o"></i>
				</a>

			</div>
            <span class="widget-icon">
                 <i class="fa fa-table"></i>
            </span>
			<h2><?=isset($category_name) ? $category_name : ' '?></h2>
			<h2><?=$date_time?>实时数据</h2>
		</header>
		<!--弹出框-->
		<div id="dialog_simple"   title="Dialog Simple Title" style="display:none;width: auto">

		</div>
		<!--end	-->
		<div class="well well-sm well-light">

			<div id="tabs" >
				<ul>
					<li>
						<a href="#tabs-all1">电</a>
					</li>
					<li>
						<a href="#tabs-modbus1">水</a>
					</li>

				</ul>

				<div id="tabs-all1">
					<table id="tabs-electric" style="width: 100px"></table>
					<div id="tabs-electric2"></div>
				</div>
				<div id="tabs-modbus1">
					<table id="tabs-water"></table>
					<div id="tabs-water2"></div>
				</div >


			</div>
		</div>

	</div>

</section>

	<section id="widget-grid" class="">

	</section>

	</div>

<?php
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/jquery.jqGrid.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/grid.locale-en.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

?>

<script>
    window.onload=function(){
		var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
		var jqgrid_data =<?=$data?>;
		var electricdata=Array();
		var waterdata=Array();
		var category_tree=<?=$category_tree?>;

		category_tree=category_tree[0];
		row=1;
		column=1;

		//数据分类 电  水
		for(var data in jqgrid_data){
			switch (jqgrid_data[data]['categroy']){
				case "电":electricdata.push(jqgrid_data[data]);break;
				case "水":waterdata.push(jqgrid_data[data]);break;
			}
		}

		init('tabs-electric',electricdata);
		init('tabs-water',waterdata);
		$('#tabs').tabs();

		//绑定事件方法
		function change() {
			$(".first").change(function () {
				var selectid=this.id
				var category_id = $("#"+selectid+" option:selected").val();
				$.ajax({
					type: "POST",
					url: "selectdata",
					data: {category_id: category_id},
					success: function (msg) {
						if (msg == 'error')
							console.log('无下属分类');
						else {
							var categorys = eval('[' + msg + ']');
							categorys = categorys[0];

							//截取selectid
							var selectrow = selectid.substring(3, 4);
							var selectcolumn = selectid.substring(10, 11);
							//删除大于选定id列数的所有元素
							for(var i=0;i<6;i++) {
								var tempcolumn=selectcolumn;
								tempcolumn++;
								//拼凑id 既行加一
								id = 'row' + selectrow + 'column' + tempcolumn;
								//判断id是否存在 若存在则删除后覆盖 不存在则加入
								if (document.getElementById(id) != undefined) {
									for(var j =tempcolumn;j<6;j++){
										var delete_id = 'row' + selectrow + 'column' + j;
										//存在则删除
										$("#" + delete_id).parent().remove();
									}
								}
							}

							//添加元素
							var html = '<div class="col-md-3" style="display: inline-block;margin: 5px;" ><select id=' + id + ' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
							for (var key in categorys)
								html = html + '<option value=' + key + '>' + categorys[key] + '</option>';
							html = html + '</select></div>';
							$("#" + selectid).parent().parent().append(html);
							//解绑 再绑定
							$(".first").unbind();
							change();
						}
					}
				});
			})
		}

		//绑定弹出框
		$('#dialog_simple').dialog({
			open : function(){
				id='row'+row+'column'+column;
				//添加分类
				var html='<div class="row"><div class="col-md-3" style="margin: 5px;"><select id='+id+' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
				for(var key in category_tree)
					html=html+'<option value='+key+'>'+category_tree[key]+'</option>';
				html=html+'</select></div></div>';
				$("#dialog_simple").append(html);
				//先解绑再绑定chang事件
				$(".first").unbind();
				change();
				row++;column++;
			},
			autoOpen : false,
			width : 600,
			resizable : false,
			modal : true,
			title :  "搜索",
			buttons : [{
				html : "<i class='fa fa-search'></i>&nbsp; 搜索",
				"class" : "btn btn-danger",
				click : function() {
					alert('搜索');
					$(this).dialog("close");
					$("#dialog_simple").empty();
				}
			}, {
				html : "<i class='fa fa-times'></i>&nbsp; 取消",
				"class" : "btn btn-default",
				click : function() {
					$(this).dialog("close");
					$("#dialog_simple").empty();
				}
			},{
					html : "<i class='fa fa-add'></i>&nbsp;添加分类",
					"class" : "btn btn-default",
					click : function() {
						id='row'+row+'column'+column;
						//添加分类
						var html='<div class="row"><div class="col-md-3" style="margin: 5px;"><select id='+id+' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
						for(var key in category_tree)
							html=html+'<option value='+key+'>'+category_tree[key]+'</option>';
						html=html+'</select></div></div>';
						$("#dialog_simple").append(html);
						//先解绑再绑定chang事件
						$(".first").unbind();
						change();
						row++;column++;
					}
				}],
			close: function(){$("#dialog_simple").empty();}
		});

	 $("#time_type").change(function (){
		_type = $(this).val();
		timeTypeChange(_type, $('#_time'));
	});
	$("#time_type").trigger('change');
	$('#_time').val($_date_time);

		$('#_cl').click(function(){
			if($("#_loop").css('display') == 'none'){
				$('#_line').hide();
			}else{
				$('#_line').show();
			}
			$("#_loop").slideToggle("slow");

		});

		//对表单进行验证
		$("#_submit").click(function(){
			//获取所有 需要选择的参数
			var time_type = $('#time_type').val();
			var time = $('#_time').val();
			var $error_infor = '';
			var $error_signal = 0;
			if(!time_type){
				$error_infor = '时间类型不能为空';
				$error_signal = 1;
			}
			if(!time){
				$error_infor = '时间不能为空';
				$error_signal = 1;
			}
			if($error_signal){
				$('#_error_log').text($error_infor);
				$('#_error_log').show();
				return false;
			}

		})

		function init(element,data) {
			jQuery("#" + element).jqGrid({
				data: data,
				datatype: "local",
				width: 780,
				height: 200,
				height: 'auto',
				colNames: ['id','名称','表数值','用量' ],
				colModel: [
					{name: 'point_id', index: 'id'},
					{name: 'name', index: 'name'},
					{name: 'last_value', index: 'last_value'},
					{name: 'value', index: 'value'},
				],
				rowNum: 10,
				rowList: [10, 20, 30],
				pager: "#" + element + '2',
				sortname: 'id',
				toolbarfilter: true,
				viewrecords: true,
				sortorder: "asc",
				editurl: "crud/jqgrid",//action页面crud/jqgrid
				caption: "Point",
				multiselect: false,
				autowidth: true,


			});
			//保存最后一行数据
			var Save = function () {
				$('#dialog_simple').dialog('open');
			};
			jQuery("#" + element).jqGrid('navGrid', "#" + element + '2', {
				save:false,
				search:false,
				refresh:false,
				edit: false,
				add: false,
				del: false,
			})
				//自定义save按钮
				.navButtonAdd("#" + element + '2',
				{
					caption: " ",
					buttonicon: "ui-icon-disk",
					onClickButton: Save,
					position: "first"
				});

			// remove classes
			$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
			$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
			$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
			$(".ui-jqgrid-pager").removeClass("ui-state-default");
			$(".ui-jqgrid").removeClass("ui-widget-content");

			// add classes
			$(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
			$(".ui-jqgrid-btable").addClass("table table-bordered table-striped");


			$(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
			$(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
			$(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
			$(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
			$(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
			$(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
			$(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-search").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
			$(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

			$(".ui-icon.ui-icon-seek-prev").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

			$(".ui-icon.ui-icon-seek-first").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

			$(".ui-icon.ui-icon-seek-next").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

			$(".ui-icon.ui-icon-seek-end").wrap("<div class='btn btn-sm btn-default'></div>");
			$(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");

			$(window).on('resize.jqGrid', function () {
				$("#"+element).jqGrid('setGridWidth', $("#content").width()-50);
			})
		}

    }
    </script>