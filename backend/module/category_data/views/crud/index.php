<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>参数设定</legend>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('category_ids', isset($category_ids) ? $category_ids : '')?>
                    <div class="col-md-4">
                        <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::textInput('date_time', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'placeholder' => '选择时间', 'id' => '_time'])?>
                    </div>
                    <!-- 查看的 分类id-->
                </div>

            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><span id = '_header'>建筑电能统计分析</span></h2>
            <div class="jarviswidget-ctrls">
                <a style = 'display:none;' href = <?=Url::toRoute('').'?query_type=export&'.http_build_query(Yii::$app->request->getQueryParams());?> id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a href = <?=Url::toRoute('') . '?' . http_build_query(Yii::$app->request->getQueryParams()) . '&row_type=' . (isset($row_type) ? (!$row_type ? 1 : 0) : 1);?> id="_row_type" class="button-icon" href="#" data-toggle="modal" rel="tooltip" data-original-title="行列转化" data-placement="bottom">
                    <i class="fa fa-sort-numeric-desc"></i>
                </a>
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>

            </div>
        </header>
        <!-- widget div-->

        <div class="no-padding">
            <div id="container_chart" style="height:500px"></div>
            <div id="container_table" style="display:none">
                <table class = 'table'>
                <?php if(!empty($category_data)):?>
                    <?php $i = 0;?>
                    <?php foreach($category_data as $k1 => $v1):?>
                        <!-- 循环第一次 将 table 的标题 嵌入-->
                        <?php if(!$i):?>
                            <thead>
                            <tr>
                                <th>#</th>
                                <?php foreach($v1 as $name => $value):?>
                                    <th><?=$name . ' (' . $category_unit . ')';?></th>
                                <?php endforeach;?>
                                <th>总计 (<?=$category_unit?>)</th>
                            </tr>
                            </thead>
                        <?php endif;?>

                        <!-- 循环第一次 将 table 的标题 嵌入-->

                        <!-- tbody 主体-->
                        <?php if(!$i):?>
                            <tbody>
                        <?php endif;?>
                        <tr>
                            <td><?=$k1?></td>
                            <?php $total = 0;?>
                            <?php foreach($v1 as $k2 => $v2):?>
                                <td><?=$v2?></td>
                                <?php $total += $v2;?>
                            <?php endforeach;?>
                            <td><?=$total?></td>
                        </tr>
                        <?php if(!$i):?>
                            </tbody>
                         <?php endif;?>
                        <!-- tbody 主体-->
                        <?php $i++;?>
                    <?php endforeach;?>
                    </table>
                <?php endif;?>
            </div>
        </div>
    </div>
    <!-------     图表内容 end-->
</section>
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<!-- js 脚本区域 start-->
<script>
window.onload = function(){
    var $chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
    var $_categories = <?= isset($categories) ? $categories : "''"?>;
    var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
    var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
    var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
    var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
    var $_ch_type = '天';
    var $_date_format = '%H:%M';
    var $_mr = 3600;
//    if(!$chart_data){
//        $('#_loop').show();
//        $('#_jarviswidget').hide();
//    }else{
//        $('#_loop').hide();
//        $('#_jarviswidget').show();
//    }

    switch($_type){
        case 'hour':
            $_ch_type = '小时';
            break;
        case 'day':
            $_date_format = '%H:%M';
            $_mr = 3600;
            $_ch_type = '天';
            break;
        case 'week':
            $_date_format = '%e. %b';
            $_mr = 86400;
            $_ch_type = '周';
            break;
        case 'month':
            $_date_format = '%e. %b';
            $_mr = 86400;
            $_ch_type = '月';
            break;
        case 'year':
            $_date_format = '%b';
            $_mr = 86400;
            $_ch_type = '年';
            break;
    }
//    $('#_header').text($_statistic_prefix + '_' + $_ch_type + '统计分析');

    $('#_cl').click(function(){
        if($("#_loop").css('display') == 'none'){
            $('#_line').hide();
        }else{
            $('#_line').show();
            if($chart_data.length){$('#_jarviswidget').show();}
        }
        $("#_loop").slideToggle("slow");

    });

    $("#time_type").change(function (){
        _type = $(this).val();
        timeTypeChange(_type, $('#_time'));
    });
    $("#time_type").trigger('change');
    $('#_time').val($_date_time);
    // 图形
    highcharts_lang_date();
    highChart('line');
    // highchart 代码
    function highChart($chart_type){
        $('#container_chart').highcharts({
            chart: {
                zoomType: 'x',
                type: 'column'
            },

            title: {
                text: $_statistic_prefix + ' -' + $_ch_type,

            },
            subtitle: {
               text: 'Time: ' + $_date_time
            },
            xAxis: {
                categories: $_categories
            },

            yAxis: {
//                reversed: true,
                title: {
                    text:' (' + $_unit + ')'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            tooltip: {
                headerFormat: '{point.key} '+'<br />',
                shared: true,
                style: {
                    width: 230
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'top',
                x: 0,
                y: 70
            },
            series: $chart_data
        });

    }

    // 图、表切换

    $('#_chart_switch').click(function(){
        var $_chart_type = $(this).data('type');
        if($_chart_type == 'chart'){
            $('#container_chart').hide();
            $('#chart_type').hide();
            $('#container_table').show();
            $('#_save').show();
            $(this).data('type', 'table');
        }else{
            $('#container_chart').show();
            $('#chart_type').show();
            $('#container_table').hide();
            $('#_save').hide();
            $(this).data('type', 'chart');
        }
    });

    //图形样式切换
    $('.radiobox').click(function(){
        var $_chart_type = $(this).data('value');
        if($_chart_type){
            highChart($_chart_type);
        }
    })
}
</script>
<!-- js 脚本区域 end-->