<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
$testdata=array(['Firefox',45.0],
    ['IE',26.8],
    ['Chrome',12.8],
    ['Safari',8.5],
    ['Opera',6.2],
    ['Others',0.7],
);

?>

<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >

                <header>


                    <div class="widget-toolbar smart-form" data-toggle="buttons">

                        <button class="btn btn-xs btn-primary" id="submit_button"  >
                            提交
                        </button>

                    </div>



                    <div class="widget-toolbar smart-form" style="width: 300px">

                        <label class="input"> <i class="icon-append fa fa-question-circle"></i>
                            <!-- 分类 隐藏框 -->
                            <?=Html::hiddenInput('Point[energy_id]', null, ['id' => 'category_id1'])?>
                            <div class="input-group" id = 'category_tree1' style="width: 100%;">
                                <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name1' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                    <ul id = 'select_category1' class="select2-choices choices1" style = 'min-height: 24px'>
                                    </ul>
                                </div>

                            </div>
                            <div id = 'tree1' style = 'display:none; width:100%'></div>
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Some helpful information</b>
                        </label>

                    </div>


                    <div class="widget-toolbar smart-form">

                        <label class="input"> <i class="icon-append fa fa-question-circle"></i>
                            <input type="text" name="date_time" id="time1" placeholder="Time">
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Some helpful information</b>
                        </label>

                    </div>

                    <!--       dropdown list             -->
                    <div class="widget-toolbar">
                        <?=Html::hiddenInput('time_type', null, ['id' => 'time_type1'])?>
                        <!-- add: non-hidden - to disable auto hide -->
                        <div class="btn-group">
                            <button class="btn dropdown-toggle btn-xs btn-default" id="time_button" data-toggle="dropdown">
                                时间类型 <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu js-status-update pull-right">
                                <li>
                                    <a href="javascript:void(0);" id="mt">Year</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="ag">Month</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="td">Day</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>

                    <h2>能源分类 </h2>

                </header>




                <div id="error" style="display: none;color: red;">
                    <h4 align="center">时间不能为空 </h4>
                </div>
                <!--    add  null form  with  post element 建立空表单 之后加入要提交的信息          -->
                <?php $form = MyActiveForm::begin(['method' => 'get','id'=>'testform']) ?>

                <?php MyActiveForm::end(); ?>


                <div id="tabs"  class="col-lg-6 " style="width:100%;height: 600px; ">

                    <ul>
                        <?php foreach ($top_location as $key=>$value) {
                            echo '<li>'
                                .'<a class="tabs_a" href="#tabs-'.$key.'">'.$value.'</a>'
                                .'</li>';

                        } ?>
                    </ul>

                    <?php foreach ($top_location as $key=>$value) {
                        echo  '<div id="tabs-'.$key.'" style="height: 450px" >'

                                .'</div>';
                    } ?>

                <div id="column_test" style="min-width:700px;height:400px"></div>

            </div>


            <!-- end widget -->
        </article>



    </div>

</section>
<!--</section>-->





<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);



$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);


?>
<script>

    window.onload = function(){
        var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        var _categories = <?= isset($categories) ? $categories : "''"?>;
        var re_query_param=<?= isset($re_query_param) ? $re_query_param : "''"?>;
        var $__type = '日';
        $("#tabs").tabs();
        switch(re_query_param['time_type']) {
            case 'day':
                $__type = '日';
                break;
            case 'month':
                $__type = '月';
                break;
            case 'year':
                $__type = '年';
                break;
        }
        if(re_query_param.length!=0){
            $("#time_type1").val(re_query_param['time_type']);
            document.getElementById('time_button').innerHTML=$__type+'<i class="fa fa-caret-down"></i>';
            $("#time1").val(re_query_param['date_time']);
        }


        $(".tabs_a").click(function(){
            $(Highcharts.charts).each(function(i,chart){
                var height = chart.renderTo.clientHeight;
                var width = chart.renderTo.clientWidth;
                chart.setSize(width, height);
            });

        })




        //验证表单  并 提交
        $("#submit_button").click(function(){
            //获取表单信息
            var time_type=$("#time_type1").val();
            var time=$("#time1").val();
            var category_id=$("#category_id1").val();
            //验证表单信息
            console.log(time_type== '');
            console.log(time=='');
            if(time_type!= '' && time!='') {
                //把要传递的信息都加入form表单后提交
                $("#testform").append($("#time_type1"));
                $("#testform").append($("#time1"));
                $("#testform").append($("#category_id1"));
//                $("#testform").append($("#category_id2"));
                $("#testform").submit();
            }
            else {
                $("#error").show();
                setTimeout('$("#error").hide()',1000)
                return false;
            }
        })








// calendar month
        $('#mt').click(function() {
            var temp=document.getElementById('time_button').innerHTML;
            console.log(temp);
            $("#time_type1").val('year');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='年  <i class="fa fa-caret-down"></i>';
            timeTypeChange('year', $('#time1'));
        });

        // calendar agenda week
        $('#ag').click(function() {
            $("#time_type1").val('month');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='月  <i class="fa fa-caret-down"></i>';
            timeTypeChange('month', $('#time1'));
        });

        // calendar agenda day
        $('#td').click(function() {
            $("#time_type1").val('day');
            console.log($("#time_type1").val());
            document.getElementById('time_button').innerHTML='日   <i class="fa fa-caret-down"></i>';
            timeTypeChange('day', $('#time1'));
        });













        var data=<?=$json_data?>;
        initcolumn('tabs-236',data[236]);
        initcolumn('tabs-240',data[240]);

//        initcolumn1('column_test',1);
        function initcolumn(element,data) {
            console.log(data);
            $("#"+element).highcharts({
                chart: {
                    type: 'column'
//                    ,
//                    events: {
//                        load: function (event) {
//                            charts_resize();
//                        }
//                    }
                },
                title: {
                    text: ' '
                },
                xAxis: {
                    categories:data['_category']
//                        ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                    //测试数据
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ' '
                    }
//                    ,
//                    stackLabels: {
//                        enabled: true,
//                        style: {
//                            fontWeight: 'bold',
//                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
//                        }
//                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },

                //去水印
                credits: {
                    enabled: false
                }
                ,
                series:data['data']
//                    [{
//                        name: 'John',
//                        data: [5, 3, 4, 7, 2]
//                    }, {
//                        name: 'Jane',
//                        data: [2, 2, 3, 2, 1]
//                    }, {
//                        name: 'Joe',
//                        data: [3, 4, 4, 2, 5]
//                    }]
                //测试数据
            });
        }


        function initcolumn1(element,data) {
            console.log(data);
            $("#"+element).highcharts({
                chart: {
                    type: 'column'
//                    ,
//                    events: {
//                        load: function (event) {
//                            charts_resize();
//                        }
//                    }
                },
                title: {
                    text: ' '
                },
                xAxis: {
                    categories://data['_category']
                        ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                    //测试数据
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ' '
                    }
//                    ,
//                    stackLabels: {
//                        enabled: true,
//                        style: {
//                            fontWeight: 'bold',
//                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
//                        }
//                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },

                //去水印
                credits: {
                    enabled: false
                }
                ,
                series://data['data']
                    [{
                        name: 'John',
                        data: [5, 3, 4, 7, 2],
                        color:'#CD6090'
                    }, {
                        name: 'Jane',
                        data: [2, 2, 3, 2, 1],
                        color:'#FFDEAD'
                    }, {
                        name: 'Joe',
                        data: [3, 4, 4, 2, 5],
                        color:'#8968CD'
                    }]
                //测试数据
            });
        }
//        initfancytree(<?//=$location_tree?>//,'2');

        initfancytree(<?=$energy_tree?>,'1');
        //初始化树形下拉列表框  data为树状数据源  id 为元素后缀名
        function initfancytree(data,id){
            //fancytree 树状多选框初始化
            var $current_keys = [];
            var $count = 0;

            function delete_all_category() {
                $('.choices'+id+' li').each(function(){
                    if($(this).data('type') != 'plus'){
                        $(this).remove();
                    }
                })
            }
            console.log("#tree"+id);

            $("#tree"+id).fancytree({
                extensions: ["glyph", "edit", "wide"],
                checkbox: true,
                selectMode: 1,
                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                glyph: {
                    map: {
                        doc: "glyphicon glyphicon-file",
                        docOpen: "glyphicon glyphicon-file",
                        checkbox: "glyphicon glyphicon-unchecked",
                        checkboxSelected: "glyphicon glyphicon-check",
                        checkboxUnknown: "glyphicon glyphicon-share",
                        error: "glyphicon glyphicon-warning-sign",
                        expanderClosed: "glyphicon glyphicon-plus-sign",
                        expanderLazy: "glyphicon glyphicon-plus-sign",
                        // expanderLazy: "glyphicon glyphicon-expand",
                        expanderOpen: "glyphicon glyphicon-minus-sign",
                        // expanderOpen: "glyphicon glyphicon-collapse-down",
                        folder: "glyphicon glyphicon-folder-close",
                        folderOpen: "glyphicon glyphicon-folder-open",
                        loading: "glyphicon glyphicon-refresh"
                        // loading: "icon-spinner icon-spin"
                    }
                },
                wide: {
                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                },
                source:data,
                select: function(event, data) {
                    // Get a list of all selected nodes, and convert to a key array:
                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                        return node.key;
                    });
                    console.log('当前所有分类' + '_________________________' + $sel_keys);
                    if ($sel_keys.length >= $current_keys.length) {
                        for (var i = 0; i < $sel_keys.length; i++) {

                            if($.inArray($sel_keys[i], $current_keys) === -1) {
                                console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                console.log('上次所选的分类 --- ' + $current_keys);
                                console.log('当前选择分类 --- ' + $sel_keys[i]);
                                var $current_key = $sel_keys[i];
                                var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                for (var j = 0; j < $_other_sibling.length; j++) {
                                    console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                    $_other_sibling[j].setSelected(false);
                                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                        return node.key;
                                    });
                                }
                            }

                        }
                    }

                    //获取所有选中的分类将其加入到选择框中
                    var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                        console.log(node.key + ' ---- ' + node.title);
                        var $_html = '';
                        $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                            "<div>" + node.title + "</div>" +
                            "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                        return $_html;
                    });
                    //将分类选择框中的分类全部清除
                    delete_all_category();
                    $('#select_category'+id).append($_html);
                    //将选择的分类id放入分类隐藏框中
                    $('#category_id'+id).val($sel_keys);
                    //将最新所选的分类集合跟新到$current_keys 里
                    $current_keys = $sel_keys;
                    console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                }
            });

            $('#_clear'+id).click(function () {
                delete_all_category();
                $("#tree").fancytree();
            });

            $('#category_tree'+id).click(function() {
                $('#tree'+id).toggle();
            });
            $('#tree'+id).mouseleave(function() {
                $('#tree'+id).hide();
            });
        }
        //提交验证
        $('#_sub').click(function() {

        });
        $('#energy_cateogry').click(function() {
            $('#tree1').toggle();
        });

//    $('#tree1').mouseleave(function() {
//        $('#tree1').hide();
//    });
    }
    </script>
