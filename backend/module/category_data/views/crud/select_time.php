<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
$testdata=array(['Firefox',45.0],
    ['IE',26.8],
    ['Chrome',12.8],
    ['Safari',8.5],
    ['Opera',6.2],
    ['Others',0.7],
);
// echo '<pre>';
// print_r($param['point_ids']);die;
?>




            <!--submit form start-->
            <div id = '_loop' style = 'display: block;border:2px solid #D5D5D5;padding-bottom:15px'>
                <?php $form = MyActiveForm::begin(['method' => 'get']) ?>

                <fieldset style="margin:-20px auto;padding-bottom:20px">
                    <legend style="visibility:hidden;"></legend>

                    <div class="form-group">
                        <?=Html::hiddenInput('point_ids', $param['point_ids'])?>

                        <div class="col-md-2">
                            <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'day' => Yii::t('app', 'Day'),
                               'hour' => Yii::t('app', 'Hour'),
                            ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                        </div>


                        <div class="col-md-3" style="width:230px;">
                            <?=Html::textInput('date_time', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'placeholder' => date('Y-m',time()), 'id' => '_time'])?>
                        </div>
                    </div>


                    <!--提交-->
                    <div style="float:right;margin:1px 39px;">
                        <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                    </div>

                    <div style="">
                        <div style = 'padding-left:12px;margin-right:30px;margin-bottom:30px;color:red;text-align: left; display:none' id = '_error_log'></div>
                    </div>

                </fieldset>

            <?php MyActiveForm::end(); ?>
        </div>
        <!--submit form end-->







<section id="widget-grid" class="">

    <article class="col-sm-12 col-md-12 col-lg-12" style="padding:0px !important">

        <!-- content  图表内容 start-->
        <div style="padding:0px !important" class="jarviswidget jarviswidget-color-blueDark _jarviswidget"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
             data-widget-Collapse="false"
             data-widget-custom="false"
             data-widget-togglebutton="false">
            <header>
                <div class="jarviswidget-ctrls">
                    <a style = 'display:none;' href = '<?=Url::toRoute('export-energy-calculate').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                        <i class="fa fa-file-excel-o"></i>
                    </a>
                    <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                        <i class="fa fa-bar-chart-o"></i>
                    </a>

                </div>
            <span class="widget-icon">
                 <i class="fa fa-table"></i>
            </span>
                <h2><?=isset($category_name) ? $category_name : ' '?></h2>
                <h2>实时数据</h2>
            </header>
            <!--         widget div-->


        <div class="no-padding">

            <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                <div class="`" >
                    <label class="radio radio-inline"  style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>
                        <span>曲线图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>
                        <span>柱状图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'area'>
                        <span>堆栈图</span>
                    </label>
                </div>

            </div>

            <div id="container_chart" style="height: 600px;"></div>

            <!--container_table start-->
            <div id="container_table" style="">
                <div id="_table"></div>

                <!--                --><?//= \yii\widgets\LinkPager::widget([
                //                    'pagination' => $pages
                //                ])
                //                ?>

                <!--container_table end-->

            </div>

        </div>
        <!--v图表内容 end-->

    </article>

</section>
<!--</section>-->

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>

<script type="text/javascript">
    window.onload = function () {
        $("#time_type").change(function () {
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });


        //对表单进行验证
        $("#_submit").click(function(){
            //获取所有 需要选择的参数
            var time_type = $('#time_type').val();
            var time = $('#_time').val();
//            alert(data_value);
            var $error_infor = '';
            var $error_signal = 0;
            // if(!data_value){
            //     $error_infor = '请选择分类类型';
            //     $error_signal = 1;
            // }
            if(!time){
                $error_infor = '时间不能为空';
                $error_signal = 1;
            }

            if(!time_type){
                $error_infor = '时间类型不能为空';
                $error_signal = 1;
            }
            if($error_signal){
                $('#_error_log').text($error_infor);
                $('#_error_log').show();
                return false;
            }

        });


    }
</script>