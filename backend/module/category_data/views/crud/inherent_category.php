<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>参数设定</legend>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('parent_category_ids', isset($parent_category_id) ? $parent_category_id : '')?>
                    <div class="col-md-3">
                        <?=MyHtml::dropDownList('category_ids', isset($query_param['category_ids'])?$query_param['category_ids']:null, isset($category_tree) ? $category_tree :  []
                            , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=Html::dropDownList('time_type', isset($query_param['time_type'])?$query_param['time_type']:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=Html::textInput('date_time', isset($query_param['date_time'])?$query_param['date_time']:null,  ['class' => 'form-control', 'placeholder' => '选择时间', 'id' => '_time'])?>
                    </div>
                    <!-- 查看的 分类id-->
                </div>

            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?= isset($category_name) ? $category_name  : ''?></h2>
            <div class="jarviswidget-ctrls">
                <a style = 'display:none;' href = <?=Url::toRoute('').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?> id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>

            </div>
        </header>
        <!-- widget div-->

        <div class="no-padding">

            <div class="form-group" id="chart_type"  <?php if(isset($display_type) && $display_type == 'table') $style =  'display:none;padding:10px 0px 10px'; else $style = 'padding:10px 0px 10px' ?> style = <?=$style;?>
                <div class="col-md-10" style = 'border-bottom:1px solid #b2dba1'>
<!--                    <label class="radio radio-inline"  style = 'margin-top:0px'>-->
<!--                        <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>-->
<!--                        <span>曲线图</span>-->
<!--                    </label>-->
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>
                        <span>柱状图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'bar'>
                        <span>柱状图_2</span>
                    </label>
                </div>

                <div id="container_chart" style="min-height:500px"></div>


            <div id="container_table"  <?php if(isset($display_type) && $display_type == 'chart'):?> style = 'display:none' <?php endif;?>>
                <?=Html::hiddenInput('content_type', 'chart')?>

                <?php if(isset($dataProvider) && !empty($dataProvider)):?>
                    <?php \yii\widgets\Pjax::begin(); ?>
                    <?= GridView::widget([
                            'formatter' => ['class' => 'common\library\MyFormatter'],
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover table-middle',
                                'style' => 'margin-top:20px'
                            ],
                            'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                            'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                            'columns' => [
                                ['attribute' => '_timestamp', 'label' => '时间', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                ['attribute' => 'point', 'label' => '点位名称','format' => ['Json', 'name'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
                                ['attribute' => 'value', 'label' => '数值', 'format' => 'Floor', 'headerOptions' => ['data-hide' => 'phone,tablet']]
                            ],
                        ]); ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                <?php endif;?>
            </div>

        </div>
    </div>
    <!-------     图表内容 end-->
</section>
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<!-- js 脚本区域 start-->
<script>
window.onload = function(){
    var $chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
    var $_categories = <?= isset($categories) ? $categories : "''"?>;
    var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
    var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
    var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
    var $_type = <?= isset($query_param['time_type']) ? "'" .$query_param['time_type'] ."'" : "'day'"?>;
    var $_date_time = <?= isset($query_param['date_time']) ? "'" .$query_param['date_time'] ."'" : "''"?>;
    var $display_type = <?= isset($display_type) ? "'" .$display_type ."'" : "''"?>;

    // 图、表切换

    $('#_chart_switch').click(function(){
        var $_chart_type = $(this).data('type');
        if($_chart_type == 'chart'){
            $('#container_chart').hide();
            $('#chart_type').hide();
            $('#container_table').show();
            $('#_save').show();
            $(this).data('type', 'table');
        }else{
            $('#container_chart').show();
            $('#chart_type').show();
            $('#container_table').hide();
            $('#_save').hide();
            $(this).data('type', 'chart');
        }
    });

    //根据 display_type 判断上次用户请求的是哪个 标签页
    if($display_type == 'table') {
        $('#_chart_switch').data('type', 'chart');
        $('#_chart_switch').trigger('click');
    }else {
        $('#_chart_switch').data('type', 'table');
        $('#_chart_switch').trigger('click');
    }

//    if(!$chart_data){
//        $('#_loop').show();
//        $('#_jarviswidget').hide();
//    }else{
//        $('#_loop').hide();
//        $('#_jarviswidget').show();
//    }
//
//    switch($_type){
//        case 'hour':
//            $_ch_type = '小时';
//            break;
//        case 'day':
//            $_date_format = '%H:%M';
//            $_mr = 3600;
//            $_ch_type = '天';
//            break;
//        case 'week':
//            $_date_format = '%e. %b';
//            $_mr = 86400;
//            $_ch_type = '周';
//            break;
//        case 'month':
//            $_date_format = '%e. %b';
//            $_mr = 86400;
//            $_ch_type = '月';
//            break;
//        case 'year':
//            $_date_format = '%b';
//            $_mr = 86400;
//            $_ch_type = '年';
//            break;
//    }

    $('#_cl').click(function(){
        if($("#_loop").css('display') == 'none'){
            $('#_line').hide();
        }else{
            $('#_line').show();
            if($chart_data.length){$('#_jarviswidget').show();}
        }
        $("#_loop").slideToggle("slow");

    });

    $("#time_type").change(function (){
        _type = $(this).val();
        timeTypeChange(_type, $('#_time'));
    });
    $("#time_type").trigger('change');
    $('#_time').val($_date_time);
    // 图形
    highcharts_lang_date();
    highChart('column');
    // highchart 代码
    function highChart(type){
        $('#container_chart').highcharts({
            chart: {
                type: type,
                zoomType: 'xy'
            },
            title: {
                text: $category_name
            },
            subtitle: {
                text: $_date_time + ' (' + '<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>' + ')'
            },
            xAxis: {
                categories: $_categories
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            legend: {
//                align: 'right',
//                x: -30,
//                verticalAlign: 'bottom',
//                y: 25,
//                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1
//                shadow: false
            },

            series: [{
                name: $category_name,
                data: $chart_data,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });

    }

    //图形样式切换
    $('.radiobox').click(function(){
        var $_chart_type = $(this).data('value');
        if($_chart_type){
            highChart($_chart_type);
        }
    })
}
</script>
<!-- js 脚本区域 end-->