<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
//echo '<pre>';
//print_r($category_ids);
//die;
?>

<!-- html 区域 start-->
<section id="widget-grid" class="" style="display:inline;width: 80%">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->


    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark _jarviswidget"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false">
        <header>
            <div class="jarviswidget-ctrls">
                <a style = 'display:none;' href = '<?=Url::toRoute('export-statistic-category').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>

            </div>
            <span class="widget-icon">
                 <i class="fa fa-table"></i>
            </span>
            <h2><?=isset($category_name) ? $category_name : ' '?></h2>
            <h2>实时数据</h2>
        </header>
        <!--         widget div-->

        <div class="no-padding">

            <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                <div class="col-md-10" >
                    <label class="radio radio-inline"  style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>
                        <span>曲线图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>
                        <span>柱状图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'bar'>
                        <span>柱状图_2</span>
                    </label>

                        <input type="button" class="btn btn-default" name="style-0a" id="add_" data-value = 'bar'>
                        <span>添加</span>

                </div>

            </div>

            <div id="container_chart" style="height: 600px;"></div>



        </div>






    </div>
    <!-------     图表内容 end-->



</section>
<!-- html 区域 end-->

<!--弹出框-->
<div id="dialog_simple"   title="Dialog Simple Title" style="display:none;width: auto">
    <?php $form = MyActiveForm::begin(['method' => 'get','id'=>'_form']) ?>

    <?php MyActiveForm::end(); ?>
</div>
<!--end	-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/skin-bootstrap/skin-win7/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>

<!-- js 脚本区域 start-->
<script>
    window.onload = function(){
        row=1;
        column=1;
        //tree_json [0][1][2]分别为 系统分类 地理分类 能源分类
        var tree_json=<?= isset($tree_json) ? $tree_json : "''"?>;

        chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        _categories = <?= isset($categories) ? $categories : "''"?>;
        var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
        var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
        var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
        var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var $display_type = <?= isset($display_type) ? "'" .$display_type ."'" : "''"?>;
        var $__type = '日';

        $('#add_').click(function (){
            $('#dialog_simple').dialog('open');

        })

        function array_by_name(name,type){
            var data=Array();
                var timetypeEle=document.getElementsByName(name);
                    for(var i=0;i<timetypeEle.length;i++){
                        var id=timetypeEle[i].id;
                        var value;
                        switch (type){
                            case 'select':
                                     value=$("#"+id +" option:selected").val(); break;
                            case 'input':
                                     value=$("#"+id).val();
                    }
                        data.push(value);
            }
            return data;
        }

        //绑定弹出框
        $('#dialog_simple').dialog({
            autoOpen : false,
            width : 700,
            resizable : false,
            modal : true,
            title :  "搜索",
            buttons : [{
                html : "<i class='fa fa-trash-o'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
//                    $('#_form').submit();
                    //遍历form把表单 得到搜索条件
                    var time_type=array_by_name('time_type[]','select');
                    var time=array_by_name('time[]','input');
                    var energy_id=array_by_name('energy_id[]','input');
                    var location_id=array_by_name('location_id[]','input');
                    time_type = JSON.stringify(time_type);
                    time = JSON.stringify(time);
                    energy_id = JSON.stringify(energy_id);
                    location_id = JSON.stringify(location_id);

                    //调试用
                   // window.location.href="compare-ajax?time_type="+time_type+'&time='+time+'&energy_id='+energy_id+'&location_id='+location_id;
                    //ajax 更新表格
                    $.ajax({
                        type: "POST",
                        url: "/category-data/crud/compare-ajax",
                        data: {time_type:time_type, time:time,energy_id:energy_id,location_id:location_id},
                        success: function (msg) {
                            var data=eval('['+msg+']');
                            _categories=data[0]['categorys'];
                            chart_data=data[0]['chart_data'];
                            highChart('column');
                            //highChart('column',[0,1],[ 222,133]);
                           // console.log(111);
                        }
                    });
                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }, {
                    html : "<i class='fa fa-times'></i>&nbsp; 删除分类",
                    "class" : "btn btn-default",
                    click : function() {
                        row--;
                        var id='row'+row;
                        $("#"+id).parent().remove();
                        console.log('删除'+id);
                    }
                },{
                html : "<i class='fa fa-times'></i>&nbsp;添加分类",
                "class" : "btn btn-default",
                click : function() {
                    //添加搜索框
                    addprop();

                    //返回搜索条件，



                }
            }]
        });



        switch($_type) {
            case 'day':
                $__type = '日';
                break;
            case 'month':
                $__type = '月';
                break;
            case 'year':
                $__type = '年';
                break;
        }
        // 图、表切换

        $('#_chart_switch').click(function(){
            var $_chart_type = $(this).data('type');
            if($_chart_type == 'chart'){
                $('#container_chart').hide();
                $('#chart_type').hide();
                $('#container_table').show();
                $('#_save').show();
                $(this).data('type', 'table');
            }else{
                $('#container_chart').show();
                $('#chart_type').show();
                $('#container_table').hide();
                $('#_save').hide();
                $(this).data('type', 'chart');
            }
        });

        //根据 display_type 判断上次用户请求的是哪个 标签页
        if($display_type == 'table') {
            $('#_chart_switch').data('type', 'chart');
            $('#_chart_switch').trigger('click');
        }else {
            $('#_chart_switch').data('type', 'table');
            $('#_chart_switch').trigger('click');
        }

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
                if($chart_data.length){$('._jarviswidget').show();}
            }
            $("#_loop").slideToggle("slow");

        });

        function time(row) {
            var time_type="time_type"+row;
            var _time="_time"+row;
            console.log(time_type);
            console.log(_time);
            $("#"+time_type).change(function () {
                _type = $(this).val();
                timeTypeChange(_type,$("#"+_time));
            });
            $("#"+time_type).trigger('change');
            $("#"+_time).val($_date_time);
        }

        // 图形
        highcharts_lang_date();
        highChart('column');
        //highChart('column',[0,1],[0,133]);
        //highChart('column',$_categories,$chart_data);
        // highchart 代码
        function highChart(type){
            console.log(_categories);
            console.log(chart_data);
            $('#container_chart').highcharts({
                chart: {
                    type: type,
                    zoomType: 'x'
                },
                title: {
                    text: $category_name
                },
                subtitle: {
                    text: $_date_time + ' ' + '<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>' + '累计量'
                },
                xAxis: {
                    categories: _categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },

                legend: {
//                align: 'right',
//                x: -30,
//                verticalAlign: 'bottom',
//                y: 25,
//                floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1
//                shadow: false
                },

                series: [{
                    name: $category_name,
                    data: chart_data,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }]
            });

        }




        //图形样式切换
        $('.radiobox').click(function(){
            var $_chart_type = $(this).data('value');
            if($_chart_type){
                highChart($_chart_type);
            }
        })




        function inittree(row,type){
            //fancytree 树状多选框初始化
            var $current_keys = [];
            var $count = 0;
            var tree="tree"+row+type;
            var select_category="select_category"+row+type;
            var category_id="category_id"+row+type;
            var category_tree="category_tree"+row+type;
            var select2_choices="select2-choices"+row+type;
            function delete_all_category() {
                $('.'+select2_choices+' li').each(function(){
                    if($(this).data('type') != 'plus'){
                        $(this).remove();
                    }
                })
            }

            $("#"+tree).fancytree({
                extensions: ["glyph", "edit", "wide"],
                checkbox: true,
                selectMode: 1,
                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                glyph: {
                    map: {
                        doc: "glyphicon glyphicon-file",
                        docOpen: "glyphicon glyphicon-file",
                        checkbox: "glyphicon glyphicon-unchecked",
                        checkboxSelected: "glyphicon glyphicon-check",
                        checkboxUnknown: "glyphicon glyphicon-share",
                        error: "glyphicon glyphicon-warning-sign",
                        expanderClosed: "glyphicon glyphicon-plus-sign",
                        expanderLazy: "glyphicon glyphicon-plus-sign",
                        // expanderLazy: "glyphicon glyphicon-expand",
                        expanderOpen: "glyphicon glyphicon-minus-sign",
                        // expanderOpen: "glyphicon glyphicon-collapse-down",
                        folder: "glyphicon glyphicon-folder-close",
                        folderOpen: "glyphicon glyphicon-folder-open",
                        loading: "glyphicon glyphicon-refresh"
                        // loading: "icon-spinner icon-spin"
                    }
                },
                wide: {
                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                },
//                source:<?//=$tree_json;?>//,
                source:tree_json[type],
                select: function(event, data) {
                    // Get a list of all selected nodes, and convert to a key array:
                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                        return node.key;
                    });
                    console.log('当前所有分类' + '_________________________' + $sel_keys);
                    if ($sel_keys.length >= $current_keys.length) {
                        for (var i = 0; i < $sel_keys.length; i++) {

                            if($.inArray($sel_keys[i], $current_keys) === -1) {
                                console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                console.log('上次所选的分类 --- ' + $current_keys);
                                console.log('当前选择分类 --- ' + $sel_keys[i]);
                                var $current_key = $sel_keys[i];
                                var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                for (var j = 0; j < $_other_sibling.length; j++) {
                                    console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                    $_other_sibling[j].setSelected(false);
                                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                        return node.key;
                                    });
                                }
                            }

                        }
                    }

                    //获取所有选中的分类将其加入到选择框中
                    var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                        console.log(node.key + ' ---- ' + node.title);
                        var $_html = '';
                        $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                            "<div>" + node.title + "</div>" +
                            "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                        return $_html;
                    });
                    //将分类选择框中的分类全部清除
                    delete_all_category();
                    $('#'+select_category).append($_html);
                    //将选择的分类id放入分类隐藏框中
                    $('#'+category_id).val($sel_keys);
                    //将最新所选的分类集合跟新到$current_keys 里
                    $current_keys = $sel_keys;
                    console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                }
            });

//            $('#_clear'+row).click(function () {
//                delete_all_category();
//                $("#tree"+row).fancytree();
//            });

            $('#'+category_tree).click(function() {
                console.log(111);
                $('#'+tree).toggle();
            });
            $('#'+tree).mouseleave(function() {
                $('#'+tree).hide();
            });
            //提交验证
            $('#_sub').click(function() {

            });
        }
        function addprop(){
            var id='row'+row;
            //添加分类
            var html='<div class="row" style="margin: 5px;">'
                    //start 年月日选择
                +'<div id='+id+' class="col-md-1" style="margin: 5px;">'
                +'<select id="time_type'+row+'" class="btn btn-default btn-md  dropdown-toggle first time_type" name="time_type[]" placeholder="请选择相应点位">'
                +'<option value='+'year'+'>'+'年'+'</option>'
                +'<option value='+'month'+'>'+'月'+'</option>'
                +'<option value='+'day'+'>'+'日'+'</option>'
                + '</select>'
                + '</div>'
                    //end年月日选择
                +'<div class="col-md-1" style="margin: 5px;">'
                +'</div>'
                    //start 时间框
                +'<div class="col-md-3" style="margin: 5px;">'
                +'<input class ="form-control", name="time[]" placeholder="选择时间", id="_time'+row+'"/>'
                +'</div>'
                    //end 时间框
                    //start 能源分类选择
                +'<div class="col-md-3" style="margin: 5px;">'
                +'<input type="hidden" id="category_id'+row+'2'+'" name="energy_id[]">'
                +'<div class="input-group" id = "category_tree'+row+'2'+'">'
                +'<div rel="tooltip" data-original-title="分类选择" data-placement="top" id = "category_name'+row+'2'+'" class = "select2-contaner select2-container-multi" style = "margin-top:2px;width: 100%;" >'
                +'<ul id = "select_category'+row+'2'+'" class="select2-choices select2-choices'+row+'2'+'" style = "width:150px;min-height: 32px">'
                +'</ul>'
                +'</div>'

//                +'<div class="input-group-btn">'
//                +'<button class="btn btn-default " type="button"  id = "_clear'+row+'2'+'">'
//                +'<i class="fa fa-mail-reply-all"></i> Clear '
//                +'</button>'
//                +'</div>'
                +'</div>'
                +'<div id = "tree'+row+'2'+'" style = "display:none;position:absolute;z-index:10;width:100%"></div><br />'
                +'</div>'
                    //end 能源分类选择
                    //start地理位置分类选择
                +'<div class="col-md-3" style="margin: 5px;">'
                +'<input type="hidden" id="category_id'+row+'1'+'" name="location_id[]">'
                +'<div class="input-group" id = "category_tree'+row+'1'+'">'
                +'<div rel="tooltip" data-original-title="分类选择" data-placement="top" id = "category_name'+row+'1'+'" class = "select2-contaner select2-container-multi" style = "margin-top:2px;width: 100%;" >'
                +'<ul id = "select_category'+row+'1'+'" class="select2-choices select2-choices'+row+'1'+'" style = "width:150px;min-height: 32px">'
                +'</ul>'
                +'</div>'
//                +'<div class="input-group-btn">'
//                +'<button class="btn btn-default " type="button"  id = "_clear'+row+'1'+'">'
//                +'<i class="fa fa-mail-reply-all"></i> Clear '
//                +'</button>'
//                +'</div>'
                +'</div>'
                +'<div id = "tree'+row+'1'+'" style = "display:none;position:absolute;z-index:10;width:100%"></div><br />'
                +'</div>'
                    //end 分类选择框
                +'</div>';

            $("#_form").append(html);
            console.log('添加'+id);
            //初始化 分类框
            inittree(row,'2');
            inittree(row,'1');
            //初始化时间框
            time(row);
            row++;
        }
    }
</script>
<!-- js 脚本区域 end-->