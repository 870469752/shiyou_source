<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
?>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <!--配置jqgrid插件，生成button按钮,禁用delete、edit、color、sort按钮-->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                    >
            <header>
                <h2>时间切换</h2>
                <div class="jarviswidget-ctrls">
                    <a style = 'display:none;' href = '<?=Url::toRoute('export-statistic-category').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                        <i class="fa fa-file-excel-o"></i>
                    </a>
                </div>
                 <span class="widget-icon">
                <a id="backward" class="button-icon"   data-type="chart" rel="tooltip" data-original-title="" data-placement="bottom">
                    <i class="fa fa-fast-backward"></i>
                </a>
            </span>
                <h2 id="time_" name="0">年</h2>
                <span class="widget-icon">
                <a id="forward" class="button-icon"   data-type="chart" rel="tooltip" data-original-title="" data-placement="bottom">
                    <i class="fa fa-fast-forward"></i>
                </a>
            </span>
                <h2><?=isset($category_name) ? $category_name : ' '?></h2>
                <h2>实时数据</h2>
            </header>
            <!--弹出框-->
            <div id="dialog_simple"   title="Dialog Simple Title" style="display:none;width: auto">
            </div>
            <!--end	-->
            <div class="well well-sm well-light">

                <div id="tabs" >
                    <ul>
                        <li>
                            <a href="#tabs-all1">电</a>
                        </li>
                        <li>
                            <a href="#tabs-modbus1">水</a>
                        </li>

                    </ul>

                    <div id="tabs-all1">
                        <table id="tabs-electric" style="width: 100px"></table>
                        <div id="tabs-electric2"></div>
                    </div>
                    <div id="tabs-modbus1">
                        <table id="tabs-water"></table>
                        <div id="tabs-water2"></div>
                    </div >


                </div>
            </div>
        </div>
        </div>
    </section>

<?php
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/jquery.jqGrid.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("/js/plugin/jqgrid/jqGrid_4.8.2/grid.locale-en.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

?>

<script>
    window.onload=function(){
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;

        var year =<?=$year?>;
        var quarter =<?=$quarter?>;
        var month =<?=$month?>;
        var day =<?=$day?>;
        //分类搜索用 暂停写
        var category_tree=<?=$category_tree?>;
        category_tree=category_tree[0];
        row=1;
        column=1;

        function showbytype(timetype) {
            switch (timetype)
            {
                case 'year':jqgrid_data=year;break;
                case 'quarter':jqgrid_data=quarter;break;
                case 'month':jqgrid_data=month;break;
                case 'day':jqgrid_data=day;break;
            }
            var electricdata=Array();
            var waterdata=Array();

            //数据分类 电  水
            for (var data in jqgrid_data) {
                //console.log(jqgrid_data[data]['categroy']);
                switch (jqgrid_data[data]['categroy']) {
                    case "电":
                        electricdata.push(jqgrid_data[data]);
                        break;
                    case "水":
                        waterdata.push(jqgrid_data[data]);
                        break;
                }
            }

            //清空数据
            jQuery("#tabs-electric").jqGrid("clearGridData");
            jQuery("#tabs-water").jqGrid("clearGridData");
            //重新加载
            var mygrid1 = jQuery("#tabs-electric")[0];
            var mygrid2 = jQuery("#tabs-water")[0];
            mygrid1.addJSONData(electricdata);
            mygrid2.addJSONData(waterdata);
        }

        var time={0:'year',1:'quarter',2:'month',3:'day'};
        var zh_cn={'year':'年','quarter':'季度','month':'月','day':'日'};

        $("#backward").click(function(){
                var element=document.getElementById('time_');
                var name=element.getAttribute("name");
                if(name==0)name=3;
             else name--;
            element.setAttribute("name",name);
            element.innerText=zh_cn[time[name]];
            showbytype(time[name]);
        });

        $("#forward").click(function(){
            var element=document.getElementById('time_');
            var name=element.getAttribute("name");
            if(name==3)name=0;
            else name++;
            element.setAttribute("name",name);
            element.innerText=zh_cn[time[name]];
            showbytype(time[name]);
        });

        //点击事件 切换数据
        init('tabs-electric',[]);
        init('tabs-water',[]);
        $('#tabs').tabs();
        showbytype('year');

        //绑定事件方法
        function change() {
            $(".first").change(function () {

                var selectid=this.id;
                var category_id = $("#"+selectid+" option:selected").val();

                $.ajax({
                    type: "POST",
                    url: "selectdata",
                    data: {category_id: category_id},
                    success: function (msg) {
                        if (msg == 'error')
                            console.log('无下属分类');
                        else {
                            var categorys = eval('[' + msg + ']');
                            categorys = categorys[0];
                            //截取selectid
                            var selectrow = selectid.substring(3, 4);
                            var selectcolumn = selectid.substring(10, 11);
                            //删除大于选定id列数的所有元素
                            for(var i= 0;i<6;i++) {
                                var tempcolumn=selectcolumn;
                                tempcolumn++;
                                //拼凑id 既行加一
                                id = 'row' + selectrow + 'column' + tempcolumn;
                                //判断id是否存在 若存在则删除后覆盖 不存在则加入
                                if (document.getElementById(id) != undefined) {
                                    for(var j =tempcolumn;j<6;j++){
                                        var delete_id = 'row' + selectrow + 'column' + j;
                                        //存在则删除
                                        $("#" + delete_id).parent().remove();
                                    }
                                }
                            }

                            //添加元素
                            var html = '<div class="col-md-3" style="display: inline-block;margin: 5px;" ><select id=' + id + ' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
                            for (var key in categorys)
                                html = html + '<option value=' + key + '>' + categorys[key] + '</option>';
                            html = html + '</select></div>';
                            $("#" + selectid).parent().parent().append(html);
                            //解绑 再绑定
                            $(".first").unbind();
                            change();

                        }
                    }
                });
            })
        }

        //绑定弹出框
        $('#dialog_simple').dialog({
            open : function(){
                id='row'+row+'column'+column;
                //添加分类
                var html='<div class="row"><div class="col-md-3" style="margin: 5px;"><select id='+id+' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
                for(var key in category_tree)
                    html=html+'<option value='+key+'>'+category_tree[key]+'</option>';
                html=html+'</select></div></div>';
                $("#dialog_simple").append(html);
                //先解绑再绑定chang事件
                $(".first").unbind();
                change();
                row++;column++;
            },
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            title :  "搜索",
            buttons : [{
                html : "<i class='fa fa-search'></i>&nbsp; 搜索",
                "class" : "btn btn-danger",
                click : function() {
                    alert('搜索');
                    $(this).dialog("close");
                    $("#dialog_simple").empty();
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                    $("#dialog_simple").empty();
                }
            },{
                html : "<i class='fa fa-add'></i>&nbsp;添加分类",
                "class" : "btn btn-default",
                click : function() {
                    id='row'+row+'column'+column;
                    //添加分类
                    var html='<div class="row"><div class="col-md-3" style="margin: 5px;"><select id='+id+' class="btn btn-default btn-md  dropdown-toggle first" name="point_id" placeholder="请选择相应点位">';
                    for(var key in category_tree)
                        html=html+'<option value='+key+'>'+category_tree[key]+'</option>';
                    html=html+'</select></div></div>';
                    //console.log(html);
                    $("#dialog_simple").append(html);
                    //先解绑再绑定chang事件
                    $(".first").unbind();
                    change();
                    row++;column++;
                }
            }],
            close:function(){$("#dialog_simple").empty();}
        });

        $("#time_type").change(function (){
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });
        $("#time_type").trigger('change');
        $('#_time').val($_date_time);

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
            }
            $("#_loop").slideToggle("slow");
        });

        function init(element,data) {
            jQuery("#" + element).jqGrid({
                data: data,
                datatype: "local",
                width: 780,
                height: 200,
                height: 'auto',
//				colNames: ['Inv No', '名称', 'Name', '采集间隔', '协议类型', '基础值', '历史基础值', '是否可用',
//					'是否屏蔽', '是否上传', '是否记录', '是否系统', '最新数据', ' 单位', '值类型', '过滤值', 'Actions'],
                colNames: ['id','名称','表数值','用量' ],
                colModel: [
                    {name: 'point_id', index: 'id'},
                    {name: 'name', index: 'name'},
                    {name: 'last_value', index: 'last_value'},
                    {name: 'value', index: 'value'},
                ],
                rowNum: 10,
                rowList: [10, 20, 30],
                pager: "#" + element + '2',
                sortname: 'id',
                toolbarfilter: true,
                viewrecords: true,
                sortorder: "asc",
                editurl: "crud/jqgrid",//action页面crud/jqgrid
                caption: "Point",
                multiselect: false,
                autowidth: true,

            });
            //保存最后一行数据
            var Save = function () {
                $('#dialog_simple').dialog('open');
            };
            jQuery("#" + element).jqGrid('navGrid', "#" + element + '2', {
                save:false,
                search:false,
                refresh:false,
                edit: false,
                add: false,
                del: false,
            })
                //自定义save按钮
                .navButtonAdd("#" + element + '2',
                {
                    caption: " ",
                    buttonicon: "ui-icon-disk",
                    onClickButton: Save,
                    position: "first"
                });

            // remove classes
            $(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
            $(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
            $(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
            $(".ui-jqgrid-pager").removeClass("ui-state-default");
            $(".ui-jqgrid").removeClass("ui-widget-content");

            // add classes
            $(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
            $(".ui-jqgrid-btable").addClass("table table-bordered table-striped");

            $(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
            $(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
            $(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
            $(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
            $(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
            $(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
            //修改search图标，由fa fa-save改为fa fa-search
            $(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-search").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
            $(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

            $(".ui-icon.ui-icon-seek-prev").wrap("<div class='btn btn-sm btn-default'></div>");
            $(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

            $(".ui-icon.ui-icon-seek-first").wrap("<div class='btn btn-sm btn-default'></div>");
            $(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

            $(".ui-icon.ui-icon-seek-next").wrap("<div class='btn btn-sm btn-default'></div>");
            $(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

            $(".ui-icon.ui-icon-seek-end").wrap("<div class='btn btn-sm btn-default'></div>");
            $(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");

            $(window).on('resize.jqGrid', function () {
                $("#"+element).jqGrid('setGridWidth', $("#content").width()-50);
            })
        }

        //对表单进行验证
        $("#_submit").click(function(){
            //获取所有 需要选择的参数
            var time_type = $('#time_type').val();
            var time = $('#_time').val();
            var $error_infor = '';
            var $error_signal = 0;
            if(!time_type){
                $error_infor = '时间类型不能为空';
                $error_signal = 1;
            }
            if(!time){
                $error_infor = '时间不能为空';
                $error_signal = 1;
            }
            if($error_signal){
                $('#_error_log').text($error_infor);
                $('#_error_log').show();
                return false;
            }
        });

        //能源监测菜单下的数据表格，更改单页显示条数操作，监听select.ui-pg-selbox的change方法，调用showbytype方法
        $("select.ui-pg-selbox").on("change",function(){
            var time_type = time[$("#time_").attr("name")];
            showbytype(time_type);
        });

    }
</script>