<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
//echo '<pre>';
//print_r($tree_json);
//die;
?>

<!DOCTYPE html>
<html>
<head>


    <?php
    $this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

    $this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/fancytree/jquery.fancytree.dnd.js", ['backend\assets\AppAsset']);
    ?>

    <!-- (Irrelevant source removed.) -->

    <style type="text/css">
        /* Define custom width and alignment of table columns */
        #treetable {
            table-layout: fixed;
        }
        #treetable tr td:nth-of-type(1) {
            text-align: right;
        }
        #treetable tr td:nth-of-type(2) {
            text-align: center;
        }
        #treetable tr td:nth-of-type(3) {
            min-width: 100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>


</head>

<body>




<h3> Plain tree </h3>

<p id="plainTreeStyles">
    <b>Add container class:</b><br>
    <!--
        <label><input type="checkbox" data-class="fancytree-colorize-hover"> fancytree-colorize-hover</label>
    -->
    <label><input type="checkbox" data-class="fancytree-colorize-selected"> fancytree-colorize-selected</label>
</p>

<div class="panel panel-default">


    <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id'])?>
    <div class="input-group" id = 'category_tree'>
        <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
            <ul id = 'select_category' class="select2-choices" style = 'min-height: 30px'>
            </ul>
        </div>

        <div class="input-group-btn">
            <button class="btn btn-default btn-primary" type="button"  id = '_clear'>
                <i class="fa fa-mail-reply-all"></i> Clear
            </button>
        </div>
    </div>
    <div id="tree" class="panel-body fancytree-colorize-hover">
    </div>

    <div id="tree1" class="panel-body fancytree-colorize-hover">
</div>







<!-- (Irrelevant source removed.) -->
</body>
</html>

<script  >
    window.onload = function() {


        //fancytree 树状多选框初始化
        var $current_keys = [];
        var $count = 0;
        console.log(<?=$tree_json;?>);
        function delete_all_category() {
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        }

        $("#tree").fancytree({
            extensions: ["glyph", "edit", "wide"],
            checkbox: true,
            selectMode: 1,
            toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
            glyph: {
                map: {
                    doc: "glyphicon glyphicon-file",
                    docOpen: "glyphicon glyphicon-file",
                    checkbox: "glyphicon glyphicon-unchecked",
                    checkboxSelected: "glyphicon glyphicon-check",
                    checkboxUnknown: "glyphicon glyphicon-share",
                    error: "glyphicon glyphicon-warning-sign",
                    expanderClosed: "glyphicon glyphicon-plus-sign",
                    expanderLazy: "glyphicon glyphicon-plus-sign",
                    // expanderLazy: "glyphicon glyphicon-expand",
                    expanderOpen: "glyphicon glyphicon-minus-sign",
                    // expanderOpen: "glyphicon glyphicon-collapse-down",
                    folder: "glyphicon glyphicon-folder-close",
                    folderOpen: "glyphicon glyphicon-folder-open",
                    loading: "glyphicon glyphicon-refresh"
                    // loading: "icon-spinner icon-spin"
                }
            },
            wide: {
                iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
            },
            source:<?=$tree_json?>,
//                [
//                {"title": "Animalia", "expanded": true, "folder": true, "children": [
//                    {"title": "Chordate", "folder": true, "children": [
//                        {"title": "Mammal", "children": [
//                            {"title": "Primate", "children": [
//                                {"title": "Primate", "children": [
//                                ]},
//                                {"title": "Carnivora", "children": [
//                                ]}
//                            ]},
//                            {"title": "Carnivora", "children": [
//                                {"title": "Felidae", "lazy": true}
//                            ]}
//                        ]}
//                    ]},
//                    {"title": "Arthropoda", "expanded": true, "folder": true, "children": [
//                        {"title": "Insect", "children": [
//                            {"title": "Diptera", "lazy": true}
//                        ]}
//                    ]}
//                ]}
//            ],

            select: function(event, data) {
                // Get a list of all selected nodes, and convert to a key array:
                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                    return node.key;
                });
                console.log('当前所有分类' + '_________________________' + $sel_keys);
                if ($sel_keys.length >= $current_keys.length) {
                    for (var i = 0; i < $sel_keys.length; i++) {

                        if($.inArray($sel_keys[i], $current_keys) === -1) {
                            console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                            console.log('上次所选的分类 --- ' + $current_keys);
                            console.log('当前选择分类 --- ' + $sel_keys[i]);
                            var $current_key = $sel_keys[i];
                            var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                            for (var j = 0; j < $_other_sibling.length; j++) {
                                console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                $_other_sibling[j].setSelected(false);
                                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                    return node.key;
                                });
                            }
                        }

                    }
                }

                //获取所有选中的分类将其加入到选择框中
                var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                    console.log(node.key + ' ---- ' + node.title);
                    var $_html = '';
                    $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                        "<div>" + node.title + "</div>" +
                        "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                        "</li>";
                    return $_html;
                });
                //将分类选择框中的分类全部清除
                delete_all_category();
                $('#select_category').append($_html);
                //将选择的分类id放入分类隐藏框中
                $('#category_id').val($sel_keys);
                //将最新所选的分类集合跟新到$current_keys 里
                $current_keys = $sel_keys;
                console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
            }
        });

        $('#_clear').click(function () {
            delete_all_category();
            $("#tree").fancytree();
        });

        $('#category_tree').click(function() {
            $('#tree').toggle();
        });
        $('#tree').mouseleave(function() {
            $('#tree').hide();
        });
        //提交验证
        $('#_sub').click(function() {

        });

//        glyph_opts = {
//            map: {
//                doc: "glyphicon glyphicon-file",
//                docOpen: "glyphicon glyphicon-file",
//                checkbox: "glyphicon glyphicon-unchecked",
//                checkboxSelected: "glyphicon glyphicon-check",
//                checkboxUnknown: "glyphicon glyphicon-share",
//                dragHelper: "glyphicon glyphicon-play",
//                dropMarker: "glyphicon glyphicon-arrow-right",
//                error: "glyphicon glyphicon-warning-sign",
//                expanderClosed: "glyphicon glyphicon-plus-sign",
//                expanderLazy: "glyphicon glyphicon-plus-sign",  // glyphicon-expand
//                expanderOpen: "glyphicon glyphicon-minus-sign",  // glyphicon-collapse-down
//                folder: "glyphicon glyphicon-folder-close",
//                folderOpen: "glyphicon glyphicon-folder-open",
//                loading: "glyphicon glyphicon-refresh"
//            }
//        };
//        $(function(){
//            // Initialize Fancytree
//            $("#tree1").fancytree({
//                extensions: ["dnd", "edit", "glyph", "wide"],
//                checkbox: true,
//                dnd: {
//                    focusOnClick: true,
//                    dragStart: function(node, data) { return true; },
//                    dragEnter: function(node, data) { return false; },
//                    dragDrop: function(node, data) { data.otherNode.copyTo(node, data.hitMode); }
//                },
//                glyph: glyph_opts,
//                selectMode:1,//1单选 2 多选
//                source: [
//                    {"title": "Animalia", "expanded": true, "folder": true, "children": [
//                        {"title": "Chordate", "folder": true, "children": [
//                            {"title": "Mammal", "children": [
//                                {"title": "Primate", "children": [
//                                    {"title": "Primate", "children": [
//                                    ]},
//                                    {"title": "Carnivora", "children": [
//                                    ]}
//                                ]},
//                                {"title": "Carnivora", "children": [
//                                    {"title": "Felidae", "lazy": true}
//                                ]}
//                            ]}
//                        ]},
//                        {"title": "Arthropoda", "expanded": true, "folder": true, "children": [
//                            {"title": "Insect", "children": [
//                                {"title": "Diptera", "lazy": true}
//                            ]}
//                        ]}
//                    ]}
//                ],
//                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
//                wide: {
//                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
//                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
//                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
//                },
//
//                iconClass: function(event, data){
//                    // if( data.node.isFolder() ) {
//                    //   return "glyphicon glyphicon-book";
//                    // }
//                },
//                lazyLoad: function(event, data) {
//
//                }
//            });
//
//
//        });
    }
</script>