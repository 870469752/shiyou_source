<?php
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
$testdata=array(['Firefox',45.0],
    ['IE',26.8],
    ['Chrome',12.8],
    ['Safari',8.5],
    ['Opera',6.2],
    ['Others',0.7],
);

// echo'<pre>';
//print_r($data);
//die;
?>




<section id="widget-grid" class="">

    <!-- row -->
        <div class="row">

    <!-- NEW WIDGET START -->
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
            >
            <header>

                <div class="jarviswidget-ctrls">
                    <a style = 'display:none;' href = '<?=Url::toRoute('export-statistic-category').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                        <i class="fa fa-file-excel-o"></i>
                    </a>
                    <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                        <i class="fa fa-bar-chart-o"></i>
                    </a>

                </div>

                <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                <h2>能源分类 </h2>
            </header>
                <div >
                <!-- 表单start-->
                <?php $form = MyActiveForm::begin(['method' => 'get']) ?>

                <fieldset>

                    <div class="form-group">
                        <div class="col-md-1"></div>
<!--                        --><?//=Html::hiddenInput('parent_category_ids', isset($parent_category_id) ? $parent_category_id : '')?>
<!--                        <div class="col-md-1">-->
<!--                           --><?//=Html::label('参数设定')?>
<!--                            </div>-->
                        <div class="col-md-1">
                            <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour'),

                            ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                        </div>

                        <div class="col-md-2">
                            <?=Html::textInput('date_time', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'placeholder' => '选择时间', 'id' => '_time'])?>
                        </div>


                        <div class="col-md-2">
                            <!-- 分类 隐藏框 -->
                            <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id'])?>
                            <div class="input-group" id = 'category_tree' style="width: 100%;">
                                <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                    <ul id = 'select_category' class="select2-choices  choices" style = 'min-height: 30px'>
                                    </ul>
                                </div>

                                <!--                        <div class="input-group-btn">-->
                                <!--                            <button class="btn btn-default btn-primary" type="button"  id = '_clear'>-->
                                <!--                                <i class="fa fa-mail-reply-all"></i> Clear-->
                                <!--                            </button>-->
                                <!--                        </div>-->
                            </div>
                            <div id = 'tree' style = 'display:none;position:absolute;width:100%'></div> </div>



                        <div class="col-md-2" >
                            <!-- 分类 隐藏框 -->
                            <?=Html::hiddenInput('Point[category_id1]', null, ['id' => 'category_id1'])?>
                            <div class="input-group" id = 'category_tree1' style="width: 100%;">
                                <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name1' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                                    <ul id = 'select_category1' class="select2-choices choices1" style = 'min-height: 30px'>
                                    </ul>
                                </div>

                                <!--                        <div class="input-group-btn">-->
                                <!--                            <button class="btn btn-default btn-primary" type="button"  id = '_clear1'>-->
                                <!--                                <i class="fa fa-mail-reply-all"></i> Clear-->
                                <!--                            </button>-->
                                <!--                        </div>-->
                            </div>
                            <div id = 'tree1' style = 'display:none;position:absolute;width:100%'></div></div>

                        <div class="col-md-2" >
                            <div style = 'color:red;height: 40px;text-align: center; displau:none' id = '_error_log'></div>
                        </div>

                        <div class="col-md-2" >
                            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                        </div>
                    </div>

                </fieldset>

                <?php MyActiveForm::end(); ?>
                <!-- 表单end-->
            </div>

            <!-- widget div-->

            <div id="tabs" class="col-lg-6 " style="width:50% ">

                <ul>
                    <?php foreach ($top as $key=>$value) {
                        echo '<li>'
                            .'<a href="#tabs-'.$key.'">'.$value.'</a>'
                            .'</li>';

                    } ?>
                </ul>

                <?php foreach ($top as $key=>$value) {
                    echo  '<div id="tabs-'.$key.'" style="height: 600px" >'
                        .'<div class="accordion">'
                        .'<div>'
                        .'<h4>饼图数据</h4>'
                        .'<div class="padding-10">'
                        .'<div id="tabs-'.$key.'-pie">'
                        .'</div>'
                        .'</div>'
                        .'</div>'

                        .'<div>'
                        .'<h4>列表详细数据</h4>'
                        .'<div class="padding-10">'
                        .'<div id="tabs-'.$key.'-chart">'
                        .'</div>'
                        .'</div>'
                        .'</div>'


                        .'</div>'
                        .'</div>';
                } ?>

            </div>


            <div id="tabs2" class="col-lg-6 " style="width:50%;height: 600px ">

                <ul>
                    <li>
                        <a id="href12"href="#container1">下级</a>
                    </li>

                </ul>
                <div id="container1" style="height: 600px">


                    <div class="accordion">
                        <div>
                            <h4>饼图数据</h4>
                            <div class="padding-10">
                                <div id="container12" >

                                </div>

                            </div>
                        </div>

                        <div>
                            <h4>列表详细数据</h4>
                            <div class="padding-10">
                                <div id="container21">
                                </div>
                            </div>
                        </div>


                    </div>

                </div>


            </div>



        </div>



        <!-- end widget -->
    </article>
        </div>
</div>



</section>
<!--</section>-->




<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
//$this->registerJsFile('js/highcharts/highchartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);



$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>
<script>
    //    var  testdata=<?//=json_encode($testdata)?>//;
    //    console.log(testdata);
    //      console.log(<?//=json_encode($data)?>//);
    window.onload = function(){
//    var $chart_data = <?//= isset($chart_data) ? $chart_data : "''"?>//;
//    var $_categories = <?//= isset($categories) ? $categories : "''"?>//;
        var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
        var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
        var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
        var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var $display_type = <?= isset($display_type) ? "'" .$display_type ."'" : "''"?>;

        var chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        var _categories = <?= isset($categories) ? $categories : "''"?>;
        var $__type = '日';
        switch($_type) {
            case 'day':
                $__type = '日';
                break;
            case 'month':
                $__type = '月';
                break;
            case 'year':
                $__type = '年';
                break;
        }
        //初始化tabs窗口
        $('#tabs').tabs();
        //初始化accordion窗口
        var accordionIcons = {
            header: "fa fa-plus",    // custom icon class
            activeHeader: "fa fa-minus" // custom icon class
        };
        $(".accordion").accordion({
            autoHeight : false,
            heightStyle : "content",
            collapsible : true,
            animate : 300,
            icons: accordionIcons,
            header : "h4",
        })

        //对表单进行验证
        $("#_submit").click(function(){
            //获取所有 需要选择的参数
            var time_type = $('#time_type').val();
            var time = $('#_time').val();
            var $error_infor = '';
            var $error_signal = 0;
            if(!time_type){
                $error_infor = '时间类型不能为空';
                $error_signal = 1;
            }
            if(!time){
                $error_infor = '时间不能为空';
                $error_signal = 1;
            }
            if($error_signal){
                $('#_error_log').text($error_infor);
                $('#_error_log').show();
                return false;
            }

        })

        $("#time_type").change(function (){
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });
        $("#time_type").trigger('change');
        $('#_time').val($_date_time);

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
                //if(jqgrid_data.length){$('._jarviswidget').show();}
            }
            $("#_loop").slideToggle("slow");

        });



        $('#tabs').tabs();
        $('#tabs2').tabs();





//    var _categories=data['170']['detail']['categories'];
//    var chart_data=data['170']['detail']['chart_data'];
        //highChart('container_chart','column',170);


        /********************************柱状图-start************************************************/

        //highChart('column');
        //highChart('column',[0,1],[0,133]);
        //ajax_data(170);
        function ajax_data(id){
            var query_param=JSON.stringify(<?=json_encode($query_param)?>)
            window.location.href="/category-data/crud/columndata?id="+id+"&query_param="+query_param;
//        $.ajax({
//        type: "POST",
//        url: "/category-data/crud/getpoint",
//        data: {id:id,query_param:<?//=json_encode($query_param)?>//},
//        success: function (msg) {
//           console.log(msg);
//        }
//    });

        }

        // highchart 代码
        function highChart(element,type,id){
            var data=<?=json_encode($data)?>;
            var _categories = [];
            var chart_data = [];
            console.log(data);
            console.log(id);
            console.log(data[id]);
            console.log(data[id]['detail']);
            if(data[id]['detail'].length!=0) {
                _categories = data[id]['detail']['categories'];
                chart_data = data[id]['detail']['chart_data'];
            }
            console.log(_categories);
            console.log()
            $('#'+element).highcharts({
                chart: {
                    type: type,
                    zoomType: 'x'
                },
                title: {
                    text: $category_name
                },
                subtitle: {
                    text: $_date_time + ' ' + '<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>' + '累计量'
                },
                xAxis: {
                    categories: _categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },

                legend: {
//                align: 'right',
//                x: -30,
//                verticalAlign: 'bottom',
//                y: 25,
//                floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1
//                shadow: false
                },

                series: [{
                    name: $category_name,
                    data: chart_data,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }]
            });

        }




        //图形样式切换
        $('.radiobox').click(function(){
            var $_chart_type = $(this).data('value');
            if($_chart_type){
                highChart($_chart_type,_categories,chart_data);
            }
        })


        /********************************柱状图-end************************************************/
        /********************************表格-start************************************************/
        //初始化表格
        // id为 string id
        // 或    数组data数据
        function initGridview(element,id){
            var chartdata;
            if(typeof(id)=='string'){
                var data=<?=json_encode($data)?>;
                chartdata=data[id]['children'];
            }
            else {
                var chartdata=id;
            }

            var html= '<div id="'+element+'" style="height: 400px" > <table class="table table-bordered table-striped"><thead><tr><th>name</th> <th>value</th> </tr> </thead>'
                +'<tbody>';
            //循环写入
            for(var key in chartdata) {
                //console.log(key+ '  '+chartdata[key])
                html=html+'<tr>'
                    + '<td>'+chartdata[key]['name']+'</td>'
                    + '<td>'+chartdata[key]['value']+'</td>'
                    + '</tr>';
            }
            html=html+'</tbody>'
                +'</table></div>';
            $('#'+element).replaceWith(html);
            $('#tabs').tabs();
        }

        /*********************************表格-end***************************************************/




        /******************************饼图-start**************************************/

        // 单层饼图 by id
        function pie1(element1,id) {
            var id_link=Array();
            id_link.push(id);
            var data=pie1data(id);
            var text;
            if(pie1data(id).length==0){
                text='无数据'
            }

            else text ='详细数据';
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: '详细信息',
                                onclick: function () {
                                    console.log('详细信息');
                                    initview(element1,id);
                                }
                            }, {
                                separator: true
                            }]
                                //自带的到处等功能按钮
                                .concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: text
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
//                        dataLabels: {
//                            distance: 40,//数据标签距离边缘的距离值 为负数就越靠近饼图中心
//                            rotation: 0,//数据标签旋转角度
//                            enabled: true,
//                            format: '{point.name}'
//                        }
//                    ,
//                    events: {
//                        click: function(event) {
//                            alert(this.name +' clicked\n'+'highcharts');
//                        }
//                    }
                    }
                    ,
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                //调试用
                                //window.location.href="/category-data/crud/getpoint?id="+e.point.id;
                                //ajax 查询后再显示
                                $.ajax({
                                    type: "POST",
                                    url: "/category-data/crud/getpoint",
                                    data: {id: e.point.id},
                                    success: function (msg) {
//                                             console.log(msg);
                                        //返回false则有下级显示下级
                                        if (msg == 'false') {
                                            pie1("container12", e.point.id);
                                            initGridview("container21", pie1data(e.point.id));
                                        }
                                        else {
                                            //无下级 显示点位信息
                                            msg = '[' + msg + ']';
                                            var a = eval(msg);
//                                                 console.log(a[0]);
                                            $('#container12').show;
                                            pie2("container12", pie1data2(a[0]));
                                            initGridview("container21", a[0]);
                                        }
                                    }
                                });
//                                 }

                            }
                        }
                    }
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data: data
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
                }]
            });
        }

        //单层饼图 by data   显示分类下 点位信息
        function pie2(element1,data) {
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: '详细信息',
                                onclick: function () {
                                    console.log('详细信息');
                                }
                            }, {
                                separator: true
                            }]
                                //自带的到处等功能按钮
                                .concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: ' '
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
//                    dataLabels: {
//                        distance: 40,//数据标签距离边缘的距离值 为负数就越靠近饼图中心
//                        rotation: 0,//数据标签旋转角度
//                        enabled: true,
//                        format: '{point.name}'
//                    }
                    }
                    ,
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                console.log(e.point.id);
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data:data
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
                }]
            });
        }
        /******************************************二层饼状图**********************************************************************/
        //二层饼状图
        function init(element1,id) {
            var data1=pie2data(id);
//            console.log(data1.categorys);
//            console.log(data1.data);
            //二层饼状图
            var colors = Highcharts.getOptions().colors,
            //categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
                categories = data1.categorys,
                name = 'Browser brands',
                data =data1.data;

            //console.log(data);
            // console.log(piedata());
            // Build the data arrays
            var browserData = [];
            var versionsData = [];
            for (var i = 0; i < data.length; i++) {

                // add browser data
                browserData.push({
                    name: categories[i],
                    y: data[i].y,
                    id:data[i].id,
                    color: data[i].color
                });

                // add version data
                for (var j = 0; j < data[i].drilldown.data.length; j++) {
                    var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
                    versionsData.push({
                        name: data[i].drilldown.categories[j],
                        y: data[i].drilldown.data[j],
                        color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                        id:data[i].drilldown.id[j]
                    });
                }
            }



            pie1("container12",178);
            //initGridview("container21",178);
            // Create the chart
            $('#'+element1).highcharts({
                chart: {
                    type: 'pie'
                },
                lang: {
                    downloadJPEG: "下载JPEG 图片",
                    downloadPDF: "下载PDF文档",
                    downloadPNG: "下载PNG 图片",
                    downloadSVG: "下载SVG 矢量图",
                    exportButtonTitle: "导出图片"
                },
                exporting: {

                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: '详细信息',
                                onclick: function () {
                                    console.log('详细信息');
                                    initview(element1,id);
                                }
                            }, {
                                separator: true
                            }]
                            //自带的导出等功能按钮
                            //.concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)

                        }
                    }
                },
                title: {
                    text: ' '
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
                plotOptions: {
                    pie: {
                        shadow: false,
                        center: ['50%', '50%']
                    },
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        events: {
                            click: function (e) {
                                //console.log(111);
                                $('#container12').show;
                                console.log(e.point.id);
                                var data= <?=json_encode($data)?>;
                                console.log(e.point.id);
                                console.log(data);
                                //初始化单层饼图
                                pie1("container12",e.point.id);

                                var data= <?=json_encode($data)?>;
                                console.log(e.point.id);
                                //不太好 *****待解决//
                                var gridview_data=data[170]['children'][e.point.id]['children'];
                                initGridview("container21",gridview_data);



                            }
                        }
                    }
                },
                tooltip: {
                    valueSuffix: '%'
                },
                //去水印
                credits: {
                    enabled: false
                }
                ,
                series: [{
                    name: 'Browsers',
                    data: browserData,
                    size: '60%',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
//                    dataLabels: {
//                        formatter: function () {
//                            return this.y > 5 ? this.point.name : null;
//                        },
//                        color: 'white',
//                        distance: -30
//                    }
                }, {
                    name: 'Versions',
                    data: versionsData,
                    size: '80%',
                    innerSize: '60%',
                    //图例
//                    dataLabels: {
//                        enabled: false
//                    },
//                    showInLegend: true,
                    dataLabels: {
                        formatter: function () {
                            // display only if larger than 1
                            return this.y > 5 ? this.point.name : null;
                            //return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
                        },
                        color: 'black',
                        distance: 30
                    }
                }]
            });

        }


        //二层结构的数据
        function pie2data(id){
            var data= <?=json_encode($data)?>;
            var colors = Highcharts.getOptions().colors;
            var ldata=new Array();
            var total= data[id]['value'];
            var i=0;
            var rdata=new Array();
            var categorys_data=new Array();
            //console.log(total);
            var twodata=data[id]['children'];
            for (var piekey in twodata) {


                var categorys = new Array();
                var data = new Array();
                var id = new Array();
                categorys_data.push(twodata[piekey]['name']);

                var child = twodata[piekey]['children'];
                for (var childkey in child) {
                    var name = child[childkey]['name'];
                    var value = child[childkey]['value'];
                    categorys.push(name);
                    data.push(value);
                    id.push(childkey);

                }
                var name1 = child[childkey]['name'];
                a = {
                    y: parseFloat(((twodata[piekey]['value'] / total) * 100).toFixed(2)),
                    color: colors[i],
                    id: piekey,
                    drilldown: {
                        name: name1,
                        id: id,
                        categories: categorys,
                        data: data,
                        color: colors[i],
                    }
                }
                rdata.push(a);
                i++;
            }
            return {
                data:rdata,
                categorys:categorys_data
            };
        }


        //  处理相应的id  data[id]数据为一层饼图数据  data为整个能源分类树状数据
        function pie1data(id) {
            var data= <?=json_encode($data)?>;
            var colors = Highcharts.getOptions().colors;
            var ldata = new Array();
            var flag=false;
            var i = 1;
            var rdata = new Array();
            //判断id在哪个数组
            for(var key in data){
                if(key==id)
                    flag=true||flag;
            }
            if(flag) {
                var total = data[id]['value'];
                var twodata = data[id]['children'];
            }
            else{
                var total = data[170]['children'][id]['value'];
                var twodata = data[170]['children'][id]['children'];
            }
            //判断如果数据都为0 则返回空数组
            if(total==0){
                rdata=[];
            }
            else {
                for (var pie_key in twodata) {
                    var name1 = twodata[pie_key]['name'];
                    a = {
                        name: name1,
                        y: parseFloat(((twodata[pie_key]['value'] / total) * 100).toFixed(2)),
                        id: pie_key,
                        colors: colors[i]
                    }
                    rdata.push(a);
                    i++;
                }
            }
            return rdata;
        }


        //处理多个  数据为  一层饼图数据
        function pie1data2(data){
            var colors = Highcharts.getOptions().colors;
            var i = 1;
            //合值
            var sum=0;
            var rdata=new Array();
            //console.log(data);
            for(var key in data){
                sum=sum+parseFloat(data[key]['value']);
            }
////            console.log(sum);
//            if(sum==0){
//                rdata=[];
//            }
//            else {
            for (var key in data) {
                a = {
                    name: data[key]['name'],
                    y: (data[key]['value'] / sum) * 100,
                    id: key,
                    colors: colors[i]
                }
                rdata.push(a);
                i++;
            }
//            }
            return rdata;
        }


        //id是否在data的一层数组键值中 返回ture+键值 false+null
        function Isin(id,data){
            var flag=false;
            var key=null;
            for(var key in data){
                if(key==id) {
                    flag = true || flag;
                    key=key;
                }
            }
            return {
                flag:falg,
                key:key
            };
        }

        /******************************双层饼图-end*******************************************/


        /******************************饼图-end*******************************************/


            //初始化各个tabs-id div
            //打印 执行函数 0-0
            //170电 为双层饼图初始化init()
            //其他  为单层饼图初始化pie1()
            //initGridview() 表格===待替换
            //highChart()
        <?php foreach($top as $key=>$value){
                if($key==170)  {
                    //双层不要
                    //echo ' init("tabs-'.$key.'-pie","'.$key.'");';
                     echo'pie1("tabs-'.$key.'-pie","'.$key.'");';
                    echo'initGridview("tabs-'.$key.'-chart","'.$key.'");';
                    //echo'highChart("tabs-'.$key.'-chart-next","column",'.$key.');';
                }
                else {
                    echo 'initGridview("tabs-'.$key.'-chart","'.$key.'");';
                    echo'pie1("tabs-'.$key.'-pie","'.$key.'");';
                    //echo'highChart("tabs-'.$key.'-chart-next","column",'.$key.');';
            }
        }
        ?>
        console.log(<?=json_encode($data)?>);
        //highChart('container11','column',170);
//    pie2("test",pie1data(111));



        /*****************************************************************************************/
        //TODO  找树状图路径  ***待完成
        function Findpath(id) {
            var data = <?=json_encode($data)?>;
            var path = new Array();
            var flag = Isin(id, data);
            //如果不在 往下遍历
            if (!Isin(id, data).flag) {
                for (var key in data) {
                    //记录路径
                    path.push(key);
                    if (!Isin(id, data[key]).flag) {

                    }

                }

            }
        }
        /*****************************************************************************************/


        //初始化能源分类 与 地理位置分类 框
        initfancytree(<?=$tree_json_energy?>,'');
        initfancytree(<?=$tree_json_location?>,1);


        //初始化树形下拉列表框  data为树状数据源  id 为元素后缀名
        function initfancytree(data,id){
            //fancytree 树状多选框初始化
            var $current_keys = [];
            var $count = 0;

            function delete_all_category() {
                $('.choices'+id+' li').each(function(){
                    if($(this).data('type') != 'plus'){
                        $(this).remove();
                    }
                })
            }
            console.log("#tree"+id);

            $("#tree"+id).fancytree({
                extensions: ["glyph", "edit", "wide"],
                checkbox: true,
                selectMode: 1,
                toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                glyph: {
                    map: {
                        doc: "glyphicon glyphicon-file",
                        docOpen: "glyphicon glyphicon-file",
                        checkbox: "glyphicon glyphicon-unchecked",
                        checkboxSelected: "glyphicon glyphicon-check",
                        checkboxUnknown: "glyphicon glyphicon-share",
                        error: "glyphicon glyphicon-warning-sign",
                        expanderClosed: "glyphicon glyphicon-plus-sign",
                        expanderLazy: "glyphicon glyphicon-plus-sign",
                        // expanderLazy: "glyphicon glyphicon-expand",
                        expanderOpen: "glyphicon glyphicon-minus-sign",
                        // expanderOpen: "glyphicon glyphicon-collapse-down",
                        folder: "glyphicon glyphicon-folder-close",
                        folderOpen: "glyphicon glyphicon-folder-open",
                        loading: "glyphicon glyphicon-refresh"
                        // loading: "icon-spinner icon-spin"
                    }
                },
                wide: {
                    iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                    iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                    levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                },
                source:data,
                select: function(event, data) {
                    // Get a list of all selected nodes, and convert to a key array:
                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                        return node.key;
                    });
                    console.log('当前所有分类' + '_________________________' + $sel_keys);
                    if ($sel_keys.length >= $current_keys.length) {
                        for (var i = 0; i < $sel_keys.length; i++) {

                            if($.inArray($sel_keys[i], $current_keys) === -1) {
                                console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                console.log('上次所选的分类 --- ' + $current_keys);
                                console.log('当前选择分类 --- ' + $sel_keys[i]);
                                var $current_key = $sel_keys[i];
                                var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                for (var j = 0; j < $_other_sibling.length; j++) {
                                    console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                    $_other_sibling[j].setSelected(false);
                                    var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                        return node.key;
                                    });
                                }
                            }

                        }
                    }

                    //获取所有选中的分类将其加入到选择框中
                    var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                        console.log(node.key + ' ---- ' + node.title);
                        var $_html = '';
                        $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                            "<div>" + node.title + "</div>" +
                            "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                            "</li>";
                        return $_html;
                    });
                    //将分类选择框中的分类全部清除
                    delete_all_category();
                    $('#select_category'+id).append($_html);
                    //将选择的分类id放入分类隐藏框中
                    $('#category_id'+id).val($sel_keys);
                    //将最新所选的分类集合跟新到$current_keys 里
                    $current_keys = $sel_keys;
                    console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                }
            });

            $('#_clear'+id).click(function () {
                delete_all_category();
                $("#tree").fancytree();
            });

            $('#category_tree'+id).click(function() {
                $('#tree'+id).toggle();
            });
            $('#tree'+id).mouseleave(function() {
                $('#tree'+id).hide();
            });
        }




        //提交验证
        $('#_sub').click(function() {

        });

    }
</script>