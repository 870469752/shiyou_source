<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
//echo '<pre>';
//print_r($category_ids);
//die;
?>

<!-- html 区域 start-->
<section id="widget-grid" class="" style="display:inline;width: 80%">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
            <div style = 'padding:0px 7px 15px 7px'>
            <div id="drag" style="position:absolute;z-index:110;width: 220px" draggable="true">
                <div class="jarviswidget jarviswidget-color-blueDark"
                         data-widget-deletebutton="false"
                         data-widget-editbutton="false"
                         data-widget-colorbutton="false"
                         data-widget-fullscreenbutton='false'>

                </div>
                </div>
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>

            <fieldset>
                <legend>参数设定</legend>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('parent_category_ids', isset($parent_category_id) ? $parent_category_id : '')?>
                    <div class="col-md-3">
                        <?=MyHtml::dropDownList('category_ids', isset($category_ids)?$category_ids:null, isset($category_tree) ? $category_tree :  []
                            , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'day' => Yii::t('app', 'Day'),
                                'hour' => Yii::t('app', 'Hour'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=Html::textInput('date_time', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'placeholder' => '选择时间', 'id' => '_time'])?>
                    </div>
                    <!-- 查看的 分类id-->
                </div>

            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark _jarviswidget"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false">
        <header>
            <div class="jarviswidget-ctrls">
                <a style = 'display:none;' href = '<?=Url::toRoute('export-statistic-category').'?export=1&'.http_build_query(Yii::$app->request->getQueryParams());?>' id="_save" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>

            </div>
            <span class="widget-icon">
                 <i class="fa fa-table"></i>
            </span>
            <h2><?=isset($category_name) ? $category_name : ' '?></h2>
            <h2>实时数据</h2>
        </header>
<!--         widget div-->

        <div class="no-padding">

            <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                <div class="col-md-10" >
                     <label class="radio radio-inline"  style = 'margin-top:0px'>
                         <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>
                         <span>曲线图</span>
                     </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'column' checked>
                        <span>柱状图</span>
                    </label>
                    <label class="radio radio-inline" style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'bar'>
                        <span>柱状图_2</span>
                    </label>
                </div>

        </div>

            <div id="container_chart"></div>

            <div id="container_table">
                <?=Html::hiddenInput('content_type', 'chart')?>
                <?php \yii\widgets\Pjax::begin(); ?>
                    <?php if(isset($category_data_info['name'])):?>
                    <table class="table table-striped table-bordered table-hover table-middle">
                        <thead>
                            <tr>
                                <th data-hide="phone,tablet">时间 (<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>)</th>
                                <th data-hide="phone,tablet">分类名称</th>
                                <th data-hide="phone,tablet">数值</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($category_data_info['name'] as $key => $value):?>
                            <tr>
                                <td><?=$query_param['date_time']?></td>
                                <td><?=$value?></td>
                                <td><?=$category_data_info['data'][$key]?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    <?php endif;?>
                    </table>

                <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $pages
                    ])
                ?>
                <?php \yii\widgets\Pjax::end(); ?>

            </div>

        </div>






    </div>
    <!-------     图表内容 end-->
</section>
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/skin-bootstrap/skin-win7/ui.fancytree.css", ['backend\assets\AppAsset']);

//$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
//$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
//$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
//$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>

<!-- js 脚本区域 start-->
<script>
window.onload = function(){
//    $('#drag').draggable();
//    $("#tree").fancytree({
//        activeVisible: true, // Make sure, active nodes are visible (expanded).
//        aria: false, // Enable WAI-ARIA support.
//        autoActivate: true, // Automatically activate a node when it is focused (using keys).
//        autoCollapse: false, // Automatically collapse all siblings, when a node is expanded.
//        autoScroll: false, // Automatically scroll nodes into visible area.
//        clickFolderMode: 4, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
//        checkbox: false, // Show checkboxes.
//        debugLevel: 2, // 0:quiet, 1:normal, 2:debug
//        disabled: false, // Disable control
//        focusOnSelect: false, // Set focus when node is checked by a mouse click
//        generateIds: false, // Generate id attributes like <span id='fancytree-id-KEY'>
//        idPrefix: "ft_", // Used to generate node id´s like <span id='fancytree-id-<key>'>.
//        icons: false, // Display node icons.
//        keyboard: true, // Support keyboard navigation.
//        keyPathSeparator: "/", // Used by node.getKeyPath() and tree.loadKeyPath().
//        minExpandLevel: 1, // 1: root node is not collapsible
//        quicksearch: false, // Navigate to next node by typing the first letters.
//        selectMode: 2, // 1:single, 2:multi, 3:multi-hier
//        tabbable: true, // Whole tree behaves as one single control
//        titlesTabbable: false // Node titles can receive keyboard focus
//    });
//    $("#tree").fancytree("getRootNode").visit(function(node){
//        node.setExpanded(true);
//    });
//    $('.ui-fancytree').css({'height': '400px'});
//   $("#tree").click(function ( ) {
//       console.log(window.event.srcElement);
//   })



    var $chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
    var $_categories = <?= isset($categories) ? $categories : "''"?>;
    var $category_name = <?= isset($category_name) ? "'" .$category_name ."'" : "''"?>;
    var $_unit = <?= isset($category_unit) ? "'" .$category_unit ."'" : "''"?>;
    var $_statistic_prefix = <?= isset($statistic_prefix) ? "'" .$statistic_prefix ."'" : "''"?>;
    var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
    var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
    var $display_type = <?= isset($display_type) ? "'" .$display_type ."'" : "''"?>;
    var $__type = '日';

        console.log($category_name);
        console.log(111);
        console.log($chart_data);

    switch($_type) {
        case 'day':
            $__type = '日';
            break;
        case 'month':
            $__type = '月';
            break;
        case 'year':
            $__type = '年';
            break;
    }
console.log($_categories);
    // 图、表切换

    $('#_chart_switch').click(function(){
        var $_chart_type = $(this).data('type');
        if($_chart_type == 'chart'){
            $('#container_chart').hide();
            $('#chart_type').hide();
            $('#container_table').show();
            $('#_save').show();
            $(this).data('type', 'table');
        }else{
            $('#container_chart').show();
            $('#chart_type').show();
            $('#container_table').hide();
            $('#_save').hide();
            $(this).data('type', 'chart');
        }
    });

    //根据 display_type 判断上次用户请求的是哪个 标签页
    if($display_type == 'table') {
        $('#_chart_switch').data('type', 'chart');
        $('#_chart_switch').trigger('click');
    }else {
        $('#_chart_switch').data('type', 'table');
        $('#_chart_switch').trigger('click');
    }

    $('#_cl').click(function(){
        if($("#_loop").css('display') == 'none'){
            $('#_line').hide();
        }else{
            $('#_line').show();
            if($chart_data.length){$('._jarviswidget').show();}
        }
        $("#_loop").slideToggle("slow");

    });

    $("#time_type").change(function (){
        _type = $(this).val();
        timeTypeChange(_type, $('#_time'));
    });
    $("#time_type").trigger('change');
    $('#_time').val($_date_time);
    // 图形
    highcharts_lang_date();
    highChart('column');
    // highchart 代码
    function highChart(type){
        $('#container_chart').highcharts({
            chart: {
                type: type,
                zoomType: 'x'
            },
            title: {
                text: $category_name
            },
            subtitle: {
                text: $_date_time + ' ' + '<?=Yii::t('app', isset($query_param['time_type'])?ucfirst($query_param['time_type']):'')?>' + '累计量'
            },
            xAxis: {
                categories: $_categories
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },

            legend: {
//                align: 'right',
//                x: -30,
//                verticalAlign: 'bottom',
//                y: 25,
//                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1
//                shadow: false
            },

            series: [{
                name: $category_name,
                data: $chart_data,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });

    }


    //图形样式切换
    $('.radiobox').click(function(){
        var $_chart_type = $(this).data('value');
        if($_chart_type){
            highChart($_chart_type);
        }
    })

    $('.prev').next().find('a').trigger('click');
}
</script>
<!-- js 脚本区域 end-->