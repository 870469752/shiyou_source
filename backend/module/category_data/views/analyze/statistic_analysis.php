<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 * 本页面 全部为ajax 请求 如需改为同步 请设置ajax配置参数
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use common\library\MyHtml;
use yii\data\Pagination;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .loader {
        background:#fff url('/img/Preloader_1.gif') no-repeat center;
        opacity:0.7;
        filter:alpha(opacity=50);
        -moz-opacity:0.5;
        width:200px;
        height:200px;
    }
</style>

<!-- html 区域 start-->
<section id="widget-grid" class="">
<!-- 分隔提示线 -->
<hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>
<!-- 参数主体 start-->
<div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
    <div style = 'padding:0px 7px 15px 7px'>
        <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
        <fieldset>
            <legend>参数设定</legend>
            <div class="form-group">
                <div class="col-md-1"></div>
                <?=Html::hiddenInput('parent_category_ids', isset($params['parent_category_ids']) ? $params['parent_category_ids'] : 2, ['id' => 'category_id'])?>
                <?php if(isset($params['tag']) && $params['tag'] == 1):?>
                    <div class="col-md-3">
                            <?=MyHtml::dropDownList('category_ids', isset($query_param['category_ids'])?$query_param['category_ids']:null, isset($category_tree) ? $category_tree :  []
                            , ['placeholder' => Yii::t('app', 'Choose Category'), 'title' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'category_ids'])?>
                    </div>
                <?php endif;?>
                <div class="col-md-1"></div>
                <div class="col-md-3" rel="tooltip" data-placement="top" data-original-title="">
                    <?=Html::dropDownList('time_type', isset($params['time_type'])?$params['time_type']:null, ['' => '',
                            'year' => Yii::t('app', 'Year'),
                            'month' => Yii::t('app', 'Month'),
                            'day' => Yii::t('app', 'Day'),
//                            'hour' => Yii::t('app', 'Hour'),
                            //                                                              'custom' => Yii::t('app', 'Custom')
                        ],
                        ['class' => 'select2','title' => Yii::t('app', 'Time Type'), 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <?=Html::textInput('date_time', isset($params['date_time'])?$params['date_time']:null,  ['class' => 'form-control', 'title' =>'选择时间', 'placeholder' => '选择时间', 'id' => '_time'])?>
                </div>
                <!-- 查看的 分类id-->
            </div>

        </fieldset>
    </div>
    <!--提交-->

    <?php MyActiveForm::end(); ?>
    </div>
<div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
<!-- 参数主体 end-->

    <!-- content  饼图内容 start-->
    <div class = 'row'>
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
            <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget_1'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?= isset($category_name) ? $category_name  : '饼图占比'?></h2>
            <div class="jarviswidget-ctrls">
                <a style = 'display:none;' href = 'javascript:void(0)' class="button-icon _save" href="#" data-toggle="modal" data-type="pie" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                    <i class="fa fa-file-excel-o"></i>
                </a>
                <a class="button-icon _chart_switch" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>
            </div>
        </header>
        <!-- widget div-->
        <div class="no-padding" style="min-height:580px;">

            <div id="chart_pie" class = 'container_chart' style = 'margin-top:55px;margin-bottom: 120px'></div>


            <div id = 'pie_table' class="container_table" data-table = 'pie' style = 'display:none'>
                <?php \yii\widgets\Pjax::begin(); ?>
                <table class="table table-striped table-bordered table-hover table-middle">
                    <thead>
                    <tr>
                        <th data-hide="phone,tablet">分类名称</th>
                        <th data-hide="phone,tablet">数值</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <?php \yii\widgets\Pjax::end(); ?>
                <div id ='pagination-pie' class = 'pagination'></div>

            </div>

        </div>
        </div>
        </article>
     <!-------     饼图内容 end-->
    <!-- 总用量 start-->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                     data-widget-Collapse="false"
                     data-widget-custom="false"
                     data-widget-fullscreenbutton = 'false'
                     data-widget-togglebutton="false" id = '_jarviswidget_2'>
                    <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                        <h2><?= isset($category_name) ? $category_name  : '总量对比'?></h2>
                        <div class="jarviswidget-ctrls">
                            <a class="button-icon _chart_type" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图形样式" data-placement="bottom">
                                <select id = 'total_type' style = 'color: #000000'>
                                    <option value = 'column'>柱状图</option>
                                    <option value = 'line'>折线图</option>
                                </select>
                            </a>
                            <a style = 'display:none;' href = 'javascript:void(0)' class="button-icon _save" href="#" data-toggle="modal" data-type="total" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                                <i class="fa fa-file-excel-o"></i>
                            </a>
                            <a class="button-icon _chart_switch" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                                <i class="fa fa-bar-chart-o"></i>
                            </a>
                            <a href="javascript:void(0);" data-status = 'normal' class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                <i class="fa fa-expand"></i>
                            </a>
                        </div>
                    </header>
                    <!-- widget div-->
                     <div class="no-padding" style="min-height:200px;">

                        <div id = 'total_chart' class="container_chart" style="height:200px;"></div>


                        <div id = 'total_table' data-table = 'total' class="container_table" style= 'display:none'>
                            <?php \yii\widgets\Pjax::begin(); ?>
                            <table class="table table-striped table-bordered table-hover table-middle">
                                <thead>
                                <tr>
                                    <th data-hide="phone,tablet">时间</th>
                                    <th data-hide="phone,tablet">数值</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <?php \yii\widgets\Pjax::end(); ?>
                            <div id ='pagination-total' class = 'pagination'></div>
                        </div>

                    </div>
                </div>
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                     data-widget-Collapse="false"
                     data-widget-custom="false"
                     data-widget-fullscreenbutton = 'false'
                     data-widget-togglebutton="false" id = '_jarviswidget_3'>
                    <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                        <h2><?= isset($category_name) ? $category_name  : '子节点数据统计
                        '?></h2>
                        <div class="jarviswidget-ctrls">
                            <a class="button-icon _chart_type" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图形样式" data-placement="bottom">
                                <select id = 'sub_type' style = 'color: #000000'>
                                    <option value = 'line'>折线图</option>
                                    <option value = 'column'>柱状图</option>
                                </select>
                            </a>
                            <a style = 'display:none;' href = 'javascript:void(0)' class="button-icon _save" href="#" data-toggle="modal" data-type="sub" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
                                <i class="fa fa-file-excel-o"></i>
                            </a>
                            <a class="button-icon _chart_switch" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                                <i class="fa fa-bar-chart-o"></i>
                            </a>
                            <a href="javascript:void(0);" data-status = 'normal' data-type = 'sub' class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen">
                                <i class="fa fa-expand"></i>
                            </a>
                        </div>
                    </header>
                    <!-- widget div-->

                    <div class="no-padding" style="min-height:300px">

                        <div id = 'sub_chart' class="container_chart" style="height:300px"></div>

                        <div id = 'sub_table' data-table = 'sub' class="container_table" style = 'display:none'>
                            <?php \yii\widgets\Pjax::begin(); ?>
                            <table class="table table-striped table-bordered table-hover table-middle">
                                <thead>
                                <tr>

                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <?php \yii\widgets\Pjax::end(); ?>
                            <div id ='pagination-sub' class = 'pagination'></div>
                        </div>

                    </div>
                </div>
        </article>
    <!-- 总用量 end-->
    </div>

<a style="" href="/category-data/analyze/export" class="button-icon _save" data-toggle="modal" data-type="pie" rel="tooltip" data-original-title="excel导出" data-placement="bottom">
    <i class="fa fa-file-excel-o"></i>
</a>
</section>
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/twbs-pagination/jquery.twbsPagination.min.js", ['yii\web\JqueryAsset']);
?>
<!-- js 脚本区域 start-->
<script>
    window.onload = function(){
        var $_date_time = '';   //具体时间
        var $_category_id = ''; //查询分类id
        var $_time_type = '';   //查询时间类型

        var $_category_info = ''; //查询的分类信息 包括 名称等
        var $_page_num = 5;    //table 每页展示数据的数量
        var $_pie_data = '';    //构造 存储饼图数据的变量
        var $_total_data = '';  //构造 存储统计图数据
        var $_sub_data = '';    //构造 子集统计图数据

        var $sub_error_num = 1;         //子节点ajax请求次数
        var $total_error_num = 1;       //总量ajax请求次数
        //初始化调用 入口方法
        dateChange();
        highcharts_lang_date();

        function dateEn2Cn($time_type) {
            switch($time_type) {
                case 'day':
                    return '天';
                    break;
                case 'month':
                    return '月';
                    break;
                case 'year':
                    return '年';
                    break;
            }
        }

        //时间选择框 改变调用函数
        function dateChange(){
            var $parent = $('#time_type').parent();
            $_time_type = $('#time_type').val();
            $_category_id = $('#category_ids').val() ? $('#category_ids').val() : $('#category_id').val();
            $parent.attr({
                'data-original-title':''
            });
            //简单的验证
            if($_time_type == '') {
                $parent.attr({
                    'data-original-title':'请选择时间类型'
                });
                $parent.trigger('mouseover');
                $parent.attr('data-original-title', '');
                return false;
            }
            $_date_time = $('#_time').val();
            // 调用
            $('.highcharts-container').remove();
            var $sub_error_num = 1;         //子节点ajax请求次数
            var $total_error_num = 1;       //总量ajax请求次数
            pieAjax($_category_id, $_time_type, $_date_time);
            totalAjax($_category_id, $_time_type, $_date_time);
            subAjax($_category_id, $_time_type, $_date_time);

            //修改导出地址
            exportUpdateSrc();
        }

        //根据select 选择的时间触发ajax
        $('#_time').datetimepicker().on('changeDate',dateChange);


        function exportUpdateSrc(){
            if($_time_type == '' || $_date_time == '' || $_category_id == '')
                return false;
            $.each($('._save'), function() {
                $(this).attr('href', '<?=Url::toRoute('export')?>' +
                                    '?export_type=' + $(this).data('type') +
                                    '&time_type=' + $_time_type +
                                    '&time_type=' + $_time_type +
                                    '&date_time=' + $_date_time +
                                    '&category_ids=' + $_category_id);
            });
        }

        //向table中添加当前页数的数据
        function addTable($data, $table_obj , $page){
            $table_obj.find('tbody').empty();
            //table
            var $_tbody = '';
            for(var $i = 0; $i < $data.length; $i++) {
                if($table_obj.data('table') == 'pie'){
                    if( $_page_num * ($page - 1) <= $i && $i < $_page_num * ($page)){
                        //将数据集 分开 饼图 和 折现、柱状图数据格式不同
                        $_tbody += '<tr><td>' + $data[$i].name + '</td><td>' + $data[$i].y + '</td></tr>';
                    }
                } else {
                    if( $_page_num * ($page - 1) <= $i && $i < $_page_num * ($page)){
                        //将数据集 分开 饼图 和 折现、柱状图数据格式不同
                        $_tbody += '<tr>';
                        for(var $j = 0; $j < $data[$i].length; $j++) {
                            $_tbody += '<td>' + $data[$i][$j] + '</td>';
                        }
                        $_tbody += '</tr>';
                    }
                }

            }
            $table_obj.find('tbody').append($_tbody);
        }

        //添加标题
        function addTablex($name, $table_obj){
            $table_obj.find('thead').empty();
            //多加一个时间列
            var $_thead = '<td>时间</td>';
            for(var $i = 0; $i < $name.length; $i++) {
                //判断是否含有数据 没有就不记录到表数据中
                    $_thead += '<td>' + $name[$i] + '</td>';
            }
            $table_obj.find('thead').append('<tr>' + $_thead + '</tr>');
        }

        //当ajax获取分项插件后 给每一个分页按钮添加点击事件
        function triggerAfterAjax($table_obj, $data, $total_page){
            $table_obj.find('.pagination').twbsPagination({
                totalPages: $total_page,
                visiblePages: 7,
                onPageClick: function (event, page) {
                    addTable($data, $table_obj, page);

                }
            });
        }

        /********************                  Js分为三个部分 既三个div图                  ********************/
//        $.ajaxSetup({async: false});         //设置全局ajax为 同步请求
        // 饼图的 ajax方法
        function pieAjax($_category_id, $time_type, $time_date) {
            $('#pie_table').parent().addClass('loader');
            var $url = '<?=Url::toRoute('ajax-pie')?>';
            $.post($url, {category_ids : $_category_id,
                          time_type: $time_type,
                          date_time: $time_date},
                                                function(data) {
                                                    //将拿到的数据放入全局变量中
                                                    $_pie_data = data;
                                                    $_category_info = data.category_info;
                                                    // 饼图 highcharts
                                                    $('#pie_table').parent().removeClass('loader');
                                                    $('#chart_pie').highcharts({
                                                        chart: {
                                                            plotBackgroundColor: null,
                                                            plotBorderWidth: null,
                                                            plotShadow: false
                                                        },
                                                        title: {
                                                            text: $_category_info.name + '子分类占比',
                                                            x: -20
                                                        },
                                                        subtitle: {
                                                            text: $_date_time + '' + dateEn2Cn($_time_type) + '占比',
                                                            x: -20
                                                        },
                                                        tooltip: {
                                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                        },
                                                        credits: {
                                                            enabled: false
                                                        },
                                                        plotOptions: {
                                                            pie: {
                                                                allowPointSelect: true,
                                                                cursor: 'pointer',
                                                                dataLabels: {
                                                                    enabled: true,
                                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                    style: {
                                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        series: [{
                                                            type: 'pie',
                                                            name: '占比',
                                                            data: $_pie_data.pie_data
                                                        }]
                                                    });
                                                    addTable($_pie_data.pie_data, $('#pie_table'), 1);
                                                    var $page_total = Math.ceil($_pie_data.pie_data.length/$_page_num);
                                                    if($page_total != 0)
                                                        triggerAfterAjax($('#pie_table'), $_pie_data.pie_data, $page_total);
            }, 'json').error(function() {
                    $('#pie_table').parent().removeClass('loader');
                    console.log('%c error:' + $_category_id + ', ' + $time_type + ', ' + $time_date, 'color:red');
                    alert('服务器未响应，请再次请求');
                });
        }

        //总量统计的 ajax方法
        function totalAjax($_category_id, $time_type, $time_date) {
            $('#total_table').parent().addClass('loader');
            var $url = '<?=Url::toRoute('ajax-total')?>';
            $.post($url,
                  {category_ids : $_category_id,
                   time_type: $time_type,
                   date_time: $time_date},
                   function(data) {
                       $total_error_num = 1;
                       $_total_data = data;
                       $_category_info = data.category_info;
                       $('#total_table').parent().removeClass('loader');
                       totalChart('column');  //默认是柱状图
                       addTable($_total_data.total_data, $('#total_table'), 1);
                       var $page_total =  Math.ceil($_total_data.total_data.length/$_page_num);
                       if($page_total != 0)
                           triggerAfterAjax($('#total_table'), $_total_data.total_data, $page_total);
                   }, 'json').error(function() {
                        console.log($total_error_num);
                        $('#total_table').parent().removeClass('loader');
                        console.log('%c error:' + $_category_id + ', ' + $time_type + ', ' + $time_date, 'color:red');
                        $total_error_num > 4 ? alert('服务器未响应，请再次请求') : totalAjax($_category_id, $time_type, $time_date);
                        $total_error_num++;
                });
        }


        //子类按时间统计 ajax方法
        function subAjax($_category_id, $time_type, $time_date) {
            $('#sub_table').parent().addClass('loader');
            var $url = '<?=Url::toRoute('ajax-sub')?>';
            $.post($url,
                {category_ids : $_category_id,
                    time_type: $time_type,
                    date_time: $time_date},
                function(data) {
                    $sub_error_num = 1;
                    $_sub_data = data;
                    $_category_info = data.category_info;
                    $('#sub_table').parent().removeClass('loader');
                    subChart('line');

                    addTablex($_sub_data.table_data.name, $('#sub_table'));
                    addTable($_sub_data.table_data.data, $('#sub_table'), 1);
                    var $page_total = Math.ceil($_sub_data.table_data.data.length/$_page_num);
                    if($page_total != 0)
                        triggerAfterAjax($('#sub_table'), $_sub_data.table_data.data, $page_total);
                }, 'json').error(function() {
                    console.log($sub_error_num);
                    $('#sub_table').parent().removeClass('loader');
                    console.log('%c error:' + $_category_id + ', ' + $time_type + ', ' + $time_date, 'color:red');
                    $sub_error_num > 4 ? alert('服务器未响应，请再次请求') : subAjax($_category_id, $time_type, $time_date);
                    $sub_error_num++;
                });
        }

        //统计图形highcharts --js
        function totalChart($chart_type) {
            $('#total_chart').highcharts({
                    chart: {
                        type: $chart_type,
                        zoomType: 'x',
                        borderWidth: 0
                    },
                    title: {
                        text: $_category_info.name + '时间统计',
                        x: -20
                    },
                    subtitle: {
                        text: $_date_time + ' ' + dateEn2Cn($_time_type) + '统计',
                        x: -20
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        },
                        categories: $_total_data.categories
                    },
                yAxis: {

                    title: {
                        text: ''
                    }
                },
//                tooltip: {
//                                   valueSuffix: '°C'
//                               },
//                               scrollbar: {
//                                   enabled: true
//                               },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled:false
                    },

                    series: [ {
                        data: $_total_data.total_data
                    }]
                });
        }

        function subChart($chart_type) {
                $('#sub_chart').highcharts({
                    chart: {
                        type: $chart_type,
                        zoomType: 'xy',
                        borderWidth: 0
                    },

                    title: {
                        text: $_category_info.name + '子节点统计',
                        x: -20
                    },
                    subtitle: {
                        text: $_date_time + ' ' + dateEn2Cn($_time_type) + '统计',
                        x: -20
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    scrollbar: {
                        enabled: true
                    },
                    yAxis: {

                        title: {
                            text: ''
                        }
                    },
                    credits : {
                        enabled : false
                    },
                    plotOptions: {
                        series: {
                            groupPadding: 0.3,
                            dataLabels: {
//                                enabled: true,
                                x: 4,
                                color: '#000033'
                                //               formatter: function(event) {
                                //               	chart = $('#container').highcharts();
                                //        		var extremesObject = chart.xAxis[0].getExtremes();
                                // min = extremesObject.min;
                                // max = extremesObject.max;

                                //        		if ((max - min)  <3)
                                //        			return '<span>'+this.point.y+'</span>';
                                //        		else
                                //        			return ;
                                //        	},

                            }
                        }
                    },
                    tooltip: {
                        shared: true,
                        valueDecimals: 2,
                        headerFormat: '<span>时间： </span>{point.key} <br />',
                        valueSuffix: ' KWh'
                    },

                    series: $_sub_data.sub_data
                });
        }

        //根据选择的图标样式 进行联动
        $('#total_type').on('change', function() {
            totalChart($(this).val());
        });
        $('#sub_type').on('change', function() {
            subChart($(this).val());
        });
        // 图、表切换
        $('._chart_switch').on('click', function(){
            var $_chart_type = $(this).data('type');
            var $_p_row = $(this).parent().parent().parent().find('.no-padding');
            var $_chart = $($_p_row).find('.container_chart');
            var $_table = $($_p_row).find('.container_table');
            var $_save_button = $(this).parent().find('._save');
            var $_select_button = $(this).parent().find('._chart_type');
            if($_chart_type == 'chart'){
                $_chart.hide();
                $_table.show();
                $_save_button.show();
                $_select_button.hide();
                $(this).data('type', 'table');
            }else{
                $_chart.show();
                $_table.hide();
                $_save_button.hide();
                $_select_button.show();
                $(this).data('type', 'chart');
            }
        });


        //参数设定 js
        $('#_cl').on('click', function() {
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
//                if($chart_data.length){$('#_jarviswidget').show();}
            }
            $("#_loop").slideToggle("slow");

        });

        //放大 调整div大小和一些细节
        $('.jarviswidget-fullscreen-btn').on('click', function() {
            var $_this = $(this);
            if($_this.data('status') == 'normal'){
                $_this.data('status', 'full');
                setTimeout(
                    function()
                    {
                        $_this.parent().parent().parent().find('.no-padding > .container_chart').css('height', '500px');
                        $('._chart_switch, ._chart_type').show();

                    }, 1);
            } else if($_this.data('status') == 'full') {
                console.log($_this.data('status'));
                $_this.data('status', 'normal');
                if($(this).data('type') == 'sub')
                    $_this.parent().parent().parent().find('.container_chart').css('height', '300px');
                else
                    $_this.parent().parent().parent().find('.container_chart').css('height', '230px');
            }

        })

        $("#time_type").on('change', function () {
            $(this).parent().attr({
                'data-original-title':''
            });
            _type = $(this).val();
            timeTypeChange(_type, $('#_time'));
        });

        $("#time_type").trigger('change');
        $('#_time').val($_date_time);
    }
</script>
<!-- js 脚本区域 end-->