<?php

namespace backend\module\category_data;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\category_data\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
