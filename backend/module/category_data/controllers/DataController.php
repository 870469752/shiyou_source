<?php

namespace backend\module\category_data\controllers;

use backend\controllers\MyController;
use backend\models\Point;
use backend\models\PointDataStatistic;
use common\library\MyStringHelper;
use Yii;
//模型
use backend\models\form\CategoryDataForm;
use backend\models\Category;
//帮助类
use common\library\MyExport;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;
use common\library\Ssp;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class DataController extends MyController
{
    public function actionIndex()
    {
        $category_type = Yii::$app->request->get('category_type');//能源类型id
        $date_type = Yii::$app->request->get('date_type');//日期类型（day1,month2,quarter4,year3）
        $time_interval = Yii::$app->request->get('time_interval');//(null,或2015-01-01:00:00:00__2015-01-01:23:59:59)

        $title='';
        $tb1 = 'core.point';
        $tb2 = 'core.point_data_statistic';
        switch($category_type){
            case 18:
                $title.='水表';
                $select ="$tb1.id,$tb1.name->'data'->>'zh-cn' as name,$tb1.value,sum($tb2.value) as sumvalue";
                break;
            case 29:
                $title.='电表';
                $select ="$tb1.id,$tb1.name->'data'->>'zh-cn' as name,$tb1.value,sum($tb2.value) as sumvalue";
                break;
            default:
                $title.='水表';
                $select ="$tb1.id,$tb1.name->'data'->>'zh-cn' as name,$tb1.value,sum($tb2.value) as sumvalue";
                break;
        }

        switch($date_type){
            case 'day':

                $title.='日报表';
                $start_time=date('Y-m-d',time()).' 00:00:00';
                $end_time  =date('Y-m-d',time()).' 23:59:59';
                break;
            case 'month':
                $number=50;
                $title.='月报表';
                $start_time=date('Y-m',time()).'-01 00:00:00';
                $end_time  =date('Y-m',time()).'-'.date('t',time()).' 23:59:59';
                break;
            case "quarter":
                $number=150;
                $title.='季度报表';
                $date=getdate();
                $month=$date['mon'];//当前月份
                $year=$date['year'];//当前年份
                $start=floor($month/3)*3;//当前季度第一个月
                $start_time=mktime(0,0,0,$start,1,$year);//当前季度第一天时间戳
                $start_time=date('Y-m-d',$start_time).' 00:00:00';
                $end_time=mktime(0,0,0,$start+3,1,$year);//当前季度最后一天时间戳
                $end_time  =date('Y-m-d',$end_time).' 23:59:59';
                break;
            case "year":
                $number=600;
                $title.='年报表';
                $start_time=date('Y',time()).'-1-31 00:00:00';
                $end_time  =date('Y',time()).'-12-31 23:59:59';
                break;
            default:
                $start_time=date('Y-m-d',time()).' 00:00:00';
                $end_time  =date('Y-m-d',time()).' 23:59:59';
                $title.='日报表';
                break;
        }


//        $point_str_ids = implode(',',Category::findCategoryPoint([[$category_type]]));
        $point_str_ids = Category::findCategoryPoint([[$category_type]]);
        $result = [];


        $data = Point::find()
            ->select($select)
            ->leftJoin("$tb2","$tb1.id = $tb2.point_id")
            ->where([$tb2.".interval_type"=>1,$tb2.".value_type"=>5])
            ->andWhere("timestamp >='".$start_time."'")
            ->andWhere("timestamp <='".$end_time."'")
            ->andWhere([$tb1.".id"=>$point_str_ids])
            ->groupBy("$tb1.id")
//            ->createCommand()->getSql();
            ->asArray()->all();
//        echo '<pre>';
//        print_r($data);
//        print_r("timestamp >'".$start_time."'");
//        die;
        foreach($data as $key=>$value){
            $data[$key]['value']=round($value['value'],2);
            $data[$key]['sumvalue']=round($value['sumvalue'],2);

        }
        return $this->render('index', [
            'data' => $data,
            'title' => $title,
            'category_type' => $category_type,
            'date_type'=>$date_type,
            'time_interval'=>$time_interval,
            'number'=>$number
        ]);
    }

    public function actionGetData()
    {
        $category_type = Yii::$app->request->get('category_type');//能源类型id
        $date_type = Yii::$app->request->get('date_type');//日期类型（day1,month2,quarter4,year3）
        $point_str_ids = implode(',',Category::findCategoryPoint([[$category_type]]));
        $result = [];
        $tb1 = 'core.point';
        $tb2 = 'core.point_data_statistic';
        //echo "<pre>";print_r(Yii::$app->request->getQueryParams());die;
        //echo "<pre>";print_r($str_ids);die;
        $select ="$tb1.id,$tb1.name->'data'->>'zh-cn' as name,$tb1.value,sum($tb2.value) as sumvalue";
        $result['data'] = Point::find()->select($select)->leftJoin("$tb2","$tb1.id = $tb2.point_id")->where(["$tb2.interval_type"=>1,"$tb2.value_type"=>5])->where("$tb1.id in($point_str_ids)")->groupBy("$tb1.id")->asArray()->all();
        //echo Point::find()->createCommand()->getRawSql();die;
        //echo "<pre>";print_r($result);die;
        $result['recordsTotal']= $result['recordsFiltered']=count($result['data']);
        $result['raw']=1;
        echo json_encode($result);


        /*$columns = array(
            //array( 'db' => "CONCAT('row_',id) as DT_RowId", 'dt' => "dt_rowid", 'dj'=>"id"),
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),
            array( 'db' => 'is_shield',   'dt' => 'is_shield' ,'dj'=>"is_shield" ),
            array( 'db' => "value_filter->'data'->>'min'as min", 'dt' => 'min' ,'dj'=>"value_filter->'data'->>'min'" ),
            array( 'db' => "value_filter->'data'->>'max' as max", 'dt' => 'max' ,'dj'=>"value_filter->'data'->>'max'" ),
            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
        );*/

        /*echo json_encode(
            Ssp::simple( $_POST, $table, $primaryKey, $columns )
        );*/
    }
}
