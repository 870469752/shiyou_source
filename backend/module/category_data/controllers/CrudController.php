<?php

namespace backend\module\category_data\controllers;

use backend\controllers\MyController;
use backend\models\Category;
use backend\models\EnergyCategory;
use backend\models\form\CategoryDataForm;
use backend\models\Location;
use backend\models\PointCategory;
use backend\models\PointCategoryRel;
use backend\models\PointData;
use backend\models\PointDataStatistic;
use backend\models\PointCategoryConfig;
use backend\models\search\CategoryDataSearch;
use Yii;
use common\library\MyFunc;
use backend\models\Point;
use backend\models\Unit;
use common\library\MyExport;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\helpers\BaseArrayHelper;
class CrudController extends MyController
{

//    /**
//     * 目前考虑的只是 两种分类并列并且是第一次级下的分类展示 如 电 、建筑 只会展示建筑A的电、建筑B的电
//     * 默认传递的分类id 在展示时 是以 列横展示的 如 建筑、电， table中每一行展示的是建筑用电
//     * @return string
//     */
//    public function actionIndex()
//    {
//        $params = Yii::$app->request->getQueryParams();
//        $model = new CategoryDataForm();
//        if($model->load($params, '')) {
//            //获取图标数据
//            $row_type = isset($params['row_type']) ?  $params['row_type'] : 0;
//            $table_info = $model->getCategoryDataById('table', true, $row_type);
//            $chart_info = MyFunc::getChartDataByPercent($model->getCategoryDataById('chart', false, $row_type), $table_info['unit'], true);
//            //获取图标信息
//            $category_arr = explode(',', $params['category_ids']);
//            if($row_type) {
//                $category_arr = MyFunc::Row_transform($category_arr);
//            }
//            $statistic_prefix = '';
//            if(isset($category_arr[0]) && isset($category_arr[1]) && !empty($category_arr[0]) && !empty($category_arr[1])){
//                //根据列的名字来统计数据
//                $statistic_prefix = Category::getCategoryById($category_arr[0])['name'] . Category::getCategoryById($category_arr[1])['name'];
//            }
//
//            /************************************考虑到方法臃肿 再次将导出和展示分开 ***************************/
//            if(isset($params['query_type']) && $params['query_type'] == 'export'){
//                //传递数据给导出方法
//                $this->export($table_info, $params, $statistic_prefix . '_' . Yii::t('app', $model->time_type));
//            }
//            return $this->render('index',
//                                ['category_data' => $table_info['category_data'],
//                                 'category_unit' => $table_info['unit'],
//                                 'date_time' => $model->date_time,
//                                 'time_type' => $model->time_type,
//                                 'chart_data' => json_encode($chart_info['data']),
//                                 'categories' => json_encode($chart_info['categories']),
//                                 'statistic_prefix' => $statistic_prefix,
//                                 'row_type' => $row_type,
//                                 'category_ids' => $params['category_ids']]);
//        }
//    }

//    /**
//     * 报表导出
//     * @param array $data  点位相关数据
//     * @param $point_info  点位相关信息
//     */
//    public function export($data = [], $param_info, $statistic_prefix)
//    {
//        if(isset($data['category_data']) && !empty($data['category_data'])){
//            //将需要的点位数据提出来
//            $export_data = [];
//            $header = [];
//            $i = 0;
//            foreach($data['category_data'] as $name => $_data){
//                $export_data[$i][] = $name;
//                $total = 0;
//                foreach($_data as $key => $value){
//                    if(!$i) {
//                        $header[] = $key . $data['unit'];
//                    }
//                    $total += $value;
//                    $export_data[$i][] = $value;
//                }
//                $export_data[$i][] = $total;
//                $i++;
//            }
//            //表格的前后加上 # 和 total
//            array_unshift($header, '#');
//            array_push($header, '总计' . $data['unit']);
//
//            //如果存在 数据才导出
//           if(isset($export_data) && !empty($export_data)) {
//               MyExport::run($export_data, [$header], ['save_file' => ['file_name' => $statistic_prefix]]);
//           }
//            //传递数据 和 列名 及 导出报表的名称
//        }
//    }
//
//    /**
//     * 根据点位id获取 点位的所有信息 包括点位的分类、单位等
//     * @param string $ids
//     * @return array|null|string|\yii\db\ActiveRecord
//     */
//    public static function getPointInfor($ids = '')
//    {
//        $Point_model = new Point();
//        //获取 点位的信息 点位名称 点位单位
//        if($ids){
//            $point_info = $Point_model::getPointInformation($ids);
//            return $point_info;
//        }
//        return '';
//    }

    /**
     * 此方法的 时间类型与其他方法时间类型概念上有区别
     * 本分类分项展示 是将选择出的所有分类直接进行展示
     * 其中的单位应该代表分类的单位
     * 固有分类展示  展示当前分类下所有的点位数据
     * 展示内容：
     *          1、按时间分组展示当前分类下的栈图
     *          2、按时间类型统计当前分类下的折线图
     *
     * 传递的参数有 固有分类id(category_id)、统计时间类型(time_type)、查看的时间数值(date_time)
     */
    public function actionInherentCategory()
    {
        $query_param = Yii::$app->request->getQueryParams();
        $model = new CategoryDataForm();

        if($model->load($query_param, '')) {
            //分类相关数据查询
            if($res_data = $model->getCategoryDataById('chart', true)) {
                return isset($query_param['export']) ? self::export($res_data, $query_param) : $this->display($res_data, $query_param);
            } else {
                //查询当前分类的子分类
                $parent_id = isset($query_param['parent_category_ids']) ? $query_param['parent_category_ids'] : $query_param['category_ids'];
                $tree = MyFunc::TreeData(Category::getSubCategory($parent_id));
                return $this->render('inherent_category',[
                        'category_tree' => $tree,
                        'parent_category_id' => $parent_id
                    ]);
            }
        }
    }


    //
    public function actionEnergy(){
        $query_param = Yii::$app->request->getQueryParams();
        //默认时间
        if(empty($query_param)){
            $query_param= Array (
                'time_type' => 'year',
                'date_time' => 2015
            );
        }
        
        $data=$this->GetDataByTime($query_param);
        $categorys=Category::getChildrenById(0);
        unset($categorys[0]);
        $category_tree='['.json_encode($categorys).']';

        $data=json_encode($data);
        return $this->render('energy',[
            'category_tree'=>$category_tree,
            'categorys'=>$categorys,
            'time_type'=>$query_param['time_type'],
            'date_time'=>$query_param['date_time'],
            'data'=>$data
        ]);
    }
    //根据时间得到水电数据
    public function GetDataByTime($query_param){
        if(empty($query_param)){
            $query_param= Array (
                'time_type' => 'year',
                'date_time' => 2015
            );
        }
        $category = new Category();
        //在这个$query_param时间段的点位数据
        $data = $category->GetDataByTime($query_param);
        //属于电的点位
        $point_ids1 = Category::findCategoryPoint([[Yii::t('app', 'ElectricID')]]);
        $point_ids2 = Category::findCategoryPoint([[Yii::t('app', 'WaterID')]]);
        $realdata=null;
        if(empty($point_ids1))$point_ids1=[];
        if(empty($point_ids2))$point_ids2=[];
        //在这个时间段且是电类型的点
        foreach($data as $value){

            if(in_array($value['point_id'],$point_ids1)) {
                $pointInfo= Point::getPointInfoById($value['point_id']);
//                echo '<pre>';
//                print_r($pointInfo);
//                die;
                $last_value=$pointInfo['0']['last_value'];
                $name=$pointInfo['0']['name'];
                $value['name']=MyFunc::DisposeJSON($name);
                $value['last_value']=$last_value;
                $value['categroy']='电';
                $realdata[] = $value;
            }
            if(in_array($value['point_id'],$point_ids2)) {
                $pointInfo= Point::getPointInfoById($value['point_id']);

                $last_value=$pointInfo['0']['last_value'];
                $name=$pointInfo['0']['name'];
                $value['name']=MyFunc::DisposeJSON($name);
                $value['last_value']=$last_value;
                $value['categroy']='水';
                $realdata[] = $value;
            }
        }
        return $realdata;
    }

    public function actionStatisticEnergy(){
        //分别得到日月年的三种$query_param格式
        $query_param['year']= Array (
            'time_type' => 'year',
            'date_time' =>date('Y',time())
        );
        $query_param['quarter']= Array (
            'time_type' => 'quarter',
            'date_time' =>date('Y-m-d',time())
        );
        $query_param['month']= Array (
            'time_type' => 'month',
            'date_time' =>date('Y-m-d',time())
        );
        $query_param['day']= Array (
            'time_type' => 'day',
            'date_time' =>date('Y-m-d',time())
        );

        foreach($query_param as $key=>$value){
            $datatemp=$this->GetDataByTime($value);
            $data[$key]=json_encode($datatemp);
        }

        $categorys=Category::getChildrenById(0);
        unset($categorys[0]);
        $category_tree='['.json_encode($categorys).']';


        return $this->render('statistic_energy',[
            'category_tree'=>$category_tree,
            'categorys'=>$categorys,
            'year'=>$data['year'],
            'quarter'=>$data['quarter'],
            'month'=>$data['month'],
            'day'=>$data['day']
        ]);
    }

    //ajax
    public function actionSelectdata(){
        $category_id=$_POST['category_id'];
        $categorys=Category::getChildrenById($category_id);
//        echo'<pre>';
//        print_r($categorys);
//        die;
        if(empty($categorys))
           echo 'error';
        else {
            $categorys = json_encode($categorys);
            echo $categorys;
        }


    }
    public function actionTest(){
        $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo())[1];
//        echo '<pre>';
//        print_r($tree_data);
//        die;

        $tree_json = json_encode($tree_data);
        return $this->render('test',[
            'tree_json'=>$tree_json
        ]);
    }
    //多层饼图（未完成）
    public function actionTest1(){
        //当前的point数据
        //$this->getPointdata();

        return $this->render('test1');
    }

    //给分类添加颜色
    //上级分类和下级分类颜色不一样，本级分类颜色各自不同
    public function actionAddColor(){
        self::setcolor(null);

    }



    //脚本添加数据
    public function actionAddPointData(){
        $start_time = '2014-01-01 00:00:00';
        $end_time = '2016-01-01 00:00:00';
        $points_=Point::find()->select('id')->asArray()->all();
        foreach ($points_ as $value) {
            $points[]=$value['id'];

        }
        $interval_type=[1,2,3];
        $value_type=['1','2','3','4','5','6','7','8'];
        //调用函数
        for($i=0;$i<999;$i++) {
            $model=new PointDataStatistic();
            $time=PointDataStatistic::rand_time($start_time, $end_time);
            $data=[
                'PointDataStatistic'=>[
                    'point_id'=>$points[array_rand($points)],
                    'interval_type'=>$interval_type[array_rand($interval_type)],
                    'value_type'=>$value_type[array_rand($value_type)],
                    'value'=>(string)rand(1,100),
                    'timestamp'=>$time,
                ]
            ];
            $model->load($data);
            $model->save();
        }
        die;
    }

    public function actionLocationShow_test(){
        echo '<pre>';
//        $aaa[]=Category::getCategoryById(28);
//        $bbb=BaseArrayHelper::map($aaa,'id','name');
//        $ccc=Category::getChildrenById(145);
//        print_r($aaa);
//        print_r($bbb);
////        print_r();
//        die;
        $query_param = Yii::$app->request->getQueryParams();
        if(empty($query_param)){
            $query_param=[
                'time_type'=>'year',
                'date_time'=>'2015',
                'Point'=>[
                    'category_id'=>170
                ]
            ];
        }
        $point_data_statistic=new PointDataStatistic();
//        $data_test=$point_data_statistic->getPointdata();

        $location_category = new Location();

        //当前的point数据
        $data = $location_category->getPointdata($query_param);

        //能源分类id

        $tree = MyFunc::TreeData(Location::getSubCategory(0));
        $top_energy_category=Location::getTopnode();
        krsort($top_energy_category);
        unset($top_energy_category[0]);
//        //电
//        $data[170];
//        $data=self::Piedata();
        //最顶部的节点id
        $ids=$location_category->getTopnode();
        $tree_data = MyFunc::get_fancyTree_data1(EnergyCategory::getCategoryInfo());
        $tree_json = json_encode($tree_data);
        krsort($top_energy_category);
//        echo '<pre>';
//        print_r($top_energy_category);
//        die;
        //柱状图 数据格式
        //数据
        $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $category_name='能耗比较图';

        return $this->render('location_show_test',[
            'data'=>$data,
            'ids'=>$ids,
            're_query_param'=>json_encode($query_param),
            'query_param'=>$query_param,
            'top'=>$top_energy_category,
            'category_name'=>$category_name,
            'chart_data'=>$chart_data,
            'categories'=>$categories,
            //'category_tree'=>$tree,
            'tree_json'=>$tree_json
        ]);
    }
    public function actionEnergyLocationShow(){

        $query_param = Yii::$app->request->getQueryParams();
        /*
         *  'time_type'=> 'year','month','day'
         *  'date_time'=> String
         *  'Point'=>[
         *              'energy_id'=>integer
         *          ]
         * */

        //处理首次页面加载时表单为空的数据
        if(!empty($query_param))
        $energy_id=$query_param['Point']['energy_id'];
        else $energy_id=null;

        //地理位置下拉图
        $LocationcategoryInfo=MyFunc::get_fancyTree_data1(Location::getCategoryInfo());
        $Location_tree_json = json_encode($LocationcategoryInfo);
        //能源分类下拉图
        $EnergycategoryInfo=MyFunc::get_fancyTree_data1(EnergyCategory::getCategoryInfo());
        $Energy_tree_json = json_encode($EnergycategoryInfo);
        //得到地理位置的最顶级
        $top_location=Location::getTopnode();

        $category = new Category();
        $energy_category=new EnergyCategory();
        //当前的point数据
        $point_data_statistic=new PointDataStatistic();
        $all_value=$point_data_statistic->getPointdata($query_param);

        if(!empty($all_value)) {
            $all_value = MyFunc::map($all_value,'point_id','value');
        }

        $color=Category::getcolor();
        $location_categorys=Location::getTopnode();
        $electric_categorys=$energy_category->getChildrenById($energy_id);

        //初始化数组
        foreach ($location_categorys as $location_key=>$location_value) {

            $floor_categorys=$category->getChildrenById($location_key);
            ksort($floor_categorys);
                foreach ($electric_categorys as $electric_key => $electric_value) {
                    foreach ($floor_categorys as $floor_key=>$floor_value ) {
                        $result[$location_key][$electric_key][$floor_key] = 0;
                }
            }
        }


        //填充数组
        foreach ($location_categorys as $location_key=>$location_value) {
            $floor_categorys=$category->getChildrenById($location_key);
            ksort($floor_categorys);
            foreach ($electric_categorys as $electric_key => $electric_value) {
                foreach ($floor_categorys as $floor_key=>$floor_value ) {
                    $points=$category->findCategoryPoint([[$electric_key],[$floor_key]]);
                    if(!empty($points)) {
                        foreach ($points as $point) {
                            if (array_key_exists($point, $all_value))
                                $result[$location_key][$electric_key][$floor_key] += $all_value[$point];
                        }
                    }
                }
            }
        }

        foreach ($result as $building_key=>$building_value) {
            foreach ($building_value as $energy_key=>$energy_value) {
                foreach ($energy_value as $floor_key=>$floor_value) {
                    $data[]=$floor_value;
                    $_category[]=Category::getCategoryById($floor_key)['name'];
                }
                $name=Category::getCategoryById($energy_key)['name'];
                $temp=['name'=>$name,'color'=>$color[$energy_key],'data'=>$data];
                $last_data[$building_key]['_category']=$_category;
                $last_data[$building_key]['data'][]=$temp;
                $data=null;
                $_category=null;
            }
        }

        $json=json_encode($last_data);

        return $this->render('energy_location_show',[
                're_query_param'=>json_encode($query_param),
                'location_tree'=>$Location_tree_json,
                'energy_tree'=>$Energy_tree_json,
                'top_location'=>$top_location,
                'json_data'=>$json

        ]);
    }

//      [{
//                          name: 'John',
//                          data: [5, 3, 4, 7, 2]
//          }, {
//                          name: 'Jane',
//                          data: [2, 2, 3, 2, 1]
//                    }, {
//                          name: 'Joe',
//                          data: [3, 4, 4, 2, 5]
//                    }]
    public function actionLocationShow(){
        $query_param = Yii::$app->request->getQueryParams();
        if(empty($query_param)){
            $query_param=[
                'time_type'=>'year',
                'date_time'=>'2015',
                'Point'=>[
                    'category_id'=>170
                ]
            ];
        }
//        echo '<pre>';
//        print_r($query_param);
//        die;
        $location_category = new Location();

        //当前的point数据
        $data = $location_category->getPointdata($query_param);

        //能源分类id

        $tree = MyFunc::TreeData(Location::getSubCategory(0));
        $top_energy_category=Location::getTopnode();
        krsort($top_energy_category);
        unset($top_energy_category[0]);
//        //电
//        $data[170];
//        $data=self::Piedata();
        //最顶部的节点id
        $ids=$location_category->getTopnode();
        $tree_data = MyFunc::get_fancyTree_data1(EnergyCategory::getCategoryInfo());
        $tree_json = json_encode($tree_data);
        krsort($top_energy_category);
//        echo '<pre>';
//        print_r($top_energy_category);
//        die;
        //柱状图 数据格式
        //数据
        $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $category_name='能耗比较图';

        return $this->render('location_show',[
            'data'=>$data,
            'ids'=>$ids,
            're_query_param'=>json_encode($query_param),
            'query_param'=>$query_param,
            'top'=>$top_energy_category,
            'category_name'=>$category_name,
            'chart_data'=>$chart_data,
            'categories'=>$categories,
            //'category_tree'=>$tree,
            'tree_json'=>$tree_json
        ]);
    }


    public function array_search_key($needle, $haystack){
        global $nodes_found;
        foreach ($haystack as $key1=>$value1) {

            if ($key1=== $needle){

                $nodes_found[] = $value1;

            }
            if (is_array($value1)){
                self::array_search_key($needle, $value1);
            }


        }
        return $nodes_found;
    }


    public function actionCategoryShow(){
        $query_param = Yii::$app->request->getQueryParams();


        $category = new Category();

        //当前的point数据
        $data = $category->getPointdata($query_param);

//        $tree = MyFunc::TreeData(Category::getSubCategory(0));

        $top_energy_category=EnergyCategory::getTopnode();

//        //电

        //最顶部的节点id
        $ids=$category->getTopnode();
        //地理位置下拉图
        $LocationcategoryInfo=MyFunc::get_fancyTree_data1(Location::getCategoryInfo());
        $top_Location_category=Location::find()
            ->select(['id as key', 'name as title', 'parent_id'])
            ->where(['parent_id'=>null])->asArray()->all();
        $location_categorys=Location::find()
            ->select(['id as key', 'name as title', 'parent_id'])
            ->where(['parent_id'=>$top_Location_category[0]['key']])->asArray()->all();
        $location_categorys[]=$top_Location_category[0];
        $LocationcategoryInfo=MyFunc::get_fancyTree_data1($location_categorys);

//        $LocationcategoryInfo=MyFunc::get_fancyTree_data1(Location::find()
//            ->where('parent_id')
//            ->asArray()->all());
        $tree_json = json_encode($LocationcategoryInfo);
        krsort($top_energy_category);
        unset($top_energy_category[0]);


        $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $category_name='能耗比较图';
//            echo '<pre>';
//            print_r($data);
//            die;
        return $this->render('category_show',[
            'data'=>$data,
            'ids'=>$ids,
            're_query_param'=>json_encode($query_param),
            'query_param'=>$query_param,
            'top'=>$top_energy_category,
            'category_name'=>$category_name,
            'chart_data'=>$chart_data,
            'categories'=>$categories,
            //'category_tree'=>$tree,
            'tree_json'=>$tree_json
        ]);
    }

    public function actionCategoryAnalysis(){

        $query_param = Yii::$app->request->getQueryParams();

        $category = new Category();


        //当前的point数据
        $data = $category->getPointdata($query_param);
//        echo '<pre>';
//        print_r($data);
//        die;
        //地理分类id
        $location_id=Yii::t('app', 'LocationID');
        $energy_id=Yii::t('app', 'EnergyID');

        $tree = MyFunc::TreeData(Category::getSubCategory($location_id));
        $top_energy_category=Category::getChildrenById($location_id);
//        //电
//        $data[170];
//        $data=self::Piedata();
        //最顶部的节点id
        $ids=$category->getTopnode();
        //地理位置数据  //[0]系统分类 [1]地理位置分类[2]能源类型分类
        $tree_data=MyFunc::get_fancyTree_data(Category::getCategoryInfo());
        $tree_data_energy = $tree_data[2];
        $tree_data_location = $tree_data[1];
        $tree_json_energy = json_encode($tree_data_energy);
        $tree_json_location = json_encode($tree_data_location);
        krsort($top_energy_category);
//        echo '<pre>';
//        print_r($tree_data);
//        die;
        //柱状图 数据格式
        //数据
        $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $category_name='能耗比较图';

        return $this->render('category_analysis',[
            'data'=>json_encode($data[$location_id]['children']),
            'ids'=>$ids,
            'query_param'=>$query_param,
            'top'=>$top_energy_category,
            'category_name'=>$category_name,
            'chart_data'=>$chart_data,
            'categories'=>$categories,
            //'category_tree'=>$tree,
            'tree_json_energy'=>$tree_json_energy,
            'tree_json_location'=>$tree_json_location

        ]);
    }


    public function actionColumndata(){
        //参数处理
        $id=$_POST['id'];
        $query_param=$_POST['query_param'];
        //json_decode  参数1把stdClass Object转化为array
        $query_param=json_decode($query_param,1);

        //测试数据


        if(empty($query_param)) {
            //$id=170;
            $query_param = Array
            (
                "parent_category_ids" => null,
                "time_type" => 'year',
                "date_time" => date('Y',time()),
                "Point" => ["category_id" => null]
            );
        }

        $category = new Category();
        $all_value=$category->GetDataGroupByTime($query_param);

        $testdata=$category->category_column($query_param,$all_value,$id);

        $child_category=$testdata['categorys'];
        $testdata=$testdata['result'];

        $x_data=array();
        $i=1;
        $color=Category::getcolor();
        foreach($testdata as $testdata_key=>$testdata_value){
            $x_data[]=$i++;
                foreach ($child_category as $child_key=>$child_value) {
                    if(array_key_exists($child_key,$testdata_value)) {
                        $result[$child_key][] = (int)$testdata_value[$child_key]['value'];
                    }
                    else
                        $result[$child_key][]=null;

                }
        }

        $last_data=array();
        foreach ($child_category as $child_key=>$child_value) {
            $last_data[]=['name'=>$child_value,'color'=>$color[$child_key],'data'=>$result[$child_key]];
        }
        $data=['_category'=>$x_data,'chart_data'=>$last_data];
//        echo '<pre>';
//        print_r($data);
//        die;
        $json=json_encode($data);
        return $json;
    }

    public function actionGetpoint(){
//        echo '<pre>';
//        print_r($_POST);
//        die;
        $id=$_POST['id'];
        $data=$_POST['data'];
        $data=json_decode($data,1);

        $category=new Category();
        //判断是否还有下级, 无下级则显示点位信息
        if(Category::leafIsEmpty($id)) {
            $point_ids = $category->findCategoryPoint([[$id]]);
            $result = Point::find()->where(['id' => $point_ids])->asArray()->all();
            $name = BaseArrayHelper::map($result, 'id', 'name');
            $value1 = BaseArrayHelper::map($result, 'id', 'last_value');
            foreach ($name as $key => $value) {
                $name[$key] = ['name' => MyFunc::DisposeJSON($name[$key]), 'value' => $value1[$key]];
            }
            $json = json_encode(['result'=>'true','data'=>$name]);
            return [$json];
        }
        //有下级则显示下级
        else return json_encode(['result'=>'false','data'=>self::array_search_key((int)$id,$data)]);
    }








    /**
     * 点位分类展示的 调用方法
     * @param $res_data
     * @param $query_param
     * @return string
     */
    public function display($res_data, $query_param){
        $display_type = isset($query_param['page']) ? 'table' : 'chart';
        //查询当前分类的名称
        $category_info = Category::getCategoryById($query_param['category_ids']);
        //查询当前分类的子分类
        $parent_id = isset($query_param['parent_category_ids']) ? $query_param['parent_category_ids'] : $query_param['category_ids'];
        $tree = MyFunc::TreeData(Category::getSubCategory($parent_id));
        // 报表 数据
        $search_model = new CategoryDataSearch();
        $chart_data = [];
        $categories = [];
        if(isset($res_data['data']) && !empty($res_data['data'])) {
            foreach($res_data['data'] as $key => $value) {
                $chart_data[$key] = intval($value['value']);
                $categories[$key] = !empty($value['point']['name']) ? MyFunc::DisposeJSON($value['point']['name']) : 'poitn_'.$value['point']['id'];
            }
        }

        //处理成highcharts 的数据格式 $chart_data = [], $unit = '', $chart_categories = false
//        $chart_data = MyFunc::getChartDataType($res_data['data'], $res_data['unit'], ['time']);

        $dataProvider = $search_model->inherentSearch($query_param, $res_data['dataProvider']);

        //判断 是否是分类提交
        return $this->render('inherent_category',[
                'query_param' => $query_param,
                'chart_data' => json_encode($chart_data),
                'categories' => json_encode($categories),
                'dataProvider' => $dataProvider,
                'category_name' => $category_info['name'],
                'data' => $res_data['data'],
                'display_type' => $display_type,
                'category_tree' => $tree,
                'parent_category_id' => $parent_id
            ]);
    }

    /**
     * 报表导出
     * @param array $data  点位相关数据
     * @param $point_info  点位相关信息
     */
    public static function export($res_data, $query_param)
    {
        $time_type = isset($query_param['time_type']) ? $query_param['time_type'] : 'day';
        $date_time = isset($query_param['date_time']) ? $query_param['date_time'] : date('Y-m-d');
        //查询当前分 类的名称
        $category_info = Category::getCategoryById($query_param['category_ids']);
        //分类相关数据查询
        if(isset($res_data['data']) && !empty($res_data['data'])){
            //将需要的点位数据提出来
            $export_data = [];
            foreach($res_data['data'] as $time => $_data){
                if(isset($_data['_timestamp']) && isset($_data['value'])){
                    $export_data[] = [$_data['_timestamp'], MyFunc::DisposeJSON($_data['point']['name']), $_data['value']];
                }
            }
            $unit = isset($res_data['unit'])?$res_data['unit']:'';
            //传递数据 和 列名 及 导出报表的名称
            MyExport::run($export_data, [['时间', '点位名称', '数值( ' .$unit .' )']], ['save_file' => ['file_name' => $category_info['name'].'('.$date_time.Yii::t('app',ucfirst($time_type)).'统计点位报表']]);
        }
    }

    /*-——+）（+——）                      分类统计 **********************/
    public function actionStatisticCategory()
    {
        $query_param = Yii::$app->request->getQueryParams();
        //获取当前选择分类的信息
        $parent_id = isset($query_param['parent_category_ids']) ? $query_param['parent_category_ids'] : $query_param['category_ids'];
        $tree = MyFunc::TreeData(Category::getSubCategory($parent_id));
        $statistic_category_data = $this->StatisticCategoryOfData($query_param);
        $data = $statistic_category_data['data'];
        $model = $statistic_category_data['model'];

        $count = count($data['category_data_info']['data']);
        $pages = new Pagination(['totalCount' => $count,
                                'pageSize' => 1,
                                'route' => Url::toRoute('ajax-statistic-category'),
                                'params' => ['count' => $count,
                                            'category_ids' => $model->category_ids,
                                            'time_type' => $model->time_type,
                                            'date_time' => $model->date_time]]);
//        echo'<pre>';
//        print_r($tree);
//        print_r(Category::getSubCategory(201));
//        die;
        return $this->render('statistic_category',[
                'category_tree' => $tree,
                'chart_data' => json_encode($data['category_data_info']['data']),
                'categories' => json_encode($data['category_data_info']['name']),
                'parent_category_id' => $parent_id,
                'time_type' => $model->time_type,
                'date_time' => $model->date_time,
                'category_ids' => $model->category_ids,
                'category_name' => $data['category_info']['name'],
                'pages' => $pages,
            ]);

    }

    /**
     * ajax获取分类统计数据
     * @return string
     */
    public function actionAjaxStatisticCategory()
    {
        $query_param = Yii::$app->request->getQueryParams();
        $pages = new Pagination(['totalCount' => $query_param['count']]);
        $statistic_category_data = $this->StatisticCategoryOfData($query_param, $pages->offset, $pages->limit);
        //查找实时数据   表号 设备名称（地理位置）  实时数据
        //             pointdata_id    点位对应的设备名称位置 name             point_data的value
        $sql="";
        $query=Point::findBySql($sql);
        $query = Point::find()
            ->select(['y.id as id','core.point.last_value as last_value'  ])
            //->from('core.point')
            //->where(['not in','unit.coefficient',[1]])
            //->join('LEFT JOIN','core.unit z','z.id=unit.standard_unit')
            ->join('LEFT JOIN','core.point_data y','y.point_id=point.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
//        echo'<pre>';
//        print_r($query_param);
//        die;
        return $this->render('statistic_category',[
                'query_param' => $query_param,
                'category_data_info' => $statistic_category_data['data']['category_data_info'],
                'pages' => $pages,
                'dataProvider'=>$dataProvider,
            ]);
    }

    public function actionEnergyCompare(){
        $chart_data=Array();
        $categories=Array();
        //$query_param = Yii::$app->request->getQueryParams();
        $category=new Category();
        $query_param =Yii::$app->request->getQueryParams();
        if(empty($query_param)) {
            $query_param = Array(
                'time_type' => ['year'],
                'time' => [2015],
                'energy_id' => ['0'=>null],
                'location_id' => ['0'=>null ]
            );
        }
            foreach ($query_param['time_type'] as $key=>$value) {
                $query = Array(
                    'time_type' =>$query_param['time_type'][$key],
                    'date_time' => $query_param['time'][$key],
                    'category_id' => $query_param['energy_id'][$key],
                    'location_id' => $query_param['location_id'][$key]
                );
                $data=$category->data_with_location($query);
                $categories[]=$key;
                $chart_data[]=$data;
             }


        //所有分类信息
        $categoryInfo=MyFunc::get_fancyTree_data(Category::getCategoryInfo());
        $tree_json = json_encode($categoryInfo);

         $category_name='能耗比较图';
        //条形图数据
//         $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
//        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $chart_data=json_encode($chart_data);

        $categories=json_encode($categories);
        return $this->render('energy_compare',[
                'category_name'=>$category_name,
                'chart_data'=>$chart_data,
                'categories'=>$categories,
                'tree_json'=>$tree_json
        ]);
    }
    public function actionCategoryCompare(){
        $chart_data=Array();
        $categories=Array();
        //$query_param = Yii::$app->request->getQueryParams();
        $category=new Category();
        $query_param =Yii::$app->request->getQueryParams();
        if(empty($query_param)) {
            $query_param = Array(
                'time_type' => ['year'],
                'time' => [2015],
                'energy_id' => ['0'=>null],
                'location_id' => ['0'=>null ]
            );
        }
        foreach ($query_param['time_type'] as $key=>$value) {
            $query = Array(
                'time_type' =>$query_param['time_type'][$key],
                'date_time' => $query_param['time'][$key],
                'category_id' => $query_param['energy_id'][$key],
                'location_id' => $query_param['location_id'][$key]
            );
            $data=$category->data_with_location($query);
            $categories[]=$key;
            $chart_data[]=$data;
        }


        //所有分类信息
        $categoryInfo=MyFunc::get_fancyTree_data(Category::getCategoryInfo());
        $tree_json = json_encode($categoryInfo);

        $category_name='能耗比较图';
        //条形图数据
//         $chart_data=json_encode(Array(5591793, 5444444, 3591793, 2591793));
//        $categories=json_encode(Array("照明插座用电", "特殊用电", "动力用电", "空调用电"));
        $chart_data=json_encode($chart_data);

        $categories=json_encode($categories);
        return $this->render('category_compare',[
            'category_name'=>$category_name,
            'chart_data'=>$chart_data,
            'categories'=>$categories,
            'tree_json'=>$tree_json
        ]);

    }
    //一段时间内的数据   分段统计
    public function actionCategoryCompareAjax(){
        $category=new Category();
        $point_data_statistic=new PointDataStatistic();
        $time=[];
        $time_type=2222;
        $startTime='2014-1-1';
        $endTime='2016-5-1';
        $point_category=[
            [29,236],
            [29,240]
        ];
        //查询得到在此分类下的点位集合
        $points=$category->findCategoryPoint([[29],[236]]);
        $data=$point_data_statistic->find()
            ->where(['point_id'=>$points])
            ->andWhere("TO_CHAR(TIMESTAMP,'YYYY-MM-DD')>'" . $startTime . "'")
            ->andWhere("TO_CHAR(TIMESTAMP,'YYYY-MM-DD')<'" . $endTime . "'")
            ->asArray()
            ->all();
        //划分时间间隔 hour/day/month/year
        $num=100;
        //拼凑需要的数组格式
        $result=[];
        for($i=0;$i<$num;$i++){
            $result[$i]=0;
        }
        //填充数组数据
        $min_time=(strtotime($endTime)-strtotime($startTime))/$num;
        foreach ($data as $key=>$value) {
            $order=(strtotime($value['timestamp'])-strtotime($startTime))/$min_time;
            $order=round($order,0);
            $result[$order]+=$value['value'];
        }
//        echo '<pre>';
//        print_r($result);
//        die;
        $data=[
            'name'=>'测试',
            'data'=>$result
        ];
       return  json_encode($data);

    }
//    public function actionCompareAjax($time_type,$time,$energy_id,$location_id){
        public function actionCompareAjax(){
        //$data=$_POST['data'];
        $category=new Category();
//        $time_type=json_decode($time_type);
//        $time=json_decode($time);
//        $energy_id=json_decode($energy_id);
//        $location_id=json_decode($location_id);
        $time_type=json_decode($_POST['time_type']);
        $time=json_decode($_POST['time']);
        $energy_id=json_decode($_POST['energy_id']);
        $location_id=json_decode($_POST['location_id']);

        foreach ($time_type as $key=>$value) {
            $query = Array(
                'time_type' =>$time_type[$key],
                'date_time' => $time[$key],
                'category_id' => $energy_id[$key],
                'location_id' => $location_id[$key]
            );
            $data=$category->data_with_location($query);
            $categories[]=$key;
            $chart_data[]=$data;
        }
            $result['categorys']=$categories;
            $result['chart_data']=$chart_data;
            $result=json_encode($result);
       echo $result;

    }

    /**
     * 节点方法 用于调用模型 获取数据库中的数据
     * @param $query_param
     * @param string $offset
     * @param string $limit
     * @return array
     */
    public function StatisticCategoryOfData($query_param, $offset = '', $limit = '')
    {
        $model = new CategoryDataForm();
        if($model->load($query_param, '')) {
            //获取儿子节点的数据 和 当前分类的信息
            $data = $model->statisticDataById($offset, $limit);
            //使用Yii自带的分页类
            return ['data' => $data,
                    'model' => $model];
        }
    }

    /**
     * 分类统计导出方法
     */
    public function actionExportStatisticCategory()
    {
        $query_param = Yii::$app->request->getQueryParams();
        $date_time = isset($query_param['date_time']) ? $query_param['date_time'] : date('Y-m-d');
        $time_type = isset($query_param['time_type']) ? $query_param['time_type'] : 'day';
        //获取当前选择分类的信息
        $statistic_category_data = $this->StatisticCategoryOfData($query_param);
        //分类相关数据查询
        if(isset($statistic_category_data['data']['category_data_info']) && !empty($statistic_category_data['data']['category_data_info'])){
            //将需要的点位数据提出来
            $export_data = [];
            foreach($statistic_category_data['data']['category_data_info']['name'] as $key => $name){
                if(isset($statistic_category_data['data']['category_data_info']['data'][$key]) && isset($name)){
                    $export_data[] = [$date_time, $name, $statistic_category_data['data']['category_data_info']['data'][$key]];
                }
            }
            $unit = isset($res_data['unit'])?$res_data['unit']:'';
            $export_name = $statistic_category_data['data']['category_info']['name'];
            //传递数据 和 列名 及 导出报表的名称
            MyExport::run($export_data, [['时间', '点位名称', '数值( ' .$unit .' )']], ['save_file' => ['file_name' => $export_name.'('.$date_time.Yii::t('app', ucfirst($time_type)).'累计报表)']]);
        }
    }

/****************************************************************************************************************/
    /**
     * energy YoY
     * 1.获取能源分类id下各级子分类id
     * 2.获取属于能源id的所有point_id
     * 3.根据point_id和$query_param查询点位数据
     * 4.将所有点位数据求和
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function actionEnergyYoy(){

        if(!$query_param= Yii::$app->request->getQueryParams()){
            $query_param['compare_type'] = 0;
            $query_param['parent_category_ids'] = 56;
            $query_param['time_type'] = 'month';
            $query_param['date_time'] = date('Y-m',time());
            if (!Category::getCategoryInfo()) {
                $query_param['Point']['category_id'] = null;
            } else {
                $query_param['Point']['category_id'] = 29;
            }
        }
        $cate_id = $query_param['Point']['category_id'];
        $category = new Category();
        $query_param = $category->handleTime($query_param);

        if (!$cate_id) {
            $tree_data = [];
            $categoryName = [];
            $res = [];
            $chart_data_single = [
                []
            ];
            $currentTotal = json_encode([]);
            $versusTotal = json_encode([]);
        } else {

                $interval_type = $query_param['interval_type'];
                $value_type = 5;

//                print_r(PointCategoryRel::getId($query_param['Point']['category_id']));
                $chart[$query_param['Point']['category_id']]['current'] = PointDataStatistic::categoryStatistical($interval_type, $value_type, $query_param['date_time'], $query_param['time_type'], PointCategoryRel::getId($query_param['Point']['category_id']));
                $chart[$query_param['Point']['category_id']]['versus'] = PointDataStatistic::categoryStatistical($interval_type, $value_type, $query_param['compare_date_time'], $query_param['time_type'], PointCategoryRel::getId($query_param['Point']['category_id']));
//                echo '<pre>';
//                print_r($chart);die;
                $chart_data_single = $this->formatTheData($chart, $query_param);

                $res = [];
                $currentTotal = null;//
                $versusTotal = null;//
                for ($i = 0; $i < 2; $i++) {
                    if ($i == 0) {
                        $res[$i]['name'] = $query_param['date_time'];
                        $res[$i]['color'] = "#7cb5ec";

                        $temp = $chart_data_single[0];
                        foreach ($temp as $key => $value) {
                            if (!$value) {
                                $temp[$key] = 0;
                            }
                            $currentTotal += $value;
                        }
                        ksort($temp);
                        $res[$i]['data'] = $temp;


                    } else {
                        $res[$i]['name'] = $query_param['compare_date_time'];
                        $res[$i]['color'] = "#434348";

                        $temp = $chart_data_single[1];
                        foreach ($temp as $key => $value) {
                            if (!$value) {
                                $temp[$key] = 0;
                            }
                            $versusTotal += $value;
                        }
                        ksort($temp);
                        $res[$i]['data'] = $temp;

                    }
                }
            }

            //curent point data, defalut : $data[201]=> type of energy,$data[204]=> type of system,$data[205]=> type of location
//            $data = $category->getPointdata($query_param);
            $categoryName = $this->returnNameCategory($cate_id);

            //能源类型分类
            $tree_data = MyFunc::get_energy_yoy_category_data(EnergyCategory::getCategoryInfo());
//            echo '<pre>';
//            print_r($tree_data);
//            die;

//        }

        return $this->render('energy_yoy',[
            'data'=>[],
            'date_time'=>$query_param['date_time'],
            'query_param'=>$query_param,
            'tree_json'=>json_encode($tree_data),
            'location_tree_json' => json_encode([]),
            'chart_data'=>json_encode($res),
            'compare_type'=>$query_param['time_type'],
            'categoriesName'=>json_encode($categoryName),
            'time_type' => $query_param['time_type'],
            'count' => count($chart_data_single[0]),
            'current_total'=>$currentTotal,
            'versus_total'=>$versusTotal
        ]);
    }

    /**
     * @return string
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function actionEnergyMom(){

        if(!$query_param= Yii::$app->request->getQueryParams()){
            $query_param['compare_type'] = 1;
            $query_param['parent_category_ids'] = 56;
            $query_param['time_type'] = 'month';
            $query_param['date_time'] = date('Y-m',time());
//            $query_param['time_type'] = 'year';
//            $query_param['date_time'] = '2015';
            if (!Category::getCategoryInfo()) {

                $query_param['Point']['category_id'] = null;

            } else {

                $query_param['Point']['category_id'] = 29;

            }

        }

        $cate_id = $query_param['Point']['category_id'];
//        echo '<pre>';
//        print_r($cate_id);die;

        $category = new Category();
        //handle the time
        $query_param = $category->handleTime($query_param);
//        echo '<pre>';
//        print_r($query_param);die;
        if (!$cate_id) {
            $tree_data = [];
            $categoryName = [];
            $res = [];
            $chart_data_single = [
                []
            ];
            $currentTotal = json_encode([]);
            $versusTotal = json_encode([]);
        } else {

            $interval_type = $query_param['interval_type'];
            $value_type = 5;

//            $test = new PointCategoryConfig();
//            $transaction = Yii::$app->db->beginTransaction();
//            echo '<pre>';
//            print_r(PointCategoryConfig::getId($query_param['Point']['category_id']));
//            die;
//            print_r(PointCategoryRel::getId($query_param['Point']['category_id']));


//            foreach (PointCategoryRel::getId(18) as $value) {
//                    $_test = clone $test;
//                    $_test -> category_id = 18;
//                    $_test -> point_id = $value;
//                    $_test -> is_skipped = false;
//                    $_test -> save();
//            }
//            die;

            //获取指定能源分类下，所有点位数据
//            $chart[$query_param['Point']['category_id']] = $this->getTargetedData($query_param);
            $chart[$query_param['Point']['category_id']]['current'] = PointDataStatistic::categoryStatistical($interval_type, $value_type, $query_param['date_time'], $query_param['time_type'], PointCategoryConfig::getId($query_param['Point']['category_id']));
            $chart[$query_param['Point']['category_id']]['versus'] = PointDataStatistic::categoryStatistical($interval_type, $value_type,$query_param['compare_date_time'], $query_param['time_type'], PointCategoryConfig::getId($query_param['Point']['category_id']));
//            echo '<pre>';
//            print_r($chart);die;
            $chart_data_single = $this->formatTheData($chart, $query_param);
//            echo '<pre>';
//            print_r($chart_data_single);die;
            //asseble the formatted data
            $res = [];
            $currentTotal = null;//
            $versusTotal = null;//
            for ($i = 0; $i < 2; $i++) {
                if ($i == 0) {
                    $res[$i]['name'] = $query_param['date_time'];
                    $res[$i]['color'] = "#7cb5ec";
                    $temp = $chart_data_single[0];
                    foreach ($temp as $key => $value) {
                        if (!$value) {
                            $temp[$key] = 0;
                        }
                        $currentTotal += $value;
                    }
                    ksort($temp);
                    $res[$i]['data'] = $temp;
                } else {
                    $res[$i]['name'] = $query_param['compare_date_time'];
                    $res[$i]['color'] = "#434348";
                    $temp = $chart_data_single[1];
                    foreach ($temp as $key => $value) {
                        if (!$value) {
                            $temp[$key] = 0;
                        }
                        $versusTotal += $value;
                    }
                    ksort($temp);
                    $res[$i]['data'] = $temp;
                }
            }
            //curent point data, defalut : $data[201]=> type of energy,$data[204]=> type of system,$data[205]=> type of location
//            $data = $category->getPointdata($query_param);
            $categoryName = $this->returnNameCategory($cate_id);
            $tree_data = MyFunc::get_energy_yoy_category_data(EnergyCategory::getCategoryInfo());
        }

        return $this->render('energy_mom',[
            'data'=>[],//use to generate the table
            'query_param'=>$query_param,
            'date_time'=>$query_param['date_time'],
            'tree_json'=>json_encode($tree_data),
            'location_tree_json' => json_encode([]),
            'chart_data'=>json_encode($res),
            'compare_type'=>$query_param['time_type'],
            'categoriesName'=>json_encode($categoryName),
            'time_type' => $query_param['time_type'],
            'count' => count($chart_data_single[0]),
            'current_total'=>$currentTotal,
            'versus_total'=>$versusTotal
        ]);
    }

    /**
     * recursive function in order to convert a multiple dimensions array to a single dimension array
     * @param array $array
     * @param array $res
     * @return $res
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function convertToSinleArray($array, &$res=[])
    {
        foreach ($array as $key => $value) {
            array_push($res,$key);
            self::convertToSinleArray($value, $res);
        }
        return $res;
    }

    /**
     * @param $query_param
     * @return mixed
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function getTargetedData ($query_param)
    {
        $id = $query_param['Point']['category_id'];
        $tempp = $query_param;
        $category = new Category();
        //获取当前时间该能源分类下所有点位数据，$point_ids = [$category_id=>[]]
        $chart['current'] = $category->getWantedData($this->getAllPointIdByCategoryId($id), $tempp);
        //查询对比时间该分类下所有点位数据
        $tempp['date_time'] = $query_param['compare_date_time'];
        $chart['versus'] = $category->getWantedData($this->getAllPointIdByCategoryId($id), $tempp);
        return $chart;
    }

    /**
     *
     */
    public function getValue()
    {

    }

    /**
     * return the name of category
     * @param inter $category_id
     * @return string
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function returnNameCategory ($category_id)
    {

//        return $category_id;

        if ($category_id == null) {
            return null;
        }
        if ($category_id == 'other'){
            return '其他';
        } else {
            $category = new Category();
            $temp = json_decode($category->find()->select('name')->where(['id'=>$category_id])->One()->name,true);
            foreach ($temp as $key => $value) {
                if (is_array($value)) {
                    return $value['zh-cn'];
                }
            }
        }
    }

    /**
     * get all point id by category id
     * @param $categoryId
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function getAllPointIdByCategoryId($categoryId)
    {
        $category = new Category();
        //获取能源分类category_id下各级子类id
        $temp = $category->getSubCategoryId($categoryId);
        $sub_category_id = $this->convertToSinleArray($temp);
        array_unshift($sub_category_id,$categoryId);
        //获取能源分类category_id下所有点位id,$point_ids = [$category_id=>[]]
        $all_point_ids = [];
        if ($sub_category_id) {
            foreach ($sub_category_id as $key => $value) {
                $temp = PointCategoryRel::getPointIds($value);
                if ($temp) {
                    foreach ($temp as $val) {
                        if ($val) {
                            foreach($val as $va) {
                                array_push($all_point_ids,$va);
                            }
                        }

                    }
                }
            }
        }
        array_unique($all_point_ids);
        return [$categoryId => $all_point_ids];
    }

    /**
     * @param $data
     * @param $query_param
     * @return array [0=>当前时间段,1=>对比时间段]
     * @auther pzzrudlf pzzrudlf@gamil.com
     */
    public function formatTheData($data, $query_param)
    {
//        echo '<pre>';
        $formatted = [];
        $formatted1 = [];
        $formatted2 = [];
        //first : get the target array count
        $count = null;
        $i = 0;
        foreach ($data as $categoryId => $current_versus_data) {
            foreach ($current_versus_data as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    if ($key2 == 'format') {
                        foreach ($value2 as $key3 => $value3) {
                            if ($query_param['time_type'] == 'year') {
                                $count =12;
                            } elseif ($query_param['time_type'] == 'month') {
                                $count = substr($value3['end'],8,2);
                            } elseif ($query_param['time_type'] == 'day') {
                                $count = 24;
                            }
                        }
                    }
                }
            }
        }

//        print_r($count);

        //second : assemble the array that we wanted (e.g. [0=>当前时间段,1=>对比时间段])
        foreach ($data as $categoryId => $current_versus_data) {
            foreach ($current_versus_data as $key1 => $value1) {
                if ($key1 == 'current') {
                    foreach ($value1 as $key2 => $value2) {
                        if ($key2 == 'data') {
                            foreach ($value2 as $key3 => $value3) {

//                                echo '<pre>';
//                                print_r($value3['_timestamp']);
//                                print_r($value3['value']);

                                if ($query_param['time_type'] == 'year') {
                                    $i = substr($value3['_timestamp'],5,2);

                                        $i = (integer)$i-1;


                                } elseif ($query_param['time_type'] == 'month') {
                                    $i = substr($value3['_timestamp'],8,2);

                                        $i = (integer)$i-1;


                                } elseif ($query_param['time_type'] == 'day') {
                                    $i = substr($value3['_timestamp'],11,2);

//                                    if ($i == '00') {
//                                        $i = 0;
//                                    } else {
                                        $i = (integer)$i;
//                                    }

                                }

                                $formatted1[$i] = round((float)$value3['value'],2);
                            }
                        }
                    }
                }

                if ($key1 == 'versus') {

                    foreach ($value1 as $key2 => $value2) {
                        if ($key2 == 'data') {
                            foreach ($value2 as $key3 => $value3) {


                                if ($query_param['time_type'] == 'year') {
                                    $i = substr($value3['_timestamp'],5,2);

                                        $i = (integer)$i-1;

                                } elseif ($query_param['time_type'] == 'month') {
                                    $i = substr($value3['_timestamp'],8,2);

                                        $i = (integer)$i-1;

                                } elseif ($query_param['time_type'] == 'day') {
                                    $i = substr($value3['_timestamp'],11,2);

//                                    if ($i == '00') {
//                                        $i = 0;
//                                    } else {
                                        $i = (integer)$i;
//                                    }

                                }
                                $formatted2[$i] = round((float)$value3['value'],2);
                            }
                        }
                    }

                }
            }
        }

        for ($i = 0; $i < $count; $i++) {
            if (!isset($formatted1[$i])) {
                $formatted1[$i] = null;
            } else {
                $formatted1[$i] = $formatted1[$i];
            }
        }
        for ($i = 0; $i < $count; $i++) {
            if (!isset($formatted2[$i])) {
                $formatted2[$i] = null;
            } else {
                $formatted2[$i] = $formatted2[$i];
            }
        }

        $formatted[] = $formatted1;
        $formatted[] = $formatted2;
//        print_r($formatted);
//        die;
        return $formatted;
    }

    /**
     * @desc 点位数据分析
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @date 2015-12-15 
     */
    public function actionPointAnalysis()
    {
        $model = new Point();
        // $param = Yii::$app->request->getBodyParams();
        $param = Yii::$app->request->getQueryParams();
        // $param = $_POST;
        $categoryDataSearch = new CategoryDataSearch();

       // echo '<pre>';
       // print_r($param);die;

        if (!$param) {

            $tree = MyFunc::TreeData(Category::getAllCategory());
            //在此添加一个 所有分类
            array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

            $location_tree = MyFunc::TreeData(Location::getAllCategory());
            //在此添加一个 所有分类
            array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);


            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo());
            $tree_data_location = MyFunc::get_fancyTree_data(Location::getCategoryInfo());
            $tree_json = json_encode($tree_data);
            $tree_json_location = json_encode($tree_data_location);
            return $this->render('select_point', [
                'category_tree' => $tree,
                'location_tree' => $location_tree,
                'unit' => $unit,
                'tree' => $tree_data,
                'tree_json' => $tree_json,
                'tree_json_location' => $tree_json_location,
                'model' => $model
            ]);

        } else {

            $pointDataModel = new PointData();
            $dataProvider = new ActiveDataProvider();

            if (!isset($param['time_type'])) {

                return $this->render('point_analysis',[
                        'param' => $param
                    ]);


            } else {

                $processParam = $this->handleTime($param);
                // echo '<pre>';
                // print_r($processParam);
                // echo '<hr>';
                $chartData = $this->getPointsData($processParam);
                echo '<pre>';
                print_r($chartData);die;

                return $this->render('point_analysis',[
                        'param' => $param,
                        // 'chartData' => $chartData
                    ]);
                
            }


        }

    }

    public function getPointsData($param)
    {
        $result = [];
        $pointDataModel = new PointData();
        $pointIds = explode(',', $param['point_ids']);
        if ($pointIds) {
            foreach ($pointIds as $key => $value) {
                $result[$key] = $pointDataModel::findIdTimeRange($value, $param['date_time'], $param['end_time']);
            }
        }

        return $result;


    }

    public function handletime($param)
    {
        $date_time = '';
        $start_time = '';
        $end_time = '';
        if (!$param) {
            $start_time = date('Y-m-d H:i:s');
            $end_time = date('Y-m-d H:i:s',strtotime('-1 day'));
        }
        $date_time = $param['date_time'];
        switch ($param['time_type'])
        {
            case 'year':
                $start_time = $date_time.'-01-01 00:00:00';
                $end_time = $date_time.'-12-31 23:59:59';
                break;
            case 'month':
                $start_time = $date_time.'-01 00:00:00';
                $days = cal_days_in_month(CAL_GREGORIAN, substr($date_time, 5, 2), substr($date_time, 0, 4));
                $end_time = $date_time.'-'.$days.' 23:59:59';
                break;
            case 'day':
                $start_time = $date_time.' 00:00:00';
                $end_time = $date_time. ' 23:59:59';
                break;
            case 'hour':
                $temp = substr($date_time, 0, 13);
                $start_time = $temp.':00:00';
                $end_time = $temp.':59:59';
                break;
        }

        $param['date_time'] = $start_time;
        $param['end_time'] = $end_time;
        return $param;
    }

    /**
     * 能源分类比較导出方法
     */
    public function actionExportEnergyCalculate()
    {
        if(!$query_param= Yii::$app->request->getQueryParams()){
            $query_param['compare_type'] = 0;
            $query_param['parent_category_ids'] = 56;
            $query_param['time_type'] = 'month';
            $query_param['date_time'] = date('Y-m',time());
            $query_param['Point']['category_id'] = 201;
        }
        $cate_id = $query_param['Point']['category_id'];

        $query_param['time_type'] = 'day';
        $query_param['date_time'] = '2015-11-12';

        $category = new Category();
        $query_param = $category->handleTime($query_param);
//        echo '<pre>';
//        print_r($query_param);
        //获取指定能源分类下，所有点位数据
        $chart[$query_param['Point']['category_id']] = $this->getTargetedData($query_param);

        $chart_data_single = $this->formatTheData($chart, $query_param);
        $date_time = $query_param['date_time'];
        $time_type = $query_param['time_type'];
        //asseble the exported array
        for ($i = 0; $i < 2*count($chart_data_single[0]); $i++){

            foreach ($chart_data_single as $key => $value) {

                ksort($value);

                for ($j = 0; $j <2; $j++) {

//                    print_r($value);

                    $export_data[$i][$j] = 'name';

                }

            }
        }

        echo '<hr>';
        echo '<pre>';
        print_r($export_data);
        die;
        //单位
        $unit = isset($res_data['unit'])?$res_data['unit']:'kg';

        //文件名字
        $export_name = 'demo';
//        $date_time = $query_param['date_time'];
//        $time_type = $query_param['time_type'];
        //传递数据 和 列名 及 导出报表的名称
        MyExport::run($export_data, [['时间','数值( ' .$unit .' )']], ['save_file' => ['file_name' => $export_name.'('.$date_time.Yii::t('app', ucfirst($time_type)).'累计报表)']]);

    }

    /***************************************以下方法已舍弃，不使用了**************************************************/
    /**
     * 查找不属于任何子类的点位id
     * @param inter $point_id
     * @param inter $category_id
     * @return mixed | bool 如果不属于该分类，则返回一个数组，这个数组包含不属于该分类的点位id如果属于则
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function judgeInCategory($category_id)
    {
        //查找所有属于子类的pointid
        $category = new Category();
        //获取能源分类category_id下各级子类id
        $temp = $category->getSubCategoryId($category_id);
        $sub_category_id = $this->convertToSinleArray($temp);
        //获取所有子类点位id
        $sub_point_ids = [];
        if ($sub_category_id) {
            foreach ($sub_category_id as $key => $value) {
                $temp = PointCategoryRel::getPointIds($value);
                if ($temp) {
                    foreach ($temp as $val) {
                        if ($val) {
                            foreach($val as $va) {
                                array_push($sub_point_ids,$va);
                            }
                        }
                    }
                }
            }
        }
        array_unshift($sub_category_id,$category_id);
        //获取能源分类category_id下所有点位id,$point_ids = [$category_id=>[]]
        $all_point_ids = [];
        if ($sub_category_id) {
            foreach ($sub_category_id as $key => $value) {
                $temp = PointCategoryRel::getPointIds($value);
                if ($temp) {
                    foreach ($temp as $val) {
                        if ($val) {
                            foreach($val as $va) {
                                array_push($all_point_ids,$va);
                            }
                        }
                    }
                }
            }
        }

        $all = array_unique($all_point_ids);
        $sub = array_unique($sub_point_ids);

        return array_diff($all, $sub);
    }


    /**
     * @param $categoryArray
     * @return array
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function getFirstSubCategoryId($categoryArray)
    {
        $res = [];
        foreach ($categoryArray as $key => $value) {
            $res[] = $key;
        }
        return $res;
    }



}
