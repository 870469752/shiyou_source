<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-6-15
 * Time: 上午10:30
 */

namespace backend\module\category_data\controllers;

use backend\controllers\MyController;
use common\library\MyStringHelper;
use Yii;
//模型
use backend\models\form\CategoryDataForm;
use backend\models\Category;
//帮助类
use common\library\MyExport;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;

class AnalyzeController extends MyController{

    public function actionStatisticalAnalysis()
    {

        //对参数进行处理
        $params = Yii::$app->request->getQueryParams();
        $parent_category_ids = isset($params['parent_category_ids']) && !empty($params['parent_category_ids']) ? $params['parent_category_ids'] : 2;
        //为了方便使用 CategoryDataFrom model类
        $params['category_ids'] = isset($params['category_ids']) && !empty($params['category_ids']) ? $params['category_ids'] : $parent_category_ids;
        $params['time_type'] = isset($params['time_type']) && !empty($params['time_type']) ? $params['time_type'] : 'day';
        $params['date_time'] = isset($params['date_time']) && !empty($params['date_time']) ? $params['date_time'] : date('Y-m-d');
        //实例化 获取数据
        $model = new CategoryDataForm();
        if($model->load($params, '')) {
            //获取儿子节点的数据 和 当前分类的信息
            $data = $model->statisticDataById();
        }

        if(isset($params['tag']) && $params['tag'] == 1){
            $sub_category = Category::getSubCategory($params['category_ids']);
            $sub_parent_id = Category::getCategoryById($params['category_ids']);
            $tree = MyFunc::CategoryOfMenu($sub_category, ArrayHelper::map($sub_category, 'parent_id', 'parent_id'), $sub_parent_id['parent_id'], 100);
            return $this->render('statistic_analysis', ['params' => $params,
                                                        'category_tree' => $tree]);
        }
        return $this->render('statistic_analysis', ['params' => $params]);
    }

    /**
     * ajax 获取 饼图的展示数据
     */
    public function actionAjaxPie()
    {
        $params = Yii::$app->request->getBodyParams();
        $params = isset($params) && !empty($params) ? $params : Yii::$app->request->getQueryParams();
        $model = new CategoryDataForm();
        if($model->load($params, '')) {
            //获取儿子节点的数据 和 当前分类的信息
            $data = $model->statisticDataById();
        }
        //总体饼图占比
        $sum_value = array_sum($data['category_data_info']['data']);
        $pie_data = [];
        //饼图数据处理
        foreach($data['category_data_info']['data'] as $key => $value) {
            if($value)          //如果数据为NULL 就不展示
                $pie_data[] = [$data['category_data_info']['name'][$key], floatval(number_format($value/$sum_value, 2 ,'.', ''))];
        }
        return json_encode(['pie_data' => $pie_data,
                            'category_info' => $data['category_info']]);
    }

    /**
     * 获取 时间统计图
     * @return string
     */
    public function actionAjaxTotal()
    {
        $params = Yii::$app->request->getBodyParams();
        $params = isset($params) && !empty($params) ? $params : Yii::$app->request->getQueryParams();
        $model = new CategoryDataForm();
        if($model->load($params, '')) {
            //获取儿子节点的数据 和 当前分类的信息
            $data = $model->statisticDataByDate();
        }
        //将数据转为 highcharts 柱状图的格式
        $total_data = [];                   //y轴的数据
        $categories = [];                    //x轴的数据
        if(isset($data['category_data']['data']) && !empty($data['category_data']['data'])) {
            foreach($data['category_data']['data'] as $key => $value) {
                if(isset($value['value']) && isset($value['_timestamp'])) {
                    $timestamp = MyStringHelper::getTimeByType($value['_timestamp'], $params['time_type']);
                    $total_data[] = [$timestamp, floatval(number_format($value['value'], 2 ,'.', ''))];
                    $categories[] = $timestamp;
                }
            }
        }
        return json_encode(['total_data' => $total_data,
                            'categories' => $categories,
                            'category_info' => $data['category_info']]);
    }

    /**
     * 子节点数据统计 ajax
     * @return string
     */
    public function actionAjaxSub()
    {
        $params = Yii::$app->request->getBodyParams();
        $params = isset($params) && !empty($params) ? $params : Yii::$app->request->getQueryParams();
        $model = new CategoryDataForm();
        if($model->load($params, '')) {
            //获取儿子节点的数据 和 当前分类的信息
            $data = $model->statisticDataBySub();
        }

        return json_encode(['sub_data' => $data['category_data_info'],
                            'table_data' => $data['table_data'],
                            'category_info' => $data['category_info']]);
    }

    public function actionExport()
    {

        $params = Yii::$app->request->getQueryParams();
        if(!isset($params['export_type']) || empty($params['export_type']))
            return false;
        switch($params['export_type']) {
            case 'pie':
                $data_info = $this->actionAjaxPie();
                $table_data = json_decode($data_info, true)['pie_data'];
                $table_title = ['分类名称', '数值'];
                $title = '占比';
                break;
            case 'total':
                $data_info = $this->actionAjaxTotal();
                $table_data = json_decode($data_info, true)['total_data'];
                $table_title = ['时间', '数值'];
                $title = '总量比';
                break;
            case 'sub':
                $data_info = $this->actionAjaxSub();
                $_data = json_decode($data_info, true)['table_data'];
                $table_data = $_data['data'];
                $table_title = $_data['name'];
                array_unshift($table_title, '时间');
                $title = '子类数据比';
                break;
            default:
                $data = [];
                break;
        }
        MyExport::run($table_data, [$table_title], ['save_file' => ['file_name' => $title.'(分类统计表)']]);
    }
}
