<?php
namespace backend\module\modbus_address\controllers;
use backend\controllers\MyController;
use backend\models\Modbus_conn;
use backend\models\Modbus_controller;
use backend\models\Modbus_point;
use backend\models\ModbusPoint;
use yii\data\ActiveDataProvider;
use yii;
class CrudController extends MyController
{
    public function actionIndex()
    {
        $query = ModbusPoint::find()->select(['protocol.modbus_point.name as name','last_value','is_available','is_shield','last_update'])
            ->join('LEFT JOIN','core.protocol y','y.id=protocol.modbus_point.protocol_id')
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



}










