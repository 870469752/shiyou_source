<?php
namespace backend\module\modbus_address\controllers;
use backend\controllers\MyController;
use backend\models\Modbus_config;
use backend\models\Modbus_controller;
use backend\models\Modbus_point;
use yii\data\ActiveDataProvider;
use yii;
class DeviceController extends MyController
{
    public function actionIndex()
    {
        $query = Modbus_controller::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        $query = Modbus_controller::find()->where(['config_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $title=Modbus_config::find()->select(['rtu_port','tcp_ip'])->where(['id'=>$id])->asArray()->all();
//        echo '<pre>';
//        print_r($title);
//        die;
        $title='编辑'.$title[0]['rtu_port'].$title[0]['tcp_ip'];
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'config_id'=>$id,
            'title'=>$title,
        ]);
    }

    public function actionCreate($config_id){
        $model = new Modbus_controller();
        $model->is_available=1;
        $model->is_shield=0;
        $query = Modbus_controller::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if($model->load($data = Yii::$app->request->post())){
            $model->config_id=$config_id;
            $result = $model->save();
            /*操作日志*/
            return $this->redirect('modbus_address/device/view?id='.$config_id,[
                'dataProvider'=>$dataProvider,
                'config_id'=>$config_id,

            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $config_id=Modbus_controller::find()->select(['config_id'])->where(['id'=>$id])->asArray()->all();
        $config_id=$config_id[0]['config_id'];

        if($model->load($data = Yii::$app->request->post())){

            $result = $model->save();

            /*操作日志*/
            return $this->redirect('modbus_address/device/view?id='.$config_id,[

            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionNext($id){
        $model=new Modbus_controller();
        $model->config_id=$id;
        return $this->redirect('modbus_address/point/view?controller_id='.$id,[
            'model'=>$model,
        ]);
    }


    public function actionDelete($id)
    {
        $config_id=Modbus_controller::find()->select(['config_id'])->where(['id'=>$id])->asArray()->all();
        $config_id=$config_id[0]['config_id'];

        $this->findModel($id)->delete();
        /*操作日志*/
        return $this->redirect('modbus_address/device/view?id='.$config_id);
    }


    protected function findModel($id)
    {
        if (($model = Modbus_controller::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}