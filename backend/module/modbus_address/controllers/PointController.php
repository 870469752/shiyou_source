<?php
namespace backend\module\modbus_address\controllers;
use backend\controllers\MyController;
use backend\models\Modbus_config;
use backend\models\Modbus_controller;
use backend\models\Modbus_point;
use yii\data\ActiveDataProvider;
use yii;
class PointController extends MyController
{
    public function actionIndex()
    {
        $query = Modbus_point::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($controller_id)
    {
        $query = Modbus_point::find()->where(['controller_id'=>$controller_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        //值可能为空判断
        $config_id=Modbus_point::find()->select(['config_id'])->where(['controller_id'=>$controller_id])->asArray()->all();
        if(!empty($config_id)) {
            $config_id = $config_id[0]['config_id'];
        }
        else $config_id='';
        if($config_id=='') {
            $slave_id = '';
        }
          else {
              $slave_id = Modbus_controller::find()->select(['slave_id'])->where("config_id=$config_id")->asArray()->all();
            if (!empty($slave_id)) {
                $slave_id = $slave_id[0]['slave_id'];
            }
            else $slave_id='';
        }

        //默认的name
        $name=Modbus_config::find()
            ->select(['rtu_port','tcp_ip'])
            ->where(['id'=>$config_id])
            ->asArray()->all();
        if(!empty($config_id)) {
            $title=$name[0]['rtu_port'].$name[0]['tcp_ip'].'-'.$slave_id.'-';
        }
        else $title=''.'-'.''.'-';

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'controller_id'=>$controller_id,
            'title'=>$title,
        ]);
    }


    public function actionCreate($controller_id){
        $model = new Modbus_point();
        //查找对应controller_id的config_id
//        echo '<pre>';
//        print_r($controller_id);
//        die;
        $config_id=Modbus_point::find()->select(['config_id'])->where(['controller_id'=>$controller_id])->asArray()->all();
        $config_id=$config_id[0]['config_id'];
        $slave_id=Modbus_controller::find()->select(['slave_id'])->where("config_id=$config_id")->asArray()->all();
        $slave_id=$slave_id[0]['slave_id'];
        //默认的name
        $name=Modbus_config::find()
            ->select(['rtu_port','tcp_ip'])
            ->where(['id'=>$config_id])
            ->asArray()->all();
        $name=$name[0]['rtu_port'].$name[0]['tcp_ip'].'-'.$slave_id.'-';
        $model->name=$name;
        $model->is_available=1;
        $model->is_shield=0;

        $query = Modbus_point::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        if($model->load($data = Yii::$app->request->post())){
            $model->controller_id=$controller_id;
            $model->config_id=$config_id;
            $result = $model->save();
            /*操作日志*/
            return $this->render('index',[
                'dataProvider'=>$dataProvider,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'title' =>$name,
            ]);
        }

    }


    public function actionDelete($id)
    {
        $controller_id=Modbus_controller::find()->select(['controller_id'])->where(['id'=>$id])->asArray()->all();
        $controller_id=$controller_id[0]['controller_id'];

        $this->findModel($id)->delete();
        /*操作日志*/
        return $this->redirect('modbus_address/device/view?id='.$controller_id);
    }


    public function actionUpdate($id,$controller_id)
    {
        $model = $this->findModel($id);
        $controller_id=Modbus_point::find()->select(['controller_id'])->where(['id'=>$id])->asArray()->all();
        $controller_id=$controller_id[0]['controller_id'];

        if($model->load($data = Yii::$app->request->post())){

            $result = $model->save();

            /*操作日志*/
            return $this->redirect('modbus_address/point/view?controller_id='.$controller_id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Modbus_point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}