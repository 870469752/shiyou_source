<?php
namespace backend\module\modbus_address\controllers;
use backend\controllers\MyController;
use backend\models\Modbus_config;
use backend\models\Modbus_controller;
use backend\models\Modbus_point;
use yii\data\ActiveDataProvider;
use yii;
class ConnController extends MyController
{
//    public function actionIndex()
//    {
//        return $this->render('index');
//    }



    public function actionIndex()
    {
        $query = Modbus_config::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    public function Test(){

    }
    public function actionNext($id){
        $model=new Modbus_controller();
        $query = Modbus_controller::find()->where(['config_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->redirect('modbus_address/device/view?id='.$id,[
            'model'=>$model,
            'dataProvider'=>$dataProvider,
            'conn_id'=>$id,
        ]);
    }


    public function actionCreate()
    {
        $model = new Modbus_config();
        $model->type=1;
        $model->is_available=1;
        $model->is_shield=0;
            if ($model->load($data = Yii::$app->request->post())) {
                $result = $model->save();

                /*操作日志*/
                return $this->redirect('modbus_address/conn', [

                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
        }

    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        /*操作日志*/
        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->load($data = Yii::$app->request->post())){

            $result = $model->save();

            /*操作日志*/
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


        protected function findModel($id)
        {
            if (($model = Modbus_config::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

}