<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\EventPointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'Event Points');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>

                    <?= GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'options' => ['class' => 'widget-body no-padding'],
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover',
                            'width' => '100%',
                            'id' => 'datatable'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [
                            ['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'last_value'],
                            ['attribute' => 'last_update'],
                            ['attribute' => 'is_shield', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'is_available', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],

                            ['class' => 'common\library\MyActionColumn', 'template' => '', 'headerOptions' => ['data-class' => 'expand']],
                        ],
                    ]); ?>
                </div>
            </div>
        </article>
    </div>
</section>
