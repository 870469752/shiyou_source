<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 */
$this->title = Yii::t('modbus_conn', 'Create {modelClass}', [
  'modelClass' => 'Modbus_conn',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'title' =>$title,
    ]) ?>
