<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 */

$this->title = Yii::t('equipment', 'Update {modelClass}: ', [
  'modelClass' => 'Modbus_Point',
])
?>
    <?= $this->render('_form', [
        'model' => $model,
        'title' =>'Modbus_Point'
    ]) ?>
