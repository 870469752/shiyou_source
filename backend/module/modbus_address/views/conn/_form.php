<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\Equipment $model
 * @var yii\widgets\ActiveForm $form
 */

$s=300;
$data=array();
for($i=0;$i<8;$i++){
	$data[$s]=$s;
	$s=$s*2;
}
$data[43000]=43000;
$data[56000]=56000;
$data[57600]=57600;
$data[115200]=115200;
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin([
						]); ?>
						<fieldset>
						<?= $form->field($model, 'type')->dropDownList(['1'=>'RTU','2'=>'TCP'],['id'=>'type']) ?>
						<?= $form->field($model, 'rtu_port')->textInput(['maxlength' => 255,'class'=>'rtu form-control']) ?>
						<?= $form->field($model, 'rtu_baud_rate')->dropDownList($data,['class'=>'rtu form-control','prompt' => '请选择']) ?>
						<?= $form->field($model, 'rtu_parity')->dropDownList(['0'=>'N','1'=>'E','2'=>'O'],['class'=>'rtu form-control','prompt' => '请选择']) ?>
						<?= $form->field($model, 'rtu_data_bits')->dropDownList(['0'=>'5','1'=>'6','2'=>'7','3'=>'8'],['class'=>'rtu form-control','prompt' => '请选择']) ?>
						<?= $form->field($model, 'rtu_stop_bits')->dropDownList(['1','2'],['class'=>'rtu form-control','prompt' => '请选择']) ?>
						<?= $form->field($model, 'tcp_ip')->textInput(['maxlength' => 255,'class'=>'tcp form-control']) ?>
						<?= $form->field($model, 'tcp_port')->textInput(['maxlength' => 255,'class'=>'tcp form-control']) ?>
						<?= $form->field($model, 'is_shield')->dropDownList(['0'=>'是','1'=>'否'],['selected'=>0]) ?>
						<?= $form->field($model, 'is_available')->dropDownList(['0'=>'否','1'=>'是'],['selected'=>0]) ?>
						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('equipment', 'Create') : Yii::t('equipment', 'Update'), ['id'=>'submit' ,'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
							<?=HTML::button("取消",['id'=>'select','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerJsFile("js/plugin/chosen/chosen.jquery.min.js", ['backend\assets\AppAsset']);
?>
<script>
	//设置初始为RTU
	function test(){
		$('#type').val("1")
		$('.tcp').each(function(){
			$(this).hide();
			$(this).parent().hide();
		})
	}
window.onload = function(){
		test();
//    $('#equipment-description').chosen();
	$('#select').click(function(){
		window.location.href="/modbus_address/conn";
	})


	$('#submit').click(function(){
		if($('#type').val()==1){
			return true;
		}
		else if(!f_check_IP()) {
				 return false;
				}
			 else return true;
	})
	//隐藏
	$('#type').change(function(){
		console.log($('#type').val());
		if($('#type').val()==2) {
			$('.rtu').each(function(){
				$(this).hide();
				$(this).parent().hide();
			})
			$('.tcp').each(function(){
				$(this).show();
				$(this).parent().show();
			})
		}
		  if($('#type').val()==1){
			$('.tcp').each(function(){
				$(this).hide();
				$(this).parent().hide();
			})
			$('.rtu').each(function(){
				$(this).show();
				$(this).parent().show();
			})
		}
	})

	function f_check_IP()
	{  var ip = document.getElementById('modbus_config-tcp_ip').value;
		var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;//正则表达式
		if(re.test(ip))
		{
			if( RegExp.$1<256 && RegExp.$2<256 && RegExp.$3<256 && RegExp.$4<256)
				return true;
		}
		console.log($("#modbus_conn-tcp_ip").is(":hidden"));
		alert("IP有误！");
		return false;
	}
}
</script>