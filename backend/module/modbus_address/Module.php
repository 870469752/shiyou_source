<?php

namespace backend\module\modbus_address;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\modbus_address\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
