<?php

namespace backend\module\point_batch;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\point_batch\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
