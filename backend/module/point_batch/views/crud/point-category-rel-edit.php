<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:36
 */

use yii\helpers\Html;
use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
//echo '<pre>';
//echo  Url::toRoute('page').'?'.http_build_query(Yii::$app->request->getQueryParams());die;
//var_dump(http_build_query(Yii::$app->request->getQueryParams()));die;

// echo '<pre>';
// print_r($category_tree);
// echo '<hr>';
// print_r($location_tree);
// echo '<hr>';
// echo '<hr>';
// print_r($point_ids);
// die;
?>
<style>

</style>


<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <!-- end row -->


    <div class="widget-body" style="display: inline-block;width:100%;">

        <?php $form = ActiveForm::begin(['method' => 'post']); ?>
        <?= Html::hiddenInput('point_ids', $point_ids);?>
        <fieldset  >

            <!-- widget div-->

            <div class = 'form-group' style ='width:100%;'>

                    <!-- 分类 隐藏框 -->
                    <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id2'])?>
                    <!-- 点位分类 -->
                    <label>能源分类</label>
                    <div class="input-group" id = 'category_tree2' style="width: 100%;">
                        <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name2' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                            <ul id = 'select_category2' class="select2-choices choices2" style = 'width:100%;'>
                            </ul>
                        </div>
                    </div>
                    <div id = 'tree2' style = 'display:none;position:absolute;width:100%;'></div>


                    <!-- 分类 隐藏框 -->
                    <?=Html::hiddenInput('Point[category_id1]', null, ['id' => 'category_id1'])?>
                    <!-- 点位分类 -->
                    <label>地理分类</label>
                    <div class="input-group" id = 'category_tree1' style="width: 100%;">
                        <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name1' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                            <ul id = 'select_category1' class="select2-choices choices1" style = 'width:100%;'>
                            </ul>
                        </div>
                    </div>
                    <div id = 'tree1' style = 'display:none;position:absolute;width:100%'></div>
            
            </div>
        </fieldset>
        <div class="form-actions">
            <?= Html::submitButton(Yii::t('app', '提交') , ['class' => 'btn btn-primary', 'id' => '_sub']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>


</section>
<!-- end widget grid -->


<?php

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);

$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);


// $this->registerJsFile('js/myfunc.js');
?>


<!-- js 脚本区域 start-->
<script>
    window.onload = function(){
        var category_tree = <?= isset($category_tree)?json_encode($category_tree):'';?>;
        var location_tree = <?= isset($location_tree)?json_encode($location_tree):'';?>;
        console.log(category_tree);
        console.log(location_tree);
             

        //提交验证
        $('#_sub').click(function() {

            var location_iid = $('#category_id1').val();

            var category_iid = $('#category_id2').val();

            if (!location_iid){

                alert('请选择地理位置');
                return false;

            }

            if (!category_iid) {

                alert('请选择能源分类');
                return false;
            }

        })


                //初始化能源分类 与 地理位置分类 框
                initfancytree(<?=$tree_json?>,2);
                initfancytree(<?=$tree_json_location?>,1);


                //初始化树形下拉列表框  data为树状数据源  id 为元素后缀名
                function initfancytree(data,id){
                    //fancytree 树状多选框初始化
                    var $current_keys = [];
                    var $count = 0;

                    function delete_all_category() {
                        $('.choices'+id+' li').each(function(){
                            if($(this).data('type') != 'plus'){
                                $(this).remove();
                            }
                        })
                    }

                    console.log("#tree"+id);

                    $("#tree"+id).fancytree({
                        extensions: ["glyph", "edit", "wide"],
                        checkbox: true,
                        selectMode: 1,
                        toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
                        glyph: {
                            map: {
                                doc: "glyphicon glyphicon-file",
                                docOpen: "glyphicon glyphicon-file",
                                checkbox: "glyphicon glyphicon-unchecked",
                                checkboxSelected: "glyphicon glyphicon-check",
                                checkboxUnknown: "glyphicon glyphicon-share",
                                error: "glyphicon glyphicon-warning-sign",
                                expanderClosed: "glyphicon glyphicon-plus-sign",
                                expanderLazy: "glyphicon glyphicon-plus-sign",
                                // expanderLazy: "glyphicon glyphicon-expand",
                                expanderOpen: "glyphicon glyphicon-minus-sign",
                                // expanderOpen: "glyphicon glyphicon-collapse-down",
                                folder: "glyphicon glyphicon-folder-close",
                                folderOpen: "glyphicon glyphicon-folder-open",
                                loading: "glyphicon glyphicon-refresh"
                                // loading: "icon-spinner icon-spin"
                            }
                        },
                        wide: {
                            iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                            iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                            levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
                        },
                        source:data,
                        select: function(event, data) {
                            // Get a list of all selected nodes, and convert to a key array:
                            var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                return node.key;
                            });
                            console.log('当前所有分类' + '_________________________' + $sel_keys);
                            if ($sel_keys.length >= $current_keys.length) {
                                for (var i = 0; i < $sel_keys.length; i++) {

                                    if($.inArray($sel_keys[i], $current_keys) === -1) {
                                        console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                                        console.log('上次所选的分类 --- ' + $current_keys);
                                        console.log('当前选择分类 --- ' + $sel_keys[i]);
                                        var $current_key = $sel_keys[i];
                                        var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                                        for (var j = 0; j < $_other_sibling.length; j++) {
                                            console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);

                                            $_other_sibling[j].setSelected(false);
                                            var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                                return node.key;
                                            });
                                        }
                                    }

                                }
                            }

                            //获取所有选中的分类将其加入到选择框中
                            var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                                console.log(node.key + ' ---- ' + node.title);
                                var $_html = '';
                                $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                                    "<div>" + node.title + "</div>" +
                                    "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                                    "</li>";
                                return $_html;
                            });
                            //将分类选择框中的分类全部清除
                            delete_all_category();
                            $('#select_category'+id).append($_html);
                            //将选择的分类id放入分类隐藏框中
                            $('#category_id'+id).val($sel_keys);
                            //将最新所选的分类集合跟新到$current_keys 里
                            $current_keys = $sel_keys;
                            console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
                        }
                    });

                    $('#category_tree'+id).click(function() {
                        $('#tree'+id).toggle();
                    });
                    $('#tree'+id).mouseleave(function() {
                        $('#tree'+id).hide();
                    });
                }

    }
</script>
<!-- js 脚本区域 end-->