<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:36
 */

use yii\helpers\Html;
use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--css 样式文件 start-->
<style>

</style>
<!-- css 样式文件 end-->


<?php $form = ActiveForm::begin(['method' => 'post']) ?>

<div class="action-forms" style="">
    <?= Html::input('submit',null,Yii::t('app', '提交'),['class' => 'btn btn-primary', 'id' => '_sub', 'type' => 'button', 'style'=>'float:right;'])?>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">



    <!-- row -->
    <div class="row" style="padding-top:40px;">


        
        <?= Html::hiddenInput('point_ids', $point_ids);?>
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-collapsed="false" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                    <div style="margin-top:0px;padding-left:30px;">能源分类</div>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tree smart-form" style="padding-left:20px;">

                            <li>
                                <?= MyHtml::treeView($category_tree, 'category_id')?>
                            </li>


                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        
        </article>
        <!-- WIDGET END -->

        <article class="col-sm-12 col-md-12 col-lg-6">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1"
                 data-widget-editbutton="false"
                 data-widget-collapsed="false"
                 data-widget-colorbutton="false"
                 data-widget-editbutton="false"
                 data-widget-togglebutton="false"
                 data-widget-fullscreenbutton="false"
                 data-widget-custombutton="false"
                 data-widget-sortable="false"
                 data-widget-deletebutton="false"
                >
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
                    <h2>地理位置分类</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tree smart-form" style="padding-left:20px;">

                            <li>
                                <?= MyHtml::treeView($location_tree, 'location_id')?>
                            </li>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->



        <?php ActiveForm::end(); ?>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


<!--注册js文件 start-->
<?php

?>
<!--注册js文件 end-->


<!-- js 脚本区域 start-->
<script>
    window.onload = function(){



        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });


        window.checkTheValue = function(label)
        {
            console.log($(label).find('input').val());
            $(label).find('input').attr('checked');
        }

        //submit click verrification
        $('#_sub').click(function(){
            console.log('submiter');
            var category_id = $('input[name=category_id]:checked').attr('value');
            var location_id = $('input[name=location_id]:checked').attr('value');
            console.log(category_id);
            console.log(location_id);
            if (!category_id) {

             alert('不选择能源分类，不会改变原来的能源分类');

            }

            if (!location_id) {
               alert('不选择地理位置，不会改变原来的地理位置分类');

            }


        });
    }
</script>
<!-- js 脚本区域 end-->