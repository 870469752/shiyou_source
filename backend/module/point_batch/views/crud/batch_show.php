<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:36
 */

use yii\helpers\Html;
use common\library\MyHtml;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
//echo '<pre>';
//echo  Url::toRoute('page').'?'.http_build_query(Yii::$app->request->getQueryParams());die;
//var_dump(http_build_query(Yii::$app->request->getQueryParams()));die;
?>
<style>
    .ms-container{
        background: transparent url('/img/multi-select/switch.png') no-repeat 50% 50%;
        width: 900px;
    }
    .custom-header{
        text-align: center;
        padding: 3px;
        background: #000;
        color: #fff;
    }
    ul.fancytree-container {
        max-height: 200px;
        width: 100%;
    }
</style>

<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' style = 'border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = MyActiveForm::begin(['method' => 'get', 'id' => '_ws']) ?>
            <fieldset>
                <legend>搜索条件</legend>
                <div class="form-group">
                    <div class="col-md-3">
                        <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Name'), 'class' => 'form-control'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=MyHtml::dropDownList('category_id', isset($category_id)?$category_id:null, $category_tree
                            , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <?=Html::dropDownList('protocol_id', isset($protocol_id)?$protocol_id:null, ['' => '',
                                 ''=>'所有点位',
                                1 => Yii::t('app', 'BACnet Point'),
                                4 => Yii::t('app', 'Calculate Point'),
                                7 => Yii::t('app', 'Simulation Point'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Point Type'), 'id' => 'time_type'])?>
                    </div>
                </div>
            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <a id="_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
            <?=Yii::t('app', 'Search')?>
            </a>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?=Yii::t('app', 'Point Batch')?></h2>
            <div class="jarviswidget-ctrls">

            </div>
        </header>
        <div class="widget-body">

            <?php $form = ActiveForm::begin(['method' => 'post']); ?>
            <fieldset>
                <!-- widget div-->
                <div class = 'form-group'>
                    <label class="col-sm-1 control-label"><?=Yii::t('app', 'Bacth Filter')?></label>
                    <div class = 'col-sm-6' >
                        <?=Html::dropDownList('point_ids', '', ['' => '',
                            ],
                            ['id' => 'point_select', 'multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Point Type')])?>
                    </div>
                </div>
                <!--    批量修改 点位的一些属性      -->
                <div class = 'form-group' style = 'margin-top:230px'>
                    <?= $form->field($model, 'filter_min_value')->textInput(['placeholder' => '最小过滤值']) ?>

                    <?= $form->field($model, 'filter_max_value')->textInput(['placeholder' => '最大过滤值']) ?>

                    <?= $form->field($model, 'value_type')->dropDownList([
                            ''=> '',
                            '0'=> Yii::t('app', '实时'),
                            '1'=> Yii::t('app', '累计'),
                            '2'=> Yii::t('app', '开关'),
                        ],['class' => 'select2', 'placeholder' => '请选择点位数值类型']) ?>

                    <?= $form->field($model, 'interval')->dropDownList([
                            ''=>'',
                            '60'=>'1 '.Yii::t('app', 'Minute'),
                            '300'=>'5 '.Yii::t('app', 'Minute'),
                            '600'=>'10 '.Yii::t('app', 'Minute'),
                            '1800'=>'30 '.Yii::t('app', 'Minute'),
                            '3600'=>'1 '.Yii::t('app', 'Hour'),
                            '18000'=>'5 '.Yii::t('app', 'Hour'),
                            '43200'=>'12 '.Yii::t('app', 'Hour'),
                            '86400'=>'24 '.Yii::t('app', 'Hour'),
                        ],['class' => 'select2', 'placeholder' => '请选择点位记录间隔']) ?>

                    <!--在系统中选择 点位单位-->
                    <?= $form->field($model, 'unit')->dropDownList(
                        $unit,
                        ['class'=>'select2', 'style' => 'width:200px', 'placeholder' => '请选择单位(默认为空)']
                    )?>

                    <!-- 分类 隐藏框 -->
                    <?=Html::hiddenInput('Point[category_id]', null, ['id' => 'category_id'])?>

                    <!-- 点位分类 -->
                    <label>分类</label>
                    <!--使用Fancytree 来做分类的展示-->
                    <div class="input-group" id = 'category_tree'>
                        <div rel="tooltip" data-original-title="分类选择" data-placement="top" id = 'category_name' class = 'select2-contaner select2-container-multi' style = 'margin-top:2px;width: 100%;' >
                            <ul id = 'select_category' class="select2-choices" style = 'min-height: 30px'>
                            </ul>
                        </div>

                        <div class="input-group-btn">
                            <button class="btn btn-default btn-primary" type="button"  id = '_clear'>
                                <i class="fa fa-mail-reply-all"></i> Clear
                            </button>
                        </div>
                    </div>
                    <div id = 'tree' style = 'display:none;position:absolute;width:100%;'></div>


                    <?= $form->field($model, 'is_shield')->checkbox() ?>

                    <?= $form->field($model, 'is_upload')->checkbox() ?>
                </div>
            </fieldset>
            <div class="form-actions">
                <?= Html::submitButton(Yii::t('app', 'Submit') , ['class' => 'btn btn-primary', 'id' => '_sub']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <!-------     图表内容 end-->
</section>
<!-- html 区域 end-->
<?php

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);

$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);
?>


<!-- js 脚本区域 start-->
<script>
    window.onload = function(){

        var $current_keys = [];
        var $count = 0;
        $('#point_select').multiSelect({
            selectableHeader: "<div class='custom-header'>搜索结果</div>",
            selectionHeader: "<div class='custom-header'>已选点位</div>",
            keepOrder: true
        });
        $('#point_select').multiSelect('deselect_all');
        //ajax提交
        $('#_submit').click(function() {
            var $url = '/point-batch/crud/ajax-get-point?' + $('#_ws').serialize();
            console.log($url);
            $.get($url, function(data) {
                console.log(data);
                if(data) {
                    //动态添加元素
                    $('#point_select').multiSelect('addOption', data);
                }
            }, 'json');
        });

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
            }
            $("#_loop").slideToggle("slow");

        });

        function delete_all_category() {
            $('.select2-choices li').each(function(){
                if($(this).data('type') != 'plus'){
                    $(this).remove();
                }
            })
        }

        $("#tree").fancytree({
            extensions: ["glyph", "edit", "wide"],
            checkbox: true,
            selectMode: 2,
            toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
            glyph: {
                map: {
                    doc: "glyphicon glyphicon-file",
                    docOpen: "glyphicon glyphicon-file",
                    checkbox: "glyphicon glyphicon-unchecked",
                    checkboxSelected: "glyphicon glyphicon-check",
                    checkboxUnknown: "glyphicon glyphicon-share",
                    error: "glyphicon glyphicon-warning-sign",
                    expanderClosed: "glyphicon glyphicon-plus-sign",
                    expanderLazy: "glyphicon glyphicon-plus-sign",
                    // expanderLazy: "glyphicon glyphicon-expand",
                    expanderOpen: "glyphicon glyphicon-minus-sign",
                    // expanderOpen: "glyphicon glyphicon-collapse-down",
                    folder: "glyphicon glyphicon-folder-close",
                    folderOpen: "glyphicon glyphicon-folder-open",
                    loading: "glyphicon glyphicon-refresh"
                    // loading: "icon-spinner icon-spin"
                }
            },
            wide: {
                iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
                iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
                levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
            },
            source:<?=$tree_json;?>,
            select: function(event, data) {
                // Get a list of all selected nodes, and convert to a key array:
                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                    return node.key;
                });
                console.log('当前所有分类' + '_________________________' + $sel_keys);
                if ($sel_keys.length >= $current_keys.length) {
                    for (var i = 0; i < $sel_keys.length; i++) {

                        if($.inArray($sel_keys[i], $current_keys) === -1) {
                            console.log($current_keys + '___________---    循环            -------__________________________________________________-' + $sel_keys[i]);

                            console.log('上次所选的分类 --- ' + $current_keys);
                            console.log('当前选择分类 --- ' + $sel_keys[i]);
                            var $current_key = $sel_keys[i];
                            var $_other_sibling = data.tree.getNodeByKey($current_key).getParent().getOtherChildren($current_key);
                            for (var j = 0; j < $_other_sibling.length; j++) {
                                console.log($current_keys + '---------------------------移除' + $_other_sibling[j].key);
//                                    if($sel_keys.index(',')) {
////                                        var $sel_keys_arr = $sel_keys.split(',');
////                                        for(var k = 0; k < $sel_keys_arr.length; k++) {
////                                            if($sel_keys_arr[k] == $_other_sibling[j].key)
////                                        }
//                                        $current_key = $sel_keys;
//                                    }else {
//                                        $current_key = $sel_keys;
//                                    }
                                $_other_sibling[j].setSelected(false);
                                var $sel_keys = $.map(data.tree.getSelectedNodes(), function(node) {
                                    return node.key;
                                });
                            }
                        }

                    }
                }

                //获取所有选中的分类将其加入到选择框中
                var $_html = $.map(data.tree.getSelectedNodes(), function(node) {
                    console.log(node.key + ' ---- ' + node.title);
                    var $_html = '';
                    $_html +="<li class='select2-search-choice' data-value = "+node.key+" >" +
                        "<div>" + node.title + "</div>" +
                        "<a style = 'display:none' href='javascript:void(0)' class='select2-search-choice-close _delete' tabindex='-1' ></a>" +
                        "</li>";
                    return $_html;
                });
                //将分类选择框中的分类全部清除
                delete_all_category();
                $('#select_category').append($_html);
                //将选择的分类id放入分类隐藏框中
                $('#category_id').val($sel_keys);
                //将最新所选的分类集合跟新到$current_keys 里
                $current_keys = $sel_keys;
                console.log($current_keys + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@' + $count++);
            }
        });

        $('#_clear').click(function () {
            delete_all_category();
            $("#tree").fancytree();
        });

        $('#category_tree').click(function() {
            $('#tree').toggle();
        });
        $('#tree').mouseleave(function() {
            $('#tree').hide();
        })
        //提交验证
        $('#_sub').click(function() {

        })
    }
</script>
<!-- js 脚本区域 end-->