<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:32
 */

namespace backend\module\point_batch\controllers;

//yii 引用
use backend\models\Location;
use backend\models\PointCategoryRel;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
//模型 引用
use backend\models\Point;
use backend\models\Protocol;
use backend\models\search\PointSearch;
use backend\models\Unit;
use backend\models\Category;
use backend\models\EnergyCategory;
use backend\models\PointCategoryRelNew;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
    }

    /**
     * ajax 获取搜索点位
     */
    public function actionAjaxGetPoint()
    {
        $searchModel = new PointSearch();
        $dataProvider = $searchModel->_search(Yii::$app->request->getQueryParams());
        if($data = $dataProvider->query->asArray()->all()) {
            $point_data = MyFunc::_map($data, 'id', 'name');

            MyFunc::multi_select($point_data);
        }
    }

    /**
     * ajax 获取搜索点位
     */
    public function actionAjaxFindPoint()
    {
        $query_params=Yii::$app->request->getQueryParams();
//        echo '<pre>';
//        print_r($query_params);
        $name=$query_params['name'];
        $protocol_id=$query_params['protocol_id'];
        $energy_cagetegory_id=$query_params['energy_category_id'];
        $location_category_id=$query_params['location_category_id'];
        //拼凑两个参数，分别为地理位置 与能源类型的 查找
        $energy_query_param=[
            'name'=>$name,
            'category_id'=>$energy_cagetegory_id,
            'protocol_id'=>$protocol_id
        ];

        $location_query_param=[
            'name'=>$name,
            'category_id'=>$location_category_id,
            'protocol_id'=>$protocol_id
        ];

        $searchModel = new PointSearch();
        $dataProvider1 = $searchModel->_search_test($energy_query_param,1);
        $dataProvider2 = $searchModel->_search_test($location_query_param,2);
        $data1 = $dataProvider1->query->asArray()->all();
        $data2 = $dataProvider2->query->asArray()->all();
//        print_r($energy_query_param);
//        print_r($data1);
//        print_r($data2);
        if(empty($data1) && empty($data2)) {
            $point_data1 = MyFunc::_map($data1, 'id', 'name');
            $point_data2 = MyFunc::_map($data2, 'id', 'name');

//            print_r($point_data1);
//            print_r($point_data2);
            $point_data=array_intersect($point_data1,$point_data2);
            MyFunc::multi_select($point_data);
        }
        elseif(!empty($data1)){
            $point_data = MyFunc::_map($data1, 'id', 'name');
            MyFunc::multi_select($point_data);
        }
        elseif(!empty($data2)){
            $point_data = MyFunc::_map($data2, 'id', 'name');
            MyFunc::multi_select($point_data);
        }

    }


    /**
     * ajax 获取搜索点位 (rewrite)
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function actionAjaxFindPointSpecific()
    {
        $protocol = new Protocol();
        $query_params=Yii::$app->request->getQueryParams();
        $name=$query_params['name'];
        $protocol_id=$query_params['protocol_id'];
        $energy_cagetegory_id=(integer)$query_params['energy_category_id'];
        $location_category_id=(integer)$query_params['location_category_id'];
        $wanteIds=[];
        if($energy_cagetegory_id==0 && $location_category_id!=0)
            $wanteIds = [[$location_category_id]];
        else if($location_category_id==0 && $energy_cagetegory_id!=0)
            $wanteIds = [[$energy_cagetegory_id]];
        else if($energy_cagetegory_id!=0 && $location_category_id!=0)
            $wanteIds=[[$energy_cagetegory_id],[$location_category_id]];
        $query_param=[
            'name'=>$name,
            'category_id'=>$wanteIds,
            'protocol_id'=>$protocol_id
        ];
        $searchModel = new PointSearch();
        $dataProvider = $searchModel->_search_test($query_param);
        $data = $dataProvider->query->asArray()->all();
//        if ($data) {
//            foreach ($data as $key => $value) {
//                $value['protocol_id'] = $protocol->getbyId($value['protocol_id'])->getAttribute('name');
//                $data[$key] = $value;
//            }
            return MyFunc::multi_select_point($data);
//        }
    }


    public function actionBatch()
    {
        $model = new Point();
        $param = Yii::$app->request->getBodyParams();

        if($param && $model->load(Yii::$app->request->getBodyParams())) {
            Point::saveBatchPoint($param['point_ids'], $param);
        }

            $tree = MyFunc::TreeData(Category::getAllCategory());
            //在此添加一个 所有分类
            array_unshift($tree, ['id' => 0, 'name' => '所有分类']);
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo());
            $tree_json = json_encode($tree_data);

            return $this->render('batch_show',[
                    'category_tree' => $tree,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                    'model' => $model
                ]);
    }


    public function actionClassify()
    {
        $model = new Point();
        $param = Yii::$app->request->getBodyParams();

        // echo '<pre>';
        // print_r($param);die;

        if($param && $model->load(Yii::$app->request->getBodyParams())) {
            Point::saveBatchPoint($param['point_ids'], $param);
        }

        $tree = MyFunc::TreeData(Category::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);


        $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
        $tree_data = MyFunc::get_fancyTree_data1(Category::getCategoryInfo());
        $tree_data_location=MyFunc::get_fancyTree_data1(Location::getCategoryInfo());
        $tree_json = json_encode($tree_data);
        $tree_json_location=json_encode($tree_data_location);
        return $this->render('point_classify',[
            'category_tree' => $tree,
            'location_tree' =>$location_tree,
            'unit' => $unit,
            'tree' => $tree_data,
            'tree_json' => $tree_json,
            'tree_json_location'=>$tree_json_location,
            'model' => $model
        ]);
    }


    /**
    * @auther pzzrudlf pzzrudlf@gmail.com
    * @date 2015.12.16 
    */

    public function actionEditPointCategoryRel()
    {
        $allIds = [];
        $model = new Point();
        $param = Yii::$app->request->getBodyParams();
        $tree = MyFunc::TreeData(EnergyCategory::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);


        $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
        $tree_data = MyFunc::get_fancyTree_data1(Category::getCategoryInfo());
        $tree_data_location=MyFunc::get_fancyTree_data1(Location::getCategoryInfo());
        $tree_json = json_encode($tree_data);
        $tree_json_location=json_encode($tree_data_location);


        if (!isset($param['point_ids'])) {

            return $this->render('point-category-rel',[
                'category_tree' => $tree,
                'location_tree' =>$location_tree,
                'unit' => $unit,
                'tree' => $tree_data,
                'tree_json' => $tree_json,
                'tree_json_location'=>$tree_json_location,
                'model' => $model

                ]);

        } else {


            if (isset($param['category_id']) && isset($param['location_id'])) {

                $param['point_ids'] = explode(',', $param['point_ids']);
                if (isset($param['Point']['category_id1'])) {
                    $param['Point']['location_id']= $param['Point']['category_id1'];
                }

                $param['Point']['category_id'] = $param['category_id'];
                $param['Point']['location_id'] = $param['location_id'];

                Point::saveBatchPoint($param['point_ids'], $param);

                return $this->render('point-category-rel',[

                    'category_tree' => $tree,
                    'location_tree' =>$location_tree,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                    'tree_json_location'=>$tree_json_location,
                    'model' => $model

                ]);

            } elseif (isset($param['category_id']) && !isset($param['location_id'])) {

                //获取能源分类下所有的能源分类id

                if ($allId = EnergyCategory::getCategoryInfo()) {

                    foreach ($allId as $key => $value) {
                        $allIds[] = $value['key'];
                    }

                }

                //获取每个点位对应的locationid
                $param['point_ids'] = explode(',', $param['point_ids']);
                $param['Point']['category_id'] = $param['category_id'];

                if ($param['point_ids']) {

                    foreach ($param['point_ids'] as $key => $value) {

                        if ($dd = PointCategoryRelNew::getPointCategoryId($value)) {
                            foreach ($dd as $key1 => $value1) {
                                if (!in_array($value1['category_id'], $allIds)) {
                                    $location_id = $value1['category_id'];
                                }
                            }
                        }

                        $pa['_csrf'] = $param['_csrf'];
                        $pa['point_ids'] = [$value];
                        $pa['category_id'] = $param['category_id'];
                        $pa['location_id'] = (string)$location_id;
                        $pa['Point']['category_id'] = $param['category_id'];
                        $pa['Point']['location_id'] = (string)$location_id;

                        Point::saveBatchPoint($pa['point_ids'], $pa);

                    }

                }

                return $this->render('point-category-rel',[

                    'category_tree' => $tree,
                    'location_tree' =>$location_tree,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                    'tree_json_location'=>$tree_json_location,
                    'model' => $model

                ]);

            } elseif (!isset($param['category_id']) && isset($param['location_id'])) {

                //获取能源分类下所有的能源分类id

                if ($allId = EnergyCategory::getCategoryInfo()) {

                    foreach ($allId as $key => $value) {
                        $allIds[] = $value['key'];
                    }

                }

                //获取每个点位对应的categoryid
                $param['point_ids'] = explode(',', $param['point_ids']);
                $param['Point']['location_id'] = $param['location_id'];

                if ($param['point_ids']) {

                    foreach ($param['point_ids'] as $key => $value) {

                        if ($dd = PointCategoryRelNew::getPointCategoryId($value)) {
                            foreach ($dd as $key1 => $value1) {
                                if (in_array($value1['category_id'], $allIds)) {
                                    $category_id = $value1['category_id'];
                                }
                            }
                        }

                        $pa['_csrf'] = $param['_csrf'];
                        $pa['point_ids'] = [$value];
                        $pa['location_id'] = $param['location_id'];
                        $pa['category_id'] = (string)$category_id;
                        $pa['Point']['location_id'] = $param['location_id'];
                        $pa['Point']['category_id'] = (string)$category_id;

                        Point::saveBatchPoint($pa['point_ids'], $pa);

                    }

                }

                return $this->render('point-category-rel',[

                    'category_tree' => $tree,
                    'location_tree' =>$location_tree,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                    'tree_json_location'=>$tree_json_location,
                    'model' => $model

                ]);

            } elseif (!isset($param['category_id']) && !isset($param['location_id'])) {

                return $this->render('point-category-rel-edit-new',[
                    'category_tree' => $tree,
                    'location_tree' =>$location_tree,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                    'tree_json_location'=>$tree_json_location,
                    'model' => $model,
                    'point_ids' => $param['point_ids']
                ]);

            }
        }
    }

    /**
     * Displays a single Point model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new Point model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Point;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing Point model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->savePoint()) {
            return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);
            $unit = MyFunc::SelectDataAddArrayTop(Unit::getAllUnit());
            $tree_data = MyFunc::get_fancyTree_data(Category::getCategoryInfo());
            $model->filter_max_value = MyFunc::JsonFindValue($model->value_filter, 'data.max');
            $model->filter_min_value =MyFunc::JsonFindValue($model->value_filter, 'data.min');

            $tree_json = json_encode($tree_data);
            return $this->render('update', [
                    'model' => $model,
                    'unit' => $unit,
                    'tree' => $tree_data,
                    'tree_json' => $tree_json,
                ]);
        }
    }

    /**
     * Deletes an existing Point model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Point model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Point the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Point::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
