<?php

namespace backend\module\scheduledtask;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\scheduledtask\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
