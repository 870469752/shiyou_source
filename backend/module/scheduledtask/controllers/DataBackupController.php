<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-2-9
 * Time: 上午9:39
 */

namespace backend\module\scheduledtask\controllers;


use common\library\MyFunc;
use Yii;
use backend\models\ScheduledTask;
use backend\models\search\ScheduledTaskSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OperateLog;
class DataBackupController extends MyController {
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScheduledTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = ScheduledTask::getDataBackup();
        $url_prefix = '/scheduled-task/data-backup/';
        !$model?$this->redirect($url_prefix.'create'):$this->redirect($url_prefix.'update?id='.$model->id);
    }

    /**
     * Displays a single ScheduledTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new ScheduledTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScheduledTask;

        if ($model->load(Yii::$app->request->post()) && $result = $model->saveTask()) {
            /*操作日志*/
            $op['zh'] = '创建任务';
            $op['en'] = 'Create task';
            $this->getLogOperation($op,'',$result);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing ScheduledTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $result = $model->saveTask()) {
            /*操作日志*/
            $op['zh'] = '修改数据备份时间';
            $op['en'] = 'Update the time of data backup';
            $this->getLogOperation($op,'',$result);
            return $this->redirect(['index']);
        } else {
            $model->db_backup_type = Myfunc::JsonFindValue($model->args, 'data.data_backup_mode');

            $interval_time = Myfunc::JsonFindValue($model->schedule_mode, 'data.cron');
            $model->export_start = date('Y-m-d', strtotime(Myfunc::JsonFindValue($model->schedule_mode, 'data.start_date')));
            $model->export_end = date('Y-m-d', strtotime(Myfunc::JsonFindValue($model->schedule_mode, 'data.end_date')));
            $model->mission_mode = Myfunc::JsonFindValue($model->schedule_mode, 'data.type');
            //将秒处理成对应的数据
            $model->statistic_time = MyFunc::SecondChangeToTime($model->statistic_time);
            $model->export_interval_time = $model->mission_mode != 'cron' ? MyFunc::SecondChangeToTime($interval_time) : $interval_time;
            return $this->render('update', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing ScheduledTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScheduledTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduledTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduledTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
} 