<?php

namespace backend\module\scheduledtask\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\ScheduledTask;
use backend\models\search\ScheduledTaskSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for ScheduledTask model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScheduledTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduledTaskSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single ScheduledTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScheduledTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScheduledTask;
        if ($model->load(Yii::$app->request->post()) && $model->saveTask()) {
            echo '<pre>';
            print_r(Yii::$app->request->post());
            die;
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ScheduledTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveTask()) {
            return $this->redirect(['index']);
        } else {
            $model->export_type = Myfunc::JsonFindValue($model->args, 'data.export_args.type');
            $model->export_path = Myfunc::JsonFindValue($model->args, 'data.export_args.export_path');
            $model->export_method = Myfunc::JsonFindValue($model->args, 'data.export_type.type');
            $model->statistic_time = Myfunc::JsonFindValue($model->args, 'data.statistic_gap');
            $model->export_title = Myfunc::JsonFindValue($model->args, 'data.export_args.title');
            $model->export_water_mark = Myfunc::JsonFindValue($model->args, 'data.export_args.water_mark');
            $model->export_order = Myfunc::JsonFindValue($model->args, 'data.export_args.heading_order');
            $model->export_ids = Myfunc::JsonFindValue($model->args, 'data.point_id');
            $model->export_directory = Myfunc::JsonFindValue($model->args, 'data.export_args.table_of_contents');

            $model->mission_mode = Myfunc::JsonFindValue($model->schedule_mode, 'data.type');
            $interval_time = Myfunc::JsonFindValue($model->schedule_mode, 'data.' . $model->mission_mode);
            $model->export_start = date('Y-m-d', strtotime(Myfunc::JsonFindValue($model->schedule_mode, 'data.start_date')));
            $model->export_end = date('Y-m-d', strtotime(Myfunc::JsonFindValue($model->schedule_mode, 'data.end_date')));
            //将秒处理成对应的数据
            $model->statistic_time = MyFunc::SecondChangeToTime($model->statistic_time);
            $model->export_interval_time = $model->mission_mode != 'cron' ? MyFunc::SecondChangeToTime($interval_time) : $interval_time;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ScheduledTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScheduledTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduledTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduledTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
