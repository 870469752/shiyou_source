<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ScheduledTask $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Scheduled Task',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
