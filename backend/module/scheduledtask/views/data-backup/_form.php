<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;
/**
 * @var yii\web\View $this
 * @var backend\models\ScheduledTask $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>

                        <?= Html::hiddenInput('ScheduledTask[type]', 'db_backup')?>

                        <?= Html::hiddenInput('ScheduledTask[db_backup_type]', 'partial')?>

						<?= $form->field($model, 'export_start')->textInput(['id' => 'start_time']) ?>

                        <?= $form->field($model, 'export_end')->textInput(['id' => 'end_time']) ?>

                        <hr />
                        <!--                       任务计划的间隔时间 及 报表导出的统计时间                                               -->
                        <div id = 'mission_mode'>
                            <!--                  任务计划的间隔时间 分为 定时任务 ， 定期任务                                                                  -->
                            <?= $form->field($model, 'mission_mode')->dropDownList(['' => '',
                                                    'interval' => Yii::t('app', 'Timing Task'),
                                                    'cron' => Yii::t('app', 'Regular Task')],
                                                    ['class' => 'select2', 'placeholder' => '选择时间模式']
                                )?>
                            <br />
                                <!-- 定时任务 time   这里 a标签显示 要与 隐藏框相邻 将会直接将a标签 输入的值赋值给隐藏框-->
                            <?= Html::hiddenInput('ScheduledTask[export_interval_time]', $model->export_interval_time ? $model->export_interval_time : 'Empty', ['id' => 'interval_time'])?>
                            <div id = '_interval' class = 'time_mode'>
                                    <lable><?=Yii::t('app', 'Repeat Interval')?> :</lable>
                                    <a href="#"  id="interval" data-placement="right" data-type = 'timec' data-title="Please, fill time"  data-pk="1"  class="editable editable-click"></a>
                                </div>
                                <!-- 定期任务 date-->
                                <div id = '_cron' class = 'time_mode'>
                                    <lable><?=Yii::t('app', 'Backup Date')?> :</lable>
                                    <span id='selector'></span>
                                </div>
                        </div>
                                                        <!--  end by 时间选择 -->

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
    $this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
    $this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
//cron
    $this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
    $this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
    $this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
    $this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
?>

<script>
    window.onload = function(){
        var $start_time = $('#start_time').val();
        var $end_time = $('#end_time').val();
        var $interval = $('#interval_time').val();
        var $static =$('#static_time').val();
        /* linux 任务计划时间插件*/
        var $cron = $('#selector').cron({
            initial: '* * * * *',
            onChange: function() {
                $('#interval_time').val($(this).cron("value"))
            },
            useGentleSelect: true
        });

        /* 任务模式下拉onchange*/
        $('#scheduledtask-mission_mode').change(function() {
            $('.time_mode').hide();
            $('#_'+$(this).val()+'').show();
        })
        $('#scheduledtask-mission_mode').trigger('change');

        /*                                 update 初始化                       */

        /* 时间插件的调用 */
        timeTypeChange('day', $("#start_time"));
        timeTypeChange('day', $("#end_time"));
        $('#start_time').val($start_time);
        $('#end_time').val($end_time);

        /* x-editable 生成时间选择插件 */
        timeOfEditable( $("#interval") );
        timeOfEditable( $('#static') );
        $('#interval').text($interval);
        $('#static').text($static);
        $('#scheduledtask-mission_mode').val() === 'cron' ? $cron.cron('value', $interval) : '';

    }
</script>