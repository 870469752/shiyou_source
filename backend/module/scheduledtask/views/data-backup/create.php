<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ScheduledTask $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Scheduled Task',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
