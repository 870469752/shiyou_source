<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 */
$this->title = '创建模式控制信息';
?>
    <?= $this->render('create_form_new', [
    'model' => $model,
    'modeselect'=>$modeselect,
    ]) ?>
