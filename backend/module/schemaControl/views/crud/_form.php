<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<?php
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset {
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }

</style>
<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- START ROW -->
    <div class="row">
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->
                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
                        <!-- 时间模式选择-->
                        <fieldset id="timemode">
                            <section>
                                <div class="form-group field-scheduledtask-export_title has-success">
                                    <label class="label">时间模式选择</label>
                                    <?=Html::dropDownList('_timemode', '', $modeselect, ['id'=>'select','class' => 'select2', 'placeholder' => '请选择时间模式'])?>
                                </div>
                            </section>
                        </fieldset>
                        <!-- 开始时间，结束时间-->
                        <div id='control_info' style="border: 1px solid #36b1ff">
                        <fieldset >
                            <fieldset>
                                        <label style="margin-left: -10px" class="label">执行计划</label>
                            </fieldset>
                            <div  style="float:left">
                                <label class="label">开始时间</label>
                                <label class="">
                                    <select id="begin"  class="form-control" name="begin">
                                        <option value="0" selected >0点</option>
                                        <?php  for($i=1;$i<24;$i++){ ?>
                                            <option value="<?=$i?>"><?=$i?>点</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                            </div>
                            <div style="float:left;margin: 25px 50px 0px 50px">
                                <label class="">---</label>
                                </label>
                            </div>
                            <div id="enddate"style="float:left">
                                <label class="label">结束时间</label>
                                <label class="">
                                    <select id="end" class="form-control" name="end">
                                        <option value="0" selected>0点</option>
                                        <?php  for($i=1;$i<24;$i++){ ?>
                                            <option value="<?=$i?>"><?=$i?>点</option>
                                        <?php }?>
                                    </select><i></i>
                                </label>
                                </label>
                            </div>
                        </fieldset>
                        <!--控制字段-->
                        <fieldset id="control_attr">
                            <div style="clear:both">
                                <div style="float:left;margin-right: 10px">
                                    <label class="label">控制字段</label>
                                    <input type="text"  class="form-control" name="control_attr_name" value="" style="width:200px">
                                </div>
                                <div style="float:left">
                                    <label class="label">默认值</label>
                                    <input type="text"  class="form-control" name="control_attr_value" value="" style="width:200px">
                                </div>
                                <div  style="float:left;margin: 25px 100px ">
                                    <label a="vvv" class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-ok" ></span></button></label>
                                    <label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>
                                </div>
                            </div>
                        </fieldset>
                        </div>
                        <!--存储控制字段信息-->
                        <input type="text" id="control_attr_name_hidden" name="control_attr_name_hidden" value="" style="display:none">
                        <input type="text" id="control_attr_value_hidden"  name="control_attr_value_hidden" value="" style="display:none">
                        <input type="text" id="start_array_hidden" name="start_array_hidden" value="" style="display:none">
                        <input type="text" id="end_array__hidden"  name="end_array__hidden" value="" style="display:none">
                        <div class="form-actions">
                            <button type="button" id="" class="create btn-success" onclick="addtimeplan(this)">继续添加执行计划</button>&nbsp;&nbsp;
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!-- end widget content -->
            </div>
            <!-- end widget div -->
    </div>
    <!-- end widget -->
    </article>
    <!-- END COL -->
    </div>
</section>

<script>
    var controlInfo_name_array=[]; //控制字段名称二维数组
    var controlInfo_value_array=[];//控制字段值二维数组
    var start_array=[];//开始时间二维数组
    var end_array=[];//结束时间二维数组
    var controlInfo_name=[];  //控制字段名称
    var controlInfo_value=[]; //控制字段值
    var starttime='';
    var endtime='';
    window.onload=function(){
        //点击加号添加控制信息
        window.exedel = function(object){
            var str='<div style="clear:both">'+
                '<div style="float:left;margin-right: 10px">'+
                '<label class="label">控制字段</label>'+
                '<input type="text"  class="form-control" name="control_attr_name" value="" style="width:200px">'+
                '</div>'+
                '<div style="float:left">'+
                '<label class="label">默认值</label>'+
                '<input type="text"  class="form-control" name="control_attr_value" value="" style="width:200px">'+
                '</div>'+
                '<div  style="float:left;margin: 25px 100px ">'+
                '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-ok" ></span></button></label>'+
                '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
                '</div>'+
                '</div>';
            if(object.name=='ok'){
                $('#control_attr').append(str);
            }else{
                $(object).parent().parent().parent().remove();
            }
        };

        $('#_submit').click(function(event){
        /*    $(":input").each(
                function() {
                    if($(this).val() ==''){
                        alert('请检查必输项！');
                        return false;
                    }
                }
            );*/
            $('#control_attr_name_hidden').val(JSON.stringify(controlInfo_name_array));
            $('#control_attr_value_hidden').val(JSON.stringify(controlInfo_value_array));
            $('#start_array_hidden').val(JSON.stringify(start_array));
            $('#end_array__hidden').val(JSON.stringify(end_array));
            console.log($('#control_attr_name_hidden').val());
            console.log( $('#control_attr_value_hidden').val());
            console.log($('#start_array_hidden').val());
            console.log( $('#end_array__hidden').val());
            controlInfo_name_array=[];
            controlInfo_value_array=[];
            start_array=[];
            end_array=[];
        });

        //时间模式切换时
        $("#select").change(function(){
            var id=$(this).val();
            alert($(this).parent().html());
            $.ajax({
                type: "POST",
                url: "/schemaControl/crud/get-time-mode",
                data: {'id':id},
                success: function (msg) {
                    if(msg=='[]'|| msg=='' || msg==null){

                    }else{
                        var time_info=eval("("+msg+")");
                        console.log(time_info);
                        var time_mode=eval("("+time_info['time_mode']+")");
                        console.log(time_mode);

                    }

                }
            })
        });
    }

    //保存时给控制字段赋值
    function setAttr(){
        starttime=$('#begin').val();
        endtime=$('#end').val();
        $("input[name='control_attr_name']").each(
            function() {
               if($(this).val() !=''){
                   controlInfo_name.push($(this).val());
               }
            }
        );
        $("input[name='control_attr_value']").each(
            function() {
                if($(this).val() !=''){
                    controlInfo_value.push($(this).val());
                }
            }
        );
    }

    //添加执行计划
    function addtimeplan(object){
        setAttr();
        controlInfo_name_array.push(controlInfo_name);
        controlInfo_value_array.push(controlInfo_value);
        start_array.push(starttime);
        end_array.push(endtime);
        clean();
        alert('添加成功');
    }

    //清空
    function clean(){
        controlInfo_name=[];
        controlInfo_value=[];
        $("input[name='control_attr_name']").each(
            function() {
                $(this).val('') ;
            }
        );
        $("input[name='control_attr_value']").each(
            function() {
                $(this).val('') ;
            }
        );
    }

</script>