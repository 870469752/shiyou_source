<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 */

$this->title = 'Update Schema Manage: ' . $model->id;
?>
    <?= $this->render('update_form_new', [
    'model' => $model,
    'modeselect'=>$modeselect,
    'modelarr'=>$modelarr,
    ]) ?>
