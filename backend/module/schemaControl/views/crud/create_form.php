<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<?php
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .btn-success1{
        color: #fff;
        background-color: #73959E;
        border-color: #659265;
    }
    .btn-success2{
        color: #fff;
        background-color: #a90329;
    border-color: #659265;
    }
    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset {
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }
</style>
<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- START ROW -->
    <div class="row">
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->
                    <!-- widget content -->
                    <div class="widget-body">
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
                        <!-- 时间模式选择-->
                        <fieldset>
                            <section>
                                <div class="form-group field-scheduledtask-export_title has-success">
                                    <label class="label">模式标题</label>
                                    <input type="text" id="title" class="form-control" name="name" value="">
                                </div>
                            </section>
                        </fieldset>
                        <fieldset id="timemode">
                            <section>
                                <div class="form-group field-scheduledtask-export_title has-success">
                                    <label class="label">时间模式选择</label>
                                    <?=Html::dropDownList('_timemode',$model->shema_manage_id, $modeselect, ['id'=>'select','class' => 'select2', 'placeholder' => '请选择时间模式'])?>
                                </div>
                            </section>
                        </fieldset>
                        <div id="timebutton" style=""></div>
                        <!-- 开始时间，结束时间-->
                        <div id='control_info' style="border: 1px solid #36b1ff">
                            <fieldset >
                                <fieldset>
                                    <label style="margin-left: -10px" class="label">执行计划</label>
                                </fieldset>
                                <div  style="float:left;margin-right: 108px">
                                        <label class="label" style="float:left">开始时间</label>
                                        <select id="begin"  class="form-control" name="begin" style="float:left;">
                                            <option value="0" selected >0点</option>
                                            <?php  for($i=1;$i<24;$i++){ ?>
                                                <option value="<?=$i?>"><?=$i?>点</option>
                                            <?php }?>
                                        </select>
                                </div>
                                <div id="enddate"style="float:left">
                                    <label class="label" style="float:left">结束时间</label>
                                        <select id="end" class="form-control" name="end">
                                            <option value="0" selected>0点</option>
                                            <?php  for($i=1;$i<24;$i++){ ?>
                                                <option value="<?=$i?>"><?=$i?>点</option>
                                            <?php }?>
                                        </select>
                                </div>
                                <div style="float:left;margin-left: 800px;margin-top:20px">
                                    <button type="button" id="delete" class="create btn-success2" onclick="deleteBytime()" style="display: none">删除</button>
                                </div>
                            </fieldset>
                            <!--控制字段-->
                            <fieldset id="control_attr">
                                <div style="clear:both">
                                    <div style="float:left;margin-right: 10px">
                                        <label class="label">控制字段</label>
                                        <input type="text"  class="form-control" name="control_attr_name0" value="" style="width:200px">
                                    </div>
                                    <div style="float:left">
                                        <label class="label">默认值</label>
                                        <input type="text"  class="form-control" name="control_attr_name0" value="" style="width:200px">
                                    </div>
                                    <div  style="float:left;margin: 25px 100px ">
                                        <label  class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>
                                        <label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <!--存储控制字段信息-->
                        <input type="text" id="control_data" name="control_data" value="" style="display:none">
                        <div class="form-actions">
                            <button type="button" id="" class="create btn-success" onclick="addtimeplan(this)">添加执行计划</button>&nbsp;&nbsp;
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!-- end widget content -->
            </div>
            <!-- end widget div -->
    </div>
    <!-- end widget -->
    </article>
    <!-- END COL -->
    </div>
</section>

<script>
    //data数组,time,control数组
    var data=[];
    var time=[];
    var control=[];
    //控制字段与默认值
    var control_name_value=[];
    //时间段个数
    var time_count=0;
    //每个时间段控制记录数
    var control_name_count=0;
    //总记录数
    var count=0;
    //得到data中所有时间
    var alltime=[];
    //当前修改的时间段
    var currentTime=[];
    //删除的起始时间
    var deletestart;
    window.onload=function(){
        //点击加号添加控制信息
        //初始化函数
        Init();
        window.exedel = function(object){
            if(object.name=='ok'){
                control_name_count++;
                var name='control_attr_name'+control_name_count;
                var str='<div style="clear:both">'+
                    '<div style="float:left;margin-right: 10px">'+
                    '<label class="label">控制字段</label>'+
                    '<input type="text"  class="form-control" name="'+name+'" value="" style="width:200px">'+
                    '</div>'+
                    '<div style="float:left">'+
                    '<label class="label">默认值</label>'+
                    '<input type="text"  class="form-control" name="'+name+'" value="" style="width:200px">'+
                    '</div>'+
                    '<div  style="float:left;margin: 25px 100px ">'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
                    '</div>'+
                    '</div>';
                $('#control_attr').append(str);
            }else{
                control_name_count--;
                $(object).parent().parent().parent().remove();
            }
        };

        $('#_submit').click(function(event){
            $('#control_data').val(JSON.stringify(data));
            console.log(JSON.stringify(data));
        });
    }

    function addtimeplan(){
        var starttime=parseInt($('#begin').val());
        var endtime=parseInt($('#end').val());
        $('#delete').hide();
        if(starttime<endtime){
            if(TimeIsRepeat(starttime,endtime)){
                time.push(starttime);  time.push(endtime);
                var s=starttime+'点'+'--'+endtime+'点';
                var str='<button type="button"  class="create btn-success1" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';
                $('#timebutton').append(str);
                setAttr();
                var temp=[];
                temp.push(time);temp.push(control);
                data.push(temp);
                count++;
               // console.log(data);
                clean();
                alert('添加成功！');
            }else{
                if(TimeIsSime(starttime,endtime))
                {
                    update(starttime,endtime);
                  //  console.log(data);
                }else
                {
                    alert('该时间段已存在');
                    return ;
                }
            }
        }else{
            alert('起始时间不能大于结束时间');
        }

    }
    //保存时给控制字段赋值
    function setAttr(){
        var flag=true;
        for(var i=0;i<=control_name_count;i++){
            var name='control_attr_name'+i;
            $("input[name="+name+"]").each(
                function() {
                    if($(this).val() !=''){
                        control_name_value.push($(this).val());
                    }else{
                        control_name_value.push('');
                    }
                }
            );
            control.push(control_name_value);
            control_name_value=[];
        }
        if(!flag){
         //   alert('有字段未填写，请检查！');
        }
    }
    //点击时间按钮显示详细信息
    function show(object){
        flag=true;
        var text=$(object).html();
        if((text.substring(1,2)=='点') && (text.substring(5,6)=='点')){
            var begin=parseInt(text.substring(0,1));
            var end=parseInt(text.substring(4,5));
        }else if((text.substring(1,2)=='点') && (text.substring(6,7)=='点')){
            var begin=parseInt(text.substring(0,1));
            var end=parseInt(text.substring(4,6));
        }else if((text.substring(2,3)=='点') && (text.substring(7,8)=='点')){
            var begin=parseInt(text.substring(0,2));
            var end=parseInt(text.substring(5,7));
        }else if((text.substring(2,3)=='点') && (text.substring(6,7)=='点')){
            var begin=parseInt(text.substring(0,2));
            var end=parseInt(text.substring(4,6));
        }
        deletestart=begin;
        $('#delete').show();
        $('#control_attr').html('');
        for(var i=0;i<data.length;i++){
            control_name_count=0;
            if(begin==data[i][0][0] && end==data[i][0][1]){
                timeNbumber=i;
                $('#begin').val(begin);
                $('#end').val(end);
                for(var j=0;j<data[i][1].length;j++){
                    control_name_count=j;
                    var control_name=data[i][1][j][0];
                    var control_value=data[i][1][j][1];
                    var name='control_attr_name'+j;
                    var str='<div style="clear:both">'+
                        '<div style="float:left;margin-right: 10px">'+
                        '<label class="label">控制字段</label>'+
                        '<input type="text"  class="form-control" name="'+name+'" value="'+control_name+'" style="width:200px">'+
                        '</div>'+
                        '<div style="float:left">'+
                        '<label class="label">默认值</label>'+
                        '<input type="text"  class="form-control" name="'+name+'" value="'+control_value+ '" style="width:200px">'+
                        '</div>'+
                        '<div  style="float:left;margin: 25px 100px ">'+
                        '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>'+
                        '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
                        '</div>'+
                        '</div>';
                    $('#control_attr').append(str);
                }
                currentTime.push(begin);
                currentTime.push(end);
            }
        }
    }
    //判断时间段是否重复
    function TimeIsRepeat(start,end)
    {
        alltime=[];
        getAllTime();
        var flag1=true;
        for(var i=0;i<alltime.length;i++){
            if((start<alltime[i][0] && end<=alltime[i][0]) || (start>=alltime[i][1] && end>alltime[i][1])){
                flag1=true;
            }else{
                flag1= false;
                return flag1;
            }
        }
        flag=flag1;
        return flag1;
    }
    //判断时间是否完全重合，重合即修改
    function TimeIsSime(start,end)
    {
        alltime=[];
        getAllTime();
        var flag1=false;
        for(var i=0;i<alltime.length;i++){
            if(start==alltime[i][0] && end==alltime[i][1]){
                flag1=true;
                return true;
            }else{
                flag1= false;
            }
        }
        return flag1;
    }
    //取出所有data中所有time
    function getAllTime(){
        for(var i=0;i<data.length;i++){
            alltime.push(data[i][0]);
        }
        console.log(alltime);
    }
    //清空数组
    function clean()
    {
        for(var i=0;i<=control_name_count;i++){
            var name='control_attr_name'+i;
            $("input[name="+name+"]").each(
                function() {
                    $(this).val('') ;
                }
            );
        }
        control_name_value=[];
        control=[];
        time=[];
    }
    //初始化data数组
    function Init()
    {
            data=[];
            control=[];
            time=[];
      //  console.log(data);

    }
    //更新data中的数据
    function update(start,end)
    {
        for(var i=0;i<data.length;i++){
            if(start==data[i][0][0] && end==data[i][0][1]){
                timeNbumber=i;
                for(var j=0;j<=control_name_count;j++){
                    var name='control_attr_name'+j;
                    $("input[name="+name+"]").each(
                        function() {
                            if($(this).val() !=''){
                                control_name_value.push($(this).val());
                            }else{
                                flag=false;
                            }
                        }
                    );
                    control.push(control_name_value);
                    control_name_value=[];
                }
                data[i][1]=control;
                alert('修改成功！');
                return ;
            }
        }
    }
    //删除操作
    function deleteBytime()
    {
        for(var i=0;i<data.length;i++){
            if(deletestart==data[i][0][0]){
                timeNbumber=i;
                data[i]=[];
                alert('删除成功！');
            }
        }
        var tempAlldata=[];
        time=[];
        $('#timebutton').html('');
        for(var i=0;i<data.length;i++)
        {
            if(data[i]!=null && data[i]!='' && data[i]!=[])
            {
                time=data[i][0];
                tempAlldata.push(data[i]);
                drawButton();
            }
        }
        data=tempAlldata;
    }

    //初始化序列按钮
    function drawButton()
    {
        if(time.length>0){
            var starttime=time[0];
            var endtime=time[1];
            var s=starttime+'点'+'--'+endtime+'点';
            var str='<button type="button"  class="create btn-success1" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';
            $('#timebutton').append(str);
        }
    }
</script>