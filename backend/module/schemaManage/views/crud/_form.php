<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<?php
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<style>
	.create{
		display: inline-block;
		margin-bottom: 0;
		font-weight: 400;
		text-align: center;
		vertical-align: middle;
		cursor: pointer;
		background-image: none;
		border: 1px solid transparent;
		white-space: nowrap;
		padding: 6px 12px;
		font-size: 13px;
		line-height: 1.42857143;
		border-radius: 2px;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	.popover-content:last-child {
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
	}
	.popover-content {
		padding: 9px 14px;
	}
	.popover-title {
		margin: 0;
		padding: 8px 14px;
		font-size: 13px;
		font-weight: 400;
		line-height: 18px;
		background-color: #f7f7f7;
		border-bottom: 1px solid #ebebeb;
		border-radius: 2px 2px 0 0;
	}
	.editable-container.popover {
		width: auto;
	}
	.smart-form .col-4 {
		width: 100%;
	}
	.smart-form .state-success {
		width: 10%;
		float: left;
	}
	.smart-form fieldset {
		height: auto;
		display: block;
		padding-top: 10px;
		padding-bottom: 10px;
		border-bottom: 1px dashed rgba(0,0,0,.2);
		background: rgba(255,255,255,.9);
		position: relative;
	}

</style>
<!-- widget grid -->
<section id="widget-grid" class="">
	<!-- START ROW -->
	<div class="row">
		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>
				</header>
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->
					<!-- widget content -->
					<div class="widget-body">
						<?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
								<fieldset>
									<section>
										<div class="form-group field-scheduledtask-export_title has-success">
											<label class="label">时间模式标题</label>
											<input type="text" id="name" class="form-control" name="name" value="">
										</div>
									</section>
								</fieldset>
								<!--时间设置-->
								<fieldset>
									<label class="label">时间选择方式</label>
									<label class="radio state-success"><input type="radio" id="radio_month" name="radio_date" value="week"><i></i>选择星期</label>
									<label class="radio state-success"><input type="radio" id="radio_year" name="radio_date" value="month"><i></i>选择月份</label>
								</fieldset>
								<!--时间详细设计-->
						        <fieldset style="display: none" id="fieldset_week">
							         <label class="label">周范围</label>
							         <?php for($i=1;$i<53;$i++){?>
								     <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_checkbox" value="<?=$i?>" ><i></i>第<?=$i?>周</label>
							         <?php }?>
						        </fieldset>
								<fieldset style="display: none" id="fieldset_month">
									<label class="label">月范围</label>
									<?php for($i=1;$i<13;$i++){?>
										<label class="checkbox state-success"><input type="checkbox" name="fieldset_month_checkbox" value="<?=$i?>" ><i></i><?=$i?>月</label>
									<?php }?>
								</fieldset>
								<fieldset style="display: none" id="fieldset_month_day" name="fieldset_month_day_checkbox">
									<label class="label">每月天范围</label>
									<?php for($i=1;$i<32;$i++){?>
										<label class="checkbox state-success"><input type="checkbox" name="fieldset_day_checkbox" value="<?=$i?>" ><i></i><?=$i?>号</label>
									<?php }?>
								</fieldset>
						        <fieldset style="display: none" id="fieldset_week_day" name="fieldset_week_day_checkbox">
							         <label class="label">每周范围</label>
							         <?php for($i=1;$i<8;$i++){?>
								      <label class="checkbox state-success"><input type="checkbox" name="fieldset_week_day_checkbox" value="<?=$i?>" ><i></i>周<?php if($i==7){echo '日';} else if($i==6) {echo '六';} else if($i==5) {echo '五';} else if($i==4) {echo '四';} else if($i==3) {echo '三';} else if($i==2) {echo '二';} else if($i==1) {echo '一';}?></label>
							        <?php }?>
						        </fieldset>
						<!--隐藏框设置值传递-->
						<input type="text" id="week_day" class="form-control" name="week_day" value="" style="display:none" >
						<input type="text" id="day" class="form-control" name="day" value="" style="display:none" >
						<input type="text" id="week" class="form-control" name="week" value="" style="display:none">
						<input type="text" id="month" class="form-control" name="month" value="" style="display:none">
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
						</div>
							</div>
						<?php ActiveForm::end(); ?>
					</div>
				<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		<!-- END COL -->
	</div>
</section>

<script>
    var week=[];    //星期
	var month=[];   //月份
	var day=[];     //天
	var week_day=[];//每周的工作日
	var type;
	var model;
	//统计选中的个数
	var week_count=0;
	var week_day_count=0;
	var month_count=0;
	var day_count=0;
	window.onload=function(){
		type='<?=$type?>';
		if(type=='update'){
			Init();
		}
		$("input:radio[name='radio_date']").change(function () {
			var $temp = $("input[name='radio_date']:checked").val();
		   if ($temp == 'week') {
               $('#fieldset_week').show();
			   $('#fieldset_week_day').show();
			   $('#fieldset_month').hide();
			   $('#fieldset_month_day').hide();
			  var months=$("input:checkbox[name='fieldset_month_checkbox']:checked");
		/*	   months.each(
				   function() {
					   $(this).attr('checked',false);
				   }
			   );*/
		   } else if ($temp == 'month') {
			   $('#fieldset_week').hide();
			   $('#fieldset_week_day').show();
			   $('#fieldset_month').show();
			   $('#fieldset_month_day').show();
			}
		});
		$('#_submit').click(function (event) {
		   setParam();
		})
        //选择星期几，隐藏日期
		$("input:checkbox[name='fieldset_week_day_checkbox']").click(function (){
			if(this.checked){
				week_day_count++;
				$("input:checkbox[name='fieldset_day_checkbox']:checked").each(function(){
					$(this).attr('checked',false);
				});
				$('#fieldset_month_day').hide();
			}else{
				week_day_count--;
				if(week_day_count==0){
					$('#fieldset_month_day').show();
				}
			}
		});
		//选择日期，隐藏星期几
		$("input:checkbox[name='fieldset_day_checkbox']").click(function (){
			if(this.checked){
				day_count++;
				$("input:checkbox[name='fieldset_week_day_checkbox']:checked").each(function(){
					$(this).attr('checked',false);
				});
				$('#fieldset_week_day').hide();
			}else{
				day_count--;
				if(day_count==0){
					$('#fieldset_week_day').show();
				}
			}
		});
		//选择月份，清空星期
		$("input:checkbox[name='fieldset_month_checkbox']").click(function (){
			if(this.checked){
				month_count++;
				$("input:checkbox[name='fieldset_week_checkbox']").each(
					function() {
							$(this).attr('checked',false);
					}
				);
			}else{
				month_count--;
			}
		});
		//选择星期，清空星期
		$("input:checkbox[name='fieldset_week_checkbox']").click(function (){
			if(this.checked){
				week_count++;
				$("input:checkbox[name='fieldset_month_checkbox']").each(
					function() {
						$(this).attr('checked',false);
					}
				);
			}else{
				week_count--;
			}
		});
	}
	//遍历月份，周，每月天数，每周天数
	function setParam(){
		var weeks=$("input:checkbox[name='fieldset_week_checkbox']:checked");
		var months=$("input:checkbox[name='fieldset_month_checkbox']:checked");
		var days=$("input:checkbox[name='fieldset_day_checkbox']:checked");
		var week_days=$("input:checkbox[name='fieldset_week_day_checkbox']:checked");
		weeks.each(
			function() {
				week.push($(this).val());
			}
		);
		months.each(
			function() {
				month.push($(this).val());
			}
		);
		days.each(
			function() {
				day.push($(this).val());
			}
		);
		week_days.each(
			function() {
				week_day.push($(this).val());
			}
		);
		$('#day').val(JSON.stringify(day));
		$('#month').val(JSON.stringify(month));
		$('#week').val(JSON.stringify(week));
		$('#week_day').val(JSON.stringify(week_day));
	}

	//更新时初始化函数
	function Init(){
		model=<?=json_encode($modelarr)?>;
		var namejson=eval('('+model['0']['name']+')');
		console.log(namejson);
		var name=namejson['data']['zh-cn'];
		$('#name').val(name);
		var time_mode=eval('('+model['0']['time_mode']+')');
		var m=time_mode['data']['mode']['month'];
		for(var i in m){
			month.push(parseInt(m[i]));
		}
		var w=time_mode['data']['mode']['week'];
		for(var i in w){
			week.push(parseInt(w[i]));
		}
		var d=time_mode['data']['mode']['day'];
		for(var i in d){
			day.push(parseInt(d[i]));
		}
		var wd=time_mode['data']['mode']['week_day'];
		for(var i in wd){
			week_day.push(parseInt(wd[i]));
		}
		var weeks=$("input:checkbox[name='fieldset_week_checkbox']");
		var months=$("input:checkbox[name='fieldset_month_checkbox']");
		var days=$("input:checkbox[name='fieldset_day_checkbox']");
		var week_days=$("input:checkbox[name='fieldset_week_day_checkbox']");
		weeks.each(
			function() {
					if(isInArray(week,$(this).val())==1){
						week_count++;
						$(this).attr('checked','checked');
					}
				}
		);
		months.each(
			function() {
				if(isInArray(month,$(this).val())==1){
					$(this).attr('checked','checked');
					month_count++;
				}
			}
		);
		days.each(
			function() {
				if(isInArray(day,$(this).val())==1){
					$(this).attr('checked','checked');
					day_count++;
				}
			}
		);
		week_days.each(
			function() {
				if(isInArray(week_day,$(this).val())==1){
					$(this).attr('checked','checked');
					week_day_count++;
				}
			}
		);
		 week=[];    //星期
		 month=[];   //月份
		 day=[];     //天
		 week_day=[];//每周的工作日
	}

	//是否存在数组中
	function isInArray(arr,n){
		for(var i=0;i<arr.length;i++){
			if(arr[i]==n){
				return 1;
			}
		}
		return 0;
	}
</script>