<?php

namespace backend\module\device;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\device\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
