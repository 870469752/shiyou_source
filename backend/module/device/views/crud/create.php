<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Device $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Device',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
