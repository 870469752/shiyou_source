<?php
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
TableAsset::register ( $this );
?>

<section>
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?=$title?> 点位管理</h2>
                </header>
                <div>
                    <div   style="float: left;width:100%;">
                        <div class="well well-sm well-light">
                            <div id="table">
                                <div id="table_content">
                                    <table id="device" class="table table-striped table-bordered table-hover" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll' name="checkAll"></th>
                                            <th data-class="expand">点位ID</th>
                                            <th data-class="expand">中文名</th>
                                            <th data-class="expand">操作</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </div>
</section>
<div id="point_select" title="点位选择" style="display:none;width: auto;">
    <div class="col-md-12">
        <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
            <div class="panel-body" ng-transclude style="overflow-x: auto; overflow-y: auto; " >
                <div style="width:40%;float: left;height: 600px;background: white;overflow:scroll">
                    <div>
                        <ul id="treeDemo1" class="ztree"></ul>
                    </div>
                </div>
                <div   style="float: left;width:60%;height: 600px;overflow:scroll;">
                    <div class="well well-sm well-light">
                        <div id="collect">
                            <div id="collect_content">
                                <table id="collect_table" class="table table-striped table-bordered table-hover" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_collect' name="checkAll_collect"></th>
                                        <th data-class="expand">ID</th>
                                        <th data-class="expand">中文名</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </panel>
    </div>
</div>
<?php
$this->registerJsFile("js/zTree/jquery.ztree.core.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/zTree/metroStyle/metroStyle.css", ['backend\assets\AppAsset']);
?>
<script>
    var table_content=document.getElementById("table").innerHTML;
    var collect_content=document.getElementById("collect").innerHTML;
    var device;//当前点击的device
    var collector;//当前点击的collector
    var  selectData=new Array();  //device 已有点位复选按钮
    var  selectDate_collect=new Array();  //collect 已选择点位的复选按钮
    var JsonData;//存放最终要处理的点位，包括新增和删除的。
    //去掉数组中选定的值
    function array_remove(array,type,element){
        var result=[];
        for(var key in array){
            if(type=="key"){
                if(key!=element)
                    result[key]=array[key];
            }
            if(type="value"){
                if(array[key]!=element)
                    result[key]=array[key];
            }
        }
        return result;
    }

    //第三位参数为类型  判断是 key还是value
    function in_array(array,type,element){
        for(var i in array){
            if(type=='value') {
                if (array[i] == element)
                    return 1;
            }
            if(type=='key') {
                if (i == element)
                    return 1;
            }
        }
        return 0;
    }
    //checkbox操作
    window.select_=function(data){
        var checked=$(data).prop("checked");

        if(checked==true){
            //添加
            selectData.push(parseInt($(data).val()));
            console.log(selectData);
        }
        if(checked==false){
            //去掉
            selectData=array_remove(selectData,'value',$(data).val());

            console.log(selectData);
        }
    };
    window.select1_=function(data){
        var checked=$(data).prop("checked");

        if(checked==true){
            //添加
            selectDate_collect.push(parseInt($(data).val()));
            console.log(selectDate_collect);
        }
        if(checked==false){
            //去掉
            selectDate_collect=array_remove(selectDate_collect,'value',$(data).val());

            console.log(selectDate_collect);
        }
    };
    window.onload = function() {
        selectDate_collect=<?=$points?>;
        console.log(selectDate_collect);
        var ddd_collect=<?=$treeCollectData?>;
        var data_collect=[];
        for(var a in ddd_collect){
            data_collect.push(ddd_collect[a]);
        }
        var nodes = [
            { "id":1, "name":"test1",
                children: [
                    { "id":3, "name":"test3"},
                    { "id":4, "name":"test4"},
                    { "id":5, "name":"test5"}
                ]
            },
            { "id":2, "name":"test2"  }
        ];

        var setting_collect={
            callback: {
                onClick: zTreeOnClick1
            }
        };
        $.fn.zTree.init($("#treeDemo1"), setting_collect, data_collect);


        init(<?=$model->id?>);
        /**
         * 初始化设备表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,tool,device_id) {
            console.log("设备id: "+device_id);
            table = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        //"csv",
                        "xls",
                        {
                            "sExtends": "pdf",
                            "name": "测试",
                            "sTitle": "点位管理",
                            "sPdfMessage": "pdf_test",
                            "sPdfSize": "letter"
                        },
                        {
                            "sExtends": "print",
                            "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                        }
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,10,20, 30, -1], [5,10, 20, 30, "All"]],
                "iDisplayLength": 10,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "ajax": {
                    "url": "/point_device_rel/crud/get-points-dev?id="+device_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {
                        "mDataProp": "point_id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if(in_array(selectData,"value",sData)==1)
                                $(nTd).html("<input type='checkbox' name='checkList' onclick='javascript:select_(this)' checked='checked' value='" + sData + "' >");
                            else
                                $(nTd).html("<input type='checkbox' name='checkList' onclick='javascript:select_(this)' value='" + sData + "' >");
                        }
                    },
                    {"mDataProp": "point_id"},
                    {"mDataProp": "cn"},
                    {
                        "mDataProp": "point_id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href=/point/crud/point-update?id="+sData+"><i class='fa fa-pencil'/></a>"+"   <a href=/device/crud/point-delete?point_id="+sData+"&device_id="+device+"><i class='glyphicon glyphicon-trash'/></a>");
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {
                    $('<a  class="btn btn-success" id="aaa1" style="margin-right:20px">添加点位</a>' + '&nbsp;').appendTo($('.'+tool));//myBtnBox
                    $('<a  class="btn btn-success" id="save1" style="margin-right:20px">保存</a>' + '&nbsp;').appendTo($('.'+tool));
                    $('<a  class="btn btn-danger"  id="deleteAll1">批量删除</a>' + '&nbsp;').appendTo($('.'+tool));
                    $("#aaa1").click(function(){
                        addPoint();
                    })
                    $("#save1").click(function(){
                        save();
                    })
                    $("#deleteAll1").click(function(){
                        deleteAll();
                    })
                },
                "order": [1, 'asc']
            });
            /* END TABLETOOLS */
            return table;
        };
        //初始化collect
        function initTable1(name,tool,controller_id) {
            table1 = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [

                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[5,10,100], [5,10,100]],
                "iDisplayLength": 10,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                "ajax": {
                    "url": "/point/crud/get-bacnet-point-data?controller_id="+controller_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            if(in_array(selectDate_collect,"value",sData)==1)
                                $(nTd).html("<input type='checkbox' name='checkList_collect' onclick='javascript:select1_(this)' checked='checked' value='" + sData + "' >");
                            else
                                $(nTd).html("<input type='checkbox' name='checkList_collect' onclick='javascript:select1_(this)' value='" + sData + "' >");
                        }
                    },
                    {"mDataProp": "id"},
                    {"mDataProp": "cn"}
                ],
                "order": [1, 'asc']
            });
            /* END TABLETOOLS */
            return table1;
        }
        //点击设备节点
        function init(treeId) {
            //清空复选框数据
            selectData=[];
//            selectDate_collect=[];
            console.log(selectData);
            console.log(selectDate_collect);
//        只有叶子节点才有刷新表单
                $("#table_content").remove();
                $("#table").append(table_content);
                initTable('device', 'tool_others', treeId);
                device=treeId;
            $("#checkAll").click(function () {
                var checkAll=$("#checkAll").prop("checked");
                $("#device").find($("input[name='checkList']")).each(function () {
                    var checkValue=parseInt($(this).val());
                    //全选
                    if(checkAll==true) {
                        $(this).prop("checked", true);
                        if(in_array(selectData,'value',checkValue)==0){
                            selectData.push(checkValue);
                        }
                    }
                    //全取消
                    if(checkAll==false) {
                        $(this).prop("checked", false);
                        //如果取消的值在数组中则去掉
                        if (in_array(selectData, 'value', checkValue) == 1) {
                            selectData = array_remove(selectData, 'value', checkValue);
                        }
                    }
                });
                console.log(selectData);
            });
        };
        //点击collect节点
        function zTreeOnClick1(event, treeId, treeNode) {
     //       selectDate_collect=[];
            selectDate_collect=<?=$points?>;
            if(treeNode['type']=='collect') {
                $("#collect_content").remove();
                $("#collect").append(collect_content);
                initTable1('collect_table', 'tool_others', treeNode['id']);
                collector=treeNode['id'];
            }
            //点击collect checkbox时  用selectData记录选取的值
            $("#checkAll_collect").click(function () {
                var checkAll=$("#checkAll_collect").prop("checked");
                $("#collect_table").find($("input[name='checkList_collect']")).each(function () {
                    var checkValue=parseInt($(this).val());
                    //全选
                    if(checkAll==true) {
                        $(this).prop("checked", true);
                        if(in_array(selectDate_collect,'value',checkValue)==0){
                            selectDate_collect.push(checkValue);
                        }
                    }
                    //全取消
                    if(checkAll==false) {
                        $(this).prop("checked", false);
                        //如果取消的值在数组中则去掉
                        if (in_array(selectDate_collect, 'value', checkValue) == 1) {
                            selectDate_collect = array_remove(selectData, 'value', checkValue);
                        }
                    }
                });
                console.log(selectDate_collect);
            });
        };
        //点击添加点位按钮
        function addPoint()
        {
            add_point.dialog("open");
        }
        //保存按钮
        function save()
        {

            var url="/device/crud/create-points?device_id="+device+"&JsonData="+JsonData;
            location.href = url;
        }
        //批量删除
        function deleteAll()
        {
            if (confirm("您是否确认删除？")) {
                var url="/device/crud/delete-all?device_id="+device+"&JsonData="+JSON.stringify(selectData);
                location.href = url;
            }
            else {

            }

        }
        //点击device checkbox时  用selectData记录选取的值 checkAll_collect
        $("#checkAll").click(function () {
            var checkAll=$("#checkAll").prop("checked");
            $("#device").find($("input[name='checkList']")).each(function () {
                var checkValue=parseInt($(this).val());
                //全选
                if(checkAll==true) {
                    $(this).prop("checked", true);
                    if(in_array(selectData,'value',checkValue)==0){
                        selectData.push(checkValue);
                    }
                }
                //全取消
                if(checkAll==false) {
                    $(this).prop("checked", false);
                    //如果取消的值在数组中则去掉
                    if (in_array(selectData, 'value', checkValue) == 1) {
                        selectData = array_remove(selectData, 'value', checkValue);
                    }
                }
            });
        });
        //点击collect checkbox时  用selectData记录选取的值
        $("#checkAll_collect").click(function () {
            var checkAll=$("#checkAll_collect").prop("checked");
            $("#collect_table").find($("input[name='checkList_collect']")).each(function () {
                var checkValue=parseInt($(this).val());
                //全选
                if(checkAll==true) {
                    $(this).prop("checked", true);
                    if(in_array(selectDate_collect,'value',checkValue)==0){
                        selectDate_collect.push(checkValue);
                    }
                }
                //全取消
                if(checkAll==false) {
                    $(this).prop("checked", false);
                    //如果取消的值在数组中则去掉
                    if (in_array(selectData, 'value', checkValue) == 1) {
                        selectDate_collect = array_remove(selectDate_collect, 'value', checkValue);
                    }
                }
            });
            console.log(selectDate_collect);
        });
        //点击添加点位出现div
        var add_point = $("#point_select").dialog({
            autoOpen : false,
            width : 1000,
            resizable : true,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-plus'></i>&nbsp; 添加",
                "class" : "btn btn-primary",
                click : function() {
                    JsonData=JSON.stringify(selectDate_collect);
                    console.log(JsonData);
                    $(this).dialog("close");
                }
            }]
        });
        /*******************************************    全选 取消 * 按钮****************************************************************/
        function check_all(name){
            $("input[name='checkList_"+name+"']").each(function(){
                $(this).prop('checked',true);
            });
        }
        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;
            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }

    }
</script>
