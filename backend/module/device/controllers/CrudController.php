<?php

namespace backend\module\device\controllers;


use backend\models\Sub_System_Category;
use backend\models\PointEvent;
use backend\models\SubSystemElement;
use backend\models\SubSystem;
use Yii;
use backend\models\Device;
use backend\models\Collector;
use backend\models\BacnetController;
use backend\models\PointDeviceRel;
use backend\models\search\DeviceSearch;
use backend\controllers\MyController;
use backend\models\ModbusController;
use common\library\MyFunc;
use yii\web\NotFoundHttpException;
use yii\helpers\BaseArrayHelper;
use common\library\Ssp;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for Device model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new DeviceSearch;
//        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
//
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'searchModel' => $searchModel,
//        ]);
        return $this->render('index_datatable');
    }

    public function actionGetIndex()
    {
        //db用于select后面的字段，dt用于返回数组的键值，dj查询条件字段（如果不想该字段用于查询，则不加dj属性即可)
        $table = 'core.device';
        $con='1=1';
        $primaryKey = 'id';
        $columns = array(
            array( 'db' => "name->'data'->>'zh-cn'as cn", 'dt' => 'cn', 'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "id", 'dt' => 'id', 'dj'=>"id")
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        echo json_encode($result);
    }

    /**
     * Displays a single Device model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->name = MyFunc::DisposeJSON($model->name);
        $model->name = empty($model->name) ? '[No Name]' : $model->name;
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Device model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Device();
        if ($data = Yii::$app->request->post()) {
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$data['Device']['name'],
                    'en-us'=>''
                )
            );
            $gojsdata=array(
                'data_type'=>'gojs_data',
                'data'=>array(

                )
            );
            $model->name=json_encode($name);
            $model->type=1;
            $model->gojs_data=json_encode($gojsdata);
            $model->save();
            $result=$model->errors;
            if(!empty($result)) {
                echo '<pre>';
                print_r($result);
                die;
            }
            return $this->redirect(['index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /*
     * 给设备选点位页面
     */
    public function actionUpdatePoint($id,$type=0)
    {
        $device=Device::find()->where(['id'=>$id])->one();
        $pointDeviceRel=PointDeviceRel::find()->where(['device_id'=>$id])->asArray()->all();
        $points=array();
        foreach($pointDeviceRel as $k=>$v) {
            $points[]=$v['point_id'];
        }
        $treeCollectData=self::getCollectData();
        $title=json_decode($device->name,1);
        return $this->render('device_point', [
            'model' => $device,
            'treeCollectData'=>$treeCollectData,
            'title'=>$title['data']['zh-cn'],
            'points'=>json_encode($points)
        ]);
    }

    /*
     * 给设备选点
     */
    public function actionCreatePoints($device_id=null,$JsonData=null)
    {
        $data=json_decode($JsonData,1);
        $pointDeviceRel=PointDeviceRel::find()->where(['device_id'=>$device_id])->asArray()->all();
        $points=array();
        foreach($pointDeviceRel as $k=>$v) {
            $points[]=$v['point_id'];
        }
        if(!empty($data)){
            foreach($data as $k=>$v){
                $model = new PointDeviceRel;
                $model->device_id=$device_id;
                $model->point_id=$v;
                if(in_array($v,$points)){
                }else{
                    $model->save();
                }
            }
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['index']);
        }
    }
    /**
     * Updates an existing Device model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($data = Yii::$app->request->post()) {
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$data['Device']['name'],
                    'en-us'=>''
                )
            );
            $model->name=json_encode($name);
            $model->save();
            $result=$model->errors;
            if(!empty($result)) {
                echo '<pre>';
                print_r($result);
                die;
            }
            return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);
            $model->name = empty($model->name) ? '[No Name]' : $model->name;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Device model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete1($id)
    {
        PointDeviceRel::deleteAll(['device_id'=>$id]);
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Device model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Device the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Device::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //查询collect
    public static function getCollectData()
    {
        $model=new Collector();
        $collector=Collector::find()->select(['eid','name'])->asArray()->all();
        $collector=MyFunc::map($collector,'eid','name');
        $controller_bacnet=BacnetController::find()->select(['id','name','src_eid','object_instance_number'])->asArray()->all();
        $controller_modbus=ModbusController::find()->select(['id','name','src_eid'])->asArray()->all();
        $controller_opc=$model->findBySql('select id,name,src_eid from protocol.opc_controller')->asArray()->all();

        $treeData=[];
        $controller_type=array(
            '0'=>array(
                "id"=>0,
                "name"=>'bacnet',
                "children"=>array()
            ),
            '1'=>array(
                "id"=>1,
                "name"=>'modbus',
                "children"=>array()
            ),
            '2'=>array(
                "id"=>2,
                "name"=>'opc',
                "children"=>array()
            ),
        );
        foreach ($collector as $key=>$value) {
            $treeData[$key]['name']=$value;
            $treeData[$key]['children']=$controller_type;
            $treeData[$key]['type']='all';
        }

        foreach ($controller_bacnet as $key=>$value) {
            $value['name']=$value['object_instance_number'].'('.MyFunc::DisposeJSON($value['name']).')';
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['0']['children'][]=$value;
        }
        foreach ($controller_modbus as $key=>$value) {
            $value['name']=MyFunc::DisposeJSON($value['name']);
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['1']['children'][]=$value;
        }
        foreach ($controller_opc as $key=>$value) {
            $value['name']=MyFunc::DisposeJSON($value['name']);
            $value['type']='collect';
            $treeData[$value['src_eid']]['name']=isset($treeData[$value['src_eid']])?$treeData[$value['src_eid']]['name']:'无';
            $treeData[$value['src_eid']]['children']['2']['children'][]=$value;
        }

        return json_encode($treeData);
    }

    //删除device里面的某个点位
    public function actionPointDelete($point_id,$device_id)
    {
        $model=PointDeviceRel::find()->where(['device_id'=>$device_id,'point_id'=>$point_id])->one();
        $model->delete();
        $result=$model->errors;
        if(!empty($result)) {
            echo '<pre>';
            print_r($result);
            die;
        }
        return $this->redirect(['index']);
    }
    //批量删除某个device里面的多个点位
    public function actionDeleteAll($device_id=null,$JsonData=null)
    {
        $data=json_decode($JsonData,1);
        if(!empty($data)){
            foreach($data as $k=>$v){
                $model = PointDeviceRel::find()->where(['device_id'=>$device_id,'point_id'=>$v])->one();
                $model->delete();
                $result=$model->errors;
                if(!empty($result)) {
                    echo '<pre>';
                    print_r($result);
                    die;
                }
            }
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['index']);
        }
    }

    //初始化设备表
    //初始化sub_system表的points字段（直接运行此函数）
    public function actionInit(){
        //查找已有的设备
        $devices=Device::find()->asArray()->all();
        $devices_arr=array();
        foreach($devices as $k=>$v) {
            $devices_arr[]=$v['id'];
        }
        $subsystems=SubSystem::find()->asArray()->all();
        foreach ($subsystems as $key=>$value) {
            if(in_array($value['id'],$devices_arr)){

            }else{
                $sub_system_data=self::getSubSystemPoint([$value]);
                $event_points=self::GetEventPoint($sub_system_data['events']);
                $points=array_merge($sub_system_data['points'],$event_points);
                $points=array_unique($points);
                $model_d=new Device();
                $t=json_decode($value['name'],1);
                $name=array(
                    'data_type'=>'description',
                    'data'=>array(
                        'zh-cn'=>$t['data']['zh-cn'],
                        'en-us'=>''
                    )
                );
                $gojsdata=array(
                    'data_type'=>'gojs_data',
                    'data'=>array(

                    )
                );
                $model_d->id=$value['id'];
                $model_d->name=json_encode($name);
                $model_d->type=1;
                $model_d->gojs_data=json_encode($gojsdata);
                $model_d->save();
                $result=$model_d->errors;
                if(!empty($result)) {
                    echo '<pre>';
                    print_r($result);
                    die;
                }
                foreach($points as $k=>$v){
                    $model_p=new PointDeviceRel();
                    $model_p->device_id=$value['id'];
                    $model_p->point_id=$v;
                    $model_p->type=1;
                    $model_p->save();
                    $resultt=$model_p->errors;
                    if(!empty($resultt)) {
                        echo '<pre>';
                        print_r($resultt);
                        die;
                    }
                }
            }

        }
        return $this->redirect(['index']);
    }
    //$sub_system 为子系统数据组成的数组
    public function getSubSystemPoint($sub_system){
        //得到子系统内所有绑定的点位 组成数组
        $points=[];$sub_system_data=[];
        $camera_points=[];
        $keys=[];
        $point_keys=[];
        $camera_key=[];
        $events=[];
        $events_keys=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];
            foreach ($data as $data_key=>$data_value) {
                //group 不存在category字段
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'state':
                    case 'state_event':
                    case 'state_defend':
                    case 'state_defend_1':
                    case 'state_defend_ctrl':
                    case 'gif':
                    case 'gif_value':
                    case 'control':
                    case 'state_value':
                    case 'value_1':
                        //得到 需要查询节点的key
                        if( $data_value['key']!=null) {
                            $keys[] = $data_value['key'];
                            $sub_system_data[$system['id']][$data_value['key']] = null;
                        }
//                    if($data_value['img']=="/uploads/pic/摄像头1.jpg")
                        //判断是不是摄像头的点
                        if(isset($data_value['img']))
                            if(
                                $data_value['img']=="/uploads/pic/摄像头1.jpg" || $data_value['img']=="/uploads/pic/摄像头2.jpg" ||
                                $data_value['img']=="/uploads/pic/摄像头3.jpg" || $data_value['img']=="/uploads/pic/摄像头4.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头5.jpg"||$data_value['img']=="/uploads/pic/摄像头6.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头7.jpg"||$data_value['img']=="/uploads/pic/摄像头8.jpg"
                            ) {
                                $camera_key[] =$data_value['key'];
                            }
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //根据子系统id 和key 找到需要查询的点位集合
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();
            $type='points';

            foreach ($configs as $key=>$configs_value) {
                $data=json_decode($configs_value['config'],1)['data'];
                if(isset($data['points'])){
                    $type='points';
                    $points_info=$data['points'];
                }
                if(isset($data['events'])) {
                    $type='events';
                    $points_info = $data['events'];

                }
                if(isset($data['point_event'])) {
                    $type='point_event';
                    $points_info = $data['point_event'];

                }
                if($points_info!="") {
                    if (!is_array($points_info)) {
                        $points[] = $points_info;
                        $point_keys[]=$configs_value['element_id'];
                        $sub_system_data[$system['id']][$configs_value['element_id']] = ['id' => $points_info];
                        if(!empty($camera_key)){
                            $camera_points[]= $points_info;
                        }
                    } else {
                        foreach ($points_info as $point_id => $point_name) {
                            if($type=='events' || $type=='point_event')  {
                                if($point_id!='name'){
                                    $events[]=$point_id;
                                    $events_keys[]=$configs_value['element_id'];
                                }
                            }
                            if($type=='points')  {$points[] = $point_id; $point_keys[]=$configs_value['element_id'];}
                            $sub_system_data[$system['id']][$configs_value['element_id']][] = ['id' => $point_id, 'name' => $point_name];
                        }
                    }
                }

            }
        }
        if(!empty($sub_system_data )){
            foreach($sub_system_data as $sub_system_id=>$sub_system){
                foreach($sub_system as $key=>$value){
                    if(empty($value))
                        unset($sub_system_data[$sub_system_id][$key]);
                }
            }
        }
        return [
            'data'=>$sub_system_data,
            'points'=>array_unique($points),'points_key'=>array_unique($point_keys),
            'camera_points'=>array_unique($camera_points),
            'events'=>array_unique($events),'events_key'=>array_unique($events_keys)
        ];
    }
    //得到event对应的点位信息
    public function GetEventPoint($events){
        $points=PointEvent::find()
            ->select('point_id')
            ->where(['id'=>$events])
            ->asArray()->all();
        $result=[];
        if(!empty($points)){
            foreach ($points as $key=>$value) {
                $result[]=$value['point_id'];
            }
            $result=array_unique($result);
        }
        else $result=[];

        return $result;
    }
}
