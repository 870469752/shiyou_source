<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\TableAsset;

use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\UnitSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'Units');
$this->params['breadcrumbs'][] = $this->title;

?>


<section>
    <!-- row -->
    <div class="row">


        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-greenDark" id="wid-id-2" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>点位联结</h2>

                </header>
                <div>
                    <?= GridView::widget([
                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'options' => ['class' => 'widget-body no-padding'],
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover',
                            'width' => '100%',
                            'id' => 'datatable'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

                            ['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'main_point', 'headerOptions' => ['data-hide' => 'phone']],
                            ['attribute' => 'link_point', 'headerOptions' => ['data-hide' => 'phone']],

                            ['class' => 'yii\grid\ActionColumn','template' => '{delete}','headerOptions' => ['data-class' => 'expand']],
                            //['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
                        ],
                    ]); ?>

                    <?php
                    // 搜索和创建按钮
                    $create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

                    // 添加两个按钮和初始化表格
                    $js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$create_button}');
table_config('datatable', options);
JAVASCRIPT;
                    $this->registerJs ( $js_content, View::POS_READY);
                    ?>

                </div>
            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->
    </div>
    <!-- end row -->
</section>
