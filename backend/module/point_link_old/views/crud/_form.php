<?php
use yii\helpers\Html;
use common\library\MyHtml;
use yii\helpers\BaseHtml;
use common\library\MyFunc;
use common\library\MyActiveForm;
use yii\bootstrap;
use yii\bootstrap\ActiveForm;
//echo '<pre>';
//print_r($all_sub_system);
//die;

?>



<!-- html 点位搜索弹出框 start-->
<section id="point_search" class=""style="width: 100%;position:relative;display: ">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>


    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?=Yii::t('app', 'Search Point')?></h2>
            <div class="jarviswidget-ctrls">

            </div>
        </header>

        <!-- 点位搜索框 start      -->
        <div id = '_loop' style = 'border:2px solid #D5D5D5;padding-bottom:15px'>
            <div style = 'padding:0px 7px 15px 7px'>
                <?php $form = MyActiveForm::begin(['method' => 'get', 'id' => '_ws']) ?>
                <fieldset>

                    <div class="form-group">
                        <div class="col-md-2" style="margin-left:20px;">
                            <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Point Name'), 'class' => 'form-control'])?>
                        </div>
                        <!--                        <div class="col-md-1"></div>-->
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('energy_category_id', 0, $category_tree
                                , ['placeholder' => Yii::t('app', 'Choose Category'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
                        <div class="col-md-3">
                            <?=MyHtml::dropDownList('location_category_id', 0, $location_tree
                                , ['placeholder' => Yii::t('app', 'Choose Location'), 'class' => 'select2', 'tree' => true,  'id' => 'point'])?>
                        </div>
                        <!--                        <div class="col-md-1"></div>-->

                        <div class="col-md-2" style="display:none;">
                            <?=Html::dropDownList('protocol_id', isset($protocol_id)?$protocol_id:null, ['' => '',
                                ''=>'所有点位',
                                1 => Yii::t('app', 'BACnet Point'),
                                4 => Yii::t('app', 'Calculate Point'),
                                7 => Yii::t('app', 'Simulation Point'),
                                //                                                              'custom' => Yii::t('app', 'Custom')
                            ],
                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Point Type'), 'id' => 'point_location_demo'])?>
                        </div>

                    </div>
                    <!--提交-->
                    <div class="form-actions" style = 'float:right;padding:0px;margin:-19px 130px -20px 39px;'>
                        <a id="point_search_submit" class="btn btn-success plus-left" href="javascript:void(0)" data-toggle="modal" rel="tooltip" data-original-title="搜索" data-placement="bottom"/>
                        <?=Yii::t('app', 'Search Point')?>
                        </a>
                    </div>
                </fieldset>
            </div>

            <?php MyActiveForm::end(); ?>
        </div>


        <div id= 'point_search_div' class="widget-body"  ">


        <?= Html::hiddenInput('point_ids', '', ['id' => 'point_selected'])?>

        <fieldset style="display:none;">

            <!-- widget div-->
            <div class = 'form-group'>
                <label class="col-sm-1 control-label"><?=Yii::t('app', 'Bacth Filter')?></label>
                <div class = 'col-sm-6' >
                    <?=Html::dropDownList('point_ids', '', ['' => '',
                    ],
                        ['id' => 'point_select', 'multiple' => 'multiple', 'placeholder' => Yii::t('app', 'Point Type')])?>
                </div>
            </div>

            <!--    批量修改 点位的一些属性      -->
            <div class = 'form-group' style = 'margin-top:230px'>

            </div>


            <div style="display: inline-block;">
            </div>

        </fieldset>



        <div class="widget-body no-padding">

            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th data-hide="phone">点位ID</th>
                    <th data-hide="phone">设备D</th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                    <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                    <th>协议名称</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                    <th data-hide="phone,tablet">值类型</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 加入1</th>
                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 加入2</th>
                </tr>
                </thead>
                <tbody id="tbodyData">

                </tbody>
            </table>

        </div>








        <!-- <?php //ActiveForm::end(); ?> -->
        <!--            --><?php //ActiveForm::end(); ?>
        <!--            <button id="checkAllBtn" class="button">CheckAll</button>-->
    </div>

    <!-- 点位搜索框 end      -->



    <!-- 参数主体 start-->

    </div>
    <!-- 图表内容 end-->
</section>

<div style="width: 100%;">
    <div style="width: 48%;float:left;">
        <!-- 点位 搜索展示框 start-->
        <!--         Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-deletebutton="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2><?=Yii::t('point', '点位')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <table id="selected_datatable_tabletools1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>

                        </tr>
                        </thead>
                        <tbody id="tbodyData_one1">

                        </tbody>
                    </table>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>

        <!-- end widget -->
        <!-- 点位 搜索展示框 end-->

    </div>
    <div style="width: 48%;float:right;">
        <!-- 点位 搜索展示框 start-->
        <!--         Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">

            <header>
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2><?=Yii::t('point', '点位')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <table id="selected_datatable_tabletools2" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone">点位ID</th>
                            <th data-hide="phone">设备D</th>
                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                            <!-- <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Phone</th> -->
                            <th>协议名称</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs"></i> 单位</th>
                            <th data-hide="phone,tablet">值类型</th>
                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i> 操作</th>

                        </tr>
                        </thead>
                        <tbody id="tbodyData_one2">

                        </tbody>
                    </table>

                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>

        <!-- end widget -->
        <!-- 点位 搜索展示框 end-->

    </div>

</div>


<div id="_loop1" style="width: 100%;" class="form-actions">
    <?= Html::submitButton(Yii::t('app', '查看全部') , ['class' => 'btn btn-primary', 'id' => 'view']) ?>
    <?= Html::submitButton(Yii::t('app', '确定联结') , ['class' => 'btn btn-primary', 'id' => 'sub']) ?>
</div>





<div id="layer_name" title="联结名称" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('联结名称',null,  ['class' => 'control-label'])?>
        <input id="point_link_name" class="form-control" name="value" value="">
    </div>
</div>
<>
<?php


$this->registerCssFile("css/multi-select/multi-select.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/multi-select/jquery.multi-select.js", ['yii\web\JqueryAsset']);


$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<script>
    window.GlobalData=new Array();
    window.selectedPointContainer = new Array();
    window.selectedPointContainer1 = new Array();



    //修改点位名称后更改selectedPointContainer 内的值
    function update_selected(id,value){
        for(var key in selectedPointContainer){
            if(selectedPointContainer[key]['pointId']== id)
                selectedPointContainer[key]['pointName']= value;
        }
    }
    window.onload = function () {





        /*****************************************POINT SEARCH**************************************************************/



        var temp = new Array();

        var $__type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;


        var $current_keys = [];
        var $count = 0;

        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet : 1024,
            phone : 480
        };

        /* TABLETOOLS */
        var datable = $('#datatable_tabletools').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
//            "bLengthChange":true,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });

        var datable1 = $('#selected_datatable_tabletools1').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools1'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });

        var datable2 = $('#selected_datatable_tabletools2').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
            },
            "autoWidth" : true,
//            "bPaginate":false,
//            "bLengthChange":true,
//            "sPaginationType":"full_numbers",
//            "iDisplay":10,
            "iDisplayLength":5,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_tabletools) {
                    responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#selected_datatable_tabletools2'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_tabletools.respond();
            }
        });







        $('#selected_datatable_tabletools3').dataTable({
            "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "bSort" : false,
//            "sScrollX": "100%",
//            "bScrollCollapse": true,
            "paging":   false
        } );



        $('#point_select').multiSelect({
            selectableHeader: "<div class='custom-header'>搜索结果</div>",
            selectionHeader: "<div class='custom-header'>已选点位</div>",
            keepOrder: true
        });
        $('#point_select').multiSelect('deselect_all');

        $("#checkAllBtn").click(function() {
            console.log(111);
            $("#point_select").multiSelect("select_all");
        });
        //ajax提交
        $('#all_select').click(function(){
            console.log(111);
            $('#point_select').multiSelect('select_all');
        });




        //        timeTypeChange('year', $('#_time'));
        function regenerateTable(data, id, exeparam,flag)
        {
            //动态添加元素
            // $('#point_select').multiSelect('addOption', data);
            var trstr = '<tbody id="'+id+'">';
            for (var i in data) {
                switch (data[i]['value_type']) {
                    case 0:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect1" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" value="'+flag+'" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }


                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></button></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>默认</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>默认</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                    case 1:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>实时量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>实时量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 2:
                        if (data[i]['unit'] == null) {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {
                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this,1);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>累积量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>累积量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }
                        break;
                    case 3:

                        if (data[i]['unit'] == null) {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }

                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        } else {

                            switch (exeparam){
                                case 'exeselect':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'" ><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exeselect1(this);" class="exeselect" id="'+data[i]['pointId']+'" name="'+data[i]['pointName']+'" ><span class="glyphicon glyphicon-ok"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                                case 'exedel':
                                    trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+'-'+'</td><td>开关量</td>' +
                                        '<td><label class=""><button type="button" onclick="javascript:exedel(this);" class="exeselect" id="'+data[i]['pointId']+'"  name="'+data[i]['pointName']+'"><span class="glyphicon glyphicon-remove"></span></button></label></td>' +
                                        '</tr>';
                                    break;
                            }
                            // trstr += '<tr><td>'+data[i]['pointId']+'</td><td>'+data[i]['device_id']+'</td><td>'+data[i]['pointName']+'</td><td>'+data[i]['protocolName']+'</td><td>'+data[i]['unit']+'</td><td>开关量量</td><td><input type="button" onclick="javascript:exeselect(this);" class="exeselect" id="'+data[i]['pointId']+'" value="选择" /></td></tr>';
                        }

                        break;
                }

            }

            trstr += '</table>';

            switch (exeparam){
                case 'exeselect':
                    //clear the table tbody content and set the new data
                    datable.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    break;
                case 'exedel':
                    //clear the table tbody content and set the new data
                    datable2.fnClearTable();
                    id = '#'+id;
                    $(id).replaceWith(trstr);

                    //recall the method dataTable() to regenerate the table
                    $('#selected_datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    $('#selected_datatable_tabletools1').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>","pageLength":5});
                    break;
            }

        }

        $('#point_search_submit').click(function() {

            //clear the table tbody content and set the new data
            datable.fnClearTable();


            //recall the method dataTable() to regenerate the table
            $('#datatable_tabletools').dataTable({"bDestroy":true,"sDom": "t"+"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>"});



            var energy_id = $('select[name=energy_category_id] option:checked').val();
            var location_id = $('select[name=location_category_id] option:checked').val();
            if (energy_id == '') {
                alert('请选择能源分类');
                return false;
            }

            if (location_id == '') {
                alert('请选择地理位置分类');
                return false;
            }


            var $url = '/point-batch/crud/ajax-find-point-specific?' + $('#_ws').serialize();
            console.log($url);
            var get_points=$.ajax({
//                type: "GET",
                url: $url,
                timeout:15000,
                data:  {},
                success: function (msg) {
                    var data=eval(msg);
                    GlobalData =data;
                    console.log(data);
                    if(data) {
                        //先从server返回的json里面删除已选择的点位
                        //动态添加表格数据
                        regenerateTable(data, 'tbodyData', 'exeselect');

                    }
                },



                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status=='timeout'){//超时,status还有success,error等值的情况

                        get_points.abort(); //取消请求
                        alert("搜索点位太多，请填写更详细信息");

                    }
                }
            });

//            $.get($url, function(data) {
//                GlobalData = data;
//
//                if(data) {
//                    //先从server返回的json里面删除已选择的点位
//                    //动态添加表格数据
//                    regenerateTable(data, 'tbodyData', 'exeselect');
//
//                }
//
//
//            }, 'json');
        });



        window.exedel = function(data){
            if(data.value==0) {
                for (var i in selectedPointContainer) {
                    if (selectedPointContainer[i]['pointId'] == data.id) {
                        GlobalData.push(selectedPointContainer[i]);
                        selectedPointContainer.splice(i, 1);
                    }
                }
                regenerateTable(selectedPointContainer, 'tbodyData_one1', 'exedel',0);
            }
            if(data.value==1) {
                for (var i in selectedPointContainer1) {
                    if (selectedPointContainer1[i]['pointId'] == data.id) {
                        GlobalData.push(selectedPointContainer1[i]);
                        selectedPointContainer1.splice(i, 1);
                    }
                }
                regenerateTable(selectedPointContainer1, 'tbodyData_one2', 'exedel',1);
            }

            //判断selectedPointContainer内的点位数据名字是否改变
            //若名字改变则要保留改变的名字信息

            regenerateTable(GlobalData, 'tbodyData', 'exeselect');
//            regenerateTable(selectedPointContainer, 'tbodyData_one', 'exedel');
//            regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');

        };


        window.exeselect = function(data){
            if(selectedPointContainer==undefined)
                selectedPointContainer=new Array();
            console.log(selectedPointContainer);
            console.log(selectedPointContainer1);
            if(selectedPointContainer!=undefined)
                if (selectedPointContainer.length != 0) {
                    for (var i in selectedPointContainer) {
                        if (data.id == selectedPointContainer[i]['pointId']) {
                            alert('已经存在该点位');
                            return false;
//
                        }
                    }
                }

//                //判断是否已经添加点位
//                if (window.IsEmpty())

            for (var i in GlobalData) {
                if (data.id == GlobalData[i]['pointId']) {
                    selectedPointContainer.push(GlobalData[i]);
                    GlobalData.splice(i, 1);
                }
            }
            regenerateTable(GlobalData, 'tbodyData', 'exeselect');
            regenerateTable(selectedPointContainer, 'tbodyData_one1', 'exedel',0);
//            regenerateTable(selectedPointContainer, 'tbodySelected', 'exedel');
        };


        window.exeselect1 = function(data){
            if(selectedPointContainer1==undefined)
                selectedPointContainer1=new Array();
            console.log(selectedPointContainer);
            console.log(selectedPointContainer1);
            if(selectedPointContainer1!=undefined)
                if (selectedPointContainer1.length != 0) {
                    for (var i in selectedPointContainer1) {
                        if (data.id == selectedPointContainer1[i]['pointId']) {
                            alert('已经存在该点位');
                            return false;
//
                        }
                    }
                }

//                //判断是否已经添加点位
//                if (window.IsEmpty())

            for (var i in GlobalData) {
                if (data.id == GlobalData[i]['pointId']) {
                    selectedPointContainer1.push(GlobalData[i]);
                    GlobalData.splice(i, 1);
                }
            }
            regenerateTable(GlobalData, 'tbodyData', 'exeselect');
            regenerateTable(selectedPointContainer1, 'tbodyData_one2', 'exedel',1);
//            regenerateTable(selectedPointContainer1, 'tbodySelected', 'exedel');
        };

        //初始化tabs窗口
        $('#tabs').tabs();
        //初始化accordion窗口
        var accordionIcons = {
            header: "fa fa-plus",    // custom icon class
            activeHeader: "fa fa-minus" // custom icon class
        };
        $(".accordion").accordion({
            autoHeight: false,
            heightStyle: "content",
            collapsible: true,
            animate: 300,
            icons: accordionIcons,
            header: "h4",
        })

        var layer_name = $("#layer_name").dialog({
            autoOpen : false,
            width : 400,
            height:270,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {
                html : "<i class='fa fa-plus'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
                    var main_point=selectedPointContainer[0]['pointId'];
                    var link_point=[];
                    for(var key in selectedPointContainer1){
                        var point=selectedPointContainer1[key]['pointId'];
                        link_point.push(point);
                    }
//                    console.log(main_point);
//                    console.log(binding_points);
                    var link_name=$("#point_link_name").val();
                    if(link_name.length==0)
                        alert("名称不能为空");
                    else {
                        //ajax
                        $.ajax({
                            type: "POST",
                            url: "/point_link/crud/ajax-point-link",
                            data: {name: link_name, main_point:main_point, link_point:link_point},
                            //暂时未加判定是否成功插入
                            success: function (msg) {
                                console.log(msg);
                            }
                        });
                        $(this).dialog("close");
                    }

                }
            }
            ]
        });



        //提交时候，对表单进行验证
        $("#sub").click(function ()   {

            if(selectedPointContainer.length==0 || selectedPointContainer1.length==0)
                alert("选择点位不能为空");
            else layer_name.dialog("open");
        });
        $("#view").click(function ()   {

            window.location.href="/point_link/crud";
        })









    }
</script>