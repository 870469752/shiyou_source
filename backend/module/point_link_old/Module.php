<?php

namespace backend\module\point_link;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\point_link\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
