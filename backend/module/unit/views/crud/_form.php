<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\Unit $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                            <a id = 'ordinary' class="btn btn-primary btn-sm" href="javascript:void(0);">普通单位</a>
                            <a id = 'standard' class="btn btn-success btn-sm" href="javascript:void(0);">标准单位</a>
                            <HR />
						<?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
						<?= $form->field($model, 'unit_symbol')->textInput() ?>
                        <!-- 选择 标准单位 -->
                        <?= $form->field($model, 'standard_unit')->DropDownList(
                            $standard_unit,
                            ['class' => 'select2', 'style' => 'width:200px']
                        ) ?>

                        <?= $form->field($model, 'coefficient')->textInput() ?>

						<?= $form->field($model, 'category_id')->DropDownList(
							$unit_category,
							['class' => 'select2', 'style' => 'width:200px']
						) ?>
							<?= $form->field($model, 'flag')->hiddenInput(['value'=>'1']) ?>
						</fieldset>

						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<script>
    window.onload = function(){
        /*给两个按钮添加点击事件*/
        var i = 0;
        // 普通单位添加
        $('#ordinary').click(function(){
            i = 0;
            $('.field-unit-standard_unit').show();
            $('.field-unit-coefficient').show();
        });
        // 标准单位添加
        $('#standard').click(function(){
            i = 1;
            $('.field-unit-standard_unit').hide();
            $('.field-unit-coefficient').hide();
        })
        //提交时 判断 当前添加的单位是 普通 还是 标准
        $('.form-actions .btn').click(function (){
			if($('#unit-coefficient').val()==1 || $('#unit-coefficient').val()=='can not be 1')
			{$('#unit-coefficient').val('can not be 1');return false;}
            var $unit = $("#unit-name").val();
            if(i)
            {
                $('#unit-standard_unit').append("<option value='"+$unit+"'>"+$unit+"</option>");
                $('#unit-standard_unit').val($unit);
                console.log($('#unit-standard_unit').val())
                $('#unit-coefficient').val(1);
            }
        })
    }
</script>