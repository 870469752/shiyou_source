<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\UnitSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Units');
$this->params['breadcrumbs'][] = $this->title;

?>


<section>
<!-- row -->
<div class="row">


	<!-- NEW WIDGET START -->
	<article class="col-sm-12 col-md-12 col-lg-6">

		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget jarviswidget-color-greenDark" id="wid-id-2" data-widget-editbutton="false">
			<!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>标准单位</h2>

			</header>
            <div>
			<?= GridView::widget([
				'formatter' => ['class' => 'common\library\MyFormatter'],
				'dataProvider' => $dataProvider,
				'options' => ['class' => 'widget-body no-padding'],
				'tableOptions' => [
					'class' => 'table table-striped table-bordered table-hover',
					'width' => '100%',
					'id' => 'datatable1'
				],
				'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
				'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
				'columns' => [
					['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

					['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
					['attribute' => 'unit_symbol', 'headerOptions' => ['data-hide' => 'phone']],
					['attribute' => 'standard_unit', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
					['attribute' => 'coefficient', 'headerOptions' => ['data-hide' => 'phone,tablet']],
					['attribute' => 'category_id', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
					//['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
				],
			]); ?>
			</div>
		</div>
		<!-- end widget -->

	</article>
	<!-- WIDGET END -->

	<!-- NEW WIDGET START -->
	<article class="col-sm-12 col-md-12 col-lg-6">

		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget jarviswidget-color-greenLight" id="wid-id-3" data-widget-editbutton="false">
			<!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>自定义单位</h2>

			</header>

			<!-- widget div-->
			<div>
				<?= GridView::widget([
					'formatter' => ['class' => 'common\library\MyFormatter'],
					'dataProvider' => $dataProvider1,
					'options' => ['class' => 'widget-body no-padding'],
					'tableOptions' => [
						'class' => 'table table-striped table-bordered table-hover',
						'width' => '100%',
						'id' => 'datatable'
					],
					'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
					'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
					'columns' => [
						['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

						['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
						['attribute' => 'unit_symbol', 'headerOptions' => ['data-hide' => 'phone']],
						['attribute' => 'standard_unit', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
						['attribute' => 'coefficient', 'headerOptions' => ['data-hide' => 'phone,tablet']],
						['attribute' => 'category_id', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],

						['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
					],
				]); ?>

				<?php
				// 搜索和创建按钮
				$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
				$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

				// 添加两个按钮和初始化表格
$js_content1 = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$search_button}');
options.button.push('{$create_button}');
table_config('datatable', options);
JAVASCRIPT;
				$this->registerJs ( $js_content1, View::POS_READY);
				?>
				<!-- 搜索表单和JS -->
				<?php MyFunc::TableSearch($searchModel) ?>

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->

	</article>
	<!-- WIDGET END -->

</div>
<!-- end row -->
</section>
    