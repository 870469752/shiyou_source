<?php

namespace backend\module\unit\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\Unit;
use backend\models\search\UnitSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\BaseArrayHelper;
use backend\models\OperateLog;
use backend\models\UnitCategory;
use backend\models\UnitCategoryRel;
/**
 * CrudController implements the CRUD actions for Unit model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnitSearch;
        //得到所有固定单位
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        //得到所有自定义单位
        $dataProvider1 = $searchModel->_search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider1'=>$dataProvider1,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Unit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Unit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Unit();
        $unit_category_model=new UnitCategory();

        if ($model->load($form_data = Yii::$app->request->post())&&    $result = $model->saveUnit()) {

            $result1=$result['result'];
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建单位';
            $op['en'] = 'Create unit';
            @$this->getLogOperation($op,$info,$result1);
            return $this->redirect(['index']);
        } else {
            //获取所有标准单位
            $standard_unit = BaseArrayHelper::map(Unit::getStandardUnit(), 'id', 'name');
            //处理name格式
            foreach($standard_unit as $key=>$value){
                $standard_unit[$key]=MyFunc::DisposeJSON($standard_unit[$key]);
            }
            $unit_category =BaseArrayHelper::map($unit_category_model->find()->asArray()->all(),'id','name');
            //处理name格式
            foreach($unit_category as $key=>$value){
                $unit_category[$key]=MyFunc::DisposeJSON($unit_category[$key]);
        }
            return $this->render('create', [
                'model' => $model,
                'standard_unit' => $standard_unit,
                'unit_category' => $unit_category,
            ]);
        }
    }

    /**
     * Updates an existing Unit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $unit_category_model=new UnitCategory();
        if ( $model->load($form_data = Yii::$app->request->post()) && $result = $model->saveUnit()) {
//            echo'<pre>';
//            print_r($form_data);
//            print_r($model);
//            die;
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建单位';
            $op['en'] = 'Create unit';
            @$this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);
            //获取所有标准单位
            $standard_unit = BaseArrayHelper::map(Unit::getStandardUnit(), 'id', 'name');
            //处理name格式
            foreach($standard_unit as $key=>$value){
                $standard_unit[$key]=MyFunc::DisposeJSON($standard_unit[$key]);
            }
            $unit_category =BaseArrayHelper::map($unit_category_model->find()->asArray()->all(),'id','name');
            //处理name格式
            foreach($unit_category as $key=>$value){
                $unit_category[$key]=MyFunc::DisposeJSON($unit_category[$key]);
            }

            return $this->render('update', [
                'model' => $model,
                'standard_unit' => $standard_unit,
                'unit_category' => $unit_category,
            ]);
        }
    }

    /**
     * Deletes an existing Unit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '删除单位';
        $op['en'] = 'Delete unit';
        @$this->getLogOperation($op,'','');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
