<?php

namespace backend\module\unit;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\unit\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
