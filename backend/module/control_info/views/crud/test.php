

<?php
use yii\base\View;
?>
<table id="user" class="table table-bordered table-striped" style="clear: both">
    <tbody>
    <tr>
        <td style="width:35%;">Simple text field</td>
        <td style="width:65%"><a href="form-x-editable.html#" id="username" data-type="text" data-pk="1" data-original-title="Enter username">superuser</a></td>
    </tr>
    <tr>
        <td>Empty text field, required</td>
        <td><a href="form-x-editable.html#" id="firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-original-title="Enter your firstname"></a></td>
    </tr>
    <tr>
        <td>Select, local array, custom display</td>
        <td><a href="form-x-editable.html#" id="sex" data-type="select" data-pk="1" data-value="" data-original-title="Select sex"></a></td>
    </tr>
    <tr>
        <td>Select, remote array, no buttons</td>
        <td><a href="form-x-editable.html#" id="group" data-type="select" data-pk="1" data-value="5" data-source="/groups" data-original-title="Select group">Admin</a></td>
    </tr>
    <tr>
        <td>Select, error while loading</td>
        <td><a href="form-x-editable.html#" id="status" data-type="select" data-pk="1" data-value="0" data-source="/status" data-original-title="Select status">Active</a></td>
    </tr>

    <tr>
        <td>Datepicker</td>
        <td><a href="#" id="vacation" data-type="date" data-viewformat="dd.mm.yyyy" data-pk="1" data-placement="right" data-original-title="When you want vacation to start?">25.02.2013</a></td>
    </tr>
    <tr>
        <td>Combodate (date)</td>
        <td><a href="form-x-editable.html#" id="dob" data-type="combodate" data-value="1984-05-15" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1" data-original-title="Select Date of birth"></a></td>
    </tr>
    <tr>
        <td>Combodate (datetime)</td>
        <td><a href="form-x-editable.html#" id="event" data-type="combodate" data-template="D MMM YYYY  HH:mm" data-format="YYYY-MM-DD HH:mm" data-viewformat="MMM D, YYYY, HH:mm" data-pk="1" data-original-title="Setup event date and time"></a></td>
    </tr>

    <tr>
        <td>Textarea, buttons below. Submit by <i>ctrl+enter</i></td>
        <td><a href="form-x-editable.html#" id="comments" data-type="textarea" data-pk="1" data-placeholder="Your comments here..." data-original-title="Enter comments">awesome user!</a></td>
    </tr>

    <tr>
        <td>Twitter typeahead.js</td>
        <td><a href="form-x-editable.html#" id="state2" data-type="typeaheadjs" data-pk="1" data-placement="right" data-original-title="Start typing State.."></a></td>
    </tr>

    <tr>
        <td>Checklist</td>
        <td><a href="form-x-editable.html#" id="fruits" data-type="checklist" data-value="2,3" data-original-title="Select fruits"></a></td>
    </tr>

    <tr>
        <td>Select2 (tags mode)</td>
        <td><a href="form-x-editable.html#" id="tags" data-type="select2" data-pk="1" data-original-title="Enter tags">html, javascript</a></td>
    </tr>

    <tr>
        <td>Select2 (dropdown mode)</td>
        <td><a href="form-x-editable.html#" id="country" data-type="select2" data-pk="1" data-select-search="true" data-value="BS" data-original-title="Select country"></a></td>
    </tr>

    <tr>
        <td>Custom input, several fields</td>
        <td><a href="form-x-editable.html#" id="address" data-type="address" data-pk="1" data-original-title="Please, fill address"></a></td>
    </tr>

    </tbody>
</table>

<a href="form-x-editable.html#" id="username" data-type="text" data-pk="1" data-original-title="Enter username">superuser</a>

    <?php

    $this->registerJsFile('/js/plugin/x-editable/moment.min.js', ['depends' => 'yii\web\JqueryAsset']);
    $this->registerJsFile("/js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
    $this->registerJsFile('/js/plugin/x-editable/x-editable.min.js', ['depends' => 'yii\web\JqueryAsset']);

    ?>
    <script>

        window.onload = function () {

            //ajax mocks
            $.mockjaxSettings.responseTime = 500;

            $.mockjax({
                url: '/post',
                response: function (settings) {
                    log(settings, this);
                }
            });
            //editables
            $('#firstname').editable({
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'This field is required';
                }
            });
            //editables
            $('#username').editable({
                url: '/post',
                type: 'text',
                pk: 1,
                name: 'username',
                title: 'Enter username'
            });
            $('#sex').editable({
                prepend: "not selected",
                source: [{
                    value: 1,
                    text: 'Male'
                }, {
                    value: 2,
                    text: 'Female'
                }],
                display: function (value, sourceData) {
                    var colors = {
                        "": "gray",
                        1: "green",
                        2: "blue"
                    }, elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });

                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });

            $('#status').editable();

            $('#group').editable({
                showbuttons: false
            });

            $('#vacation').editable({
                datepicker: {
                    todayBtn: 'linked'
                }
            });

            $('#dob').editable();

            $('#event').editable({
                placement: 'right',
                combodate: {
                    firstItem: 'name'
                }
            });

            $('#meeting_start').editable({
                format: 'yyyy-mm-dd hh:ii',
                viewformat: 'dd/mm/yyyy hh:ii',
                validate: function (v) {
                    if (v && v.getDate() == 10)
                        return 'Day cant be 10!';
                },
                datetimepicker: {
                    todayBtn: 'linked',
                    weekStart: 1
                }
            });

            $('#comments').editable({
                showbuttons: 'bottom'
            });

            $('#note').editable();
            $('#state').editable({
                source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
                    "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
                    "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
                    "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
                    "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon",
                    "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
                    "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"
                ]
            });

            $('#state2').editable({
                value: 'California',
                typeahead: {
                    name: 'state',
                    local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
                        "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
                        "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan",
                        "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
                        "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio",
                        "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
                        "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia",
                        "Wisconsin", "Wyoming"
                    ]
                }
            });

            $('#fruits').editable({
                pk: 1,
                limit: 3,
                source: [{
                    value: 1,
                    text: 'banana'
                }, {
                    value: 2,
                    text: 'peach'
                }, {
                    value: 3,
                    text: 'apple'
                }, {
                    value: 4,
                    text: 'watermelon'
                }, {
                    value: 5,
                    text: 'orange'
                }]
            });

            $('#tags').editable({
                inputclass: 'input-large',
                select2: {
                    tags: ['html', 'javascript', 'css', 'ajax'],
                    tokenSeparators: [",", " "]
                }
            });
        }
    </script>
