<?php


use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2016/7/6
 * Time: 15:30
 */
?>
<!-- NEW WIDGET START -->
<article class="col-sm-12 col-md-12 col-lg-12">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-greenDark" id="wid-id-2" data-widget-editbutton="false">
        <!-- widget options:
        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

        data-widget-colorbutton="false"
        data-widget-editbutton="false"
        data-widget-togglebutton="false"
        data-widget-deletebutton="false"
        data-widget-fullscreenbutton="false"
        data-widget-custombutton="false"
        data-widget-collapsed="true"
        data-widget-sortable="false"

        -->
        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2>控制信息</h2>

        </header>
        <div>
            <?= GridView::widget([
                'formatter' => ['class' => 'common\library\MyFormatter'],
                'dataProvider' => $dataProvider,
                'options' => ['class' => 'widget-body no-padding'],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover',
                    'width' => '100%',
                    'id' => 'datatable1'
                ],
                'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],


                    ['attribute' => 'name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
                    ['attribute' => 'time_mode_name', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
//                    ['attribute' => 'unit_symbol', 'headerOptions' => ['data-hide' => 'phone']],
//                    ['attribute' => 'standard_unit', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
//                    ['attribute' => 'coefficient', 'headerOptions' => ['data-hide' => 'phone,tablet']],
//                    ['attribute' => 'category_id', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone']],
                    ['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
                ],
            ]); ?>
        </div>
    </div>
    <!-- end widget -->

</article>
<!-- WIDGET END -->