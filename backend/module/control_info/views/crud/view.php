<?php
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2016/7/11
 * Time: 12:11
 */
?>
<div id="controlInfoDIV">
    <!--时间  信息    -->
    <div id="controlTime" style="border: 1px solid #36b1ff">
    </div>
    <!--  选中时间信息  -->
    <div id='timeSelect'>
        <div  style="border: 1px solid #36b1ff">
            <fieldset >
                <fieldset>
                    <label style="margin-left: -10px" class="label">执行计划</label>
                </fieldset>
                <div  style="float:left;margin-right: 108px">
                    <label class="label" style="float:left">开始时间</label>
                    <select id="begin" class="chosen-select" name="begin" style="width: 200px;float:left;">
                        <option value="0" selected >0点</option>
                        <?php  for($i=1;$i<24;$i++){ ?>
                            <option value="<?=$i?>"><?=$i?>点</option>
                        <?php }?>
                    </select>
                </div>
                <div style="float:left">
                    <label class="label"  style="float:left;margin-right: 108px">结束时间</label>
                    <select id="over"  class="chosen-select" name="end" style="width: 200px;">
                        <option value="0" selected>0点</option>
                        <?php  for($i=1;$i<24;$i++){ ?>
                            <option value="<?=$i?>"><?=$i?>点</option>
                        <?php }?>
                    </select>
                </div>
                <div style="float:left;">
                    <button type="button" id="delete" class="create btn-success" onclick="deleteBytime(this)" style="margin-left: 20px ">删除</button>
                    <button type="button" id="update" class="create btn-success" onclick="updateBytime(this)" style="margin-left: 20px ">更新</button>
                    <button type="button" id="restore" class="create btn-success" onclick="restore(this)" style="margin-left: 20px ">恢复</button>
                </div>
            </fieldset>
            <!--控制字段 信息-->
            <fieldset id="control_attr">


            </fieldset>
        </div>
    </div>

    <!--控制字段 信息-->
    <fieldset id="control_attr">


    </fieldset>
</div>

<?php


$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);
$this->registerJsFile('/js/plugin/x-editable/moment.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("/js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile('/js/plugin/x-editable/x-editable.min.js', ['depends' => 'yii\web\JqueryAsset']);


?>


<script>
    //得到控制信息
    var controlData=eval(<?=$model->data?>);


    //点击时间按钮显示详细信息
    function show(object) {
        /* 判断是否保存上个   start*/
        //更新点击的时间节点控制信息时 先保存上个节点的信息
        //如果不存在这个时间点则说明已经删除了,不需要执行保存
        //遍历找到对应时间的控制信息并保存
        var controlInfo=controlData;
        if(controlInfo!=undefined) {
            controlInfo=controlInfo['controlInfo']['data'];
            for (var key in controlInfo) {
                var start = controlInfo[key]['time']['start'];
                var end = controlInfo[key]['time']['end'];
                if (now_start == start && now_end == end) {
                    //即是表明这个时间存在 才能够保存
//                        saveControlInfo(tab_id, now_start, now_end);
                }
            }
        }

        /*判断是否保存上个     end*/

        var start = $(object).attr("start");
        var end = $(object).attr("end");

        //把时间写入时间选择下拉框
        $("#begin").val(start);
        $("#begin").trigger("chosen:updated");
        $("#over").val(end);
        $("#over").trigger("chosen:updated");

        now_start = start;
        now_end = end;
//            alert(start+'-'+end);
        //拼凑容纳模式控制信息的div的id
        //得到此id的div  如果得不到就加入一个 如果得到就置为空
        var controlAttrDiv = document.getElementById(control_attr);
        if (controlAttrDiv == null) {
            $("#controlInfoDIV").append('<table class="table table-bordered table-striped" style="clear: both"><tbody id=' + control_attr + '></tbody></table>');
        }
        else $("#"+control_attr).html('');
        console.log(2222222);
        console.log(controlData);
        //从存储的全局变量中取得control模式信息
        var controlInfo = controlData['controlInfo']['data'];
        //controlInfo内包含有多个时间段信息   标识符存在按钮的temDate属性中
        //所以当前时间段的控制信息为
        var temData = $(object).attr("temData");
        var clickControlInfo = controlInfo[temData]['control'];
        console.log(111111111111);
        console.log(clickControlInfo);

        for (var key in clickControlInfo) {
            var name = 'control_attr_name' + key;
            var controlName = clickControlInfo[key]['name'];
            var controlValue = clickControlInfo[key]['value'];
            console.log(controlName);
            console.log(controlValue);
            var str = '<tr name=' + name + '>' +
//                '<td style="width:30%><a href="form-x-editable.html#" class="test" data-type="select2" data-pk="1" data-select-search="true" data-value="BS" data-original-title="Select country"></a></td>'
                '<td style="width:30%"><a href="form-x-editable.html#"  class="controlName" data-type="text" data-pk="1" data-original-title="Enter username" >superuser</a></td>' +
                '<td style="width:30%"><a href="form-x-editable.html#"  class="pointSelect" data-type="select2" data-pk="2" data-value="" data-original-title="Select" ></a></td>' +
                '<td style="width:30%"><a href="form-x-editable.html#"  class="controlValue" data-type="select2" data-pk="3" data-value="" data-original-title="Select" >superuser</a></td>' +
                '<td style="width:5%">' +
                '<label class="" style="margin-right: 5px"><span onclick="javascript:exedel(this)"name="ok" class="glyphicon glyphicon-plus" ></span></label>' +
                '</td>' +
                '<td style="width:5%">' +
                '<label class="" style="margin-right: 5px"><span onclick="javascript:exedel(this)"name="remove" class="glyphicon glyphicon-remove" ></span></label>' +
                '</td>' +
                '</tr>';
            //把构建的 属性显示框一行行放入展示div中
            $("#" + control_attr).append(str);
            $(".chosen-select").chosen();
            //初始化这一行的三个输入框
            editable();
            //名称赋值
            $("tr[name=" + name + "]").find($(".controlName")).editable("setValue", controlName);
            //点位赋值
            var controlPoints = clickControlInfo[key]['pointId'];
            if (controlPoints != undefined) {
                $("tr[name=" + name + "]").find($(".pointSelect")).editable("setValue", controlPoints);
                //控制值赋值
                $("tr[name=" + name + "]").find($(".controlValue")).editable("setValue", controlValue);
                //如果点位值不为空则更新点位框值

                //TODO根据名字匹配给点位选择框赋值
            }
            //属性列举完之后加入按钮

        }
    }
    window.onload = function () {

        //根据信息生成控制表格信息
        console.log(controlData);
        Init(controlData,"controlTime");

        //画出时间标签在标签号为tab_num标签内
        function Init(data,controlTime)
        {
            var temData=data['data'];

            //得到此id的div  如果得不到就加入一个 如果得到就置为空
            var controlTimeDId = controlTime;
            $("#"+controlTimeDId).html('');
            if(temData.length>=0)
                for(var i in temData)
                {
                    var start=temData[i]['time']['start'];
                    var end=  temData[i]['time']['end'];
                    var s=start+'点'+'--'+end+'点';
                    var str='<button type="button" temData='+i+' start="'+start+'" end="'+end+'"  class="create btn-success" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';

                    //在本标签页下加入时间按钮信息
                    $("#"+controlTimeDId).append(str);
                }
        }


    }
</script>