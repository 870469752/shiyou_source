<?php
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2016/6/30
 * Time: 14:57
 */

use yii\helpers\Html;


//$timeModeInfo=[
//    1=>'夏级模式',
//    2=>'冬季模式'
//];
//$controlModelInfo=[
//    1=>'控制模式1',
//    2=>'控制模式2',
//    3=>'控制模式3'
//];
//$subsystemPoint=[
//    1=>'testpoint',
//    2=>'testPoint2',
//    3=>'测试点位'
//];
$subsystemPoint=$point;
?>
<style>
    .create{
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 13px;
        line-height: 1.42857143;
        border-radius: 2px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .popover-content:last-child {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .popover-content {
        padding: 9px 14px;
    }
    .popover-title {
        margin: 0;
        padding: 8px 14px;
        font-size: 13px;
        font-weight: 400;
        line-height: 18px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 2px 2px 0 0;
    }
    .editable-container.popover {
        width: auto;
    }
    .smart-form .col-4 {
        width: 100%;
    }
    .smart-form .state-success {
        width: 10%;
        float: left;
    }
    .smart-form fieldset {
        height: auto;
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px dashed rgba(0,0,0,.2);
        background: rgba(255,255,255,.9);
        position: relative;
    }
</style>
<!-- html 区域 start-->
<section id="widget-grid" class="">
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false"
            >
            <header>
                <!--    -->
                <div class="widget-toolbar smart-form" data-toggle="buttons">
                    <button class="btn btn-xs btn-primary" id="subsystem_config"  >
                        保存
                    </button>
                    <button id="add_tab" class="btn btn-primary">
                        配置
                    </button>
                </div>
                <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                <h2>子系统控制模式配置 </h2>

            </header>

            <div>
                <p> <h4>名称</h4></p>
                <?=Html::textInput('name', '', ['placeholder' => Yii::t('app', 'Name'), 'class' => 'form-control','id'=>'ControlName'])?>
                <p> <h4>控制信息</h4></p>
                        <div style="margin-top: 20px">
                        <!--      控制信息标签页                      -->
                            <div id="tabs2">
                    <ul>
                        <li>
                            <a href="#tabs-1">Nunc tincidunt</a>
                        </li>
                    </ul>
                    <div id="tabs-1">
                        <div id="timebutton" style=""></div>
                        <!-- 开始时间，结束时间-->
                            <div id='timeSelect'>
                                <div  style="border: 1px solid #36b1ff">
                                <fieldset >
                                    <fieldset>
                                        <label style="margin-left: -10px" class="label">执行计划</label>
                                    </fieldset>
                                    <div  style="float:left;margin-right: 108px">
                                        <label class="label" style="float:left">开始时间</label>
                                        <select id="begin" class="chosen-select" name="begin" style="width: 200px;float:left;">
                                            <option value="0" selected >0点</option>
                                            <?php  for($i=1;$i<24;$i++){ ?>
                                                <option value="<?=$i?>"><?=$i?>点</option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div style="float:left">
                                        <label class="label"  style="float:left;margin-right: 108px">结束时间</label>
                                        <select id="over"  class="chosen-select" name="end" style="width: 200px;">
                                            <option value="0" selected>0点</option>
                                            <?php  for($i=1;$i<24;$i++){ ?>
                                                <option value="<?=$i?>"><?=$i?>点</option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div style="float:left;">
                                        <button type="button" id="delete" class="create btn-success" onclick="deleteBytime(this)" style="margin-left: 20px ">删除</button>
                                        <button type="button" id="update" class="create btn-success" onclick="updateBytime(this)" style="margin-left: 20px ">更新</button>
                                        <button type="button" id="restore" class="create btn-success" onclick="restore(this)" style="margin-left: 20px ">恢复</button>

                                    </div>
                                </fieldset>
                                <!--控制字段 信息-->
                                <fieldset id="control_attr">


                                </fieldset>
                            </div>
                            </div>
                    </div>
                </div>
                        </div>


        </div>
    </article>
</section>
<!-- html 区域 end-->
<!-- Demo -->
<div id="addtab" title="模式选择">
    <form>
        <fieldset>
            <?=Html::dropDownList('时间模式',null,$timeModelInfo,['class'=>'select2','id'=>'timeMode']);?>

            <?=Html::dropDownList('控制模式',null,$schemaControlName,['class'=>'select2','id'=>'controlModel']);?>
        </fieldset>
    </form>
</div>


<?php
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.colVis.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);
?>

<script>
    //记录子系统所有控制模块儿的信息
    var controlData=[];
    //data数组,time,control数组
    var timeModelData=[];
    var time=[];
    var control=[];
    //控制字段与默认值
    var control_name_value=[];
    //时间段个数
    var time_count=0;
    //每个时间段控制记录数
    var control_name_count=0;
    //总记录数
    var count=0;
    //得到data中所有时间
    var alltime=[];
    //修改标志
    var flag=false;
    //当前修改的时间段
    var currentTime=[];
    //初始页面数据
    var initData;
    //删除的起始时间
    var deletestart;
    //标签页编号
    var tabCounter = 2;
    //生成 点位下拉框
    var subsystemPoints=<?=json_encode($subsystemPoint)?>;
    //记录当前所在的标签id  开始时间 和结束时间
    var tab_id=null,now_start=null,now_end=null;
    console.log(subsystemPoints);
    //点位下拉框的html
    var PointSelect=  '<select class="chosen-select pointSelect "  multiple style="width:100%;height:100px">';
    for(var key in subsystemPoints){
        PointSelect+='<option value='+key+'>'+subsystemPoints[key]+'</option>';
    }
    PointSelect+='</select>';

    //数组remove指定键值元素 数据
    function remove(a,id){
        var result=new Array();
        for(var key in a){
            if(key!=id)
                result[key]=a[key];
        }
        return result;
    }
    //得到时间显示框的html
        //点击时间按钮显示详细信息
        function show(object){
            //更新点击的时间节点控制信息时 先保存上个节点的信息
            saveControlInfo(tab_id,now_start,now_end);
        //本标签的id
        var id=$(object).parent().parent().attr("id");
        console.log($(object).parent());
        //得到标签编号
        var tab_num=id.substring(14,id.length);
        var text=$(object).html();
        var start=$(object).attr("start");
        var end=$(object).attr("end");

        console.log("开始结束时间");
        console.log(start);
        console.log(end);
        //把时间写入时间选择下拉框
        $("#begin").val(start);
        $("#begin").trigger("chosen:updated");
        $("#over").val(end);
        $("#over").trigger("chosen:updated");

        now_start= start;
        now_end=end;
//            alert(start+'-'+end);
        //拼凑容纳模式控制信息的div的id
        var control_attr="control_attr"+tab_num;
        //得到此id的div  如果得不到就加入一个 如果得到就置为空
        var controlAttrDiv=document.getElementById(control_attr);
        if(controlAttrDiv==null) {
            $(object).parent().parent().append('<div id=' + control_attr + ' style="margin-top: 20px"> </div>');
        }
        else $("#"+control_attr).html('');


        //从存储的全局变量中取得control模式信息
        var controlInfo=controlData[tab_num]['controlInfo']['data'];
        //controlInfo内包含有多个时间段信息   标识符存在按钮的temDate属性中
        //所以当前时间段的控制信息为
        var  temData=$(object).attr("temData");
        var clickControlInfo=controlInfo[temData]['control'];
        console.log(clickControlInfo);
            for(var key in clickControlInfo){
                var name='control_attr_name'+key;
                var controlName=clickControlInfo[key]['name'];
                var controlValue=clickControlInfo[key]['value'];
                var str='<div class="control" style="clear:both;margin-top: 50px;margin-bottom:50px;" >'+
                    '<div style="float:left;margin-right: 20px" name='+name+'>'+
                    '<input type="text"  class="form-control controlName"  value='+controlName+ ' style="width:200px">'+
                    '</div>'+
                    '<div style="float:left;margin-right:20px;width:400px" name='+name+' ">'+
                     PointSelect+
                    '</div>'+
                    '<div style="float:left;margin-right:20px" name='+name+'>'+
                    '<input type="text"  class="form-control controlValue" value='+controlValue+ ' style="width:200px">'+
                    '</div>'+
                    '<div  style="float:left; ">'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>'+
                    '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
                    '</div>'+
                    '<div style="height:20px;"> </div>'+
                    '</div>';
                //更新单选框里的值
                var controlPoints=clickControlInfo[key]['pointId'];

                //把构建的 属性显示框一行行放入展示div中
                $("#"+control_attr).append(str);
                //给点位选择框初始化
                $(".chosen-select").chosen();
                //如果点位值不为空 则更新下拉框的值
                if(controlPoints!=undefined){
                var control_msg=$("#"+control_attr).find($("div[name="+name+""));
//                var controName= control_msg.find($(".controlName")).val();
                var pointSelect=control_msg.find($(".pointSelect"));
                    pointSelect.val(controlPoints);
                    pointSelect.trigger("chosen:updated");
//                var controlValue= control_msg.find($(".controlValue")).val();
                }
                //TODO根据名字匹配给点位选择框赋值
            }
        //属性列举完之后加入按钮

    }
    //根据当前时间选择删除本标签页下的控制信息
    function deleteBytime(obj){
        var id=$(obj).parent().parent().parent().parent().parent().attr("id");
        //得到标签编号
        var tab_num=id.substring(14,id.length);
        //得到开始结束时间
        var begin=$("#begin").val();
        var over=$("#over").val();
        //获取当前控制信息
        var controlInfo=controlData[tab_num]['controlInfo']['data'];
        var newControlInfo=[];
        //遍历找到对应时间的控制信息并删除
        for(var key in controlInfo){
            var start=controlInfo[key]['time']['start'];
            var end=controlInfo[key]['time']['end'];
            if(begin==start&& end==over){
                console.log('删除时间'+begin+'-'+end);
            }
            else {
                newControlInfo.push(controlInfo[key]);
            }
        }
        //最后把新拼凑的值放进控制信息字段
        controlData[tab_num]['controlInfo']['data']=newControlInfo;
        //重画时间标签
        Init(controlData[tab_num]['controlInfo'],tab_num);
    }

    function updateBytime(obj){
        var id=$(obj).parent().parent().parent().parent().parent().attr("id");
        //得到标签编号
        var tab_num=id.substring(14,id.length);
        //得到开始结束时间
        var begin=$("#begin").val();
        var over=$("#over").val();
        saveControlInfo(tab_num,begin,over);
    }
    //提供 当前标签页  开始时间 结束时间 即可获取控制信息更新
    function saveControlInfo(tab_num,begin,over){

        if(tab_num!=null) {
//            alert("保存信息"+tab_num+'-'+begin+'-'+over);
            //获取当前控制信息
            var controlInfo = controlData[tab_num]['controlInfo']['data'];
            //遍历当前的控制信息写入选定的时间内
            var control_attr = $("#control_attr" + tab_num).children();
            //存储新的数据
            console.log(controlInfo);
            var new_controlInfo = [];
            control_attr.each(function () {
                //再靠find找到指定的几个类名得到需要的数据
                var controlName = $(this).find($(".controlName")).attr("value");
                var pointId = $(this).find($(".pointSelect")).val();
                var controlValue = $(this).find($(".controlValue")).attr("value");
                var control = {
                    name: controlName,
                    pointId: pointId,
                    value: controlValue
                };
                new_controlInfo.push(control);
            });
            //根据当前的时间更新time值
            //遍历找到对应时间的控制信息并更新  如果没找到则写入新的
            var timeExist = false;//标识此时间段是否存在
            for (var key in controlInfo) {
                var start = controlInfo[key]['time']['start'];
                var end = controlInfo[key]['time']['end'];
                //时间值一样 则更新
                if (begin == start && end == over) {
                    //存在此事件段则更改标识值 并更新
                    timeExist = true;
                    controlInfo[key]['control'] = new_controlInfo;
                    console.log('更新时间' + begin + '-' + end + '的控制信息');
                }
            }
            //判断标识值如果为false 则把新的信息写入
            if (timeExist == false) {
                var timeControlInfo = {
                    'control': new_controlInfo,
                    'time': {
                        'start': begin,
                        'end': over
                    }
                }
                controlInfo.push(timeControlInfo);

            }
            //最后把新拼凑的值放进控制信息字段
            controlData[tab_num]['controlInfo']['data'] = controlInfo;
            console.log("保存信息"+tab_num+'-'+begin+'-'+over);
           //重画时间标签
            if(timeExist==false)
            Init(controlData[tab_num]['controlInfo'], tab_num);
        }
    }
    //画出时间标签在标签号为tab_num标签内
    function Init(data,tab_num)
    {
        var temData=data['data'];

        //得到此id的div  如果得不到就加入一个 如果得到就置为空
        var controlTimeDId = "controlTime" + tab_num;
        $("#"+controlTimeDId).html('');
        if(temData.length>=0)
            for(var i in temData)
            {
                var start=temData[i]['time']['start'];
                var end=  temData[i]['time']['end'];
                var s=start+'点'+'--'+end+'点';
                var str='<button type="button" temData='+i+' start="'+start+'" end="'+end+'"  class="create btn-success" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';

                //在本标签页下加入时间按钮信息
                $("#"+controlTimeDId).append(str);
            }
    }
    window.onload = function () {
        var type=<?='"'.$type.'"'?>;
        //选择框初始化
        timeSelect=$("#timeSelect").html();
        $(".chosen-select").chosen();

//        console.log(timeSelect);
        var timeModeInfo=<?=json_encode($timeModelInfo)?>;
        var schemaControlInfo=<?=json_encode($schemaControlInfo)?>;
        console.log(schemaControlInfo);
        $("#timeMode option[value='"+'1'+"']").attr("selected","selected");
        $("#timeMode").chosen();

        // addTab button: just opens the dialog
        $("#add_tab").button().click(function() {
            dialog.dialog("open");
        });

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen : false,
            width : 600,
            height:300,
            resizable : false,
            modal : true,
            buttons : [{
                html : "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");

                }
            }, {

                html : "<i class='fa fa-plus'></i>&nbsp; Add",
                "class" : "btn btn-danger",
                click : function() {
                    //TODO
                    //加入模式之前还要判断  此模式是否与其他模式冲突
                    //冲突指  模式时间不能彼此覆盖
                    addTab();
                    $(this).dialog("close");
                }
            }]
        });
    // Dynamic tabs
    var tabTitle = '默认';
    var tabContent = null;
    var tabTemplate = "<li style='position:relative;'> " +
            "<span class='air air-top-left delete-tab' style='top:7px; left:7px;'>" +
            "<button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i>" +
            "</button>" +
            "</span>" +
            "</span>" +
            "<a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}" +
            "</a>" +
            "</li>";

    var tabs = $("#tabs2").tabs();


    // 添加标签页
    function addTab() {
        //时间模式的id 名称
        var timeModeId = $("#timeMode").val();
        var label=timeModeInfo[timeModeId];
        //控制模式的id 以及控制信息
        var ControlModelId=$("#controlModel").val();
        var ControlInfo=schemaControlInfo[ControlModelId];
        //清除别的地方的timeselect放在现在点击的地方
        $("#timeSelect").remove();

        //创建标签页  start
        var id = "tabs-" + tabCounter;
        var li = $(tabTemplate.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, label));
        var tabContentHtml=
            '<div id="myDiagram'+id+'"style="width:100%; height:650px;" ">' +
                '<div id="controlTime'+tabCounter+'" style="border: 1px solid #36b1ff"> ' +
                '</div >'+
                    '<div id="timeSelect">'+
                    timeSelect+
                    '</div>'+
            '</div>';
        tabs.find(".ui-tabs-nav").append(li);
        tabs.append("<div id='" + id + "'><p>" + tabContentHtml + "</p></div>");
        tabs.tabs("refresh");
        tabCounter++;
        console.log(11111);
        console.log(id);
        // clear fields
        $("#tab_title").val("");
        $("#tab_content").val("");
        //创建标签页   end
        //显示时间模式的信息
        //记下本模式的数据到要保存的数组中 以待更新
        console.log('加载的控制数据');
        console.log(ControlInfo);
        Init(ControlInfo,tabCounter-1);
        //并且把控制信息 加入到全局变量数据
            //时间和控制信息
            controlData[tabCounter-1]={
                'controlId':ControlModelId,
                'controlInfo':ControlInfo,
                'timeId':timeModeId
            };
        console.log('保存的控制信息');
        console.log(controlData);

    }
        //删掉标签页
        $("#tabs2").on("click", 'span.delete-tab', function() {
            var panelId = $(this).closest("li").remove().attr("aria-controls");
            $("#" + panelId).remove();
            //TODO  删除标签页时去掉controlData内存储的本标签页下控制信息
            var tab_num=panelId.substr(5,panelId.length);
            console.log(controlData);
            controlData=remove(controlData,tab_num);
            console.log(controlData);
            tabs.tabs("refresh");
        });

        //点击标签页事件
        $("#tabs2").on("click", 'a.ui-tabs-anchor', function() {
            //切换标签时  保存上一个标签的时间信息
            saveControlInfo(tab_id,now_start,now_end);
            //点击标签页 把timeSelect放入本标签页内
            var href=$(this).attr("href");
            var tab_num=href.substr(6,href.length);
            var controlTime='controlTime'+tab_num;
            //清除别的地方的timeselect放在现在点击的地方
            //清除之前记下时间
            $("#timeSelect").remove();
            $("#"+controlTime).after( '<div id="timeSelect">'+ timeSelect+ '</div>');
            //模拟点击第一个
            var buttons=$("#"+controlTime).children();
            buttons.eq(0).trigger("click");
            //更新表示当前标签页id的全局变量
            tab_id=tab_num;


        });




        /**********************************  模式数据加载 start  ***********************************/
            //子系统选择,子系统时间模式选择,子系统本事件模式下的控制模式选择



        //初始化时间按钮
        function drawButton()
        {
            if(time.length>0){
                var starttime=time[0];
                var endtime=time[1];
                var s=starttime+'点'+'--'+endtime+'点';
                var str='<button type="button"  class="create btn-success" onclick="show(this)" style="margin-right: 5px;margin-top: 2px">'+s+'</button>';
                var id = "controlTime" + (tabCounter-1);
                //在本标签页下加入时间按钮信息
                $("#"+id).append(str);
            }
        }

        window.exedel = function(object){
            var control_attr=$(object).parent().parent().parent().parent();
            var  str='<div class="control" style="clear:both;margin-top: 50px;margin-bottom:50px;" >'+
            '<div style="float:left;margin-right: 20px" name='+name+'>'+
            '<input type="text"  class="form-control controlName"  value='+'控制字段'+ ' style="width:200px">'+
            '</div>'+
            '<div style="float:left;margin-right: 20px;width:200px name='+name+' ">'+
            PointSelect+
            '</div>'+
            '<div style="float:left;margin-right:20px" name='+name+'>'+
            '<input type="text"  class="form-control controlValue" value='+0+ ' style="width:200px">'+
            '</div>'+
            '<div  style="float:left; ">'+
            '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="ok"><span class="glyphicon glyphicon-plus" ></span></button></label>'+
            '<label class=""><button style="width:30px;height:30px;margin-right:20px" type="button" onclick="javascript:exedel(this);" class="exeselect" id="" name="remove"><span class="glyphicon glyphicon-remove" ></span></button></label>'+
            '</div>'+
            '<div style="height:20px;"> </div>'+
            '</div>';

            if(object.name=='ok'){
                control_attr.append(str);
                $(".chosen-select").chosen();
            }else{
                $(object).parent().parent().parent().remove();
            }
        };

        /**********************************  模式数据加载 end     **********************************/
        //保存信息到control_info表中
        $("#subsystem_config").click(function(){
                console.log(controlData);
           if(type=="points")  sub_type=1;
            if(type=="subsystem")  sub_type=2;
            var name=$("#ControlName").val();
            if(name=='')alert("请填写名称")
            else {
                $.ajax({
                    type: "POST",
                    url: "/subsystem/crud/ajax-config-save",
                    data: {
                        name: name,
                        type: sub_type,
                        related_id:<?=$related_id?>,
                        data: controlData
                    },
                    success: function (msg) {
                        console.log('msg');
                    }
                });
            }
        })
}
</script>