<?php

namespace backend\module\control_info\controllers;

use backend\controllers\MyController;
use backend\models\InfoGroup;
use backend\models\InfoItem;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
