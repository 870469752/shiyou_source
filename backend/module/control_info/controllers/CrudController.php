<?php

namespace backend\module\control_info\controllers;

use backend\models\ControlInfo;
use backend\models\Image;
use backend\models\Location;
use backend\models\search\ControlInfoSearch;
use backend\models\TimeLinkage;
use Yii;
use backend\models\InfoGroup;
use backend\models\search\InfoGroupSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\SubSystem;
use backend\models\SubSystemElement;
use backend\models\ImageGroup;
use backend\models\CommandLog;
use common\models\User;
use backend\models\Point;
use backend\models\SchemaManage;
use backend\models\SchemaControl;
use common\library\MyFunc;
/**
 * CrudController implements the CRUD actions for InfoGroup model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ControlInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    public function actionView($id)
    {
        $mdoel=ControlInfo::findone($id);

        //控制模式信息
        $schemaControl=SchemaControl::find()->indexBy('id')
            ->asArray()->all();

        foreach ($schemaControl as $controlId=>$controlValue) {
            $schemaControl[$controlId]['control_info']=json_decode($schemaControl[$controlId]['control_info'],1);
        }
        $schemaControlName=MyFunc::_map($schemaControl,'id','name');
        $schemaControlInfo=MyFunc::map($schemaControl,'id','control_info');
        //时间模式信息
        $timeModelInfo=schemaManage::find()->asArray()->all();

        $timeModelInfo=MyFunc::_map($timeModelInfo,'id','name');
//        $pointsInfo=MyFunc::map($points,'id','cn');
        return $this->render('view',[
            'model'=>$mdoel,
            //模式信息
            'schemaControlInfo'=>json_encode($schemaControlInfo),
            //模式下拉列表信息
            'schemaControlName'=>json_encode($schemaControlName),
            'timeModelInfo'=>json_encode($timeModelInfo)
        ]);
    }
    public function actionTest(){
        return $this->render('test');
    }
    public function actionControlView($id){

        //得到本系统下的模式
        //type=2; related_id 为{$subsystem_id}
        $type=2;$related_id='{'.$id.'}';
        $searchModel = new ControlInfoSearch();
        $dataProvider = $searchModel->search(['type'=>$type,'related_id'=>$related_id]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    //$type=subsystem添加配置本系统下的模式
    //$type=points 配置对应点位的模式
    public function actionControl($related_id,$type){
        if($type=='subsystem') {
            //得到本子系统信息
            $subsystem = SubSystem::findOne($related_id);
            //处理得到本子系统内的点位信息
            $subsystem_points = json_decode($subsystem->points, 1)['data'];
            $subsystem_points = isset($subsystem_points['points']) ? $subsystem_points['points'] : null;
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $subsystem_points])
                ->asArray()->all();
        }
        if($type=='points'){
            $subsystem=null;
            $points=$related_id;
            $points=substr($points,1,strlen($points)-2);
            $points=explode(',',$points);
            //处理字符串为整数
            foreach($points as $key=>$value){
                $value=substr($value,1,strlen($value)-2);
                $points[$key]=(int)$value;
            }
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $points])
                ->asArray()->all();
        }


        //控制模式信息
        $schemaControl=SchemaControl::find()->indexBy('id')
            ->asArray()->all();

        foreach ($schemaControl as $controlId=>$controlValue) {
            $schemaControl[$controlId]['control_info']=json_decode($schemaControl[$controlId]['control_info'],1);
        }
        $schemaControlName=MyFunc::_map($schemaControl,'id','name');
        $schemaControlInfo=MyFunc::map($schemaControl,'id','control_info');
        //时间模式信息
        $timeModelInfo=schemaManage::find()->asArray()->all();

        $timeModelInfo=MyFunc::_map($timeModelInfo,'id','name');
        $pointsInfo=MyFunc::map($points,'id','cn');


        return $this->render('control',[
            'subsystem'=>$subsystem,
            'point'=>$pointsInfo,
            'type'=>$type,
            'related_id'=>json_encode($related_id),
            //模式信息
            'schemaControlInfo'=>$schemaControlInfo,
            //模式下拉列表信息
            'schemaControlName'=>$schemaControlName,
            'timeModelInfo'=>$timeModelInfo
        ]);
    }

    /*
     *
     * 模式 配置页面
     * $type为               'subsystem'  'points'
     * $related_id 对应为     子系统id      点位id
     *
    */
    public function actionConfig($related_id,$type){

        if($type=='subsystem') {
            //得到本子系统信息
            $subsystem = SubSystem::findOne($related_id);
            //处理得到本子系统内的点位信息
            $subsystem_points = json_decode($subsystem->points, 1)['data'];
            $subsystem_points = isset($subsystem_points['points']) ? $subsystem_points['points'] : null;
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $subsystem_points])
                ->asArray()->all();
        }
        if($type=='points'){
            $subsystem=null;
            $points=$related_id;
            $points=substr($points,1,strlen($points)-2);
            $points=explode(',',$points);
            //处理字符串为整数
            foreach($points as $key=>$value){
                $value=substr($value,1,strlen($value)-2);
                $points[$key]=(int)$value;
            }
            $points = Point::find()
                ->select("id,name->'data'->>'zh-cn' as cn")
                ->where(['id' => $points])
                ->asArray()->all();
        }


        //控制模式信息
        $schemaControl=SchemaControl::find()->indexBy('id')
            ->asArray()->all();

        foreach ($schemaControl as $controlId=>$controlValue) {
            $schemaControl[$controlId]['control_info']=json_decode($schemaControl[$controlId]['control_info'],1);
        }
        $schemaControlName=MyFunc::_map($schemaControl,'id','name');
        $schemaControlInfo=MyFunc::map($schemaControl,'id','control_info');
        //时间模式信息
        $timeModelInfo=schemaManage::find()->asArray()->all();

        $timeModelInfo=MyFunc::_map($timeModelInfo,'id','name');
        $pointsInfo=MyFunc::map($points,'id','cn');


        return $this->render('config',[
            'subsystem'=>$subsystem,
            'point'=>$pointsInfo,
            'type'=>$type,
            'related_id'=>json_encode($related_id),
            //模式信息
            'schemaControlInfo'=>$schemaControlInfo,
            //模式下拉列表信息
            'schemaControlName'=>$schemaControlName,
            'timeModelInfo'=>$timeModelInfo
        ]);
    }
    //点位模式 配置页面
    public function actionConfigPoint(){


        return $this->render('config_point',[
        ]);
    }
    //config信息保存
    public function actionAjaxConfigSave(){
        $param=Yii::$app->request->post();
//        echo '<pre>';
//        print_r($param);
//        die;
        $type=$param['type'];
        //类型是2 则表示关联着子系统
        if($type==2){
            $subsystem_id=$param['related_id'];
            $subsystem=SubSystem::findone($subsystem_id);
            $subsystemName=json_decode($subsystem->name,1);

            $name=[
                'data_type'=>'description',
                'data'=>[
                    'zh-cn'=>isset($subsystemName['data']['zh-cn'])?$subsystemName['data']['zh-cn']:null,
                    'en-us'=>isset($subsystemName['data']['en-us'])?$subsystemName['data']['en-us']:null
                ]
            ];
            $related_id='{'.$param['related_id'].'}';
        }

        //类型是1 则表示关联着 自定义点位
        if($type==1){
            $related_id=json_decode($param['related_id']);
            $saveId=null;
            foreach ($related_id as $key=>$value) {
                $saveId=$saveId.','.$value;
            }

            $name=[
                'data_type'=>'description',
                'data'=>[
                    'zh-cn'=>$param['name'],
                    'en-us'=>$param['name']
                ]
            ];
            $related_id='{'.substr($saveId,1,strlen($saveId)-1).'}';

        }


            foreach($param['data'] as $key=>$param_data) {
                    if(!empty($param_data)){
                       //处理data数据为新格式放入数据库
                        $data=[];

                        foreach ($param_data['controlInfo']['data'] as $num=>$value) {

                            $start=$value['time']['start'];
                            $end=$value['time']['end'];
                            //TODO 修改$valu[control]数据
                            foreach ($value['control'] as $control_key=>$control_value) {
                                    $value['control'][$control_key]['point_ids']=$control_value['pointId'];
                            }

                            $data[]=[
                                'start'=>$start,
                                'end'=>$end,
                                'control'=>$value['control']
                            ];
                       }

                        $model=new TimeLinkage();
                        $loaddata['TimeLinkage']=[
                            'name'=>json_encode($name),
                            'type'=>1,
                            'related_id'=>$related_id,
                            'time_mode_id'=>$param_data['timeId'],
                            'data'=>json_encode([
                                'data_type'=>'control_mode',
                                'data'=>$data
                                ]
                            )
                        ];

                        $model->load($loaddata);

                        $model->save();

                        $result=$model->errors;

                        if(!empty($result)){
                            echo '<pre>';
                            print_r($result);
                            die;
                        }
                    }
            }
    }
}
