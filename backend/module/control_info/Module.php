<?php

namespace backend\module\control_info;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\control_info\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
