<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Task $model
 */

$this->title = 'Update Task: ' . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
