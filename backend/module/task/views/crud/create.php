<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Task $model
 */
$this->title = 'Create Task';
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
