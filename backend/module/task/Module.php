<?php

namespace backend\module\task;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\task\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
