<?php

namespace backend\module\point_table_report\controllers;
//yii 引用
use backend\models\LocationCategory;
use backend\models\Protocol;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;
//模型 引用
use backend\models\Point;
use backend\models\CalculatePoint;
use backend\models\SimulatePoint;
use backend\models\BacnetPoint;
use backend\models\DemoPoint;
use backend\models\PointEvent;
use backend\models\search\PointDataNewSearch;
use backend\models\Unit;
use backend\models\Category;
use backend\models\PointDataNew;
use backend\models\search\PointDataSearch;
use yii\helpers\BaseArrayHelper;
use common\library\Ssp;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'active'=>'bacnet_show',
        ]);
    }

    //获取表格数据
    public function actionGetData()
    {
        $protocol_id = Yii::$app->request->get('protocol_id');
        $type = Yii::$app->request->get('type');
        if($type == 'point'){
            $con = " is_shield = false ";
        }else{
            $con = " is_shield = true ";
        }

        if($protocol_id){
            $con .= " and protocol_id = $protocol_id ";
        }else{
            if($type == 'point'){
                $con .= " and protocol_id != 1 and  protocol_id != 2 and  protocol_id != 4";
            }
        }
        //$db = Yii::$app->db;
        //echo "<pre>";print_r($con);die;
        $table = 'core.point';
        $primaryKey = 'id';
        $columns = array(
           array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => "name->'data'->>'zh-cn'as cn",  'dt' => "cn" ,'dj'=>"name->'data'->>'zh-cn'"),
            array( 'db' => "name->'data'->>'en-us'as us",  'dt' => "us" ,'dj'=>"name->'data'->>'en-us'" ),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        echo json_encode( $result);
    }

    //导出报表
    public function actionDoexport($point_id,$start=0,$end=0)
    {
        $searchModel = new PointDataNewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),$point_id,$start,$end);
        return $this->render('report', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    //导出图形
    public function actionGraphic($point_id)
    {
        $point_data=new PointDataNew();
        $name=$point_data->getName($point_id);
        $today=date('Y-m-d',time()).' '.'00:00:00';
        $tomorrow=date("Y-m-d",strtotime("+1 day")).' '.'00:00:00';
     $data=$point_data->find()->select(['id','point_id','value','timestamp'])
         ->where(['point_id'=>$point_id])
         ->andWhere(['between' ,'timestamp',$today,$tomorrow])
         ->orderBy('timestamp  asc')
         ->asArray()->all();
        return $this->render ( 'graph_new', [
            'data' => $data,
            'point_id'=>$point_id,
            'name'=>$name
        ] );
    }

    //根据时间获取数据
    public function actionGetDataByTime(){
        $begintime= $_GET['begintime'].' '.'00:00:00';
        $endtime=$_GET['endtime'].' '.'23:59:59';
        $point_id=(integer)$_GET['point_id'];
        $point_data=new PointDataNew();
        $data=$point_data->find()->select(['id','point_id','value','timestamp'])
            ->where(['between' ,'timestamp',$begintime,$endtime])
            ->andWhere(['point_id'=>$point_id])
            ->orderBy('timestamp  asc')
            ->asArray()->all();
        return json_encode($data);
    }
}
