<?php

namespace backend\module\point_table_report;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\point_table_report\controllers';

    public function init()
    {

        parent::init();
        // custom initialization code goes here
    }
}
