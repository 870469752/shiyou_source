<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/6/12
 * Time: 13:41
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<div  >
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                    >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                        <div style="float:left; margin-top:5px;">
                            <label>&nbsp;&nbsp;&nbsp;<?=$name?>&nbsp;&nbsp;&nbsp;运行时报表</label>
                        </div >
                        <div style="float:right; ">
                            <div style="float:left; ">
                                <label >开始时间</label>
                                <input type="text" id="datetimepicker1" class="datetimepicker" style="color:black">&nbsp;&nbsp;&nbsp;
                            </div>
                            <div style="float:left; ">
                                <label >结束时间</label>
                                <input type="text" id="datetimepicker2" class="datetimepicker" style="color:black">
                            </div>
                        </div>
                    </header>
                    <div id="container" style="min-width:700px;height:500px"></div>
                </div>
            </article>
        </div>
    </section>



</div>
<script>
    var yvalue=[];
    var name='';
    var option={
        chart: {
            renderTo: 'container',
            borderWidth: 0,
            borderRadius: 0,
            type: 'line',
            zoomType: 'x'
        },
        scrollbar: {
            liveRedraw: false
        },
        credits:{
            enabled: false,
        },
        rangeSelector : {
            enabled: false
        },
        title: {
            text: null
        },
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 2,
            itemStyle: {
                fontSize: '10px'
            }
        },
        tooltip:{
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            valueDecimals: 2,
            shared : true,
            style : {
                //color : 'white',
                padding : '7px',
                fontsize : '9pt'
            }
        },
        navigator : {
            adaptToUpdatedData: false,
            type: 'areaspline',
            color: '#4572A7',
            fillOpacity: 0.05,
            dataGrouping: {
                smoothed: true
            },
            lineWidth: 1,
            marker: {
                enabled: false
            }

        },
        xAxis:{
            labels:{
                enabled: false
            },
            dateTimeLabelFormats: {
                second: '%Y-%m-%d<br/>%H:%M:%S',
                minute: '%Y-%m-%d<br/>%H:%M',
                hour: '%Y-%m-%d<br/>%H:%M',
                day: '%Y<br/>%m-%d',
                week: '%Y<br/>%m-%d',
                month: '%Y-%m',
                year: '%Y'
            },
        },
        yAxis:{
            title: {
                text: 'y轴点位'
            },
            labels: {
                formatter: function() {
                    return this.value;
                }
            },
            opposite:false
        },
        series: [{
            name:'<?=$name?>',
            data:yvalue
        }]
    };
    window.onload = function() {

        highcharts_lang_date();
        $(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
        $(".datetimepicker").datetimepicker({
            format: "yyyy-mm-dd",
            todayBtn:'linked',
            minView: 'month',
            pickerPosition: "bottom-left",
            autoclose: true,
            timepicker:false    //关闭时间选项
        });
        var data=<?=json_encode($data)?>;
        if(data==null || data==''){
            yvalue=[];
        }else{
            for(var i in data){
                var a=new Array();
                a.push( data[i]['timestamp']);
                a.push( Number(data[i]['value']));
                yvalue.push( a);
            }
            //       var p=option;
            option['series'][0]['data']=yvalue;
            console.log(option);
            new Highcharts.StockChart(option);
            console.log(option);
        }
        //  option['series']=[{
        //      name:'<?=$name?>',
        //     data:yvalue
    }];
    $('#datetimepicker2').datetimepicker().on('changeDate', function(ev){
        var begintime=$('#datetimepicker1').val();
        var endtime=$('#datetimepicker2').val();
        $.ajax({
            type: "GET",
            url: "/point_table_report/crud/get-data-by-time",
            data: {'begintime':begintime,'endtime':endtime,'point_id':<?=$point_id?>},
            success: function (msg) {
                if(msg=='[]'|| msg=='' || msg==null){
                    yvalue=[];
                    $('#container').html('');
                    var pp=option;
                    pp['series'][0]['data']=yvalue;
                    new Highcharts.StockChart(pp);
                    option['series']=[{
                        name:'<?=$name?>',
                        data:yvalue
                    }];
                }else{
                    var jsondata=eval("("+msg+")");
                    yvalue=[];
                    console.log(option);
                    for(var i in jsondata){
                        var a=new Array();
                        a.push(jsondata[i]['timestamp']);
                        a.push( Number(jsondata[i]['value']));
                        yvalue.push( Number(jsondata[i]['value']));
                    }
                    $('#container').html('');
                    var op=option;
                    op['series'][0]['data']=yvalue;
                    new Highcharts.StockChart(op);
                    option['series']=[{
                        name:'<?=$name?>',
                        data:yvalue
                    }];
                }

            }
        });
    });

    }
</script>
