<?php
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
TableAsset::register ( $this );
?>

<section id="widget-grid" class="">
    <div class="well well-sm well-light">
        <div id="tabs" >
            <ul>
                <li><a href="#bacnet" id="bacnet_show">Bacnet点位</a></li>
                <li><a href="#modbus" id="modbus_show">Modbus点位</a></li>
                <li><a href="#others" id="others_show">其他点位</a></li>
            </ul>
            <div id="bacnet">
                <table id="datatable_bacnet" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_bacnet' name="checkAll_bacnet"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div id="modbus">
                <table id="datatable_modbus" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_modbus' name="checkAll_modbus"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">操作</th>
                    </tr>
                    </thead>
                </table>
            </div >
            <div id="others">
                <table id="datatable_others" class="table table-striped table-bordered table-hover" style="width:100%;">
                    <thead>
                    <tr>
                        <th data-class="expand" style="width:15px;"><input type="checkbox" id='checkAll_others' name="checkAll_others"></th>
                        <th data-class="expand">ID</th>
                        <th data-class="expand">中文名</th>
                        <th data-class="expand">英文名</th>
                        <th data-class="expand">操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</section>


<script>

    window.onload = function() {
        var oTable_others; var oTable_bacnet; var oTable_modbus;
        var temp_others = 0; var temp_bacnet = 0; var temp_modbus = 0;

        $("#bacnet_show").click(function(){
            if(!temp_bacnet){
                oTable_bacnet = initTable('datatable_bacnet','oTable_bacnet','tool_bacnet',1,'point');
                temp_bacnet = 1;
            }
        })

        $("#modbus_show").click(function(){
            if(!temp_modbus){
                oTable_modbus = initTable('datatable_modbus','oTable_modbus','tool_modbus',2,'point');
                temp_modbus = 1;
            }
        })


        $("#others_show").click(function(){
            if(!temp_others){
                oTable_others = initTable('datatable_others','oTable_others','tool_others',0,'point');
                temp_others = 1;
            }
        })



        $(document).ready(function () {
            $('#tabs').tabs({
                activate: function(event, ui) {
                    ttInstances = TableTools.fnGetMasters();
                    for (i in ttInstances) {
                        if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                    }
                }
            });
            setTimeout(function(){$("#<?=$active ?>").click();},1000);

        });

        /**
         * 初始化表格数据
         * oTable  表格对象
         * name 表格ID
         * tool 初始化表格的编辑栏ID
         * protocol_id 数据类型 {1: "bacnet", 2: "modbus", 3: "coologic", 4: "calculate", 5: "event", 6: "upload", 7: "simulate",9: "Camera", 100: "demo"}
         * type 判断搜索的内容类型{point：“正常非屏蔽点位”，reve:"回收站点位"}
         */
        function initTable(name,oTable,tool,protocol_id,type) {
            /* TABLETOOLS */
            table = $('#'+name).dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4 "+tool+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
                //"sdom": "Bfrtip",
                "oTableTools": {
                    "aButtons": [
                    ],
                    "sRowSelect": "os",
                    "sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                "lengthMenu": [[14,15, 16, 20, 25, 30, -1], [14,15, 16, 20, 25, 30, "All"]],
                "iDisplayLength": 14,
                //"bSort": false,
                "language": {
                    "sProcessing": "处理中...",
                    "sClear":"test",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                    "sInfoPostFix": "",
                    "sSearch": "搜索:",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    },
                    "oAria": {
                        "sSortAscending": ": 以升序排列此列",
                        "sSortDescending": ": 以降序排列此列"
                    }
                },
                "processing": false,
                "serverSide": true,
                'bPaginate': true,
                "bDestory": true,
                "bRetrieve": true,
                'bStateSave': true,
                'bFilter':true,
                "ajax": {
                    "url": "/point_table_report/crud/get-data?type="+type+"&protocol_id="+protocol_id,
                    "type": "post",
                    "error": function () {
                        alert("服务器未正常响应，请重试");
                    }
                },
                "aoColumns": [
                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<input type='checkbox' name='checkList_"+name+"' value='" + sData + "' >");
                        }
                    },
                    {"mDataProp": "id"},
                    {"mDataProp": "cn"},
                    {"mDataProp": "us"},

                    {
                        "mDataProp": "id",
                        "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/point_table_report/crud/doexport?point_id="+sData+"'>报表查看</a>"+"<br>"+"<br>"+"<a href='/point_table_report/crud/graphic?point_id="+sData+"'>图形查看</a>");
                        }
                    }
                ],
                "fnInitComplete": function (oSettings, json) {
                },
                "order": [1, 'asc']
            });
            /* END TABLETOOLS */
            return table;
        }

        $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
            this.fnClearTable(this);
            this.oApi._fnProcessingDisplay(oSettings, true);
            var that = this;

            $.getJSON(oSettings.sAjaxSource, null, function (json) {
                /* Got the data - add it to the table */
                for (var i = 0; i < json.aaData.length; i++) {
                    that.oApi._fnAddData(oSettings, json.aaData[i]);
                }
                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                that.fnDraw(that);
                that.oApi._fnProcessingDisplay(oSettings, false);
            });
        }


    }

</script>
