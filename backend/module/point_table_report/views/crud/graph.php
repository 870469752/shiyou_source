<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/6/12
 * Time: 13:41
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

?>
<div  >
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false"
                    >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                        <div style="float:left; margin-top:5px;">
                            <label>&nbsp;&nbsp;&nbsp;<?=$name?>&nbsp;&nbsp;&nbsp;运行时报表</label>
                        </div >
                        <div style="float:right; ">
                            <div style="float:left; ">
                                <label >开始时间</label>
                                <input type="text" id="datetimepicker1" class="datetimepicker" style="color:black">&nbsp;&nbsp;&nbsp;
                            </div>
                            <div style="float:left; ">
                                <label >结束时间</label>
                                <input type="text" id="datetimepicker2" class="datetimepicker" style="color:black">
                            </div>
                        </div>
                    </header>
                    <div id="container" style="min-width:700px;height:500px"></div>
                </div>
            </article>
        </div>
    </section>



</div>
<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    var yvalue=[];
    var xvalue=[];
    var name='';
    var option={
        chart: {
            type: 'line',
            zoomType:'x'

        },
        credits:
        {
            enabled:false//不显示highCharts版权信息
        },
        title: {
            text:null
        },
        scrollbar: {
            enabled: true
        },
        xAxis: {
          //  tickInterval:20,
            categories: xvalue,
            tickmarkPlacement:'on',
            title:{
                text:'时间戳'
            },
            labels:{
                enabled: false
            },
           type:'datetime'
        },
        yAxis: {
            title: {
                text: 'y轴点位值'
            },
          //  x:10,
            type: 'linear',
            labels: {
                formatter:function(){
                    return this.value;
                }
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                //    enabled: true
                },
                enableMouseTracking: true
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            valueDecimals: 2,
            shared : true,
            style : {
                //color : 'white',
                padding : '7px',
                fontsize : '9pt'
            }
        },
        series: [{
            name:'<?=$name?>',
            data:yvalue
        }]
    };
    window.onload = function() {
        Highcharts.setOptions({
            lang: {
                printChart:"打印图表",
                downloadJPEG: "下载JPEG 图片" ,
                downloadPDF: "下载PDF文档"  ,
                downloadPNG: "下载PNG 图片"  ,
                downloadSVG: "下载SVG 矢量图" ,
                exportButtonTitle: "导出图片"
            }
        });
        $(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
        $(".datetimepicker").datetimepicker({
            format: "yyyy-mm-dd",
            todayBtn:'linked',
             minView: 'month',
            pickerPosition: "bottom-left",
            autoclose: true,
            timepicker:false    //关闭时间选项
        });
        var data=<?=json_encode($data)?>;
        if(data==null || data=='' ||data =='[]'){
            yvalue=[];
            xvalue=[];
        }else{
            for(var i in data){
                xvalue.push(data[i]['timestamp']);
                yvalue.push( Number(data[i]['value']));
            }
           $('#container').html('');
            option['xAxis']['categories']=xvalue;
            option['series'][0]['data']=yvalue;
        }
        $('#container').highcharts(option);
        $('#datetimepicker2').datetimepicker().on('changeDate', function(ev){
            var begintime=$('#datetimepicker1').val();
            var endtime=$('#datetimepicker2').val();
            $.ajax({
                type: "GET",
                url: "/point_table_report/crud/get-data-by-time",
                data: {'begintime':begintime,'endtime':endtime,'point_id':<?=$point_id?>},
                success: function (msg) {
                    if(msg=='[]'|| msg=='' || msg==null){
                        yvalue=[];
                        xvalue=[];
                        $('#container').html('');
                        option['xAxis']['categories']=xvalue;
                        option['series'][0]['data']=yvalue;
                        $('#container').highcharts(option);
                    }else{
                        var jsondata=eval("("+msg+")");
                        xvalue=[];
                        yvalue=[];
                        for(var i in jsondata){
                            xvalue.push(jsondata[i]['timestamp']);
                            yvalue.push( Number(jsondata[i]['value']));
                        }
                        $('#container').html('');
                        option['xAxis']['categories']=xvalue;
                        option['series'][0]['data']=yvalue;
                        $('#container').highcharts(option);
                    }

                }
            });
        });

    }
</script>
