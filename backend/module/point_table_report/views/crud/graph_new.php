<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/6/12
 * Time: 13:41
 */
use backend\assets\TableAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

TableAsset::register($this);
$this->title = '点位曲线图';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <div style="float:left; margin-top:5px;;height:100%">
                        <label style="height:100%">&nbsp;&nbsp;&nbsp;<?= $name ?></label>
                    </div>
                    <div style="float:right; ;height:100%">
                        <div style="float:left; ;height:100%">
                            <label style="height:100%">开始时间</label>
                            <input type="text" id="datetimepicker1" class="datetimepicker"
                                   style="color:black;height:100%">&nbsp;&nbsp;&nbsp;
                        </div>
                        <div style="float:left; ;height:100%">
                            <label style="height:100%">结束时间</label>
                            <input type="text" id="datetimepicker2" class="datetimepicker"
                                   style="color:black;height:100%">
                        </div>
                    </div>
                </header>
                <div id="container1" style="height:500px;float:left;"></div>
            </div>
        </article>
    </div>
</section>

<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/theme/black_theme.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    var yvalue = [];
    var name = '';
    var option = {
        chart: {
            renderTo: 'container1',
            borderWidth: 0,
            borderRadius: 0,
            type: 'spline',
            zoomType: 'x'
        },
        credits: {
            enabled: false,
        },
        rangeSelector: {
            buttons: [{
                type: 'hour',
                count: 1,
                text: '1h'
            }, {
                type: 'day',
                count: 1,
                text: '1d'
            }, {
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'year',
                count: 1,
                text: '1y'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: true, // it supports only days
            selected: 4 // all
        },
        title: {
            text: null
        },
        //标题按钮
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'top',
            borderWidth: 2,
            itemStyle: {
                fontSize: '10px'
            }
        },
        xAxis: {
            labels: {
                enabled: true
            },
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: 'y轴点位'
            },
            minorTickInterval: 'auto',
            lineColor: '#000',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#000',
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            opposite: false
        },
        //y轴网格线
        plotOptions: {
            column: {
                dataLabels: {
                    rotation: -90,
                    enabled: true,
                    align: 'right',
                    x: 4,
                    y: -45,
                    //        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',

                }
            }
        },
        series: [
            {
                name: '<?=$name?>',
                data: yvalue,
                marker: {
                    enabled: true,
                    radius: 2
                },
                shadow: false,
                tooltip: {
                    valueDecimals: 2
                }
            }
        ]
    };
    window.onload = function () {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        var chart = new Highcharts.StockChart(option);
        $(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
        $(".datetimepicker").datetimepicker({
            format: "yyyy-mm-dd",
            todayBtn: 'linked',
            minView: 'month',
            pickerPosition: "bottom-left",
            autoclose: true,
            timepicker: false
        });
        var data =<?=json_encode($data)?>;
        if (data == null || data == '') {
            yvalue = [];
            chart.showLoading('该时间段内没有数据！');
            chart.series[0].setData(yvalue);
        } else {
            for (var i in data) {
                var a = new Array();
                a.push(Date.parse(data[i]['timestamp']));
                a.push(Number(data[i]['value']));
                yvalue.push(a);
            }
            chart.series[0].setData(yvalue);
        }
        $('#datetimepicker2').datetimepicker().on('changeDate', function (ev) {
            var begintime = $('#datetimepicker1').val();
            var endtime = $('#datetimepicker2').val();
            if (begintime > endtime) {
                alert("开始时间不能大于结束时间！");
                return;
            }
            chart.showLoading('Loading data from server...');
            $.ajax({
                type: "GET",
                url: "/point_table_report/crud/get-data-by-time",
                data: {'begintime': begintime, 'endtime': endtime, 'point_id':<?=$point_id?>},
                success: function (msg) {
                    if (msg == '[]' || msg == '' || msg == null) {
                        yvalue = [];
                        chart.series[0].setData(yvalue);
                        chart.showLoading('该时间段内没有数据！');
                    } else {
                        var jsondata = eval("(" + msg + ")");
                        yvalue = [];
                        for (var i in jsondata) {
                            var a = new Array();
                            a.push(Date.parse(jsondata[i]['timestamp']));
                            a.push(Number(jsondata[i]['value']));
                            yvalue.push(a);
                        }
                        console.log(yvalue);
                        console.log(chart);
                        chart.series[0].setData(yvalue);
                        chart.hideLoading();
                    }
                }
            });
        });
    }
    function afterSetExtremes(e) {
    }
</script>
