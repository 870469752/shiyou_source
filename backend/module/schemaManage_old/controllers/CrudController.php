<?php

namespace backend\module\schemaManage\controllers;

use Yii;
use backend\models\schemaManage;
use backend\models\search\schemaManageSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;

/**
 * CrudController implements the CRUD actions for schemaManage model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all schemaManage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new schemaManageSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single schemaManage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new schemaManage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new schemaManage();
        if($data = Yii::$app->request->post()){
            $detail=json_decode($data['detail'],1);
            $merge=$data['config'];
            $arr = explode("&&",$merge);
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$arr[0],
                    'en-cn'=>null
                )
            );
            $model->name=json_encode($name);
            $config=array(
                'data_type'=>'time_mode',
                'data'=>array(
                    'type'=>$arr[1],
                    'range'=>array()
                )
            );
            //遍历时间
            for($i=0;$i<count($detail);$i++)
            {
                $row=array(
                    '_id'=>$detail[$i][0],
                    'title'=>$detail[$i][1],
                    'start'=>$detail[$i][2],
                    'end'=>$detail[$i][3],
                    'allDay'=>$detail[$i][4],
                    'description'=>$detail[$i][5],
                    'className'=>array($detail[$i][6][0],$detail[$i][6][1]),
                    'row'=>$detail[$i][7],
                    'category'=>$detail[$i][8],
                    'specical'=>$detail[$i][9],
                );
                $config['data']['range'][$i]=$row;
            }

            $model->data=json_encode($config);
            if (!$model->save()) {
                var_dump($model->errors);die;
            }else{
                $model->save();
            }
            return $this->redirect(['index']);
        }else{
            return $this->render('create',[
                'model' => $model,
            ]);
        }
    }
    public function actionCreate1()
    {
        $model = new schemaManage;
        $data=Yii::$app->request->post();
        if($data!=null){
            $title=$data['name'];
            $namearr=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$title,
                    'en-cn'=>null
                )
            );
            $mode_data=array(
                'mode'=>array(
                    'month'=>json_decode($data['month'],1),
                    'week'=>json_decode($data['week'],1),
                    'day'=>json_decode($data['day'],1),
                    'week_day'=>json_decode($data['week_day'],1),
                )
            );
            $model->name=json_encode($namearr);
            $model->time_mode= MyFunc::createJson($mode_data, 'time_mode');

            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelarr'=>null,
                'type'=>'create',
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelarr = schemaManage::find()->where(['id'=>$id])->asArray()->one();
        if($data = Yii::$app->request->post()){
            $detail=json_decode($data['detail'],1);
            $merge=$data['config'];
            $arr = explode("&&",$merge);
            $name=array(
                'data_type'=>'description',
                'data'=>array(
                    'zh-cn'=>$arr[0],
                    'en-cn'=>null
                )
            );
            $model->name=json_encode($name);
            $config=array(
                'data_type'=>'time_mode',
                'data'=>array(
                    'type'=>$arr[1],
                    'range'=>array()
                )
            );
            //遍历时间
            for($i=0;$i<count($detail);$i++)
            {
                $row=array(
                    '_id'=>$detail[$i][0],
                    'title'=>$detail[$i][1],
                    'start'=>$detail[$i][2],
                    'end'=>$detail[$i][3],
                    'allDay'=>$detail[$i][4],
                    'description'=>$detail[$i][5],
                    'className'=>array($detail[$i][6][0],$detail[$i][6][1]),
                    'row'=>$detail[$i][7],
                    'category'=>$detail[$i][8],
                    'specical'=>$detail[$i][9],
                );
                $config['data']['range'][$i]=$row;
            }

            $model->data=json_encode($config);
            if (!$model->save()) {
                var_dump($model->errors);die;
            }else{
                $model->save();
            }
            return $this->redirect(['index']);
        }else{
            return $this->render('update', [
                'model' => $model,
                'modelarr'=>$modelarr,
            ]);
        }
    }
    /**
     * Updates an existing schemaManage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate1($id)
    {
        $modelarr = schemaManage::find()->where(['id'=>$id])->asArray()->all();
        $model = $this->findModel($id);
        $data=Yii::$app->request->post();
       if (Yii::$app->request->post()) {
           $mode_data=array(
               'mode'=>array(
                   'month'=>json_decode($data['month'],1),
                   'week'=>json_decode($data['week'],1),
                   'day'=>json_decode($data['day'],1),
                   'week_day'=>json_decode($data['week_day'],1),
               )
           );
           $title=$data['name'];
           $namearr=array(
               'data_type'=>'description',
               'data'=>array(
                   'zh-cn'=>$title,
                   'en-cn'=>null
               )
           );
           $model->name=json_encode($namearr);
           $model->time_mode= MyFunc::createJson($mode_data, 'time_mode');
           $model->save();
           return $this->redirect(['index']);
       //    return $this->redirect(['view', 'id' => $model->id]);
       } else {
            return $this->render('update', [
                'model' => $model,
                'modelarr'=>$modelarr,
                'type'=>'update',
            ]);
        }
    }

    /**
     * Deletes an existing schemaManage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the schemaManage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return schemaManage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = schemaManage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
