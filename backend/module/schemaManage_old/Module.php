<?php

namespace backend\module\schemaManage;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\schemaManage\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
