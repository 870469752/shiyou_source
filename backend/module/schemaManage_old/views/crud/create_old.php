<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 */
$this->title = '创建时间模式';
?>
    <?= $this->render('_form', [
    'model' => $model,
    'modelarr'=>$modelarr,
    'type'=>'create',
    ]) ?>
