<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\schemaManage $model
 */

$this->title = 'Update Schema Manage: ' . $model->id;
?>
    <?= $this->render('_form', [
    'model' => $model,
    'modelarr'=>$modelarr,
    'type'=>'update',
    ]) ?>
