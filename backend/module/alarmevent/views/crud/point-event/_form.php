<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;
use backend\models\Category;
/**
 * @var yii\web\View $this
 * @var backend\models\AtomEvent $model
 * @var yii\widgets\ActiveForm $form
 */

?>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">
        <div id="tabs"  class="  ui-tabs ui-widget ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                <li class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                    <?=Html::a(Yii::t('app', 'Atom Event'), ['crud/create','type' => 1], ['class' => 'fa fa-sliders'])?>
                </li>
                <li>
                    <?=Html::a(Yii::t('app', 'Compound Event'), ['crud/create','type' => 2], ['class' => 'fa fa-slack'])?>
                </li>
            </ul>
        </div>
      </div>
        <hr />
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>

                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

                            <?= $form->field($model, 'point_id')->dropDownList(
                                MyFunc::_map(Point::getAvailablePoint(), 'id', 'name'),
                                ['class' => 'select2', 'style' => 'width:200px']
                            ); ?>

                            <?= $form->field($model, 'time_type')->dropDownList(['year' => Yii::t('app', 'Repeat/Year'),
                                                                                 'month' => Yii::t('app', 'Repeat/Month'),
                                                                                  'week' => Yii::t('app', 'Repeat/Week'),
                                                                                 'day' => Yii::t('app', 'Repeat/Day'),
                                                                                 'custom' => Yii::t('app', 'Custom')], [ 'class'=>'select2','style' => 'width:200px']); ?>

                            <?= Html::hiddenInput("PointEvent[time_range]"); ?>

                            <?= Html::hiddenInput("PointEvent[is_system]", 0)?>
                            <!--开始 日历插件-->
                            <div class="jarviswidget jarviswidget-color-magenta"
                                 data-widget-deletebutton="false"
                                 data-widget-editbutton="false"
                                 data-widget-colorbutton="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                                    <h2> <?= Yii::t('app', 'Select Time')?> </h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <div class="widget-body no-padding">
                                        <!-- content goes here -->
                                        <div class="widget-body-toolbar">
                                            <input type="text" style = "width:110px;display:none" id ="timepick" name="mydate" readonly placeholder="Select a date" class="form_datetime"/>

                                            <div id="calendar-buttons">
                                                    <!-- add: non-hidden - to disable auto hide -->
                                                    <div class="btn-group" id = 'select_btn'>
                                                        <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                                            Showing <i class="fa fa-caret-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu js-status-update pull-right">
                                                            <li>
                                                                <a href="javascript:void(0);" id="mt">Month</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);" id="ag">Agenda</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);" id="td">Today</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <div class="btn-group">
                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                       id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"
                                                       id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="calendar"></div>
                                        <!-- end content -->
                                    </div>
                                </div>

                                <!-- end widget div -->
                            </div>
                            <!--结束  日历插件-->
                            <?= $form->field($model, 'trigger')->textInput(['id' => 'range-slider']) ?>
                            <!--time-->
                            <div id = '_cron' class = 'time_mode' style="height: 160px">
                                <lable><?=Yii::t('app', 'Backup Date')?> :</lable>
                                <span id='selector'></span>
                            </div>
                            <!--timeend-->
                            <!-- 当前页面为原子事件 type 为 1-->
                            <?= Html::hiddenInput("PtomEvent[type]", 1) ?>

                        </fieldset>
                        <div class="form-actions">
                            <?=
                            Html::submitButton(
                                $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => '_create']
                            ) ?>
                        </div>



                        <?php
                        ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>

<?php
$this->registerCssFile("css/grumble/grumble.min.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);

$this->registerJsFile("js/plugin/ion-slider/ion.rangeSlider.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/qtip/jquery.qtip-1.0.0-rc3.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/bootbox/bootbox.js", ['yii\web\JqueryAsset']);
//$this->registerJsFile("js/my_js/calendar_1.js", ['yii\web\JqueryAsset']);

//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
?>

<script>

window.onload = function () {


    var cron = $('#selector').cron({
        initial: '* * * * *',
        onChange: function () {
            $('#interval_time').val($(this).cron("value"))
        },
        useGentleSelect: true
    });
    var date = new Date();
    var i = 1;
    /*添加一个 时间事件*/
    function addEvent(event, end)
    {
        $('#calendar').fullCalendar( 'addEventSource', [{
            title: '添加一个时间事件',
            start: event,
            end: end,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])
    }

    /*添加一个 时间事件*/
    function addEventByDay(event, end)
    {
        $('#calendar').fullCalendar( 'addEventSource', [{
            title: '添加一个时间事件',
            start: event,
            end: end,
            allDay:false,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])

    }

    /*删除一个 时间事件*/
    function removeEvent(calEvent)
    {
        $('#calendar').fullCalendar('removeEvents',calEvent._id);
        $('.qtip-content').parent().remove();
    }

    /*时间操作提示层*/
    function hintEvent(event, cal_arr, revertFunc, start, end)
    {
        bootbox.dialog({
            message: "事件发生冲突或超出范围，请选择操作：",
            title: "冲突操作",
            buttons: {
                success: {
                    label: "处理(合并)事件",
                    className: "btn-success",
                    callback: function() {
                        removeEvent(event);
                        $.each(cal_arr, function (k, v) {
                            removeEvent(v);
                        })
                        //如果含有 小时或分钟 表明该日历view不是month 需要禁用 allDay
                        if(start.getHours() || start.getMinutes()){
                            addEventByDay(start, end);
                        }
                        else{
                            addEvent(start, end);
                        }

                    }

                },
                danger: {
                    label: "返回上次设置",
                    className: "btn-danger",
                    callback: function() {
                        revertFunc();

                    }
                },
                main: {
                    label: "删除事件",
                    className: "btn-primary",
                    callback: function() {
                        removeEvent(event);
                    }
                }
            }
        });
        $('.close').css('display','none');
    }

    /*判断时间是否有冲突 -- 判断较多..需要理下思路再看*/
    function conflictEvent(event, revertFunc)
    {
        var arr_obj = [];
        var cl_start = event.start;
        var cl_end = event.end;
        var _date = $("#calendar").fullCalendar('getDate');
        var cur_month = _date.getMonth();
        var cur_year = _date.getFullYear();
        var cur_month_day = new Date(cur_year, cur_month+1, 0).getDate();
        if(event.end == null){
            if(event.start.getMonth() != cur_month){
//                revertFunc();
                cl_start = new Date(cur_year, cur_month, cur_month_day);
                // hintEvent(event, event,  revertFunc, _start, event.end);

            }
        }
        else if(event.end.getMonth() != cur_month){
            cl_start =  new Date(cur_year, cur_month, event.start.getDate()- event.end.getDate());
            if(event.start.getMonth() != cur_month)
            {
                cl_start = new Date(cur_year, cur_month, cur_month_day- (event.end.getDate()- event.start.getDate()))
            }
//            revertFunc();
//			event.start = _start;
            cl_end = new Date(cur_year, cur_month, cur_month_day);
            // hintEvent(event, event,  revertFunc, _start, _end);
        }

        /*********************************************** 冲突判断 判断 **************************************************************************/

        $('#calendar').fullCalendar('clientEvents', function ( cal )
        {
            if(event._id != cal._id)
            {
                var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-mm");
                var event_start = $.fullCalendar.formatDate(cl_start, "yyyy-MM-dd-HH-mm");
                var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-mm");
                var event_end =  $.fullCalendar.formatDate(cl_end, "yyyy-MM-dd-HH-mm");
                var view = $('#calendar').fullCalendar('getView');
                console.log(view.name)   //获取当前日历 格式
                console.log('event_start:'+event_start+',cal_start:'+cal_start);
                console.log('event_end'+event_end+',cal_end'+cal_end)
                //改为半开区间event_start < cal_end
                if( cal_start <= event_start &&  event_start < cal_end )
                {
//                    console.log(1)
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start){}
                    cl_start = cal.start;
                    if(event_end <= cal_end){
                        if(cl_end == '' || cal.end >= cl_end)
                            cl_end = cal.end;
//							hintEvent(event, cal,  revertFunc, cal.start, cal.end);
                    }
                    else
                    {
                        cl_end = cl_end;
//							hintEvent(event, cal,  revertFunc, cal.start, event.end);
                    }
                }
                //改为半开区间cal_start < event_end
                else if(cal_start < event_end && event_end <= cal_end)
                {
//                    console.log(2)
                    arr_obj.push(cal);
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
                    if(event_start <= cal_start)
                    {
                        cl_start = cl_start
//							hintEvent(event, cal, revertFunc, event.start, cal.end);
                    }
                    else
                    {
                        if(cl_start == '' || cal.start <= cl_start)
                            cl_start = cal.start
//							hintEvent(event, cal, revertFunc, cal.start, cal.end);
                    }

                }
                else if(view.name == 'month' && event_start <= cal_start && event_end >= cal_start ||
                    view.name != 'month' && event_start <= cal_start && event_end > cal_start)
                {
//                    console.log(3)
                    arr_obj.push(cal);
                    cl_start = cl_start;
                    cl_end = cl_end;
//						hintEvent(event, cal, revertFunc, event.start, event.end);
                }
                else if(event_end == '' && (cal_start <= event_start && event_start <= cal_end) || cal_start == event_start)
                {
//                    console.log(4)
//                    console.log(event_end + 'cre')
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start)
                        cl_start = cal.start;
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
//						hintEvent(event, cal, revertFunc, cal.start, cal.end);
                }
            }
            $('.qtip-content').parent().remove();
        });
//        console.log(cl_start +'--'+cl_end)
        if(cl_start != event.start || arr_obj.length){
            hintEvent(event, arr_obj, revertFunc, cl_start, cl_end);
        }
    }

    /*********************************************** 结束 判断 **************************************************************************/

    /*日历插件主体*/
    $('#calendar').fullCalendar (
        {
            editable: true,
            selectable:false,
            firstDay:1,
            weekNumbers:true,
            contentHeight: 300,
            slotEventOverlap:false,
            weekMode: 'liquid',
            header:{
                left: '',
                center: 'title',
                right: ''
            },
            columnFormat:{
                //  month: '-',
                week: "ddd",
                day: 'Day',
                default:false
            },

//            axisFormat: 'H:mm',
//            allDayText:'全天',
//            header:{
//                left: '',
//                center: 'title',
//                right: ''
//            },
//            titleFormat: {
//                month: 'yyyy' + '年' + 'MMMM',
//                week: "MMM d[ yyyy]{'&#8212:'[ MM] d yyyy}",
//                day: 'dddd, MMM d, yyyy'
//            },
//            columnFormat:{
//                month: 'ddd',
//                week: "ddd",
//                day: '天'
//            },
//            timeFormat:{'': 'h(:mm)t'     },
//            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
//            dayNamesShort:["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
            //

            /*点击时  添加一个时间事件*/
            dayClick: function ( date, allDay, jsEvent, view )
            {
                if(date.getMonth() !== view.start.getMonth())
                {
                    return false;
                }
                else{
                    $('#calendar').fullCalendar('clientEvents', function ( cal )
                    {
                        var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-ss");
                        var event_start = $.fullCalendar.formatDate(date, "yyyy-MM-dd-HH-ss");
                        var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-ss");
                        if(cal_start == event_start || cal_start <= event_start && event_start <= cal_end)
                        {
                            i = i + 1;
                            //最后才会 弹出提示框
                            bootbox.alert("时间事件有冲突，请重新添加！", function() {
                                i = 1;
                            });
                            $('.close').css('display','none');
                        }
                    })
                    if(i == 1){
                        i = 1;
                        var view = $('#calendar').fullCalendar('getView');
//                        console.log( date.getMilliseconds())
                        if(date.getHours() || date.getMilliseconds()){
                            addEventByDay(date);

                        }
                        else{
                            addEvent(date);
                        }
                    }
                }
            },

            /* 监听 拖动事件  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc)
            {
                var view = $('#calendar').fullCalendar('getView');
                if(view.name != 'month' && allDay){
                    revertFunc();
                }
                conflictEvent(event, revertFunc);
            },

            /* 监听 改变时间块儿的大小  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventResize: function(event, dayDelta, minuteDelta, revertFunc)
            {
                conflictEvent(event, revertFunc);
            },


            /*友情 提示 事件*/
//            eventRender: function ( event, element, view )
//            {
//                if(view.name == 'month'){
//                    element.qtip({
//                        content: event.description
//                    });
//                }
//            },
            /*监听鼠标点击事件 ：1、点击对应的事件 将会被删除；2、消除提示框*/
            eventClick: function ( calEvent, jsEvent, view )
            {
                removeEvent(calEvent);
            },
            /*渲染*/
            windowResize: function ( view )
            {
                $('#calendar').fullCalendar('render');
                var view = $('#calendar').fullCalendar('getView');
//                console.log(view)
            }
        });

    /**************************************************************************************************************/

    console.log(321);
    var _obj = ' ';
    var cur_from = <?= isset($model->trigger) ? substr($model->trigger, 0, strpos($model->trigger, ',')) : -1;?>;
    var cur_to = <?= isset($model->trigger) ? substr($model->trigger, strpos($model->trigger, ',') + 1) : 8;?>;
    var cur_min = parseInt(cur_from/10)*10 - 10;
    var cur_max = parseInt(cur_to/10)*10 + 10;
    //添加一个 触发数值伸缩条 关联的 input 框
    $("<input id = 'min' style = 'width:6%;margin-left:10px' type = 'text' > - <input id = 'max' style = 'width:6%' type = 'text' >").insertAfter($('.field-pointevent-trigger label'))
    //给 input框赋默认值
    $("#min").val(cur_from);
    $("#max").val(cur_to);

    $("#range-slider").ionRangeSlider({
        min: cur_min,
        max: cur_max,
        type: "double",
        step: 0.5,
        from: cur_from,
        to: cur_to,
        hasGrid: true,
        onChange: function (obj) {
            _obj = obj;
            if (obj.min > -1000 && obj.max < 1000) {
                if (obj.fromNumber == obj.min) {
                    $("#range-slider").ionRangeSlider("update", {
                        min: obj.min - 10,                        // change min value
                        max: obj.max,                        // change max value
                        from: obj.fromNumber,                       // change default FROM setting
                        to: obj.toNumber,                         // change default TO setting
                        step: 0.5                        // change slider step
                    });
                }
                else if (obj.fromNumber - 10 > obj.min) {
                    $("#range-slider").ionRangeSlider("update", {
                        min: obj.min + 10,                        // change min value
                        max: obj.max,                        // change max value
                        from: obj.fromNumber,                       // change default FROM setting
                        to: obj.toNumber,                         // change default TO setting
                        step: 0.5                        // change slider step
                    });
                }
                if (obj.toNumber == obj.max) {
                    $("#range-slider").ionRangeSlider("update", {
                        min: obj.min,                        // change min value
                        max: obj.max + 10,                        // change max value
                        from: obj.fromNumber,                       // change default FROM setting
                        to: obj.toNumber,                         // change default TO setting
                        step: 0.5                        // change slider step
                    });
                }
                else if (obj.toNumber + 10 < obj.max) {
                    $("#range-slider").ionRangeSlider("update", {
                        min: obj.min,                        // change min value
                        max: obj.max - 10,                        // change max value
                        from: obj.fromNumber,                       // change default FROM setting
                        to: obj.toNumber,                         // change default TO setting
                        step: 0.5                        // change slider step
                    });
                }
                $("#min").val(obj.fromNumber);
                $("#max").val(obj.toNumber);
            }
        },
        onLoad: function (obj) {        // callback is called after slider load and update

            _obj = obj;
        }
    });

    //给 数值范围inout框 添加 失去焦点事件
    $("#min").blur(function(){
        var _min = $(this).val();
        var _max = $("#max").val();
        if(_min - _max < 0)
        {
            $("#range-slider").ionRangeSlider("update", {
                min: Math.floor(_min/10)*10,                        // change min value
                max: Math.ceil(_max/10)*10,                        // change max value
                from: _min,                       // change default FROM setting
                to: _max,                         // change default TO setting
                step: 0.5                        // change slider step
            })
        }
    })

    $("#max").blur(function(){
        var _max = $(this).val();
        var _min = $("#min").val();
        if(_min - _max < 0)
        {
            $("#range-slider").ionRangeSlider("update", {
                min: Math.floor(_min/10)*10,                        // change min value
                max: Math.ceil(_max/10)*10,                        // change max value
                from: _min,                       // change default FROM setting
                to: _max,                         // change default TO setting
                step: 0.5                        // change slider step
            })
        }
    })

    $('.jarviswidget-ctrls').eq(1).hide()
    $('.widget-toolbar').hide()
    $(document).keydown(function (e) {
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) {
            code = e.keyCode;
        } else if (e.which) {
            code = e.which;
        }

        if (code == 37) {
            $("#range-slider").ionRangeSlider("update", {
                min: _obj.min,                        // change min value
                max: _obj.max,                        // change max value
                from: _obj.fromNumber - 0.5,                       // change default FROM setting
                to: _obj.toNumber,                         // change default TO setting
                step: 0.5                        // change slider step
            });
        } else if (code == 39) {
            $("#range-slider").ionRangeSlider("update", {
                min: _obj.min,                        // change min value
                max: _obj.max,                        // change max value
                from: _obj.fromNumber,                       // change default FROM setting
                to: _obj.toNumber + 0.5,                         // change default TO setting
                step: 0.5                        // change slider step
            });
        }
    });


    var date = new Date();
    /*                                                                 数值选择js*/
    $("#range-slider").ionRangeSlider({
        min: -100,
        max: 100,
        from: 1,
        to: 10,
        type: 'double',
        step: 0.5,
        prettify: false,
        hasGrid: true
    });
    var str = '';
    var up_type = 'year';
    var _data = '';
    /*初始化日历事件  获取所有event 并展示到日历上*/
    <?php if ($time_range = $model->original_time): ?>
    str = <?=$time_range;?>;
    $.each(str, function (key, value) {
        if (key == 'data') {
            $.each(value, function (k1, v1) {
                if (k1 == 'type') {
                    up_type = v1;
                }
                else if (k1 == 'time_range') {
                    _data = '['
                    $.each(v1, function (k2, v2) {
                        var curent_day = new Date(v2['start']);
                        _data += '{';
                        _data += "start:$.fullCalendar.parseDate('" + v2['start'] + "') ,";
                        _data += "end:$.fullCalendar.parseDate('" + v2['end'] + "')  ,";
                        _data += "title:<?="'".str_replace(' ','',$model->name)."'"?>,";
                        _data += 'description:' + "'<span><?=Yii::t('app', 'Prompt')?></span>:<li><?=Yii::t('app', 'Click delete this time')?></li><li><?=Yii::t('app', 'Drag the right margin change the time')?></li>',";
                        _data += 'className:' + "['event', 'bg-color-darken'],";
                        if(curent_day.getHours() || curent_day.getMinutes()){
                            _data += 'allDay: false';
                        }
                        _data += '},';
                    })

                }
            })
        }

    })
    _data = _data.substring(0, _data.length - 1);
    _data += ']';

    $('#calendar').fullCalendar('addEventSource', eval("(" + _data + ")"));
    <?php endif;?>
    /*                                                                                      结束*/


    /*添加自定义按钮 点击事件*/
    $('#calendar-buttons #btn-prev').click(function () {
        $('#calendar').fullCalendar('prev');
        return false;
    });
    $('#calendar-buttons #btn-next').click(function () {
        $('#calendar').fullCalendar('next');
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });
    /* 隐藏 日历切换按钮 */
    function hide_buttons() {
        $('.fc-header-right, .fc-header-center').hide();
        $('#calendar-buttons #btn-prev').hide();
        $('#calendar-buttons #btn-next').hide();
        $('#timepick').hide();
    }

    /* 显示 日历切换按钮 */
    function show_buttons() {
        $('.fc-header-right, .fc-header-center').show();
        $('#calendar-buttons #btn-prev').show();
        $('#calendar-buttons #btn-next').show();
        $('#timepick').show();
    }


    /*                                                                                 对默认类型 下拉菜单 进行监听*/
    $('#pointevent-time_type').change(function () {
        var _type = $(this).val();
        $('#select_btn').hide();
        if (up_type != _type) {
            $('#calendar').fullCalendar('removeEvents');
        }
        if (_type == 'year') {
            timeTypeChange('month');
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('today');
            show_buttons();
        }
        else if (_type == 'month') {
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if (_type == 'week') {

            $('#calendar').fullCalendar('changeView', 'agendaWeek');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if (_type == 'day') {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('gotoDate', $.fullCalendar.parseDate('2008-09-01'));
            hide_buttons();
        }
        else if(_type == 'custom') {
            $('#calendar').fullCalendar('changeView', 'month');
            $('#calendar').fullCalendar('rerenderEvents');
            $('#calendar').fullCalendar('today');
            show_buttons();
            $('#select_btn').show();
            $('#mt').click(function () {
                timeTypeChange('month');
                $('#calendar').fullCalendar('changeView', 'month');
            });

            $('#ag').click(function () {
                timeTypeChange('day');
                $('#calendar').fullCalendar('changeView', 'agendaWeek');
            });

            $('#td').click(function () {
                timeTypeChange('day');
                $('#calendar').fullCalendar('changeView', 'agendaDay');
            });
        }
    })

    function timeTypeChange(time_type)
    {
        $(".form_datetime").datetimepicker('remove');
        $(".form_datetime").val('');
        if(time_type == 'month')
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm",
                startView: 'year',
                minView: 'year',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01');
        }
        else
        {
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm-dd",
                startView: 'month',
                minView: 'month',
                todayBtn:'linked',
                autoclose: true
            });
            $(".form_datetime").datetimepicker('setStartDate', '2014-01-01');
        }
    }

    /*                                                                                                  结束*/
    $('#pointevent-time_type').val(up_type);
    $('#pointevent-time_type').trigger('change');

    /*时间选择框中的 值 变化时 对应的 fullcalendar 也发生变化*/
    $('.form_datetime').datetimepicker().on('changeDate', function(ev){
        var time_data = new Date($(".form_datetime").val());
        $("#calendar").fullCalendar('gotoDate', time_data.getFullYear(),time_data.getMonth(),time_data.getDate());
        });


    /*点击提交  收集日历上所有的事件 并将时间值赋给 time_range input框*/
    $('#_create').click(function () {
        var _datas = '';
        var data = $('#calendar').fullCalendar('clientEvents');
        $.each(data, function (key, value) {
            $.each(value, function (k, v) {
                if (k == 'start' || k == 'end') {
                    _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm") + ',';
                }
            })
            _datas += ';';
        })
        $("input[name='PointEvent[time_range]']").val(_datas);
    })
}
</script>