<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AtomEvent $model
 */
$this->title = Yii::t('app', 'Create Atom Event', [
  'modelClass' => 'Atom Event',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>