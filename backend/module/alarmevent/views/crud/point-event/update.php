<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AtomEvent $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Atom Event',
]) . $model->name;
?>
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>
