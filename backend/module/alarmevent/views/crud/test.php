<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\AlarmEvent;
/**
 * @var yii\web\View $this
 * @var backend\models\CompoundEvent $model
 * @var yii\widgets\ActiveForm $form
 */
$alarm=MyFunc::SelectDataAddArrayTop(MyFunc::_map(AlarmEvent::getAvailableEvent(), 'id', 'name'));
$num=array(['0'=>'1','1'=>'2']);
//echo '<pre>';
//print_r($alarm);
//die;
?>

<!-- widget grid -->
<!-- widget grid -->
<!--弹出菜单css-->
<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }
</style>

<section id="widget-grid" class="">
    <!--弹出菜单-->
    <div id="contextMenu">
        <ul>
            <li><a href="#" id="menu1" onclick="AtomicEvent()">原子事件</a></li>
            <li><a href="#" id="menu5" onclick="">属性</a></li>
            <li><a href="#" id="menu3" onclick=" ">持续时间</a></li>
            <li><a href="#" id="menu4" onclick=" ">删除</a></li>
        </ul>
    </div>
    <!--弹出框-->
    <div id="dialog_simple"   title="Dialog Simple Title" style="display: none">
        <?= HTML::dropDownList($name='1', $selection='0', $items = $alarm, $options = ['id'=>'Event','class'=>"select2"]) ;?>
        <?= HTML::dropDownList($name='1', $selection='0', $items = $num , $options = ['id'=>'selectme','class'=>"select2"]) ;?>
        <!--time-->
        <div id = '_cron' class = 'time_mode' style="height: 160px">
            <lable><?=Yii::t('app', 'Backup Date')?> :</lable>
            <span id='selector'></span>
        </div>
        <!--timeend-->
    </div>
    <!-- START ROW -->
    <div class="row">
        <div id="tabs"  class="  ui-tabs ui-widget ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                <li>
                    <?=Html::a(Yii::t('app', 'Atom Event'), ['crud/create','type' => 1], ['class' => 'fa fa-slack'])?>
                </li>
                <li  class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                    <?=Html::a(Yii::t('app', 'Compound Event'), ['crud/create','type' => 2], ['class' => 'fa fa-sliders'])?>
                </li>

            </ul>
        </div>
        <hr />
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>

                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <section id="widget-grid" class="">
                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
                        <!-- 当前页面为原子事件 type 为 2-->
                        <?= Html::hiddenInput("CompoundEvent[type]", 2) ?>


                        <?= $form->field($model, 'event_id')->dropDownList(
                            MyFunc::SelectDataAddArrayTop(MyFunc::_map(AlarmEvent::getAvailableEvent(), 'id', 'name')),
                            //由于本页面的jquery 已被 gojs重新,及当前的$符号不是代表jquery的对象，而是gojs对象，所以不是gojs插件的事件都用原始的js方法使用
                            ['class' => 'select2','placeholder' => Yii::t('app','Select the alarm event'), 'style' => 'width:200px', 'onchange' => '_change()']) ?>
                        <fieldset>
                            <!-- START ROW -->
                            <div class="row">

                                <!-- NEW COL START -->
                                <div id="">

                                    <!--gojs  主体   end-->
                                    <div class = "col-sm-10 col-md-10 col-lg-10" style="width:94%; white-space:nowrap;">
                                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:100px">
                                      <div id="palette" style="background-color: ; border: solid 1px black; height: 500px;margin-left:10px"></div>
                                    </span>
                                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:90%">
                                      <div id="myDiagram" style="border: solid 1px black; height: 500px"></div>
                                    </span>
                                    </div>
                                </div>

                            </div>

                            <span id = 'diagramEventsMsg'></span>

                            <div id="buttons" style = 'display:none'>
                                <a id="loadModel" class = 'btn' onclick="load()">Load</a>
                                <a id="saveModel" class = 'btn'  onclick="save()">Save</a>
                                <a id="sd" class = 'btn'  onclick="change()">Change</a>
                                节点key:<input type = 'text' id = 'out'>
                            </div>
                            <!--gojs  主体  end -->
                            <?= Html::textarea('CompoundEvent[gojs_data]', $model->gojs_data, ['style' => 'display:none', 'id' => 'gojs_data']) ?>
                            <textarea id="mySavedModel" style="width:100%;height:200px;display:none">
                                { "class": "go.GraphLinksModel",
                                "linkFromPortIdProperty": "fromPort",
                                "linkToPortIdProperty": "toPort",
                                "nodeDataArray": [
                                {"category":"input", "key":"input",  "desc":"first letter","color":"lightblue","loc":"-570 -160"},
                                {"category":"input", "key":"input2", "desc":"second letter","color":"lightblue","loc":"-570 -60"},
                                {"category":"input", "key":"input3", "desc":"third letter","color":"lightblue","loc":"-450 30"},
                                {"category":"and", "key":"and", "loc":"-460 -100"},
                                {"category":"or", "key":"or", "loc":"-340 -30"},
                                {"category":"output", "key":"output", "loc":"-190 -30"}
                                ],
                                "linkDataArray": [
                                {"from":"input", "to":"and", "fromPort":"", "toPort":"in1"},
                                {"from":"input2", "to":"and", "fromPort":"", "toPort":"in2"},
                                {"from":"and", "to":"or", "fromPort":"out", "toPort":"in1"},
                                {"from":"input3", "to":"or", "fromPort":"", "toPort":"in2"},
                                {"from":"or", "to":"output", "fromPort":"out", "toPort":""}
                                ]}
                            </textarea>
                </div>
                <!-- END COL -->
                </fieldset>
                <div class="form-actions">
                    <?=
                    Html::submitButton(
                        $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'saveModel', 'onclick' => "save()"]
                    ) ?>
                </div>

                <?php
                ActiveForm::end(); ?>
            </div>
            <!-- end widget div -->
        </article>

        <!-- END COL -->
    </div>
</section>

<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
?>

<script>
    var red = "#E2C9BD";
    var green = "forestgreen";
    //弹出框绑定事件$在onload内不能使用。
    function dialog(){
        $('#dialog_simple').dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            title :  "点位绑定",
            buttons : [{
                html : "<i class='fa fa-trash-o'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
                    change1();

                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }]
        });


    }
    //改变 gojs中的内容
    function changevalue()
    {
        //Event_id与Event值
        var sele_obj = document.getElementById('Event');
        var event_id = sele_obj.value;
        var event_value = sele_obj.options[ sele_obj.selectedIndex ].text;
        //次数
        var subnum_obj= document.getElementById('selectme');
        var submum = subnum_obj.options[ subnum_obj.selectedIndex ].text;
        //时间
//    var
//    var

        console.log(event_id);
        console.log(event_value);
        console.log(submum);
        var model = myDiagram.model;
        var $select_node = '';
        var count_value = 0;
        model.startTransaction("increment");  // all model changes should happen in a transaction
        myDiagram.nodes.each(function(node) {
            if (node.category === "input") {
                if(node.data.key.match(event_value +'*')){ count_value ++;}       //在此统计 当前选中事件出先在复合事件中的个数
                if(node.isSelected){$select_node = node;}                                           //如果在同一复合时间中 出现多个名称相同的事件时，会分不清流程中哪个是哪个，需要加一个标识
            }
        });
        event_value += (count_value ? '*' + count_value : '');                                     //设置唯一标识 即当前用户选中同一事件的次数
        event_value = findValueOfDiagram(event_value);

        $select_node ? model.setDataProperty($select_node.data, "key", event_value) : '';
        $select_node ? model.setDataProperty($select_node.data, "event_id", event_id) : '';
        $select_node ? model.setDataProperty($select_node.data, "num",submum) : '';
        model.commitTransaction("increment");  // all model changes should happen in a transaction
    }

    function change1() {
        changevalue();

    }
    function SetAttribute(){
        $('#dialog_simple').dialog('open');
    }
    //原子事件
    function AtomicEvent(){

    }


    function time() {
        /* linux 任务计划时间插件*/
        var cron = $('#selector').cron({
            initial: '* * * * *',
            onChange: function () {
                $('#interval_time').val($(this).cron("value"))
            },
            useGentleSelect: true
        });
    }
    //cron.cron('value', $interval)  ;



    window.onload = function () {
        //绑定弹出框
        dialog();
        //删除函数
        function delete1(){
            var diagram = myDiagram;
            if (!(diagram.currentTool instanceof go.ContextMenuTool)) return;
            diagram.commandHandler.deleteSelection();
            diagram.currentTool.stopTool();
        }


        //得到关系
        function test () {
            var diagram = this.myDiagram;
            var temp = diagram.model.toJson();
            console.log(temp);
            var data = eval('(' + temp + ')');
            //console.log(data['nodeDataArray']);

            // 从output开始遍历关系图
            var linkDataArray = data['linkDataArray'];
            var nodeDataArray = data['nodeDataArray'];
            var link = new Array();
            var fromvalue = new Array();
            fromvalue.push('output');
            while (fromvalue.length!=0) {
                var linktemp=new Array();
                for (i = 0; i < linkDataArray.length; i++) {
                    var lastvalue=fromvalue[fromvalue.length-1];
                    var p=1;
                    if (linkDataArray[i]['to'] ==lastvalue ) {
                        var value=linkDataArray[i]['from'];
                        fromvalue.pop();
                        fromvalue.push(value);
                        fromvalue.push(lastvalue);
                        linktemp.push(value);
                    }
                }
                if(linktemp.length!=0){
                    link[fromvalue[fromvalue.length-1]]=linktemp;
                }
                fromvalue.pop();
            }

            //得到关系数组
            console.log(link);
            return link;
        }
        //处理data信息
        //菜单栏绑定事件
        document.getElementById("menu5").onclick=function(){
            SetAttribute();
        }
        //绑定删除事件
        document.getElementById("menu4").onclick=function(){
            delete1 ();
        }
        document.getElementById("menu3").onclick=function(){
            test();
        }

        //改变节点类型
        function changeCategory(e, obj) {
            var node = obj.part;
            if (node) {
                var diagram = node.diagram;
                diagram.startTransaction("changeCategory");
                var cat = diagram.model.getCategoryForNodeData(node.data);
                if (cat === "input")
                    cat = "detailed";
                else
                    cat = "input";
                diagram.model.setCategoryForNodeData(node.data, cat);
                diagram.commitTransaction("changeCategory");
            }
        }

        //
        time();
        var $ = go.GraphObject.make;  // for conciseness in defining templates

        function showMessage(s) {
            document.getElementById("diagramEventsMsg").textContent = s;
        }
        myDiagram =
            $(go.Diagram, "myDiagram",  // create a new Diagram in the HTML DIV element "myDiagram"
                {
                    initialContentAlignment: go.Spot.Center,
                    allowDrop: true,  // Nodes from the Palette can be dropped into the Diagram
                    "draggingTool.isGridSnapEnabled": true,  // dragged nodes will snap to a grid of 10x10 cells
                    "undoManager.isEnabled": true
                });

        // install the PortShiftingTool as a "mouse move" tool
        myDiagram.toolManager.mouseMoveTools.insertAt(0, new PortShiftingTool());
        //myDiagram.toolManager.contextMenuTool
        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("saveModel");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
                if (idx < 0) document.title += "*";
            } else {
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });
//    myDiagram.addDiagramListener("ObjectSingleClicked",
//        function(e) {
//            console.log(e.subject.part.data.key);
//            if(e.subject.part.data.key == 'output')
//            {
//            }
//        });
        myDiagram.addDiagramListener("SelectionDeleting",  //添加 删除监听事件
            function(e) {
                e.diagram.selection.each(function(node){
                    if(node.category == 'output')
                    {
                        e.cancel = true;
                        showMessage("Cannot delete output selected parts");
                    }
                })
//            console.log(e.diagram.selection)
//                e.cancel = true;
//                showMessage("Cannot delete multiple selected parts");
            });
        var palette = new go.Palette("palette");  // create a new Palette in the HTML DIV element "palette"

        // creates relinkable Links that will avoid crossing Nodes when possible and will jump over other Links in their paths
        myDiagram.linkTemplate =
            //未建立时的属性
            $(go.Link,
                { routing: go.Link.AvoidsNodes,
                    curve: go.Link.JumpOver,
                    corner: 3,
                    relinkableFrom: true, relinkableTo: true,
                    selectionAdorned: false, // Links are not adorned when selected so that their color remains visible.
                    shadowOffset: new go.Point(0, 0), shadowBlur: 5, shadowColor: "blue"
                },
                //建立连接 后的属性
                new go.Binding("isShadowed", "isSelected").ofObject(),
                $(go.Shape,
                    { name: "SHAPE", isPanelMain: true, strokeWidth: 2, stroke: red }));

        // node template helpers  ----------------------------看下
        var sharedToolTip =
            $(go.Adornment, "Auto",
                $(go.Shape, "RoundedRectangle", { fill: "lightyellow" }),
                $(go.TextBlock, { margin: 2 },
                    new go.Binding("text",  "" , function(d) { return d.category; })));

        // define some common property settings  ----------------节点样式
        function nodeStyle() {
            return [new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("isShadowed", "isSelected").ofObject(),
                {

                    selectionAdorned: false,
                    shadowOffset: new go.Point(0, 0),
                    shadowBlur: 15,
                    shadowColor: "blue",
//                resizable: true,
                    resizeObjectName: "NODESHAPE",
                    toolTip: sharedToolTip
                }];
        }

        function shapeStyle() {
            return {
                name: "NODESHAPE",
                fill: "lightgray",
                stroke: "darkslategray",
                desiredSize: new go.Size(60, 60), //设置 node的 size
                strokeWidth: 2
            };
        }

        function portStyle(input) {
            return {
                desiredSize: new go.Size(6, 6),
                fill: "black",
                fromSpot: go.Spot.Right,
                fromLinkable: !input,
                toSpot: go.Spot.Left,
                toLinkable: input,
                toMaxLinks: 1,
                cursor: "pointer"
            };
        }
        // define templates for each type of node ***************************************************************************设置 input node
        var inputTemplate =
            $(go.Node, "Spot", nodeStyle(),
                //contextMenu右键菜单属性
                {
                    contextMenu: $(go.Adornment)
                },
                $(go.Shape, "Circle", shapeStyle(),
                    { fill: red }),  // override the default fill (from shapeStyle()) to be red
                $(go.Shape, "Rectangle", portStyle(false),  // the only port
                    { portId: "", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "input", font: "12pt",textAlign: "center" }, new go.Binding("text", "key")), //将显示的内容和 json中的key联系起来
                $("Button",
                    { alignment: go.Spot.TopRight },
                    $(go.Shape, "AsteriskLine", { width: 8, height: 8 }),
                    { click: changeCategory }),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        e.diagram.startTransaction("Toggle Input");
                        var shp = obj.findObject("NODESHAPE");
                        shp.fill = (shp.fill === green) ? red : green;
                        //console.log(shp);
                        updateStates();
                        e.diagram.commitTransaction("Toggle Input");
                    }
                }
            );
        function showSmallPorts(node, show) {
            node.ports.each(function(port) {
                if (port.portId !== "") {  // don't change the default port, which is the big shape
                    port.fill = show ? "rgba(0,0,0,.3)" : null;
                }
            });
        }
        function makePort(name, spot, output, input) {
            // the port is basically just a small transparent square
            return $(go.Shape, "Circle",
                {
                    fill: null,  // not seen, by default; set to a translucent gray by showSmallPorts, defined below
                    stroke: null,
                    desiredSize: new go.Size(7, 7),
                    alignment: spot,  // align the port on the main Shape
                    alignmentFocus: spot,  // just inside the Shape
                    portId: name,  // declare this object to be a "port"
                    fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                    fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                    cursor: "pointer"  // show a different cursor to indicate potential link point
                });
        }
        var nodeSelectionAdornmentTemplate =
            $(go.Adornment, "Auto",
                $(go.Shape, { fill: null, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] }),
                $(go.Placeholder)
            );
        var nodeResizeAdornmentTemplate =
            $(go.Adornment, "Spot",
                { locationSpot: go.Spot.Right },
                $(go.Placeholder),
                $(go.Shape, { alignment: go.Spot.TopLeft, cursor: "nw-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Top, cursor: "n-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.TopRight, cursor: "ne-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Left, cursor: "w-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Right, cursor: "e-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomLeft, cursor: "se-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Bottom, cursor: "s-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomRight, cursor: "sw-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" })
            );
        var nodeRotateAdornmentTemplate =
            $(go.Adornment,
                { locationSpot: go.Spot.Center, locationObjectName: "CIRCLE" },
                $(go.Shape, "Circle", { name: "CIRCLE", cursor: "pointer", desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { geometryString: "M3.5 7 L3.5 30", isGeometryPositioned: true, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] })
            );
        var testTemplate =
            $(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                { selectable: true, selectionAdornmentTemplate: nodeSelectionAdornmentTemplate },
                //{ resizable: true, resizeObjectName: "PANEL", resizeAdornmentTemplate: nodeResizeAdornmentTemplate },
                { rotatable: true, rotateAdornmentTemplate: nodeRotateAdornmentTemplate },
                new go.Binding("angle").makeTwoWay(),
                // the main object is a Panel that surrounds a TextBlock with a Shape
                $(go.Panel, "Vertical",//竖直排列
//                    { name: "PANEL" },
//                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
//                    $(go.Shape, "Rectangle",  // default figure
//                        {
//                            portId: "", // the default port: if no spot on link data, use closest side
//                            fromLinkable: true, toLinkable: true, cursor: "pointer",
//                            fill: "white"  // default color
//                        },
//                        new go.Binding("figure"),
//                        new go.Binding("fill")),
                    $(go.Picture,
                        {
                            maxSize: new go.Size(75, 75)
                        },
                        new go.Binding("source", "img")),
                    $(go.TextBlock,
                        {
                            margin: new go.Margin(3, 0, 0, 0),
                            maxSize: new go.Size(160, NaN),
                            //wrap: go.TextBlock.WrapFit,
                            editable: true,
                            isMultiline: false
                        },
                        new go.Binding("text", "text"))
                ),
                $(go.Panel, "Horizontal",//竖直排列
                    $(go.TextBlock,
                        {
                            width: 40, height: 9,
                            margin: 2,
                            editable: true,
                        },
                        new go.Binding("text", "text1")),
                    $(go.TextBlock,
                        {
                            width: 40, height: 9,
                            margin: 2,
                            editable: true,
                        },
                        new go.Binding("background", "background"),
                        new go.Binding("text", "text2"))
                ),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('改变');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                },
                // four small named ports, one on each side:
                makePort("T", go.Spot.Top, false, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, false),
                { // handle mouse enter/leave events to show/hide the ports
                    mouseEnter: function(e, node) { showSmallPorts(node, true); },
                    mouseLeave: function(e, node) { showSmallPorts(node, false); }
                }
            );

        var detailtemplate =
            $(go.Node, "Spot",
                $(go.Panel, "Auto",
                    $(go.Shape, "RoundedRectangle", shapeStyle(),
                        { fill: red }),
                    $(go.Panel, "Table",
                        { defaultAlignment: go.Spot.Left },
                        $(go.TextBlock, { row: 0, column: 0, columnSpan: 2, font: "bold 12pt sans-serif" },
                            new go.Binding("text", "key")),

                        $(go.TextBlock, { row: 1, column: 0 }, "Num:"),
                        $(go.TextBlock, { row: 1, column: 1 }, new go.Binding("text", "num")),

                        $(go.TextBlock, { row: 3, column: 0 }, "Time:"),
                        $(go.TextBlock, { row: 3, column: 1 }, new go.Binding("text", "time"))
                    )
                ),
                $("Button",
                    { alignment: go.Spot.TopRight },
                    $(go.Shape, "AsteriskLine", { width: 8, height: 8 }),
                    { click: changeCategory })
            );

        // This is the actual HTML context menu:
        var cxElement = document.getElementById("contextMenu");
        // We don't want the div acting as a context menu to have a (browser) context menu!
        cxElement.addEventListener("contextmenu", function(e) { e.preventDefault(); return false; }, false);
        cxElement.addEventListener("blur", function(e) { cxMenu.stopTool(); }, false);
        // Override the ContextMenuTool.showContextMenu and hideContextMenu methods
        // in order to modify the HTML appropriately.
        var cxTool = myDiagram.toolManager.contextMenuTool;

        //重写showContextMenu方法
        cxTool.showContextMenu = function(contextmenu, obj) {
            var diagram = this.diagram;
            if (diagram === null) return;
            // Hide any other existing context menu
            if (contextmenu !== this.currentContextMenu) {
                this.hideContextMenu();
            }
            // Show only the relevant buttons given the current state.
//        var cmd = diagram.commandHandler;
//        document.getElementById("cut").style.display = cmd.canCutSelection() ? "block" : "none";
//        document.getElementById("copy").style.display = cmd.canCopySelection() ? "block" : "none";
//        document.getElementById("paste").style.display = cmd.canPasteSelection() ? "block" : "none";
//        document.getElementById("delete").style.display = cmd.canDeleteSelection() ? "block" : "none";
//        document.getElementById("color").style.display = obj !== null ? "block" : "none";
            // Now show the whole context menu element
            cxElement.style.display = "block";

            // we don't bother overriding positionContextMenu, we just do it here:
            var mousePt = diagram.lastInput.viewPoint;
            cxElement.style.left = mousePt.x +170+ "px";
            cxElement.style.top = mousePt.y +170+ "px";
            // Remember that there is now a context menu showing
            this.currentContextMenu = contextmenu;
        }
        //
        cxTool.hideContextMenu = function() {
            if (this.currentContextMenu === null) return;
            cxElement.style.display = "none";
            this.currentContextMenu = null;
        }




        var outputTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "Rectangle", shapeStyle(),
                    { fill: green }),  // override the default fill (from shapeStyle()) to be green
                $(go.Shape, "Rectangle", portStyle(true),  // the only port
                    { portId: "", alignment: new go.Spot(0, 0.5) }),
                $(go.TextBlock,{text: "output", font: "12pt",textAlign: "center"}),
                {

                }
            );

        var andTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "AndGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in1", alignment: new go.Spot(0, 0.3) }),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in2", alignment: new go.Spot(0, 0.7) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "and", background: "lightblue", font: "12pt",textAlign: "center" }),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('删除');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                }
            );

        var orTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "OrGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in1", alignment: new go.Spot(0.16, 0.3) }),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in2", alignment: new go.Spot(0.16, 0.7) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "or", background: "lightblue", font: "12pt",textAlign: "center" }),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('删除');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                }
            );
//
//    var xorTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "XorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.26, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.26, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var norTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "NorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.16, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.16, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var xnorTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "XnorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.26, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.26, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var nandTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "NandGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );

        var notTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "NandGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in", alignment: new go.Spot(0, 0.5) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "not", background: "lightblue", font: "12pt",textAlign: "end" }),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('删除');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                }
            );

        // add the templates created above to myDiagram and palette //gojs元素 模板
        myDiagram.nodeTemplateMap.add("input", inputTemplate);
        myDiagram.nodeTemplateMap.add("test", testTemplate);
        //myDiagram.nodeTemplateMap.add("detailed",detailinputTemplate);
        myDiagram.nodeTemplateMap.add("detailed",detailtemplate);
        myDiagram.nodeTemplateMap.add("output", outputTemplate);
        myDiagram.nodeTemplateMap.add("and", andTemplate);
        myDiagram.nodeTemplateMap.add("or", orTemplate);
//    myDiagram.nodeTemplateMap.add("xor", xorTemplate);
        myDiagram.nodeTemplateMap.add("not", notTemplate);
//    myDiagram.nodeTemplateMap.add("nand", nandTemplate);
//    myDiagram.nodeTemplateMap.add("nor", norTemplate);
//    myDiagram.nodeTemplateMap.add("xnor", xnorTemplate);

        // share the template map with the Palette
        palette.nodeTemplateMap = myDiagram.nodeTemplateMap;

        //gojs展示盘
        palette.model.nodeDataArray = [
            { category: "input", key : 'input' },
            { category: "test", key : 'test' ,text: "Start",  img:"/uploads/pic/2222.png"},
//        { category: "output", key : 'output' },
            { category: "and",key:'and'},
            { category: "or",key:'or'},
//        { category: "xor",key:'xor' },
            { category: "not",key:'not'},
//        { category: "nand" ,key:'nand'},
//        { category: "nor" ,key:'nor'},
//        { category: "xnor" ,key:'xnor'}
        ];

        // load the initial diagram
        load();

        // continually update the diagram
        loop();

    }

    // update the diagram every 250 milliseconds
    function loop() {
        setTimeout(function() { updateStates(); loop(); }, 250);
    }

    // update the value and appearance of each node according to its type and input values
    function updateStates() {
        var oldskip = myDiagram.skipsUndoManager;
        myDiagram.skipsUndoManager = true;
        // do all "input" nodes first
        myDiagram.nodes.each(function(node) {
            if (node.category === "input") {
                doInput(node);
            }

        });
        // now we can do all other kinds of nodes
        myDiagram.nodes.each(function(node) {
            switch (node.category) {
                case "and":       doAnd(node); break;
                case "or":         doOr(node); break;
//            case "xor":       doXor(node); break;
                case "not":       doNot(node); break;
//            case "nand":     doNand(node); break;
//            case "nor":       doNor(node); break;
//            case "xnor":     doXnor(node); break;
                case "output": doOutput(node); break;
                case "input": break;  // doInput already called, above
            }
        });
        myDiagram.skipsUndoManager = oldskip;
    }

    // helper predicate
    function linkIsTrue(link) {  // assume the given Link has a Shape named "SHAPE"
        return link.findObject("SHAPE").stroke === green;
    }

    // helper function for propagating results
    function setOutputLinks(node, color) {
        node.findLinksOutOf().each(function(link) { link.findObject("SHAPE").stroke = color; });
    }

    // update nodes by the specific function for its type
    // determine the color of links coming out of this node based on those coming in and node type

    function doInput(node) {
        // the output is just the node's Shape.fill
        setOutputLinks(node, node.findObject("NODESHAPE").fill);
    }

    function doAnd(node) {
        var color = node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNand(node) {
        var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNot(node) {
        var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }

    function doOr(node) {
        var color = node.findLinksInto().any(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNor(node) {
        var color = !node.findLinksInto().any(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }

    function doXor(node) {
        var truecount = 0;
        node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
        var color = truecount % 2 === 0 ? green : red;
        setOutputLinks(node, color);
    }
    function doXnor(node) {
        var truecount = 0;
        node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
        var color = truecount % 2 !== 0 ? green : red;
        setOutputLinks(node, color);
    }

    function doOutput(node) {
        // assume there is just one input link
        // we just need to update the node's Shape.fill
        node.linksConnected.each(function(link) { node.findObject("NODESHAPE").fill = link.findObject("SHAPE").stroke; });
    }

    // save a model to and load a model from Json text, displayed below the Diagram
    function save() {
        document.getElementById("gojs_data").value = myDiagram.model.toJson();
        myDiagram.isModified = false;
    }
    function load() {
        var $gojs_json = document.getElementById("gojs_data").value;
        if($gojs_json)
            myDiagram.model = go.Model.fromJson($gojs_json);
        else
            myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }


    //改变 gojs中的内容
    function _change()
    {
        var sele_obj = document.getElementById('compoundevent-event_id');
        var event_id = sele_obj.value;
        var event_value = sele_obj.options[ sele_obj.selectedIndex ].text;
        change(event_value, event_id);
        console.log(event_id);
        console.log(event_value);
    }

    function change(event_value, event_id)
    {
        var model = myDiagram.model;
        var $select_node = '';
        var count_value = 0;
        model.startTransaction("increment");  // all model changes should happen in a transaction
        myDiagram.nodes.each(function(node) {
            if (node.category === "input") {
                if(node.data.key.match(event_value +'*')){ count_value ++;}       //在此统计 当前选中事件出先在复合事件中的个数
                if(node.isSelected){$select_node = node;}                                           //如果在同一复合时间中 出现多个名称相同的事件时，会分不清流程中哪个是哪个，需要加一个标识
            }
        });
        event_value += (count_value ? '*' + count_value : '');                                     //设置唯一标识 即当前用户选中同一事件的次数
        event_value = findValueOfDiagram(event_value);

        $select_node ? model.setDataProperty($select_node.data, "key", event_value) : '';
        $select_node ? model.setDataProperty($select_node.data, "event_id", event_id) : '';
        model.commitTransaction("increment");  // all model changes should happen in a transaction
    }

    // 如果 input 中有相同名称的 事件则在其后 加 '-1'
    function findValueOfDiagram(value)
    {
        myDiagram.nodes.each(function(node) {
            console.log(node);
            if (node.category === "input" && node.data.key == value) {
                value += '-1';
                return true;
            }
        });
        return value;
    }
</script>

