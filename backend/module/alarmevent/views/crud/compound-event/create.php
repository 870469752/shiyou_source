<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CompoundEvent $model
 */
$this->title = Yii::t('app', '创建复合事件', [
  'modelClass' => 'Compound Event',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
