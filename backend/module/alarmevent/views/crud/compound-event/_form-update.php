<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\AlarmEvent;
/**
 * @var yii\web\View $this
 * @var backend\models\CompoundEvent $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<!-- widget grid -->
<section id="widget-grid" class="">
        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>

                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <section id="widget-grid" class="">
                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
                        <!-- 当前页面为原子事件 type 为 2-->
                        <?= Html::hiddenInput("CompoundEvent[type]", 2) ?>


                        <?= $form->field($model, 'event_id')->dropDownList(
                            MyFunc::SelectDataAddArrayTop(MyFunc::_map(AlarmEvent::getAvailableEvent(), 'id', 'name')),
                            //由于本页面的jquery 已被 gojs重新,及当前的$符号不是代表jquery的对象，而是gojs对象，所以不是gojs插件的事件都用原始的js方法使用
                            ['class' => 'select2','placeholder' => Yii::t('app','Select the alarm event'), 'style' => 'width:200px', 'onchange' => '_change()']) ?>
                        <fieldset>
                            <!-- START ROW -->
                            <div class="row">

                                <!-- NEW COL START -->
                                <div id="">

                                    <!--gojs  主体   end-->
                                    <div class = "col-sm-10 col-md-10 col-lg-10" style="width:94%; white-space:nowrap;">
                                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:100px">
                                      <div id="palette" style="background-color: ; border: solid 1px black; height: 500px;margin-left:10px"></div>
                                    </span>
                                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:90%">
                                      <div id="myDiagram" style="border: solid 1px black; height: 500px"></div>
                                    </span>
                                    </div>
                                </div>

                            </div>

                            <span id = 'diagramEventsMsg'></span>

                            <div id="buttons" style = 'display:none'>
                                <a id="loadModel" class = 'btn' onclick="load()">Load</a>
                                <a id="saveModel" class = 'btn'  onclick="save()">Save</a>
                                <a id="sd" class = 'btn'  onclick="change()">Change</a>
                                节点key:<input type = 'text' id = 'out'>
                            </div>
                            <!--gojs  主体  end -->
                            <?= Html::textarea('CompoundEvent[gojs_data]', $model->gojs_data, ['style' => 'display:none', 'id' => 'gojs_data']) ?>
                            <textarea id="mySavedModel" style="width:100%;height:200px;display:none">
                                { "class": "go.GraphLinksModel",
                                "linkFromPortIdProperty": "fromPort",
                                "linkToPortIdProperty": "toPort",
                                "nodeDataArray": [
                                {"category":"input", "key":"input", "loc":"-570 -160"},
                                {"category":"input", "key":"input2", "loc":"-570 -60"},
                                {"category":"input", "key":"input3", "loc":"-450 30"},
                                {"category":"and", "key":-3, "loc":"-460 -100"},
                                {"category":"or", "key":-4, "loc":"-340 -30"},
                                {"category":"output", "key":"output", "loc":"-190 -30"}
                                ],
                                "linkDataArray": [
                                {"from":"input", "to":-3, "fromPort":"", "toPort":"in1"},
                                {"from":"input2", "to":-3, "fromPort":"", "toPort":"in2"},
                                {"from":-3, "to":-4, "fromPort":"out", "toPort":"in1"},
                                {"from":"input3", "to":-4, "fromPort":"", "toPort":"in2"},
                                {"from":-4, "to":"output", "fromPort":"out", "toPort":""}
                                ]}
                            </textarea>
                </div>
                <!-- END COL -->
                </fieldset>
                <div class="form-actions">
                    <?=
                    Html::submitButton(
                        $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'saveModel', 'onclick' => "save()"]
                    ) ?>
                </div>

                <?php
                ActiveForm::end(); ?>
            </div>
            <!-- end widget div -->
        </article>

        <!-- END COL -->
    </div>
</section>

<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
?>

<script>
    var red = "#E2C9BD";
    var green = "forestgreen";
    window.onload = function () {
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        function showMessage(s) {
            document.getElementById("diagramEventsMsg").textContent = s;
        }
        myDiagram =
            $(go.Diagram, "myDiagram",  // create a new Diagram in the HTML DIV element "myDiagram"
                {
                    initialContentAlignment: go.Spot.Center,
                    allowDrop: true,  // Nodes from the Palette can be dropped into the Diagram
                    "draggingTool.isGridSnapEnabled": true,  // dragged nodes will snap to a grid of 10x10 cells
                    "undoManager.isEnabled": true
                });

        // install the PortShiftingTool as a "mouse move" tool
        myDiagram.toolManager.mouseMoveTools.insertAt(0, new PortShiftingTool());

        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("saveModel");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
                if (idx < 0) document.title += "*";
            } else {
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });
//    myDiagram.addDiagramListener("ObjectSingleClicked",
//        function(e) {
//            console.log(e.subject.part.data.key);
//            if(e.subject.part.data.key == 'output')
//            {
//            }
//        });
        myDiagram.addDiagramListener("SelectionDeleting",  //添加 删除监听事件
            function(e) {
                e.diagram.selection.each(function(node){
                    if(node.category == 'output')
                    {
                        e.cancel = true;
                        showMessage("Cannot delete output selected parts");
                    }
                })
//            console.log(e.diagram.selection)
//                e.cancel = true;
//                showMessage("Cannot delete multiple selected parts");
            });
        var palette = new go.Palette("palette");  // create a new Palette in the HTML DIV element "palette"

        // creates relinkable Links that will avoid crossing Nodes when possible and will jump over other Links in their paths
        myDiagram.linkTemplate =
            //未建立时的属性
            $(go.Link,
                { routing: go.Link.AvoidsNodes,
                    curve: go.Link.JumpOver,
                    corner: 3,
                    relinkableFrom: true, relinkableTo: true,
                    selectionAdorned: false, // Links are not adorned when selected so that their color remains visible.
                    shadowOffset: new go.Point(0, 0), shadowBlur: 5, shadowColor: "blue"
                },
                //建立连接 后的属性
                new go.Binding("isShadowed", "isSelected").ofObject(),
                $(go.Shape,
                    { name: "SHAPE", isPanelMain: true, strokeWidth: 2, stroke: red }));

        // node template helpers  ----------------------------看下
        var sharedToolTip =
            $(go.Adornment, "Auto",
                $(go.Shape, "RoundedRectangle", { fill: "lightyellow" }),
                $(go.TextBlock, { margin: 2 },
                    new go.Binding("text",  "" , function(d) { return d.category; })));

        // define some common property settings  ----------------节点样式
        function nodeStyle() {
            return [new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                new go.Binding("isShadowed", "isSelected").ofObject(),
                {
                    selectionAdorned: false,
                    shadowOffset: new go.Point(0, 0),
                    shadowBlur: 15,
                    shadowColor: "blue",
//                resizable: true,
                    resizeObjectName: "NODESHAPE",
                    toolTip: sharedToolTip
                }];
        }

        function shapeStyle() {
            return {
                name: "NODESHAPE",
                fill: "lightgray",
                stroke: "darkslategray",
                desiredSize: new go.Size(60, 60), //设置 node的 size
                strokeWidth: 2
            };
        }

        function portStyle(input) {
            return {
                desiredSize: new go.Size(6, 6),
                fill: "black",
                fromSpot: go.Spot.Right,
                fromLinkable: !input,
                toSpot: go.Spot.Left,
                toLinkable: input,
                toMaxLinks: 1,
                cursor: "pointer"
            };
        }
        // define templates for each type of node ***************************************************************************设置 input node
        var inputTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "Circle", shapeStyle(),
                    { fill: red }),  // override the default fill (from shapeStyle()) to be red
                $(go.Shape, "Rectangle", portStyle(false),  // the only port
                    { portId: "", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "input", font: "12pt",textAlign: "center" }, new go.Binding("text", "key")), //将显示的内容和 json中的key联系起来
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        e.diagram.startTransaction("Toggle Input");
                        var shp = obj.findObject("NODESHAPE");
                        shp.fill = (shp.fill === green) ? red : green;
                        updateStates();
                        e.diagram.commitTransaction("Toggle Input");
                    }
                }
            );

        var outputTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "Rectangle", shapeStyle(),
                    { fill: green }),  // override the default fill (from shapeStyle()) to be green
                $(go.Shape, "Rectangle", portStyle(true),  // the only port
                    { portId: "", alignment: new go.Spot(0, 0.5) }),
                $(go.TextBlock,{text: "output", font: "12pt",textAlign: "center"}),
                {
                }
            );

        var andTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "AndGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in1", alignment: new go.Spot(0, 0.3) }),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in2", alignment: new go.Spot(0, 0.7) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "and", background: "lightblue", font: "12pt",textAlign: "center" })
            );

        var orTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "OrGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in1", alignment: new go.Spot(0.16, 0.3) }),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in2", alignment: new go.Spot(0.16, 0.7) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "or", background: "lightblue", font: "12pt",textAlign: "center" })
            );
//
//    var xorTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "XorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.26, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.26, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var norTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "NorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.16, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.16, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var xnorTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "XnorGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0.26, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0.26, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );
//
//    var nandTemplate =
//        $(go.Node, "Spot", nodeStyle(),
//            $(go.Shape, "NandGate", shapeStyle()),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in1", alignment: new go.Spot(0, 0.3) }),
//            $(go.Shape, "Rectangle", portStyle(true),
//                { portId: "in2", alignment: new go.Spot(0, 0.7) }),
//            $(go.Shape, "Rectangle", portStyle(false),
//                { portId: "out", alignment: new go.Spot(1, 0.5) })
//        );

        var notTemplate =
            $(go.Node, "Spot", nodeStyle(),
                $(go.Shape, "NandGate", shapeStyle()),
                $(go.Shape, "Rectangle", portStyle(true),
                    { portId: "in", alignment: new go.Spot(0, 0.5) }),
                $(go.Shape, "Rectangle", portStyle(false),
                    { portId: "out", alignment: new go.Spot(1, 0.5) }),
                $(go.TextBlock,{text: "not", background: "lightblue", font: "12pt",textAlign: "end" })
            );

        // add the templates created above to myDiagram and palette //gojs元素 模板
        myDiagram.nodeTemplateMap.add("input", inputTemplate);
        myDiagram.nodeTemplateMap.add("output", outputTemplate);
        myDiagram.nodeTemplateMap.add("and", andTemplate);
        myDiagram.nodeTemplateMap.add("or", orTemplate);
//    myDiagram.nodeTemplateMap.add("xor", xorTemplate);
        myDiagram.nodeTemplateMap.add("not", notTemplate);
//    myDiagram.nodeTemplateMap.add("nand", nandTemplate);
//    myDiagram.nodeTemplateMap.add("nor", norTemplate);
//    myDiagram.nodeTemplateMap.add("xnor", xnorTemplate);

        // share the template map with the Palette
        palette.nodeTemplateMap = myDiagram.nodeTemplateMap;

        //gojs展示盘
        palette.model.nodeDataArray = [
            { category: "input", key : 'input' },
//        { category: "output", key : 'output' },
            { category: "and"},
            { category: "or"},
//        { category: "xor" },
            { category: "not"},
//        { category: "nand" },
//        { category: "nor" },
//        { category: "xnor" }
        ];

        // load the initial diagram
        load();

        // continually update the diagram
        loop();
    }

    // update the diagram every 250 milliseconds
    function loop() {
        setTimeout(function() { updateStates(); loop(); }, 250);
    }

    // update the value and appearance of each node according to its type and input values
    function updateStates() {
        var oldskip = myDiagram.skipsUndoManager;
        myDiagram.skipsUndoManager = true;
        // do all "input" nodes first
        myDiagram.nodes.each(function(node) {
            if (node.category === "input") {
                doInput(node);
            }

        });
        // now we can do all other kinds of nodes
        myDiagram.nodes.each(function(node) {
            switch (node.category) {
                case "and":       doAnd(node); break;
                case "or":         doOr(node); break;
//            case "xor":       doXor(node); break;
                case "not":       doNot(node); break;
//            case "nand":     doNand(node); break;
//            case "nor":       doNor(node); break;
//            case "xnor":     doXnor(node); break;
                case "output": doOutput(node); break;
                case "input": break;  // doInput already called, above
            }
        });
        myDiagram.skipsUndoManager = oldskip;
    }

    // helper predicate
    function linkIsTrue(link) {  // assume the given Link has a Shape named "SHAPE"
        return link.findObject("SHAPE").stroke === green;
    }

    // helper function for propagating results
    function setOutputLinks(node, color) {
        node.findLinksOutOf().each(function(link) { link.findObject("SHAPE").stroke = color; });
    }

    // update nodes by the specific function for its type
    // determine the color of links coming out of this node based on those coming in and node type

    function doInput(node) {
        // the output is just the node's Shape.fill
        setOutputLinks(node, node.findObject("NODESHAPE").fill);
    }

    function doAnd(node) {
        var color = node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNand(node) {
        var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNot(node) {
        var color = !node.findLinksInto().all(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }

    function doOr(node) {
        var color = node.findLinksInto().any(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }
    function doNor(node) {
        var color = !node.findLinksInto().any(linkIsTrue) ? green : red;
        setOutputLinks(node, color);
    }

    function doXor(node) {
        var truecount = 0;
        node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
        var color = truecount % 2 === 0 ? green : red;
        setOutputLinks(node, color);
    }
    function doXnor(node) {
        var truecount = 0;
        node.findLinksInto().each(function(link) { if (linkIsTrue(link)) truecount++; });
        var color = truecount % 2 !== 0 ? green : red;
        setOutputLinks(node, color);
    }

    function doOutput(node) {
        // assume there is just one input link
        // we just need to update the node's Shape.fill
        node.linksConnected.each(function(link) { node.findObject("NODESHAPE").fill = link.findObject("SHAPE").stroke; });
    }

    // save a model to and load a model from Json text, displayed below the Diagram
    function save() {
        document.getElementById("gojs_data").value = myDiagram.model.toJson();
        myDiagram.isModified = false;
    }
    function load() {
        var $gojs_json = document.getElementById("gojs_data").value;
        if($gojs_json)
            myDiagram.model = go.Model.fromJson($gojs_json);
        else
            myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }

    //改变 gojs中的内容
    function _change()
    {
        var sele_obj = document.getElementById('compoundevent-event_id');
        var event_id = sele_obj.value
        var event_value = sele_obj.options[ sele_obj.selectedIndex ].text
        change(event_value, event_id);
    }

    function change(event_value, event_id)
    {
        var model = myDiagram.model;
        var $select_node = '';
        var count_value = 0;
        model.startTransaction("increment");  // all model changes should happen in a transaction
        myDiagram.nodes.each(function(node) {
            if (node.category === "input") {
                if(node.data.key.match(event_value +'*')){ count_value ++;}       //在此统计 当前选中事件出先在复合事件中的个数
                if(node.isSelected){$select_node = node;}                                           //如果在同一复合时间中 出现多个名称相同的事件时，会分不清流程中哪个是哪个，需要加一个标识
            }
        });
        event_value += (count_value ? '*' + count_value : '');                                     //设置唯一标识 即当前用户选中同一事件的次数
        event_value = findValueOfDiagram(event_value);

        $select_node ? model.setDataProperty($select_node.data, "key", event_value) : '';
        $select_node ? model.setDataProperty($select_node.data, "event_id", event_id) : '';
        model.commitTransaction("increment");  // all model changes should happen in a transaction
    }

    // 如果 input 中有相同名称的 事件则在其后 加 '-1'
    function findValueOfDiagram(value)
    {
        myDiagram.nodes.each(function(node) {
            console.log(node);
            if (node.category === "input" && node.data.key == value) {
                value += '-1';
                return true;
            }
        });
        return value;
    }
</script>