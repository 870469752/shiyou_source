<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\CompoundEvent $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">
        <div id="tabs"  class="  ui-tabs ui-widget ui-widget-content ui-corner-all col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class = 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all'>
                <li>
                    <?=Html::a(Yii::t('app', 'Atom Event'), ['crud/create','type' => 1], ['class' => 'fa fa-slack'])?>
                </li>
                <li class = 'ui-state-default ui-corner-top ui-tabs-active ui-state-active'>
                    <?=Html::a(Yii::t('app', 'Compound Event'), ['crud/create','type' => 2], ['class' => 'fa fa-sliders'])?>
                </li>

            </ul>
        </div>
		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->
					<input class="form-control" type="text">
				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body">

					<?php $form = ActiveForm::begin(); ?>
					<fieldset>
						<?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

						<?= $form->field($model, 'dependence')->textInput() ?>

						<?= $form->field($model, 'rule')->textInput(['maxlength' => 255]) ?>
                        <div id="myDiagramDiv" style="border: solid 1px blue; width:400px; height:150px"></div>
                        <!-- 当前页面为原子事件 type 为 1-->
                        <?= Html::hiddenInput("CompoundEvent[type]", 2) ?>
					</fieldset>
					<div class="form-actions">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
				<!-- end widget content -->
				
			</div>
			<!-- end widget div -->
			
		</div>
		<!-- end widget -->
		
	</article>
	<!-- END COL -->
</div>
</section>

<?php
    $this->registerJsFile("js/gojs/go.min.js", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function () {

        var diagram = new go.Diagram("myDiagramDiv");
        var $ = go.GraphObject.make;
        var violetbrush = $(go.Brush, go.Brush.Linear, { 0.0: "Violet", 1.0: "Lavender" });
        diagram.add(
            $(go.Part, "Vertical",
                $(go.Shape, { row: 0, column: 0, figure: "Club", width: 40, height: 40, margin: 4,
                    fill: "green" }),
                $(go.TextBlock, "green", { row: 1, column: 0 }),
                $(go.Shape, { row: 0, column: 1, figure: "Club", width: 40, height: 40, margin: 4,
                    fill: "white" }),
                $(go.TextBlock, "white", { row: 1, column: 1 }),
                $(go.Shape, { row: 0, column: 2, figure: "Club", width: 40, height: 40, margin: 4,
                    fill: "transparent" }),
                $(go.TextBlock, "transparent", { row: 1, column: 2 }),
                $(go.Shape, { row: 0, column: 3, figure: "Club", width: 40, height: 40, margin: 4,
                    fill: null }),
                $(go.TextBlock, "null", { row: 1, column: 3 })
            ));

        diagram.model = new go.GraphLinksModel(
            [
                { key: "Hello" },   // two node data, in an Array
                { key: "World!" }
            ],
            [
                { from: "Hello", to: "World!"}
            ]  // one link, in an Array
        );
    }
</script>