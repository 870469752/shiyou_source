<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\CompoundEvent $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Compound Event',
]) . $model->name;
?>
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>
