<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmEvent $model
 */
$this->title = Yii::t('app', 'Create Atom Event');
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
