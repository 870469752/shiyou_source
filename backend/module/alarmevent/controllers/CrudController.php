<?php

namespace backend\module\alarmevent\controllers;

use backend\models\AlarmLog;
use backend\models\ImageGroup;
use Yii;
use backend\models\AlarmEvent;
use backend\models\PointEvent;
use backend\models\CompoundEvent;
use backend\models\search\AlarmEventSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\OperateLog;
/**
 * AlarmEventController implements the CRUD actions for AlarmEvent model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AlarmEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlarmEventSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single AlarmEvent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $type = Yii::$app->request->get('type');
        $type = !empty($type) ? $type : 1;
        return $this->render('view', [
            'model' => $this->findModel($id,$type),
        ]);
    }

    /**
     * Creates a new AlarmEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $type = Yii::$app->request->get('type');
        $type = !empty($type) ? $type : 1;
        $model = $type == 1 ? new PointEvent() : new CompoundEvent();
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->saveEvent()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建报警事件';
            $op['en'] = 'Create alarm event';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            return $this->render($type == 1 ? 'point-event/create' : 'compound-event/create', [
                'model' => $model,
            ]);
        }
    }
    public function  actionTest(){
        $model =new CompoundEvent();
        return $this->render('test1',
            [
                'model' => $model,
            ]);
    }
    public function actionSave(){
        //$model=new ImageGroup();
        $model=$this->findImageGroupModel(1);
//        echo '<pre>';
//        print_r($model);
//        die;

//        $data['image']['id']=$_POST['id'];
//        $data['name']=$_POST['name'];
//        $data['data']=$_POST['device_info'];
        //$data['image_group']['id']=1;
        $data['ImageGroup']['name']='{"data_type":"description","data":{"zh-cn":"test1","em-us":"test1"}}';
        $data['ImageGroup']['data']='1111';
        $model->load($data);
//        echo '<pre>';
//        print_r($data);
//        print_r($model);
//        die;
        $result=$model->save();
        echo $result;
    }

    /**
     * Updates an existing AlarmEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $_type = Yii::$app->request->get('type');
        $type = !empty($type) ? $type : 1;
        $model = $this->findModel($id, Yii::$app->request->get('type'));

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->saveEvent()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改报警事件';
            $op['en'] = 'update alarm event';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $model->name = MyFunc::DisposeJSON($model->name);
            if($_type == 1)
            {
                $trigger = json_decode($model->trigger, true);
                $model->trigger = implode(',',$trigger['data'][0]);
            }
            return $this->render($_type == 1 ? 'point-event/update' : 'compound-event/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AlarmEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除报警事件';
        $op['en'] = 'Delete alarm event';
        $this->getLogOperation($op);

        $type = $_GET['type'];
        $this->findModel($id, $type)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AlarmEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AlarmEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $type)
    {
        if (($model = AlarmEvent::findOne($id)) !== null) {
            if($type == 1){
                $model = PointEvent::findOne($id);
            }
            else if($type == 2){
                $model = CompoundEvent::findOne($id);
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findImageGroupModel($id)
    {
        if (($model = ImageGroup::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new ImageGroup();
            return $model;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
