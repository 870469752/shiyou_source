<?php

namespace backend\module\alarmevent;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\alarmevent\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
