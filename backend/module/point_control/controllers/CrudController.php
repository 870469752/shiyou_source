<?php

namespace backend\module\point_control\controllers;
//yii 引用
use backend\models\LocationCategory;
use backend\models\Protocol;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
//自定义类 引用
use common\library\MyFunc;
use backend\controllers\MyController;
use common\library\MyExport;
//模型 引用
use backend\models\Point;
use backend\models\PointControl;
use backend\models\search\PointSearch;
use backend\models\Unit;
use backend\models\Category;
use backend\models\search\PointDataSearch;
use yii\helpers\BaseArrayHelper;
use common\library\Ssp;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Creates a new Point model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dataOfGet = Yii::$app->request->getQueryParams();
        $dataOfGet['point_condition'] = isset($dataOfGet['point_condition']) ? $dataOfGet['point_condition'] : '';
        $dataOfGet['active'] = isset($dataOfGet['active']) ? $dataOfGet['active'] : '_more_';
        $searchModel = new PointSearch();
        $dataProvider = $searchModel->_search_by($dataOfGet);
        //echo "<pre>";print_r($data);die;
        return $this->render('create',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'point_condition' => $dataOfGet['point_condition'],
            'active' => $dataOfGet['active']
        ]);
    }

    public function actionGetData()
    {
        $con = " id != 0 ";
        $table = 'core.point_control';
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => "id",
                'dt' => 'DT_RowId',
                'formatter' => function( $d, $row ) {
                    return 'row_'.$d;
                },
                'dj' =>'id'
            ),
            array( 'db' => 'id', 'dt' => 'id', 'dj'=>"id"),
            array( 'db' => 'point_id',   'dt' => 'point_id' ,'dj'=>"point_id" ),
            array( 'db' => 'time',   'dt' => 'time' ,'dj'=>"time" ),
            array( 'db' => 'value',  'dt' => 'value',  'dj' => 'value' ),
        );
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        //echo "<pre>";print_r($result);die;
        $point = new Point();
        for($i=0; $i<count($result['data']); $i++){
            $point_name = $point->getPointNameById($result['data'][$i]['point_id']);
            $result['data'][$i]['cn']=$point_name['cn'];
            $result['data'][$i]['us']=$point_name['us'];
        }
        echo json_encode( $result);
    }

    public function actionQuickAdd()
    {
        $param_arr = Yii::$app->request->getQueryParams();
        //echo "<pre>";print_r($param_arr);die;
        //判断是否 有选中 点位
        if($param_arr['point_ids'])
        {
            //将window.name 中的点位字符串  转为 数组
            $point_id_arr = explode(',', trim($param_arr['point_ids'], ','));

            //遍历 点位数组 一一生成 点位的系统事件 和 规则
            foreach($point_id_arr as $k => $v)
            {
                //实例化 事件对象
                $point_control = new PointControl();
                $point_control -> load([
                    'time' =>$param_arr['hour'].":".$param_arr['second'],
                    'value' => $param_arr['set_val'],
                    'point_id' => $v,
                ], '');
                $point_control -> saveEvent();
            }
        }
        $this->redirect('/point_control/crud');
    }

    //点位控制批量删除
    public function actionDeletePointCon(){
        $ids = explode(",",Yii::$app->request->post('ids'));
        foreach($ids as $k=>$v){
            $p_c = new PointControl();
            $result = $p_c->_delete($v);
        }
        return $result;
    }
}
