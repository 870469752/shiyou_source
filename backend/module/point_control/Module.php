<?php

namespace backend\module\point_control;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\point_control\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
