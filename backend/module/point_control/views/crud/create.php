<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\library\MyActiveForm;
/**
 * @var yii\web\View $this
 * @var backend\models\AlarmBatch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode('添加点位控制') ?></h2>
                </header>
				<!-- widget div-->
				<div>
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->
					<!-- widget content -->
					<div class="widget-body" id="wid_one">
						<?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                        <?= Html::hiddenInput('active', '_one_', ['id' => '_active'])?>
                        <fieldset>
                            <div id = '_ones_' >
                                <label>搜索点位</label>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <?= Html::textInput('point_condition', '',
                                                        [
                                                            'data-type' => 'select2',
                                                            'id' => '_tags',
                                                            'title' => '请输入关键字',
                                                            'placeholder' => '请输入关键字'
                                                        ]) ?>
                                                    <div class="input-group-btn">
                                                        <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('app', 'Search Point'), ['class' =>'btn btn-primary', 'id' => 'scre']) ?>
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                            <li id = '_clear'><a href="javascript:void(0);">Clear</a></li>
                                                            <li><a href="javascript:void(0);">Cancel</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            <div style="height: 500px;overflow-y: scroll">
                                <?= GridView::widget([
                                        'formatter' => ['class' => 'common\library\MyFormatter'],
                                        'dataProvider' => $dataProvider,
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered table-hover table-middle',
                                            'style' => 'margin-top:20px'
                                        ],
                                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                                        'columns' => [
                                            [
                                                'class' => 'common\library\MyCheckboxColumn',
                                                'header' => Html::checkbox('_check', '', ['id' => '_check', 'label' => Yii::t('app', 'Invert Selected'), 'data-status']),
                                                'name' => 'point_id',
                                                'checkboxOptions' => ['class' => 'table_checkbox']
                                            ],
                                            ['attribute' => 'name', 'label' => Yii::t('app', 'Point'), 'format' => 'JSON'],
                                            ['attribute' => 'value', 'label' => '当前值'],
                                            ['attribute' => 'protocol_id', 'label' => '协议号'],
                                            ['attribute' => 'is_shield', 'label' => '是否屏蔽'],
                                            ['attribute' => 'is_upload', 'label' => '是否上传'],
                                        ],
                                    ]); ?>
                                </div>
                            <hr>
                            <div>
                                <span>
                                小时：<select id="hour" name="hour" style="width: 100px"><?php for($i=0;$i<24;$i++){ if($i<10){$i='0'.$i;}?><option value="<?=$i;?>"><?=$i;?></option><?php }?></select>
                                </span>
                                <span style="margin-left: 40px">
                                分钟：<select id="second" name="second" style="width: 100px;"><?php for($i=0;$i<60;$i++){ if($i<10){$i='0'.$i;}?><option value="<?=$i;?>"><?=$i;?></option><?php }?></select>
                                </span>
                                <span style="margin-left: 40px">设定值：<input id="set_val" name="set_val" type="text" /></span>
                                <span id="val_pro" style="color: red"></span>
                            </div>
                            <div class="form-actions">
                                <span id = 'error_tip_one' style="color:red;float:left"></span>
                                <a href="/point_control/crud" class="btn btn-success"> 返回列表</a>
                                <a href="javascript:void(0)" id="gd" class="btn btn-success"> <?=Yii::t('app', 'Create')?></a>
                            </div>
                            <?php MyActiveForm::end(); ?>
                    </div>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		<!-- END COL -->
	</div>
</section>
<?php
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>
<script>
window.onload = function () {
    //获取查找类型的按钮参数
    var $_active = <?="'".$active."'"?>;
    //获取单点查找的条件数据
    var $point_condition = <?="'".$point_condition."'"?>;
    //绑定点位
    var $binding

    /*******************************************以 点位分类 查找 还是 单个点位搜索 * start **************************************/
    $('#_more_').click(function(){
        $('#_active').val('_more_');
        check_refresh();
        $('#_ones_').hide();
        $('#_mores_').show();
        $('#_tags').tagsinput('removeAll');
    });
    $('#_one_').click(function(){
        //给当前按钮设置data-tag
        $('#_active').val('_one_');
        check_refresh();
        $('#_ones_').show();
        $('#_mores_').hide();
        $('.select2-offscreen').select2('val','');
    })
    /*******************************************以 点位分类 查找 还是 单个点位搜索 * end **************************************/

    //当点击筛选时 清空window.name 内的数据
    $('#scre').click(function(){
        window.name = '';
    })
    /****************************************   单点查找 input框事件 * start *******************************************/
    var elt = $("#_tags");
    //绑定 多选框事件
    elt.tagsinput({
        allowDuplicates: true,
        confirmKeys: [13]
    });

    /*******************************************返回到请求页面 并将参数 写入查询框*****************************************************************/
    $('#'+$_active).trigger('click');
    elt.tagsinput('add', $point_condition);

    /*******************************************搜索框清除 ***********************************************************/
    $('#_clear').click(function(){
        $('#_tags').tagsinput('removeAll');
    })

    /***************************************  给选中框 添加 window.name 或 本地保存 *************************************************************************/
    var $checkbox;
    check_refresh();
    //加载 选中checkbox
    function check_refresh(){
        var $check_status = 1;
        var $_check = $('#_check');
        $('tr .table_checkbox').each(function(){
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                $(this).attr('checked', true);
            }
            else
            {
                $check_status = 0;
                $(this).attr('checked', false);
            }
        })
        //判断当前页点位是否全部被选中，如果是则将反选选中，否则取选中
        $check_status ? $_check.prop('checked', true) : $_check.prop('checked', false);
    }

    /*******************************************给每个checkbox框 添加window.name*******************************************************/
    $('tr .table_checkbox').change(function (e) {
        if (this.checked == true) {
            window.name += ',' + $(this).val();
        }
        else {
            window.name = window.name.replace(','+$(this).val(), '');
        }
        $checkbox = window.name;
        console.log(window.name)
    })


    /*******************************************    全选 取消 * 按钮****************************************************************/

    function check_all(){
        $('tr .table_checkbox').each(function(){
            //判断如果不是选中而且点击全选 才会把前前 点位的id放入window.name
            if(!$(this).prop('checked'))
            {
                window.name += ',' + $(this).val() ;
            }
            $(this).prop('checked',true);
        });
        console.log(window.name);
    }

    function check_none(){
        $('tr .table_checkbox').each(function(){
            $(this).filter(':checkbox').prop('checked',false);
            if((window.name+',').indexOf(','+$(this).val()+',') != -1)
            {
                window.name = window.name.replace(','+$(this).val(), '');
            }
        })
        console.log(window.name);
    }

    $('#_check').click(function(){
        var $status = $(this).prop('checked');
        $status ? check_all() : check_none();
    })

    /******************************** 判断是否 绑定点位 *****************************************************************/


    /******************************************* 按钮 事件 *****************************************************/
    //创建按钮
    $("#gd").click(function(){
        check_refresh();
        var set_val = $("#set_val").val();
        if(set_val == ''){
            $("#val_pro").text("请填写控制值！");
            $("#set_val").focus();
            setTimeout("$('#val_pro').text('')",5000);
            return false;
        }
        if(isNaN(set_val)){
            $("#val_pro").text("控制值为非数值！请重新填写.");
            $("#set_val").focus();
            setTimeout("$('#val_pro').text('')",5000);
            return false;
        }
        //请求地址
        var url = '/point_control/crud/quick-add?';
        //传递 参数
        var $form = $('#w0').serializeArray();
        //console.log($form);return false;
        var $param = '';
        $($form).each(function(k, v){
            console.log(v.name + ':' + v.value)
            //移除 checkbox 不完整的 数据
            if(v.name.indexOf('point_id') != -1)
            {
                console.log(v.name+'---------');
            }
            else
            {
                $param += v.name + '=' + v.value+'&';
            }
            //在此  可以移除

        })
        $param +='point_ids' + '=' + window.name;
        window.name = '';
        window.location.href = url+$param;
    })

    //事件列表
    $("#to_list").click(function(){
        //请求地址
        var url = '/system-public/alarm/list';
        window.location.href = url;
    })

    //下一步
    $("#to_next").click(function(){
        $("#wid_one").hide();
        $("#wid_two").show();
    })

    //上一步
    $("#to_last").click(function(){
        $("#wid_two").hide();
        $("#wid_one").show();
    })
}
</script>
