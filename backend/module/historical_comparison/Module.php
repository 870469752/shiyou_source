<?php

namespace backend\module\historical_comparison;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\historical_comparison\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
