<?php

namespace backend\module\historical_comparison\controllers;

use backend\controllers\MyController;
use backend\models\form\DataContrastForm;
use backend\models\PointData;
use backend\models\search\DataContrastSearch;
use Yii;
use common\library\MyFunc;
use backend\models\Point;
use common\library\MyExport;

class CrudController extends MyController
{
    /**
     * 因为导出报表和页面展示的报表是相同的 所以在此将导出和展示的主题放在同一个方法内，
     * 以后有什么需求可以更改
     * @return string
     */
    public function actionIndex()
    {
        $model = new DataContrastForm();
        $points = MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getAvailablePoint(), 'id', 'name'));
        if ($model->load(Yii::$app->request->get(), '')) {
            //获取 数据
            $data = $model->getCharData();
            //获取点位信息
            $point_info = self::getPointInfor($model->point_ids);
            //请求参数
            $query_info = Yii::$app->request->getQueryParams();
            /************************************考虑到方法臃肿 再次将导出和展示分开 ***************************/
            if(isset($query_info['query_type']) && $query_info['query_type'] == 'export'){
                //传递数据给导出方法
                $this->export($data, $point_info, $query_info);
            }
            //将需要的数据传递给 展示方法
            return $this->chartShow($points, $data,  $point_info, $query_info);
        } else {
            return $this->render('index',
                                ['points' => $points]);
        }
    }

    /**
     * 页面展示 图表 和 报表
     * @param $points       所有点位  id => name
     * @param $data         点位相关数据
     * @param $point_info   点位相关信息
     * @param $query_param  表单传递的参数
     * @return string
     */
    public function chartShow($points, $data, $point_info, $query_param)
    {
        //生成 highcharts 数据格式 暂时将数据处理放在这里 目前只有一个点位的对比简单处理 以后该成多个点位的话 需要修改处理方式
        $chart_data = [];
        $i = 0;
        if(!empty($data['data'])){
            foreach($data['data'] as $key => $value){
                $chart_data[$i]['data'] = $value;
                $chart_data[$i]['name'] =$key . ' & ' .MyFunc::DisposeJSON($point_info['name']);
                $chart_data[$i]['tooltip']['valueSuffix'] = ' '.$point_info['unit']['name'];
                $chart_data[$i]['xAxis'] = $i;
                ++$i;
            }
        }
        //获取表格数据对象
        $table_query = (isset($data['dataProvider']) && !empty($data['dataProvider'])) ? $data['dataProvider'] : '';
        $search_model = new DataContrastSearch();
        $dataProvider = $search_model->search($query_param, $table_query);
        return $this->render('index',
            ['points' => $points,
                'chart_data' => json_encode($chart_data),
                'unit' => $point_info['unit']['name'],
                'point_name' => MyFunc::DisposeJSON($point_info['name']),
                'point_id' => $query_param['point_ids'],
                'time_type' => $query_param['time_type'],
                'time_one' => $query_param['date_time'][0],
                'time_two' => $query_param['date_time'][1],
                'date_time' => implode(' & ', [implode(' - ', $data['time_format'][0]), implode(' - ', $data['time_format'][1])]),
                'dataProvider' => $dataProvider]);
    }

    /**
     * 报表导出
     * @param array $data  点位相关数据
     * @param $point_info  点位相关信息
     */
    public function export($data = [], $point_info, $query_param)
    {
        if(isset($data['data']) && !empty($data['data'])){
            //将需要的点位数据提出来
            $export_data = [];
            foreach($data['data'] as $time => $_data){
                foreach($_data as $key => $value){
                    if(isset($value[0]) && isset($value[1])){
                        $export_data[] = [date('Y-m-d H:i:s', ceil($value[0]/1000)), $value[1]];
                    }
                }
            }
            //报表名称
            switch($query_param['time_type']){
                case 'day':
                    $file_suffix = '历史同期天报表' .' (' .implode(' & ', $query_param['date_time']) .')';
                    break;
                case 'month':
                    $file_suffix = '历史同期月报表' .' (' .implode(' & ', $query_param['date_time']) .')';
                    break;
                case 'week':
                    $file_suffix = '历史同期周报表' .' (' .implode(' & ', [implode(' - ', $data['time_format'][0]), implode(' - ', $data['time_format'][1])]) .')';
            }
            $file_name =  MyFunc::DisposeJSON($point_info['name']) .$file_suffix;
            //传递数据 和 列名 及 导出报表的名称
            MyExport::run($export_data, [['时间', '数值('.$point_info['unit']['name'].')']], ['save_file' => ['file_name' => $file_name]]);
        }
    }

    /**
     * 根据点位id获取 点位的所有信息 包括点位的分类、单位等
     * @param string $ids
     * @return array|null|string|\yii\db\ActiveRecord
     */
    public static function getPointInfor($ids = '')
    {
        $Point_model = new Point();
        //获取 点位的信息 点位名称 点位单位
        if($ids){
            $point_info = $Point_model::getPointInformation($ids);
            return $point_info;
        }
        return '';
    }
}
