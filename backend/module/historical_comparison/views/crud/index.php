<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:24
 */
use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

TableAsset::register($this);
$this->title = Yii::t('point', 'Points');
$this->params['breadcrumbs'][] = $this->title;
//echo '<pre>';
//echo  Url::toRoute('page').'?'.http_build_query(Yii::$app->request->getQueryParams());die;
//var_dump(http_build_query(Yii::$app->request->getQueryParams()));die;
?>
<!-- html 区域 start-->
<section id="widget-grid" class="">
    <!-- 分隔提示线 -->
    <hr id = '_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id = '_loop' style = 'display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style = 'padding:0px 7px 15px 7px'>
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>对比参数设定</legend>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::dropDownList('point_ids', isset($point_id)?$point_id:null, $points, ['placeholder' => Yii::t('app', 'Choose Point'), 'class' => 'select2', 'id' => 'point'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::dropDownList('time_type', isset($time_type)?$time_type:null, ['' => '',
                                                                  'year' => Yii::t('app', 'Year'),
                                                                  'week' => Yii::t('app', 'Week'),
                                                                  'day' => Yii::t('app', 'Day'),
    //                                                              'custom' => Yii::t('app', 'Custom')
                                                                 ],
                            ['class' => 'select2', 'placeholder' => Yii::t('app', 'Time Type'), 'id' => 'time_type'])?>
                    </div>
                </div>

                <!-- 分格块 -->
                <div style = 'height:35px'></div>

                <!--对比时间选择-->
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::textInput('date_time[]', isset($time_one)?$time_one:null,  ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Contrast time one'), 'id' => '_time_one'])?>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <?=Html::textInput('date_time[]', isset($time_two)?$time_two:null,  ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Contrast time two'), 'id' => '_time_two'])?>
                    </div>
                </div>
             </fieldset>
         </div>
        <!--提交-->
        <div class="form-actions" style = 'margin-left:0px;margin-right:0px'>
            <div style = 'color:red;text-align: center; displau:none' id = '_error_log'></div>
            <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div  style = 'text-align: center'><i id = '_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify" data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->

    <!-- content  图表内容 start-->
    <div class="jarviswidget jarviswidget-color-blueDark"
         data-widget-deletebutton="false"
         data-widget-editbutton="false"
         data-widget-colorbutton="false"
         data-widget-sortable="false"
         data-widget-Collapse="false"
         data-widget-custom="false"
         data-widget-togglebutton="false" id = '_jarviswidget'>
        <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
            <h2><?=(isset($point_name) ? $point_name : '') .' 历史同期对比'?></h2>
            <div class="jarviswidget-ctrls">
                <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                    <i class="fa fa-bar-chart-o"></i>
                </a>
            </div>
        </header>
        <!-- widget div-->

        <div class="no-padding">

            <div class="form-group" id="chart_type" style = 'padding:10px 0px 10px'>
                <label class="col-md-1 control-label">图表类型:</label>
                <div class="col-md-10" style = 'border-bottom:1px solid #b2dba1'>
                    <label class="radio radio-inline"  style = 'margin-top:0px'>
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'line'>
                        <span>曲线图</span>
                    </label>
                    <label class="radio radio-inline">
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'bar'>
                        <span>柱状图</span>
                    </label>
                    <label class="radio radio-inline">
                        <input type="radio" class="radiobox" name="style-0a" data-value = 'area'>
                        <span>区域图</span>
                    </label>
                </div>
            </div>

            <div id="container_chart" style="height:500px"></div>
            <div id="container_table" style="height:500px;display:none">
                <div class="col-xs-2 col-md-1 col-sm-1 demo-icon-font pull-right">
                    <a href = <?=Url::toRoute('').'?query_type=export&'.http_build_query(Yii::$app->request->getQueryParams());?> rel = 'tooltip' data-original-title="导出" data-placement="bottom">
                        <i class="fa fa-save"> 保存</i>
                    </a>
                </div>
                <?php if(isset($dataProvider) && !empty($dataProvider)):?>
                    <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([

                        'formatter' => ['class' => 'common\library\MyFormatter'],
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover table-middle',
                            'style' => 'margin-top:20px'
                        ],
                        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                        'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                        'columns' => [
                            ['attribute' => '_timestamp', 'label' => '时间', 'headerOptions' => ['data-hide' => 'phone,tablet']],
                            ['attribute' => 'value', 'label' => '数值', 'format' => 'Floor', 'headerOptions' => ['data-hide' => 'phone,tablet']]
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                <?php endif;?>
            </div>
        </div>
    </div>
    <!-------     图表内容 end-->
</section>
<!-- html 区域 end-->
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<!-- js 脚本区域 start-->
<script>
    window.onload = function(){
        var $chart_data = <?= isset($chart_data) ? $chart_data : "''"?>;
        var test=JSON.stringify($chart_data);
        console.log(test);
        var $_unit = <?= isset($unit) ? "'" .$unit ."'" : "''"?>;
        var $_point_name = <?= isset($point_name) ? "'" .$point_name ."'" : "''"?>;
        var $_type = <?= isset($time_type) ? "'" .$time_type ."'" : "'day'"?>;
        var $_date_time = <?= isset($date_time) ? "'" .$date_time ."'" : "''"?>;
        var $_time_one = <?= isset($time_one) ? "'" .$time_one ."'" : "''"?>;
        var $_time_two = <?= isset($time_two) ? "'" .$time_two ."'" : "''"?>;


        if(!$chart_data){
            $('#_loop').show();
            $('#_jarviswidget').hide();
        }else{
            $('#_loop').hide();
            $('#_jarviswidget').show();
        }
        //对表单进行验证
        $("#_submit").click(function(){
            //获取所有 需要选择的参数
            var $point_id = $('#point').val();
            var $_type = $('#time_type').val();
            var $time_one = $('#_time_one').val();
            var $time_two = $('#_time_two').val();
            var $error_infor = '';
            var $error_signal = 0;

            if(!$point_id){
                $error_infor = '点位不能为空';
                $error_signal = 1;
            }
            if(!$_type){
                $error_infor = '时间类型不能为空';
                $error_signal = 1;
            }
            if(!$time_one || !$time_two){
                $error_infor = '对比时间不能为空';
                $error_signal = 1;
            }

            if($error_signal){
                $('#_error_log').text($error_infor);
                $('#_error_log').show();
                return false;
            }

        })


        switch($_type){
            case 'day':
                $_date_format = '%H:%M';
                $_mr = 3600;
                $_ch_type = '天';
                break;
            case 'week':
                $_date_format ='%d'; //'%e. %b';
                $_mr = 86400;
                $_ch_type = '周';
                break;
            case 'year':
                $_date_format = '%b';
                $_mr = 86400;
                $_ch_type = '年';
                break;
        }

        $('#_cl').click(function(){
            if($("#_loop").css('display') == 'none'){
                $('#_line').hide();
            }else{
                $('#_line').show();
                if($chart_data.length){$('#_jarviswidget').show();}
            }
            $("#_loop").slideToggle("slow");

        });

        $("#time_type").change(function (){
            _type = $(this).val();
            timeTypeChange(_type, $('#_time_one'));
            timeTypeChange(_type, $('#_time_two'));
        });
        $("#time_type").trigger('change');
        $('#_time_one').val($_time_one);
        $('#_time_two').val($_time_two);
        // 图形
        highcharts_lang_date();
        highChart('line');
        // highchart 代码
        function highChart($chart_type){
            $('#container_chart').highcharts({
                chart: {
                    zoomType: 'x',
                    type: $chart_type
                },

                title: {
                    text: $_point_name + ' 同期'+$_ch_type+'对比 ',
                    x: -20 //center
                },
                subtitle: {
                    text: $_date_time,
                    x: -20
                },
                xAxis: [{
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        day: $_date_format
                    },
                    minRange: $_mr * 1000
//                reversed: true
                }, {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        day: $_date_format
                    },
                    minRange: $_mr * 1000,
                    opposite: true
//                reversed: true
                }],
                yAxis: {
//                reversed: true,
                    title: {
                        text: $_point_name + ' (' + $_unit + ')'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueDecimals: 2,
                    shared: true,
                    formatter: function (){
                        if($_type != 'week'){
                            var cc = '<b>时间:' +Highcharts.dateFormat($_date_format, this.x) + '</b>';
                        }
                        else{
                            //向前 推一天 才是从星期一开始
                            var cc = '<b>时间:' +Highcharts.dateFormat('%A', this.x - 86400*1000) + '</b>';
                        }
                        $.each(this.points, function(i, point) {
                            cc += '<br />' + "<span style = 'color:"+point.series.color+"'><b>" + this.series.name + ": " + this.y + ' ' + $_unit + '</b></span>';
                        });
                        return cc;
                    }
                },
                plotOptions: {
                    spline: {
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        },
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        }
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'top',
                    x: 0,
                    y: 70
                },
                series: $chart_data
            });

        }

        // 图、表切换

        $('#_chart_switch').click(function(){
            var $_chart_type = $(this).data('type');
            if($_chart_type == 'chart'){
                $('#container_chart').hide();
                $('#chart_type').hide();
                $('#container_table').show();
                $(this).data('type', 'table');
            }else{
                $('#container_chart').show();
                $('#chart_type').show();
                $('#container_table').hide();
                $(this).data('type', 'chart');
            }
        });

        //图形样式切换
        $('.radiobox').click(function(){
            var $_chart_type = $(this).data('value');
            if($_chart_type){
                highChart($_chart_type);
            }
        })
    }
</script>
<!-- js 脚本区域 end-->