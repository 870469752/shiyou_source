<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\CustomReportSearch $searchModel
 */

TableAsset::register($this);
$this->title = Yii::t('app', 'Custom Reports');
$this->params['breadcrumbs'][] = $this->title;

?>
<section id="widget-grid" class="">

    <!-- 分隔提示线 -->
    <hr id='_line' style="margin:0px;height:1px;border:0px;background-color:#D5D5D5;color:#D5D5D5;"/>

    <!-- 参数主体 start-->
    <div id='_loop' style='display: none;border:2px solid #D5D5D5;padding-bottom:15px'>
        <div style='padding:0px 7px 15px 7px;'>
            <?php $form = MyActiveForm::begin(['method' => 'get']) ?>
            <fieldset>
                <legend>统计参数设定</legend>
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <?=Html::hiddenInput('id', Yii::$app->request->get('id'))?>
                    <div class="col-md-3">
                        <?=
                        Html::dropDownList(
                            'time_type',
                            isset($param['time_type']) ? $param['time_type'] : null,
                            [
                                '' => '',
                                'year' => Yii::t('app', 'Year'),
                                'month' => Yii::t('app', 'Month'),
                                'week' => Yii::t('app', 'Week'),
                                'day' => Yii::t('app', 'Day'),
                            ],
                            [
                                'class' => 'select2',
                                'id' => 'time_type',
                                'title' => Yii::t('app', 'Time Type'),
                                'placeholder' => Yii::t('app', 'Time Type'),
                                'id' => 'time_type'
                            ]
                        )?>
                    </div>

                    <div class="col-md-3">
                        <?= Html::textInput(
                            'date_time',
                            isset($param['date_time']) ? $param['date_time'] : null,
                            [
                                'id' => 'date_time',
                                'class' => 'form-control',
                                'title' => Yii::t('app', 'Select Time'),
                                'placeholder' => Yii::t('app', 'Select Time'),
                            ]
                        ) ?>
                    </div>

                    <div class="col-md-3">
                        <?=
                        Html::dropDownList(
                            'statistic_type',
                            isset($param['statistic_type']) ? $param['statistic_type'] : null,
                            [
                                '' => '',
                                'sum' => Yii::t('statistic_data', 'Sum'),
                                'avg' => Yii::t('statistic_data', 'Avg'),
                                'max' => Yii::t('statistic_data', 'Max'),
                                'min' => Yii::t('statistic_data', 'Min')
                            ],
                            [
                                'id' => 'statistic_type',
                                'class' => 'select2',
                                'title' => Yii::t('app', 'Statistics Type'),
                                'placeholder' => Yii::t('app', 'Statistics Type'),
                            ]
                        )?>
                    </div>
                </div>

                <!-- 分格块 -->
                <div style='height:35px'></div>

            </fieldset>
        </div>
        <!--提交-->
        <div class="form-actions" style='margin-left:0px;margin-right:0px'>
            <span id='error_mes' class='pull-left' style = 'color:red'></span>

            <div style='color:red;text-align: center; displau:none' id='_error_log'></div>
            <?= Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
        </div>
        <?php MyActiveForm::end(); ?>
    </div>
    <div style='text-align: center'><i id='_cl' rel="tooltip" data-placement="bottom" class="fa fa-align-justify"
                                       data-original-title="点击收缩"></i></div>
    <!-- 参数主体 end-->


    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= isset($report_name) ? $report_name : Html::encode($this->title) ?></h2>

                    <div class="jarviswidget-ctrls">
                        <a href= <?= Url::toRoute('statistic-export') . '?' . http_build_query(
                            Yii::$app->request->getQueryParams()
                        ); ?> id="aSave" class="button-icon" rel="tooltip" data-original-title="保存报表"
                           data-placement="bottom">
                            <i class="fa fa-save"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div>

                    <?=
                    GridView::widget(
                        [
                            'formatter' => ['class' => 'common\library\MyFormatter'],
                            'dataProvider' => $dataProvider,
                            'options' => ['class' => 'widget-body no-padding'],
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover',
                                'width' => '100%',
                                'id' => 'datatable'
                            ],
                            'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
                            'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
                            'columns' => $col,
                        ]
                    ); ?>

                    <!-- 搜索表单和JS -->
                </div>
            </div>
        </article>
    </div>


</section>

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function () {
        var $_time_type = <?= isset($param['time_type']) ? "'" .$param['time_type'] ."'" : "''"?>;
        var $_date_time = <?= isset($param['date_time']) ? "'" .$param['date_time'] ."'" : "''"?>;

        $('#_cl').click(function () {
            if ($("#_loop").css('display') == 'none') {
                $('#_line').hide();
            } else {
                $('#_line').show();
            }
            $("#_loop").slideToggle("slow");

        });

        //根据所选择的时间统计类型 来初始化时间插件
        $('#time_type').change(function () {
            var $_type = $(this).val();
            console.log($_type);
            timeTypeChange($_type, $('#date_time'));
        });

        //验证提交数据
        $('#_submit').click(function () {
            if (!$('#time_type').val()) {                   //time_type
                $('#error_mes').text('时间类型不能为为空！');
                return false;
            } else if (!$('#date_time').val()) {            //date_time
                $('#error_mes').text('请选择正确的时间！');
                return false;
            } else if (!$('#statistic_type').val()) {       //statistic_type
                $('#error_mes').text('统计类型不能为空！');
                return false;
            }
        })
        timeTypeChange($_time_type, $('#date_time'));
        $('#date_time').val($_date_time);
    }
</script>