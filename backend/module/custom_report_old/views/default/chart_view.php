<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use common\library\MyActiveForm;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\CustomReportSearch $searchModel
 */
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Custom Reports');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
        <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
        <div class="jarviswidget jarviswidget-color-blueDark"
             data-widget-deletebutton="false"
             data-widget-editbutton="false"
             data-widget-colorbutton="false"
             data-widget-sortable="false">
            <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                <h2><?= isset($report_name) ? $report_name : Html::encode($this->title) ?></h2>
                <div class="jarviswidget-ctrls">

                </div>
            </header>

<!-- widget div-->
<div role="content">

<!-- widget edit box -->
<div class="jarviswidget-editbox">
    <!-- This area used as dropdown edit box -->

</div>
<!-- end widget edit box -->

<!-- widget content -->
<div class="widget-body">

<div class="tab-content">
<div class="tab-pane active" id="hr2">

    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#iss1" id = 'chart_all' style = 'color:#472525!important' data-toggle="tab" data-type = 'all'>双轴</a>
        </li>
        <li class="">
            <a href="#iss2" id = 'chart_left' style = 'color:#472525!important' data-toggle="tab" data-type = 'left'>左轴</a>
        </li>
        <li class="">
            <a href="#iss3" id = 'chart_right' style = 'color:#472525!important' data-toggle="tab" data-type = 'right'>右轴</a>
        </li>

    </ul>
</div>
<div class="tab-content padding-10">
<div class="tab-pane active" id="iss1">
    <div class="widget-body">
        <div id = 'all_content' style="height:450px;"></div>
    </div>
</div>

<div class="tab-pane fade" id="iss2">
    <div class="widget-body">
        <div id = 'left_content' style="height:450px;"></div>
    </div>
</div>

<div class="tab-pane fade" id="iss3">
    <div class="widget-body">
        <div id = 'right_content' style="height:450px;"></div>
    </div>
</div>

</div>

</div>

</div>
</div>

</div>

</section>

<?php
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
?>


<script>
    window.onload = function () {
        //chart 中保持不变的量
        var $point_info   = <?=isset($point_info) ? json_encode($point_info) : '{}'?>;
        var $left_unit = <?=isset($left_unit) ? "'" .$left_unit ."'" : ''?>;
        var $right_unit  = <?=isset($right_unit) ? "'" .$right_unit ."'" : ''?>;

        yAxisCreate('all');

        //y轴 构建function
        function yAxisCreate($type)
        {
                $yAxisOptions = [];     //y轴数组
                 switch ($type) {
                    case 'all':
                        $yAxisOptions[0] = {
                                            title: {

                                                text: '左轴点位(' + $left_unit + ')'
                                            },
                                            labels: {
                                                formatter: function() {
                                                    return this.value;
                                                }
                                            },
                            opposite:false
                                        };
                        $yAxisOptions[1] =  {
                                            title: {

                                                text: '右轴点位(' + $right_unit + ')'
                                            },
                                            labels: {
                                                formatter: function() {
                                                    return this.value;
                                                }
                                            },
                            opposite : true

                                        }
                        break;
                    case 'left':
                        $yAxisOptions[0] = {
                                            title: {

                                                text: '左轴点位(' + $left_unit + ')'
                                            },
                                            labels: {
                                                formatter: function() {
                                                    return this.value;
                                                }
                                            },
                            opposite:false
                                        };
                        break;
                    case 'right':
                        $yAxisOptions[0] = {};
                                $yAxisOptions[1] = {
                                                        title: {

                                                            text: '右轴点位'+ $right_unit
                                                        },
                                                        labels: {
                                                            formatter: function() {
                                                                return this.value;
                                                            }
                                                        }
                                                    };
                        break;
                }

            ajaxGetData(<?="'" .Url::toRoute('ajax-chart-data') .
                           '?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?> +
                            "&type=" + $type,
                            $type + '_content', $yAxisOptions);
        }

        function afterSetExtremes(e)
        {
            chart.showLoading('Loading data from server...');
            $.getJSON(<?="'" .Url::toRoute('ajax-chart-data') .
                           '?'.http_build_query(Yii::$app->request->getQueryParams()) ."'";?> +
                "&type=" + $type +
                '&start_time=' + e.min +
                '&end_time=' + e.max,
                function (result) {
                    // 他必须得一组一组数据插入
                    for (var i = 0; i < result.length; i++) {
                        console.log(result[i].data);

                        chart.series[i].setData(result[i].data);
                    }
                    chart.hideLoading();
                });
        }

        //数据获取
        function ajaxGetData(url, content, $yAxisOptions)
        {
            $.getJSON(url, function (data) {
                chart = new Highcharts.StockChart({
                    chart: {
                        renderTo: content,
                        borderWidth: 0,
                        borderRadius: 0,
                        backgroundColor: 'whiteSmoke',
                        type: 'spline',
                        zoomType: 'x'
                    },
                    credits : {
                        enabled : false
                    },
                    legend: {
                        enabled: true,
                        align: 'center',
                        verticalAlign: 'top',
                        borderWidth: 2,
                        itemStyle: {
                            fontSize: '14px'
                        }
                    },
                    rangeSelector : {
                        inputEnabled : false,
                        buttons: [ {
                            type: 'day',
                            count: 1,
                            text: '1d'
                        }, {
                            type: 'day',
                            count: 15,
                            text: '15d'
                        }, {
                            type: 'month',
                            count: 1,
                            text: '1m'
                        }, {
                            type: 'month',
                            count: 6,
                            text: '6m'
                        }, {
                            type: 'year',
                            count: 1,
                            text: '1y'
                        }, {
                            type: 'all',
                            text: 'All'
                        }],
                        selected : 0
                    },

                    navigator : {
                        adaptToUpdatedData: false,
                        series : {
                            data : data
                        }
                    },
<!--                    exporting : {-->
<!--                        url: '--><?//=site_url('export/index')?><!--',-->
<!--                        buttons : {-->
<!--                            printButton : {-->
<!--                                enabled : false-->
<!--                            }-->
<!--                        }-->
<!--                    },-->

                    xAxis : {
                        events : {
                            afterSetExtremes : afterSetExtremes
                        },
                        minRange: 3600 * 1000
                    },
                    yAxis: $yAxisOptions,


                    tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                        valueDecimals: 2,
                        shared : true,
                        style : {
                            //color : 'white',
                            padding : '7px',
                            fontsize : '9pt'
                        }
                    },
                    series: data
                });
            });
        }

        //标签点击事件
        $('.nav li a').click(function() {
            console.log(2222);
            $type = $(this).data('type');
            yAxisCreate($type);

        });
    }
</script>