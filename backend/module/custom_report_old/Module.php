<?php

namespace backend\module\custom_report;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\custom_report\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
