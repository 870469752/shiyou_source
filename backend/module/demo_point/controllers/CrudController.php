<?php

namespace backend\module\demo_point\controllers;

use Yii;
use backend\models\DemoPoint;
use backend\models\search\DemoPointSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Unit;
use common\library\MyFunc;
/**
 * CrudController implements the CRUD actions for DemoPoint model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DemoPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DemoPointSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single DemoPoint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DemoPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DemoPoint;

        if ($model->load($data=Yii::$app->request->post()) && $result = $model->saveDemoPoint()) {
            /*操作日志*/
            $info = MyFunc::DisposeJSON($model->name);
            $op['zh'] = '创建统计点位';
            $op['en'] = 'Create demo points';
            @$this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $unit = Unit::getAllUnit();
            return $this->render('create', [
                'model' => $model,
                    'unit' => $unit,
            ]);
        }
    }

    /**
     * Updates an existing DemoPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $protocol_id = '')
    {
        $model = $this->findModel($id);
            echo'<pre>';
            //print_r($model);
        $params = Yii::$app->request->getBodyParams();
        if ($model->load(Yii::$app->request->post()) && $result = $model->saveDemoPoint()) {

            //print_r($model);
            print_r(Yii::$app->request->post());
            die;
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改统计点位';
            $op['en'] = 'Update demo points';
            @$this->getLogOperation($op,$info,$result);

            //判断当前修改是哪个控制器请求的 如果protocol_id不为空说明来自point/crud的请求在执行完毕后跳转回point/crud,否则跳到当前控制器
            if($params['protocol_id']) {
                return $this->redirect(['/point/crud']);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            $model->name = Myfunc::DisposeJSON($model->name);
            $unit = Unit::getAllUnit();
            return $this->render('update', [
                'protocol_id' => $protocol_id,
                'model' => $model,
                    'unit' => $unit,
            ]);
        }
    }

    /**
     * Deletes an existing DemoPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '修改一个统计点位';
        $op['en'] = 'Update demo points';
        @$this->getLogOperation($op,'','');
        return $this->redirect(['index']);
    }

    /**
     * Finds the DemoPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DemoPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DemoPoint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
