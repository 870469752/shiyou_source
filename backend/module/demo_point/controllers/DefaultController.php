<?php

namespace backend\module\demo_point\controllers;

use backend\controllers\MyController;
use backend\models\PointData;
use Yii;
class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
//SELECT * FROM "core"."point_data" where "timestamp" >= '2015-03-26' and point_id = 2;
    public function actionUpdate()
    {
        $model = new PointData();
        ini_set('max_execution_time', '0');
        $start = strtotime ( '2015-06-1 00:00:00' );
        $end = strtotime ( '2015-06-16 08:35:00' );
        $step = 60;
        for($i = $start; $i <= $end; $i += $step){
            $num = mt_rand(23, 189) +  sprintf("%.2f",rand(0,100)/100);
            $time = date ( 'Y-m-d H:i:s', $i );
            Yii::$app->db->createCommand("insert into core.point_data(point_id, value, timestamp) values(4, {$num}, '{$time}')")->queryAll();
        }
        die;
    }
}
