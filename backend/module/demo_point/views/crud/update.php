<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DemoPoint $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Demo Point',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'protocol_id' => $protocol_id,
        'model' => $model,
        'unit' => $unit,
    ]) ?>
