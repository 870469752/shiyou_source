<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\DemoPoint $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Demo Point',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'unit' => $unit,
    ]) ?>
