<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\DemoPoint $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                        <?= Html::hiddenInput('protocol_id', isset($protocol_id)?$protocol_id:'')?>

                        <?= $form->field($model, 'name')->textInput() ?>

                        <?= $form->field($model, 'interval')->dropDownList([
                                '' => '',
                                300 => '5分钟',
                                900 => '15分钟',
                                1800 => '30分钟',
                            ],['class' => 'select2', 'placeholder' => '选择记录间隔']) ?>

						<?= Html::hiddenInput('DemoPoint[protocol_id]', 100) ?>


						<?= $form->field($model, 'base_value')->textInput() ?>

                        <?= $form->field($model, 'max_value')->textInput(['id' => '_max']) ?>

                        <?= $form->field($model, 'min_value')->textInput(['id' => '_min']) ?>

                        <?=$form->field($model, 'unit')->dropDownList(
                            $unit,
//                            ArrayHelper::map(Unit::getAllUnit(), 'id', 'unit'),
                            ['class'=>'select2', 'style' => 'width:200px']
                        )?>

                        <?= $form->field($model, 'value_type')->dropDownList([
                                                                            '' => '',
                                                                            1 => '实时量',
                                                                            2 => '累计量',
                                                                            3 => '开关量',
                            ],['class' => 'select2', 'placeholder' => '选择数值类型']) ?>


						<?= $form->field($model, 'is_shield')->checkbox() ?>

						<?= $form->field($model, 'is_upload')->checkbox() ?>

						<?= $form->field($model, 'is_record')->checkbox(['id' => '_record']) ?>


						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<script>
    window.onload = function(){
        $('.form-actions .btn-success').click(function(){
            var $_min = $('#_min').val();
            var $_max = $('#_max').val();
            if($_max - $_min <= 0){
                $('.form-actions').before("<div class='help-block' style ='color:red'>最大值不能小于最小值</div>");
                return false;
            }
        })
    }
</script>