<?php

namespace backend\module\demo_point;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\demo_point\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
