<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmLog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2>日志-报警散点图</h2>

                </header>

                <!-- widget div-->
                <div id = 'chart_content' class="widget-body">
                    <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'type')->dropDownList([
                                'month' => Yii::t('app', 'Month'),
                                'week' => Yii::t('app', 'Week'),
                                'day' => Yii::t('app', 'Day')],['class'=>'select2', 'diaplay'=>'inline']); ?>
                        <?= $form->field($model, 'time')->textInput(['id' => "timepick"])?>
                    <?php ActiveForm::end(); ?>
                    <!-- widget edit box -->
                        <!-- This area used as dropdown edit box -->
                    <div class="row">
                        <div class="btn-group col-xs-offset-11 col-xs-1">
                            <a href="javascript:void(0)" class="btn btn-default btn-xs"
                               id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                            <a href="javascript:void(0)" class="btn btn-default btn-xs"
                               id="btn-next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div id="alarm_chart"></div>
                    <!-- end widget edit box -->


                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

$this->registerJsFile("js/my_js/scatter.js", ['backend\assets\AppAsset']);
?>
