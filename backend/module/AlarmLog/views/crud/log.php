<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\AlarmLogSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = Yii::t('app', 'Alarm Logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div >
    <div class="row" >
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-colorbutton="false"
                 data-widget-editbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div class="jarviswidget-ctrls">
                        <a id="_confirm" class="button-icon" href="#" data-toggle="modal" data-target="#SaveMenu" rel="tooltip" data-original-title="确认本页" data-placement="bottom">
                            <i class="fa fa-star-o"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div id = 'content'>
                        <table id = 'jqGrid'></table>
                        <div id="pjqgrid"></div>

                </div>
            </div>
        </article>
        </div>
    </div>
</section>

<?php
$this->registerJsFile("js/plugin/jqgrid/jqGrid_4.8.2/grid.locale-cn.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/jqgrid/jqGrid_4.8.2/jquery.jqGrid.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/jqGrid/ui.jqgrid.css", ['backend\assets\AppAsset']);
?>

<script>
    window.onload = function(){
        var lastsel;
        $("#jqGrid").jqGrid({
            url: '<?=Url::toRoute('ajax-get-table-data')?>',
            datatype: "json",
            height : 'auto',
            rownumbers: true,
            colNames:['<?=Yii::t('app', 'Log ID')?>',
                '<?=Yii::t('app', 'Confirm')?>',
                '<?=Yii::t('app', 'Level')?>',
                '<?=Yii::t('app', 'Send Status')?>',
                '<?=Yii::t('app', 'Start Time')?>',
                '<?=Yii::t('app', 'End Time')?>',
                '<?=Yii::t('alarm', 'Description')?>',
                '<?=Yii::t('alarm', 'System Log')?>',
                '<?=Yii::t('alarm', 'Remark')?>',
                '<?=Yii::t('app', 'Action')?>',
            ],
            colModel: [
                {
                    name: 'id',
                    key: true,
                    editable: false,
                    align: 'center'
                },

                {
                    name: 'is_confirmed',
                    editable: false,
                    align: 'center',
                    editrules: {
//                        custom_func: validatePositive,
                        required: true
                    },
                    formatter:function(cellvalue, options, rowObject){
                        if(cellvalue == 1)
                            return '是';
                        else
                            return '否';
                    }
                },
                {
                    name: 'alarm_level',
                    editable: false,
                    align: 'center'
                },
                {
                    name: 'send_status',
                    editable: false,
                    align: 'center'
                },
                {
                    name: 'start_time',
                    editable: false,
                    align: 'center'

                },
                {
                    name: 'end_time',
                    editable: false,
                    align: 'center'
                },
                {
                    name: 'description',
                    editable: true,
                    align: 'center'
                },
                {
                    name: 'system_log',
                    editable: false,
                    align: 'center'
                },
                {
                    name: 'remark',
                    editable: true,
                    align: 'center'
                },
                {name:'act',index:'act', align: 'center'}
            ],
            rowNum : 10,
            rowList : [10, 20, 30],
            pager : '#pjqgrid',
            sortname: 'id',
            viewrecords: true,
            sortorder: "desc",
            gridComplete: function(){
                var ids = jQuery("#jqGrid").jqGrid('getDataIDs');
                for(var i=0;i < ids.length;i++){
                    var cl = ids[i];
                    de = "<a href = '<?=Url::toRoute('delete')?>?id="+cl+"' title= '删除' class='btn btn-xs btn-default' data-method='post' data-confirm='您确定要删除此项吗？'><i class='fa fa-times'></i></a>";
                    cf = "<a href = '<?=Url::toRoute('confirm')?>?id="+cl+"' title= '确认报警信息' class='btn btn-xs btn-default' data-method='post' onclick=\"event.stopPropagation();\"><i class='fa fa-check _confirm'></i></a>";
                    jQuery("#jqGrid").jqGrid('setRowData',ids[i],{act:de + cf});
                }
            },
            onSelectRow: function(id){
                if(id && id!==lastsel){
                    jQuery('#jqGrid').jqGrid('restoreRow',lastsel);
                    jQuery('#jqGrid').jqGrid('editRow',id,true);
                    lastsel=id;
                }
            },
            autowidth : true,
//            multiselect : true,
            editurl: '<?=Url::toRoute('ajax-edit-table-data')?>'
        });


        jQuery("#jqGrid").jqGrid('navGrid',"#pjqgrid",{edit:false,add:false,del:false,search:false});

        $('.jarviswidget').on('resize', function () {
            var $_width = $("#jarviswidget-fullscreen-mode").width();
            $("#jqGrid").jqGrid( 'setGridWidth', ($_width == null ? $('#content').width() : $_width) - 60);
        })




        /**********************************获取当前页报警记录确认数 **********************************************/
        function currntPageLog()
        {
            var $_yes = 0;
            var $_no = 0;
            var $_class = 'fa fa-star-0';
            $('tr').each(function(){
                var $this = $(this);
                var $_confirm = $this.find('td').eq(2).text();
                var $_action = $this.find('._conf');
                if($_confirm == '是' || $_confirm == 'Yes')
                {
                    $_action.css('color', '#A8A0A0');
                    $_action.css('pointer-events', 'none');
                    ++$_yes;
                }else if($_confirm == '否' || $_confirm == 'no')
                {
                    $_action.css('color', '#64E84E');
                    ++$_no;
                }
            })

            if($_yes && $_no){
                //如果 既含有确认的 也含有未确认的
                $_class = 'fa fa-star-half-empty';
            }
            else if($_yes && !$_no){
                $_class = 'fa fa-star';
            }
            else{
                $_class = 'fa fa-star-o';
            }
            $('#_confirm').find('i').removeClass().addClass($_class);
        }


        /*************************  获取 当前页面所有的记录对应的 记录id（data-key）****************************/
        $('#_confirm').click(function(){
            var $ids = '';
            //将本页所有报警日志的确认显示都先变为是
            $('tr').each(function(){
                var $this = $(this);
                var $_action =  $this.find('._conf');
                $this.find('td').eq(2).text('是');
                $ids += $(this).data('key') ? $(this).data('key') + ',' : '';
                $_action.css('color', '#A8A0A0');
                $_action.css('pointer-events', 'none');
            })
            $.get("/alarm-log/crud/confirm-all", 'ids=' + $ids, function(data){
            });
            //移除事件冒泡
            event.stopPropagation();
            $(this).find('i').removeClass().addClass('fa fa-star');
            //刷新提示框
            $('#_load').trigger('click');
        })
        //调用显示报警确认情况
        currntPageLog();

        $('._status').on('click', function() {
            $(this).next().toggle();
        })

            $("input").removeClass("ui-corner-all");


    };


</script>
