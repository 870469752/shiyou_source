<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\AlarmLogSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Alarm Logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row" >
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark"
			data-widget-deletebutton="false"
			data-widget-colorbutton="false"
            data-widget-editbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>
                <div class="jarviswidget-ctrls">
                    <a id="_confirm" class="button-icon" href="#" data-toggle="modal" data-target="#SaveMenu" rel="tooltip" data-original-title="确认本页" data-placement="bottom">
                        <i class="fa fa-star-o"></i>
                    </a>
                </div>
			</header>
			<!-- widget div-->
			<div>
                <?= GridView::widget([
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

            ['attribute' => 'alarm_level', 'headerOptions' => ['data-hide' => 'phone']],
            ['attribute' => 'is_confirmed', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'send_status', 'format' => ['status'], 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'start_time', 'format' => 'TimeFormatter', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'end_time', 'format' => 'LogEndTime', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'description', 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone,tablet']],

            ['class' => 'common\library\MyActionColumn', 'header' => Yii::t('app', 'Action'), 'template' => '{confirm} {delete}','headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>

<?php
// 搜索和创建按钮
$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$search_button}');
//options.button.push('{$create_button}');
table_config('datatable', options);

//确认操作 点击效果
$('#datatable tbody tr').each( function(){
    var _is = $(this).find('td').eq(2).text();
    if(_is == '是' || _is == 'Yes')
    {
        console.log(_is);
        $(this).css('color', '#0D942E')
    }
    else
    {
        $(this).css('color', '#5D1371')
    }
})


$("#datatable a").on('click', function(){
    var _id = $(this).attr('id');
    $("#_status"+_id).toggle();
})

JAVASCRIPT;
$this->registerJs ( $js_content, View::POS_READY);
?>
<!-- 搜索表单和JS -->
<?php //MyFunc::TableSearch($searchModel) ?>

    			</div>
			</div>
		</article>
	</div>
</section>

<!-- 添加搜索框 -->
<?php echo $this->render('//layouts/public/search',['searchModel' => $searchModel])?>

<script>
    window.onload = function(){
        /**********************************获取当前页报警记录确认数 **********************************************/
        function currntPageLog()
        {
            var $_yes = 0;
            var $_no = 0;
            var $_class = 'fa fa-star-0';
            $('tr').each(function(){
                var $this = $(this);
                var $_confirm = $this.find('td').eq(2).text();
                var $_action = $this.find('._conf');
                if($_confirm == '是' || $_confirm == 'Yes')
                {
                    $_action.css('color', '#A8A0A0');
                    $_action.css('pointer-events', 'none');
                    ++$_yes;
                }else if($_confirm == '否' || $_confirm == 'no')
                {
                    $_action.css('color', '#64E84E');
                    ++$_no;
                }
            })

            if($_yes && $_no){
                //如果 既含有确认的 也含有未确认的
                $_class = 'fa fa-star-half-empty';
            }
            else if($_yes && !$_no){
                $_class = 'fa fa-star';
            }
            else{
                $_class = 'fa fa-star-o';
            }
            $('#_confirm').find('i').removeClass().addClass($_class);
        }


        /*************************  获取 当前页面所有的记录对应的 记录id（data-key）****************************/
        $('#_confirm').click(function(){
            var $ids = '';
            //将本页所有报警日志的确认显示都先变为是
            $('tr').each(function(){
                var $this = $(this);
                var $_action =  $this.find('._conf');
                $this.find('td').eq(2).text('是');
                $ids += $(this).data('key') ? $(this).data('key') + ',' : '';
                $_action.css('color', '#A8A0A0');
                $_action.css('pointer-events', 'none');
            })
            $.get("/alarm-log/crud/confirm-all", 'ids=' + $ids, function(data){
            });
            //移除事件冒泡
            event.stopPropagation();
            $(this).find('i').removeClass().addClass('fa fa-star');
            //刷新提示框
            $('#_load').trigger('click');
        })
        //调用显示报警确认情况
        currntPageLog();
    }
</script>
