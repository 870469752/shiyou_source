<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\AlarmLog $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Alarm Log',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
