<?php

use yii\helpers\Html;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = '报警查看';
$this->params['breadcrumbs'][] = $this->title;
?>

    <style>
        a{text-decoration: none;color:inherit;}
        table tr td{text-align: center}
        table tr th{text-align: center}
    </style>
    <a  id="show_vedio_div"  data-toggle="modal" data-target="#myModal" ></a>
    <section id="widget-grid" class="">
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-sortable="false">
                    <header><span class="widget-icon"><i class="fa fa-table"></i></span>
                        <h2><?= Html::encode($this->title) ?></h2>
                        <div style="float: right;padding-right: 40px;padding-bottom: 10px">
                            弹出视频：<input type="radio" name="judge_video" value="1">是&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="judge_video" value="0" checked>否
                        </div>
                    </header>
                    <div class="well well-sm well-light">
                        <table id="datatable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th data-class="expand" >日期</th>
                                <th data-class="expand" >报警点</th>
                                <th data-class="expand" >描述</th>
                                <th data-class="expand">等级</th>
                                <th data-class="expand">恢复时间</th>
                                <th data-class="expand">状态</th>
                                <th data-class="expand">操作</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
            </article>
        </div>

    </section>
    <!-- END MAIN CONTENT -->



    <script>

    window.onload = function() {
        var oTable;
        var g_iWndIndex = 0; //可以不用设置这个变量，有窗口参数的接口中，不用传值，开发包会默认使用当前选择窗口
        szPort = 80;
        szUsername = "admin";
        szPassword = "12345";
        indexNum = 0;
        $(document).ready(function () {
            $('#tabs').tabs();
            getRealTimeAlarmInfo(0);
            setInterval(function(){myrefresh();},1800000);
        });
    }
    //定时刷新页面
    function myrefresh()
    {
        window.location.reload();
    }

    /**
     * 确认报警信息
     * @private
     */
    function toConfirm(id) {
        $.ajax({
            url: '/alarm-log/default/confirm?id='+id,
            async: true,
            success: function (data) {
                getRealTimeAlarmInfo(0);
            }
        });
    }

    /**
     * 查看报警点位关联视频
     * @private
     */
    function toShowVideo(id) {
        indexNum = 0;
        $.ajax({
            url: '/alarm-log/default/show-video?id='+id,
            async: true,
            success: function (data) {
                //console.log(data);return false;
                dataChange(data);
            }
        });
    }

    /**
     * 查看报警点位关联视频
     * @private
     */
    function autoShowVideo(log_id) {
        indexNum = 0;
        $.ajax({
            url: '/alarm-log/default/auto-show-video?log_id='+log_id,
            async: true,
            success: function (data) {
                if(data){
                    dataChange(data);
                }
                return false;
            },
            complete: function (XHR, TS) { XHR = null }
        });
    }

    function dataChange(data){
        var temp_data = JSON.parse(data);
        var temp_len = temp_data.length;
        var num = 1;
        if(temp_len>1 && temp_len<=4){
            num = 2;
        }else if(temp_len>4 && temp_len<=9){
            num = 3;
        }else if(temp_len>9 && temp_len<=16){
            num = 4;
        }else if(temp_len>16){
            num = 4;
            temp_data = temp_data.slice(0,16);
        }
        var tem_class = $("#myModal").attr("class");
        if(tem_class == "modal"){
            $("#show_vedio_div").click();
        }
        $("#divPlugin").text("");
        init_env(num);
        camera_patrol(temp_data);
    }

    /**
     * 获取实时报警信息
     * @private
     */
    function getRealTimeAlarmInfo(log_id) {
        $.ajax({
            type: 'POST',
            dataType: "json",
            async: true,
            url: '/alarm-log/default/get-day-alarm-log',
            success: function (data) {
                var str = '';
                if(data.length>0){
                    for(var j in data){
                        if(data[j].is_confirmed){
                            data[j].is_confirmed='已确认';
                            tem_style ='color:green;algin:center';
                        }else{
                            data[j].is_confirmed='待确认';
                            tem_style ='color:#ff0000;algin:center';
                        }
                        if(data[j].end_time){
                            data[j].is_confirmed='已恢复';
                            tem_style ='color:black;algin:center';
                        }
                        str += '<tr style="'+tem_style+'"><td>' + data[j].start_time.substr(0,19)+"</td><td>" + data[j].p_name+"</td><td>" + data[j].description+"</td><td>" + data[j].alarm_level+"</td><td>" + data[j].end_time+"</td><td>" + data[j].is_confirmed+"</td><td><button style='color: #000000;height: 21px' onclick=toConfirm("+data[j].id+")>确认</button>";
                        if(data[j].p_link){
                            str += "&nbsp;&nbsp;<button style='color: #000000;height: 21px' onclick=toShowVideo(\'"+data[j].p_link+"\')>查看</button></td></tr>";
                        }else{
                            str += "</td></tr>";
                        }
                    }
                }else{
                    str = "<tr><td colspan='9'>暂无数据</td></tr>";
                }
                $("#datatable tbody").html("");
                $("#datatable tbody").append(str);
                var judge_result = $('input[name="judge_video"]:checked').val();
                if(judge_result==1){autoShowVideo(log_id);}
                return false;
            },
            complete: function (XHR, TS) { XHR = null }
        });
    }

    function init_env(num){
        // 初始化插件参数及插入插件
        WebVideoCtrl.I_InitPlugin('100%','410', {
            iWndowType: num,
            cbSelWnd: function (xmlDoc) {
                g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
            }
        });
        WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

        // 窗口事件绑定
        $(window).bind({
            resize: function () {
                var $Restart = $("#restartDiv");
                if ($Restart.length > 0) {
                    var oSize = getWindowSize();
                    $Restart.css({
                        width: oSize.width + "px",
                        height: oSize.height + "px"
                    });
                }
            }
        });

        //初始化日期时间
        var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
        $("#starttime").val(szCurTime + " 00:00:00");
        $("#endtime").val(szCurTime + " 23:59:59");
    }

    function camera_patrol(data){
        $.each(data, function (k, v) {
            clickLogin2(v.ip,v.channel_id);
            sleep(100);
        })
    }

    function sleep(n){
        var  start=new Date().getTime();
        while(true) if(new Date().getTime()-start>n)  break;
    }

    function clickLogin2(szIP,ID) {
        if(szIP == '11.8.173.51'|| szIP == '11.8.173.52'|| szIP == '11.8.173.53'|| szIP == '11.8.173.54'){
            szPassword = "7ca29e68";
        }else{
            szPassword = "12345";
        }
        var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
            success: function (xmlDoc) {
                console.log(szIP + " 登录成功！");
                setTimeout(function(){
                    var oWndInfo = WebVideoCtrl.I_GetWindowStatus(indexNum);
                    if (oWndInfo != null) {// 已经在播放了，先停止
                        WebVideoCtrl.I_Stop(indexNum);
                    }
                    var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
                        iWndIndex:indexNum,
                        iStreamType: 1,
                        iChannelID: ID,
                        bZeroChannel: false
                    });
                    indexNum++;
                    if (0 == iRet) {
                        szInfo = "开始预览成功！";
                        var iRet = WebVideoCtrl.I_Logout(szIP);
                    } else {
                        szInfo = "开始预览失败！";
                        var iRet = WebVideoCtrl.I_Logout(szIP);
                    }
                    console.log(szIP + " " + szInfo);
                },100)
            },
            error: function () {
                console.log(szIP + " 登录失败！");
            }
        });
        if (-1 == iRet) {
            console.log(szIP + " 已登录过！");
        }
    }
    </script>

    <div class="modal"  style="padding-top: 100px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;height: 500px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h6 class="modal-title" id="myModalLabel">视频监控 </h6>
                </div>
                <div class="modal-body" id="divPlugin">
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?php
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
?>