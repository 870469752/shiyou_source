<?php
use yii\helpers\Html;
use backend\assets\TableAsset;
use backend\models\SubSystem;
use common\library\MyFunc;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */
$PointSubsystem=msgpack_unpack(Yii::$app->redis->get("PointSubsystem"));
if(empty($PointSubsystem)) {
    SubSystem::PointSubsystemRel();
    $PointSubsystem=msgpack_unpack(Yii::$app->redis->get("PointSubsystem"));
}
TableAsset::register ( $this );
$this->title = '报警查看';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/ScrollingTable.js" );
$this->registerJsFile("js/gojs/node_template_edit_view.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/socket/socket.io.js", ['backend\assets\AppAsset']);

$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highcharts.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);

$this->registerCssFile("css/skin-bootstrap/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/skin-bootstrap/skin-win7/ui.fancytree.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.edit.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.glyph.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/fancytree/jquery.fancytree.wide.js", ['backend\assets\AppAsset']);
?>

<style>

    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }

    #infoBoxHolder {
        z-index: 300;
        position: absolute;
        left: 5px;
    }

    #infoBox {
        border: 1px solid #999;
        padding: 8px;
        background-color: whitesmoke;
        opacity:0.9;
        position: relative;
        width: 250px;
    //height: 60px;
        font-family: arial, helvetica, sans-serif;
        font-weight: bold;
        font-size: 11px;
    }

    /* this is known as the "clearfix" hack to allow
       floated objects to add to the height of a div */
    #infoBox:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }

    div.infoTitle {
        width: 50px;
        font-weight: normal;
        color:  #787878;
        float: left;
        margin-left: 4px;
    }

    div.infoValues {
        width: 150px;
        text-align: left;
        float: right;
    }


    a{text-decoration: none;color:inherit;}
    table tr td{text-align: center}
    table tr th{text-align: center}
</style>
<!--右键菜单-->
<div id="contextMenu">
    <ul>
        <li><a href="#" id="layer_set" onclick="">层次</a></li>
        <li><a href="#" id="menu5" onclick="">属性</a></li>
        <li><a href="#" id="console" onclick=" ">发送控制命令</a></li>
        <li><a href="#" id="font-style" onclick=" ">更新字体</a></li>
        <li><a href="#" id="font-color" onclick=" ">更新颜色</a></li>
        <li><a href="#" id="event_defend" onclick=" ">布防撤防</a></li>
        <li><a href="#" id="report_table" onclick=" ">图表查看</a></li>
    </ul>
</div>

<a  id="show_vedio_div"  data-toggle="modal" data-target="#myModal" ></a>
<section id="widget-grid" class="">
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-sortable="false">
                <header><span class="widget-icon"><i class="fa fa-table"></i></span>
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div style="float: right;padding-right: 40px;padding-bottom: 10px">
                        弹出视频：<input type="radio" name="judge_video" value="1">是&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="judge_video" value="0" checked>否
                    </div>
                </header>
                <div class="well well-sm well-light">
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th data-class="expand" >日期</th>
                            <th data-class="expand" >报警点</th>
                            <th data-class="expand" >描述</th>
                            <th data-class="expand">等级</th>
                            <th data-class="expand">恢复时间</th>
                            <th data-class="expand">状态</th>
                            <th data-class="expand">操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
        </article>
    </div>

</section>
<!-- END MAIN CONTENT -->
<div class="modal"  style="padding-top: 100px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 650px;height: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h6 class="modal-title" id="myModalLabel">视频监控 </h6>
            </div>
            <div class="modal-body" id="divPlugin">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div id="dialoge_test" style="width: 600px;display: none">
    <div id="myDiagramX" style="border: solid 1px black; width:100%; height:1000px;background:rgb(29,27,29)"></div>
    <div id="infoBoxHolder">
        <!-- Initially Empty, it is populated when updateInfoBox is called -->
    </div>
</div>
<!--多点位展示-->
<div id="many_points" style="display: none">
    <!--         Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-4" data-widget-editbutton="false" data-widget-deletebutton="false">

        <header>
            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
            <h2><?=Yii::t('point', '点位')?></h2>

        </header>

        <div>
            <div class="widget-body no-padding">
                <table id="selected_datatable_tabletools3" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>点位名称</th>
                        <th>值</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyData_many_points">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- font  color-->
<div id="console_board" title="命令控制台" style="width: 400px;">
    <?=Html::hiddenInput('font_css', null,['id'=>'font_css'])?>
    <div style="margin: 5px;width: 300px;">
        <?=Html::label('类型',null,  ['class' => 'control-label'])?>
        <div id="control_drop" style="width: 300px;height: 120px;">

        </div>
    </div>
</div>

<!--属性面板-->
<div id="AttributeMenu" style="width: auto;position:relative;z-index:10;display:none">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                <?=Html::hiddenInput('image_group_id',$model->id,['id'=>'image_group_id'])?>
                                <div class="row" style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('sub_system_id',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('sub_system_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'sub_system_id'])?>
                                    </div>
                                </div>

                                <div class="row"  style="display:none">
                                    <div class="col-md-9" style="margin: 1px;">
                                        <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                        <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                </div>

                                <div id='infoBox'>
                                    <div>Info</div>
                                    <div class='infoTitle'>属性名称</div>
                                    <div class='infoValues'>值</div>
                                    <div  class='infoTitle'>楼层id</div>
                                    <div id='location' class='infoValues'>值</div>
                                    <div  class='infoTitle'>key</div>
                                    <div id='node_key' class='infoValues'>值</div>
                                    <div  class='infoTitle'>分类</div>
                                    <div id='node_category' class='infoValues'>值</div>
                                    <div  class='infoTitle'>点位id</div>
                                    <div id='point_id' class='infoValues'>值</div>
                                    <div class='infoTitle'>点位值</div>
                                    <div  id='point_value'class='infoValues'>值</div>
                                    <div class='infoTitle'>更新时间</div>
                                    <div id='update_time' class='infoValues'>值</div>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>
<!--图标查看模块-->
<div id="report_table_view" style="display:none;width: auto">
    <div style="float:right; ">
        <div style="float:left; ">
            <label >开始时间</label>
            <input type="text" id="datetimepicker1" class="datetimepicker">
        </div>
        <div style="float:left; ">
            <label >结束时间</label>
            <input type="text" id="datetimepicker2" class="datetimepicker">
        </div>
    </div>
    <div id="container1" style="height:300px;float:left;">

    </div>
</div>
<p id="sub_system_id" style="display: none;"></p>
<p id="diagramEventsMsg" style="display: none;">Msg</p>
<script>

var user_id=<?=$user_id?>;
var is_control=<?=$control?>;
var sub_system_data=new Array();
var sub_system_info=<?=$sub_system_data?>;
var point_data=new Array();
var event_data=new Array();
var error_data=new Array();
var responsiveHelper_datatable_tabletools = undefined;
var sub_system_msg=null;
var diagram_now=null;
var node_now=null;
var all_image=<?=json_encode($all_image)?>;
var location_id=<?=$model->location_id?>;
var  PointSubsystem=<?=json_encode($PointSubsystem)?>;
window.onload = function() {
    document.getElementById("sub_system_id").value= <?=$id?>;
    var data=<?=$model->data?>;
    var image_url='<?=$base_map?>';
    var socket = io('http://11.29.1.17:9090');
    socket.on('connection', function () {
        console.log('connection setup for socket.io')
    });
    var socket_name='Point:'+'<?=$id?>';
    var points=sub_system_info['points'];
    var events=sub_system_info['events'];
    var send_message={
        socket_name:socket_name,
        Points:points,
        Events:events
    };

    socket.emit('socket_connect',send_message);
    socket.on(socket_name, function (msg) {
        var channel=msg['channel'];
        if(msg['value']=="False")msg['value']=0;
        if(msg['value']=="True")msg['value']=1;
        var channel_id=channel.substr(6, channel.length);
        if(channel.indexOf('Point')!=-1) {
            if (in_array(sub_system_info.points,channel_id)==1,'element') {
                point_data[channel_id]=toDecimal(Number(msg['value']));
            }
        }
        if(channel.indexOf('Event')!=-1) {
            if (in_array(sub_system_info.events,channel_id)==1,'element') {
                event_data[channel_id]=toDecimal(Number(msg['value']));
            }
        }
    })
    $('.ui-fancytree').css({'height': '400px'});
    //属性窗口可拖动
    $('#AttributeMenu').draggable();
    $("#check_menu").click(function () {
        var display=document.getElementById("AttributeMenu").style.display
        if(display=='none') document.getElementById("AttributeMenu").style.display='';
        else document.getElementById("AttributeMenu").style.display='none';
    })
    //初始化下拉框
    $("#select_point option[value='"+'en'+"']").attr("selected","selected");
    $("#select_point").chosen();

    //提供 图id与 设施key  ajax得到设施信息
    function ajaxGetAttribute(sub_system_id,s){
        var id=$("#image_group_id").val();
        //ajax获取信息
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/ajax-get-element",
            data: {sub_system_id:sub_system_id, element_id: s.key},
            success: function (msg) {
                var data = eval('[' + msg + ']')[0];
                //只有一个点位  同时更新到原来的属性框中
                if (data.length == 1)
                    update(data[0]['id']);
                if(s.img=="/uploads/pic/摄像头1.jpg" || s.img=="/uploads/pic/摄像头2.jpg" || s.img=="/uploads/pic/摄像头3.jpg" ||
                    s.img=="/uploads/pic/摄像头4.jpg"||s.img=="/uploads/pic/摄像头5.jpg"||s.img=="/uploads/pic/摄像头6.jpg"||
                    s.img=="/uploads/pic/摄像头7.jpg"||s.img=="/uploads/pic/摄像头8.jpg"||
                    s.img=="/uploads/20160217176ca4b6420dc19eec0f487f04090bf5.jpg"
                    ) {
                    show_video(data[0]['id']);
                }
            }
        });
    }

    $("#subsystem_config").click(function(){
        //所属的子系统id
        var sub_system_id =<?=$id?>;
        if(sub_system_id=='值')
            alert("请选中子系统");
        else {
            //跳转至子系统编辑界面
            var url="/control_info/crud/control?related_id="+sub_system_id+"&type=subsystem";
            window.location=url;
        }

    });



    //撤防布防  set撤防布防的值
    $("#event_defend").click(function(){
        var sub_system_id = document.getElementById("sub_system_id").value;
        var key=document.getElementById("element_id").value;
        var defend=1;
        var node_type=node_now.category;
        //撤防布防 为第三张图时为撤防
        if(node_now.img==node_now.text[3])
            defend=0;
        $.ajax({
            type: "POST",
            url: "/subsystem_manage/crud/ajax-set-defend",
            data: {
                sub_system_id: sub_system_id,
                element_id: key,
                node_type:node_type,
                defend: defend
            },
            success: function (msg) {
                console.log('msg');
            }
        });
        diagram_now.currentTool.stopTool();
    })

    //点击图表查看
    $("#report_table").click(function(){

        report_table_view.dialog("open");
    })
    setInterval(gif,200);

    //动态图测试
    function gif() {
        for (var key in sub_system_data) {
            //tabs_id 对应div的value值为
            var tabs_id = "tabs-myDiagram-" + key;
            var sub_system_id = $("#" + tabs_id).attr("value");
            //加入指定的node
            var diagram = sub_system_data[key];
            var model = diagram.model;
            var arr = model.nodeDataArray;
            model.startTransaction("flash");
            for (var i = 0; i < arr.length; i++) {
                var data = arr[i];
                //gif类型动态图
                if (data.category == 'gif'  ) {
                    var img = data.img;
                    var text = data.text;
                    var active = data.text.active;
                    if(data.text.gif==1) {

                        active++;
                        if (text.pic[active] != undefined) {
                            img = text.pic[active];
                            text.active = active;
                        }
                        else {
                            img = text.pic[0];
                            text.active = 0;
                        }
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                    else {
                        img = text.pic[0];
                        text.active = 0;
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                }
                //gif_value类型动态图
                if (data.category == 'gif_value' ) {
                    var img = data.img;
                    var text = data.text;
                    var active = data.text.active;
                    if(data.text.gif==data.text.active_value) {
                        active++;
                        if (text.pic[active] != undefined) {
                            img = text.pic[active];
                            text.active = active;
                        }
                        else {
                            img = text.pic[0];
                            text.active = 0;
                        }
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                    else {
                        img = text.pic[0];
                        text.active = 0;
                        model.setDataProperty(data, "img", '/' + img);
                        model.setDataProperty(data, "text", text);
                    }
                }

                diagram.model.commitTransaction("flash");
            }
        }
    }
    //控制按钮值的更新
    function controlupdate(console_type){
        var diagram=diagram_now;
        //得到 字体字段
        var font_css=$("#font_css").val();
        var font_color=$("#fontcolor").val();
        var key=$("#element_id").val();
        var model = diagram.model;
        var arr = model.nodeDataArray;
        model.startTransaction("flash");
        for (var i = 0; i < arr.length; i++) {
            var data = arr[i];
            if (data.key==key) {
                model.setDataProperty(data, "color", '#333');
                if(data.value[0]==1){
                    switch (console_type) {
                        case undefined:
                            model.setDataProperty(data, "text", '开');
                            break;
                        case null:
                            model.setDataProperty(data, "text", '开');
                            break;
                        default :
                            model.setDataProperty(data, "text", console_type);
                            break;
                    }
                }
                //其他显示config值
                else
                {
                    switch (console_type) {
                        case undefined:
                            model.setDataProperty(data, "text", console_type);
                            break;
                        case null:
                            model.setDataProperty(data, "text", "????");
                            break;
                        case '0':case 0:
                        model.setDataProperty(data, "text", data.value[0]);
                        break;
                        case '-1':case -1:
                        model.setDataProperty(data, "text", data.value[-1]);
                        break;
                        case '1':case 1:
                        model.setDataProperty(data, "text", data.value[1]);
                        break;
                        case '255':case 255:
                        model.setDataProperty(data, "text", data.value[255]);
                        break;
                        default :
                            model.setDataProperty(data, "text", "????");
                            break;
                    }
                }
            }
            diagram.model.commitTransaction("flash");
        }
    }
    init_updatevalue();
    setInterval(socket_updatevalue,500);
    //初始化更新节点值  读取 control值一次
    function init_updatevalue(){
        if(sub_system_info.points.length==0 && sub_system_info.events.length==0)
            console.log('无点');
        else {
            var ajax_data=JSON.stringify(sub_system_info);
            //ajax 查询点位信息后更新
            $.ajax({
                type: "POST",
                url: "/subsystem_manage/crud/ajax-update-value-new",
                data: {data: ajax_data},
                success: function (msg) {
                    //返回value绑定点位的值
                    msg = eval('(' + msg + ')');
                    sub_system_msg = msg;
                    for (var key in sub_system_data) {
                        var tabs_id = "tabs-myDiagram-" + key;
                        var sub_system_id =document.getElementById("sub_system_id").value;
                        //加入指定的node
                        var diagram = sub_system_data[key];
                        var temp = diagram.model.toJson();
                        var data = eval('(' + temp + ')');
                        //得到节点对象
                        var nodeDataArray = data['nodeDataArray'];
                        var key_value = msg[sub_system_id];

                        var model = diagram.model;
                        var arr = model.nodeDataArray;
                        model.startTransaction("flash");

                        if (key_value != undefined) {
                            //标记此图的value是否改变
                            var flag = false;
                            for (var i = 0; i < arr.length; i++) {
                                var data = arr[i];
                                var key = data.key;
                                if (key_value[key] != undefined) {
                                    switch (data.category) {
                                        case 'value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'onoffvalue':
                                            switch (key_value[key]['value']) {

                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '关');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '开');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'alarmvalue':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case '0':
                                                case 0:
                                                    model.setDataProperty(data, "text", '正常');
                                                    break;
                                                case '1':
                                                case 1:
                                                    model.setDataProperty(data, "text", '报警');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'state':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                                case '0':
                                                case 0:
                                                case '2':
                                                case 2:
                                                case '1':
                                                case 1:
                                                case 255:
                                                case '255':
                                                    model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "img", data.text[3]);
                                                    break;
                                            }
                                            break;
                                        case 'state_event':
                                            var fire_state=-1;
                                            var equip_state=-1;
                                            //节点值为 key_value[key] 数组 前两个数据为 报警 火灾的值
                                            for(var f_key in key_value[key]){
                                                //循环数组数据 根据名字来判断点位属于[报警 火灾]
                                                if(key_value[key][f_key]['name'].indexOf('火警报警')>0) {
                                                    fire_state = key_value[key][f_key]['value'];
                                                }
                                                if(key_value[key][f_key]['name'].indexOf('设备故障')>0) {
                                                    equip_state = key_value[key][f_key]['value'];
                                                }
                                            }
                                            if(fire_state || equip_state)
                                                model.setDataProperty(data, "img", data.text[1]);//red
                                            else model.setDataProperty(data, "img", data.text[2]);//green
                                            break;
                                        case 'state_value':
                                        case 'control':
                                            model.setDataProperty(data, "color", '#908988');
                                            //手动设置 显示点位值
                                            if (data.value[0] == 1) {
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", '开');
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", '开');
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text",key_value[key]['value']);
                                                        break;
                                                }
                                            }
                                            //其他显示config值
                                            else {
                                                switch (key_value[key]['value']) {
                                                    case undefined:
                                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                                        break;
                                                    case null:
                                                        model.setDataProperty(data, "text", "开");
                                                        break;
                                                    case '0':
                                                    case 0:
                                                        model.setDataProperty(data, "text", data.value[0]);
                                                        break;
                                                    case '1':
                                                    case 1:
                                                        model.setDataProperty(data, "text", data.value[1]);
                                                        break;
                                                    case '255':
                                                    case 255:
                                                        model.setDataProperty(data, "text", data.value[255]);
                                                        break;
                                                    default :
                                                        model.setDataProperty(data, "text", "开");
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'state_defend':
                                        case 'state_defend_1':
                                        case 'state_defend_ctrl':
                                            var a = -1;
                                            var b = -1;
                                            var img = data.text[3];
                                            //遍历 绑定的事件
                                            if(key_value[key].length!=0)
                                                for (var abc in key_value[key]) {
                                                    var name = key_value[key][abc]['name'];
                                                    if (
                                                        (data.category=='state_defend' && name.indexOf('布防操作成功') != -1)||
                                                            (data.category=='state_defend_1' && name.indexOf('布防') != -1)||
                                                            (data.category=='state_defend_ctrl' && name.indexOf('布防') != -1)
                                                        )
                                                        a = key_value[key][abc]['value'];
                                                    if (
                                                        (data.category=='state_defend' && name.indexOf('盗警防区报警') != -1)||
                                                            (data.category=='state_defend_1' && name.indexOf('劫盗/周界报警') != -1)||
                                                            (data.category=='state_defend_ctrl' && name.indexOf('报警') != -1)
                                                        )
                                                        b = key_value[key][abc]['value'];
                                                }
                                            if (a == 0){
                                                img = data.text[3];
                                            }
                                            else {
                                                if ( b== 0) img = data.text[1];
                                                if ( b== 1) img = data.text[2];
                                            }
                                            model.setDataProperty(data, "img", img);
                                            break;
                                        case 'only_value':
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    model.setDataProperty(data, "text", key_value[key]['value']);
                                                    break;
                                            }
                                            break;
                                        case 'value_1':

                                            if(data.config==undefined ) {
                                                data.config=new Array();
                                                data.config={'default':-1};
                                            }
                                            else if(data.config.length==0){
                                                data.config={'default':-1};
                                            }
                                            var value=key_value[key]['value'];
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                case null:
                                                    model.setDataProperty(data, "text", '?????');
                                                    break;
                                                default :
                                                    var defalut=Number(data.config.default);

                                                    for(var aaa in data.config){

                                                        if(aaa!='defalut' && value>aaa ){
                                                            defalut++;
                                                        }
                                                    }
                                                    var floor=value + defalut;
                                                    if(floor==0)floor='B1';
                                                    model.setDataProperty(data, "text", floor);
                                                    break;
                                            }
                                            break;
                                        case 'gif':

                                            var text = data.text;
                                            switch (key_value[key]['value']) {
                                                case undefined:
                                                    text.gif = 0;
                                                    break;
                                                case 1:
                                                    text.gif = 1;
                                                    break;
                                                default :
                                                    text.gif = 0;
                                                    break;
                                            }
                                            model.setDataProperty(data, "text", text);
                                            break;
                                        case 'main_edit':
                                            if (key_value != undefined) {
                                                if (
                                                    data['name'] == '0'
                                                    ) {
                                                    model.setDataProperty(data, "name", key_value[key]['name']);
                                                }
                                                else {
                                                }
                                                model.setDataProperty(data, "value", key_value[key]['value']);
                                            }
                                            break;
                                        case 'table':
                                            model.setDataProperty(data, "items", key_value[key]);
                                            break;
                                    }
                                }
                                diagram.model.commitTransaction("flash");
                            }

                        }
                    }
                }
            });
        }
    }

    //利用point_data   event_data更新点位信息后更新节点
    function update_subsystem_msg(){
        if(sub_system_msg!=null)
        //循环  节点对应的数据来更新节点值
            for(var sub_system_id in sub_system_msg){
                error_data[sub_system_id]=new Array();
                for(var key in sub_system_msg[sub_system_id]){
                    //绑定的是point点位 则从point_data内获取值 更新 数据数组
                    if(in_array(sub_system_info['points_key'],key,'element')){
                        //如果为 undefined 则不为数组
                        if(sub_system_msg[sub_system_id][key][0]==undefined) {
                            //绑定单个点位得到点位id
                            var value_id = sub_system_msg[sub_system_id][key]['id'];
                            //更新此节点的point值 或 记下无值key
                            if(point_data[value_id]==undefined){
                                error_data[sub_system_id][key]='无值';
                            }
                            else {
                                error_data[sub_system_id][key]= point_data[value_id];
                                sub_system_msg[sub_system_id][key]['value'] = point_data[value_id];
                            }
                        }
                        //否则绑定的点位为多个
                        else{
                            error_data[sub_system_id][key]=new Array();
                            for(var i in sub_system_msg[sub_system_id][key]){
                                var value_id = sub_system_msg[sub_system_id][key][i]['id'];
                                //更新此节点的point值 或 记下无值key
                                if(point_data[value_id]==undefined){
                                    error_data[sub_system_id][key][i]='无值';
                                }
                                else {
                                    error_data[sub_system_id][key][i]=point_data[value_id];
                                    sub_system_msg[sub_system_id][key][i]['value'] = point_data[value_id];
                                }
                            }
                        }
                    }
                    //绑定的是event点位 则从event_data内获取值 更新 数据数组
                    if(in_array(sub_system_info['events_key'],key,'element')){
                        //如果为 undefined 则不为数组
                        if(sub_system_msg[sub_system_id][key][0]==undefined) {
                            //绑定单个点位得到点位id
                            var value_id = sub_system_msg[sub_system_id][key]['id'];
                            //更新此节点的point值 或 记下无值key
                            if(point_data[value_id]==undefined){
                                error_data[sub_system_id][key]='无值';
                            }
                            else {
                                error_data[sub_system_id][key]=event_data[value_id];
                                sub_system_msg[sub_system_id][key]['value'] = event_data[value_id];
                            }
                        }
                        //否则绑定的点位为多个
                        else{
                            error_data[sub_system_id][key]=new Array();
                            for(var i in sub_system_msg[sub_system_id][key]){
                                var value_id = sub_system_msg[sub_system_id][key][i]['id'];
                                //更新此节点的point值 或 记下无值key
                                if(event_data[value_id]==undefined){
                                    error_data[sub_system_id][key][i]='无值';
                                }
                                else {
                                    error_data[sub_system_id][key][i]= point_data[value_id];
                                    sub_system_msg[sub_system_id][key][i]['value'] = event_data[value_id];
                                }
                            }

                        }
                    }
                }
            }
    }
    function socket_updatevalue(){
        console.log("updatevalue");
        if(sub_system_info.points.length==0 && sub_system_info.events.length==0)
            console.log('无点');
        else {
            //更新节点数据数组
            update_subsystem_msg();
            if(sub_system_msg!=null) {
                update_node();
            }
        }
    }

    function update_node(){
        var   msg=sub_system_msg;
        for (var key in sub_system_data) {
            var tabs_id = "tabs-myDiagram-" + key;
            var sub_system_id =document.getElementById("sub_system_id").value;
            //加入指定的node
            var diagram = sub_system_data[key];
            var temp = diagram.model.toJson();
            var data = eval('(' + temp + ')');
            //得到节点对象
            var nodeDataArray = data['nodeDataArray'];
            var key_value = msg[sub_system_id];
            var model = diagram.model;
            var arr = model.nodeDataArray;
            model.startTransaction("flash");

            if (key_value != undefined) {
                //标记此图的value是否改变
                var flag = false;

//                            var node_value=key_value[nodekey];
                for (var i = 0; i < arr.length; i++) {
                    var data = arr[i];
                    var key = data.key;
                    if (key_value[key] != undefined) {
                        switch (data.category) {
                            case 'value':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'onoffvalue':
                                switch (key_value[key]['value']) {

                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case '0':
                                    case 0:
                                        model.setDataProperty(data, "text", '关');
                                        break;
                                    case '1':
                                    case 1:
                                        model.setDataProperty(data, "text", '开');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'alarmvalue':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case '0':
                                    case 0:
                                        model.setDataProperty(data, "text", '正常');
                                        break;
                                    case '1':
                                    case 1:
                                        model.setDataProperty(data, "text", '报警');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'state':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "img", data.text[3]);
                                        break;
                                    case null:
                                        model.setDataProperty(data, "img", data.text[3]);
                                        break;
                                    case '0':
                                    case 0:
                                    case '2':
                                    case 2:
                                    case '1':
                                    case 1:
                                    case 255:
                                    case '255':
                                        model.setDataProperty(data, "img", data.text[key_value[key]['value']]);
                                        break;
                                    default :
                                        model.setDataProperty(data, "img", data.text[1]);
                                        break;
                                }
                                break;
                            case 'state_event':
                                var fire_state=-1;
                                var equip_state=-1;
                                //节点值为 key_value[key] 数组 前两个数据为 报警 火灾的值
                                for(var f_key in key_value[key]){
                                    //循环数组数据 根据名字来判断点位属于[报警 火灾]
                                    if(key_value[key][f_key]['name'].indexOf('火警报警')>0) {
                                        fire_state = key_value[key][f_key]['value'];
                                    }
                                    if(key_value[key][f_key]['name'].indexOf('设备故障')>0) {
                                        equip_state = key_value[key][f_key]['value'];
                                    }
                                }
                                if(fire_state || equip_state)
                                    model.setDataProperty(data, "img", data.text[1]);//red
                                else model.setDataProperty(data, "img", data.text[2]);//green
                                break;
                            case 'state_value':
                                //手动设置 显示点位值
                                if (data.value[0] == 1) {
                                    switch (key_value[key]['value']) {
                                        case undefined:
                                            model.setDataProperty(data, "text", '?????');
                                            break;
                                        case null:
                                            model.setDataProperty(data, "text", '?????');
                                            break;
                                        default :
                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                            break;
                                    }
                                }
                                //其他显示config值
                                else {
                                    switch (key_value[key]['value']) {
                                        case undefined:
                                            model.setDataProperty(data, "text", key_value[key]['value']);
                                            break;
                                        case null:
                                            model.setDataProperty(data, "text", "????");
                                            break;
                                        case '0':
                                        case 0:
                                            model.setDataProperty(data, "text", data.value[0]);
                                            break;
                                        case '1':
                                        case 1:
                                            model.setDataProperty(data, "text", data.value[1]);
                                            break;
                                        case '255':
                                        case 255:
                                            model.setDataProperty(data, "text", data.value[255]);
                                            break;
                                        default :
                                            model.setDataProperty(data, "text", "????");
                                            break;
                                    }
                                }
                                break;
                            case 'state_defend':
                            case 'state_defend_1':
                            case 'state_defend_ctrl':
                                var a = -1;
                                var b = -1;
                                var img = data.text[3];
                                //遍历 绑定的事件
                                if(key_value[key].length!=0)
                                    for (var abc in key_value[key]) {
                                        var name = key_value[key][abc]['name'];
                                        if (
                                            (data.category=='state_defend' && name.indexOf('布防操作成功') != -1)||
                                                (data.category=='state_defend_1' && name.indexOf('布防') != -1)||
                                                (data.category=='state_defend_ctrl' && name.indexOf('布防') != -1)
                                            )
                                            a = key_value[key][abc]['value'];
                                        if (
                                            (data.category=='state_defend' && name.indexOf('盗警防区报警') != -1)||
                                                (data.category=='state_defend_1' && name.indexOf('劫盗/周界报警') != -1)||
                                                (data.category=='state_defend_ctrl' && name.indexOf('报警') != -1)
                                            )
                                            b = key_value[key][abc]['value'];
                                    }
                                if (a == 0){
                                    img = data.text[3];
                                }
                                else {
                                    if ( b== 0) img = data.text[1];
                                    if ( b== 1) img = data.text[2];
                                }
                                model.setDataProperty(data, "img", img);
                                break;
                            case 'only_value':
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        model.setDataProperty(data, "text", key_value[key]['value']);
                                        break;
                                }
                                break;
                            case 'value_1':

                                if(data.config==undefined ) {
                                    data.config=new Array();
                                    data.config={'default':-1};
                                }
                                else if(data.config.length==0){
                                    data.config={'default':-1};
                                }
                                var value=key_value[key]['value'];
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    case null:
                                        model.setDataProperty(data, "text", '?????');
                                        break;
                                    default :
                                        var defalut=Number(data.config.default);

                                        for(var aaa in data.config){

                                            if(aaa!='defalut' && value>aaa ){
                                                defalut++;
                                            }
                                        }
                                        var floor=value + defalut;
                                        if(floor==0)floor='B1';
                                        model.setDataProperty(data, "text", floor);
                                        break;
                                }
                                break;
                            case 'gif':
                                var text = data.text;
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        text.gif = 0;
                                        break;
                                    case 1:
                                        text.gif = 1;
                                        break;
                                    default :
                                        text.gif = 0;
                                        break;
                                }
                                model.setDataProperty(data, "text", text);
                                break;
                            case 'gif_value':
                                var text = data.text;
                                switch (key_value[key]['value']) {
                                    case undefined:
                                        text.gif = 0;
                                        break;
                                    default :
                                        text.gif = key_value[key]['value'];
                                        break;
                                }
                                model.setDataProperty(data, "text", text);
                                break;
                            case 'main_edit':
                                if (key_value != undefined) {
                                    if (
                                        data['name'] == '0'
                                        ) {
                                        model.setDataProperty(data, "name", key_value[key]['name']);
                                    }
                                    model.setDataProperty(data, "value", key_value[key]['value']);
                                }
                                break;
                            case 'table':
                                model.setDataProperty(data, "items", key_value[key]);
                                break;
                        }
                    }
                    diagram.model.commitTransaction("flash");
                }

            }
        }
    }
    function update_console_board(type,html){
        $("#control_drop").empty();
        $("#control_drop").append(html);

        //当是下拉框时初始化下拉框并给宽度300px
        if(type==0) {
            $("#console_type").chosen();
            $("#console_type_chosen").width(300);
        }
    }

    //更新属性框内的点位值
    function update(value){
        //chosen 先设置值在执行更新函数
        $("#select_point").val(value);
        $("#select_point").trigger("chosen:updated");
    }

    function update_points(data){
        var html='';
        if(data.length!=0){
            $("#tbodyData_many_points").empty();
            for(var i in data){
                html += '<tr><td>'+data[i]['name']+'</td><td>'+data[i]['value']+'</td></tr>';
            }
            $("#tbodyData_many_points").append(html);
        }
        many_points.dialog("open");
    }

    //跟着窗口滚动
    function scroll(){
        $(window).scroll(function(){
            var oParent = document.getElementById('AttributeMenu');
            var x =oParent.offsetLeft;
            var y = oParent.offsetTop;
            var yy = $(this).scrollTop();//获得滚动条top值
            if ($(this).scrollTop() < 30) {
                $("#AttributeMenu").css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位
            }else{
                $("#AttributeMenu").css({"position":"absolute",top:yy+"px",left:x+"px"});
            }
        });
    }
    //s为所选节点 id为此节点所在的 myDiagram所在数组sub_system_data中的键值
    function showMessage(s,id) {
        var tabs_id = "sub_system_id";
        var sub_system_id =<?=$id?>;
        //节点信息
        document.getElementById("diagramEventsMsg").textContent = s;
        var sub_system_id=document.getElementById("sub_system_id").value;
        document.getElementById("element_id").value = s.key;
        document.getElementById("location").innerText = location_id;
        document.getElementById("node_key").innerText = s.key;
        document.getElementById("node_category").innerText = s.category;
        if(sub_system_msg[sub_system_id][s.key]!=undefined) {
            document.getElementById("point_id").innerText = sub_system_msg[sub_system_id][s.key]['id'];
            document.getElementById("point_value").innerText = sub_system_msg[sub_system_id][s.key]['value'];
            document.getElementById("update_time").innerText = sub_system_msg[sub_system_id][s.key]['time'];
        }
        else {
            document.getElementById("point_id").innerText = '无';
            document.getElementById("point_value").innerText = '无';
            document.getElementById("update_time").innerText = '无';
        }

        ajaxGetAttribute(sub_system_id,s);
        update('zh');
    }
    scroll();
    var tab_num=1;
    if(data['image_base']==0) {
        init2('myDiagramX', 1);
        sub_system_data[1].model = go.Model.fromJson(data['data']);
        sub_system_data[1].requestUpdate();
        num=1;
    }
    if(data['image_base']==1) {
        image_url=all_image[data['image_id']]['url'];
        init1('myDiagramX', 2,image_url);
        sub_system_data[2].model = go.Model.fromJson(data['data']);
        sub_system_data[2].requestUpdate();
        num=2;
    }

    var image_in=new Array();
    var data_name=new Array();

    //数组remove指定键值元素 数据
    function remove(a,id){
        var result=new Array();
        for(var key in a){
            if(key!=id) result[key]=a[key];
        }
        return result;
    }

    var id = "sub_system";

    //弹出框内三个值
    var sub_system_name=$("#sub_system_name");
    var image_base=$("#image_base");
    var sub_system_category=$("#sub_system_category");
    //控制台 操作页面

    $("many_points").css("display","block");
    $("#dialoge_test").css("display","block");
    var dialoge_test = $("#dialoge_test").dialog({
        width : 1000,
        autoOpen : false,
        resizable : true,
        modal : true
    });

    //控制台 操作页面
    var console_board = $("#console_board").dialog({
        autoOpen : false,
        width : 400,
        resizable : false,
        modal : true,
        buttons : [{
            html : "<i class='fa fa-times'></i>&nbsp; 取消",
            "class" : "btn btn-default",
            click : function() {
                $(this).dialog("close");

            }
        }, {
            html : "<i class='fa fa-plus'></i>&nbsp; 确定",
            "class" : "btn btn-danger",
            click : function() {
                //获取所操作子系统sub_system_id 以及节点的element_id
                //以及  命令值
                var sub_system_id=$("#sub_system_id").val();
                var element_id=$("#element_id").val();
                var console_type=$("#console_type").val();
                //先更新节点值
                controlupdate(console_type);
                //ajax 在数据表内写入命令
                $.ajax({
                    type: "POST",
                    url: "/subsystem_manage/crud/ajax-control",
                    data: {sub_system_id: sub_system_id,element_id:element_id,console_type:console_type},
                    success: function (msg) {
                        switch (msg){
                            case '-1':
                                alert('未绑定点位');break;
                            case '0':
                                alert('命令发送失败');break;
                        }
                    }
                });
                $(this).dialog("close");
            }
        }]
    });

    //控制台 操作页面
    var many_points = $("#many_points").dialog({
        autoOpen : false,
        width : 'auto',
        resizable : false,
        modal : true
    });

    //初始化 有底图的
    function init1(element,num,image_url) {
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        var cellSize = new go.Size(10, 10);
        myDiagram =
            $(go.Diagram, element,
                {
                    grid: $(go.Panel, "Grid",
                        {gridCellSize: cellSize}
                    )
                },
                {
                    allowVerticalScroll: false,
                    allowDrop: false,// must be true to accept drops from the Palette
                    "draggingTool.isGridSnapEnabled": true,
                    "draggingTool.gridSnapCellSpot": go.Spot.Center,
                    contentAlignment: go.Spot.Center,
                    isReadOnly: true,  // allow selection but not moving or copying or deleting
                    "toolManager.hoverDelay": 100  // how quickly tooltips are shown
                });
        sub_system_data[num] = myDiagram;
        //单击事件
        sub_system_data[num].addDiagramListener("ObjectSingleClicked",
            function (e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)) {
                    if (part.data.category == 'button') {
                        button = sub_system_data[num];
                        button_key = part.data.key;
                        linkdialog.dialog("open");
                    }
                    else {
                        showMessage(part.data, num);
                    }
                }
            });

        //双击事件
        sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
            function(e) {
                var part = e.subject.part;

                if (!(part instanceof go.Link)) {
                    //更新属性框信息
                    showMessage(part.data, num);
                    switch (part.data.category) {
                        //control类型 弹出控制台
                        case 'control':
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            var value = part.data.value;
                            console.log(value[0] != 1);
                            if (value[0] != 1) {
                                //根据此节点data.value更新控制台的下拉列表信息
                                var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                for (var key in value) {
                                    option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                }
                                option_html += '</select>';
                                var type = 0;
                            }
                            else {

                                var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                var type = 1;
                            }
                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;
                        case 'table':
                            console.log(part.data.value);
                            break;
                    }
                };
            });

        //右键事件
        sub_system_data[num].addDiagramListener("ObjectContextClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    if (
                        part.data.category == 'table'||
                            part.data.category == 'edit' ||
                            part.data.category == 'value'||
                            part.data.category == 'main' ||
                            part.data.category == 'only_value'
                        )
                    {
                        diagram_now=sub_system_data[num];
                        showMessage(part.data, num);
                    }
                }
            });

        sub_system_data[num].add(
            $(go.Part,  // this Part is not bound to any model data
                {
                    selectable: false, pickable: false
                },
                $(go.Picture, "/" + image_url)
            ));

        var node_template = init_node_template_edit();
        //���nodeģ��
        sub_system_data[num].nodeTemplateMap.add("main", node_template.main);
        sub_system_data[num].nodeTemplateMap.add("edit", node_template.edit);
        sub_system_data[num].nodeTemplateMap.add("value", node_template.value);
        sub_system_data[num].nodeTemplateMap.add("onoffvalue", node_template.onoff);
        sub_system_data[num].nodeTemplateMap.add("alarmvalue", node_template.alarm);
        sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
        sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
        sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
        sub_system_data[num].nodeTemplateMap.add("gif_value", node_template.gif_value);
        sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
        sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
        sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
        sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
        sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
        sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
        sub_system_data[num].nodeTemplateMap.add("state_event", node_template.state_event);
        sub_system_data[num].nodeTemplateMap.add("state_defend", node_template.state_defend);
        sub_system_data[num].nodeTemplateMap.add("state_defend_1", node_template.state_defend_1);
        sub_system_data[num].nodeTemplateMap.add("state_defend_ctrl", node_template.state_defend_ctrl);
        sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
        sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
        sub_system_data[num].groupTemplate=node_template.group;
    }
    //初始化 无底图的
    function init2(element,num) {
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        var cellSize = new go.Size(10, 10);
        myDiagram =
            $(go.Diagram, element,  // must name or refer to the DIV HTML element
                {
                    grid: $(go.Panel, "Grid",
                        {gridCellSize: cellSize}
                    )
                },
                {
                    allowDrop: false,// must be true to accept drops from the Palette
                    contentAlignment:go.Spot.Center,
                    isReadOnly: true  // allow selection but not moving or copying or deleting
                }
            );

        //把myDiagram 加入sub_system_data内
        sub_system_data[num]=myDiagram;

        //双击事件
        sub_system_data[num].addDiagramListener("ObjectDoubleClicked",
            function(e) {
                console.log('双击');

                var part = e.subject.part;
                if (!(part instanceof go.Link)) {
                    //更新属性框信息
                    diagram_now=sub_system_data[num];
                    showMessage(part.data, num);
                    is_control==1
                    switch (part.data.category) {
                        //control类型 弹出控制台
                        case 'control':
                            button = sub_system_data[num];
                            button_key = part.data.key;
                            var value = part.data.value;
                            console.log(value[0] != 1);
                            if (value[0] != 1) {
                                //根据此节点data.value更新控制台的下拉列表信息
                                var option_html = '<select id="console_type" class= "chosen_select"   style="width: 300px" name="point_id" placeholder="请选择相应点位">';
                                for (var key in value) {
                                    option_html += '<option value="' + key + '" >' + value[key] + '</option>';
                                }
                                option_html += '</select>';
                                var type = 0;
                            }
                            else {

                                var option_html = '<input id="console_type" class="form-control" name="value" value="">';
                                var type = 1;
                            }
                            update_console_board(type, option_html);
                            console_board.dialog("open");
                            break;

                        case 'table':
                            console.log(part.data);
                            break;
                        case 'many':
                            //得到点击的子系统id  key
                            var sub_system_id = document.getElementById("sub_system_id").value;
                            var element_id = document.getElementById("element_id").value;

                            var value = sub_system_msg[sub_system_id][element_id];
                            update_points(value);
                            break;
                    }};
            });

        //右键事件
        sub_system_data[num].addDiagramListener("ObjectContextClicked",
            function(e) {
//                    console.log('右键');
                var part = e.subject.part;
                if (!(part instanceof go.Link)){
                    if (part.data.category == 'table'||
                        part.data.category == 'edit' ||
                        part.data.category == 'value'||
                        part.data.category == 'main' ||
                        part.data.category == 'many' ||
                        part.data.category == 'only_value'
                        )
                    {
                        diagram_now=sub_system_data[num];
                        showMessage(part.data, num);
                        show_contextmenu();
                    }
                    if(
                        part.data.category == 'state_defend'||
                            part.data.category == 'state_defend_1'||
                            part.data.category == 'state_defend_ctrl'
                        ){
                        diagram_now=sub_system_data[num];
                        showMessage(part.data, num);
                        node_now=part.data;
                        //还原右键菜单
                        show_contextmenu();
                        //隐藏除了撤防布防的按钮
                        document.getElementById("layer_set").style.display='none';
                        document.getElementById("menu5").style.display='none';
                        document.getElementById("console").style.display='none';
                        document.getElementById("font-style").style.display='none';
                        document.getElementById("font-color").style.display='none';
                    }else
                    {
                        //还原右键菜单
                        show_contextmenu();
                        showMessage(part.data, num);
                        //隐藏除了撤防布防的按钮
                        document.getElementById("layer_set").style.display='none';
                        document.getElementById("menu5").style.display='none';
                        document.getElementById("console").style.display='none';
                        document.getElementById("font-style").style.display='none';
                        document.getElementById("font-color").style.display='none';
                        document.getElementById("event_defend").style.display='none';
                    }
                }

            });

        var node_template=init_node_template_edit();
        init_context_menu("contextMenu",sub_system_data[num]);
        sub_system_data[num].nodeTemplateMap.add("main",node_template.main);
        sub_system_data[num].nodeTemplateMap.add("edit",node_template.edit);
        sub_system_data[num].nodeTemplateMap.add("value",node_template.value);
        sub_system_data[num].nodeTemplateMap.add("onoffvalue",node_template.onoff);
        sub_system_data[num].nodeTemplateMap.add("alarmvalue",node_template.alarm);
        sub_system_data[num].nodeTemplateMap.add("button", node_template.button);
        sub_system_data[num].nodeTemplateMap.add("test", node_template.test);
        sub_system_data[num].nodeTemplateMap.add("gif", node_template.gif);
        sub_system_data[num].nodeTemplateMap.add("gif_value", node_template.gif_value);
        sub_system_data[num].nodeTemplateMap.add("table", node_template.table);
        sub_system_data[num].nodeTemplateMap.add("figure", node_template.figure);
        sub_system_data[num].nodeTemplateMap.add("main_edit", node_template.main_edit);
        sub_system_data[num].nodeTemplateMap.add("only_value", node_template.only_value);
        sub_system_data[num].nodeTemplateMap.add("state", node_template.state);
        sub_system_data[num].nodeTemplateMap.add("state_value", node_template.state_value);
        sub_system_data[num].nodeTemplateMap.add("state_event", node_template.state_event);
        sub_system_data[num].nodeTemplateMap.add("state_defend", node_template.state_defend);
        sub_system_data[num].nodeTemplateMap.add("state_defend_1", node_template.state_defend_1);
        sub_system_data[num].nodeTemplateMap.add("state_defend_ctrl", node_template.state_defend_ctrl);
        sub_system_data[num].nodeTemplateMap.add("control", node_template.control);
        sub_system_data[num].nodeTemplateMap.add("many", node_template.many);
        sub_system_data[num].nodeTemplateMap.add("value_1", node_template.value_1);
        sub_system_data[num].groupTemplate=node_template.group;
    }
    window.showSubsystem = function(subsystem_id){
        $.ajax({
            type: "POST",
            url:"/subsystem_manage/crud/alarm-subsystem?id="+subsystem_id,
            data: {
                sub_system_id: subsystem_id
            },
            success: function (msg) {
                document.getElementById("sub_system_id").value= subsystem_id;
                //得到新图数据
                msg=eval('('+msg+')');
                //更新各个全局变量的值
                user_id=msg['user_id'];
                is_control=msg['control'];
                document.getElementById("sub_system_id").value=msg['id'];
                sub_system_info=eval('('+msg['sub_system_data']+')');
                var data=eval('('+msg['data']+')');
                location_id=msg['location_id'];

                /*socket监听端口更新*/
                var socket_name='Point:'+subsystem_id;
                var points=sub_system_info['points'];
                var events=sub_system_info['events'];
                var send_message={
                    socket_name:socket_name,
                    Points:points,
                    Events:events
                };
                console.log(send_message);

                socket.emit('socket_connect',send_message);
                socket.on(socket_name, function (msg) {
                    var channel=msg['channel'];
                    if(msg['value']=="False")msg['value']=0;
                    if(msg['value']=="True")msg['value']=1;
                    var channel_id=channel.substr(6, channel.length);
                    if(channel.indexOf('Point')!=-1) {
                        if (in_array(sub_system_info.points,channel_id)==1,'element') {
                            point_data[channel_id]=toDecimal(Number(msg['value']));
                        }
                    }
                    if(channel.indexOf('Event')!=-1) {
                        if (in_array(sub_system_info.events,channel_id)==1,'element') {
                            event_data[channel_id]=toDecimal(Number(msg['value']));
                        }
                    }
                })

                //重画子系统图
                if(data['image_base']==0) {
                    $("#myDiagramX").replaceWith('<div id="myDiagramX" style="border: solid 1px black; width:100%; height:600px;background:rgb(29,27,29)"></div>');
                    init2('myDiagramX', 1);
                    dialoge_test.dialog("open");
                    sub_system_data[1].model = go.Model.fromJson(data['data']);
                    sub_system_data[1].requestUpdate();
                    num=1;
                }
                if(msg['image_base']==1) {
                    image_url=all_image[data['image_id']]['url'];
                    $("#myDiagramX").replaceWith('<div id="myDiagramX" style="border: solid 1px black; width:100%; height:600px;background:rgb(29,27,29)"></div>');
                    init1('myDiagramX', 2,image_url);
                    dialoge_test.dialog("open");
                    sub_system_data[2].model = go.Model.fromJson(data['data']);
                    sub_system_data[2].requestUpdate();
                    num=2;
                }
                init_updatevalue();//初始化本图数据
            }
        });
    }

    $('#selected_datatable_tabletools3').dataTable({
        "sDom": "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "bSort" : false,
        "paging":   false
    } );
    var oTable;
    var g_iWndIndex = 0; //可以不用设置这个变量，有窗口参数的接口中，不用传值，开发包会默认使用当前选择窗口
    szPort = 80;
    szUsername = "admin";
    szPassword = "12345";
    indexNum = 0;
    $(document).ready(function () {
        $('#tabs').tabs();
        getRealTimeAlarmInfo(0);
    });
};
//第三位参数为类型  判断是 key还是value
function in_array(array,element,type){
    for(var i in array){
        if(type=='element') {
            if (array[i] == element)
                return 1;
        }
        if(type=='key') {
            if (i == element)
                return 1;
        }
    }
    return 0;
}

function toDecimal(x){
    var f=parseFloat(x);
    if(isNaN(f)){
        return;
    }
    f=Math.round(x*100)/100;
    return f;
}

function show_contextmenu(){
    //展示右键菜单
    document.getElementById("layer_set").style.display='';
    document.getElementById("menu5").style.display='';
    document.getElementById("console").style.display='';
    document.getElementById("font-style").style.display='';
    document.getElementById("font-color").style.display='';
    document.getElementById("event_defend").style.display='';
    document.getElementById("report_table").style.display='';
}

/*
 * 弹出子系统页面
 * */
function choseSubsystem(id){
    $.ajax({
        type: "POST",
        url: "/subsystem_manage/crud/test?id="+id,
        data: {
        },
        success: function (msg) {
            console.log('子系统id'+msg);
            var subsystem_ids=PointSubsystem[msg];
            if(subsystem_ids==undefined)alert("系统图不存在");
            if(subsystem_ids.length==1)
                var subsystem_id=subsystem_ids[0]['id'];
            console.log("系统图id:"+subsystem_id);
            //生成下拉框选择
            showSubsystem(subsystem_id);
        }
    });
}

/**
 * 确认报警信息
 * @private
 */
function toConfirm(id) {
    $.ajax({
        url: '/alarm-log/default/confirm?id='+id,
        async: true,
        success: function (data) {
            if( $("#alarm_log_"+id+" td").eq(5).html()!="已恢复"){
                $("#alarm_log_"+id).attr("style","color:green;algin:center");
            }
        }
    });
}

/**
 * 查看报警点位关联视频
 * @private
 */
function toShowVideo(id) {
    indexNum = 0;
    $.ajax({
        url: '/alarm-log/default/show-video?id='+id,
        async: true,
        success: function (data) {
            dataChange(data);
        }
    });
}

/**
 * 查看报警点位关联视频
 * @private
 */
function autoShowVideo(log_id) {
    indexNum = 0;
    $.ajax({
        url: '/alarm-log/default/auto-show-video?log_id='+log_id,
        async: true,
        success: function (data) {
            if(data){
                dataChange(data);
            }
            return false;
        },
        complete: function (XHR, TS) { XHR = null }
    });
}

function dataChange(data){
    var temp_data = JSON.parse(data);
    var temp_len = temp_data.length;
    var num = 1;
    if(temp_len>1 && temp_len<=4){
        num = 2;
    }else if(temp_len>4 && temp_len<=9){
        num = 3;
    }else if(temp_len>9 && temp_len<=16){
        num = 4;
    }else if(temp_len>16){
        num = 4;
        temp_data = temp_data.slice(0,16);
    }
    var tem_class = $("#myModal").attr("class");
    if(tem_class == "modal"){
        $("#show_vedio_div").click();
    }
    $("#divPlugin").text("");
    init_env(num);
    camera_patrol(temp_data);
}

/**
 * 获取实时报警信息
 * @private
 */
function getRealTimeAlarmInfo(log_id){
    $.ajax({
        type: 'POST',
        dataType: "json",
        async: true,
        url: '/alarm-log/default/get-day-alarm-log',
        success: function (data) {
            var str = '';
            if(data.length>0){
                for(var j in data){
                    if(data[j].is_confirmed){
                        data[j].is_confirmed='已确认';
                        tem_style ='color:green;algin:center';
                    }else{
                        data[j].is_confirmed='待确认';
                        tem_style ='color:#ff0000;algin:center';
                    }
                    if(data[j].end_time){
                        data[j].is_confirmed='已恢复';
                        tem_style ='color:black;algin:center';
                    }else{
                        data[j].end_time='';
                    }
                    str += '<tr id="alarm_log_'+data[j].id+'" style="'+tem_style+'">' +
                        '<td>' + data[j].start_time.substr(0,19)+"</td>" +
                        "<td>" + data[j].p_name+"</td>" +
                        "<td>" + data[j].description+"</td>" +
                        "<td>" + data[j].alarm_level+"</td>" +
                        "<td>" + data[j].end_time+"</td>" +
                        "<td>" + data[j].is_confirmed+"</td>" +
                        "<td><a title='确定'  class='glyphicon glyphicon-ok' data-value="+data[j].point_id+" onclick=toConfirm("+data[j].id+")></a>";
                    if(data[j].p_link){
                        str += "&nbsp;&nbsp;<a title='视频'  class='glyphicon glyphicon-facetime-video' onclick=toShowVideo(\'"+data[j].p_link+"\')></a>";
                    }
                    if(PointSubsystem[data[j].point_id]!=undefined){
                        str += "&nbsp;&nbsp;<a title='地图'  class='glyphicon glyphicon-globe' onclick=choseSubsystem("+data[j].id+")></a>";
                    }
                    str +="</td></tr>";
                }
            }else{
                str = "<tr><td colspan='7'>暂无数据</td></tr>";
            }
            $("#datatable tbody").html("");
            $("#datatable tbody").append(str);
            return false;
        },
        complete: function (XHR, TS) { XHR = null }
    });
}

function getOneAlarmInfo(log_info){
    var str = '';
    if(log_info.is_confirmed){
        log_info.is_confirmed='已确认';
        tem_style ='color:green;algin:center';
    }else{
        log_info.is_confirmed='待确认';
        tem_style ='color:#ff0000;algin:center';
    }
    if(log_info.end_time){
        log_info.is_confirmed='已恢复';
        tem_style ='color:black;algin:center';
    }else{
        log_info.end_time='';
    }
    str += '<tr id="alarm_log_'+log_info.id+'" style="'+tem_style+'">' +
        '<td>' + log_info.start_time.substr(0,19)+"</td>" +
        "<td>" + log_info.point_name+"</td>" +
        "<td>" + log_info.description+"</td>" +
        "<td>" + log_info.alarm_level+"</td>" +
        "<td>" + log_info.end_time+"</td>" +
        "<td>" + log_info.is_confirmed+"</td>" +
        "<td><a title='确定' class='glyphicon glyphicon-ok' onclick=toConfirm("+log_info.id+")></a>";
    if(log_info.link_point){
        str += "&nbsp;&nbsp;<a title='视频' class='glyphicon glyphicon-facetime-video' onclick=toShowVideo(\'"+log_info.link_point+"\')></a>";
    }

    if(PointSubsystem[log_info.point_id]!=undefined){
        str += "&nbsp;&nbsp;<a title='地图' class='glyphicon glyphicon-globe' data-value="+log_info.point_id+" onclick=choseSubsystem("+log_info.id+")></a>";
    }
    str +="</td></tr>"
    $("#datatable tbody tr:first").before(str);
    $("#datatable tbody tr:last").remove();
    if(log_info.point_link != ""){
        var judge_result = $('input[name="judge_video"]:checked').val();
        if(judge_result==1){autoShowVideo(log_info.id);}
    }
    return false;
}

//报警恢复
function resetAlarm(log_info){
    $("#alarm_log_"+log_info.id).attr("style","color:black;algin:center");
    $("#alarm_log_"+log_info.id+" td").eq(5).html("已恢复");
    $("#alarm_log_"+log_info.id+" td").eq(4).html(log_info.end_time);
    return false;
}

function init_env(num){
    // 初始化插件参数及插入插件
    WebVideoCtrl.I_InitPlugin('100%','410', {
        iWndowType: num,
        cbSelWnd: function (xmlDoc) {
            g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
        }
    });
    WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

    // 窗口事件绑定
    $(window).bind({
        resize: function () {
            var $Restart = $("#restartDiv");
            if ($Restart.length > 0) {
                var oSize = getWindowSize();
                $Restart.css({
                    width: oSize.width + "px",
                    height: oSize.height + "px"
                });
            }
        }
    });

    //初始化日期时间
    var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
    $("#starttime").val(szCurTime + " 00:00:00");
    $("#endtime").val(szCurTime + " 23:59:59");
}

function camera_patrol(data){
    $.each(data, function (k, v) {
        clickLogin2(v.ip,v.channel_id);
        sleep(100);
    })
}

function sleep(n){
    var  start=new Date().getTime();
    while(true) if(new Date().getTime()-start>n)  break;
}

function clickLogin2(szIP,ID) {
    if(szIP == '11.8.173.51'|| szIP == '11.8.173.52'|| szIP == '11.8.173.53'|| szIP == '11.8.173.54'){
        szPassword = "7ca29e68";
    }else{
        szPassword = "12345";
    }
    var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
        success: function (xmlDoc) {
            console.log(szIP + " 登录成功！");
            setTimeout(function(){
                var oWndInfo = WebVideoCtrl.I_GetWindowStatus(indexNum);
                if (oWndInfo != null) {// 已经在播放了，先停止
                    WebVideoCtrl.I_Stop(indexNum);
                }
                var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
                    iWndIndex:indexNum,
                    iStreamType: 1,
                    iChannelID: ID,
                    bZeroChannel: false
                });
                indexNum++;
                if (0 == iRet) {
                    szInfo = "开始预览成功！";
                    var iRet = WebVideoCtrl.I_Logout(szIP);
                } else {
                    szInfo = "开始预览失败！";
                    var iRet = WebVideoCtrl.I_Logout(szIP);
                }
                console.log(szIP + " " + szInfo);
            },100)
        },
        error: function () {
            console.log(szIP + " 登录失败！");
        }
    });
    if (-1 == iRet) {
        console.log(szIP + " 已登录过！");
    }
}
</script>
