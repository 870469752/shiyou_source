<?php

use yii\helpers\Html;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = '报警日志';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    a{text-decoration: none;color:inherit;}
    table tr td{text-align: center}
    table tr th{text-align: center}
    table button{height: 20px}
</style>
<a  id="show_vedio_div"  data-toggle="modal" data-target="#myModal" ></a>
<section id="widget-grid" class="">
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-sortable="false" data-widget-fullscreenbutton="false"
                 data-widget-togglebutton="false">
                <header><span class="widget-icon"><i class="fa fa-table"></i></span>
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" data-target="#SaveMenu" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                </header>
                <div class="well well-sm well-light">
                    <table id="datatable_all" name ='datatable' class="table table-striped table-bordered table-hover" style="width: 100%">
                        <thead>
                            <tr>
                            <th class="hasinput">
                            </th>
                            <th class="hasinput icon-addon">
                                    <span style="width: 48%;float: left">
                                        <input id="timestart" type="text" placeholder="From" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                        <label for="timestart" style="float: left" class="glyphicon glyphicon-calendar no-margin padding-top-15" rel="tooltip" title=""></label>
                                    </span>
                                    <span style="width: 48%;float: right">
                                        <input id="timeend" type="text" placeholder="To" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                        <label for="timeend" class="glyphicon glyphicon-calendar no-margin padding-top-15" rel="tooltip" title="" ></label>
                                    </span>
                            </th>
                            <th class="hasinput">
                                <input type="text" class="form-control" placeholder="Name" />
                            </th>
                            <th class="hasinput">
                                <input type="text" class="form-control" placeholder="Description" />
                            </th>
                            <th class="hasinput">
                                <input type="text" class="form-control" placeholder="Level" />
                            </th>
                            <th class="hasinput icon-addon">
                                <input id="end_time" type="text" placeholder="Recover_time" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                <label for="end_time" class="glyphicon glyphicon-calendar no-margin padding-top-15" rel="tooltip" title="" data-original-title="Filter Date"></label>
                            </th>
                            <th style="text-align: left">
                                <select id="logstate" class="input-sm">
                                    <option value="0" selected>全部</option>
                                    <option value="1">已确认</option>
                                    <option value="2">未确认</option>
                                    <option value="3">已恢复</option>
                                </select>
                            </th>
                            <th colspan="2"></th>
                        </tr>
                            <tr>
                                <th data-class="expand" style="width:2%;"><input type="checkbox" id='checkAll' name="checkAll"></th>
                                <th data-class="expand" style="width:20%">日期</th>
                                <th data-class="expand">报警点</th>
                                <th data-class="expand">描述</th>
                                <th data-class="expand" style="width: 5%">等级</th>
                                <th data-class="expand">恢复时间</th>
                                <th data-class="expand" style="width: 7%">状态</th>
                                <th data-class="expand">确认</th>
                                <th data-class="expand">查看</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- END MAIN CONTENT -->



<script>

window.onload = function() {
    var oTable_all = initTable('datatable_all','oTable_all');

    szPort = 80;
    szUsername = "admin";
    szPassword = "12345";
    indexNum = 0;

    /**
     * 初始化表格数据
     * name 表格ID
     */
    function initTable(name,otable) {
        var alarm_rule_id = <?=$alarm_rule_id;?>;
        var rule = '/alarm-log/default/get-all-alarm-log';
        if(alarm_rule_id){
            rule = '/alarm-log/default/get-all-alarm-log?alarm_rule_id='+ alarm_rule_id;
        }
        /* TABLETOOLS */
        table = $('#'+name).DataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-3'f><'col-xs-12 col-sm-3 "+otable+"'><'col-sm-6 col-xs-4 hidden-xs'TC>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
            //"sdom": "Bfrtip",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "xls",
                    {
                        "sExtends": "print",
                        "sMessage": ""
                    }
                ],
                "sRowSelect": "os",
                "sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
            },
            "autoWidth": true,
            "lengthMenu": [[14,15, 16, 20, 25, 30, -1], [14,15, 16, 20, 25, 30, "All"]],
            "iDisplayLength": 14,
            //"bSort": false,
            "language": {
                "sProcessing": "处理中...",
                "sClear":"test",
                "sLengthMenu": "显示 _MENU_ 项结果",
                "sZeroRecords": "没有匹配结果",
                "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                "sInfoPostFix": "",
                "sSearch": "搜索:",
                "sUrl": "",
                "sEmptyTable": "表中数据为空",
                "sLoadingRecords": "载入中...",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上页",
                    "sNext": "下页",
                    "sLast": "末页"
                },
                "oAria": {
                    "sSortAscending": ": 以升序排列此列",
                    "sSortDescending": ": 以降序排列此列"
                }
            },
            "processing": false,
            "serverSide": true,
            'bPaginate': true,
            "bDestory": true,
            "bRetrieve": true,
            "ajax": {
                "url": rule,
                "type": "post",
                "error": function () {
                    alert("服务器未正常响应，请重试");
                }
            },
            "aoColumns": [
                {"mDataProp": "id",
                    "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<input type='checkbox' name='checkList_"+name+"' value='" + sData + "' >");
                    }
                },
                {"mDataProp": "start_time"},
                {"mDataProp": "point_name"},
                {"mDataProp": "description"},
                {"mDataProp": "alarm_level"},
                {"mDataProp": "end_time",
                    "bSortable": true,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if(!sData){
                            sData = '';}
                        $(nTd).html(sData);
                    }
                },
                {
                    "mDataProp": "notice",
                    "bSortable": true,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var temp_sData = '';
                        if(sData == 0){
                            temp_sData = '未确认';
                        }else if(sData == 1){
                            temp_sData = '已确认';
                        }else{
                            temp_sData = '已恢复';
                        }
                        $(nTd).html(temp_sData);
                    }
                },
                {"mDataProp": "id",
                    "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).html("<a title='确定' class='glyphicon glyphicon-ok' onclick=toConfirm(this,\'"+sData +"\')></a>");
                    }
                },
                {"mDataProp": "link_point",
                    "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if(sData){
                            $(nTd).html("<a title='视频'  class='glyphicon glyphicon-facetime-video' onclick=toShowVideo(\'"+sData +"\')></a>");
                        }
                    }
                }
            ],
            "createdRow": function ( row, data, index ) {
                if ( data.notice==='2' ) {
                    $('td', row).css('color','black');
                }else if ( data.notice === '1' ) {
                    $('td', row).css('color','green');
                }else
                    $('td', row).css('color','red');
            },
            "fnInitComplete": function (oSettings, json) {
                $('<a href="#" class="btn btn-danger" id="deleteFun'+name+'">删除</a>' + '&nbsp;').appendTo($('.'+otable));//myBtnBox
                $("#deleteFun"+name).click(function(){
                    _deleteList(name,otable);
                });
            },
            "order": [1, 'desc']
        });
        return table;
    }

    $("#datatable_all thead th input[type=text]").on( 'keyup change', function (e) {
        var col_index = $(this).parent().index();
        var val = this.value;
        var id = this.id;
        if(id=="timestart"||id=="timeend"){
            var timestart = $("#timestart").val();
            var timeend = $("#timeend").val();
            if(timestart!=''&&timeend!=''){
                if(timeend<timestart){

                    $.SmartMessageBox({
                        title : "<span style='color: yellow'>提示：</span>",
                        content : "<span style='font-size: 20px'>结束时间小于开始时间,请重新选择结束时间！</span>",
                        buttons : '[确定]'
                    }, function(ButtonPress, Value) {
                        $("#timeend").focus();
                        return false;
                    });
                    e.preventDefault();
                }else{
                    val = timestart+" 00:00:01--"+timeend+" 23:59:59";
                }
            }
        }
        if(col_index==0){
            col_index=1;
        }
        oTable_all.columns(col_index).search( val ).draw();
    } );

    $("#logstate").on( 'change', function () {
        var col_index = $(this).parent().index();
        var val = this.value;
        oTable_all.columns(col_index).search( val ).draw();
    } );


    function check_all(name){
        $("input[name='checkList_"+name+"']").each(function(){
            $(this).prop('checked',true);
        });
    }

    function check_none(name){
        $("input[name='checkList_"+name+"']").each(function(){
            $(this).filter(':checkbox').prop('checked',false);
        })
    }

    $('#checkAll').click(function(){
        var $status = $(this).prop('checked');
        $status ? check_all('datatable') : check_none('datatable');
    })
    /**
     * 批量删除
     * @private
     */
    function _deleteList(name,otable) {
        var str = '';
        $("input[name='checkList_"+name+"']:checked").each(function (i, o) {
            str += $(this).val();
            str += ",";
        });
        if (str.length > 0) {
            var IDS = str.substr(0, str.length - 1);
            $.SmartMessageBox({
                title : "<span style='color: yellow'>提示：</span>",
                content : "<span style='font-size: 20px'>你要删除的数据集id为" + IDS+"</span>",
                buttons : '[取消] [确定]'
            }, function(ButtonPress, Value) {
                if(ButtonPress=='确定'){
                    _deleteFun(IDS,otable);
                }
            })
        } else {
            $.SmartMessageBox({
                title : "<span style='color: yellow'>提示：</span>",
                content : "<span style='font-size: 20px'>请至少选择一条记录操作！</span>",
                buttons : '[确定]'
            })
        }
    }

    /**
     * 删除
     * @param id
     * @private
     */
    function _deleteFun(ids,otalbe) {
        $.ajax({
            url: "/alarm-log/default/delete-alarm-log",
            data: {"ids": ids},
            type: "post",
            success: function (backdata) {
                if (backdata) {
                    resetoTable();
                } else {
                    alert('删除发生错误，请联系管理员!');
                }
            }, error: function (error) {
                console.log(error);
            }
        });
    }

    function resetoTable(){
        //console.log(oTable_all);
        var tem_table = $('#datatable_all').dataTable();
        tem_table.fnDeleteRow(this);
    }

    $.fn.dataTableExt.oApi.fnReloadAjax = function (oSettings) {
        this.fnClearTable(this);
        this.oApi._fnProcessingDisplay(oSettings, true);
        var that = this;

        $.getJSON(oSettings.sAjaxSource, null, function (json) {
            /* Got the data - add it to the table */
            for (var i = 0; i < json.aaData.length; i++) {
                that.oApi._fnAddData(oSettings, json.aaData[i]);
            }
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            that.fnDraw(that);
            that.oApi._fnProcessingDisplay(oSettings, false);
        });
    }
}

/**
 * 确认报警信息
 * @private
 */
function toConfirm(obj,id) {
    $.ajax({
        url: '/alarm-log/default/confirm?id='+id,
        success: function (data) {
            getRowObj(obj);
        }
    });
}

//改变行样式
function getRowObj(obj) {
    //console.log(obj);return false;
    var ps=obj.parentNode;
    var bro = $(ps).prev();
    if($(bro).text() != '已恢复'){
        $(bro).text("已确认");
        var i = 0;
        while(obj.tagName.toLowerCase() != "tr"){
            obj = obj.parentNode;
            if(obj.tagName.toLowerCase() == "table")return null;
        }//console.log(obj);return false;
        $('td', obj).css('color','green');
    }
}

/**
 * 查看报警点位关联视频
 * @private
 */
function toShowVideo(id) {
    indexNum = 0;
    $.ajax({
        url: '/alarm-log/default/show-video?id='+id,
        success: function (data) {
            dataChange(data);
        }
    });
}

function dataChange(data){
    var temp_data = JSON.parse(data);
    var temp_len = temp_data.length;
    var num = 1;
    if(temp_len>1 && temp_len<=4){
        num = 2;
    }else if(temp_len>4 && temp_len<=9){
        num = 3;
    }else if(temp_len>9 && temp_len<=16){
        num = 4;
    }else if(temp_len>16){
        num = 4;
        temp_data = temp_data.slice(0,16);
    }
    var tem_class = $("#myModal").attr("class");
    if(tem_class == "modal"){
        $("#show_vedio_div").click();
    }
    $("#divPlugin").text("");
    init_env(num);
    camera_patrol(temp_data);
}

function init_env(num){
    // 初始化插件参数及插入插件
    WebVideoCtrl.I_InitPlugin('100%','410', {
        iWndowType: num,
        cbSelWnd: function (xmlDoc) {
            g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
        }
    });
    WebVideoCtrl.I_InsertOBJECTPlugin("divPlugin");

    // 窗口事件绑定
    $(window).bind({
        resize: function () {
            var $Restart = $("#restartDiv");
            if ($Restart.length > 0) {
                var oSize = getWindowSize();
                $Restart.css({
                    width: oSize.width + "px",
                    height: oSize.height + "px"
                });
            }
        }
    });

    //初始化日期时间
    var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
    $("#starttime").val(szCurTime + " 00:00:00");
    $("#endtime").val(szCurTime + " 23:59:59");
}

function camera_patrol(data){
    $.each(data, function (k, v) {
        clickLogin2(v.ip,v.channel_id);
        sleep(100);
    })
}

function sleep(n){
    var  start=new Date().getTime();
    while(true) if(new Date().getTime()-start>n)  break;
}

function clickLogin2(szIP,ID) {
    if( szIP == '11.8.173.51'|| szIP == '11.8.173.52'|| szIP == '11.8.173.53'|| szIP == '11.8.173.54'){
        szPassword = "7ca29e68";
    }else{
        szPassword = "12345";
    }
    console.log(szPassword);
    var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
        success: function (xmlDoc) {
            console.log(szIP + " 登录成功！");
            setTimeout(function(){
                var oWndInfo = WebVideoCtrl.I_GetWindowStatus(indexNum);
                if (oWndInfo != null) {// 已经在播放了，先停止
                    WebVideoCtrl.I_Stop(indexNum);
                }
                var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
                    iWndIndex:indexNum,
                    iStreamType: 1,
                    iChannelID: ID,
                    bZeroChannel: false
                });
                indexNum++;
                if (0 == iRet) {
                    szInfo = "开始预览成功！";
                    var iRet = WebVideoCtrl.I_Logout(szIP);
                } else {
                    szInfo = "开始预览失败！";
                    var iRet = WebVideoCtrl.I_Logout(szIP);
                }
                console.log(szIP + " " + szInfo);
            },100)
        },
        error: function () {
            console.log(szIP + " 登录失败！");
        }
    });
    if (-1 == iRet) {
        console.log(szIP + " 已登录过！");
    }
}




</script>

<div class="modal"  style="padding-top: 100px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 650px;height: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h6 class="modal-title" id="myModalLabel">视频监控 </h6>
            </div>
            <div class="modal-body" id="divPlugin">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$this->registerJsFile("js/vedio/webVideoCtrl.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/vedio/demo.js", ['yii\web\JqueryAsset']);
?>
