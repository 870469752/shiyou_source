<?php

namespace backend\module\alarmlog;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\alarmlog\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
