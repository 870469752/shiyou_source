<?php

namespace backend\module\alarmlog\controllers;
use Yii;

use backend\models\AlarmLog;
use backend\controllers\MyController;
use backend\models\Location;
use backend\models\CameraPoint;
use common\library\Ssp;
use backend\models\Image;
use backend\models\SubSystem;
use common\library\MyFunc;
use  yii\web\Session;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogs()
    {
        $id=669;
        $all_image = new Image();
        $all_image = $all_image->findAllImage();
        $model = SubSystem::findModel($id);
        $sub_system = SubSystem::find()->where(['id' => $id])->asArray()->all();
        $sub_system_data = SubSystem::getSubSystemPoint($sub_system);
        $event_points = SubSystem::GetEventPoint($sub_system_data['events']);
        $save_state = SubSystem::SavePointCommand($event_points);
        $save_state = SubSystem::SavePointCommand($sub_system_data['points']);
        $sub_system_data = json_encode($sub_system_data);
        $base_map = 'uploads/pic/map1.jpg';
        $user_id = Yii::$app->user->id;
        $control = 0;

        $type = Yii::$app->request->get('type');//判断报表类型 1实时报表 2报表日志
        $alarm_rule_id = Yii::$app->request->get('alarm_rule_id');
        if($alarm_rule_id){$data=['alarm_rule_id'=>$alarm_rule_id];}else{$data=['alarm_rule_id'=>0];}
        if($type == 1){
            return $this->render('logs',[
                'control' => $control,
                'user_id' => $user_id,
                'model' => $model,
                'base_map' => $base_map,
                'all_image' => $all_image,
                'id' => $id,
                'sub_system_data' => $sub_system_data
            ]);
        } elseif($type == 3){
            return $this->render('firelogs',[
                'control' => $control,
                'user_id' => $user_id,
                'model' => $model,
                'base_map' => $base_map,
                'all_image' => $all_image,
                'id' => $id,
                'sub_system_data' => $sub_system_data
            ]);
        }else {
            return $this->render('logall',$data);
        }
    }

    public function actionGetDayAlarmLog(){
        $alarm_log_model = new AlarmLog;
        $alarm_log = $alarm_log_model->byGetLogsAll('day');//day 当天报警；all所有报警
        if(!empty($alarm_log)){
            return json_encode($alarm_log);
        }else{
            return false;
        }
    }

    public function actionGetFireDayAlarmLog(){
        $alarm_log_model = new AlarmLog;
        $alarm_log = $alarm_log_model->byGetFireLogsAll('day');//day 当天报警；all所有报警
        if(!empty($alarm_log)){
            return json_encode($alarm_log);
        }else{
            return false;
        }
    }


    public function actionGetAlarmById($id){
        $alarm_log_model = new AlarmLog;
        $alarm_log = $alarm_log_model->byGetAlarmById($id);
        if(!empty($alarm_log)){
            return json_encode($alarm_log);
        }else{
            return false;
        }
    }

    public function actionGetAllAlarmLog(){
        $str = '';
        $con = " id !=0 ";
        //被注释掉的是正常应用
        //$rule_ids = json_decode(Yii::$app->cache->get("rules".Yii::$app->user->id));
        $user_name = Yii::$app->user->identity->username;
        if(strstr($user_name, '电力')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Ele"));
        }elseif(strstr($user_name, '安全')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Safe"));
        }elseif(strstr($user_name, '设施')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Equi"));
        }elseif(strstr($user_name, '消防')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Fire"));
        }else{
            $rule_ids  = null;
        }
        if(!empty($rule_ids)){
            foreach($rule_ids as $k => $v){
                if($v){$str.=$v.',';}else{continue;}
            }
            $str = rtrim($str,',');
            if($str){
                $con .= " and alarm_rule_id in ($str) ";
            }
        }
        $alarm_rule_id = Yii::$app->request->get('alarm_rule_id');
        if($alarm_rule_id){
            $con .= " and alarm_rule_id = $alarm_rule_id ";
        }
        $table = "public.view_alarm_log_info";
        $primaryKey = "id";
        $columns = array(
            array(
                "db" => "id",
                "dt" => "DT_RowId",
                "formatter" => function( $d, $row ) {
                        return "row_".$d;
                    },
                "dj" =>"id"
            ),
            array( "db" => "id", "dt" => "id", "dj"=>"id"),
            array( "db" => "is_confirmed",  "dt" => "is_confirmed" ,"dj"=>"is_confirmed"),
            array( "db" => "start_time",  "dt" => "start_time" ,"dj"=>"start_time"),
            array( "db" => "point_name->'data'->>'zh-cn' as point_name","dt" => "point_name" ,"dj"=>"point_name->'data'->>'zh-cn'" ),
            array( "db" => "description->'data'->>'zh-cn' as description","dt" => "description","dj"=>"description->'data'->>'zh-cn'" ),
            array( "db" => "alarm_level",  "dt" => "alarm_level",  "dj" => "alarm_level" ),
            array( "db" => "link_point",  "dt" => "link_point",  "dj" => "link_point" ),
            array( "db" => "end_time",  "dt" => "end_time" ,"dj"=>"end_time"),
        );
        $time = $_POST['columns']['1']['search']['value'];
        $notice = $_POST['columns']['6']['search']['value'];
        if(strstr($time,"--")){
            $time_arr = explode("--",$time);
            $con.=" and (start_time>'".$time_arr[0]."' and start_time<'".$time_arr[1]."') ";
            $_POST['columns']['1']['search']['value']='';
        }
        if($notice==1){
            $con.= "and (is_confirmed = TRUE and end_time is NULL) ";
        }elseif($notice==2){
            $con.= "and (is_confirmed = FALSE and end_time is  NULL) ";
        }elseif($notice==3){
            $con.= "and (end_time is not NULL) ";
        }
        $_POST['columns']['6']['search']['value']='';
        $result = Ssp::simple( $_POST, $table, $primaryKey, $columns, $con );
        foreach($result['data'] as $k => $v){
            $result['data'][$k]['notice'] = $v['is_confirmed'].'';
            if($v['end_time']){
                $result['data'][$k]['notice'] = '2';
            }
            $result['data'][$k]['start_time']=substr($v['start_time'],0,19);
            $result['data'][$k]['end_time']=substr($v['end_time'],0,19);
        }
        echo json_encode( $result);
    }

    public function getAlarmSys($alarm_log){
        $role_id = Yii::$app->user->identity->role_id;  //4 all;  5 ele;  6 equi  7  safe
        $key = "sys_pos";
        $Info = $this->object_to_array(json_decode(Yii::$app->redis->get($key)));
        //向报警日志数组里添加报警位置信息
        foreach($alarm_log as $k => $v){
            $alarm_log[$k]['position'] = '';
            foreach($Info['pos'] as $x => $y){
                $temp = explode(",",$y['point_id']);
                if(in_array($v['point_id'],$temp)){
                    $alarm_log[$k]['position'] = $y['name'];
                    break;
                }
            }
        }
        //给各子系统添加报警点位
        foreach($Info['sys'] as $k => $v){
            $Info['sys'][$k]['alarm_point'] = [];
            $temp = explode(",",$v['point_id']);
            foreach($alarm_log as $x => $y){
                if(in_array($y['point_id'],$temp)){
                    $y['sys_name'] = $v['name'];
                    $Info['sys'][$k]['alarm_point'][] = $y;
                }
            }
        }

        $alarm_sys=array('1'=>array(),'2'=>array(),'3'=>array());//电力1，安全2，楼控3
        //合并各子系统，统一分为电力，楼控，安全三大系统
        foreach($Info['sys'] as $k => $v){
            if($v['id']==1471 || $v['id']==1470 ||$v['id']==1474 ||$v['id']==1473 ||$v['id']==1472 ||$v['id']==1481 ){
                $alarm_sys['3']=array_merge($alarm_sys['3'],$v['alarm_point']);
            }elseif($v['id']==1477 || $v['id']==1479 ||$v['id']==1480 ||$v['id']==1478 || $v['id']==1476){
                $alarm_sys['2']=array_merge($alarm_sys['2'],$v['alarm_point']);
            }else{
                $alarm_sys['1']=array_merge($alarm_sys['1'],$v['alarm_point']);
            }
        }

        //分类后，给报警点位排序
        foreach($alarm_sys as $k => $v){
            if(!empty($v['alarm_point'])){
                $alarm_sys[$k]['alarm_point']=$this->my_sort($v['alarm_point'],'id');
            }
        }

        //判断用户权限（目前还没有权限分配模块，待权限分配模块及系统模块确定后改进）
        if($role_id == 5){
            unset($alarm_sys['2'],$alarm_sys['3']);
        }elseif($role_id == 6){
            unset($alarm_sys['1'],$alarm_sys['2']);
        }elseif($role_id == 7){
            unset($alarm_sys['3'],$alarm_sys['1']);
        }
        return $alarm_sys;
    }

    //对象转数组(包括多维)
    function object_to_array($obj){
        $_arr = is_object($obj) ? get_object_vars($obj) :$obj;
        foreach ($_arr as $key=>$val){
            $val = (is_array($val) || is_object($val)) ? $this->object_to_array($val):$val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    static function my_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
        return $arrays;
    }

    /**
     * 页面 确认报警方法
     * @param $id
     */
    public function actionConfirm($id)
    {
        $result = AlarmLog::updateById($id, ['is_confirmed' => 1]);
        return json_encode($result);
    }

    /**
     * 查找报警关联视频点位
     * @param $id
     */
    public function actionShowVideo($id)
    {
        $result = null;
        $c_p = new CameraPoint();
        $ids = trim(str_replace('{','',str_replace('}','',$id)));
        $result = $c_p->getPointInfo($ids);
        return json_encode($result);
    }

    /**
     * 查找未弹出报警关联视频点位
     * @param $id
     */
    public function actionAutoShowVideo($log_id)
    {
        $alarm_log = AlarmLog::getLogsNoVedio($log_id);
        $result = array();
        if(!empty($alarm_log)){
            AlarmLog::setVedio($alarm_log);
            $ids = '';
            foreach($alarm_log as $k =>$v){
                $ids .= trim(str_replace('}',',',str_replace('{','',$v['p_link'])));
            }
            $ids = substr($ids,0,strlen($ids)-1);
            $c_p = new CameraPoint();
            $result = $c_p->getPointInfo($ids);
            return json_encode($result);
        }else{
            return false;
        }
    }

    //日志批量删除
    public function actionDeleteAlarmLog(){
        $ids = explode(",",Yii::$app->request->post('ids'));
        foreach($ids as $k=>$v){
            $a_l = new AlarmLog();
            $result = $a_l->_delete_all($v);
        }
        return $result;
    }

    public function actionLogs_2()
    {
        return $this->render('logs_2');
    }
}
