<?php

namespace backend\module\alarmlog\controllers;

use backend\models\AlarmRule;
use Yii;
use backend\models\AlarmLog;
use backend\models\search\AlarmLogSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\OperateLog;
use backend\models\Location;
use backend\models\PointCategoryRel;
use yii\web\Session;

/**
 * CrudController implements the CRUD actions for AlarmLog model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Lists all AlarmLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlarmLogSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    public function actionLog()
    {
        return $this->render(
            'log',
            [

            ]
        );
    }

    /**
     * Displays a single AlarmLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new AlarmLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AlarmLog;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing AlarmLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Deletes an existing AlarmLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['log']);
    }

    /**
     * 获取最新的10条未确定的报警数据
     * @param $type 0:页面刷新数据  1:确认后刷新数据
     * @param $ids 数据ID
     */
    public function actionAjaxSources()
    {
        $type = Yii::$app->request->get('type');
        $ids = Yii::$app->request->get('ids');
        if ($type == 1) {
            if($ids){
                $this->actionConfirmAll($ids);
            }
        }
        echo json_encode(AlarmLog::getLogs(), JSON_UNESCAPED_UNICODE);
    }

    public function actionLogChart()
    {
        $model = new AlarmLog();
        $model->load(Yii::$app->request->get(), '');
        MyFunc::TimeFloor($model->time, $model->type);
        echo json_encode(AlarmRule::getRecentLog(), JSON_UNESCAPED_UNICODE);
    }

    /**
     * 确认当前top上展示的报警日志
     * @param $ids
     */
    public function actionConfirmAll($ids)
    {
        $res = AlarmLog::updateConfirmById(trim($ids, ','));
        //$this->actionAjaxAutomaticMonitoring();
        //执行js中的返回上一次访问的地址
        //echo "<script type='text/javascript'>history.back();</script>";
    }


    public function actionScatter()
    {
        $model = new AlarmLog;
        $model->validate();
        return $this->render(
            'scatter',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * 页面 确认报警方法
     * @param $id
     */
    public function actionConfirm($id)
    {
        /*操作日志*/
        $op['zh'] = '确认报警';
        $op['en'] = 'Confirm alarm';
        @$this->getLogOperation($op, '', '');

        AlarmLog::updateById($id, ['is_confirmed' => 1]);
        //更新 session['alarm_log_count]
        $this->actionAjaxAutomaticMonitoring();
        return $this->redirect(['index']);
    }

    /**
     * 报警自动监测
     * @return int
     */
    public function actionAjaxAutomaticMonitoring()
    {
        $new_log_count = AlarmLog::getTotal();
        $session = new Session();
        $session->open();
        $old_log_count = isset($session['alarm_log_count']) && !empty($session['alarm_log_count']) ? $session['alarm_log_count'] : 0;
        //更新session中bacnet_count 的值
        $session['alarm_log_count'] = $new_log_count;
        $session->close();
        $res_log = $new_log_count - $old_log_count;
        return $res_log;
    }

    /**
     * ajax 获取数据
     * @return string
     */
    public function actionAjaxGetTableData()
    {
        $params = Yii::$app->request->getQueryParams();
        $data = AlarmLog::getLogByPage($params['page'], $params['rows']);
        $count = AlarmLog::count();
        if ($count > 0) {
            $total_pages = ceil($count / $params['rows']);
        } else {
            $total_pages = 0;
        }
        $log_data['total'] = $total_pages;

        foreach ($data as $key => $value) {
            $log_data['rows'][] = [
                'id' => $value['id'],
                'cell' => [
                    $value['id'],
                    $value['is_confirmed'],
                    $value['alarm_level'],
                    MyFunc::status($value['send_status']),
                    MyFunc::TimeFormat($value['start_time']),
                    MyFunc::TimeFormat($value['end_time']),
                    MyFunc::DisposeJSON($value['description']),
                    MyFunc::DisposeJSON($value['system_log']),
                    MyFunc::DisposeJSON($value['remark'])
                ],
            ];
        }
        return json_encode($log_data);
    }

    public function actionAjaxEditTableData()
    {
        if ($id = Yii::$app->request->getbodyparam('id')) {
            return AlarmLog::updateById($id, Yii::$app->request->getbodyparams());
        }
        return false;
    }

    /**
     * Finds the AlarmLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AlarmLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AlarmLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 计算报警日志里未确认的日志条数
     * @return int
     */
    public function actionAlarmNum()
    {
        return count(AlarmLog::getLogsAll());
    }

    /**
     * 计算报警日志里未确认的日志,及报警样式
     * @return array;
     */
    public function actionAlarmNoNotice()
    {
        $role_id = MyFunc::getRoleId();;  //4 all;  5 ele;  6 equi  7  safe
        $tem_sys = 0;
        if($role_id == 5){
            $tem_sys = 1;
        }elseif($role_id == 6){
            $tem_sys = 3;
        }elseif($role_id == 7){
            $tem_sys = 2;
        }
        $alarm_log = AlarmLog::getLogsNoNotice($tem_sys);
        $style = [];

        if(!empty($alarm_log)){
            AlarmLog::setNotice($alarm_log);
            foreach ($alarm_log as $k => $v) {
                if(!empty($alarm_log[$k])){
                    $style[$k]['position'] = $v['position'];
                    $style[$k]['p_name'] = $v['p_name'];
                    $style[$k]['title'] = $v['name'];
                    $style[$k]['content'] = $v['description'];
                    $style[$k]['color'] = $this->actionAlarmColor($v['notice_style']);
                    $style[$k]['timeout'] = $v['notice_siren'] * 1000;
                    $style[$k]['sound'] = $this->actionAlarmSound($v['notice_fade']);
                    $style[$k]['icon'] = 'fa fa-bell swing animated';
                    $style[$k]['number'] = $v['id'];
                    $style[$k]['sys_name'] = $v['sys_name'];
                }
            }
        }else{
            return false;
        }
        return json_encode($style);
    }

    /**
     * 返回报警框颜色
     * @return string
     */
    public function actionAlarmColor($data)
    {
        switch($data){
            case "0":
                return "#CD0000";
                break;
            case "1":
                return "#5D478B";
                break;
            case "2":
                return "#EEB422";
                break;
            case "3":
                return "#90EE90";
                break;
            default :
                return "#90EE90";
                break;
        }
    }

    /**
     * 返回报警框颜色
     * @return string
     */
    public function actionAlarmSound($data)
    {
        $url = "http://".$_SERVER['HTTP_HOST']."/sound/";
        switch($data){
            case "0":
                return 0;
                break;
            case "1":
                return $url."voice_alert1.mp3";
                break;
            case "2":
                return $url."voice_alert2.mp3";
                break;
            case "3":
                return $url."voice_alert3.mp3";
                break;
            case "4":
                return $url."voice_alert4.mp3";
                break;
            default :
                return 0;
                break;
        }
    }

    /**
     * @param $alarm_log
     * @return array
     */
    public function getAlarmSys($alarm_log){
        $role_id = MyFunc::getRoleId();;  //4 all;  5 ele;  6 equi  7  safe
        $key = "sys_pos";
        $Info = $this->object_to_array(json_decode(Yii::$app->redis->get($key)));
        //向报警日志数组里添加报警位置信息
        foreach($alarm_log as $k => $v){
            $alarm_log[$k]['position'] = '';
            foreach($Info['pos'] as $x => $y){
                $temp = explode(",",$y['point_id']);
                if(in_array($v['point_id'],$temp)){
                    $alarm_log[$k]['position'] = $y['name'];
                    break;
                }
            }
        }

        //给各子系统添加报警点位
        foreach($Info['sys'] as $k => $v){
            $Info['sys'][$k]['alarm_point'] = [];
            $temp = explode(",",$v['point_id']);
            foreach($alarm_log as $x => $y){
                if(in_array($y['point_id'],$temp)){
                    $y['sys_name'] = $v['name'];
                    $Info['sys'][$k]['alarm_point'][] = $y;
                }
            }
        }
        //echo "<pre>";print_r($Info);die;
        $alarm_sys=array('1'=>array(),'2'=>array(),'3'=>array());//电力1，安全2，楼控3
        //合并各子系统，统一分为电力，楼控，安全三大系统
        foreach($Info['sys'] as $k => $v){
            if($v['id']==1471 || $v['id']==1470 ||$v['id']==1474 ||$v['id']==1473 ||$v['id']==1472 ||$v['id']==1481 ){
                $alarm_sys['3']=array_merge($alarm_sys['3'],$v['alarm_point']);
            }elseif($v['id']==1477 || $v['id']==1479 ||$v['id']==1480 ||$v['id']==1478 || $v['id']==1476){
                $alarm_sys['2']=array_merge($alarm_sys['2'],$v['alarm_point']);
            }else{
                $alarm_sys['1']=array_merge($alarm_sys['1'],$v['alarm_point']);
            }
        }

        //分类后，给报警点位排序
        foreach($alarm_sys as $k => $v){
            if(!empty($v['alarm_point'])){
                $alarm_sys[$k]['alarm_point']=$this->my_sort($v['alarm_point'],'id');
            }
        }

        //判断用户权限（目前还没有权限分配模块，待权限分配模块及系统模块确定后改进）
        if($role_id == 5){
            unset($alarm_sys['2'],$alarm_sys['3']);
            return array_merge($alarm_sys['1']);
        }elseif($role_id == 6){
            unset($alarm_sys['1'],$alarm_sys['2']);
            return array_merge($alarm_sys['3']);
        }elseif($role_id == 7){
            unset($alarm_sys['3'],$alarm_sys['1']);
            return array_merge($alarm_sys['2']);
        }else{
            return array_merge($alarm_sys['1'],$alarm_sys['2'],$alarm_sys['3']);
        }
    }

    //对象转数组(包括多维)
    function object_to_array($obj){
        $_arr = is_object($obj) ? get_object_vars($obj) :$obj;
        foreach ($_arr as $key=>$val){
            $val = (is_array($val) || is_object($val)) ? $this->object_to_array($val):$val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    static function my_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
        if(is_array($arrays)){
            foreach ($arrays as $array){
                if(is_array($array)){
                    $key_arrays[] = $array[$sort_key];
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
        return $arrays;
    }

    public function actionAddName($arr){
        $temp = $arr;
        foreach($arr as $k => $v){
            if($v['parent_id'] != 1468){
                foreach($temp as $x => $y) {
                    if($v['parent_id']==$y['id']){
                        $arr[$k]['name'] = $y['name'].' '.$v['name'];
                        $arr[$k]['parent_id'] = $y['parent_id'];
                        break;
                    }
                }
            }else{
                continue;
            }
        }
        return $arr;
    }

    /**
     * 获得地理位置及子系统信息
     * @return array
     */
    public function actionGetPointInfo(){
        $temp_num_1 = 1468;//地理位置
        $temp_num_2 = 1469;//子系统
        $location_model = new Location;
        $location_pos = $location_model->byGetSubCategory($temp_num_1);//所有地理位置
        $location_sys = $location_model->byGetSubCategory($temp_num_2);//所有子系统
        $point_category_rel_model = new PointCategoryRel;
        foreach($location_pos as $k => $v){
            $location_pos[$k]['point_id'] = $point_category_rel_model->byGetPointIds($v['id']);//添加点位信息
        }
        foreach($location_sys as $k => $v){
            $location_sys[$k]['point_id'] = $point_category_rel_model->byGetPointIds($v['id']);//添加点位信息
        }

        $location_pos_name = $this->actionAddName($this->actionAddName($location_pos));
        $arr['sys'] = $location_sys;
        $arr['pos'] = $location_pos_name;
        return $arr;
    }
}