<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-20
 * Time: 上午10:32
 */

namespace backend\module\point_link\controllers;

//yii 引用
use backend\models\Point;
use backend\models\PointLink;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\MyController;
use common\library\MyFunc;
use backend\models\Category;
use backend\models\Location;
use yii\data\ActiveDataProvider;
use common\models\User;
/**
 * CrudController implements the CRUD actions for Point model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Point models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query=PointLink::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $user=User::findOne( Yii::$app->user->id);
//        $link_control=$user->link_control;
//        if($link_control==1) {
            return $this->render('index', [
                'dataProvider' => $dataProvider
            ]);
//        }
//        else {
//            return $this->render('index_simple', [
//                'dataProvider' => $dataProvider
//            ]);
//        }
    }
    public function actionCreate(){

        $main_point=[];
        $link_point=[];
        $tree = MyFunc::TreeData(Category::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);
        return $this->render('_form',[
            'id'=>0,
            'name'=>'',
            'main_point_name'=>'',
            'link_point_name'=>'',
            'selectedPointContainer'=>MyFunc::multi_select_point($main_point),
            'selectedPointContainer1'=>MyFunc::multi_select_point($link_point),
            'category_tree' => $tree,
            'location_tree' => $location_tree

        ]);
    }

    public function actionUpdate($id){
        $model=$this->findModel($id);
        $main_point=$model->main_point;
        $main_point=Point::find()->where(['id'=>$main_point])->asArray()->all();
        $link_point=$model->link_point;
        $link_point=substr($link_point,1,strlen($link_point)-2);
        $link_point=explode(',',$link_point);
        $link_point=Point::find()->where(['id'=>$link_point])->asArray()->all();

        $tree = MyFunc::TreeData(Category::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($tree, ['id' => 0, 'name' => '所有分类']);

        $location_tree = MyFunc::TreeData(Location::getAllCategory());
        //在此添加一个 所有分类
        array_unshift($location_tree, ['id' => 0, 'name' => '所有分类']);

        return $this->render('_form',[
            'id'=>$id,
            'name'=>MyFunc::DisposeJSON($model->name),
            'main_point_name'=>MyFunc::DisposeJSON($model->main_point_name),
            'link_point_name'=>MyFunc::DisposeJSON($model->link_point_name),
            'selectedPointContainer'=>MyFunc::multi_select_point($main_point),
            'selectedPointContainer1'=>MyFunc::multi_select_point($link_point),
            'category_tree' => $tree,
            'location_tree' => $location_tree
        ]);
    }

    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除用户删除图片';
        $op['en'] = 'Delete users to delete image';
        $this->getLogOperation($op);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAjaxPointLink(){
        $data=Yii::$app->request->post();
        if($data['id']==0)
        $model=new PointLink();
        else $model=$this->findModel($data['id']);
        $name=[
            'data_type' => 'description',
            'data' => [
                'zh-cn' => $data['name'],
                'en-us' => $data['name']
            ]
        ];
        $main_point_name=[
            'data_type' => 'description',
            'data' => [
                'zh-cn' => $data['main_point_name'],
                'en-us' => $data['main_point_name']
            ]
        ];
        $link_point_name=[
            'data_type' => 'description',
            'data' => [
                'zh-cn' => $data['link_point_name'],
                'en-us' => $data['link_point_name']
            ]
        ];

        $main_point=$data['main_point'];
        $link_point='{';
        foreach ($data['link_point'] as $key=>$value ) {
            $link_point=$link_point.$value.',';
        }
        $link_point=substr($link_point,0,strlen($link_point)-1).'}';


        $load_data['PointLink']=[
            'name' =>json_encode($name),
            'main_point'=>$main_point,
            'main_point_name'=>json_encode($main_point_name),
            'link_point'=>$link_point,
            'link_point_name'=>json_encode($link_point_name)

        ];
        $model->load($load_data);
        $model->save();
        echo '<pre>';
        print_r($model->errors);
        die;
    }

    protected function findModel($id)
    {
        if (($model = PointLink::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

