<?php

namespace backend\module\report_log;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\report_log\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
