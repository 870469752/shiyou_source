<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ReportLog $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Report Log',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
