<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ReportLog $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Report Log',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
