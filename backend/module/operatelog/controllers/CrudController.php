<?php

namespace backend\module\operatelog\controllers;

use Yii;
use backend\models\search\operatelogSearch;
use backend\controllers\MyController;
use yii\filters\VerbFilter;
/**
 * CrudController implements the CRUD actions for operatelog model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all operatelog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new operatelogSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

}
