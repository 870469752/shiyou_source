<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\operatelog $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Operatelog',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
