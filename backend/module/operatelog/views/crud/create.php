<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\operatelog $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Operatelog',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
