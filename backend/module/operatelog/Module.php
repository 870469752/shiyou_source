<?php

namespace backend\module\operatelog;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\operatelog\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
