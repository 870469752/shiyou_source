<?php

namespace backend\module\schedule_task;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\schedule_task\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
