<?php

namespace backend\module\schedule_task\controllers;

use backend\models\Role;
use common\models\User;
use Yii;
use backend\models\ScheduleTask;
use backend\models\Task;
use backend\models\search\ScheduleTaskSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for ScheduleTask model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScheduleTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduleTaskSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }


    /**
     * Displays a single ScheduleTask model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = new ScheduleTask;
        $roles_array=$model->findRole();
        $m=$model->find()->where(['id'=>$id])->asArray()->one();
//        echo "<pre>";
//        print_r($m);
//        die;
        $task= new Task();
        $alltask=$task->getNameMap();
        $usermodel=new User();
        $users=$usermodel->getUser();
        $users_array=array();
        $group=0;
        $assign_r=$model->find()->select(['assign_roles'])->where(['id'=>$id])->asArray()->one();
        $assign_r=explode(',',substr($assign_r['assign_roles'],1,strlen($assign_r['assign_roles'])-2));
        $assign_u=$model->find()->select(['assign_users'])->where(['id'=>$id])->asArray()->one();
        $assign_u=explode(',',substr($assign_u['assign_users'],1,strlen($assign_u['assign_users'])-2));
        for($i=0;$i<count($users);$i++)
        {
            $users_array[$users[$i]['id']]=$users[$i]['username'];
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

        } else {
            return $this->render('view', [
                'm' => $m,
                'alltask'=>$alltask,
                'task_id'=>$m['task_id'],
                'group'=>$group,
                'users_array'=>$users_array,
                'roles_array'=>$roles_array,
                'now_roles'=>$assign_r,
                'now_user_id'=>$assign_u,
            ]);
        }
    }

    /**
     * Creates a new ScheduleTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScheduleTask;
        $task= new Task();
        $alltask=$task->getNameMap();
        $usermodel=new User();
        $users=$usermodel->getUser();
        $users_array=array();
        $group=0;
        //获取当前用户的角色
        $user_roles=$usermodel->find()->select(['role_id'])->where(['id'=>Yii::$app->user->identity->id])->asArray()->one();
        $user_roles=$user_roles['role_id'];
        $user_roles=json_decode($user_roles,1);
        $user_roles_data_array=$user_roles['data'];
        //查询所有用户信息
         for($i=0;$i<count($users);$i++)
        {
            $users_array[$users[$i]['id']]=$users[$i]['username'];
        }
        //所有角色信息
        $roles_array=$model->findRole();

        if ($data = Yii::$app->request->post()) {

            //获取多条事件
            $schedule_tasks=json_decode($data['data'],1);
            foreach($schedule_tasks as $k=>$v){
                $model_new = new ScheduleTask;
                $schedule_mode=array(
                    "data_type"=>"schedule_task_time_mode",
                    "data"=>array(
                        "type"=>$v['type'],
                        "start"=>$v['start'].' 00:00:00',
                        "end"=>$v['end'].' 23:59:59',
                        "data"=>$v['config']
                     )
                );
                $name=array(
                    "data_type"=>"description",
                    "data"=>array(
                        "zh-cn"=>$v['config']['title'],
                        "en-us"=>''
                    )
                );
                $model_new->task_id=$v['task_id'];
                $model_new->create_timestamp = date('Y-m-d H:i:s');

                $model_new->update_timestamp = $model_new->create_timestamp;
                $model_new->creator = Yii::$app->user->identity->id.'';
                $model_new->time_mode=json_encode($schedule_mode);
                $model_new->name=json_encode($name);
                if($v['users']=="" || $v['users']==null) {
                    $model_new->assign_users='{'.Yii::$app->user->identity->id.'}';
                }else{
                    $model_new->assign_users='{'.$v['users'].'}';
                }
                if($v['roles']=="" || $v['roles']==null){
                    $r='';
                    foreach($user_roles_data_array as $k=>$v){
                        $r=$r.$v.',';
                    }
                    $r=substr($r,0,strlen($r)-1);
                    $model_new->assign_roles='{'.$r.'}';
                }else{
                    $model_new->assign_roles='{'.$v['roles'].'}';
                }
                $model_new->save();
            }
            return $this->redirect(['index']);
        } else {
            $models=$model->find()->asArray()->all();
            $groups=array();
            foreach($models as $k=>$v){
                $group_one=json_decode($v['time_mode'],1)['data']['data']['group'];
                $groups[]=$group_one;
            }
            if(count($groups)<=0){
                $group=0;
            }else{
                $group=max($groups);
            }
            return $this->render('create', [
                'model' => $model,
                'alltask'=>$alltask,
                'group'=>$group,
                'users_array'=>$users_array,
                'roles_array'=>$roles_array,
                'now_roles'=>$user_roles_data_array,
                'now_user_id'=>Yii::$app->user->identity->id,
            ]);
        }
    }

    /**
     * Updates an existing ScheduleTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //获得所有角色
        $roles_array=$model->findRole();
        //获得当前事件
        $m_m=new ScheduleTask();
        $m=$m_m->find()->where(['id'=>$id])->asArray()->one();
        //获得所有任务
        $task= new Task();
        $alltask=$task->getNameMap();
        //获得所有用户
        $usermodel=new User();
        $users=$usermodel->getUser();
        $users_array=array();
        for($i=0;$i<count($users);$i++)
        {
            $users_array[$users[$i]['id']]=$users[$i]['username'];
        }
        $group=0;
        //获取当前用户的角色
        $user_roles=$usermodel->find()->select(['role_id'])->where(['id'=>Yii::$app->user->identity->id])->asArray()->one();
        $user_roles=$user_roles['role_id'];
        $user_roles=json_decode($user_roles,1);
        $user_roles_data_array=$user_roles['data'];
        if ($data = Yii::$app->request->post()) {
            //获取多条事件
            $schedule_tasks=json_decode($data['data'],1);
            foreach($schedule_tasks as $k=>$v){
                $schedule_mode=array(
                    "data_type"=>"schedule_task_time_mode",
                    "data"=>array(
                        "type"=>$v['type'],
                        "start"=>$v['start'].' 00:00:00',
                        "end"=>$v['end'].' 23:59:59',
                        "data"=>$v['config']
                    )
                );
                $name=array(
                    "data_type"=>"description",
                    "data"=>array(
                        "zh-cn"=>$v['config']['title'],
                        "en-us"=>''
                    )
                );
                $model_update = $this->findModel($v['id']);
                $model_update->task_id=$v['task_id'];
                $model_update->update_timestamp = date('Y-m-d H:i:s');
                $model_update->creator = Yii::$app->user->identity->id.'';
                $model_update->time_mode=json_encode($schedule_mode);
                $model_update->name=json_encode($name);
                if($v['users']=="" || $v['users']==null) {
                    $model_update->assign_users='{'.Yii::$app->user->identity->id.'}';
                }else{
                    $model_update->assign_users='{'.$v['users'].'}';
                }
                if($v['roles']=="" || $v['roles']==null){
                    $r='';
                    foreach($user_roles_data_array as $k=>$v){
                        $r=$r.$v.',';
                    }
                    $r=substr($r,0,strlen($r)-1);

                    $model_update->assign_roles='{'.$r.'}';
                }else{
                    $model_update->assign_roles='{'.$v['roles'].'}';
                }
                $model_update->save();
            }
            return $this->redirect(['index']);
        } else {
            $mm=new ScheduleTask();
            $models=$mm->find()->asArray()->all();
            $groups=array();
            foreach($models as $k=>$v){
                $group_one=json_decode($v['time_mode'],1)['data']['data']['group'];
                $groups[]=$group_one;
            }
            if(count($groups)<=0){
                $group=0;
            }else
                $group=max($groups);{
            }
            return $this->render('update', [
                'm' => $m,
                'model'=>$model,
                'alltask'=>$alltask,
                'task_id'=>$m['task_id'],
                'group'=>$group,
                'users_array'=>$users_array,
                'roles_array'=>$roles_array
            ]);
        }
    }

    //得到同类任务的所有事件
    public function actionAjaxGetByTaskid($task_id)
    {
        $model = new ScheduleTask;
        //获取当前用户的角色
        $usermodel=new User();
        $user_roles=$usermodel->find()->select(['role_id'])->where(['id'=>Yii::$app->user->identity->id])->asArray()->one();
        $user_roles=$user_roles['role_id'];
        $user_roles=json_decode($user_roles,1);
        $user_roles_data_array=$user_roles['data'];
        //查看该用户,该角色授权下的任务
        $role_str='';
        for($i=0;$i<count($user_roles_data_array);$i++)
        {
            $role_str=$role_str.$user_roles_data_array[$i].',';
        }
        $role_str_final='{'.substr($role_str,0,strlen($role_str)-1).'}';
        $data=$model->find()->where(['task_id'=>$task_id])->andWhere(Yii::$app->user->identity->id ."= ANY (assign_users)  or ('".$role_str_final." '&& assign_roles =true )")->asArray()->all();
        return json_encode($data);
    }
    //得到当前事件
    public function actionAjaxGetById($id)
    {
        $model = new ScheduleTask;
        $data=$model->find()->where(['id'=>$id])->asArray()->one();
        return json_encode($data);
    }
    /**
     * Deletes an existing ScheduleTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScheduleTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduleTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduleTask::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
