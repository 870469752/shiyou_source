<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\ScheduleTaskSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = 'Schedule Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>
			</header>
			<!-- widget div-->
			<div>

    <?= GridView::widget([
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
			['attribute' => 'name', 'format' => 'JSON'],
			['attribute' => 'start_timestamp','format' => 'TimeFormatter', 'headerOptions' => ['data-hide' => 'phone,tablet'],'value'=>function($model){
				$time_mode=json_decode($model->time_mode,1);
				return $time_mode['data']['start'];
			}],
			['attribute' => 'end_timestamp', 'format' => 'TimeFormatter','headerOptions' => ['data-hide' => 'phone,tablet'],'value'=>function($model){
                    $time_mode=json_decode($model->time_mode,1);
				    return $time_mode['data']['end'];
			}],
			['attribute' => 'start_timestamp_event','value'=>function($model){
				$time_mode=json_decode($model->time_mode,1);
				if($time_mode['data']['type']=='day'){
                 return substr($time_mode['data']['data']['start'],11,19);
				}else if($time_mode['data']['type']=='week'){
					$time_start=$time_mode['data']['data']['start'];
					$w_d_s=date('w',strtotime($time_start));
					if($w_d_s==0){
						return "周日".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==1){
						return "周一".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==2){
						return "周二".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==3){
						return "周三".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==4){
						return "周四".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==5){
						return "周五".substr($time_mode['data']['data']['start'],11,19);
					}else if($w_d_s==6){
						return "周六".substr($time_mode['data']['data']['start'],11,19);
					}
				}else{

				}
			}],
			['attribute' => 'end_timestamp_event','value'=>function($model){
				$time_mode=json_decode($model->time_mode,1);
				if($time_mode['data']['type']=='day'){
					return substr($time_mode['data']['data']['end'],11,19);
				}else if($time_mode['data']['type']=='week'){
					$time_start=$time_mode['data']['data']['start'];
					$time_end=$time_mode['data']['data']['end'];
					$w_s=date('W',strtotime($time_start));
					$w_e=date('W',strtotime($time_end));
					$w_d_s=date('w',strtotime($time_start));
					$w_e_s=date('w',strtotime($time_end));
					if($w_e==$w_s){
                        	if($w_e_s==0){
								return "本周日".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==1){
								return "本周一".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==2){
								return "本周二".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==3){
								return "本周三".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==4){
								return "本周四".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==5){
								return "本周五".substr($time_mode['data']['data']['end'],11,19);
							}else if($w_e_s==6){
								return "本周六".substr($time_mode['data']['data']['end'],11,19);
							}
					}else{
						if($w_e_s==0){
							return "下周日".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==1){
							return "下周一".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==2){
							return "下周二".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==3){
							return "下周三".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==4){
							return "下周四".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==5){
							return "下周五".substr($time_mode['data']['data']['end'],11,19);
						}else if($w_e_s==6){
							return "下周六".substr($time_mode['data']['data']['end'],11,19);
						}
					}
				}else{

				}
			}],
            ['attribute' => 'creator', 'format' => 'Uid','headerOptions' => ['data-hide' => 'phone,tablet']],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{view} {delete}','header' => Yii::t('app', 'Action'),'headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>

<?php
// 搜索和创建按钮
$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$search_button}');
options.button.push('{$create_button}');
table_config('datatable', options);						
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);
?>
<!-- 搜索表单和JS --> 
<?php MyFunc::TableSearch($searchModel) ?>

    			</div>
			</div>
		</article>
	</div>
</section>
