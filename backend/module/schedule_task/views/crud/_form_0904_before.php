<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/7/5
 * Time: 14:48
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use common\library\MyHtml;
?>
<style>
	.create{
		display: inline-block;
		margin-bottom: 0;
		font-weight: 400;
		text-align: center;
		vertical-align: middle;
		cursor: pointer;
		background-image: none;
		border: 1px solid transparent;
		white-space: nowrap;
		padding: 6px 12px;
		font-size: 13px;
		line-height: 1.42857143;
		border-radius: 2px;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	.popover-content:last-child {
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
	}
	.popover-content {
		padding: 9px 14px;
	}
	.popover-title {
		margin: 0;
		padding: 8px 14px;
		font-size: 13px;
		font-weight: 400;
		line-height: 18px;
		background-color: #f7f7f7;
		border-bottom: 1px solid #ebebeb;
		border-radius: 2px 2px 0 0;
	}
	.editable-container.popover {
		width: auto;
	}
	.smart-form .col-4 {
		width: 100%;
	}
	.smart-form .state-success {
		width: 10%;
		float: left;
	}
	.smart-form fieldset{
		height: auto;
		display: block;
		padding-top: 10px;
		padding-bottom: 10px;
		border-bottom: 1px dashed rgba(0,0,0,.2);
		background: rgba(255,255,255,.9);
		position: relative;
	}
	table td{width: auto}
	table tr{border:dotted ; border-width:1px 0px 0px 0px;}
	table{border:dotted ; border-width:1px 1px 1px 1px;}

	/* container */
	#myContainer {
		margin: 20px auto;
		border: 2px dashed LightBlue;
		font-size: 10pt;
		display: table;
	}

	/* both tables */
	div#myContainer table {
		/*border-collapse: collapse;*/
		border-collapse: separate;
		border-spacing: 1px;
	}

	/* toolbox table */
	#toolbox {
		background-color: #ccc;
		margin: 7px 7px 15px 7px;
	}

	/* main table */
	#mainTable {
		color: #777;
		background-color: #eee;
		margin: 7px;
	}

	/* table cells */
	div#myContainer td {
		border: 1px solid navy;
		height: 40px;
		width: 70px;
		text-align: center;
		padding: 2px;
	}

	/* button styles */
	.button {
		margin: 5px 10px;
		background-color: #6A93D4;
		color: white;
		border-width: 1px;
		width: 50px;
		padding: 0px;
		font-size: 12px;
	}
	.tron  {background-color: #49daff
	}
	.bg {
		background:grey;
	}
	.context-menu
	{
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		background-color: #f2f2f2;
		border: 1px solid #999;
		list-style-type: none;
		margin: 0;
		padding: 0;
	}
	.context-menu a
	{
		display: block;
		padding: 3px;
		text-decoration: none;
		color: #333;
	}
	.context-menu a:hover
	{
		background-color: #666;
		color: white;
	}
</style>


<section id="widget-grid" class="">
	<div class="row">
		<div id="oneset">
			<div class="col-sm-12 col-md-12 col-lg-3" >
				<div class="jarviswidget jarviswidget-color-blueDark">
					<header>
						<h2>报表设置 </h2>
					</header>
					<div>
						<div class="widget-body" >
							<form id="add-event-form">
								<fieldset>
									<div class="form-group">
										<label>选择任务</label>
										<div class="form-group field-scheduledtask-export_title has-success">
											<?=Html::dropDownList('task', '', $alltask, ['id'=>'taskSelect','class' => 'select2', 'placeholder' => '请选择任务'])?>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
						<div class="well well-sm" id="event-container" >
							<form class="smart-form">
								<fieldset>
									<legend>
										任务类型
									</legend>
									<ul id='external-events' class="list-unstyled" >
										<li >
											<span data-category="day"  class="bg-color-red txt-color-white" data-description="" data-icon="fa-time" >日循环</span>
										</li>
										<li >
											<span data-category="week" class="bg-color-orange txt-color-white" data-description="" data-icon="fa-alert" >周循环</span>
										</li>
										<li >
											<span data-category="custom" class="bg-color-darken txt-color-white" data-description="" data-icon="fa-alert" >自定义</span>
										</li>
									</ul>
								</fieldset>

							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-9">
				<div class="jarviswidget jarviswidget-color-blueDark" data-widget-collapsed="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
						<h2> 事件设置 </h2>
						<div class="widget-toolbar">
							<!-- add: non-hidden - to disable auto hide -->
							<div class="btn-group">
								<button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
									视图选择 <i class="fa fa-caret-down"></i>
								</button>
								<ul class="dropdown-menu js-status-update pull-right">
									<li>
										<a href="javascript:void(0);" id="mt">月视图</a>
									</li>
									<li>
										<a href="javascript:void(0);" id="ag">周视图</a>
									</li>
									<li>
										<a href="javascript:void(0);" id="td">日视图</a>
									</li>
								</ul>
							</div>
						</div>
					</header>
					<div>
						<div class="widget-body no-padding">
							<div class="widget-body-toolbar">
								<div id="calendar-buttons" >
									<div class="btn-group">
										<a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
										<a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-today">today</i></a>
										<a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
							</div>
							<div id="calendar"></div>
							<?php $form = ActiveForm::begin(['options' => ['class' => 'smart-form']]); ?>
							<?= Html::hiddenInput("task_id")?>
							<?= Html::hiddenInput("data")?>
							<?= Html::hiddenInput("assgin_users")?>
							<?= Html::hiddenInput("assgin_roles")?>
							<div style="float:right">
<!--								<button type="button"  style="margin-right:5px" class="create btn-success" id="btn_timeshow">时间段预览</button>-->
								<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit','class' => $model->isNewRecord ? 'create btn-success' : 'create btn-primary']) ?>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="edit_task" title="任务编辑" style="display:none;width: auto">
	<div class="col-md-12">
		<panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
			<div class="panel-body" ng-transclude style="overflow-x: auto; overflow-y: auto; " >
				<div style="margin-bottom: 5px">
					<label style="font-weight:bold">编辑任务名称</label>
					<div>
						<input type="text" id="num" style="font-weight:bold;width:250px">
					</div>
				</div>
				<div style=" margin-bottom: 5px">
					<label id="start"style="font-weight:bold;">任务开始时间</label>
					<div >
						<input type="text" id="time_range_start" class="datetimepicker" style="color:black;width:250px">
					</div>
				</div>
				<div style=" margin-bottom: 5px">
					<label id="end"style="font-weight:bold;">任务结束时间</label>
					<label id="task_time_error_3" style="margin-left:10px;color: red;display: none"></label>
					<div >
						<input type="text" id="time_range_end" class="datetimepicker" style="color:black;width:250px">
					</div>
				</div>
				<div id="cycle_start" style=" margin-bottom: 5px;">
					<label  style="font-weight:bold;">循环开始时间</label>
					<label id="task_time_error_1" style="margin-left:10px;color: red;display: none"></label>
					<div class="input-group"  style="width:250px;">
						<?=Html::dropDownList('start1_week', '', [
							''=>'无',
							'1' => '周一',
							'2' => '周二',
							'3' => '周三',
							'4' => '周四',
							'5' => '周五',
							'6' => '周六',
							'7' => '周日',
						], ['class' => 'select2', 'id'=>'start1_week','placeholder' => '请选择开始日期：'])?>
						<input class="form-control" name="start" id="begin1" type="text" placeholder="Select time" data-autoclose="true">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
				</div>
				<div id="cycle_end" style="margin-bottom: 5px">
					<label style="font-weight:bold;">循环结束时间</label>
					<label id="task_time_error_2" style="margin-left:10px;color: red;display: none"></label>
					<div class="input-group" style="width:250px;">
						<?=Html::dropDownList('end1_week', '', [
							''=>'无',
							'1' => '周一',
							'2' => '周二',
							'3' => '周三',
							'4' => '周四',
							'5' => '周五',
							'6' => '周六',
							'7' => '周日',
						], ['class' => 'select2', 'id'=>'end1_week','placeholder' => '请选择结束日期：'])?>
						<input class="form-control" name="end" id="end1" type="text" placeholder="Select time" data-autoclose="true">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
				</div>
				<div style="margin-bottom: 5px；width:250px;">
					<label id="cycle_start"style="font-weight:bold;">分配用户</label>
					<?=Html::dropDownList('assing_user_d', $now_user_id, $users_array, ['class' => 'select2','multiple'=>'multiple', 'id'=>'assgin_user_d','placeholder' => '请选择用户'])?>
				</div>
				<div style="margin-bottom: 5px；width:250px;">
					<label id="cycle_start"style="font-weight:bold;">分配角色</label>
					<?=Html::dropDownList('assing_user_r', $now_roles, $roles_array, ['class' => 'select2','multiple'=>'multiple', 'id'=>'assgin_role_d','placeholder' => '请选择角色'])?>
				</div>
			</div>
		</panel>
	</div>
</div>
<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/clockpicker/clockpicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/timec.js", ['backend\assets\AppAsset']);
$this->registerJsFile('js/plugin/datatables/jquery.dataTables.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.tableTools.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fullcalendar/jquery.fullcalendar.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile('js/plugin/datatable-responsive/datatables.responsive.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
//cron
$this->registerJsFile("js/cron/jquery-cron-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-cron.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/cron/jquery-gentleSelect-min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/cron/jquery-gentleSelect.css", ['backend\assets\AppAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
?>
<script>
	var data=[];//全局数据，存放日历上的代表事件
	var group=parseInt(<?=$group?>)+1;
	var flag=0;
    var assgin_users='';
	var assgin_roles='';
	var current_event;
	var time_conflict=0;//事件时间设置是否冲突
	var total_time=1;   //任务执行时间设置是否冲突
	var task_ref=<?=json_encode($alltask)?>;
	//周映射
	var week_ref={
		'1':'周一',
		'2':'周二',
		'3':'周三',
		'4':'周四',
		'5':'周五',
		'6':'周六',
		'7':'周日'
	};
	//每个事件的配置div

	window.onload = function()
	{
		"use strict";
		$("#task_edit").css({'background':'#739e73'});
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var dialog = $("#timeshow").dialog({
			autoOpen : false,
			width : 600,
			resizable : false,
			modal : true,
			buttons : [{
				html : "<i class='fa fa-times'></i>&nbsp; 取消",
				"class" : "btn btn-default",
				click : function() {
					//  $("#pre_table_timeshow thead").text("");
					$("#pre_table_timeshow tbody").text("");
					$(this).dialog("close");

				}
			}, {
				html : "<i class='fa fa-plus'></i>&nbsp; 更新",
				"class" : "btn btn-danger",
				click : function() {
					if(true){
					}
					else {
					}
					//     $("#pre_table_timeshow thead").text("");
					$("#pre_table_timeshow tbody").text("");
					$(this).dialog("close");
				}
			}]
		});
		$(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
		$(".datetimepicker").datetimepicker({
			format: 'yyyy-mm-dd',
			todayBtn: 'linked',
			minView: 'month',
			pickerPosition: "bottom-left",
			autoclose: true,
			timepicker: false
		});
		$('#begin1').clockpicker({
			placement: 'left',
			donetext: 'Done',
			'default': 'now'
		});
		$('#end1').clockpicker({
			placement: 'left',
			donetext: 'Done',
			'default': 'now'
		});
	//	$('#timepicker').timepicker();
		var hdr = {
			left: 'title',
			center: 'month,basicWeek,agendaDay',
			right: 'prev,today,next'
		};
		var initDrag = function (e) {
			var eventObject = {
				title: $.trim(e.children().text()), // use the element's text as the event title
				description: $.trim(e.children('span').attr('data-description')),
				category: $.trim(e.children('span').attr('data-category')),
				icon: $.trim(e.children('span').attr('data-icon')),
				className: $.trim(e.children('span').attr('class')) // use the element's children as the event class
			};
			// store the Event Object in the DOM element so we can get to it later
			e.data('eventObject', eventObject);
			// make the event draggable using jQuery UI
			e.draggable({
				zIndex: 999,
				revert: true, // will cause the event to go back to its
				revertDuration: 0 //  original position after the drag
			});
		};

		$('#external-events > li').each(function () {
			var category=$(this).children('span').attr('data-category');
			initDrag($(this));
		});


		$('#calendar').fullCalendar({
			header: hdr, //只有包含在header里面，才能触发翻页按钮
			//翻页按钮
			buttonText: {
				next: '<i class="fa fa-chevron-right"></i>',
				prev:'<i class="fa fa-chevron-left"></i>'
			},
			titleFormat:{
				month: 'MMMM', // September 2013
				week: "", // Sep 7 - 13 2013
				day: '' // Tuesday, Sep 8, 2013
			},
			monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			monthNamesShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
			dayNames: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
			dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
			allDaySlot:false,
			firstDay:1,
			editable: false,
			droppable: true, // this allows things to be dropped onto the calendar !!!
			slotMinutes:15,
			axisFormat:'HH(:mm)',
			firstHour:0,
			//拖拽完成时触发
			drop: function (date, allDay) { // 往日历上面拖拽的时候调用
				var originalEventObject = $(this).data('eventObject');
				var category=$(this).children('span').attr('data-category');
				var copiedEventObject = $.extend({}, originalEventObject);

				copiedEventObject.start = date;
				var o_end=new Date();
				o_end.setFullYear(date.getFullYear());
				o_end.setDate(1);
				o_end.setMonth(date.getMonth());
				o_end.setDate(date.getDate());
				o_end.setHours(date.getHours());
				o_end.setMinutes(date.getMinutes());
				o_end.setSeconds(date.getSeconds());
				copiedEventObject.end=o_end;
				copiedEventObject.allDay = allDay;
				copiedEventObject['group']=(group++);
				copiedEventObject['_id']=copiedEventObject['group'];

				$('#calendar').fullCalendar('addEventSource', [{
					"category":copiedEventObject['category'],
					"title":copiedEventObject['title'],
					"start":date,
					"end":date,
					"description":copiedEventObject['description'],
					"className":copiedEventObject['className'],
					"_id":copiedEventObject['group'],
					"group":copiedEventObject["group"],
					"allDay":copiedEventObject['allDay']
				}]);

				var event={};
				event['type']=copiedEventObject['category'];
				event['start']=$("#time_range_start").val();
				event['end']=$("#time_range_end").val();
				event["id"]=copiedEventObject["group"];
				event["task_id"]=$("#taskSelect").val();
				event['users']='';
				event['roles']='';

				var config={};
				var start=new Date();
				start.setFullYear(copiedEventObject["start"].getFullYear());
				start.setDate(1);
				start.setMonth(copiedEventObject["start"].getMonth());
				start.setDate(copiedEventObject["start"].getDate());
				start.setHours(copiedEventObject["start"].getHours());
				start.setMinutes(copiedEventObject["start"].getMinutes());
				start.setSeconds(0);
				var end=new Date();
				end.setFullYear(start.getFullYear());
				end.setDate(1);
				end.setMonth(start.getMonth());
				end.setDate(start.getDate());
				end.setHours(start.getHours());
				end.setMinutes(start.getMinutes());
				end.setSeconds(start.getSeconds());
				config["start"]=start;
				config["end"]=end;
				config["icon"]=copiedEventObject["icon"];
				config["category"]=copiedEventObject["category"];
				config["_id"]=copiedEventObject["group"];
				config["title"]=copiedEventObject["title"];
				config["className"]=copiedEventObject["className"];
				config["description"]=copiedEventObject["description"];
				config['group']=copiedEventObject['group'];
				config['allDay']=copiedEventObject['allDay'];

				event['config']=config;
				data.push(event);
				console.log('拖拽完成');
				console.log(data);
				console.log(copiedEventObject);
				current_event=copiedEventObject;
				if(current_event['category']=='day'){
					$("#cycle_start").show();
					$("#cycle_end").show();
					$("#s2id_start1_week").hide();
					$("#s2id_end1_week").hide();
					$("#num").val("每日"+task_ref[parseInt($("#taskSelect").val())]+"任务"+copiedEventObject['group']);
				}else if(current_event['category']=='week'){
					$("#cycle_start").show();
					$("#cycle_end").show();
					$("#s2id_start1_week").show();
					$("#s2id_end1_week").show();
					$("#num").val("每周"+task_ref[parseInt($("#taskSelect").val())]+"任务"+copiedEventObject['group']);
				}else{
					time_conflict=1;
					total_time=1;
					$("#cycle_start").hide();
					$("#cycle_end").hide();
					$("#num").val(task_ref[parseInt($("#taskSelect").val())]+"任务"+copiedEventObject['group']);
				}
				$(".datetimepicker").val(new Date().format("yyyy-MM-dd"));
				console.log(typeof new Date().format("yyyy-MM-dd"));
				clearError();
				edit_task_mode.dialog("open");

			},
			//在日程事件改变大小并成功后调用,event就是该条数据
			eventResize:function( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
				var flag=0;
				var group_tem=event.group;
				var category_tem=event.category;
				for(var i in data){
					if(data[i]['config']['group']==event.group && data[i]['config']['category']==event.category){
						flag=i;
						break;
					}
				}
				data[i]['config']['start']=event.start;
				if(event.end=='' || event.end==null){
					data[i]['config']['end']=data[i]['config']['start'];
				}else{
					data[i]['config']['end']=event.end;
				}
				$('#calendar').fullCalendar('clientEvents', function ( cal ){
					if( cal.group==group_tem && cal.category==category_tem){
						$('#calendar').fullCalendar('removeEvents',cal._id);
					}
				})
				var view = $('#calendar').fullCalendar('getView');
				var nowDate=$("#calendar").fullCalendar('getDate');
				drawEventByGroup(category_tem,group_tem,view,nowDate);
				console.log('日期大小改变');
				console.log(data);
				console.log(event);
				current_event=event;
				$("#num").val(current_event["title"]);

			},
			eventResizeStop:function(event, jsEvent, ui, vie){

			},
			//日历事件被拖动完成时触发
			eventDrop:function(event, jsEvent, ui, view){
				var flag=0;
				var group_tem=event.group;
				var category_tem=event.category;
				for(var i in data){
					if(data[i]['config']['group']==event.group && data[i]['config']['category']==event.category){
						flag=i;
					}
					break;
				}
				data[i]['config']['start']=event.start;
				if(event.end=='' || event.end==null){
					data[i]['config']['end']=data[i]['config']['start'];
				}else{
					data[i]['config']['end']=event.end;
				}
				$('#calendar').fullCalendar('clientEvents', function ( cal ){
					if( cal.group==group_tem && cal.category==category_tem){
						$('#calendar').fullCalendar('removeEvents',cal._id);
					}
				})
				var view = $('#calendar').fullCalendar('getView');
				var nowDate=$("#calendar").fullCalendar('getDate');
				drawEventByGroup(category_tem,group_tem,view,nowDate);
				console.log('事件拖动完成');
				console.log(data);
				console.log(event);
				current_event=event;
				$("#num").val(current_event["title"]);
			},

			eventClick: function(event, jsEvent, view) {
				var flag=0;
				for(var i in data){
					if(data[i]['config']['group']==event['group'] && data[i]['config']['category']==event['category']){
						flag=i;
					}
				}
				current_event=event;
				if(current_event['category']=='day'){
					$("#cycle_start").show();
					$("#cycle_end").show();
					$("#s2id_start1_week").hide();
					$("#s2id_end1_week").hide();
					$("#num").val(event['title']);

				}else if(current_event['category']=='week'){
					$("#cycle_start").show();
					$("#cycle_end").show();
					$("#s2id_start1_week").show();
					$("#s2id_end1_week").show();
					$("#num").val(event['title']);


				}else{
					time_conflict=1;
					total_time=1;
					$("#cycle_start").hide();
					$("#cycle_end").hide();
					$("#num").val(event['title']);


				}
				edit_task_mode.dialog("open");
			},

			//日程事件渲染时触发
			eventRender: function (event, element, icon) {
				if (!event.description == "") {
					element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
						"</span>");
				}
				if (!event.icon == "") {
					element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
						" '></i>");
				}
			},

			windowResize: function (event, ui) {
				$('#calendar').fullCalendar('render');
			}
		});
		$('#calendar').fullCalendar('changeView', 'agendaWeek');
		//自定义年报表添加按钮
		$("#_submit").click(function(){
			for(var i in data){
				var s=data[i]['config']['start'];
				var e=data[i]['config']['end'];
				data[i]['config']['start']=$.fullCalendar.formatDate(s, "yyyy-MM-dd HH:mm:ss");
				data[i]['config']['end']=$.fullCalendar.formatDate(e, "yyyy-MM-dd HH:mm:ss");
			}
			console.log(data);
			$("input[name='data']").val(JSON.stringify(data));
		})

		$('#add-event').click(function () {
			var title = $('#title').val(),
				priority = $('input:radio[name=priority]:checked').val(),
				description = $('#description').val(),
				icon = $('input:radio[name=iconselect]:checked').val();
			    addEvent(title, priority, description, icon);
		});

		/* hide default buttons */
		$('.fc-header-right, .fc-header-center').hide();

		$('#calendar-buttons #btn-prev').click(function () {
			$('.fc-button-prev').click();
			var nowDate=$("#calendar").fullCalendar('getDate');
			$('#calendar').fullCalendar('clientEvents', function ( cal ){
					$('#calendar').fullCalendar('removeEvents',cal._id);
			})
			var view = $('#calendar').fullCalendar('getView');
			for(var i in data){
				drawEventByGroup(data[i]['config']['category'],data[i]['config']['group'],view,nowDate);
			}
			return false;
		});

		$('#calendar-buttons #btn-next').click(function () {
			$('.fc-button-next').click();
			var nowDate=$("#calendar").fullCalendar('getDate');
			$('#calendar').fullCalendar('clientEvents', function ( cal ){
				$('#calendar').fullCalendar('removeEvents',cal._id);
			})
			var view = $('#calendar').fullCalendar('getView');
			for(var i in data){
				drawEventByGroup(data[i]['config']['category'],data[i]['config']['group'],view,nowDate);
			}
			return false;
		});

		$('#calendar-buttons #btn-today').click(function () {
			$('.fc-button-today').click();
			var nowDate=$("#calendar").fullCalendar('getDate');
			$('#calendar').fullCalendar('clientEvents', function ( cal ){
				$('#calendar').fullCalendar('removeEvents',cal._id);
			})
			var view = $('#calendar').fullCalendar('getView');
			for(var i in data){
				drawEventByGroup(data[i]['config']['category'],data[i]['config']['group'],view,nowDate);
			}
			return false;
		});

		//视图切换
		$('#mt').click(function () {
			$('#calendar').fullCalendar('changeView', 'month');
		});
		$('#ag').click(function () {
			$('#calendar').fullCalendar('changeView', 'agendaWeek');
		});
		$('#td').click(function () {
			$('#calendar').fullCalendar('changeView', 'agendaDay');
		});
		//任务执行时间判断
		$("#time_range_end").change(function(){
			if($("#time_range_end").val()<$("#time_range_start").val()){
				total_time=0;
				$("#task_edit").css({'background':'grey'});
				$("#task_time_error_3").text("任务结束时间不得大于开始时间！");
				$("#task_time_error_3").show();
				$("#task_edit").attr("disabled", true);
			}else{
				total_time=1;
				$("#task_edit").css({'background':'#739e73'});
				$("#task_time_error_3").text('');
				$("#task_time_error_3").hide();
				$("#task_edit").attr("disabled", false);
			}

		})
		//时间冲突判断
		$("#begin1").change(function(){
			$("#end1").val('');
			if($(this).val()=='' || $(this).val()==null){
				$("#task_edit").css({'background':'grey'});
				$("#task_time_error_1").text("循环开始时间格式错误！");
				$("#task_time_error_1").show();
				time_conflict=0;
				$("#task_edit").attr("disabled", true);
			}else{
				$("#task_edit").css({'background':'#739e73'});
				$("#task_time_error_1").hide();
				$("#task_time_error_1").text('');
				time_conflict=1;
				$("#task_edit").attr("disabled", false);
			}
		});
		$("#end1").change(function(){
			if(current_event['category']=='week'){
				var sta=parseInt($("#start1_week").val());
				var end=parseInt($("#end1_week").val());
				var sta_day=$("#begin1").val();
				if($(this).val()=='' || $(this).val==null){
					$("#task_edit").css({'background':'grey'});
					$("#task_time_error_2").text("循环结束时间格式错误！");
					$("#task_time_error_2").show();
					time_conflict=0;
					$("#task_edit").attr("disabled", true);
				}else if(sta==end-7 && $(this).val()>sta_day){
					$("#task_edit").css({'background':'grey'});
					$("#task_time_error_2").text("循环结束时间格式错误！");
					$("#task_time_error_2").show();
					time_conflict=0;
					$("#task_edit").attr("disabled", true);
				}else{
					$("#task_edit").css({'background':'#739e73'});
					$("#task_time_error_2").hide();
					$("#task_time_error_2").text('');
					time_conflict=1;
					$("#task_edit").attr("disabled", false);
				}
			}else if(current_event['category']=='day'){
				if($(this).val()=='' || $(this).val()==null){
					$("#task_edit").css({'background':'grey'});
					$("#task_time_error_2").text("循环结束时间格式错误！");
					$("#task_time_error_2").show();
					time_conflict=0;
					$("#task_edit").attr("disabled", true);
				}else if($("#end1").val()>$("#begin1").val()){
					$("#task_edit").css({'background':'#739e73'});
					$("#task_time_error_2").hide();
					$("#task_time_error_2").text('');
					time_conflict=1;
					$("#task_edit").attr("disabled", false);
				}else if($("#end1").val()<$("#begin1").val()){
					$("#task_time_error_2").text("(第二天)");
					$("#task_time_error_2").show();
					$("#task_edit").css({'background':'#739e73'});
					time_conflict=1;
					$("#task_edit").attr("disabled", false);
				}else if($("#end1").val()==$("#begin1").val()){
					$("#task_edit").css({'background':'grey'});
					$("#task_time_error_2").text("循环开始时间不能等于结束时间！");
					$("#task_time_error_2").show();
					time_conflict=0;
					$("#task_edit").attr("disabled", true);
				}
			}
		});
		//周循环切换
		$("#start1_week").change(function(){
			$("#begin1").val('');
			$("#end1").val('');
			time_conflict=0;
			var s_w=parseInt($("#start1_week").val());
			$("#end1_week").empty();
			$("#end1_week").append("<option value='null'>无</option>");
			for(var i=s_w;i<=7+s_w;i++){
				if(i>7){
					var $tmp_i=i-7;
					if($tmp_i==1){
						$tmp_i="一";
					}else if($tmp_i==2){
						$tmp_i="二";
					}else if($tmp_i==3){
						$tmp_i="三";
					}else if($tmp_i==4){
						$tmp_i="四";
					}else if($tmp_i==5){
						$tmp_i="五";
					}else if($tmp_i==6){
						$tmp_i="六";
					}else if($tmp_i==7){
						$tmp_i="日";
					}
					$("#end1_week").append("<option value='" + i + "'>下周" + $tmp_i + "</option>");
				}else{
					var $tmp_i=i;
					if($tmp_i==1){
						$tmp_i="一";
					}else if($tmp_i==2){
						$tmp_i="二";
					}else if($tmp_i==3){
						$tmp_i="三";
					}else if($tmp_i==4){
						$tmp_i="四";
					}else if($tmp_i==5){
						$tmp_i="五";
					}else if($tmp_i==6){
						$tmp_i="六";
					}else if($tmp_i==7){
						$tmp_i="日";
					}
					$("#end1_week").append("<option value='" + i + "'>本周" + $tmp_i + "</option>");
				}
			}

		});
		$("#end1_week").change(function(){
			$("#end1").val('');
			time_conflict=0;
		});
        //任务编辑
		var edit_task_mode=$("#edit_task").dialog({
			autoOpen : false,
			width : 500,
			resizable : false,
			modal : true,
			draggable:false,
			buttons : [{
				html : "<i class='fa fa-times'></i>&nbsp; 取消",
				"class" : "btn btn-default",
				click : function() {
					$(this).dialog("close");
				}
			}, {
				id:"task_edit",
				html : "修改",
				"class" : "btn btn-primary",
				click : function() {
					if((current_event['category']=='custom') && $("#begin1").val()=='' || $("#end1").val()==''){
						time_conflict=0;
						$("#task_edit").css({'background':'grey'});
						$("#task_time_error_2").text("请填写时间信息");
						$("#task_time_error_2").show();
					}else{
						time_conflict=1;
						$("#task_edit").css({'background':'#739e73'});
						$("#task_time_error_2").text("");
						$("#task_time_error_2").hide();
					}

					if(time_conflict!=0 && total_time!=0){
						//分配用户
						var assgin_user_arr=$("#assgin_user_d").val();
						var str='';
						for(var i in assgin_user_arr){
							str+=assgin_user_arr[i]+',';
						}
						str=str.substring(0,str.length-1);
						//分配角色
					//	assgin_roles='';
						var assgin_roles_arr=$("#assgin_role_d").val();
						var str1='';
						for(var i in assgin_roles_arr){
							str1+=assgin_roles_arr[i]+',';
						}
						str1=str1.substring(0,str1.length-1);
						var _id=current_event['_id'];
						var _value = $("#num").val();
						var category_tem=current_event['category'];
						var group_tem=current_event['group'];
						var total_start=$("#time_range_start").val();
						var total_end=$("#time_range_end").val();
						var c=current_event['group'];
						//更新data中同一group的数据
						var begin1=$("#begin1").val();
						var end1=$("#end1").val();
						for(var i in data){
							var event=data[i]['config'];
							if(event['group']==group_tem && event['category']==category_tem){
								if(category_tem=='day'){
									var tem=data[i]['config']['start'].getDate();
									if(begin1>end1){
										data[i]['config']['title']=_value;
										data[i]['config']['start'].setHours(parseInt(begin1.substring(0,2)));
										data[i]['config']['start'].setMinutes(parseInt(begin1.substring(3,5)));
										data[i]['config']['end'].setDate(tem+1);
										data[i]['config']['end'].setHours(parseInt(end1.substring(0,2)));
										data[i]['config']['end'].setMinutes(parseInt(end1.substring(3,5)));
										data[i]['start']=total_start;
										data[i]['end']=total_end;
										data[i]['users']=str;
										data[i]['roles']=str1
									}else{
										data[i]['config']['title']=_value;
										data[i]['config']['start'].setHours(parseInt(begin1.substring(0,2)));
										data[i]['config']['start'].setMinutes(parseInt(begin1.substring(3,5)));
										data[i]['config']['end'].setDate(tem);
										data[i]['config']['end'].setHours(parseInt(end1.substring(0,2)));
										data[i]['config']['end'].setMinutes(parseInt(end1.substring(3,5)));
										data[i]['start']=total_start;
										data[i]['end']=total_end;
										data[i]['users']=str;
										data[i]['roles']=str1
									}
								}else if(category_tem=='week'){
									var today_week_start=data[i]['config']['start'].getDay();  //data中存贮的日期
									var today_week_end=data[i]['config']['end'].getDay();
									var start1_week=parseInt($("#start1_week").val());        //用户设定的日期
									var end1_week=parseInt($("#end1_week").val());
									if(today_week_end==0){
										today_week_end=7;
									}
									if(today_week_start==0){
										today_week_start=7;
									}
									var start_tem1=new Date();
									start_tem1.setFullYear(data[i]['config']['start'].getFullYear());
									start_tem1.setDate(1);
									start_tem1.setMonth(data[i]['config']['start'].getMonth());
									if(start1_week<=today_week_start){
										start_tem1.setDate(data[i]['config']['start'].getDate()-(today_week_start-start1_week));
									}else{
										start_tem1.setDate(data[i]['config']['start'].getDate()+(start1_week-today_week_start));
									}

									start_tem1.setHours(parseInt(begin1.substring(0,2)));
									start_tem1.setMinutes(parseInt(begin1.substring(3,5)));
									start_tem1.setSeconds(0);
									var end_tem1=new Date();
									end_tem1.setFullYear(start_tem1.getFullYear());
									end_tem1.setDate(1);
									end_tem1.setMonth(start_tem1.getMonth());
									if(end1_week>=start1_week){
										end_tem1.setDate(start_tem1.getDate()+(end1_week-start1_week));
									}else{
									}
									end_tem1.setHours(parseInt(end1.substring(0,2)));
									end_tem1.setMinutes(parseInt(end1.substring(3,5)));
									end_tem1.setSeconds(59);
									data[i]['config']['title']=_value;
									data[i]['config']['start']=start_tem1;
									data[i]['config']['end']=end_tem1;
									data[i]['start']=total_start;
									data[i]['end']=total_end;
									data[i]['users']=str;
									data[i]['roles']=str1
								}else{
									data[i]['config']['title']=_value;
									data[i]['start']=total_start;
									data[i]['end']=total_end;
									data[i]['users']=str;
									data[i]['roles']=str1
								}

							}
						}
						//删除日历上同组的group事件
						$('#calendar').fullCalendar('clientEvents', function ( cal ){
							if( cal.group==group_tem && cal.category==category_tem){
								$('#calendar').fullCalendar('removeEvents',cal._id);
							}
						})
						//将更新之后的数据显示在日历上面
						var view = $('#calendar').fullCalendar('getView');
						var nowDate=$("#calendar").fullCalendar('getDate');
						drawEventByGroup(category_tem,group_tem,view,nowDate);
						$(this).dialog("close");
						console.log(data);
					}else{
					}
				}
			}, {
				html : "删除",
				"class" : "btn btn-danger",
				click : function() {
					var _id=current_event['_id'];
					var category_tem=current_event['category'];
					var group_tem=current_event['group'];
					$('#calendar').fullCalendar('clientEvents', function ( cal ){
						if( cal.group==group_tem && cal.category==category_tem){
							$('#calendar').fullCalendar('removeEvents',cal._id);
						}
					})
					var flag=0;
					for(var i in data){
						if(data[i]['config']['group']==group_tem && data[i]['config']['category']==category_tem){
							data[i]='##';
						}
					}
					var tem=[];
					for(var i in data){
						if(data[i]!="##"){
							tem.push(data[i]);
						}
					}
					data=tem;
					console.log("删除之后的 data");
					console.log(data);
					$(this).dialog("close");
				}
			}]
		});

		//画出同组的事件
		function drawEventByGroup(category,group,view,nowDate)
		{
			//任务执行时间
			var flag=0;
			for(var i in data){
			if(data[i]['config']['group']==group && data[i]['config']['category']==category){
				flag=i;
			}
		   }
			var during_start=data[flag]['start'];
			var during_end=data[flag]['end'];
			var during_date_start=new Date();
			var during_date_end=new Date();
			during_date_start.setFullYear(parseInt(during_start.substring(0,4)));
			during_date_start.setDate(1);
			during_date_start.setMonth(parseInt(during_start.substring(5,7))-1);
			during_date_start.setDate(parseInt(during_start.substring(8,10)));
			during_date_start.setHours(0);
			during_date_start.setMinutes(0);
			during_date_start.setSeconds(0);
			during_date_end.setFullYear(parseInt(during_end.substring(0,4)));
			during_date_end.setDate(1);
			during_date_end.setMonth(parseInt(during_end.substring(5,7))-1);
			during_date_end.setDate(parseInt(during_end.substring(8,10)));
			during_date_end.setHours(23);
			during_date_end.setMinutes(59);
			during_date_end.setSeconds(59);
			//事件时间区间
			var start_data=data[flag]['config']['start'];
			var end_data=data[flag]['config']['end'];
			//data中存贮的时间
			var hour_s=start_data.getHours();
			var minute_s=start_data.getMinutes();
			var second_s=start_data.getSeconds();
			var hour_e=end_data.getHours();
			var minute_e=end_data.getMinutes();
			var second_e=end_data.getSeconds();
			var start_end=end_data.getDate()-start_data.getDate();
			if(start_end==0){
				start_end=0;
			}else{
				start_end=1;
			}
			if(category=='day'){
				var days=getdays(nowDate.getFullYear(),nowDate.getMonth()+1);
				for(var i=1;i<=days;i++){
					var start_tem=new Date();
					start_tem.setFullYear(nowDate.getFullYear());
					start_tem.setDate(1);
					start_tem.setMonth(nowDate.getMonth());
					start_tem.setDate(i);
					start_tem.setHours(hour_s);
					start_tem.setMinutes(minute_s);
					start_tem.setSeconds(second_s);
					var end_tem=new Date();
					end_tem.setFullYear(start_tem.getFullYear());
					end_tem.setDate(1);
					end_tem.setMonth(start_tem.getMonth());
					end_tem.setDate(i+start_end);
					end_tem.setHours(hour_e);
					end_tem.setMinutes(minute_e);
					end_tem.setSeconds(second_e);
					if(start_tem>=during_date_start && end_tem<=during_date_end){
						$('#calendar').fullCalendar('addEventSource', [{
							"category":data[flag]['config']['category'],
							"title":data[flag]['config']['title'],
							"start":start_tem,
							"end":end_tem,
							"description":data[flag]['config']['description'],
							"className":data[flag]['config']['className'],
							"_id":data[flag]['config']['_id']+'_'+group+'_'+i,
							"group":data[flag]['config']["group"],
							"allDay":data[flag]['config']['allDay']
						}]);
					}
				}
			}else if(category=='week'){
				var days=getdays(nowDate.getFullYear(),nowDate.getMonth()+1);
				var start_weekday=start_data.getDay();
				var end_weekday=end_data.getDay();
				if(start_weekday==0){
					start_weekday=7;
				}
				if(end_weekday==0){
					end_weekday=7;
				}
				var count=0;
			    var start_theweek=theWeek(start_data);
				var end_theweek=theWeek(end_data);
				if(start_theweek==end_theweek){
					count=end_weekday-start_weekday;
				}else{
					count=end_weekday-start_weekday+7*(end_theweek-start_theweek)
				}
				for(var i=1;i<=days;i++){
					var start_tem1=new Date();
					start_tem1.setFullYear(nowDate.getFullYear());
					start_tem1.setDate(1);
					start_tem1.setMonth(nowDate.getMonth());
					start_tem1.setDate(i);
					start_tem1.setHours(hour_s);
					start_tem1.setMinutes(minute_s);
					start_tem1.setSeconds(second_s);
					var end_tem1=new Date();
					end_tem1.setFullYear(start_tem1.getFullYear());
					end_tem1.setDate(1);
					end_tem1.setMonth(start_tem1.getMonth());
					end_tem1.setDate(i+count);
					end_tem1.setHours(hour_e);
					end_tem1.setMinutes(minute_e);
					end_tem1.setSeconds(second_e);
					var w= start_tem1.getDay();
					if(w==0){
						w=7;
					}
					if(w==start_weekday){
						if(start_tem1>=during_date_start && end_tem1<=during_date_end){
							$('#calendar').fullCalendar('addEventSource', [{
								"category":data[flag]['config']['category'],
								"title":data[flag]['config']['title'],
								"start":start_tem1,
								"end":end_tem1,
								"description":data[flag]['config']['description'],
								"className":data[flag]['config']['className'],
								"_id":data[flag]['config']['_id']+'_'+group+'_'+i,
								"group":data[flag]['config']["group"],
								"allDay":data[flag]['config']['allDay']
							}]);
						}
					}
				}
			}else{
				var startdate=new Date();
				startdate.setFullYear(parseInt(data[flag]['start'].substring(0,4)));
				startdate.setDate(1);
				startdate.setMonth(parseInt(data[flag]['start'].substring(5,7))-1);
				startdate.setDate(parseInt(data[flag]['start'].substring(8,10)));
				startdate.setHours(0);startdate.setMinutes(0);startdate.setSeconds(0);
				var enddate=new Date();
				enddate.setFullYear(parseInt(data[flag]['end'].substring(0,4)));
				enddate.setDate(1);
				enddate.setMonth(parseInt(data[flag]['end'].substring(5,7))-1);
				enddate.setDate(parseInt(data[flag]['end'].substring(8,10)));
				enddate.setHours(23);enddate.setMinutes(59);enddate.setSeconds(59);
				data[flag]['config']['start']=startdate;
				data[flag]['config']['end']=enddate;
				$('#calendar').fullCalendar('addEventSource', [{
					"category":data[flag]['config']['category'],
					"title":data[flag]['config']['title'],
					"start":startdate,
					"end":enddate,
					"description":data[flag]['config']['description'],
					"className":data[flag]['config']['className'],
					"_id":data[flag]['config']['_id']+'_'+group,
					"group":data[flag]['config']["group"],
					"allDay":data[flag]['config']['allDay']
				}]);
			}
		}

		//清楚提示错误信息
		function clearError()
		{
			$("#task_edit").css({'background':'#739e73'});
			$("#task_time_error_3").text('');
			$("#task_time_error_3").hide();
			$("#task_time_error_2").text('');
			$("#task_time_error_2").hide();
			$("#task_time_error_1").text('');
			$("#task_time_error_1").hide();
			$("#task_edit").attr("disabled", false);
		}



	}

	//得到每月天数
	function getdays(year,monflag)
	{
		var isflag=false;
		if((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)){
			isflag=true;
		}else{
			isflag=false;
		}
		if(monflag==1 || monflag==3 || monflag==5|| monflag==7|| monflag==8|| monflag==10|| monflag==12 ){
			return 31;
		}else if(monflag==4|| monflag==6|| monflag==9|| monflag==11){
			return 30;
		}else {
			if(isflag){
				return 29;
			}else{
				return 28;
			}
		}
	}
	//这个方法将取得某年(year)第几周(weeks)的星期几(weekDay)的日期
	function getXDate(year,weeks,weekDay){
		//用指定的年构造一个日期对象，并将日期设置成这个年的1月1日
		//因为计算机中的月份是从0开始的,所以有如下的构造方法
		var date = new Date(year,"0","1")
		//取得这个日期对象 date 的长整形时间 time
		var time = date.getTime();
		//将这个长整形时间加上第N周的时间偏移
		//因为第一周就是当前周,所以有:weeks-1,以此类推
		//7*24*3600000 是一星期的时间毫秒数,(JS中的日期精确到毫秒)
		time+=(weeks-1)*7*24*3600000;
		//为日期对象 date 重新设置成时间 time
		date.setTime(time);
		return getNextDate(date,weekDay);
	}
	//这个方法将取得 某日期(nowDate) 所在周的星期几(weekDay)的日期
	function getNextDate(nowDate,weekDay){
		//0是星期日,1是星期一,...
		weekDay%=7;
		var day = nowDate.getDay();
		var time = nowDate.getTime();
		var sub = weekDay-day;
		time+=sub*24*3600000;
		nowDate.setTime(time);
		return nowDate;
	}

	//js 获取某年某月有几周，以及每周的周一和周末是几号到几号
	function getInfo(year,month,count) {
		console.log(count);
		var arr=[];
		var d = new Date();
		d.setFullYear(year, month-1, 1);
		var w1 = d.getDay();
		if (w1 == 0) w1 = 7;
		d.setFullYear(year, month, 0);
		var dd = d.getDate();
		var d1;
		// first Monday
		if (w1 != 1) d1 = 7 - w1 + 2;
		else d1 = 1;
		var week_count = Math.ceil((dd-d1+1)/7);
		console.log(week_count);
		for (var i = 0; i < week_count; i++) {
			if(i==count-1){
				var monday = d1+i*7;
				var sunday = monday + 6;
				var from = year+"-"+month+"-"+monday;
				var to;
				if (sunday <= dd) {
					to = year+"-"+month+"-"+sunday;
				} else {
					d.setFullYear(year, month-1, sunday);
					to = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
				}
				arr.push(from);
				arr.push(to);
			}
		}
		return arr;
	}

	//获取当前日期在当前年第几周函数封装，例如2013-08-15 是当前年的第32周
	function theWeek(now) {
		var totalDays = 0;
		years = now.getFullYear()
		if (years < 1000)
			years += 1900
		var days = new Array(12);
		days[0] = 31;
		days[2] = 31;
		days[3] = 30;
		days[4] = 31;
		days[5] = 30;
		days[6] = 31;
		days[7] = 31;
		days[8] = 30;
		days[9] = 31;
		days[10] = 30;
		days[11] = 31;

		//判断是否为闰年，针对2月的天数进行计算
		if (Math.round(now.getFullYear() / 4) == now.getFullYear() / 4) {
			days[1] = 29
		} else {
			days[1] = 28
		}

		if (now.getMonth() == 0) {
			totalDays = totalDays + now.getDate();
		} else {
			var curMonth = now.getMonth();
			for (var count = 1; count <= curMonth; count++) {
				totalDays = totalDays + days[count - 1];
			}
			totalDays = totalDays + now.getDate();
		}
		//得到第几周
		var week = Math.round(totalDays / 7);
		console.log('##################');
		console.log(now);
		console.log(week);
		return week;
	}
</script>
