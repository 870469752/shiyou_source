<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/9/1
 * Time: 16:46
 */
?>
<div class="select2-container select2-container-multi select2 select2-dropdown-open" id="s2id_assgin_user_d" style="">
    <ul class="select2-choices">
        <li class="select2-search-choice">
            <div>电力一般用户</div>
            <a href="#" class="select2-search-choice-close" tabindex="-1"></a>
        </li>
        <li class="select2-search-choice">
            <div>e_admin</div>
            <a href="#" class="select2-search-choice-close" tabindex="-1"></a>
        </li><li class="select2-search-field">
            <label for="s2id_autogen4" class="select2-offscreen"></label>
            <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" id="s2id_autogen4" placeholder="" aria-activedescendant="select2-result-label-310" style="width: 199px;">
        </li>
    </ul>
</div>

<select id="assgin_role_d" class="select2 select2-offscreen" name="assing_user_r[]" multiple="multiple" size="4" placeholder="请选择角色" tabindex="-1">
    <option value="13">技术用户</option>
    <option value="14">管理用户</option>
    <option value="15" selected="">一般用户</option>
    <option value="19" selected="">系统管理员</option>
</select>

<div class="select2-drop select2-drop-multi select2-display-none select2-drop-active" style="left: 744px; width: 416px; top: 467.5px; bottom: auto; display: block;" id="select2-drop">
    <ul class="select2-results">
        <li class="select2-results-dept-0 select2-result select2-result-selectable" role="presentation"><div class="select2-result-label" id="select2-result-label-51" role="option">
                <span class="select2-match">
                </span>
                admin
            </div>
        </li>
        <li class="select2-results-dept-0 select2-result select2-result-selectable" role="presentation">
            <div class="select2-result-label" id="select2-result-label-52" role="option">
                <span class="select2-match">
                </span>
                jishu
            </div>
        </li>
        <li class="select2-results-dept-0 select2-result select2-result-selectable" role="presentation">
            <div class="select2-result-label" id="select2-result-label-53" role="option">
                <span class="select2-match">
                </span>
                putong
            </div>
        </li>
    </ul>
</div>