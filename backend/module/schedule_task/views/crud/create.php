<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ScheduleTask $model
 */
$this->title = 'Create Schedule Task';
?>
    <?= $this->render('_form', [
        'model' => $model,
        'alltask'=>$alltask,
        'group'=>$group,
        'users_array'=>$users_array,
        'roles_array'=>$roles_array,
        'now_roles'=>$now_roles,
        'now_user_id'=>$now_user_id
    ]) ?>
