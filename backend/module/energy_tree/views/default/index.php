<?php

use common\library\MyHtml;
use yii\helpers\Url;
use common\library\MyActiveForm;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

$this->title = '能耗概览与趋势';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>

                    <h2><?= MyHtml::encode($this->title) ?></h2>
                </header>

                <!-- widget content -->
                <div class="widget-body">
                    <div class="widget-body-toolbar bg-color-white">
                        <?php $form = MyActiveForm::begin(['method' => 'get']); ?>
                        <?= $form->field($model, 'report_time_type')->dropDownList([
                            'hour' => '日报表',
                            'day' => '月报表',
                            'month' => '年报表',
                        ], ['class' => 'select2']) ?>
                        <?= $form->field($model, 'date')->textInput() ?>
                        <?= $form->field($model, 'energy_id')->dropDownList($tree, ['class' => 'select2', 'tree' => true]) ?>

                        <?= MyHtml::submitButton('完成', ['class' => 'btn btn-success']) ?>
                        <?php MyActiveForm::end(); ?>
                    </div>
                    <div class="row">

                    <div id="pie" class="col-xs-6"></div>
                    <div id="column" class="col-xs-6"></div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<?php
$this->registerJsFile('js/highcharts/highcharts.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/drilldown.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('js/highcharts/modules/exporting.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css");
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    window.onload = function () {
        highcharts_lang_date();
        var date_format = 'yyyy-mm-dd';
        var date_type = 'month'
        var input_date = $('[name="<?= $model->formName() ?>[date]"]');
        var input_group = $('[name="<?= $model->formName() ?>[report_time_type]"]');
        time_type(input_group.val());
        changeDate();
        input_group.change(function(){
            // 当选择的报表不同时，时间插件的选择视图也不同
            time_type(this.value);
            changeDate();
        })

        function time_type(value){
            switch(value){
                case 'hour':
                    date_format = 'yyyy-mm-dd';
                    date_type = 'month'
                    break;
                case 'day':
                    date_format = 'yyyy-mm';
                    date_type = 'year'
                    break;
                case 'month':
                    date_format = 'yyyy';
                    date_type = 'decade'
                    break;
            }
        }

        function changeDate(){
            input_date.datetimepicker('remove');
            input_date.datetimepicker({
                format: date_format,
                startView: date_type,
                minView: date_type,
                startDate: '<?= $model->getMaxMin('min') ?>',
                endDate: '<?= $model->getMaxMin('max') ?>',
                autoclose: true
            });
            // 如果选择日期长度比格式化后长，那么就用选择的日期格式化。如果要短就用数据库内最大时间格式化。
            input_date.val(
                input_date.val().length >= date_format.length?
                    input_date.val().substr(0, date_format.length):
                    '<?= $model->getMaxMin('max') ?>'.substr(0, date_format.length)
            );
        }

        chartColumn('column', <?= $column_json ?>, 'KWH')
        // 饼图
        chartPie('pie', <?= $pie_json ?>, 'KWH');



    }
</script>