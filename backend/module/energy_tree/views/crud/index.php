<?php
use common\library\MyHtml;
use Yii\web\View;
use common\library\MyFunc;
use yii\helpers\Url;

$this->title = Yii::t('category', '能源分类分项');
?>
    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
                <div class="jarviswidget jarviswidget-color-blueDark"
                     data-widget-deletebutton="false"
                     data-widget-editbutton="false"
                     data-widget-colorbutton="false"
                     data-widget-sortable="false">
                    <header>
                        <span class="widget-icon">
                            <i class="fa fa-table"></i>
                        </span>

                        <h2><?= MyHtml::encode($this->title) ?></h2>
                    </header>

                    <!-- widget content -->
                    <div class="widget-body">

                        <div id="nestable-menu">
                            <!--<button type="button" class="btn btn-default"
                                    data-action="expand-all">
                                <?/*= Yii::t('category', 'Expand All') */?>
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-action="collapse-all">
                                <?/*= Yii::t('category', 'Collapse All') */?>
                            </button>-->
                            <button type="button" class="btn btn-default"
                                    data-action="add">
                                <?= Yii::t('category', '添加大类') ?>
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-action="default">
                                <?= Yii::t('category', 'Restore Modify') ?>
                            </button>
                            <button type="button" class="btn btn-default" data-action="save">
                                <i class="fa fa-cog fa-fw fa-lg"></i><?= Yii::t('category', '保存') ?>
                            </button>
                        </div>
                        <div class="dd" id="nestable">
                            <?php echo MyHtml::EnergyTreeRecursion($tree, [
                                'add_point' => true,
                            ]); ?>
                        </div>


                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget -->
            </article>
        </div>
    </section>
    <div class="modal fade" id="pointSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">点位批量关联</h4>
                </div>
                <div class="modal-body">
                    <div class="well" id="condition">
                        <?= MyHtml::dropDownList('point_id', null, $point,
                            ['class' => 'select2', 'multiple' => true, 'placeholder' => '请选择相应点位']) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button id="add_point" type="button" class="btn btn-primary" data-dismiss="modal">
                        提交
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
        window.onload = function () {
            $('.dd').nestable('collapseAll');
            var choice_li;
            var GenerateComputePoint = <?=YII::$app->request->get('GenerateState',0)?>;
            if(GenerateComputePoint > 0){
                
            }

            // 子分类标签模板
            var li = '\
        <li class="dd-item dd3-item" data-id="">\
            <div class="dd-handle dd3-handle">Drag</div>\
		    <div class="dd3-content">\
		        <span class="name">请单击修改名称</span>\
		        <em class="close pull-right " style="padding-left:10px;">×</em>\
                <a href="javascript:void(0)" style="padding-left:10px;" class="pull-right txt-color-blueDark add" rel="tooltip" title="" data-placement="top" data-original-title="添加子流向">\
                    <i class="fa fa-sitemap fa-fw"></i>\
		        </a>\
                <sapn class="pull-right point"><i data-toggle="modal" data-target="#pointSelectModal" class="fa fa-fw fa-dot-circle-o cursor-pointer"></i></sapn>\
		    </div>\
		</li>';

            /**
             * 分类中的图标事件
             */
            $('#nestable').on('click', '.close',function (e) {
                if (confirm("是否删除") == true)
                    $(this).parent().parent().remove();
            }).on('click', '.add',function (e) { // 添加子分类
                var ol = $('<ol>', {"class": "dd-list"});
                var ol_object = $(this).parent().parent().find('ol').first();
                if (ol_object.length <= 0) {
                    $(this).parent().parent().append(ol);
                    ol_object = ol;
                }
                ol_object.append(li);
            }).on('click', '.name',function (e) { // 点击名称弹出文本框
                var input = $('<input>', {"class": "text-name", "height": "20px", "type": "text", "value": $(this).text()});
                $(this).html(input);
                input.focus();
            }).on('blur', '.text-name',function (e) { // 失去焦点保存文本
                edit_text($(this));
            }).on('keydown', '.text-name', function (e) { // 按回车保存文本
                if (e.keyCode == 13) {
                    edit_text($(this));
                }
            }).on('click', '.point', function(e){ // 保留已选择点位
                choice_li = $(this).parent().parent();
                var point_id = choice_li.attr('data-point_id');// 获取JSON点位字符串
                // 解决数据库和JSON不兼容
                point_id = point_id?JSON.parse(point_id.replace('{','[').replace('}',']')):'';
                $('[name="point_id[]"]').val(point_id).trigger("change");// 读取关联的点位
            });

            $('#add_point').click(function(){
                var data_point_id = $('[name="point_id[]"]').val();
                choice_li.attr('data-point_id', (data_point_id?'{'+data_point_id+'}':''));
            })

            /**
             * 编辑分类
             */
            function edit_text(object) {
                var name_value = object.val() ? object.val() : '请单击修改名称'; // 如果没有就显示默认值
                var get_name = object.parent().parent().parent().attr('data-title');// 获取JSON名称
                var json_name = get_name?JSON.parse(get_name):{"data_type": "description", "data": {"zh-cn": "", "en-us": ""}};

                json_name.data['<?= Yii::$app->language ?>'] = name_value; // 当前什么语言就加到哪
                object.parent().parent().parent().attr('data-title', JSON.stringify(json_name));
                object.parent().html(name_value);
            }

            /**
             * 最顶上的菜单按钮
             */
            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target), action = target.data('action');
                if (action === 'expand-all') { // 展开所有
                    $('.dd').nestable('expandAll');
                }
                else if (action === 'collapse-all') { // 合并所有
                    $('.dd').nestable('collapseAll');
                }
                else if (action === 'add') { // 添加分类
                    var ol = $('<ol>', {"class": "dd-list"});
                    var ol_object = $("#nestable ol").first();
                    if (ol_object.length <= 0) {
                        $("#nestable").append(ol);
                        ol_object = ol;
                    }
                    ol_object.append(li);
                }
                else if (action === 'default') { // 恢复默认
                    location.reload();
                }
                else if (action === 'save') { // 保存数据
                    $.post("<?= Url::toRoute('/energy-tree/crud/update') ?>", {
                            "data": $('#nestable').nestable('serialize')
                        },
                        function (data) {
                            location.href = data;
                        });
                }
            });
        }
    </script>
<?php
$this->registerJs("$('#nestable').nestable();", View::POS_READY);
$this->registerJsFile('/js/plugin/jquery-nestable/jquery.nestable.min.js', [
    'depends' => 'yii\web\JqueryAsset'
]);
?>