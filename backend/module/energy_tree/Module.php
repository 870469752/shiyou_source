<?php

namespace backend\module\energy_tree;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\energy_tree\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
