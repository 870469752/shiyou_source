<?php

namespace backend\module\energy_tree\controllers;

use backend\models\EnergyTree;
use common\library\MyFunc;
use backend\models\form\EnergyTreeForm;
use Yii;
use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        $model = new EnergyTreeForm();
        $model->load(Yii::$app->request->get());
        // 转换数据为HIGHTCHARTS的JSON格式
        foreach($model->total as $k => $row){
            $pie[0]['data'][$k][] = MyFunc::DisposeJSON($row['name']);
            $pie[0]['data'][$k][] = isset($row['pointData'][0]['total'])?intval($row['pointData'][0]['total']):0;
        }
        $pie_json = isset($pie)?json_encode($pie, JSON_UNESCAPED_UNICODE):'[]';

        foreach($model->data as $k => $row){
            $point_name[$row['id']] = MyFunc::DisposeJSON($row['name']);
            $column[$row['id']] = $row['pointData'];
        }
        $column_json = isset($column)?MyFunc::HighChartsData($column, $point_name):'[]';

        $tree = MyFunc::TreeData(
            EnergyTree::find()
                ->select(['id', 'name', 'parent_id'])
                ->asArray()->indexBy('id')->all()
        );
        return $this->render('index',[
            'model' => $model,
            'tree' => $tree,
            'pie_json' => $pie_json,
            'column_json' => $column_json
        ]);
    }
}
