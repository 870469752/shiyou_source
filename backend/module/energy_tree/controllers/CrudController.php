<?php

namespace backend\module\energy_tree\controllers;

use backend\models\CalculatePoint;
use backend\models\EnergyTree;
use backend\models\Point;
use Yii;
use backend\controllers\MyController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use yii\helpers\Url;

/**
 * Class CrudController
 * @package backend\module\category\controllers
 */
class CrudController extends MyController {
    /**
     * @var array 页面上存在的ID
     */
    public $ids = [0];

	public function behaviors() {
		return [
				'verbs' => [
						'class' => VerbFilter::className (),
						'actions' => [
								'delete' => [
										'post'
								]
						]
				]
		];
	}

	/**
	 * Lists all Category models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
        $tree = MyFunc::TreeData(
            EnergyTree::find()
            ->select(['id','title'=>'name','parent_id', 'relation_point'])
            ->OrderBy('id desc')
            ->asArray()->all()
        );
        $point = ArrayHelper::map(Point::baseSelect()->asArray()->all(), 'id', 'name');
		return $this->render ( 'index', [
				'tree' => $tree,
                'point' => $point
		] );
	}

	/**
	 * 更新分类树
	 * @return mixed
	 */
	public function actionUpdate() {
        if($data = Yii::$app->request->post('data')){
            $this->recursion($data, 0); // 递归添加或更新
        }
        EnergyTree::deleteAll(['not in', 'id', $this->ids]); // 删除页面上没有的ID


        // 检索所有记录，并按层级倒序排列（自动生成计算点位部分）
        $energy_tree = EnergyTree::find()
            ->orderBy(['array_length(parent_serial, 1)' => SORT_DESC])->asArray()->all();
        foreach($energy_tree as $v){
            // 如果生成点位的字段为空就新建点位，否则修改
            $model = is_null($v['generate_point'])?new CalculatePoint():CalculatePoint::findOne($v['generate_point']);
            // 如果用户有关联点位就把所有关联点位加起来，如果没有就把下级孩子加起来，作为计算点位。
            if(is_null($v['relation_point'])){
                $formula = @EnergyTree::ChildrenSumFormula($v['id'])->asArray()->one()['formula'];
            }else{
                $formula = str_replace(',', ' + p', $v['relation_point']);
                $formula = 'sum(p'.substr($formula,1 ,strlen($formula)-2).')';
            }
            // 如果没有绑定点位或者生成的点位被删除，那么就不添加不关联
            if(isset($model) && isset($formula) && $formula != 'sum(p)'){
                $model->name = $v['name'];
                $model->formula = $formula;
                $model->interval = 3600;
                $model->save();
                // 添加好计算点位后，把ID放到对应类中
                if($model->primaryKey){
                    $energy_model = $this->findModel($v['id']);
                    $energy_model->generate_point = $model->primaryKey;
                    $energy_model->save();
                }
            }
        }

        // 刷新
        return Url::toRoute('/'.$this->module->id.'/'.$this->id);
	}

    /**
     * 存储树状数组到数据库
     * @param $tree
     * @param $pid
     */
    protected function recursion($tree, $pid){
        foreach($tree as $row){
            if(isset($row['title'])){
                $model = $row['id']?$this->findModel($row['id']):new EnergyTree(); // 如果没有ID就是添加
                $model->parent_id = $pid;
                $model->name = json_encode($row['title'], JSON_UNESCAPED_UNICODE);
                $model->relation_point = empty($row['point_id'])?null:$row['point_id'];

                if($model->save() && $row['id'] == ''){ // 保存完并且把主键ID拿过来，防止被认为页面上没有而删掉
                    $row['id'] = $model->primaryKey;
                }
                unset($model);
                $this->ids[] = $row['id']; // 把要保留的ID加进去

                if (!empty($row['children'])) {
                    $this->recursion( $row['children'], $row['id']);
                }
            }
        }
        return ;
    }

    public function actionGenerateComputePoint()
    {
        return Url::toRoute('/category/crud');
    }

	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return EnergyTree the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = EnergyTree::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}
