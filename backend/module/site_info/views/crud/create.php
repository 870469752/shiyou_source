<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\InfoGroup $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Info Group',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
