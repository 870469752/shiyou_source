<?php

use common\library\MyHtml;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\InfoGroup $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= MyHtml::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                            <?= $form->field($model, 'name')->textInput() ?>
						</fieldset>
						<div class="form-actions">
                            <?= MyHtml::button('添加字段', ['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'add_field']) ?>
							<?= MyHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>
<script>
    window.onload = function(){
        var i = 0;
        var field_value = JSON.parse('<?= empty($model->field)?'[]':$model->field ?>');
        var field_set_html =
        '<div class="form-group">'+
            ' <legend>字段设置</legend>'+
            '<label class="control-label">字段名称</label>'+
            '<?= MyHtml::textInput('name[]', null, ['class' => 'form-control name']) ?>'+
           '<div class="help-block"></div>'+
           '<label class="control-label">输入形式</label>'+
            '<?= str_replace("\n", '', MyHtml::dropDownList('input_type[]', null, ['text' => '文本框'], ['class' => 'form-control input_type'])) ?>'+
            '<div class="help-block"></div>'+
        '</div>';
        // 修改时载入数据库内容
        for(index in field_value.data){
            add_field();
            $('.name').eq(i).val(field_value.data[index].name);
            $('.input_type').eq(i).val(field_value.data[index].input_type);
            i ++;
        }
        // 点击时
        $('#add_field').click(function(){i ++;add_field();});
        function add_field(){
            $('fieldset').append(field_set_html);
            $('.name').eq(i).attr('name', 'data['+ i +'][name]');
            $('.input_type').eq(i).attr('name', 'data['+ i +'][input_type]');
        }
        add_field(); // 默认载入时添加字段
    }

</script>