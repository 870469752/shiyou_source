<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\InfoGroup $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Info Group',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
