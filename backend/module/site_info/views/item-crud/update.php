<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\InfoItem $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Info Item',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'info_group' => $info_group
    ]) ?>
