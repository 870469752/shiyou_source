<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use yii\helpers\Url;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\InfoItemSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
$get = Yii::$app->request->get();
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
                <header>
                    <span class="widget-icon">
                        <i class="fa fa-table"></i>
                    </span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                <div class="widget-body no-padding">
                    <table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
                        <thead><tr>
                        <?php foreach($header as $v): ?>
                            <th><?= $v['name'] ?></th>
                        <?php endforeach ?>
                            <th>&nbsp;</th>
                        </tr></thead>
                        <tbody>
                        <?php foreach($result as $row): ?>
                            <?php $data = json_decode($row['data'], true) ?>
                            <tr>
                                <?php foreach($header as $k): ?>
                                    <td><?= @$data[$k['name']] ?></td>
                                <?php endforeach ?>
                                <td>
                                    <span class="responsiveExpander"></span>
                                    <a href="<?= Url::toRoute(['update', 'id' => $row['id']] + $get)?>" title="更新" data-pjax="0">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="<?= Url::toRoute(['delete', 'id' => $row['id']] + $get)?>" title="删除" data-confirm="您确定要删除此项吗？" data-method="post"data-pjax="0">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>


<?php
// 创建按钮
$create_button = Html::a(Yii::t('app', 'Create'), ['create'] + $get, ['class' => 'btn btn-default']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$create_button}');
table_config('datatable', options);						
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);
?>

    			</div>
                </div>
			</div>
		</article>
	</div>
</section>
