<?php

use common\library\MyHtml;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\InfoItem $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= MyHtml::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
                            <?php
                                $field = json_decode($info_group['field'], true)['data'];
                                $data = isset($model->data)?json_decode($model->data, true):[];
                            ?>
                            <?php foreach($field as $v): ?>
                            <div class="form-group">
                                <label class="control-label"><?= $v['name'] ?></label>
                                <?= MyHtml::textInput(urlencode($v['name']), @$data[$v['name']], ['class' => 'form-control'])// name编码，防止空格出现 ?>
                                <div class="help-block"></div>
                            </div>
                            <?php endforeach ?>

						<?= $form->field($model, 'info_group_id', ['template' => '{input}'])->hiddenInput(['value' => Yii::$app->request->get('info_group_id')]) ?>

						</fieldset>
						<div class="form-actions">
							<?= MyHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>