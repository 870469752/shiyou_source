<?php

namespace backend\module\site_info;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\site_info\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
