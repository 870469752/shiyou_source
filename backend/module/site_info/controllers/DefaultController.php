<?php

namespace backend\module\site_info\controllers;

use backend\controllers\MyController;
use backend\models\InfoGroup;
use backend\models\InfoItem;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        $info = InfoGroup::find()->with('item')->asArray()->all();
        return $this->render('index', ['info' => $info]);
    }
}
