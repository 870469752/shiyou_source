<?php

namespace backend\module\site_info\controllers;

use backend\models\InfoGroup;
use Yii;
use backend\models\InfoItem;
use backend\models\search\InfoItemSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemCrudController implements the CRUD actions for InfoItem model.
 */
class ItemCrudController extends MyController
{
    public $info_group;

    public function init()
    {
        parent::init();
        $this->info_group = InfoGroup::find()->filterWhere(['id' => Yii::$app->request->get('info_group_id')])->asArray()->one();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $info_group_id = Yii::$app->request->get('info_group_id');
        $field = json_decode($this->info_group['field'], true)['data'];
        $result = InfoItem::find()->filterWhere(['info_group_id' => $info_group_id])->asArray()->all();

        return $this->render('index', [
            'title' => $this->info_group['name'],
            'header' => $field,
            'result' => $result
        ]);
    }

    /**
     * Creates a new InfoItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InfoItem;
        if ($post = Yii::$app->request->post()) {
            $model->info_group_id = $post[$model->formName()]['info_group_id'];
            unset($post['_csrf']);unset($post[$model->formName()]);
            // 字段解码，防止空格出现
            foreach($post as $k => $v){
                unset($post[$k]);
                $post[urldecode($k)] = $v;
            }
            $model->data = $post?json_encode($post, JSON_UNESCAPED_UNICODE):null;

            $model->save();
            return $this->redirect(['index']+Yii::$app->request->get());
        } else {
            return $this->render('create', [
                'model' => $model,
                'info_group' => $this->info_group
            ]);
        }
    }

    /**
     * Updates an existing InfoItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        unset($post['_csrf']);unset($post[$model->formName()]);
        // 字段解码，防止空格出现
        foreach($post as $k => $v){
            unset($post[$k]);
            $post[urldecode($k)] = $v;
        }
        $data = $post?['data' => json_encode($post, JSON_UNESCAPED_UNICODE)]:null;
        if ($model->load($data, '') && $model->save()) {
            $get = Yii::$app->request->get();
            unset($get['id']);
            return $this->redirect(['index'] + $get);
        } else {
            return $this->render('update', [
                'model' => $model,
                'info_group' => $this->info_group
            ]);
        }
    }

    /**
     * Deletes an existing InfoItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $get = Yii::$app->request->get();
        unset($get['id']);
        return $this->redirect(['index'] + $get);
    }

    /**
     * Finds the InfoItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfoItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfoItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
