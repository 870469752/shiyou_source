<?php

namespace backend\module\site_info\controllers;

use Yii;
use backend\models\InfoGroup;
use backend\models\search\InfoGroupSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrudController implements the CRUD actions for InfoGroup model.
 */
class CrudController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InfoGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfoGroupSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single InfoGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionShow(){
        $this->redirect('site-info/default');
    }
    /**
     * Creates a new InfoGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InfoGroup;

        if ($post = Yii::$app->request->post()) {
            // 删除空的字段
            foreach($post['data'] as $k => $v){
                if(empty($v['name'])){
                    array_splice($post['data'], $k, 1);
                }
            }
            $post[$model->formName()]['field'] = json_encode(
                [
                    'data_type' => 'field',
                    'data' => $post['data']
                ],
                JSON_UNESCAPED_UNICODE
            );
            $model->load($post);
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InfoGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($post = Yii::$app->request->post()) {
            // 删除空的字段
            foreach($post['data'] as $k => $v){
                if(empty($v['name'])){
                    array_splice($post['data'], $k, 1);
                }
            }
            $post[$model->formName()]['field'] = json_encode(
                [
                    'data_type' => 'field',
                    'data' => $post['data']
                ],
                JSON_UNESCAPED_UNICODE
            );
            $model->load($post);
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InfoGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InfoGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InfoGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InfoGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
