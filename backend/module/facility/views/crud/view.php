<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\AlarmEvent;
?>
<?php
$alarm=MyFunc::SelectDataAddArrayTop(MyFunc::_map(AlarmEvent::getAvailableEvent(), 'id', 'name'));
$num=[
    '0' => 'point1',
    '1' => 'point2',
    '2' => 'point3',
    '3' => 'point4',
    '4' => 'point5',
    '5' => 'point6',
    '6' => 'point7',
    '7' => 'point8'
];
//echo '<pre>';
//print_r($model->data);
//die;
?>
<style type="text/css">
    /* CSS for the traditional context menu */
    #contextMenu {
        z-index: 300;
        position: absolute;
        left: 5px;
        border: 1px solid #444;
        background-color: #F5F5F5;
        display: none;
        box-shadow: 0 0 10px rgba( 0, 0, 0, .4 );
        font-size: 12px;
        font-family: sans-serif;
        font-weight:bold;
    }
    #contextMenu ul {
        list-style: none;
        top: 0;
        left: 0;
        margin: 0;
        padding: 0;
    }
    #contextMenu li {
        position: relative;
        min-width: 60px;
    }
    #contextMenu a {
        color: #444;
        display: inline-block;
        padding: 6px;
        text-decoration: none;
    }

    #contextMenu li:hover { background: #444; }

    #contextMenu li:hover a { color: #EEE; }
</style>


<section id="widget-grid" class="">

        <!--    右键菜单-->
    <div id="contextMenu">
        <ul>
            <li><a href="#" id="menu1" onclick="dialog()">原子事件</a></li>
            <li><a href="#" id="menu5" onclick="SetAttribute()">属性</a></li>
            <li><a href="#" id="menu3" onclick="change()">改变颜色</a></li>
            <li><a href="#" id="menu4" onclick=" ">删除</a></li>
        </ul>
    </div>
        <!--弹出框-->
    <div id="dialog_simple"   title="Dialog Simple Title" style="display: none">

        <?= HTML::dropDownList($name='1', $selection='0', $items = $alarm, $options = ['id'=>'Event','class'=>"select2"]) ;?>
        <?= HTML::dropDownList($name='1', $selection='0', $items = $num , $options = ['id'=>'selectme','class'=>"select2"]) ;?>
        <!--time-->
        <div id = '_cron' class = 'time_mode' style="height: 160px">
            <lable><?=Yii::t('app', 'Backup Date')?> :</lable>
            <span id='selector'></span>
        </div>
        <!--timeend-->
    </div>
<!--    end 弹出框-->
    <div id="sample">
        <div style="width:100%; white-space:nowrap;">

            <div id="tabs" >
                <ul>
                    <li>
                        <a href="#tabs-1">图1</a>
                    </li>
                    <li>
                        <a href="#tabs-2">图2</a>
                    </li>
                    <li>
                        <a href="#tabs-3">线</a>
                    </li>
                </ul>

                <div id="tabs-1">
                     <span style="vertical-align: top; padding: 5px; highth:100px">
                          <div id="myPalette1" style="height:90px;"></div>
                        </span>
                </div>

                <div id="tabs-2">
                     <span style="vertical-align: top; padding: 5px; width:100px">
                          <div id="myPalette2" style="border: solid 1px gray;height: 110px;overflow:scroll;"></div>
                        </span>
                </div >

                <div id="tabs-3">
                     <span style="vertical-align: top; padding: 5px; width:100px">
                          <div id="myPalette3" style="border: solid 1px gray;height: 110px;overflow:scroll;"></div>
                        </span>
                </div>
            </div>

    <span style="vertical-align: top; padding: 5px; width:80%">
      <div id="myDiagram" style="border: solid 1px gray; height: 620px"></div>
    </span>
        </div>
        <?=Html::hiddenInput('imag_group_id',1,$options =['id'=>'imag_group_id'])?>
        <button id="SaveButton" onclick="save()">Save</button>
        <button onclick="load()">Load</button>
        <button onclick="updatevalue()">Update</button>
  <textarea id="mySavedModel" style="width:100%;height:300px">
       { "class": "go.GraphLinksModel",
  "linkFromPortIdProperty": "fromPort",
  "linkToPortIdProperty": "toPort",
  "modelData": {"position":"-5 95"},
  "nodeDataArray": [ {"category":"value", "text":"value",  "key":-3,  "loc":"950 150"} ],
  "linkDataArray": [  ]}
  </textarea>
        <div id="infoDraggable" style="display: inline-block; vertical-align: top; padding: 5px;"> </div>

        <p id="diagramEventsMsg">Msg</p>


</section>




    <!--属性面板-->
<!--<div id="AttributeMenu" style="width: 400px;position:fixed;z-index:10;display: ">-->
<div id="AttributeMenu" style="width: 400px;position:relative;z-index:10;display: ">
    <!-- row -->
    <div class="row"  >
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>

                    <div class="jarviswidget-ctrls">

                        <a id="_chart_switch" class="button-icon" href="#" data-toggle="modal" data-type="chart" rel="tooltip" data-original-title="图表切换" data-placement="bottom">
                            <i class="fa fa-bar-chart-o"></i>
                        </a>

                    </div>
                    <span class="widget-icon"> <i class="fa fa-lg fa-calendar"></i> </span>
                    <h2>属性 </h2>

                </header>

                <div>
                    <div>
                        <fieldset>
                            <div></div>
                            <div class="form-group">
                                    <?=Html::hiddenInput('image_group_id',$model->id,['id'=>'image_group_id'])?>
                                <div class="row">
                                    <div class="col-md-9" style="margin: 1px;">
                                    <?=Html::label('key',null,  ['class' => 'control-label'])?>
                                    <?=Html::textInput('element_id', isset($date_time)?$date_time:null,  ['class' => 'form-control', 'readOnly'=>'true','placeholder' => '', 'id' => 'element_id'])?>
                                    </div>
                                    </div>

                                <div class="col-md-1"></div>
                                <div class="help-block"></div>
                                 <?php echo'<div class="row"><div class="col-md-9" style="margin: 5px;">';?>
                                <?=Html::label('点位',null,  ['class' => 'control-label',   'id' => '_name'])?>
                                <?php echo '<select id= "select_point" multiple="multiple" style="width: 100%" name="point_id" placeholder="请选择相应点位">';
                                            foreach($num as $key=>$value)
                                             echo '<option   value='.$key.'>'.$value.'</option>';
                                            echo '</select></div></div>';
                                 ?>
                                <div class="help-block"></div>


                                <div class="row" >
                                    <div class="col-md-9" style="margin: 1px;">
                                    <?=Html::submitButton('提交', ['class' => 'btn btn-success plus-left', 'id' => '_submit']) ?>
                                    </div>
                                    </div>

                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>

    </div>

    <!-- end widget -->
    </article>

</div>
</div>


<?php
$this->registerJsFile("js/gojs/go.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/gojs/PortShiftingTool.js", ['backend\assets\AppAsset']);


$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css", ['backend\assets\AppAsset']);

?>


<script>



    //改变颜色
    function change(){
        var diagram=myDiagram;
        changeColor(diagram);
    }

    //弹出框初始化
    function dialog(){


        //初始化下拉框
        $("#select_point option[value='"+'en'+"']").attr("selected","selected");
        $("#select_point").chosen();

        $('#dialog_simple').dialog({
            autoOpen : false,
            width : 600,
            resizable : false,
            modal : true,
            title :  "属性设置",
            buttons : [{
                html : "<i class='fa fa-trash-o'></i>&nbsp; 确定",
                "class" : "btn btn-danger",
                click : function() {
                    change1();

                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; 取消",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }]
        });
    }

    function SetAttribute(){
        $('#dialog_simple').dialog('open');
    }

    //初始化标签页
    function tabs(){
        $('#tabs').tabs();
    }

    //属性窗口可拖动
    function drag(){
        $('#AttributeMenu').draggable();
    }

    //提供 图id与 设施key  ajax得到设施信息
    function ajaxGetAttribute(key){
        var id=$("#image_group_id").val();
        //ajax获取信息
        $.ajax({
            type: "POST",
            url: "/facility/crud/ajaxgetdata",
            data: {image_group_id:id, element_id:key},
            success: function (msg) {
                //根据image_group_id   element_id 得到属性框信息
                msg=eval('['+msg+']');
                var data=msg[0];
                var binding_id=data['binding_id'];
                binding_id=eval(binding_id);
                console.log(typeof (binding_id));
                update(binding_id);
            }
        });
    }

    //更新属性框内的点位值
    function update(value){
        //chosen 先设置值在执行更新函数
        $("#select_point").val(value);
        $("#select_point").trigger("chosen:updated");
    }

    function ajaxAttribute(){
        $('#_submit').click(function(){
            var id=$("#image_group_id").val();
            var element_id=$("#element_id").val();
            var point_ids=$("#select_point").val();
            //ajax保存信息
            $.ajax({
                type: "POST",
                url: "/facility/crud/ajaxsavedata",
                data: {image_group_id:id, element_id:element_id,binding_id:point_ids},
                success: function (msg) {
                    //根据image_group_id   element_id 得到属性框信息
                    msg=eval('['+msg+']');

                    console.log(msg);
                }
            });

        })
    }
    //跟着窗口滚动
    function scroll(){
        $(window).scroll(function(){
            var oParent = document.getElementById('AttributeMenu');
            var x =oParent.offsetLeft;
            var y = oParent.offsetTop;
            var yy = $(this).scrollTop();//获得滚动条top值
            if ($(this).scrollTop() < 30) {
                $("#AttributeMenu").css({"position":"absolute",top:"30px",left:x+"px"}); //设置div层定位，要绝对定位

            }else{
                $("#AttributeMenu").css({"position":"absolute",top:yy+"px",left:x+"px"});
            }
        });
    }

    window.onload = function () {
        dialog();drag();ajaxAttribute();scroll();
        var data=<?=$model->data?>;
        document.getElementById("mySavedModel").value=JSON.stringify(data);
        load();

        //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        myDiagram =
            $(go.Diagram, "myDiagram",  // must name or refer to the DIV HTML element
                {
                    grid: $(go.Panel, "Grid",
                        //画背景格子
                        $(go.Shape, "LineH", { stroke: "lightgray", strokeWidth: 0.5 }),
                        $(go.Shape, "LineH", { stroke: "gray", strokeWidth: 0.5, interval: 10 }),
                        $(go.Shape, "LineV", { stroke: "lightgray", strokeWidth: 0.5 }),
                        $(go.Shape, "LineV", { stroke: "gray", strokeWidth: 0.5, interval: 10 })
                    ),
                    allowDrop: true,  // must be true to accept drops from the Palette
                    "draggingTool.dragsLink": true,
                    "draggingTool.isGridSnapEnabled": true,
                    "linkingTool.isUnconnectedLinkValid": true,
                    "linkingTool.portGravity": 20,
                    "relinkingTool.isUnconnectedLinkValid": true,
                    "relinkingTool.portGravity": 20,
                    "relinkingTool.fromHandleArchetype":
                        $(go.Shape, "Diamond", { segmentIndex: 0, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "tomato", stroke: "darkred" }),
                    "relinkingTool.toHandleArchetype":
                        $(go.Shape, "Diamond", { segmentIndex: -1, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "darkred", stroke: "tomato" }),
                    "linkReshapingTool.handleArchetype":
                        $(go.Shape, "Diamond", { desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
                    "rotatingTool.snapAngleMultiple": 15,
                    "rotatingTool.snapAngleEpsilon": 15,
                    // don't set some properties until after a new model has been loaded
                    //"InitialLayoutCompleted": loadDiagramProperties,  // this DiagramEvent listener is defined below
                    "undoManager.isEnabled": true
                });

        function showMessage(s) {

            console.log(s.key);
            document.getElementById("diagramEventsMsg").textContent = s;
            document.getElementById("element_id").value  = s.key;
            ajaxGetAttribute(s.key);
            update('zh');
            document.getElementById("AttributeMenu").style.display='';

            //可拖动
            drag();
        }
        //点击事件
        myDiagram.addDiagramListener("ObjectSingleClicked",
            function(e) {
                var part = e.subject.part;
                if (!(part instanceof go.Link)) showMessage(part.data);
            });

        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("SaveButton");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
                if (idx < 0) document.title += "*";
            } else {
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });
        // Define a function for creating a "port" that is normally transparent.
        // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
        // and where the port is positioned on the node, and the boolean "output" and "input" arguments
        // control whether the user can draw links from or to the port.
        function makePort(name, spot, output, input) {
            // the port is basically just a small transparent square
            return $(go.Shape, "Circle",
                {
                    fill: null,  // not seen, by default; set to a translucent gray by showSmallPorts, defined below
                    stroke: null,
                    desiredSize: new go.Size(7, 7),
                    alignment: spot,  // align the port on the main Shape
                    alignmentFocus: spot,  // just inside the Shape
                    portId: name,  // declare this object to be a "port"
                    fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                    fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                    cursor: "pointer"  // show a different cursor to indicate potential link point
                });
        }
        var nodeSelectionAdornmentTemplate =
            $(go.Adornment, "Auto",
                $(go.Shape, { fill: null, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] }),
                $(go.Placeholder)
            );
        var nodeResizeAdornmentTemplate =
            $(go.Adornment, "Spot",
                { locationSpot: go.Spot.Right },
                $(go.Placeholder),
                $(go.Shape, { alignment: go.Spot.TopLeft, cursor: "nw-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Top, cursor: "n-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.TopRight, cursor: "ne-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Left, cursor: "w-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Right, cursor: "e-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomLeft, cursor: "se-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Bottom, cursor: "s-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomRight, cursor: "sw-resize", desiredSize: new go.Size(6, 6), fill: "lightblue", stroke: "deepskyblue" })
            );

        var nodeRotateAdornmentTemplate =
            $(go.Adornment,
                { locationSpot: go.Spot.Center, locationObjectName: "CIRCLE" },
                $(go.Shape, "Circle", { name: "CIRCLE", cursor: "pointer", desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { geometryString: "M3.5 7 L3.5 30", isGeometryPositioned: true, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] })
            );

            var mainTemplate=$(go.Node, "Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                { selectable: true, selectionAdornmentTemplate: nodeSelectionAdornmentTemplate },
                { resizable: true, resizeObjectName: "PANEL", resizeAdornmentTemplate: nodeResizeAdornmentTemplate },
                { rotatable: true, rotateAdornmentTemplate: nodeRotateAdornmentTemplate },
                new go.Binding("angle").makeTwoWay(),
                //contextMenu右键菜单属性
                {
                    contextMenu: $(go.Adornment)
                },
                // the main object is a Panel that surrounds a TextBlock with a Shape
                $(go.Panel, "Vertical",//竖直排列
                    { name: "PANEL" },
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
//                    $(go.Shape, "Rectangle",  // default figure
//                        {
//                            portId: "", // the default port: if no spot on link data, use closest side
//                            fromLinkable: true, toLinkable: true, cursor: "pointer",
//                            fill: "white"  // default color
//                        },
//                        new go.Binding("figure"),
//                        new go.Binding("fill")),
                    $(go.Picture,
                        {
                            maxSize: new go.Size(75, 75)
                        },
                        new go.Binding("source", "img")),
                    $(go.TextBlock,
                        {
                            margin: new go.Margin(3, 0, 0, 0),
                            maxSize: new go.Size(160, NaN),
                            //wrap: go.TextBlock.WrapFit,
                            editable: true,
                            isMultiline: false
                        },
                        new go.Binding("text", "text"))
                ),
                $(go.Panel, "Horizontal",//横向排列
                     $(go.TextBlock,
                    {
                        width: 40, height: 9,
                        margin: 2,
                        editable: true,
                    },
                    new go.Binding("text", "text1")),
                    $(go.TextBlock,
                        {
                            width: 40, height: 9,
                            margin: 2,
                            editable: true,
                        },
                        new go.Binding("background", "background"),
                        new go.Binding("text", "text2"))
                ),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('删除');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                },
                // four small named ports, one on each side:
                makePort("T", go.Spot.Top, false, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, false),
                { // handle mouse enter/leave events to show/hide the ports
                    mouseEnter: function(e, node) { showSmallPorts(node, true); },
                    mouseLeave: function(e, node) { showSmallPorts(node, false); }
                }
            );


        var editTemplate=
            $(go.Node,"Spot",
                //绑定位置 loc信息
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),

            $(go.Panel, "Horizontal",//横向排列
                //绑定位置 输入框text信息
                $(go.TextBlock, {
                        font: "bold 14px sans-serif",
                        stroke: '#333',
                        margin: 6,
                        isMultiline: false,
                        editable: true
                    },
            //new go.Binding("background", "background"),
            new go.Binding("text", "text"))
            ),

            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件
                    var diagram = myDiagram;
                    diagram.commandHandler.deleteSelection();
                    diagram.currentTool.stopTool();
                }
            }
        );
        var valueTemplate=
            $(go.Node,"Spot",
                { locationSpot: go.Spot.Center },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Panel, "Horizontal",//横向排列
                    //contextMenu右键菜单属性
                    {
                        contextMenu: $(go.Adornment)
                    },
                    $(go.TextBlock,"testvalue",
                        {margin: 2, font: "bold 14pt serif"},
                        //暂时无背景颜色
                        //new go.Binding("background", "background"),
                        new go.Binding("text", "text"))
                ),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('改变');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                }
            );


        // This is the actual HTML context menu:
        var cxElement = document.getElementById("contextMenu");
        // We don't want the div acting as a context menu to have a (browser) context menu!
        cxElement.addEventListener("contextmenu", function(e) { e.preventDefault(); return false; }, false);
        cxElement.addEventListener("blur", function(e) { cxMenu.stopTool(); }, false);
        // Override the ContextMenuTool.showContextMenu and hideContextMenu methods
        // in order to modify the HTML appropriately.
        var cxTool = myDiagram.toolManager.contextMenuTool;

        //重写showContextMenu方法
        cxTool.showContextMenu = function(contextmenu, obj) {
            var diagram = this.diagram;
            if (diagram === null) return;
            // Hide any other existing context menu
            if (contextmenu !== this.currentContextMenu) {
                this.hideContextMenu();
            }
            // Show only the relevant buttons given the current state.
//        var cmd = diagram.commandHandler;
//        document.getElementById("cut").style.display = cmd.canCutSelection() ? "block" : "none";
//        document.getElementById("copy").style.display = cmd.canCopySelection() ? "block" : "none";
//        document.getElementById("paste").style.display = cmd.canPasteSelection() ? "block" : "none";
//        document.getElementById("delete").style.display = cmd.canDeleteSelection() ? "block" : "none";
//        document.getElementById("color").style.display = obj !== null ? "block" : "none";
            // Now show the whole context menu element
            cxElement.style.display = "block";

            // we don't bother overriding positionContextMenu, we just do it here:
            var mousePt = diagram.lastInput.viewPoint;
            cxElement.style.left = mousePt.x +50+ "px";
            cxElement.style.top = mousePt.y +(230)+ "px";
            // Remember that there is now a context menu showing
            this.currentContextMenu = contextmenu;
        }
        //
        cxTool.hideContextMenu = function() {
            if (this.currentContextMenu === null) return;
            cxElement.style.display = "none";
            this.currentContextMenu = null;
        }


        var unitTemplate=
            $(go.Node,
                $(go.Panel, "Horizontal",//横向排列
                    $(go.TextBlock, {
                            font: "bold 14px sans-serif",
                            stroke: '#333',
                            margin: 6,
                            isMultiline: false,
                        },
                        //new go.Binding("background", "background"),
                        new go.Binding("text", "text"))
                ),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('改变');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                }
            );


        //添加node模板
        myDiagram.nodeTemplateMap.add("main", mainTemplate);
        myDiagram.nodeTemplateMap.add("edit", editTemplate);
        myDiagram.nodeTemplateMap.add("value",valueTemplate);
        myDiagram.nodeTemplateMap.add("unit", unitTemplate);

        function showSmallPorts(node, show) {
            node.ports.each(function(port) {
                if (port.portId !== "") {  // don't change the default port, which is the big shape
                    port.fill = show ? "rgba(0,0,0,.3)" : null;
                }
            });
        }
        var linkSelectionAdornmentTemplate =
            $(go.Adornment, "Link",
                $(go.Shape,
                    // isPanelMain declares that this Shape shares the Link.geometry
                    { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 })  // use selection object's strokeWidth
            );
        myDiagram.linkTemplate =
            $(go.Link,  // the whole link panel
                { selectable: true//, selectionAdornmentTemplate: linkSelectionAdornmentTemplate
                },
                { relinkableFrom: true, relinkableTo: true, reshapable: true },
                {
                    routing: go.Link.AvoidsNodes,
                    curve: go.Link.JumpOver,
                    corner: 5,
                    toShortLength: 4
                },
                new go.Binding("points").makeTwoWay(),
                //contextMenu右键菜单属性
                {
                    contextMenu: $(go.Adornment)
                },
//                $(go.Shape,  // the link path shape
//                    { isPanelMain: true, strokeWidth: 2 }),
//                $(go.Shape,  // the arrowhead
//                    { toArrow: "Standard", stroke: null }),
                $(go.Shape,{ isPanelMain: true},
                    new go.Binding("stroke", "color1"),
                    new go.Binding("strokeWidth", "width1")),
                $(go.Shape,{ isPanelMain: true},
                    new go.Binding("stroke", "color2"),
                    new go.Binding("strokeWidth", "width2")),
                $(go.Shape,{ isPanelMain: true},
                    new go.Binding("stroke", "color3"),
                    new go.Binding("strokeWidth", "width3"),
                    new go.Binding("strokeDashArray", "dash")),
//                $(go.Shape, { isPanelMain: true, stroke: "black", strokeWidth: 5 }),
//                $(go.Shape, { isPanelMain: true, stroke: "gray", strokeWidth: 3 }),
//                $(go.Shape, { isPanelMain: true, stroke: "white", strokeWidth: 1, name: "PIPE", strokeDashArray: [10, 10] }),
                //$(go.Shape, { toArrow: "Triangle", fill: "black", stroke: null }),
                { // if double-clicked, an input node will change its value, represented by the color.
                    doubleClick: function (e, obj) { //双击事件
                        console.log('改变');
                        var diagram = myDiagram;
                        diagram.commandHandler.deleteSelection();
                        diagram.currentTool.stopTool();
                    }
                },
                $(go.Panel, "Auto",
                    new go.Binding("visible", "isSelected").ofObject(),
                    $(go.Shape, "RoundedRectangle",  // the link shape
                        { fill: "#F8F8F8", stroke: null }),
                    $(go.TextBlock,
                        {
                            textAlign: "center",
                            font: "10pt helvetica, arial, sans-serif",
                            stroke: "#919191",
                            margin: 2,
                            minSize: new go.Size(20, NaN),
                            editable: true
                        },
                        new go.Binding("text").makeTwoWay())
                )
            );
        load();  // load an initial diagram from some JSON text
        // initialize the Palette that is on the left side of the page
        var myPalette1 =
            $(go.Palette, "myPalette1",  // must name or refer to the DIV HTML element
                {
                    maxSelectionCount: 1,
                    nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                    model: new go.GraphLinksModel([  // specify the contents of the Palette
                        {category:"main", text: "Start",  img:"/uploads/pic/2222.png" },
                        { category:"edit",text: "edit"},
                        { category:"value",text: "value"},
                        { category:"main",text: "水泵", img:"/uploads/pic/水泵.png" },
                        { category:"main",text: "冷却机",img:"/uploads/pic/冷却机.png" },
                        { category:"main",img:"/uploads/pic/冷却机.png" }
                    ], [
                        // the Palette also has a disconnected Link, which the user can drag-and-drop
                       { points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"red",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]},
                        { points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"blue",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]}
                    ])
                });


        var myPalette2 =
            $(go.Palette,"myPalette2",  // must name or refer to the DIV HTML element
                {
                    maxSelectionCount: 1,
                    nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                    model: new go.GraphLinksModel([  // specify the contents of the Palette
                       { category:"edit",text: "edit"},
                        { category:"value",text: "value"}
                    ], [
                        // the Palette also has a disconnected Link, which the user can drag-and-drop
//                        { points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"red",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]},
                    ])
                });


        var myPalette3 =
            $(go.Palette,"myPalette3",  // must name or refer to the DIV HTML element
                {
                    maxSelectionCount: 1,
                    nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                    linkTemplate: // simplify the link template, just in this Palette
                        $(go.Link,
                            { // because the GridLayout.alignment is Location and the nodes have locationSpot == Spot.Center,
                                // to line up the Link in the same manner we have to pretend the Link has the same location spot
                                locationSpot: go.Spot.Center,
                                selectionAdornmentTemplate:
                                    $(go.Adornment, "Link",
                                        { locationSpot: go.Spot.Center }
//                                        $(go.Shape,
//                                            { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 })
//                                        $(go.Shape,  // the arrowhead
//                                            { toArrow: "Standard", stroke: null })
                                    )
                            },
                            {
                                routing: go.Link.AvoidsNodes,
                                curve: go.Link.JumpOver,
                                corner: 5,
                                toShortLength: 4
                            },
                            new go.Binding("points"),
                            $(go.Shape,  // the link path shape
                                { isPanelMain: true, strokeWidth: 2 })
//                            $(go.Shape,  // the arrowhead
//                                { toArrow: "Standard", stroke: null })
                        ),
                    model: new go.GraphLinksModel([  // specify the contents of the Palette
                    ], [
                        // the Palette also has a disconnected Link, which the user can drag-and-drop
                        { points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"red",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]},
                        //{ points: new go.List(go.Point).addAll([new go.Point(0, 0), new go.Point(30, 0), new go.Point(30, 50), new go.Point(60, 50)]),color1:"blue",width1: 5,color2:"gray",width2:3,color3:"white",width3:1,dash:[10, 10]}
                    ])
                });

        // Show the diagram's model in JSON format that the user may edit
        //div初始化之后再标签化
        tabs();
    }




    function changeColor(diagram) {
        // the object with the context menu, in this case a Node, is accessible as:
        var cmObj = diagram.toolManager.contextMenuTool.currentObject;
        // but this function operates on all selected Nodes, not just the one at the mouse pointer.

        // Always make changes in a transaction, except when initializing the diagram.
        diagram.startTransaction("change color");
        diagram.selection.each(function(node) {

            //if (node instanceof go.Node) {  // ignore any selected Links and simple Parts
                // Examine and modify the data, not the Node directly.
                var data = node.data;
            console.log(data);
                if (data.color1 === "red") {
                    // Call setDataProperty to support undo/redo as well as
                    // automatically evaluating any relevant bindings.
                    //color1:"red",width1: 5,color2:"gray"
                    //color1:"blue",width1: 5,color2:"gray"
                    console.log("blue");
                    diagram.model.setDataProperty(data, "color1","blue");
                } else {
                    console.log("red");
                    diagram.model.setDataProperty(data, "color1", "red");
                }
            //}
        });
        diagram.commitTransaction("change color");
    }


    //更新所有value节点的值
    function updatevalue() {
        var id=$("#image_group_id").val();
        //加入指定的node
        var diagram=myDiagram;
        var temp = diagram.model.toJson();
        var data = eval('(' + temp + ')');
        //得到节点对象
        var nodeDataArray = data['nodeDataArray'];
        console.log(nodeDataArray);
        //找到key所表示的节点更新text为value
        var element_ids=new Array();
        for(var nodeDatas in nodeDataArray){
            console.log(nodeDatas);
            var nodecategory=nodeDataArray[nodeDatas]['category']
                //找到value节点实时更新点位的值
            console.log(nodecategory=='value');
                if(nodecategory=='value')
                {
                    console.log(nodeDataArray[nodeDatas]);
                    console.log(nodecategory);
                    //得到value节点的key
                    var nodekey=nodeDataArray[nodeDatas]['key'];
                    console.log(nodeDatas);
                    element_ids.push(nodekey);

                }
        }
            console.log(element_ids);
        //ajax保存到数据库
        $.ajax({
            type: "POST",
            url: "/facility/crud/ajaxvalue",
            data: {image_group_id:id, element_ids:element_ids},
            success: function (msg) {
                //返回value绑定点位的值
//                console.log(msg);
                msg=eval('('+msg+')');
//                console.log(msg);
                //把查询返回的值重新写入节点数组中
                for(var nodeDatas in nodeDataArray){
                    var nodecategory=nodeDataArray[nodeDatas]['category']
                    //找到value节点实时更新点位的值
                    if(nodecategory=='value'){
                        var nodekey=nodeDataArray[nodeDatas]['key'];
                        nodeDataArray[nodeDatas]['text']=msg[nodekey];
                    }
                }
                diagram.model = new go.TreeModel(nodeDataArray);
            }
        });

//        for(var nodeDatas in nodeDataArray){
//            nodeDataArray[nodeDatas]['text']=msg;
//            diagram.model = new go.TreeModel(nodeDataArray);
//        }
        //var a=eval('(' + temp + ')');

        //var a = eval(({category: "value", text: value}));
        //nodeDataArray.push(a);
        //console.log(nodeDataArray);
        //把更新完数据后的节点数组nodedataArray加入diagram中

    }

    function save() {
        saveDiagramProperties();  // do this first, before writing to JSON
        var controller_info=myDiagram.model.toJson();
        document.getElementById("mySavedModel").value = controller_info;
        var id=document.getElementById("image_group_id").value
        console.log(id);
        //ajax保存到数据库
        $.ajax({
            type: "POST",
            url: "/facility/crud/save",
            data: {ImageGroup:{id:id, name:"test",data:controller_info}},
            success: function (msg) {
                console.log(msg);
            }
        });


        myDiagram.isModified = false;
    }
    function load() {
        myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
        // loadDiagramProperties gets called later, upon the "InitialLayoutCompleted" DiagramEvent
    }
    function saveDiagramProperties() {
        myDiagram.model.modelData.position = go.Point.stringify(myDiagram.position);
    }
    // Called by "InitialLayoutCompleted" DiagramEvent listener, NOT directly by load()!
    function loadDiagramProperties(e) {
        var pos = myDiagram.model.modelData.position;
        if (pos) myDiagram.position = go.Point.parse(pos);
    }

</script>