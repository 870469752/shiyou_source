<?php

namespace backend\module\facility\controllers;

use backend\models\AlarmLog;
use backend\models\ImageGroup;
use backend\models\ImageGroupElement;
use backend\models\Point;
use Yii;
use backend\models\AlarmEvent;
use backend\models\PointEvent;
use backend\models\CompoundEvent;
use backend\models\search\AlarmEventSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\search\ImageGroupSearch;
use backend\models\OperateLog;
/**
 * AlarmEventController implements the CRUD actions for AlarmEvent model.
 */
class CrudController extends MyController
{


    /**
     * Lists all AlarmEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single AlarmEvent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()//$id
    {
        $model =new ImageGroup();
        return $this->render('view',
            [
                'model' => $model,
            ]);
    }

    /**
     * Creates a new AlarmEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

    }
    public function  actionTest(){

        return $this->render('test');
    }
    //后台在image_group_element中查询image_group_id  element_id
    //测试数据
    //        $data= [
    //            'image_group_id'=>1,    //id
    //            'element_id'=>1,        //key
    //        ];
    public function actionAjaxgetdata(){
        $data = Yii::$app->request->post();
        $model=ImageGroupElement::findImageGroupElementModelByData($data);
        $data=$model->oldAttributes;
        $data=json_encode($data);
        return $data;

    }


    public function actionAjaxsavedata(){
        $data = Yii::$app->request->post();

        $data['binding_id']=json_encode($data['binding_id']);

        //$model=new ImageGroupElement();

        $model=ImageGroupElement::findImageGroupElementModelByData($data);

        $data=['ImageGroupElement'=>$data];

        //$data['ImageGroupElement']=$data;
        $model->load($data);
        //$model->id=0;
        $result=$model->save();
        if (  $result== false )
        {
            print_r($model->errors);

        }
        else{
            return $result;
        }


    }
    //ajax得到value节点绑定的点位的值
    public function actionAjaxvalue(){
        $data = Yii::$app->request->post();
        $result=array();
        $image_group_id=$data['image_group_id'];
        $element_ids=$data['element_ids'];
        foreach ($element_ids as $value) {
            $test_data=[
                'image_group_id'=>$image_group_id,
                'element_id'=>$value
            ];

            $model=ImageGroupElement::findImageGroupElementModelByData($test_data);
            $points=$model->binding_id;
            //得到绑定的点位
            $points=json_decode($points);
            //查找点位的值
            $point_value=0;
//        var_dump($data);
            if(!empty($points)) {
                foreach ($points as $point) {
                    //TODO   现在点位为虚拟的 待修改
                    $point_info=Point::getPointInfoById(9)[0];
                    //$point_model = Point::findOne($point);
                    $point_value+=$point_info['last_value'];

                }
                  $result[$value]=round($point_value,2);
            }else $result[$value]=null;
        }
        return json_encode($result);


    }

    public function actionSave(){
        $data = Yii::$app->request->post();

        $id=$data['ImageGroup']['id'];
        $data['ImageGroup']['name']=MyFunc::createJson($data['ImageGroup']['name']);

        $model=new ImageGroup();
        $model=$model->findImageGroupModel($id);
        $model->load($data);

        $result=$model->save();

        if (  $result== false )
        {
            print_r($model->errors);

        }
        else{
            return $result;
        }
    }

    /**
     * Updates an existing AlarmEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model =new ImageGroup();
        $model=$model->findImageGroupModel($id);

        return $this->render('view',
            [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing AlarmEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {


        return $this->redirect(['index']);
    }




}
