<?php

namespace backend\module\facility;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\module\facility\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
