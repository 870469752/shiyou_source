<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SysMenu $model
 */

$this->title = Yii::t('app', 'Update Menu'). ': ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'p_data' => $p_data,
        'c_data' => $c_data,
        'i_data' => $i_data,
    ]) ?>

</div>
