<?php
use yii\helpers\Html;
use common\library\MyHtml;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\MenuSearch $searchModel
 */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <div id="nestable-menu">
        <button id = '_toggle' type="button" class="btn btn-default" data-action="expand-all">
            <?=Yii::t('app', 'Menu Off')?>
        </button>

        <?=Html::a(Yii::t('app', 'Create Menu'), ['create'], ['class' => 'btn btn-default']);?>
    </div>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>code </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-lg-offset-2">
                                <h6><?=Yii::t('app', 'Menu Tree Display')?>:</h6>
                            </div>
                            <div class="col-sm-12 col-lg-6 col-lg-offset-3">
                                <div class="dd" id="nestable">
                                    <?php echo MyHtml::MenuTreeRecursion($nav); ?>
                                </div>
                            </div>
                        </div>
                    </div>

</section>
<?php
$this->registerJsFile ( "/js/my_js/move_menu.js", ['yii\web\JqueryAsset'] );
?>

<script>
    window.onload = function() {
        //切换菜单闭合 点击事件
        $('#_toggle').click(function(){
            var $_data_status = $(this).data('action');
            if($_data_status == 'expand-all'){
                $(this).data('action', 'collapse-all');
                $(this).text(lang('Menu On'));
            }else{
                $(this).data('action', 'expand-all');
                $(this).text(lang('Menu Off'));
            }
        })
    }
</script>