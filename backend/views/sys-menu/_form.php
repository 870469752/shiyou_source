<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var backend\models\SysMenu $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?=$model->isNewRecord ? Yii::t('app', 'Create Menu') :  Yii::t('app', 'Update Menu')?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <a id = 'top_menu' class="btn btn-primary btn-sm" href="javascript:void(0);">顶级菜单</a>
                            <?php if(count($p_data)){ ?>
                                <a id = 'general_menu' class="btn btn-success btn-sm" href="javascript:void(0);">普通菜单</a>
                            <?php }?>
                            <HR />
                            <?= $form->field($model, 'parent_id')->dropDownList($p_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'description')->textInput() ?>

                            <?= Html::hiddenInput('menu_type','', ['id' => '_menu_type'])?>

                            <?= $form->field($model, 'module_id')->dropDownList($c_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'idx')->textInput() ?>

                            <label class = 'control-label'><?=Yii::t('app', 'Icon');?>: </label><?= MyHtml::activeRadioList($model, 'icon', $i_data,['class'=>'radio_icon', 'encode' => 0])?>



                        </fieldset>

                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>

<script>
    window.onload = function (){
        //获取 父级id
        var $parent_id = <?=isset($model->parent_id)? $model->parent_id : 0?>;
        var $_type = <?=isset($_GET['type'])?$_GET['type'] : 3?>;



        /**
         * 将 菜单 分为 顶级菜单 和 普通菜单
         * 顶级菜单： 只用添加 顶级菜单的 名字 和 叶子节点
         * 普通菜单： 除了 顶级菜单所需参数 还需要 选择 父级菜单
         */
        $("#top_menu").click(function (){
            $(".field-sysmenu-parent_id").hide();
            // 在下拉菜单中添加 顶级菜单的选项 然后 让其 选中
            $("#sysmenu-parent_id").append("<option id = '_top_' value = 0></option>");
            $("#sysmenu-parent_id").val(0);
            $('#_menu_type').val(1);
        });

        $("#general_menu").click(function (){
            $(".field-sysmenu-parent_id").show();
            $("#sysmenu-parent_id").val('');
            //删除 添加的 顶级菜单option
            $("#_top_").remove();
            $('#_menu_type').val(2);
        })

        //利用父级id 判断 页面展现形式
        if($parent_id){
            //如果存在 上级id,并且上级id部位0那么 就出发 普通菜单
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }
        else{
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }

        //利用传递的参数判但当前用户上次创建菜单是 顶级菜单还是 普通菜单
        if($_type == 1){
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }else if($_type == 2){
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }

        $(".form-actions .btn").click(function(){
            var $form = $('#w0').serializeArray();
            $($form).each(function(k, v){
                console.log(v.name + "==" + v.value);
            })
        })
    }
</script>