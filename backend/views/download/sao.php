<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 *
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = YII::t('app', '扫一扫');
?>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <h1 class="error-text tada animated">
                        <i class="fa fa-delicious"></i>
                        <?= Html::encode($this->title) ?>
                    </h1>
                    <h2 class="font-xl">
                        <strong>拿起手机，打开微信扫一扫</strong>
                    </h2>
                    <br />
                    <p class="lead semi-bold">
                        <strong>
                            就在下面，赶紧的！
                        </strong>
                        <br>
                        <br>
                        <small>
                            <img src="/img/ewm.png" width="200" />
                        </small>
                    </p>
                </div>

            </div>

        </div>

    </div>

</div>