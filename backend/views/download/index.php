<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 *
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = YII::t('app', '扫一扫');
?>

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <br/>
                    <br/>
                    <p class="lead semi-bold">
                        <strong>
                            点击微信右上角菜单
                            <br/>
                            在浏览器中打开
                        </strong>
                            <br/>
                            <br/>
                            再下方图标下载！

                        <br/>
                        <br/>
                        <small>
                            <a href="/ems2.apk"><img src="/img/android_download.jpg" width="200"/></a>
                        </small>
                    </p>
                </div>

            </div>

        </div>

    </div>

</div>