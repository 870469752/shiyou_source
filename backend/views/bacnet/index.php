<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\BacnetSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Bacnets');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>
			</header>
			<!-- widget div-->
			<div>

    <?= GridView::widget([
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

            ['attribute' => 'name'],
            ['attribute' => 'ip_address', 'headerOptions' => ['data-hide' => 'phone']],
            ['attribute' => 'port', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'is_shield', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}', 'headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>

<?php
// 搜索和创建按钮
//$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);
$update_button = Html::a(Yii::t('app', 'Update Bacent'), ['#'], ['class' => 'btn btn-default', 'id' => 'eg']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$create_button}');
options.button.push('{$update_button}')
table_config('datatable', options);						
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);
?>
<!-- 搜索表单和JS --> 
<?php MyFunc::TableSearch($searchModel) ?>

    			</div>
			</div>
		</article>
	</div>
</section>

<script>
    window.onload = function(){
        $('#eg').click(function(e) {
           $.smallBox({
                title : "Bacnet 设备更新中，请等待...",
                content : "更新时间大约5秒钟!",
                color : "#3276B1",
                icon :"fa fa-bell swing animated",
                timeout : 1151000,
                sound: false
            });

            $url = '/bacnet/ajax-manually-monitoring';
            $.post($url, '', function($data){
                $('#divSmallBoxes>div:last').remove();                                                                  //jquery 选择最后一个提示，移除
                $.smallBox({
                    title : "更新完成!",
                    content : " ",
                    color : "#739E73",
                    iconSmall : "fa fa-cloud",
                    timeout : 2000,
                    sound: false
                });
            });

            //数据库更新完毕 刷新页面
            location.reload();
        })
    }
    </script>
