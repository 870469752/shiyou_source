<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Bacnet $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Bacnet',
]) . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
