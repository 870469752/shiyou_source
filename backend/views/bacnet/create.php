<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Bacnet $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Bacnet',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
