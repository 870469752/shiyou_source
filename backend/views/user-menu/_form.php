<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;


/**
 * @var yii\web\View $this
 * @var backend\models\UserMenu $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?=$model->isNewRecord ? Yii::t('app', 'Create Menu') :  Yii::t('app', 'Update Menu')?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <a id = 'top_menu' class="btn btn-primary btn-sm" href="javascript:void(0);">顶级菜单</a>
                            <?php if(count($p_data)){ ?>
                                <a id = 'general_menu' class="btn btn-success btn-sm" href="javascript:void(0);">普通菜单</a>
                            <?php }?>
                            <HR />
                            <?= $form->field($model, 'parent_id')->dropDownList($p_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'description')->textInput() ?>

                            <?= Html::hiddenInput('menu_type','', ['id' => '_menu_type'])?>

                            <?= $form->field($model, 'module_id')->dropDownList($c_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'param')->dropDownList([], ['class'=>'select2']); ?>

                            <?= $form->field($model, 'idx')->textInput() ?>

                            <label class = 'control-label'><?=Yii::t('app', 'Icon');?>: </label><?= MyHtml::activeRadioList($model, 'icon', $i_data,['class'=>'radio_icon', 'encode' => 0])?>



                        </fieldset>

                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
    <div class="modal fade" style="padding-top: 200px" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 400px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel">添加</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" >时间段名
                                <input type="text" id = "time_name" style="width: 50%">
                            </div>
                            <div class="form-group">开始时间
                                <select id="select_begin_time" style="width: 50%">
                                    <option value="null" selected>无</option>
                                </select>
                            </div>
                            <div class="form-group">结束时间
                                <select id="select_end_time" style="width: 50%">
                                    <option value="null" selected>无</option>

                                </select>
                            </div>
                            <div id = "error_message" style = "display:none" class = "form-group">
                                <i class="fa-fw fa fa-warning"></i>
                                <span style = "color:red" id = "_message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id = "add_action" type="button" class="btn btn-primary">
                        Add
                    </button>
                    <button type="button" id = "_cancel" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</section>

<script>
    window.onload = function (){
        //获取 父级id
        var $parent_id = <?=isset($model->parent_id)? $model->parent_id : 0?>;
        var $_type = <?=isset($_GET['type'])?$_GET['type'] : 3?>;

        /**
         * 将 菜单 分为 顶级菜单 和 普通菜单
         * 顶级菜单： 只用添加 顶级菜单的 名字 和 叶子节点
         * 普通菜单： 除了 顶级菜单所需参数 还需要 选择 父级菜单
         */
        $("#top_menu").click(function (){
            $(".field-usermenu-parent_id").hide();
            // 在下拉菜单中添加 顶级菜单的选项 然后 让其 选中
            $("#usermenu-parent_id").append("<option id = '_top_' value = 0></option>");
            $("#usermenu-parent_id").val(0);
            $('#_menu_type').val(1);
        });

        $("#general_menu").click(function (){
            $(".field-usermenu-parent_id").show();
            $("#usermenu-parent_id").val('');
            //删除 添加的 顶级菜单option
            $("#_top_").remove();
            $('#_menu_type').val(2);
        })

        //利用父级id 判断 页面展现形式
        if($parent_id){
            //如果存在 上级id,并且上级id部位0那么 就出发 普通菜单
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }
        else{
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }

        //利用传递的参数判但当前用户上次创建菜单是 顶级菜单还是 普通菜单
        if($_type == 1){
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }else if($_type == 2){
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }

        $(".form-actions .btn").click(function(){
            var $form = $('#w0').serializeArray();
            $($form).each(function(k, v){
                console.log(v.name + "==" + v.value);
            })
        })

        $("#usermenu-module_id").change(function (){
            var module_id = $(this).val();
            $.ajax({
                url: '/user-menu/sources?id='+module_id,
                async: false,
                dateType: 'json',
                success: function(data)
                {
                    var str = '';
                    $.each(JSON.parse(data), function (k, v) {
                        str+= '<option value="'+v.id+'">'+v.name+'</option>'
                    })
                    $("#usermenu-param").html('');
                    $("#usermenu-param").html(str);
                }
            });
        })
    }
</script>