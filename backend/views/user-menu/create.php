<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SysMenu $model
 */
$this->title = Yii::t('app', 'Create Menu', [
    'modelClass' => 'Menu',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
    'p_data' => $p_data,
    'c_data' => $c_data,
    'i_data' => $i_data,
]) ?>
