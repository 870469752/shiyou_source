<?php
use yii\helpers\Html;
use common\library\MyHtml;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\UserMenuSearch $searchModel
 */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
    <div id="nestable-menu">
        <a class="btn btn-default" href="/user">用户管理</a>

        <a class="btn btn-default" data-toggle="modal" data-target="#matchMenuModal">快速匹配菜单</a>
        <button id = '_toggle' type="button" class="btn btn-default" data-action="expand-all">
            <?=Yii::t('app', 'Menu Off')?>
        </button>

        <?=Html::a(Yii::t('app', 'Create Menu'), ['create'], ['class' => 'btn btn-default']);?>
    </div>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>code </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-lg-offset-2">
                                <h6><?=$username."::".Yii::t('app', 'Menu Tree Display')?>:</h6>
                            </div>
                            <div class="col-sm-12 col-lg-6 col-lg-offset-3">
                                <div class="dd" id="nestable">
                                    <?php echo MyHtml::UserMenuTreeRecursion($nav); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <!-- Modal -->
    <div class="modal fade"  id="matchMenuModal" tabindex="-1" role="dialog" aria-labelledby="matchMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">菜单匹配</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="resForm" style="margin-left: 10px">
                        <input type="hidden" id="objectUnitId"/>
                        <div class="form-group">
                            <select id="userid" class="select2">
                                <option value="0">请选择</option>
                                <?php foreach($all_user as $k => $v){ ?>
                                    <option value="<?=$v['id']; ?>"><?=$v['username']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="btnSave">确定</button>
                    <button class="btn btn-danger" data-dismiss="modal"
                            aria-hidden="true">取消
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$this->registerJsFile ( "/js/my_js/move_menu.js", ['yii\web\JqueryAsset'] );
?>

<script>
    window.onload = function() {
        //切换菜单闭合 点击事件
        $('#_toggle').click(function(){
            var $_data_status = $(this).data('action');
            if($_data_status == 'expand-all'){
                $(this).data('action', 'collapse-all');
                $(this).text(lang('Menu On'));
            }else{
                $(this).data('action', 'expand-all');
                $(this).text(lang('Menu Off'));
            }
        })

        $('#btnSave').click(function(){
            var user_id = $('#userid').val();
            if(user_id !=0){
                $.ajax({
                    url: '/user-menu/match-menu?userid='+user_id,
                    async: true,
                    dateType: 'json',
                    success: function(data)
                    {
                        return false;
                    },
                    complete: function (XHR, TS) { XHR = null }
                })
            }else{
                alert("请选择匹配对象！");
                return false;
            }
        })
    }
</script>
