<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyHtml;


/**
 * @var yii\web\View $this
 * @var backend\models\UserMenu $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2>添加子菜单</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>
                            <?= $form->field($model, 'parent_id')->dropDownList($p_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'description')->textInput() ?>

                            <?= Html::hiddenInput('menu_type','', ['id' => '_menu_type'])?>

                            <?= $form->field($model, 'module_id')->dropDownList($c_data, ['class'=>'select2']); ?>

                            <?= $form->field($model, 'param')->dropDownList([], ['class'=>'select2']); ?>

                            <?= $form->field($model, 'idx')->textInput() ?>

                            <label class = 'control-label'><?=Yii::t('app', 'Icon');?>: </label><?= MyHtml::activeRadioList($model, 'icon', $i_data,['class'=>'radio_icon', 'encode' => 0])?>



                        </fieldset>

                        <div class="form-actions">
                            <?= Html::submitButton( Yii::t('app', 'Create') , ['class' =>  'btn btn-success'])?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>

</section>

<script>
    window.onload = function (){
        $(".form-actions .btn").click(function(){
            var $form = $('#w0').serializeArray();
            $($form).each(function(k, v){
                console.log(v.name + "==" + v.value);
            })
        })

        $("#usermenu-module_id").change(function (){
            var module_id = $(this).val();
            $.ajax({
                url: '/user-menu/sources?id='+module_id,
                async: false,
                dateType: 'json',
                success: function(data)
                {
                    //alert(data);return false;
                    var str = '';
                    $.each(JSON.parse(data), function (k, v) {
                        str+= '<option value="'+v.id+'">'+v.name+'</option>'
                    })
                    $("#usermenu-param").html('');
                    $("#usermenu-param").html(str);
                }
            });
        })
    }
</script>