<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\LoginForm $model
 */
$this->title = \Yii::t('app', 'Login');
$this->params ['breadcrumbs'] [] = $this->title;
$this->registerCssFile ( "css/lockscreen.min.css" );
$config_site = Yii::$app->cache->get('config_site');
?>

<div class="lockscreen animated flipInY" action="/index.php">
	<div class="logo" style="background-color: #000000">
            <h2 class="semi-bold" style="color: #FFFFFF;">
			&nbsp;&nbsp;<img src="/<?= YII::$app->params['uploadPath'] . ArrayHelper::getValue($config_site,'logo_url') ?>" alt="" />
                中石油科技园区集成管理平台
		</h2>
	</div>
	<div>
		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
			]); ?>
			<?= $form->field($model, 'username')->textInput(['margin-bottom' => '0px'])?>
			<?= $form->field($model, 'password')->passwordInput(['margin-bottom' => '0px'])?>
        <div style="text-align: center">
			<?= Html::submitButton(\Yii::t('app', 'Login'), ['class' => 'btn bg-color-blueDark txt-color-white', 'name' => 'login-button', 'margin-bottom' => '0px'])?>
        </div>
		<?php ActiveForm::end(); ?>
	</div>
</div>