<?php
/**
 * @var yii\web\View $this
 */
$this->title = Yii::t ( 'app', 'Home' );
?>
<h1><?= YII::t('app', 'Energy Management System') ?> 2.0 ！</h1>
<span><?= YII::t('app', 'Development work is all day and night, please look !') ?></span>
<br/>
<br/>
<h3><strong><?= YII::t('app', 'The current progress: ') ?></strong></h3>
<div class="progress progress-1x progress-striped active">
	<div class="progress-bar bg-color-darken" role="progressbar"
		style="width: 23.3%">23.3%</div>
</div>