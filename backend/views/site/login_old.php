<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\LoginForm $model
 */
$this->title = \Yii::t('app', 'Login');
$this->params ['breadcrumbs'] [] = $this->title;
$this->registerCssFile ( "css/lockscreen.min.css" );
$config_site = Yii::$app->cache->get('config_site');
?>

<div class="lockscreen animated flipInY" action="/index.php">
	<div class="logo" style="background-color: #eae6bb">
            <h1 class="semi-bold">
			&nbsp;&nbsp;<img src="/<?= YII::$app->params['uploadPath'] . ArrayHelper::getValue($config_site,'logo_url') ?>" alt="" />
			<?= MyFunc::DisposeJSON(ArrayHelper::getValue($config_site,'name')) ?>
		</h1>
	</div>
	<div>
		<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
			<?= $form->field($model, 'username')?>
			<?= $form->field($model, 'password')->passwordInput()?>
			<?= $form->field($model, 'rememberMe')->checkbox()?>
			<div class="form-group">
				<?= Html::submitButton(\Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button'])?>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>