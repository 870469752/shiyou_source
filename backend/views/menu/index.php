<?php
use yii\helpers\Html;
use common\library\MyHtml;
use yii\grid\GridView;
use backend\assets\AppAsset;
use common\library\MyFunc;
use Yii\web\View;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\MenuSearch $searchModel
 */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
$nav = Yii::$app->cache->get( 'nav' );
?>
<section id="widget-grid" class="">
    <div id="nestable-menu">
        <button id = '_toggle' type="button" class="btn btn-default" data-action="expand-all">
            <?=Yii::t('app', 'Menu On')?>
        </button>

        <?=Html::a(Yii::t('app', 'Create Menu'), ['create'], ['class' => 'btn btn-default']);?>
    </div>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>code </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-lg-offset-2">
                               <h6><?=Yii::t('app', 'Menu Tree Display')?>:</h6>
                           </div>
                           <div class="col-sm-12 col-lg-6 col-lg-offset-3">

                             

                            <div class="dd" id="nestable">
                                <ol class="dd-list">
                                   <!-- start menu-->
                                   <?php
                                   function aa($nav = []){
                                    $languge = Yii::$app->language;
                                    $menu_list = '';
                                    foreach($nav as $key => $nav_item){
                                        if(isset($nav_item['id'])){
                                            $id=$nav_item["depth"];
                                            $str_list = '<div class = "pull-right"><div>'
                                                //删除按钮
                                                . Html::a('', ['menu/deletes','id' =>$nav_item['id']], ['id' =>$nav_item['id'] ,'class' => 'fa fa-times '.'delete'.$id, 'data-confirm'=>Yii::t('app', 'This will remove all the leaf nodes are removed!')])
                                            .'&nbsp&nbsp&nbsp'
                                                . Html::a('', ['menu/update_menu','id' =>$nav_item['id']], ['class' => 'fa fa-pencil'])
                                            .'</div>'
                                            .'</div></div>';
                                        }
                                        $name = isset($nav_item["description"]) ? MyFunc::DisposeJSON($nav_item["description"]): '';
                                        $depth='id ="dd3-content'.$nav_item["depth"].'"' ;
                                        $menu_list .= '<li class="dd-item dd3-item" data-id="'.($key+1).'"><div style="cursor:auto;" class="dd3-handle">Drag</div><div class="dd3-content"'.$depth.'>';
                                        $menu_list .= isset($name) ? $name : YII::t('app', '(No Name)');
                                        if (isset ( $nav_item ["sub"] ) && $nav_item ["sub"]){
                                            $menu_list .= $str_list;
                                                //$menu_list  变量 在此 不会消失
                                            $menu_list .= '<ol class="dd-list">'.aa( $nav_item ["sub"] ).'</ol>';
                                        }else{
                                            if(empty($nav_item['url'])){
                                                $menu_list .= $str_list;
                                            }else{
                                                $menu_list .= '<div class = "pull-right"><div>'
//                                                    . Html::a('', ['menu/deletes', 'id' => $nav_item['id']], ['class' => 'fa fa-times', 'data-confirm'=>Yii::t('app', 'This will remove all the leaf nodes are removed!')])
//                                                     .'&nbsp&nbsp&nbsp'
                                                    . Html::a('', ['menu/update_menu', 'id' => $nav_item['id'], 'url' => 1], ['class' => 'fa fa-pencil'])
                                                    .'</div>'
                                                    .'</div></div>';
                                            }
                                        }
                                        $menu_list .= '</li>';

                                    }

                                    return $menu_list;
                                }
                                echo aa($nav);
                                ?>

                            </ol>
                            <!-- end menu-->

                        </div>
                    </div>
                </div>
            </div>

        </section>
<div>
<!--    --><?php //echo MyHtml::EnergyTreeRecursion($nav); ?>
    </div>
<?php
    $this->registerJsFile ( "/js/my_js/move_menu.js", ['yii\web\JqueryAsset'] );
?>

<script>
    window.onload = function() {
        //去掉主菜单的删除按钮
//        $('.delete1').remove();
        //去掉模块菜单的修改按钮
        //切换菜单闭合 点击事件
        $('#_toggle').click(function(){
            var $_data_status = $(this).data('action');
            if($_data_status == 'expand-all'){
                $(this).data('action', 'collapse-all');
                $(this).text(lang('Menu On'));
            }else{
                $(this).data('action', 'expand-all');
                $(this).text(lang('Menu Off'));
            }
        })
        $('#_toggle').trigger('click');
       // console.log($('#nestable').nestable('serialize'));
    }

    </script>