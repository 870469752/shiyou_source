<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Menu $model
 */

$this->title = Yii::t('app', 'Create Menu', [
  'modelClass' => 'Menu',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">

    <h6><?= Html::encode($this->title) ?></h6>

    <?= $this->render('_form', [
        'model' => $model,
        'p_data' => $p_data,
        'c_data' => $c_data,
        'i_data' => $i_data,
        'a_data' => $a_data,
        'id'=> $id,
        'parent_id' => $parent_id,
        'depth'=> $depth,
        'a_menu' =>$a_menu,
        'url'=>-1,
        'symbol'=>0,
    ]) ?>

</div>
