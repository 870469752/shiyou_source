<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Menu $model
 */

$this->title = Yii::t('app', 'Update Menu'). ': ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="menu-update">

    <h6><?= Html::encode($this->title) ?></h6>

    <?= $this->render('_form', [
        'model' => $model,
        'p_data' => $p_data,
        'c_data' => $c_data,
        'i_data' => $i_data,
        'a_data' => $a_data,
        'id' =>$id,
        'parent_id' => $parent_id,
        'depth' => $depth,
        'a_menu' =>$a_menu,
        'url' =>$url,
        'iconer' => $iconer,
        'symbol' =>$symbol,
    ]) ?>

</div>
