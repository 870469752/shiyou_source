<?php
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2015/9/14
 * Time: 12:16
 */
//$data=array(['Firefox',45.0],
//    ['IE',26.8],
//    ['Chrome',12.8],
//    ['Safari',8.5],
//    ['Opera',6.2],
//    ['Others',0.7],
//);
$chorme="'Chorme'";$d1=80;$id1=1;
$ie="'IE'";$d2=20;$id2=2;
                    $data1='{ name:'
                        .$chorme.
                        ',y:'.
                        $d1.
                        ',id:'
                        .$id1.
                    '}'.
                    ','.
                  '{ name:'
                  .$ie.
                  ',y:'.
                  $d2.
                  ',id:'
                  .$id2.
                  '}';

                    $data=array(['Firefox',45.0],
                        ['IE',26.8],
                        ['Chrome',12.8],
                        ['Safari',8.5],
                        ['Opera',6.2],
                        ['Others',0.7],
                        );
?>
<style>
    #chartMenu
    {
        font-family: Arial;
        border: 5px solid #dedede;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        padding:5px;
        height: 100px;
        width: 150px;
        position:absolute;
        z-index:9999;
        background-color:#dedede;
    }
    #chartMenu a, a:link,a:active,a:visited
    {
        font-size:18px;
        color:#000;
    }
    #chartMenu a:hover
    {
        color:blue;
    }
    </style>
<div id="chartMenu">
    <div>
        <a href="javascript:void(0)" onclick="ExportChart()">导出图表</a>
    </div>
    <div>
        <a href="javascript:void(0)" onclick="ExportChart()">图表重画</a>
    </div>
    <div>
        <a href="javascript:void(0)" onclick="ExportChart()">动态追加Series</a>
    </div>
    <div>
        <a href="javascript:void(0)" onclick="ExportChart()">动态修改标题</a>
    </div>
</div>

<div id="container1">
    1111
    </div>
<div id="container">
    1111
</div>
<div id="container2">
    1111
</div>

<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highstock.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/highcharts/modules/exporting.js", ['backend\assets\AppAsset']);
$this->registerJsFile('js/highcharts/highschartsContextMenu.src.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
?>
<script>
    //隐藏图表右键菜单
    function HideRightMenu() {
        $("#chartMenu").hide();
    }
    function ExportChart() {
      alert(1111);
        HideRightMenu();
    }
    //注册图表右键事件
    function RegChartRightClickEvents() {
        //图表容器的右键鼠标事件
        $("#container").bind("mouseup", function (oEvent) {
            if (!oEvent) oEvent = window.event;
            if (oEvent.button == 2) {
                //动态控制菜单的绝对位置
                $("#chartMenu").css("top", oEvent.clientY);
                $("#chartMenu").css("left", oEvent.clientX);
                $("#chartMenu").show();
            }
        });
        //整个页面的右键文本目录事件
        $(document).bind("contextmenu", function (event) {
            if (document.all)
                window.event.returnValue = false; // for IE
            else
                event.preventDefault();
        });
        //默认设置菜单隐藏
        $("#chartMenu").hide();
    }

    var data = <?= json_encode($data);?> ;
     //var data = eval('(' + data + ')');

//console.log(<?=$data1?>);
    window.onload = function(){
        $("#chartMenu").hide();
        RegChartRightClickEvents();
        $('#container').highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                },
                events: {
                    //单击图表隐藏右键菜单
                    click: function () {
                        HideRightMenu();
                    }
                }
            },
            title: {
                text: 'Browser market shares at a specific website, 2014'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        distance: -40,//数据标签距离边缘的距离值 为负数就越靠近饼图中心
                        rotation:0,//数据标签旋转角度
                        enabled: true,
                        format: '{point.name}'
                    }
//                    ,
//                    events: {
//                        click: function(event) {
//                            alert(this.name +' clicked\n'+'highcharts');
//                        }
//                    }
                }
                ,
                series: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    events: {
                        click: function(e) {

                           //console.log(e.point);
                            console.log(e.point.id);
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data:[<?=$data1?>]
//                [
//                    ['Firefox',   45.0],
//                    ['IE',       26.8],
//                    {
//                        name: 'Chrome',
//                        y: 12.8,
//                        sliced: true,
//                        selected: true
//                    },
//                    ['Safari',    8.5],
//                    ['Opera',     6.2],
//                    ['Others',   0.7]
//                ]
            }]
        });






//        //三层饼状图
//        var colors = Highcharts.getOptions().colors,
//            categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
//            name = 'Browser brands',
//            data = [{
//                y: 55.11,
//                color: colors[0],
//                drilldown: {
//                    name: 'MSIE versions',
//                    categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
//                    data: [10.85, 7.35, 33.06, 2.81],
//                    color: colors[0]
//                }
//            }, {
//                y: 21.63,
//                color: colors[1],
//                drilldown: {
//                    name: 'Firefox versions',
//                    categories: ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox 4.0'],
//                    data: [0.20, 0.83, 1.58, 13.12, 5.43],
//                    color: colors[1]
//                }
//            }, {
//                y: 11.94,
//                color: colors[2],
//                drilldown: {
//                    name: 'Chrome versions',
//                    categories: ['Chrome 5.0', 'Chrome 6.0', 'Chrome 7.0', 'Chrome 8.0', 'Chrome 9.0',
//                        'Chrome 10.0', 'Chrome 11.0', 'Chrome 12.0'],
//                    data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
//                    color: colors[2]
//                }
//            }, {
//                y: 7.15,
//                color: colors[3],
//                drilldown: {
//                    name: 'Safari versions',
//                    categories: ['Safari 5.0', 'Safari 4.0', 'Safari Win 5.0', 'Safari 4.1', 'Safari/Maxthon',
//                        'Safari 3.1', 'Safari 4.1'],
//                    data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
//                    color: colors[3]
//                }
//            }, {
//                y: 2.14,
//                color: colors[4],
//                drilldown: {
//                    name: 'Opera versions',
//                    categories: ['Opera 9.x', 'Opera 10.x', 'Opera 11.x'],
//                    data: [ 0.12, 0.37, 1.65],
//                    color: colors[4]
//                }
//            }];
//
//
//        // Build the data arrays
//        var browserData = [];
//        var versionsData = [];
//        for (var i = 0; i < data.length; i++) {
//
//            // add browser data
//            browserData.push({
//                name: categories[i],
//                y: data[i].y,
//                color: data[i].color
//            });
//
//            // add version data
//            for (var j = 0; j < data[i].drilldown.data.length; j++) {
//                var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
//                versionsData.push({
//                    name: data[i].drilldown.categories[j],
//                    y: data[i].drilldown.data[j],
//                    color: Highcharts.Color(data[i].color).brighten(brightness).get()
//                });
//            }
//        }
//
//        // Create the chart
//        $('#container').highcharts({
//            chart: {
//                type: 'pie'
//            },
//            title: {
//                text: 'Browser market share, April, 2011'
//            },
//            yAxis: {
//                title: {
//                    text: 'Total percent market share'
//                }
//            },
//            plotOptions: {
//                pie: {
//                    shadow: false,
//                    center: ['50%', '50%']
//                }
//            },
//            tooltip: {
//                valueSuffix: '%'
//            },
//            series: [{
//                name: 'Browsers',
//                data: browserData,
//                size: '60%',
//                dataLabels: {
//                    formatter: function() {
//                        return this.y > 5 ? this.point.name : null;
//                    },
//                    color: 'white',
//                    distance: -30
//                }
//            }, {
//                name: 'Versions',
//                data: versionsData,
//                size: '80%',
//                innerSize: '60%',
//                dataLabels: {
//                    formatter: function() {
//                        // display only if larger than 1
//                        return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
//                    }
//                }
//            },
//                {
//                    name: 'Versions',
//                    data: versionsData,
//                    size: '100%',
//                    innerSize: '80%',
//                    dataLabels: {
//                        formatter: function() {
//                            // display only if larger than 1
//                            return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
//                        }
//                    }
//                }]
//        });

    }
</script>