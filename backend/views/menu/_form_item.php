<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;
use Yii\web\View;
use common\library\MyHtml;
/**
 * @var yii\web\View $this
 * @var backend\models\Menu $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false"
			>
			<header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->
					<input class="form-control" type="text">
				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body">

					<?php $form = ActiveForm::begin(); ?>
					<fieldset>

						<?= $form->field($model, 'menu_id')->dropDownList(MyFunc::_map($p_data, 'id', 'description'), ['class'=>'select2']); ?> 

						<?= $form->field($model, 'description')->textInput() ?>

					</fieldset>

					<div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
                    </div>

					<?php ActiveForm::end();?>
				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->

	</article>
	<!-- END COL -->
</div>
</section>

