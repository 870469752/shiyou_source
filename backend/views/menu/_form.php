<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;
use Yii\web\View;
use common\library\MyHtml;
/**
 * @var yii\web\View $this
 * @var backend\models\Menu $model
 * @var yii\widgets\ActiveForm $form
 */
//echo '<pre>';
//print_r($url);
//print_r($symbol);die;
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
            data-widget-deletebutton="false"
            data-widget-editbutton="false"
            data-widget-colorbutton="false"
            data-widget-sortable="false"
            >
            <header>
                <span class="widget-icon">
                    <i class="fa fa-edit"></i>
                </span>
                <h2><?=$model->isNewRecord ? Yii::t('app', 'Create Menu') :  Yii::t('app', 'Update Menu')?></h2>

            </header>

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                    <input class="form-control" type="text">
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <?php $form = ActiveForm::begin(); ?>

                    <fieldset>

                                      <!--原始的顶级菜单 普通菜单 现已隐藏-->
<!--                        <a id = 'top_menu'  style="visibility:hidden" class="btn btn-primary btn-sm" href="javascript:void(0);">顶级菜单</a>-->

<!--                        <a id = 'general_menu'  style="visibility:hidden" class="btn btn-success btn-sm" href="javascript:void(0);">普通菜单</a>-->
<!--                        <HR />-->

                        <?= $form->field($model, 'description')->textInput() ?>

                        <?php if($url == -1){?>
                            <?= $form->field($model, 'leaf_node')->dropDownList(MyFunc::_map($c_data, 'id', 'description'), ['class'=>'select2','multiple' => 'multiple']); ?>
                        <?php }?>
                        <?= $form->field($model, 'parent_id')->dropDownList($a_menu, ['class'=>'select2 select1','style' => 'width:200px;margin-top:20px;']); ?>
                        <span id="wrapper" style="display:block;margin-top:35px;width:100%;">

                        </span>

                        <div style="width:100%;height:24px;"></div>

<!--                        --><?php //if ($url != 1) {?>
                        <?php if (1) {?>
                        <?//图标?>
                        <?php $model->icon = isset($iconer)? $iconer:null;?>
                        <label class = 'control-label'><?=Yii::t('app', 'Icon');?>: </label><?= MyHtml::activeRadioList($model, 'icon', $i_data,['class'=>'radio_icon', 'encode' => 0,
                            'item' => function ($index, $label, $name, $checked, $value){
                                if ($checked) {
                                    return Html::radio($name, ['checked'=>true],['value'=>$value,'label'=>$label]);
                                }
                                return Html::radio($name, $checked,['value'=>$value,'label'=>$label]);
                            }])?>
                        <?php }?>


                    </fieldset>

                        <div class="form-actions">
                            <?= MyHtml::button('取消', ['class' => 'btn btn-default', 'id' => 'abutton']) ?>
                            <?= MyHtml::button($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','type' => 'button','id'=>'_submit'])?>
                        </div>

                    <span id="createnew" value="<?= $model->isNewRecord?'0':'1' ?>"></span>

                    <?php ActiveForm::end();?>
                    
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->
        
    </article>
    <!-- END COL -->
</div>
</section>
<?php
$this->registerJsFile("js/chosen/chosen.jquery.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/chosen/chosen.jquery.min.js", ['backend\assets\AppAsset']);
$this->registerCssFile("css/chosen/chosen.css");
$this->registerCssFile("css/chosen/chosen.min.css");
?>
<script>

    //获取数组长度
    function count(o){
        var t = typeof o;
        if(t == 'string'){
            return o.length;
        }else if(t == 'object'){
            var n = 0;
            for(var i in o){
                n++;
            }
            return n;
        }
        return false;
    }

    //convert json to array
    function json2array(json){
        var temp = [];
        var keys = Object.keys(json);
        keys.forEach(function(key){
            temp.push([key,json[key]]);
        });
        return temp;

    }

    //判断是否有子节点
    function hasleaf(dataArray,id){
        var arr = [];
        for(var i= 0,a;a=dataArray[i++];){
            if(id == a[0]){
                return true;
                break;
            }
        }
        return false;

    }

    //得到原始菜单列表数组（已倒序）
    function getOriginalArrayReverse(data,id){
        //将json格式转换为数组格式
        var dataArray = json2array(data);
        var pid = getParentId(data,id);
        var originalArray = [];
        var num = 0;
        for(var i in dataArray){
            var second = dataArray[i];
            for(var j in second){
                if(j == 1){
                    var third = second[1];
                    for(var m in third){
                        var parent_id = third[m]['parent_id'];
                        var idx = third[m]['idx'];
                        var idd = third[m]['id'];
                        if(id == parent_id){
                            originalArray[idd] = idx;
                        }
                    }
                }
            }
        }
        return originalArray;
    }

    //get the originial data to use to generate the new sorted array
    function getOriginalArray(data,id){
        //将json格式转换为数组格式
        var dataArray = json2array(data);
        var pid = getParentId(data,id)
        var originalArray = [];
        var num = 0;
        for(var i in dataArray){
            var second = dataArray[i];
            for(var j in second){
                if(j == 1){
                    var third = second[1];
                    for(var m in third){
                        var parent_id = third[m]['parent_id'];
                        var idx = third[m]['idx'];
                        var idd = third[m]['id'];
                        if(pid == parent_id){
                            originalArray[idx] = idd;
                        }
                    }
                }
            }
        }
        return originalArray;
    }

    //获取depth值
    function getDepth(data,id){

        //将json格式转换为数组格式
        var dataArray = json2array(data);
        var depth = null;
        var num = 0;
        for(var i in dataArray){
            var second = dataArray[i];
            for(var j in second){
                if(j == 1){
                    var third = second[1];
                    for(var m in third){
                        var parent_id = third[m]['parent_id'];
                        var depth_value = third[m]['depth'];
                        var idx = third[m]['idx'];
                        var idd = third[m]['id'];
                        if(id == idd){
                            return depth = depth_value;
                        }
                    }
                }
            }
        }
        return depth;
    }

    //get the parent_id
    function getParentId(data,id){
        //将json格式转换为数组格式
        var dataArray = json2array(data);
        var parentId = null;
        var num = 0;
        for(var i in dataArray){
            var second = dataArray[i];
            for(var j in second){
                if(j == 1){
                    var third = second[1];
                    for(var m in third){
                        var parent_id = third[m]['parent_id'];
                        var idx = third[m]['idx'];
                        var idd = third[m]['id'];
                        if(id == idd){
                            return parentId = parent_id
                        }
                    }
                }
            }
        }
        return parentId;
    }

    //生成子模块数组
    function genSubMenu(data,id){

        //将json格式转换为数组格式
        var dataArray = json2array(data);
        var submenu = [];
        var num = 0;
        for(var i in dataArray){
            var second = dataArray[i];
            for(var j in second){
                if(j == 1){
                    var third = second[1];
                    for(var m in third){
                        var parent_id = third[m]['parent_id'];
                        var idx = third[m]['idx'];
                        var idd = third[m]['id'];
                        var desc = third[m]['description'];
                        if(id == parent_id){//有子菜单
                            submenu[idd] = desc;
                        }
                    }
                }
            }
        }
        return submenu;
    }

    /**
     * @description 根据id值，判断显示何种类型节点
     * @params (numberic) id 节点ID号.如果未定义则为新建节点，若有值则为更新节点
     * @params (json) data 要处理的节点数据
     * @auther pzzrudlf  pzzrudlf@gmail.com
     */
    function showMenu(data,_parent_id,id){
             if(id == -1){
                 id =0;
             }
            //返回自己的父id
            var pid = getParentId(data, id);
            //获取id下的子菜单，如果没有则为空
            var submenu = genSubMenu(data, _parent_id);
            //获取原有排序顺序的菜单数组，相同父id下，按idx值，从小到大排序[id=>idx]
            var originial_data_reverse = getOriginalArrayReverse(data, _parent_id);
            var size = count(submenu);
            size++;

            //显示菜单
            var selecters = '<select id="Menu[id]" data-placeholder="选择一个位置" class="menu-id" name="Menu[id]" style="width:98%;">';
            var nn = '';
            var last = 0;
            for (var i in submenu) {

                if (id == i) {
                    selecters += '<option selected value="' + i + '" posi="' + originial_data_reverse[i] + '" >' + submenu[i] + '之前</option>';
                } else {
                    selecters += '<option value="' + i + '" posi="' + originial_data_reverse[i] + '" >' + submenu[i] + '之前</option>';
                }

                nn = submenu[i];
                last = originial_data_reverse[i];

            }
            last++;
            selecters += '<option value="" posi="last" >放到最后</option>';
            selecters += '<select>';

            $("#wrapper").empty().append(selecters);
//            $(".custom-style").chosen({});
            $(".menu-id").on('change', function () {
                var posi = $(".menu-id option:selected").attr('posi');
                var value = $(".menu-id option:selected").val();
                $("#_posi_data").val(posi);
            });
    }

    function clickTest()
    {
        $(".field-menu-parent_id").show();
        $("#menu-parent_id").val(<?=$model->parent_id?>);
        //删除 添加的 顶级菜单option
        $("#_top_").remove();
        $('#_menu_type').val(2);
    }

    window.onload = function (){
        clickTest();
        //获取 父级id

        var $parent_id = <?=isset($model->parent_id)? $model->parent_id : -1?>;
        var $_type = <?=isset($_GET['type'])?$_GET['type'] : 3?>;
        var a_data = <?=isset($a_data)?$a_data:''?>;
        var id = <?=isset($id)?$id:'undefined'?>;
        var parent_id = <?=isset($parent_id)?$parent_id:'undefined'?>;
        var depth = <?=isset($depth)?$depth:'undefined'?>;
        var url =<?=isset($url)?$url:"undefined"?>;

        showMenu(a_data,parent_id,id);
        $("#menu-parent_id").change(function(){
            var parent_id =  $('select[name="Menu[parent_id]"] option:selected').val();
            showMenu(a_data,parent_id,id);
        });

        //点击修改按钮触发ajax方法，用来向php控制器传递js数组。
        $("#_submit").click(function(){
                console.log('url=>'+url);
                var _description = $("#menu-description").val();
                var _icon = $('input:radio[name="Menu[icon]"]:checked').val();
                var _parent_id = $('select[name="Menu[parent_id]"] option:selected').val();
                var _posi_data = $('select[class="menu-id"] option:selected').attr('posi');
                var _menu_type = $("#_menu_type").val();
                var _id = id;
                var _self_pid = parent_id;
                var originial_data_reverse = getOriginalArrayReverse(a_data, _parent_id);

                var _symbol;
                var modules_id = [];
                $("#menu-leaf_node option:selected").each(function () {
                    modules_id.push(this.value);
                });

                if($("#createnew").attr('value') == 1){
                    _symbol = 1;
                    if( url == -1 || url == undefined){//修改菜单
                        var URL = "/menu/update_menu?id="+id;
                    }
                    if(url == 1){//修改模块
                        var URL = "/menu/update_menu?id="+id+"&url=1";
                        _icon = '';
                    }

                }

                if($("#createnew").attr('value') == 0){//新建菜单
                    _symbol = 0;
                    URL = "/menu/create";
                }

                if( _icon == undefined) {
                    if(url != 1){
                        alert('请勾选图标样式');
                        return false;
                    }
                }
                    $.ajax({
                        type: "post",
                        url: URL,
                        data: {
                            id: _id,
                            description: _description,
                            icon: _icon,
                            parent_id: _parent_id,
                            posi_data: _posi_data,
                            'originial_data[]': originial_data_reverse,
                            'modules_id': modules_id,
                            'symbol': _symbol,
                            'url':url
                        },
                        success: function (data) {
                            console.log(data);
                            if (data == 'desc_null') {
                                alert('请填写菜单名字');
                                return false;
                            }
                            if (data == 'icon_null') {
                                alert('请勾选图标样式');
                                return false;
                            }

                            if (data == 'posi_data') {
                                alert('请勾选位置');
                                return false;
                            }

                            if (data == 'modules_id_null') {
                                alert('请选择功能模块');
                                return false;
                            }
                                window.location.href = "/menu";
                        }
                    });


        });

        /**
         * 将 菜单 分为 顶级菜单 和 普通菜单
         * 顶级菜单： 只用添加 顶级菜单的 名字 和 叶子节点
         * 普通菜单： 除了 顶级菜单所需参数 还需要 选择 父级菜单
         */
        $("#top_menu").click(function (){
            $(".field-menu-parent_id").hide();
            // 在下拉菜单中添加 顶级菜单的选项 然后 让其 选中
            $("#menu-parent_id").append("<option id = '_top_' value = 0></option>");
            $("#menu-parent_id").val(0);
            $('#_menu_type').val(1);
        });
        $("#abutton").click(function(){
                  window.location.href='index';
        });
        $("#general_menu").click(function (){
            $(".field-menu-parent_id").show();
            $("#menu-parent_id").val(<?=$model->parent_id?>);
            //删除 添加的 顶级菜单option
            $("#_top_").remove();
            $('#_menu_type').val(2);
        })

        //利用父级id 判断 页面展现形式
        if($parent_id){
            //如果存在 上级id,并且上级id部位0那么 就出发 普通菜单
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }
        else{
           //alert($parent_id);
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }

        //利用传递的参数判但当前用户上次创建菜单是 顶级菜单还是 普通菜单
        if($_type == 1){
            //console.log(1);
            $("#top_menu").trigger('click');
            $("#top_menu").addClass('active');
        }else if($_type == 2){
            //console.log(2);die;
            $("#general_menu").trigger('click');
            $("#general_menu").addClass('active');
        }
    }
</script>


