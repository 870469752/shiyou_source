<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ModuleRole $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Module Role',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'module_info' => $module_info,
        'action_info' => $action_info,
        'action_data' => $action_data,
    ]) ?>
