<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\ModuleRole $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">
	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>


                        <!--权限checkbox展示    start -->
                            <table class = "table table-bordered">
                                <thead><tr><th>模块</th><th>操作权限</th></tr> </thead>
                                <?php foreach($action_info as $module => $value):?>
                                    <?= Html::hiddenInput('ModuleRole[action_module]['.$module.']['.$module.']', $module_info[$module]['id'])?>
                                    <tbody><tr>
                                        <td>
                                            <?= Html::checkbox('ModuleRole[action_module]['.$module.'][]',
                                                isset($action_data[$module_info[$module]['id']]) ?  MyFunc::stringFindArray_value(json_decode($action_data[$module_info[$module]['id']], true)['data'], 'all') : false,
                                                ['label' =>$module_info[$module]['additional'],
                                                 'value' => 'all', 'class' => "_module", 'container'=>''])?>
                                        </td>
                                        <!--需要将数组中的 action_info 解析-->
                                        <?php $value = json_decode($value, true);?>
                                        <td>
                                            <?php foreach($value['data'][0] as $action => $des):?>
                                                <?= Html::checkbox('ModuleRole[action_module]['.$module.'][]',
                                                    isset($action_data[$module_info[$module]['id']]) ?  MyFunc::stringFindArray_value(json_decode($action_data[$module_info[$module]['id']], true)['data'], $action) : false,
                                                    ['label' => mb_strlen($des, 'utf-8') > 7 ? mb_substr($des,0,6,'utf-8').'..' : $des,
                                                     'class' => '_action', 'value' => $action, 'container'=>'',
                                                    'labelOptions' => ['rel'=>"tooltip",  'data-placement'=>"top", 'data-original-title'=>"<h7><em>说明：</em> $des</h7>", 'data-html' =>"true"]])?>
                                            <?php endforeach;?>
                                        </td>
                                    </tr></tbody>
                                <?php endforeach;?>
                            </table>
                        <!--权限checkbox展示    end -->

                        <?= Html::hiddenInput('ModuleRole[role_id]', $model->role_id) ?>

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->
		
				</div>
				<!-- end widget div -->
		
			</div>
			<!-- end widget -->
		
		</article>
		<!-- END COL -->
	</div>
</section>

<script>
    window.onload = function(){
        //模块权限 全选.
        $('._module').click(function(){
            $(this).parent().parent().parent().find('._action').prop('checked', $(this).prop('checked'));
        })

        //如果模块下所有权限都勾选了 那么次模块将自动勾选
        $('._action').click(function(){
            var $module = $(this).parent().parent().parent().find('._module');
            var $action = $(this).parent().parent().find('._action');
            var $is_all = true;
            $action.each(function(){
                if($(this).prop('checked') == false){
                    $is_all = false;
                    return false;
                }
            })
            $is_all ? $module.prop('checked', true) : $module.prop('checked', false);
        })
        //alert($('.tr0').childElementCount());
        //权限未选择  为灰色

    }
</script>