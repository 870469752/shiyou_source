<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\ModuleRole $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Module Role',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'module_info' => $module_info,
        'action_info' => $action_info
    ]) ?>
