<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use backend\models\ScheduleTaskLog;
use yii\helpers\Url;

AppAsset::register ( $this );
//轮循数据
$page_loop_data=Yii::$app->cache->get('page_loop_data');
$times=isset($page_loop_data['time'])?$page_loop_data['time']:null;
//redis读取本天计划任务的开始结束时间
$now_task=Yii::$app->cache->get("NowTask");
$today_task=Yii::$app->cache->get("TodayTask");

//$task_update=Yii::$app->cache->set("aaa",111);
$task_update=Yii::$app->redis->get("TASK_UPDATE");

//$today_task=[];
if(empty($today_task) ||$task_update=="1") {
    ScheduleTaskLog::initTodayTaskData();
    $today_task=Yii::$app->cache->get("TodayTask");
}
$user_id=Yii::$app->user->id;
$task_info=Yii::$app->redis->get("TASK_INFO:".$user_id);
$task_info=msgpack_unpack($task_info);
//echo '<pre>';
//print_r($task_info);
//die;
$num=isset($task_info['task_count'])?$task_info['task_count']:0;
//echo $num;
//die;
/*echo  2222;
die;*/
?>
<?php $this->beginPage()?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?php echo $this->render('public/header', [
            'title' => Html::encode ( $this->title?$this->title:'Energy Management System' ),
            'charset' => Yii::$app->charset
        ]);
        ?>
        <?php $this->head()?>
    </head>

    <!-- 主题更换只需修改smart-style-0末尾的数字即可，分别有0,1,2,3。后面样式主要为了修正smartadmin在移动端的BUG -->
    <body class="hidden-menu smart-style-0 mobile-view-activated">
    <?php $this->beginBody()?>

    <?php echo $this->render('public/top');?>

    <?php echo $this->render('public/nav');?>

    <div id="main" role="main">

        <?php echo $this->render('public/ribbon');?>

        <div id="content">
            <?= $content?>

        </div>
    </div>
    <!-- 任务提示框   -->
    <div id="smallbox" class="SmallBox animated fadeInRight fast" style="display: none;;top: 20px; background-color: rgb(41, 97, 145);">
        <div class="foto">
            <i class="fa fa-bell swing animated"></i>
        </div>
        <div class="textoFoto">
            <span></span>
            <p id="smallbox_p">有新的巡检任务 </p>
            <p class="text-align-right" >
                <a id="smallbox_yes" href="javascript:void(0);" class="btn btn-primary btn-sm">确认</a>
                <a id="smallbox_show" href="javascript:void(0);" class="btn btn-danger btn-sm">查看</a></p>
            <p></p>
        </div>
        <div class="miniIcono"></div>
    </div>

    <?php echo $this->render('public/footer');?>

    <?php $this->endBody()?>

    <!--[if IE 8]>
    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
    <![endif]-->

    </body>
    </html>
<?php $this->endPage() ?>
<script>
    //当前任务记录数据
    var now_task=<?=json_encode($now_task)?>;
    var today_task=<?=json_encode($today_task)?>;
    var page_time=new Date();
    var user_id=<?=$user_id?>;
    $(document).ready(function () {
        //第三位参数为类型  判断是 key还是value
        function inArray(array,type,element){
            for(var i in array){
                if(type=='value') {
                    if (array[i] == element)
                        return 1;
                }
                if(type=='key') {
                    if (i == element)
                        return 1;
                }
            }
            return 0;
        }

        $("#_a_b").html(<?=$num?>);
        var socket = io('http://11.29.1.17:9090');
        socket.on('connection', function () {
            console.log('connection setup for socket.io')
        });
        var user_name = '<?=Yii::$app->user->identity->username;?>';
        var socket_name = 'Alarm_All';
        if(user_name.indexOf('电力')!=-1){
            socket_name = 'Alarm_Ele';
        } if(user_name.indexOf('安全')!=-1){
            socket_name = 'Alarm_Safe';
        } if(user_name.indexOf('设施')!=-1){
            socket_name = 'Alarm_Equi';
        } if(user_name.indexOf('消防')!=-1){
            socket_name = 'Alarm_Fire';
        }
        socket.on(socket_name+"_Trigger", function (msg) {
            var alarm_info=eval('('+msg['value'].replace(/': u'/g,"': '")+')');
            if(alarm_info.notice_setting=='True'){
                getAlarmLeveInfo(alarm_info);
            }
            if(window.location.pathname+window.location.search=='/alarm-log/default/logs?type=1'){
                getOneAlarmInfo(alarm_info);
            }
        });
        socket.on(socket_name+"_Untrigger", function (msg) {
            var alarm_info=eval('('+msg['value'].replace(/': u'/g,"': '")+')');
            $(".bigboxnumber").each(function(){
                if($(this).text()==alarm_info.id) {
                    judge = 1;
                    var id = parseInt(($(this).parent().attr("id")).replace(/[^0-9]/ig,""));
                    $("#botClose"+id).trigger("click");
                    return false;
                }
            })
            if(window.location.pathname+window.location.search=='/alarm-log/default/logs?type=1'){
                resetAlarm(alarm_info);
            }
        });
        //监听任务 数据更新
        socket.on('AllTasksNow', function (msg) {
            //用户任务信息
            msg=eval('('+msg+')');
            //得到当前用户的任务数量
            var task_num=msg['all_tasks_now'][user_id].length;
            $("#_a_b").html(task_num);
            var start_task=msg['start_schedule_task'];
            var finish_task=msg['finish_schedule_task'];
            var end_task=msg['end_schedule_task'];

            var start_num=0;
            var finish_num=0;
            for(var i in start_task){
                start_num++;
            }
            console.log("start_num"+start_num);
            for(var j in finish_task){
                finish_num++;
            }
            console.log("finish_num"+finish_num);
                   if(start_num!=0){
                       console.log("任务开始");
                       var in_Array=inArray(start_task['users'],'value',user_id);
                       console.log(in_Array);
                       if(in_Array){
                       $("#smallbox").css("display","");
                       $("#smallbox_p").text("有新的巡检任务"+start_task['task_name']);
                       $("#smallbox_show").css("display","");
                       setTimeout('$("#smallbox").css("display","none")',10000);
                       }
                }
            if(finish_num!=0){
                console.log("任务结束");
                var in_Array=inArray(finish_task['users'],'value',user_id);
                console.log(in_Array);
                if(in_Array){
                $("#smallbox_show").css("display","none");
                $("#smallbox_p").text("任务完成"+finish_task['task_name']);
                $("#smallbox").css("display","");
                setTimeout('$("#smallbox").css("display","none")',10000);
                }
            }

            //更新当前任务列表数据
//        $.ajax({
//                type: "POST",
//                url:"/page-loop/crud/update-task-info",
//                data: {
//
//                },
//                success: function (msg) {
//                    //msg为全局的 任务信息
//                    //更新这个信息
//                    today_task=eval('('+msg+')');
//                }
//            });
        });
    });
    function changeWorkModel(id){
        switch(id)
        {
            case 53:
                $("#work_model").html("参观显示模式");
                break;
            case 54:
                $("#work_model").html("消防演练模式");
                break;
            case 55:
                $("#work_model").html("重大事件模式");
                break;
            default:
                $("#work_model").html("日常显示模式");
        }
        $.ajax({
            url: '/route/command-log?val='+id,
            async: true,
            success: function(data)
            {
                return false;
            },
            complete: function (XHR, TS) { XHR = null }
        })

    }



    $("#smallbox_yes").click(function () {
        $("#smallbox").css("display","none");
    });
    $("#smallbox_show").click(function () {
        window.location.href="/subsystem_manage/crud/electrinic-index";
    });



</script>