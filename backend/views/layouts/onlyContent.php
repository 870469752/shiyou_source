<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?php echo $this->render('public/header', [
            'title' => Html::encode($this->title ? $this->title : 'Energy Management System'),
            'charset' => Yii::$app->charset
        ]);
        ?>
        <?php $this->head() ?>
    </head>

    <!-- 主题更换只需修改smart-style-0末尾的数字即可，分别有0,1,2,3。后面样式主要为了修正smartadmin在移动端的BUG -->
    <body>
        <?php $this->beginBody() ?>

        <div>
            <?= $content ?>
        </div>


        <?php $this->endBody() ?>

        <!--[if IE 8]>
        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
        <![endif]-->
    </body>
</html>
<?php $this->endPage() ?>