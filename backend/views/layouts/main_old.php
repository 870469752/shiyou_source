<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register ( $this );
?>
<?php $this->beginPage()?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?php echo $this->render('public/header', [
            'title' => Html::encode ( $this->title?$this->title:'Energy Management System' ),
            'charset' => Yii::$app->charset
        ]);
        ?>
        <?php $this->head()?>
    </head>

    <!-- 主题更换只需修改smart-style-0末尾的数字即可，分别有0,1,2,3。后面样式主要为了修正smartadmin在移动端的BUG -->
    <body class="hidden-menu smart-style-0 mobile-view-activated">
    <?php $this->beginBody()?>

    <?php echo $this->render('public/top');?>

    <?php echo $this->render('public/nav');?>

    <div id="main" role="main">

        <?php echo $this->render('public/ribbon');?>

        <div id="content">
            <?= $content?>

        </div>
    </div>

    <?php echo $this->render('public/footer');?>

    <?php $this->endBody()?>

    <!--[if IE 8]>
    <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
    <![endif]-->

    </body>
    </html>
<?php $this->endPage() ?>
<script>

    $(document).ready(function () {
        var socket = io('http://11.29.1.17:9090');
        socket.on('connection', function () {
            console.log('connection setup for socket.io')
        });
        var send_message={
            socket_name:'Alarm',
            Points:[],
            Events:[]
        };
        socket.emit('socket_name',send_message);
        socket.on('Alarm', function (msg) {
            console.log(msg);
            var channel=msg['channel'];
            var alarm_id=msg['value'];
            if(channel.indexOf('Alarm')!=-1) {
                getNoRecord_new(alarm_id);
            }
            if(window.location.pathname+window.location.search=='/alarm-log/default/logs?type=1'){
                getRealTimeAlarmInfo();
            }
        });
    });
</script>