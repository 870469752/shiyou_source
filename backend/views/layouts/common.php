<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register ( $this );
?>

<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<?php echo $this->render ( 'public/header', [ 
			'title' => Html::encode ( $this->title?$this->title:'Energy Management System' ),
			'charset' => Yii::$app->charset 
		] );
		?>
	    <?php $this->head()?>
	</head>
    <style>
        html{ background: url('http://<?=$_SERVER['HTTP_HOST']?>/img/block/new/2.jpg') no-repeat ; background-size: 100% 100%;}
        body{background:#0f0; background-color:transparent;}
    </style>
	<body>
		<?php $this->beginBody()?>
		<div id="main" role="main">
			<?= $content?>
		</div>

		<?php echo $this->render('public/footer');?>

		<?php $this->endBody()?>

		<!--[if IE 8]>
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->

	</body>
</html>
<?php $this->endPage() ?>