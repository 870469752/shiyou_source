<!-- PAGE FOOTER -->
<div class="page-footer">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<span class="txt-color-white"></span>
		</div>

		<div class="col-xs-6 col-sm-6 text-right hidden-xs">
			<div class="txt-color-white inline-block">
				<i class="txt-color-blueLight hidden-mobile">
					<strong>系统集成管理平台 © 2.0 <?= Yii::t('app', 'Powered by') ?> GC</strong>
				</i>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE FOOTER -->