<?php

use yii\helpers\Url;

?>
<!-- RIBBON -->
<div id="ribbon">

    <div class="row padding-top-10">
        <!-- 如果是错误页面就没有语言切换 -->
        <div class="col-xs-6 hidden-mobile" id="content_breadcrumbs" style="color: #aaa"></div>

        <div class="col-mobile-12">

            <div class="pull-right">
                <span class="txt-color-yellow border-left">&nbsp;&nbsp;<i class="fa fa-sun-o fa-fw"></i> <?= @$GLOBALS['temperature'] ?> °C&nbsp;&nbsp;</span>
                <span class="txt-color-blueLight border-left">&nbsp;&nbsp;<i class="fa fa-tint fa-fw"></i> <?= @$GLOBALS['humidity'] ?> %&nbsp;&nbsp;</span>
            </div>
            <?php if (Url::toRoute('') != Url::toRoute('/site/error')): ?>
            <div class="pull-right" style="margin-right: 15px;">
                <ul class="ribbon-dropdown-list" style="position: relative;">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/blank.gif"
                                 class="flag flag-<?= Yii::t('app', 'us') ?>">
                            <span><?= Yii::t('app', 'English') ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" style="text-align: center">
                            <li <?php if (\Yii::$app->language == 'en-us'): ?> class="active"
                            <?php endif ?>>
                                <a href="<?= Url::toRoute(array_merge([''], Yii::$app->request->get(), ['lang' => 'en-us'])) ?>">
                                    <img src="/img/blank.gif" class="flag flag-us" alt="English">
                                    English
                                </a>
                            </li>
                            <li <?php if (\Yii::$app->language == 'zh-cn'): ?> class="active"
                            <?php endif ?>>
                                <a href="<?= Url::toRoute(array_merge([''], Yii::$app->request->get(), ['lang' => 'zh-cn'])) ?>">
                                    <img src="/img/blank.gif" class="flag flag-cn" alt="中文">
                                    中文
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <?php endif ?>
        </div>
    </div>



    
    <?php 
    	$this->registerCssFile ( "/css/demo.min.css" );
    	$this->registerJsFile ( "/js/demo.min.js", ['yii\web\JqueryAsset'] );
    ?>
    <!--<div class="demo">
		<span id="demo-setting">
			<i class="fa fa-cog txt-color-blueDark"></i>
		</span>

		<form>
			<legend class="no-padding margin-bottom-10"><?/*= YII::t('app', 'Layout Options') */?></legend>
			<section>
				<label>
					<input name="subscription" id="smart-fixed-header" type="checkbox"
						class="checkbox style-0">
					<span><?/*= YII::t('app', 'Fixed Header') */?></span>
				</label>
				<label>
					<input type="checkbox" name="terms" id="smart-fixed-navigation"
						class="checkbox style-0">
					<span><?/*= YII::t('app', 'Fixed Menu') */?></span>
				</label>
				<label>
					<input type="checkbox" name="terms" id="smart-fixed-ribbon"
						class="checkbox style-0">
					<span><?/*= YII::t('app', 'Fixed Ribbon') */?></span>
				</label>
				<label>
					<input type="checkbox" name="terms" id="smart-fixed-footer"
						class="checkbox style-0">
					<span><?/*= YII::t('app', 'Fixed Footer') */?></span>
				</label>
				<label>
					<input type="checkbox" name="terms" id="smart-fixed-container"
						class="checkbox style-0">
					<span><?/*= YII::t('app', 'Inside Container') */?></span>
				</label>
				<label style="display: block;">
					<input type="checkbox" id="smart-topmenu" class="checkbox style-0">
					<span><?/*= YII::t('app', 'Menu on top') */?></span>
				</label>
				<span id="smart-bgimages">
					<h6 class='margin-top-10 semi-bold'><?/*= YII::t('app', 'Background') */?></h6>
					<img src='/img/pattern/graphy-xs.png'
						data-htmlbg-url='/img/pattern/graphy.png' width='22' height='22'
						class='margin-right-5 bordered cursor-pointer'>
					<img src='/img/pattern/tileable_wood_texture-xs.png' width='22'
						height='22'
						data-htmlbg-url='/img/pattern/tileable_wood_texture.png'
						class='margin-right-5 bordered cursor-pointer'>
					<img src='/img/pattern/sneaker_mesh_fabric-xs.png' width='22'
						height='22' data-htmlbg-url='/img/pattern/sneaker_mesh_fabric.png'
						class='margin-right-5 bordered cursor-pointer'>
					<img src='/img/pattern/nistri-xs.png'
						data-htmlbg-url='/img/pattern/nistri.png' width='22' height='22'
						class='margin-right-5 bordered cursor-pointer'>
					<img src='/img/pattern/paper-xs.png'
						data-htmlbg-url='/img/pattern/paper.png' width='22' height='22'
						class='bordered cursor-pointer'>
				</span>
			</section>
			<h6 class="margin-top-10 semi-bold margin-bottom-5"><?/*= YII::t('app', 'Skins') */?></h6>
			<section id="smart-styles">
				<a href="javascript:void(0);" id="smart-style-0"
					class="btn btn-block btn-xs txt-color-white margin-right-5"
					style="background-color: #4E463F;">
					<i class="fa fa-check fa-fw" id="skin-checked"></i><?/*= YII::t('app', 'Default Skin')*/?>
                </a>
				<a href="javascript:void(0);" id="smart-style-1"
					class="btn btn-block btn-xs txt-color-white"
					style="background: #3A4558;">
                    <?/*= YII::t('app', 'Dark Elegance')*/?>
                </a>
				<a href="javascript:void(0);" id="smart-style-2"
					class="btn btn-xs btn-block txt-color-darken margin-top-5"
					style="background: #fff;">
                    <?/*= YII::t('app', 'Ultra Light')*/?>
                </a>
				<a href="javascript:void(0);" id="smart-style-3"
					class="btn btn-xs btn-block txt-color-white margin-top-5"
					style="background: #f78c40">
                    <?/*= YII::t('app', 'Google Skin')*/?>
                </a>
			</section>
		</form>
	</div>-->
</div>
<!-- END RIBBON -->
<?php
$home_lang = YII::t('app', 'Home');

$js_content = <<<JAVASCRIPT
	//面包屑
	var nav_active = $('nav [class="active"] a:first');
	var tmp = nav_active.find("span").text();
	var class_mane = nav_active.find("i").attr('class');
	var nav_str = tmp;
	while(tmp){
		nav_active = nav_active.parent().parent();
		tmp = nav_active.prev('a').find("span").text();
		if(nav_active.prev('a').find("i").attr('class')){
			class_mane = nav_active.prev('a').find("i").attr('class');
		}
		if(tmp){
			var parent_menu = tmp;
			nav_str = tmp+' > '+nav_str;
		}
	}
	nav_str = nav_str.replace(parent_menu, '');
	if(parent_menu){
		$('#content_breadcrumbs').html('<i class="'+class_mane+'"></i>'+parent_menu+' <span>'+nav_str+'</span>');
	} else if(nav_str) {
        $('#content_breadcrumbs').html('<i class="'+class_mane+'"></i>'+nav_str);
	}
	else{
		$('#content_breadcrumbs').html('<i class="fa-fw fa fa-home"></i>{$home_lang}');
	}

JAVASCRIPT;

$this->registerJs ( $js_content);
?>