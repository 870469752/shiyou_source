<?php
use common\library\MyFunc;
use yii\helpers\Url;


$idd = md5(Yii::$app->user->id);

$nav = Yii::$app->cache->get ($idd);
//$nav = Yii::$app->cache->get ( 'nav' );
$file_name = Yii::$app->user->identity->head_portrait;
$file_url =  $file_name ? '/'.$file_name : '/img/avatars/male.png';

?>
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
		<span>
			<!-- User image size is adjusted inside CSS, it should stay as is -->
			<a href="javascript:void(0);">
                <img src="<?=$file_url;?>" alt="me"  />
                <span><?= Yii::$app->user->identity->username ?></span>
            </a>
		</span>
    </div>

    <nav>

        <?php
        echo MyFunc::process_sub_nav($nav);
        ?>

    </nav>
		<span class="minifyme" data-action="minifyMenu">
			<i class="fa fa-arrow-circle-left hit"></i>
		</span>

</aside>
<!-- END NAVIGATION -->