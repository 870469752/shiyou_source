<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\library\MyFunc;
AppAsset::register ( $this );
    $obj_schema = isset($searchModel->TableSchema->columns)?$searchModel->TableSchema->columns:'';
    $columns = isset(Yii::$app->params['columns'])?Yii::$app->params['columns']:'';
    $obj_attr = $searchModel->attributeLabels();
    $obj_l18n = isset($obj_attr)?$obj_attr:'';
?>

<!-- start search -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form" id = '_cc' role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel"><?=YII::t('app', 'Search');?></h4>
                </div>
                <div class="modal-body" id = '_content' style = 'max-height: 400px;overflow: scroll'>
                    <!--  添加条件   -->
                    <div class="form-group">
                        <div class="row" >
                            <div class="col-md-4">
                                <?php if($columns && $obj_schema && $obj_attr):?>
                                    <select name = 'columns[]' class = '_columns form-control' placeholder = '选择字段'>
                                    <?php foreach($columns as $key => $value):?>
                                        <?php if(isset($value['attribute'])):?>
                                            <option value = '<?=$value['attribute'];?>'
                                                    data-dbtype = '<?=$obj_schema[$value['attribute']]->dbType;?>'>
                                                        <?=$obj_l18n[$value['attribute']];?>
                                            </option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                    </select>
                                <?php endif;?>
                            </div>
                            <!-- 通过 table_search.js 插入的Dom-->
                            <div class = 'col-md-7 _condition'></div>

                            <div class="col-md-1 _del" onclick="$(this).parent().parent().remove()" style = 'display:none'>
                                <span class="close" style="font-size:32px">×</span>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <!-- 条件 结束 -->

                </div>
                <div class="modal-footer">
                    <span id = 'c_mes' class = 'pull-left' style = 'color:red'></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?=YII::t('app', 'Cancel');?>
                    </button>
                    <button type="button" class="btn btn-success" id="c_add">
                        <?=YII::t('app', 'Add condition');?>
                    </button>
                    <button type="submit" class="btn btn-primary" id="c_sub">
                        <?=YII::t('app', 'Search');?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
$this->registerCssFile("css/datetimepicker/bootstrap-datetimepicker.min.css", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-timepicker/bootstrap-datetimepicker.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/my_js/table_search.js", ['backend\assets\AppAsset']);
?>
<!-- end search -->
