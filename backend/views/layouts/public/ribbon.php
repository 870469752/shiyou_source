<?php

use yii\helpers\Url;

?>
<!-- RIBBON -->
<div id="ribbon">

    <div class="row padding-top-10">
        <!-- 如果是错误页面就没有语言切换 -->
        <div class="col-xs-6 hidden-mobile" id="content_breadcrumbs" style="color: #aaa"></div>

        <div class="col-mobile-12">

            <div class="pull-right">
                <span class="txt-color-yellow border-left">&nbsp;&nbsp;<i class="fa fa-sun-o fa-fw"></i> <?= @$GLOBALS['temperature'] ?> °C&nbsp;&nbsp;</span>
                <span class="txt-color-blueLight border-left">&nbsp;&nbsp;<i class="fa fa-tint fa-fw"></i> <?= @$GLOBALS['humidity'] ?> %&nbsp;&nbsp;</span>
            </div>
            <?php if (Url::toRoute('') != Url::toRoute('/site/error')): ?>
            <div class="pull-right" style="margin-right: 15px;">
                <ul class="ribbon-dropdown-list" style="position: relative;">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="fa fa-lg fa-fw fa-cogs"></span>
                            <span id="work_model">
                            <?php
                                $moshi_id = \Yii::$app->redis->get("M_S");
                            switch($moshi_id){
                                case 53:
                                    echo "参观显示模式";
                                    break;
                                case 54:
                                    echo "消防演练模式";
                                    break;
                                case 55:
                                    echo "重大事件模式";
                                    break;
                                default :
                                    echo "日常显示模式";
                                    break;
                            }
                            ?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="m-56" onclick="changeWorkModel(56)" style="height: 25px">
                                <a><span class="glyphicon glyphicon-circle-arrow-right"></span>
                                    &nbsp;&nbsp;日常显示模式</a>
                            </li>
                            <li id="m-54" onclick="changeWorkModel(54)" style="height: 25px">
                                <a><span class="glyphicon glyphicon-circle-arrow-right"></span>
                                    &nbsp;&nbsp;消防演练模式</a>
                            </li>
                            <li id="m-53" onclick="changeWorkModel(53)"  style="height: 25px">
                                <a><span class="glyphicon glyphicon-circle-arrow-right"></span>
                                    &nbsp;&nbsp;参观显示模式</a>
                            </li>
                            <li id="m-55" onclick="changeWorkModel(55)" style="height: 25px">
                                <a><span class="glyphicon glyphicon-circle-arrow-right"></span>
                                    &nbsp;&nbsp;重大事件模式</a>
                            </li>
                            <li id="page-loop"  style="height: 25px">
                                <a href="/page-loop/crud/show"><span class="glyphicon glyphicon-circle-arrow-right"></span>
                                    &nbsp;&nbsp;轮巡模式</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!--   倒计时           -->
                <div class="pull-right">
                    <div id="countdown" ></div>
                </div>

            <?php endif ?>
        </div>
    </div>
    <?php 
    	$this->registerCssFile ( "/css/demo.min.css" );
    	$this->registerJsFile ( "/js/demo.min.js", ['yii\web\JqueryAsset'] );
    ?>
</div>
<!-- END RIBBON -->
<?php
$home_lang = YII::t('app', 'Home');

$js_content = <<<JAVASCRIPT
	//面包屑
	var nav_active = $('nav [class="active"] a:first');
	var tmp = nav_active.find("span").text();
	var class_mane = nav_active.find("i").attr('class');
	var nav_str = tmp;
	while(tmp){
		nav_active = nav_active.parent().parent();
		tmp = nav_active.prev('a').find("span").text();
		if(nav_active.prev('a').find("i").attr('class')){
			class_mane = nav_active.prev('a').find("i").attr('class');
		}
		if(tmp){
			var parent_menu = tmp;
			nav_str = tmp+' > '+nav_str;
		}
	}
	nav_str = nav_str.replace(parent_menu, '');
	if(parent_menu){
		$('#content_breadcrumbs').html('<i class="'+class_mane+'"></i>'+parent_menu+' <span>'+nav_str+'</span>');
	} else if(nav_str) {
        $('#content_breadcrumbs').html('<i class="'+class_mane+'"></i>'+nav_str);
	}
	else{
		$('#content_breadcrumbs').html('<i class="fa-fw fa fa-home"></i>{$home_lang}');
	}

JAVASCRIPT;

$this->registerJs ( $js_content);
?>