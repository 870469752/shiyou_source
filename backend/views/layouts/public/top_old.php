<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;
use backend\models\AlarmLog;

$config_site = Yii::$app->cache->get('config_site');

//载入数据
$logs = AlarmLog::getLogs();
$num = count(AlarmLog::getLogsAll());
?>
    <!-- HEADER -->
    <header id="header">
	    <a href="/" id="logo" class="pull-left txt-color-blueDark" style="width: auto">
			<img src="/<?= YII::$app->params['uploadPath'] . ArrayHelper::getValue($config_site, 'logo_url') ?>" alt=""/>
            <?= MyFunc::DisposeJSON(ArrayHelper::getValue($config_site, 'name')) ?>
		</a>

        <div id="logo-group" class="pull-left">

            <span id="activity" class="activity-dropdown">
                <i class="fa fa-warning"></i>
                <b class="badge" id = '_a_b'> <?= $num ?> </b>
            </span>

            <div class="ajax-dropdown">

                <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-default active notify">
                        <input type="radio">
                        <?= Yii::t('app', 'Alarm') ?> (<span id="alarm_log_num"><?= count($logs) ?></span>)
                    </label>
                    <label class="btn btn-default notify">
                        <input type="radio">
                        <?= Yii::t('app', 'Warning') ?> (0)
                    </label>
                    <label class="btn btn-default notify">
                        <input type="radio">
                        <?= Yii::t('app', 'Diagnosis') ?> (0)
                    </label>
                </div>

                <!-- notification content -->
                <div class="ajax-notifications custom-scroll">
                    <!--配置报警框内容 -->
                    <!--报警   start-->
                    <div class='content_notif' style="opacity: 0.7;">
                        <ul class="notification-body" id = 'alarm_log'>
                            <?php foreach ($logs as $key => $value): ?>
                                <li data-id = <?=$value['id'];?> >
                                <span class="padding-10 unread">
                                        <span style="width: 55%; float: left; font-size: 11px" ><a href="#"><?= MyFunc::DisposeJSON($value['description']) ?></a></span>
                                        <span style="width: 35%; float: left; font-size: 11px">|<?= date('y-m-d H:i:s',strtotime($value['start_time'])); ?>|</span>
                                        <span style="width: 6%; float: right; font-size: 11px;">
                                            <a href="#" onclick="single_alarm_confirmed(<?=$value['id'];?>)" title="确认报警信息" style="text-align:center;color: rgb(100 , 232, 78)">
                                                <span class="fa fa-check _confirm"></span>
                                            </a>
                                        </span>
                                </span>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                </div>
                <!-- end notification content -->

                <!-- footer: refresh area -->
			<span>

				<?= Yii::t('app', 'Last updated on') ?>: <span id = 'time_tip'><?php date_default_timezone_set('PRC'); echo date('Y-m-d H:i:s')?></span>

				<button id = '_load' type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i>Loading..."
                        class="btn btn-xs btn-default pull-right">
					<i class="fa fa-refresh"></i>
				</button>
                  <!--全部确认按钮-->
                   <a id = 'all_confirm' class="btn btn-info btn-xs pull-right" href="javascript:void(0);">
                       确认全部
                   </a>
			</span>
                <!-- end footer -->

            </div>
        </div>

        <!--<div style="line-height:50px" class="col-md-1 hidden-mobile"><a href="/download/sao" target="_blank">下载APP</a></div>-->
        <!-- pulled right: nav area -->
        <div class="pull-right">

            <!-- collapse menu button -->
            <div id="hide-menu" class="btn-header pull-right">
			<span>
				<a href="javascript:void(0);" title="<?= YII::t('app', 'Menu') ?>"
                   data-action="toggleMenu">
                    <i class="fa fa-reorder"></i>
                </a>
			</span>
            </div>
            <!-- end collapse menu -->
            <div id="close-sound" class="btn-header pull-right">
			<span>
				<a href="#" title="关闭报警声"
                   data-action="">
                    <i class="glyphicon glyphicon-volume-off"></i>
                </a>
			</span>
            </div>

            <!-- #MOBILE -->
            <!-- Top menu profile link : this shows only when top menu is active -->
            <ul id="mobile-profile-img"
                class="header-dropdown-list hidden-xs padding-5">
                <li class="">
                    <a href="#" class="dropdown-toggle no-margin userdropdown"
                       data-toggle="dropdown">
                        <img src="/img/avatars/male.png" alt="John Doe" class="online"/>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="profile.php"
                               class="padding-10 padding-top-0 padding-bottom-0">
                                <i class="fa fa-user fa-fw"></i>
                                <?= YII::t('app', 'Profile') ?>
                            </a>
                        </li>

                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0);"
                               class="padding-10 padding-top-0 padding-bottom-0"
                               data-action="launchFullscreen">
                                <i class="fa fa-arrows-alt fa-fw"></i>
                                <?= YII::t('app', 'Full Screen') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?= Url::toRoute('/site/logout') ?>" data-method="post"
                               class="padding-10 padding-top-0 padding-bottom-0"
                               data-action="userLogout">
                                <i class="fa fa-sign-out fa-fw"></i>
                                <?= YII::t('app', 'Logout') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- logout button -->
            <div id="logout" class="btn-header transparent pull-right">
			<span>
				<a href="<?= Url::toRoute('/site/logout') ?>" data-method="post" title="<?= Yii::t('app', 'Logout') ?>">
                    <i class="fa fa-sign-out"></i>
                </a>
			</span>
            </div>
            <!-- end logout button -->

            <!-- fullscreen button -->
            <div id="fullscreen" class="btn-header transparent pull-right">
			<span>
				<a href="javascript:void(0);" title="<?= YII::t('app', 'Full Screen') ?>"
                   data-action="launchFullscreen">
                    <i class="fa fa-arrows-alt"></i>
                </a>
			</span>
            </div>
            <!-- end fullscreen button -->

            <!-- backgroud sound -->
            <div>
                <audio id="bgMusic" src="" autoplay="autoplay" loop="loop"></audio>
            </div>
            <!-- end backgroud sound -->
        </div>
        <!-- end pulled right: nav area -->

    </header>
    <!-- END HEADER -->

    <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
                    Note: These tiles are completely responsive,
                    you can add as many as you like
                    -->
    <div id="shortcut">
        <ul>
            <li>
                <a href="/inbox.php" class="jarvismetro-tile big-cubes bg-color-blue">
				<span class="iconbox">
					<i class="fa fa-envelope fa-4x"></i>
					<span>
						Mail
						<span class="label pull-right bg-color-darken">14</span>
					</span>
				</span>
                </a>
            </li>
            <li>
                <a href="/calendar.php"
                   class="jarvismetro-tile big-cubes bg-color-orangeDark">
				<span class="iconbox">
					<i class="fa fa-calendar fa-4x"></i>
					<span>Calendar</span>
				</span>
                </a>
            </li>
            <li>
                <a href="/gmap-xml.php"
                   class="jarvismetro-tile big-cubes bg-color-purple">
				<span class="iconbox">
					<i class="fa fa-map-marker fa-4x"></i>
					<span>Maps</span>
				</span>
                </a>
            </li>
            <li>
                <a href="/invoice.php"
                   class="jarvismetro-tile big-cubes bg-color-blueDark">
				<span class="iconbox">
					<i class="fa fa-book fa-4x"></i>
					<span>
						Invoice
						<span class="label pull-right bg-color-darken">99</span>
					</span>
				</span>
                </a>
            </li>
            <li>
                <a href="/gallery.php"
                   class="jarvismetro-tile big-cubes bg-color-greenLight">
				<span class="iconbox">
					<i class="fa fa-picture-o fa-4x"></i>
					<span>Gallery </span>
				</span>
                </a>
            </li>
            <li>
                <a href="/profile.php"
                   class="jarvismetro-tile big-cubes selected bg-color-pinkDark">
				<span class="iconbox">
					<i class="fa fa-user fa-4x"></i>
					<span>My Profile </span>
				</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- END SHORTCUT AREA -->

<?php $this->registerJsFile("js/my_js/tip.js", ['yii\web\AssetBundle']);?>
<script>

    window.onload = function(){

    }
</script>
