<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Role $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Role',
]) . $model->name;
?>
    <?= $this->render('_form', [
    'model' => $model,
    'moduleinfo' => $moduleinfo,
    ]) ?>
