<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Role $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Role',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'moduleinfo'=>$moduleinfo,
    ]) ?>
