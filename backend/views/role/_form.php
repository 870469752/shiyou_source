<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<style>
.table tr th{text-align: center}

</style>
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
				 data-widget-deletebutton="false"
				 data-widget-editbutton="false"
				 data-widget-colorbutton="false"
				 data-widget-sortable="false">
				<header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
					<h2><?= Html::encode($this->title) ?></h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						<input class="form-control" type="text">
					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<?php $form = ActiveForm::begin(); ?>
						<fieldset>
							<?= $form->field($model, 'name')->textInput() ?>

							<?= $form->field($model, 'description')->textInput() ?>

							<!--checkbox    start -->
                            <div class="form-group">
                                <label class="control-label">分配权限</label>
                                <div style="width: 100%;OVERFLOW-Y: auto; OVERFLOW-X:hidden;height: 250px">
                                    <table style="width: 100%" class="table table-bordered table-condensed ng-scope">
                                        <tr style="background-color: #0000ff;text-align: center">
                                            <th style="width: 5%;"><input type="checkbox" id="all_check"></th>
                                            <th style="width: 14%">模块</th>
                                            <th style="width: 20%">描述</th>
                                            <th style="width: 60%">权限</th>
                                        </tr>
                                        <tbody>
                                        <?php
                                        if($model->isNewRecord){
                                            foreach($moduleinfo as $key => $value){
                                                ?>
                                                <tr data-id='<?=$value['id'];?>' data-name='<?=$value['name'];?>' data-description='<?=$value['description'];?>' >
                                                    <td style="text-align: center"><input class = "_check" name="module_check" id="<?=$value['id'];?>_module" type = "checkbox" value="<?=$value['id'];?>"> </td>
                                                    <td><?=$value['name'];?></td>
                                                    <td><?=$value['description'];?></td>
                                                    <td>
                                                        <?php
                                                        if(count($value['action_list'])){
                                                            $temp=$value['action_list'];
                                                            foreach($temp as $k => $v){
                                                                ?>
                                                                <div style="width: 20%;float: left"><input class = "_check" name="<?=$value['id'];?>_check" type="checkbox" value="<?=$v['name'];?>"><?=$v['name'];?> </div>
                                                            <?php }}?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        }else{
                                            foreach($moduleinfo as $key => $value){
                                                ?>
                                                <tr data-id='<?=$value['id'];?>' data-name='<?=$value['name'];?>' data-description='<?=$value['description'];?>' >
                                                    <td style="text-align: center"><input class = "_check" name="module_check" id="<?=$value['id'];?>_module" type = "checkbox" value="<?=$value['m_id'];?>"  <?=$value['checked'];?>> </td>
                                                    <td><?=$value['name'];?></td>
                                                    <td><?=$value['description'];?></td>
                                                    <td>
                                                        <?php
                                                        if(count($value['action_list'])){
                                                            $temp=$value['action_list'];
                                                            foreach($temp as $k => $v){
                                                                ?>
                                                                <div style="width: 20%;float: left"><input class = "_check" name="<?=$value['m_id'];?>_check" type="checkbox" value="<?=$v['name'];?>" <?=$v['checked'];?>><?=$v['name'];?></div>
                                                            <?php }}?>
                                                    </td>
                                                </tr>
                                            <?php }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
							<!--checkbox   end -->

							<?= Html::hiddenInput('ModuleRole[role_id]', $module_model->role_id) ?>
                            <input type="hidden" id="role-auth_action" class="form-control" name="Role[auth_action]">

						</fieldset>
						<div class="form-actions">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),['id'=>'_submit'])  ?>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- END COL -->
	</div>
</section>
<script>
	window.onload = function(){
		$("#all_check").click(function(){
            $("._check").prop("checked",$("#all_check").prop("checked"));
        })

		$('#_submit').click(function(){
            var moduleIdstr ='';
            var temp_str = '';
			$('input[name="module_check"]:checked').each(function (){
                var temp = $(this).val();
                temp_str = $(this).val();
				//console.log($(this).value);return false;
                $('input[name="'+temp+'_check"]:checked').each(function (){
                    temp_str += "|||"+$(this).val();
                });
                moduleIdstr += temp_str+"&&&"
			});
			if (moduleIdstr == '') {
				alert('模块不能为空');
				return false;
			}
            $("#role-auth_action").val(moduleIdstr);
		});
	}


</script>