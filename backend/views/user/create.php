<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'User',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'tag' => $tag,
        'role_info' => $role_info,
    ]) ?>
