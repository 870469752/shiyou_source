<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(['id' => 'form-signup','options' => ['enctype' => 'multipart/form-data']]); ?>
                        <fieldset>
                            <?= $tag == 'create' ? $form->field($model, 'username')->textInput(['value' => ' ']) :  $form->field($model, 'username')->textInput()?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                            <?= $form->field($model, 'email')->textInput() ?>

                            <?= $form->field($model, 'phone_number')->textInput(['data-mask' => '(999) 999-99-999'])?>

                            <?= $form->field($model, 'role_id')->dropDownList($role_info, ['class' => 'select2', 'multiple' => 'multiple'])?>

                            <div style = 'width:50%'>

                                <?= $form->field($model, 'head_portrait')->fileInput(['class' => 'form-control input-lg', 'id' => 'image', 'placeholder' => Yii::t('app', 'Image')]) ?>

                            </div>

                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($tag == 'create' ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $tag == 'create' ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end();?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>

<?php
$this->registerJsFile("js/fileinput/fileinput.min.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/fileinput/fileinput.min.css", ['yii\web\JqueryAsset']);
?>

<script>
    window.onload = function() {
        var str=$("#form-signup").attr("action");
        var id = 0;
        if(str !== "/user/create"){
            id = str.replace(/[^0-9]/ig,"")
        }
        $("#image").fileinput({
            'uploadUrl': "/uploads/",
            'showUpload':false,
            'previewFileType':'any'
        });

        $("#signupform-username").blur(function(){
            var username = $("#signupform-username").val();
            $.ajax({
                url: '/user/get-attr?attr=username&val='+username+'&id='+id,
                async: true,
                dateType: 'text',
                success: function(data)
                {
                    if(data==true){
                        $(".field-signupform-username").attr("class","form-group field-signupform-username required has-error");
                        $(".field-signupform-username .help-block").html("用户名重复。");
                        $("#signupform-username").focus();
                        return false;
                    }else{
                        $(".field-signupform-username").attr("class","form-group field-signupform-username required has-success");
                        $(".field-signupform-username .help-block").html("")
                    }
                }
            })
        })

        $("#signupform-email").blur(function(){
            var email = $("#signupform-email").val();
            $.ajax({
                url: '/user/get-attr?attr=email&val='+email+'&id='+id,
                async: true,
                dateType: 'text',
                success: function(data)
                {
                    if(data==true){
                        $(".field-signupform-email").attr("class","form-group field-signupform-email required has-error");
                        $(".field-signupform-email .help-block").html("邮箱重复。");
                        $("#signupform-email").focus();
                        return false;
                    }else{
                        $(".field-signupform-email").attr("class","form-group field-signupform-email required has-success");
                        $(".field-signupform-email .help-block").html("")
                    }
                }
            })
        })

        $("#signupform-phone_number").blur(function(){
            var phone_number = $("#signupform-phone_number").val();
            $.ajax({
                url: '/user/get-attr?attr=phone_number&val='+phone_number+'&id='+id,
                async: true,
                dateType: 'text',
                success: function(data)
                {
                    if(data==true){
                        $(".field-signupform-phone_number").attr("class","form-group field-signupform-phone_number required has-error");
                        $(".field-signupform-phone_number .help-block").html("手机号码重复。");
                        $("#signupform-phone_number").focus();
                        return false;
                    }else{
                        $(".field-signupform-phone_number").attr("class","form-group field-signupform-phone_number required has-success");
                        $(".field-signupform-phone_number .help-block").html("")
                    }
                }
            })
        })
    }
</script>

