<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'User',
]) . $model->username;
?>
    <?= $this->render('_form', [
        'model' => $model,
        'tag' => $tag,
        'role_info' => $role_info,
    ]) ?>
