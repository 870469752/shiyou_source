<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\UserSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>
			</header>
			<!-- widget div-->
			<div>

    <?= GridView::widget([
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

            ['attribute' => 'username'],
            // ['attribute' => 'auth_key', 'headerOptions' => ['data-hide' => 'phone']],
            // ['attribute' => 'password_hash', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            // ['attribute' => 'password_reset_token', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'phone_number','headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'email', 'format' => 'email', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            // ['attribute' => 'role', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            // ['attribute' => 'status', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'created_at', 'format' => 'datetime', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'updated_at', 'format' => 'datetime', 'headerOptions' => ['data-hide' => 'phone,tablet']],

//            ['class' => 'common\library\MyActionColumn','template' => '{update} {delete}','header' => Yii::t('app', 'Action'), 'headerOptions' => ['data-class' => 'expand']],
//            ['class' => 'common\library\MyActionColumn','template' => '{accredit}','header' => Yii::t('app', 'Accredit'), 'headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>


    			</div>
			</div>
		</article>
	</div>
</section>
