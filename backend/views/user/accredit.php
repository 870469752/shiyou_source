<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$this->title = Yii::t('app', 'Accredit {modelClass}', [
  'modelClass' => 'User',
]).$model->user_id;
?>
    <?= $this->render('form_accredit', [
        'model' => $model,
        'r_data' => $r_data,
    ]) ?>
