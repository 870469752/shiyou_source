<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Item $model
 */

$this->title = Yii::t('app', 'Accredit'). ": " . $model->parent;
?>
    <?= $this->render('form_accredit', [
        'model' => $model,
        'c_data' => $c_data,
    ]) ?>
