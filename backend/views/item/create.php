<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Item $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Item',
]);
?>
    <?php 
    $this->render('_form', [
        'model' => $model,
    ]) ?>
