<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Item $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Item',
]) . $model->name;
?>
    <?= $this->render('form_role', [
        'model' => $model,
    ]) ?>
