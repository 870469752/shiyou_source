<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 *
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = YII::t('app', $name);
?>

<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="row">
			<div class="col-sm-12">
				<div class="text-center error-box">
					<h1 class="error-text tada animated">
						<i class="fa 
							<?php if($exception->statusCode == 404): ?>
							fa-warning text-warning
							<?php elseif($exception->statusCode == 403): ?>
							fa-lock text-muted
							<?php endif ?>
						"></i>
						<?= Html::encode($this->title) ?>
					</h1>
					<h2 class="font-xl">
						<strong><?= YII::t('app', 'Oops, Go the wrong place!') ?></strong>
					</h2>
					<br />
					<p class="lead semi-bold">
						<strong>
						<?php if($exception->statusCode == 404): ?>
							<?= YII::t('app', 'The page you requested could not be found') ?>.
						<?php elseif($exception->statusCode == 403): ?>
							<?= YII::t('app', 'You do not have permission to access this page') ?>.
						<?php endif ?>
						</strong>
						<br>
						<br>
						<small>
							<?= YII::t('app', 'Use your browsers Back button to navigate to the page you have prevously come from') ?>
							<br>
							<?= YII::t('app', 'Or click on the following menu') ?>:
						</small>
					</p>
					<ul class="error-search text-left font-md list-unstyled">
						<li>
							<a href="<?= URL::home() ?>">
								<small>
									<i class="fa fa-fw fa-home"></i>
									<?= YII::t('app', 'Go to') ?> <?= YII::t('app', 'Home') ?>
								</small>
							</a>
						</li>
						<li>
							<a href="javascript:history.back();">
								<small>
									<i class="fa fa-fw fa-arrow-left"></i>
									<?= YII::t('app', 'Go back') ?>
								</small>
							</a>
						</li>
					</ul>
				</div>

			</div>

		</div>

	</div>

</div>
