<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Station $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Station',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'area_tree' => $area_tree
    ]) ?>
