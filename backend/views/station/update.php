<?php

use yii\helpers\Html;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Station $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'Station',
]) . MyFunc::DisposeJSON($model->name);
?>
    <?= $this->render('_form', [
        'model' => $model,
        'area_tree' => $area_tree
    ]) ?>
