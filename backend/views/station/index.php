<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\StationSearch $searchModel
 */
 
TableAsset::register ( $this );
$this->title = Yii::t('app', 'Stations');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= Html::encode($this->title) ?></h2>
			</header>
			<!-- widget div-->
			<div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

//            ['attribute' => 'area_code', 'headerOptions' => ['data-hide' => 'phone']],
            ['attribute' => 'ip', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'image_url', 'format' => 'url', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'is_initialized', 'format' => 'boolean', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'logo_url', 'format' => 'url', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => 'default_language', 'headerOptions' => ['data-hide' => 'phone,tablet']],
            ['attribute' => "name", 'format' => 'JSON', 'headerOptions' => ['data-hide' => 'phone,tablet']],

            ['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>
    			</div>
			</div>
		</article>
	</div>
</section>
<?php
// 搜索和创建按钮
$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$search_button}');
options.button.push('{$create_button}');
table_config('datatable', options);						
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);
?>
<!-- 搜索表单和JS --> 
<?php MyFunc::TableSearch($searchModel) ?>

