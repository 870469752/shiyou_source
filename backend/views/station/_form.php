<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\Station $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->
	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget"
			data-widget-deletebutton="false"
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>
				<h2><?= MyHtml::encode($this->title) ?></h2>
			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->
					<input class="form-control" type="text">
				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body">

                    <?php $form = MyActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                    <fieldset>
                        <?php $name_data = ArrayHelper::getValue($model, 'name'); ?>
                        <?= $form->field($model, 'name')->textInput(['name' => 'null', 'value' => MyFunc::DisposeJSON($name_data)])?>
                        <?= MyHtml::hiddenInput('Station[name]', $name_data) ?>
                        <?= $form->field($model, 'station_id')->textInput(['value' => 0]) ?>
                        <?= $form->field($model, 'country')->textInput() ?>
                        <?= $form->field($model, 'city')->textInput() ?>
                        <?= $form->field($model, 'ip')->textInput() ?>
                        <?= $form->field($model, 'web_port')->textInput() ?>
                        <?= $form->field($model, 'category_level')->textInput() ?>
<!--						?//= $form->field($model, 'area_code')->dropDownList($area_tree, ['tree' => true, 'class' => 'select2']) ?-->

<!--						?//= $form->field($model, 'address')->textInput(['maxlength' => 255]) ?-->
<!--                        ?//= $form->field($model, 'building_year')->textInput() ?-->
<!--                        ?//= $form->field($model, 'building_storey')->textInput() ?-->
<!--                        ?//= $form->field($model, 'building_area')->textInput() ?-->
<!--                        ?//= $form->field($model, 'air_condition_area')->textInput() ?-->
<!--                        ?//= $form->field($model, 'chiller_quantity')->textInput() ?-->
<!--                        ?//= $form->field($model, 'cooling_pump_quantity')->textInput() ?-->
<!--                        ?//= $form->field($model, 'frozen_pump_quantity')->textInput() ?-->
<!--                        ?//= $form->field($model, 'cooling_tower_quantity')->textInput() ?-->
                        <?= $form->field($model, 'run_time')->textInput(['placeholder' => '填写格式如：7天/周 24小时/天']) ?>
<!--                        ?//= $form->field($model, 'building_type')->dropDownList([
//                            'Commerce' => '商用',
//                            'Hotel' => '酒店',
//                            'Office' => '办公',
//                            'Hospital' => '医院',
//                            'Others' => '其他',
//                        ],['class' => 'select2']) ?-->

                        <?= $form->field($model, 'image_url')->fileInput() ?>
						<?= $form->field($model, 'logo_url')->fileInput() ?>

						<?= $form->field($model, 'default_language')->dropDownList(['zh-cn' => '中文', 'en-us' => '英文'],['class' => 'select2']) ?>

					</fieldset>
					<div class="form-actions">
						<?= MyHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					</div>

					<?php MyActiveForm::end(); ?>
				</div>
				<!-- end widget content -->
				
			</div>
			<!-- end widget div -->
			
		</div>
		<!-- end widget -->
		
	</article>
	<!-- END COL -->
</div>
</section>
<?php
$this->registerJs ( "edit_name('Station','".Yii::$app->language."');", View::POS_READY);
?>
