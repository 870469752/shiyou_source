<?php

use common\library\MyHtml;
use common\library\MyActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\models\Point;
/**
 * @var yii\web\View $this
 * @var backend\models\Station $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
				<span class="widget-icon">
					<i class="fa fa-edit"></i>
				</span>
                    <h2><?= '绑定点位' ?></h2>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = MyActiveForm::begin(); ?>
                        <fieldset>

                            <label>绑定类型</label>
                            <?= MyHtml::dropDownList('type',[] ,[
                                                                  '' => '',
                                                                  'seasonal' => Yii::t('app', 'Seasonal Point'),
                                                                ], ['class' => 'select2', 'placeholder' => '请选择绑定类型']) ?>

                            <br /> <br />

                            <label>绑定点位</label>
                            <?= MyHtml::dropDownList('point_id',[] , MyFunc::SelectDataAddArrayTop(MyFunc::_map(Point::getAvailablePoint(), 'id', 'name')), ['class' => 'select2', 'placeholder' => '请选择绑定类型']) ?>

                        </fieldset>
                        <div class="form-actions">
                            <?= MyHtml::submitButton(Yii::t('app', 'Create'),['class' => 'btn btn-success']) ?>
                        </div>

                        <?php MyActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>
</section>
<?php
$this->registerJs ( "edit_name('Station','".Yii::$app->language."');", View::POS_READY);
?>
