<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\library\MyFunc;

/**
 * @var yii\web\View $this
 * @var backend\models\Station $model
 */

$this->title = MyFunc::DisposeJSON($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="station-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'attributes' => [
            'id',
            'area_code',
            'address',
            'ip',
            'image_url:url',
            'is_initialized:boolean',
            'logo_url:url',
            'default_language',
            'name:JSON',
        ],
    ]) ?>

</div>
