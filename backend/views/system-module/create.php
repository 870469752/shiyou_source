<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SystemModule $model
 */
$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'System Module',
]);
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
