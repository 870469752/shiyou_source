<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;

/**
 * @var yii\web\View $this
 * @var backend\models\SystemModule $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false">
                <header>
					<span class="widget-icon">
						<i class="fa fa-edit"></i>
					</span>
                    <h2><?= Html::encode($this->title) ?></h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <?php $form = ActiveForm::begin(); ?>
                        <fieldset>

                            <?= $form->field($model, 'module')->textInput(['maxlength' => 255, 'placeholder' => '[Module/]Controller']) ?>

                            <?= Html::hiddenInput('SystemModule[action_info]', '', ['id' => 'systemmodule-action_info'])?>

                            <div class="col-md-12">
                                <panel panel-class="panel-primary" data-heading="Editable Rows" class="ng-isolate-scope">
                                    <div class="panel panel-primary">
                                        <div class="panel-body" ng-transclude="">
                                            <table id = "action" class="table table-bordered table-condensed ng-scope">
                                                <thead>
                                                <tr style="font-weight: bold">
                                                    <th style = "10%"><input type = "checkbox" id = "check_all"></th>
                                                    <th style="width:30%">方法名</th>
                                                    <th style="width:70%">描述</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!$model->isNewRecord):
                                                foreach($model->action_info as $key => $value):?>
                                                    <tr data-name = "<?=$key;?>" data-content = "<?=$value;?>" class="ng-scope">
                                                        <td><input class = "_check" type = "checkbox"></td>
                                                        <td><?=$key;?></td>
                                                        <td><?=$value;?></td>
                                                    </tr>
                                                <?php
                                                endforeach;
                                                endif;?>
                                                </tbody>
                                            </table>
                                            <a id = "_delete" class="btn btn-sm btn-danger" style = "float:right">Delete</a>
                                            <a id = "_add" class="btn btn-default ng-scope" data-toggle="modal" data-target="#myModal" style = "float:right" >Add row</a>
                                        </div>
                                    </div>
                                </panel>
                            </div>
                            <?= $form->field($model, 'additional')->textInput(['maxlength' => 255]) ?>

                        </fieldset>
                        <div class="form-actions">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => '_submit', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title" id="myModalLabel">添加</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id = "add_name" placeholder="方法名" required="">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id = "add_content" placeholder="描述" rows="5" required=""></textarea>
                            </div>
                            <div id = "error_message" style = "display:none" class = "form-group">
                                <i class="fa-fw fa fa-warning"></i>
                                <span style = "color:red" id = "_message"></span>
                             </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id = "_cancel" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button id = "add_action" type="button" class="btn btn-primary">
                        Add
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    <!-- END MAIN CONTENT -->
</section>

<?php
$this->registerJsFile("js/bootstrap/bootstrap.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/jquery.mockjax.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/x-editable/x-editable.min.js", ['backend\assets\AppAsset']);
$this->registerJsFile("js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js", ['backend\assets\AppAsset']);
?>

<script>
    window.onload = function(){
        //页面加载完毕 将方法列表中的数据存放入隐藏框中
        /**************************function init * start ****************/
        function my_overload()
        {
            $('#_message').text('');
            $('#error_message').hide();
        }

        function add_row($name, $content)
        {
            $('#action tbody').append(
                '<tr ng-repeat="user in users" class="ng-scope" data-name="'+$name+'" data-content="'+$content+'">' +
                    '<td><input class = "_check" type = "checkbox"></td>'+
                    '<td>'+$name+'</td>'+
                    '<td>'+$content+'</td>'+
                    '</tr>');
        }

        function add_input_action_val()
        {
            var $info = '';
            $('#action tbody tr').each(function(k, v){
                $info += $(this).data('name') + ',' + $(this).data('content') + ';';
            })
            $('#systemmodule-action_info').val($info);
        }

        /**************************function init * end ****************/

        /*------------------------------------------------------------------------------------------------------------*/

        /************  action table click event  * start ********************/
        $('#add_action').click(function(){                                                                              //弹出框 add点击事件
            //简单的验证
            var $name = $('#add_name').val().trim();
            var $content = $('#add_content').val().trim();
            if($name == ''){
                $('#error_message').show();
                $('#_message').text('方法名 不能为空!');
                return false;
            }
            if($content == ''){
                $('#error_message').show();
                $('#_message').text('描述 不能为空!');
                return false;
            }
            add_row($name, $content);my_overload();
            $('#_cancel').trigger('click');
        });

        $('#_add').click(function(){                                                                                    //页面添加row按钮点击事件
            my_overload();
        })

        $('#_delete').click(function(){                                                                                 //页面delete按钮删除事件
            //each checkbox
            $('._check').each(function(){
                if($(this).prop('checked')){
                    $(this).parent().parent().remove();
                }
            })
        })

        $('#check_all').click(function(){                                                                               //checkbox全选事件
            var $check_all = $(this);
            $('._check').each(function(){
                $(this).prop('checked', $check_all.prop('checked'));
            })
        })
        /**************** action table click event  * end ***********/

        /*------------------------------------------------------------------------------------------------------------*/

        /********************Submit data processing  * start ************************************/
        $('#_submit').click(function(){
            add_input_action_val();
        })
        /********************Submit data processing  * end ************************************/
    }
</script>
