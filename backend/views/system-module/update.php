<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\SystemModule $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
  'modelClass' => 'System Module',
]) . $model->id;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
