<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\RoleSearch $searchModel
 */

//TableAsset::register ( $this );
//$this->title = Yii::t('app', 'Roles');
//$this->params['breadcrumbs'][] = $this->title;
?>
<!--<style>-->
<!--    table tr td a{width: 120px}-->
<!--    #myTabContent{background: url("/img/block/new/2.jpg");background-size: 100% 100%;}-->
<!--</style>-->
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header style="height:44px;">
                    <!--                    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>-->
                    <ul class="nav nav-tabs pull-left in" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#s1">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >楼控系统</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s2">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >电梯系统</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s3">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >智能照明</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s4">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >锅炉系统</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s5">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >制冷站</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s6">  <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >直燃机</span></a>
                        </li>
                    </ul>

                </header>

                <!-- widget div-->
                <div class="no-padding">
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">

                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body">
                        <!-- content  -->
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
















                                <!-- widget  楼控系统 content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#tab-A12" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:35px;">A12</span> </a>
                                            </li>
                                            <li>
                                                <a href="#tab-A16" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 15px;width:35px;">A16</span></a>
                                            </li>
                                            <li>
                                                <a href="#tab-A29" data-toggle="tab"><span class="badge bg-color-greenLight txt-color-white" style="font-size: 15px;width:35px;">A29</span></a>
                                            </li>
                                            <li>
                                                <a href="#tab-A34" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:35px;">A34</span></a>
                                            </li>
                                            </li>
                                            <li>
                                                <a href="#tab-A42" data-toggle="tab"><span class="badge bg-color-pink txt-color-white" style="font-size: 15px;width:35px;">A42</span></a>
                                            </li>
                                            <li class="active">
                                                <a href="#tab-A45" data-toggle="tab"><span class="badge bg-color-red txt-color-white" style="font-size: 15px;width:35px;">A45</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane " id="tab-A12">
                                                <p>

                                                <table style="width: 90%;text-align:center;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td>  </td>

                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">送排风系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">新风系统</span></a> </td>


                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">给排水系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">热交换</span></a> </td>


                                                        <td>  </td>

                                                    </tr>
                                                </table>
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tab-A16">
                                                <p>

                                                <table style="width: 90%;text-align:center;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td>  </td>

                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">送排风系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">新风系统</span></a> </td>


                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">给排水系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">热交换</span></a> </td>


                                                        <td>  </td>

                                                    </tr>
                                                </table>
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tab-A29">

                                                <p>

                                                <table style="width: 90%;text-align:center;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td>  </td>

                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">送排风系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">新风系统</span></a> </td>


                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">给排水系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">热交换</span></a> </td>


                                                        <td>  </td>

                                                    </tr>
                                                </table>
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tab-A34">
                                                <p>

                                                <table style="width: 90%;text-align:center;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td>  </td>

                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">送排风系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">新风系统</span></a> </td>


                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">给排水系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">热交换</span></a> </td>


                                                        <td>  </td>

                                                    </tr>
                                                </table>
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tab-A42">

                                                <p>

                                                <table style="width: 90%;text-align:center;">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td>  </td>

                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">送排风系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">新风系统</span></a> </td>


                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>

                                                        <td> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">给排水系统</span></a> </td>
                                                        <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">热交换</span></a> </td>


                                                        <td>  </td>

                                                    </tr>
                                                </table>
                                                </p>
                                            </div>
                                            <div class="tab-pane active" id="tab-A45">
                                                <p>


                                                    <!-- row -->
                                                <div class="row">

                                                    <!-- NEW WIDGET START -->
                                                    <article class="col-sm-12 col-md-12 col-lg-3">


                                                        <!-- widget div-->
                                                        <div>


                                                            <!-- widget content -->
                                                            <div class="widget-body">
                                                                <div class="tree">
                                                                    <ul>
                                                                        <li>
                                                                            <span><i class="fa fa-lg fa-calendar"></i><a href="/subsystem/crud/view?id=61"> 热交换</a></span>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- end widget content -->

                                                        </div>
                                                        <!-- end widget div -->

                                                        <!-- end widget -->

                                                    </article>
                                                    <!-- WIDGET END -->
                                                    <!-- NEW WIDGET START -->
                                                    <article class="col-sm-12 col-md-12 col-lg-3">


                                                        <!-- widget div-->
                                                        <div>


                                                            <!-- widget content -->
                                                            <div class="widget-body">
                                                                <div class="tree">
                                                                    <ul>
                                                                        <li>
                                                                            <span><i class="fa fa-lg fa-calendar"></i> 给排水系统</span>
                                                                            <ul>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 换热站</span> &ndash; <a href="/subsystem/crud/view?id=299">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 洗衣房</span> &ndash; <a href="/subsystem/crud/view?id=302">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 热水机房</span> &ndash; <a href="/subsystem/crud/view?id=301">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 中水机房</span> &ndash; <a href="/subsystem/crud/view?id=304">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 锅炉水处理间</span> &ndash; <a href="/subsystem/crud/view?id=300">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 车库西RF11污水</span> &ndash; <a href="/subsystem/crud/view?id=306">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF22进风机房</span> &ndash; <a href="/subsystem/crud/view?id=315">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 换热站外污水坑</span> &ndash; <a href="/subsystem/crud/view?id=312">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF13排水机房</span> &ndash; <a href="/subsystem/crud/view?id=310">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF12排水机房</span> &ndash; <a href="/subsystem/crud/view?id=307">查看</a>
                                                                                </li>

                                                                            </ul>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- end widget content -->

                                                        </div>
                                                        <!-- end widget div -->

                                                        <!-- end widget -->

                                                    </article>
                                                    <!-- WIDGET END -->
                                                    <!-- NEW WIDGET START -->
                                                    <article class="col-sm-12 col-md-12 col-lg-3">


                                                        <!-- widget div-->
                                                        <div>


                                                            <!-- widget content -->
                                                            <div class="widget-body">
                                                                <div class="tree">
                                                                    <ul>
                                                                        <li>
                                                                            <span><i class="fa fa-lg fa-calendar"></i> 送排风系统</span>
                                                                            <ul>
                                                                                <li>
                                                                                            <span><i class="fa fa-clock-o"></i> 换热站</span> &ndash; <a href="/subsystem/crud/view?id=193">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 洗衣房</span> &ndash; <a href="/subsystem/crud/view?id=198">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 给水机房</span> &ndash; <a href="/subsystem/crud/view?id=196">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> 进风机房</span> &ndash; <a href="/subsystem/crud/view?id=202">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF22进风机房</span> &ndash; <a href="/subsystem/crud/view?id=197">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF23排风机房</span> &ndash; <a href="/subsystem/crud/view?id=201">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF13排风机房</span> &ndash; <a href="/subsystem/crud/view?id=200">查看</a>
                                                                                </li>
                                                                                <li>
                                                                                    <span><i class="fa fa-clock-o"></i> RF12进风机房</span> &ndash; <a href="/subsystem/crud/view?id=199">查看</a>
                                                                                </li>
                                                                               

                                                                            </ul>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- end widget content -->

                                                        </div>
                                                        <!-- end widget div -->

                                                        <!-- end widget -->

                                                    </article>
                                                    <!-- WIDGET END -->
                                                    <!-- NEW WIDGET START -->
                                                    <article class="col-sm-12 col-md-12 col-lg-3">


                                                        <!-- widget div-->
                                                        <div>


                                                            <!-- widget content -->
                                                            <div class="widget-body">
                                                                <div class="tree">
                                                                    <ul>
                                                                        <li>
                                                                            <span><i class="fa fa-lg fa-calendar"></i><a href="/subsystem/crud/view?id=318"> 新风系统</a></span>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- end widget content -->

                                                        </div>
                                                        <!-- end widget div -->

                                                        <!-- end widget -->

                                                    </article>
                                                    <!-- WIDGET END -->

                                                </div>

                                                <!-- end row -->

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->










                            </div>
                            <!-- end s1 tab pane -->

                            <div class="tab-pane fade" id="s2">




                                <!-- widget  电梯系统 content -->
                                <div class="widget-body" style="height: 600px">

                                    <table style="width: 90%;text-align:center;">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A12</span></a> </td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A16</span></a></td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A29</span></a> </td>

                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>


                                        <tr>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A34</span></a></td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A42</span></a> </td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A45</span></a></td>
                                        </tr>
                                    </table>

                                </div>
                                <!-- end widget content -->





                            </div>
                            <!-- end s2 tab pane -->

                            <div class="tab-pane fade" id="s3">

                                <!-- widget  智能照明 content -->
                                <div class="widget-body" style="height: 600px">

                                    <table style="width: 90%;text-align:center;">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A12</span></a> </td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A16</span></a></td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A29</span></a> </td>

                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>


                                        <tr>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A34</span></a> </td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A42</span></a></td>
                                            <td> </td>

                                        </tr>
                                    </table>

                                </div>
                                <!-- end widget content -->






                            </div>
                            <!-- end s3 tab pane -->

                            <div class="tab-pane fade" id="s4">


                                <!-- widget 锅炉系统 content -->

                                <div class="widget-body" style="height: 600px">

                                    <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                        <tr>
                                            <td><a href="javascript:void(0);" class="btn btn-primary"><span style="font-size:15px;">A12</span></a></td>
                                        </tr>

                                    </table>

                                </div>
                                <!-- end widget content -->


                            </div>
                            <!-- end s4 tab pane -->

                            <div class="tab-pane fade" id="s5">
                                <!-- widget  制冷站content -->
                                <div class="widget-body" style="height: 600px">

                                    <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">
                                        <tr>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A12</span></a> </td>
                                            <td> <a class="btn btn-primary" href="javascript:void(0);"><span style="font-size:15px;">A16</span></a></td>

                                        </tr>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end s5 tab pane -->

                            <div class="tab-pane fade" id="s6">
                                <!-- widget 直燃机 content -->
                                <div class="widget-body" style="height: 600px">

                                    <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                        <tr>
                                            <td><a href="javascript:void(0);" class="btn btn-primary"><span style="font-size:15px;">A34</span></a></td>
                                        </tr>

                                    </table>


                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end s6 tab pane -->
                        </div>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>
    <!-- end row -->
</section>
<!-- end widget grid -->


<script type="text/javascript">

    window.onload = function () {
        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });

    }

</script>