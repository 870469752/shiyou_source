<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Index_safe');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    #demo-pill-nav{font-size: 50px;}
    #tab-A45 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A45 ul li span a{text-decoration: none;color:inherit;}
    #tab-A29 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 120px}
    #tab-A29 ul li span a{text-decoration: none;color:inherit;}
    #tab-A34 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A34 ul li span a{text-decoration: none;color:inherit;}
    #tab-A42 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width:100px}
    #tab-A42 ul li span a{text-decoration: none;color:inherit;}
    /*#myTabContent{background: url("/img/background.jpg");background-size: 100% 100%;}*/
    #myTabContent{background-color:#000000;background-size: 100% 100%;}

    #A29-water ul li span{width:80px}
    #A34-others ul li span{width:110px}
    #A34-ele ul li span{width:100px}
    #A34-floorcon ul li span{width:120px}
    #A42-water ul li span{width:140px}
    #A45-floorcon ul li span{width:100px}
    #A45-others ul li span{width:110px}
    #A29-kongtiao ul li span{width:120px}
    #A42-building ul li span{width:120px;}

    .badge{border-radius:0px;}

</style>
<section id="widget-grid" class="">

    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget-color-blueDark jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div id="myTabContent" class="widget-body" style="height: 700px;">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                <li>
                                    <a href="#tab-A12" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A12地块</span> </a>
                                </li>
                                <li>
                                    <a href="#tab-A16" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A16地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A29" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A29地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A34" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A34地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A42" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A42地块</span></a>
                                </li>
                                <li class="active">
                                    <a href="#tab-A45" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A45地块</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" style="height: 700px; overflow-y: auto;overflow-x: auto">
                                <div class="tab-pane" id="tab-A12" style="margin-top:27px;margin-left: 40px">

                                </div>
                                <div class="tab-pane" id="tab-A16" style="margin-top:27px;margin-left: 40px">

                                </div>
                                <div class="tab-pane" id="tab-A29" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=143"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=148"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=153"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=164"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=165"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=167"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=168"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=169"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=208"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=210"><span> <i></i>2F</span> </a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=212"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=213"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=214"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=215"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=220"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=221"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=348"><span> <i></i>B1门禁</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=29"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left;display: none">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 动力中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=854"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=853"><span> <i></i>2F</span> </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=858"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=857"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=856"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=855"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=862"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=861"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=860"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=859"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width:22%;float: left">
                                                <a href="/route/firm-alarm"><span  style="width: 150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li>
                                                <a href="/subsystem/crud/view?id=381"><span  style="width: 150px;"> <i></i> 入侵报警</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A34" style="margin-top:27px;margin-left: 20px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=745"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=713"><span> <i></i>F1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=714"><span> <i></i>F2</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=715"><span> <i></i>F3</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=716"><span> <i></i>F4</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=717"><span> <i></i>F5</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=729"><span> <i></i>F6</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=733"><span> <i></i>F7</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=734"><span> <i></i>F8</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=735"><span> <i></i>F9</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=736"><span> <i></i>F10</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=737"><span> <i></i>F11</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=738"><span> <i></i>F12</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=746"> <span> <i></i>B1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=747"> <span> <i></i>F1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=34"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A34-others" style="width:22%;float: left;display: none;">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=684"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=685"><span> <i></i>F1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=686"><span> <i></i>F2</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=687"><span> <i></i>F3</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=691"><span> <i></i>F4</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=697"><span> <i></i>F5</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=700"><span> <i></i>F6</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=703"><span> <i></i>F7</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=706"><span> <i></i>F8</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=709"><span> <i></i>F9</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=711"><span> <i></i>F10</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=718"><span> <i></i>F11</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=718"><span> <i></i>F12</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width:22%;float: left">
                                                <a href="/route/fire-alarm"><span  style="width: 150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li style="float: left">
                                                <a href="/subsystem/crud/view?id=740"><span  style="width: 150px;"> <i></i>周界防范</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A42" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=504"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=505"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=506"><span> <i></i>3F </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=507"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=530"><span> <i></i>地下</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=509"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=512"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=513"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=514"><span> <i></i>4F </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=508"><span> <i></i>地下</span> </a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=42"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A42-others" style="width:22%;float: left;display: none">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=528"><span> <i></i>地下 </span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/category/crud/location-only-view?id=441"><span> <i></i>楼顶</span> </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="/route/fire-alarm"><span style="width: 150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li>
                                                <a href="/category/crud/location-only-view?id=344"> <span style="width: 150px;"> <i></i>入侵报警</span></a>
                                            </li>
                                            <li>
                                                <a href="/subsystem/crud/view?id=515"><span style="width: 150px;"> <i></i>周界防范</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane  active" id="tab-A45" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 9#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=127"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=152"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=128"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=129"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=65"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=68"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=74"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=87"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 14#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=135"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=136"><span> <i></i>2F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=138"><span> <i></i> B1</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=157"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 11#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=158"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=159"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=160"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 12#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=161"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=162"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=163"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=45"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li ID="A45-others" style=" float: left;display: none">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 9.10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=225"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=226"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=227"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=228"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" " >
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 11#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=209"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=210"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=211"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 12#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=213"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=214"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=221"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 13#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=217"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=218"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=219"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 14#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=221"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=222"><span> <i></i>2F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        </i><a href="/subsystem/crud/view?id=223"><span> <i></i> B1</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="float: left">
                                                <a href="/route/fire-alarm"><span style="width:150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>

<script type="text/javascript">

    window.onload = function (){

        $(document).ready(function() {

            $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
            $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                }
                e.stopPropagation();
            });

        })

    }

</script>