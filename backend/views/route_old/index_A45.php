<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\RoleSearch $searchModel
 */

?>

<style>
    table tr td a{width: 170px}
    #myTabContent{background: url("/img/block/new/2.jpg");background-size: 100% 100%;}
</style>
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <article class="col-sm-12">

            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header style="height:44px;border-bottom-width:1px;">
                    <!--                    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>-->
                    <ul class="nav nav-tabs pull-left in" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#s1"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >电梯系统</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s2"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >楼宇系统</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s3"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >电力监测</span></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#s4"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >火灾报警</span></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#s5"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >视频监控</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s6"><!--<i class="fa fa-clock-o"></i>--> <span class="hidden-mobile hidden-tablet badge bg-color-blueDark txt-color-white" style="font-size:20px;" >门禁系统</span></a>
                        </li>
                    </ul>

                </header>

                <!-- widget div-->
                <div class="no-padding">
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">

                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body">
                        <!-- content -->
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in padding-0 no-padding-bottom" id="s1">

                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s1_tab-A1" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">电梯系统</span> </a>
                                            </li>

                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">
                                            <div class="tab-pane active" id="s1_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=180" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">电梯系统</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>

                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end s1 tab pane -->

                            <div class="tab-pane fade" id="s2">

                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s2_tab-A1" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">热交换</span> </a>
                                            </li>
                                            <li>
                                                <a href="#s2_tab-A2" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 15px;width:80px;">给排水系统</span></a>
                                            </li>
                                            <li>
                                                <a href="#s2_tab-A3" data-toggle="tab"><span class="badge bg-color-greenLight txt-color-white" style="font-size: 15px;width:80px;">送排风系统</span></a>
                                            </li>
                                            <li>
                                                <a href="#s2_tab-A4" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">新风系统</span></a>
                                            </li>

                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">

                                            <div class="tab-pane active" id="s2_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=61" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">热交换</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>

                                                </table>
                                            </div>

                                            <div class="tab-pane" id="s2_tab-A2">

                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=315" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF22进风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=312" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">热换站外污水坑</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=310" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF13排水机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=307" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF12排水机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=306" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">车库西RF11污水坑</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=304" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">中   水机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=302" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">洗衣房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=301" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">热水机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=300" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">锅炉水处理间</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=299" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">换热站</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                            </div>

                                            <div class="tab-pane" id="s2_tab-A3">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">
                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=202" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">进风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=201" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF23排风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=200" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF13排风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=199" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF12进风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=198" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">洗衣房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=197" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">RF22进风机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=196" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">给水机房</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=193" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">换热站</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>

                                                    </tr>
                                                </table>

                                            </div>

                                            <div class="tab-pane" id="s2_tab-A4">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=318" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">新风系统</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>

                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end s2 tab pane -->

                            <div class="tab-pane fade" id="s3">



                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s3_tab-A1" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">电力监测</span> </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">
                                            <div class="tab-pane active" id="s3_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">
                                                    <tr>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">电力监测</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end s3 tab pane -->

                            <div class="tab-pane fade" id="s4">


                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s4_tab-A1" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">B1</span> </a>
                                            </li>
                                            <li>
                                                <a href="#s4_tab-A2" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 15px;width:80px;">14#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s4_tab-A3" data-toggle="tab"><span class="badge bg-color-greenLight txt-color-white" style="font-size: 15px;width:80px;">13#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s4_tab-A4" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">12#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s4_tab-A5" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">11#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s4_tab-A6" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">9#.10#</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">
                                            <div class="tab-pane active" id="s4_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=224" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">B1</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>

                                                </table>
                                            </div>
                                            <div class="tab-pane" id="s4_tab-A2">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=243" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=252" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>

                                                </table>



                                            </div>
                                            <div class="tab-pane" id="s4_tab-A3">

                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=231" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=233" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=239" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s4_tab-A4">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>

                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s4_tab-A5">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=230" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="javascript:void(0);" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>

                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s4_tab-A6">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=225" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=226" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=227" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=228" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">4F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end s4 tab pane -->

                            <div class="tab-pane fade" id="s5">


                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s5_tab-A1" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">B1</span></a>
                                            </li>
                                            <li>
                                                <a href="#s5_tab-A2" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">14#</span> </a>
                                            </li>
                                            <li>
                                                <a href="#s5_tab-A3" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 15px;width:80px;">10#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s5_tab-A4" data-toggle="tab"><span class="badge bg-color-greenLight txt-color-white" style="font-size: 15px;width:80px;">9#</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">
                                            <div class="tab-pane active" id="s5_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=138" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">B1</span><i class="fa fa-gear"></i></a></td>
                                                    </tr>

                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s5_tab-A2">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=135" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=136" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s5_tab-A3">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=65" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=68" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=74" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=87" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">4F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="tab-pane" id="s5_tab-A4">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/subsystem/crud/view?id=127" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=152" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=128" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/subsystem/crud/view?id=129" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">4F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->


                            </div>
                            <!-- end s5 tab pane -->

                            <div class="tab-pane fade" id="s6">


                                <!-- widget content -->
                                <div class="widget-body" style="height: 600px">

                                    <div class="tabs-left">
                                        <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                            <li>
                                                <a href="#s6_tab-A1" data-toggle="tab"><span class="badge bg-color-orange txt-color-white" style="font-size: 15px;width:80px;">12#</span></a>
                                            </li>
                                            <li>
                                                <a href="#s6_tab-A2" data-toggle="tab"><span class="badge bg-color-blue txt-color-white" style="font-size: 15px;width:80px;">11#</span> </a>
                                            </li>
                                            <li>
                                                <a href="#s6_tab-A3" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 15px;width:80px;">10#</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="margin-right:109px;">
                                            <div class="tab-pane active" id="s6_tab-A1">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                            </div>
                                            <div class="tab-pane" id="s6_tab-A2">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>


                                            </div>
                                            <div class="tab-pane" id="s6_tab-A3">
                                                <table border="0" width="98%" height="400px" style="text-align: center;table-layout:fixed;" cellpadding="2" cellspacing="1">

                                                    <tr>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">1F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">2F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">3F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                        <td><a href="/route/access-card" class="btn btn-success"><i class="fa fa-gear"></i><span style="font-size:15px;">4F</span><i class="fa fa-gear"></i></a></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <!-- end widget content -->


                            </div>
                            <!-- end s6 tab pane -->
                        </div>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>
    <!-- end row -->
</section>
