<?php
use yii\helpers\Html;

$this->title = '楼宇自控';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    #demo-pill-nav{font-size: 50px;}
    #tab-A45 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A45 ul li span a{text-decoration: none;color:inherit;}
    #tab-A29 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 120px}
    #tab-A29 ul li span a{text-decoration: none;color:inherit;}
    #tab-A34 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A34 ul li span a{text-decoration: none;color:inherit;}
    #tab-A42 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width:100px}
    #tab-A42 ul li span a{text-decoration: none;color:inherit;}
    #tab-A45 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width:140px}
    #tab-A45 ul li span a{text-decoration: none;color:inherit;}
    /*#myTabContent{background: url("/img/background.jpg");background-size: 100% 100%;}*/
    #myTabContent{background-color:#000000;background-size: 100% 100%;}

    #A29-water ul li span{width:80px}
    #A29-light ul li span{width:170px}
    #A29-kongtiao ul li span{width:120px}
    #A34-others ul li span{width:110px}
    #A34-ele ul li span{width:100px}
    #A34-floorcon ul li span{width:120px}
    #A42-building ul li span{width:120px;}
    #A42-water ul li span{width:140px}


    .badge{border-radius:0px;}
</style>
<section id="widget-grid" class="">

    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget-color-blueDark jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div id="myTabContent" class="widget-body" style="height: 700px;">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                <li>
                                    <a href="#tab-A12" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A12地块</span> </a>
                                </li>
                                <li>
                                    <a href="#tab-A16" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A16地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A29" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A29地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A34" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A34地块</span></a>
                                </li>
                                <li>
                                    <a href="#tab-A42" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A42地块</span></a>
                                </li>
                                <li class="active">
                                    <a href="#tab-A45" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A45地块</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" style="height: 700px; overflow-y: auto;overflow-x: auto">
                                <div class="tab-pane" id="tab-A12" style="margin-top:27px;margin-left: 40px">
                                </div>
                                <div class="tab-pane" id="tab-A16" style="margin-top:27px;margin-left: 40px">
                                </div>
                                <div class="tab-pane" id="tab-A29" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 31%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 楼控系统</span>
                                                <ul>
                                                    <li id = 'A29-water'>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 给排水系统</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=336"><span> <i></i>1～4号</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=399"><span> <i></i>5～8号</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=400"><span> <i></i>9～12号</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=401"><span><i></i>13～16号</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=402"><span> <i></i>17～20号</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=403"><span> <i></i>21～22号</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 新风机组</span>
                                                        <ul>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=290"><span> <i></i>2-AHU-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=291"><span> <i></i>2-AHU-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=292"> <span> <i></i>2-AHU-3</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=293"><span> <i></i>2-AHU-4</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=294"><span> <i></i>2-AHU-5</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=295"><span> <i></i>2-AHU-6</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=296"><span> <i></i>2-AHU-7</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=297"><span> <i></i>2-AHU-8</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=286"><span> <i></i>1-AHU-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=287"><span> <i></i>1-AHU-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=288"><span> <i></i>1-AHU-3</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=289"><span> <i></i>1-AHU-4</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 送排风系统</span>
                                                        <ul>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 地下一层</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=355"><span> <i></i>B1-GEX-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=354"><span> <i></i>B1-GEX-F-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=353"><span> <i></i>B1-GEX-F-3</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=352"> <span> <i></i>B1-GEX-F-4</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=358"> <span> <i></i>B1-GSA-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=357"><span> <i></i>B1-GSA-F-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=356"><span> <i></i>B1-GSA-F-3 </span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=360"><span> <i></i>B1-EEX-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=359"><span> <i></i>B1-EEX-F-2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=361"><span> <i></i>1-GEX-1-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=362"><span> <i></i>1-GEX-1-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=363"><span> <i></i>1-GEX-1-3</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=364"><span> <i></i>1-GEX-2-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=365"><span> <i></i>1-GEX-3-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=366"><span> <i></i>1-GEX-4-1</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=367"><span> <i></i>2-R-GEX-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=369"><span> <i></i>2-R-GEX-F-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=370"><span> <i></i>2-R-GEX-F-3</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=371"><span> <i></i>2-R-GEX-F-4</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=372"><span> <i></i>2-R-GEX-F-5</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=373"><span> <i></i>2-R-GEX-F-6</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li style="display:none;">
                                                                <span><i class="fa fa-lg fa-plus-circle"></i> 动力中心</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=380"><span> <i></i>3-1-GEX-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=379"><span> <i></i>3-R-GEX-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=378"><span> <i></i>3-R-GEX-F-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=377"><span> <i></i>3-R-GSA-F-1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=375"><span> <i></i>3-R-GSA-F-2</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=374"><span> <i></i>3-R-GSA-F-3 </span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=405"><span> <i></i> 空调系统</span></a>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=404"><span> <i></i> 生活水系统</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 31%; float: left" id="A29-light">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 智能照明</span>
                                                <ul>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=888"><span> <i></i>1#大厅</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=814"><span> <i></i>1#停车场智能照明</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=812"><span> <i></i>1#智能照明系统室外</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=810"><span> <i></i>1#智能照明系统</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="/subsystem/crud/view?id=222"> <span> <i></i>电梯系统</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A34" style="margin-top:27px;margin-left: 20px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li id="A34-floorcon"  style="width: 33%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 楼控系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 送排风系统</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=744"> <span> <i></i>B1送排风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=740"><span> <i></i>S座屋顶排风</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=808"><span> <i></i>B座屋顶排风</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 新风机系统</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=615"><span> <i></i>XF-F1-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=786"><span> <i></i>XF-F1-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=780"><span> <i></i>XF-F1-3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=805"><span> <i></i>XF-F1-4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=614"><span> <i></i>XF-F2-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=788"><span> <i></i>XF-F2-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=759"><span> <i></i>XF-F2-3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=804"><span> <i></i>XF-F2-4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=783"><span> <i></i>XF-F2-5</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=765"><span> <i></i>XF-F3-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=789"><span> <i></i>XF-F3-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=778"><span> <i></i>XF-F3-3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=803"><span> <i></i>XF-F3-4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=781"><span> <i></i>XF-F3-5</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=785"><span> <i></i>XF-F3-6</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=754"><span> <i></i>XF-F4-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=790"><span> <i></i>XF-F4-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=750"><span> <i></i>XF-F5-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=791"><span> <i></i>XF-F5-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=742"><span> <i></i>XF-F6-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=793"><span> <i></i>XF-F6-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=739"><span> <i></i>XF-F7-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=795"><span> <i></i>XF-F7-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=757"><span> <i></i>XF-F8-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=797"><span> <i></i>XF-F8-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=683"><span> <i></i>XF-F9-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=798"><span> <i></i>XF-F9-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=682"><span> <i></i>XF-F10-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=799"><span> <i></i>XF-F10-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=673"><span> <i></i>XF-F11-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=800"><span> <i></i>XF-F11-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=802"><span> <i></i>XF-F12-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=801"><span> <i></i>XF-F12-2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=768"><span> <i></i>K-F1-1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=784"><span> <i></i>K-F3-1</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=772"><span> <i></i>水浸系统 </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=751"><span> <i></i>排水系统</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=749"><span> <i></i>中水系统 </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=748"><span> <i></i>生活给水系统</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=698"><span style="width:130px;"> <i></i>实验室通风系统</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 33%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 智能照明</span>
                                                <ul>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=684"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=685"><span> <i></i>F1</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=686"><span> <i></i>F2</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=687"><span> <i></i>F3</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=691"><span> <i></i>F4</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=697"><span> <i></i>F5</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=700"><span> <i></i>F6</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=703"><span> <i></i>F7</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=706"><span> <i></i>F8</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=709"><span> <i></i>F9</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=711"><span> <i></i>F10</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=718"><span> <i></i>F11</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=730"><span> <i></i>大堂</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=806"><span> <i></i>园林</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="category/crud/location-only-view?id=639"><span> <i></i>电梯</span></a>
                                            </li>
                                            <li>
                                                <span> 直燃机组</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A42" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li id="A42-building" style="width: 33%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 楼控系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 空调机组</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=502"><span> <i></i>空调系统A区</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 新风机组</span>
                                                        <ul>

                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=517"><span> <i></i>A区新风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=518"><span> <i></i>B区新风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=519"><span> <i></i>B区新风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=520"><span> <i></i>B区新风系统3 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=521"><span> <i></i>B区新风系统4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=522"><span> <i></i>C区新风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=523"><span> <i></i>C区新风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=524"><span> <i></i>C区新风系统3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=525"><span> <i></i>C区新风系统4</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 送风系统</span>
                                                        <ul>

                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=461"><span> <i></i>A区送风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=462"><span> <i></i>A区送风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=463"><span> <i></i>B区送风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=464"><span> <i></i>B区送风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=465"><span> <i></i>B区送风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=466"><span> <i></i>C区送风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=467"><span> <i></i>C区送风系统1</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 排风系统</span>
                                                        <ul>

                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=468"><span> <i></i>A区排风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=469"><span> <i></i>A区排风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=470"><span> <i></i>A区排风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=471"><span> <i></i>A区排风系统3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=472"><span> <i></i>A区排风系统4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=473"><span> <i></i>A区排风系统5</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=474"><span> <i></i>B区排风系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=475"><span> <i></i>B区排风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=476"><span> <i></i>B区排风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=477"><span> <i></i>B区排风系统3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=478"><span> <i></i>B区排风系统4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=479"><span> <i></i>B区排风系统5</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=480"><span> <i></i>C区排风系统 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=481"><span> <i></i>C区排风系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=482"><span> <i></i>C区排风系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=483"><span> <i></i>C区排风系统3</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="A42-water">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 给排水系统</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=484"><span> <i></i>A区给排水系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=485"><span> <i></i>A区给排水系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=486"><span> <i></i>A区给排水系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=487"><span> <i></i>A区给排水系统3</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=488"><span> <i></i>A区给排水系统4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=489"><span> <i></i>B区给排水系统 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=490"><span> <i></i>B区给排水系统1</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=491"><span> <i></i>B区给排水系统2</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=492"><span> <i></i>B区给排水系统3 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=493"><span> <i></i>B区给排水系统4 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=494"><span> <i></i>B区给排水系统5</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=495"><span> <i></i>B区给排水系统6</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=496"><span> <i></i>C区给排水系统</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=497"><span> <i></i>C区给排水系统1 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=498"><span> <i></i>C区给排水系统2</span> </a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=499"><span> <i></i>C区给排水系统3 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=500"><span> <i></i>C区给排水系统4</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=501"><span> <i></i>C区给排水系统5</span> </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 33%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 智能照明</span>
                                                <ul>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=820"><span> <i></i>F1表</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/category/crud/location-only-view?id=822"><span> <i></i>F2表</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=823"><span> <i></i>F3表</span></a>
                                                    </li>
                                                    <li style=" ">
                                                        <a href="/subsystem/crud/view?id=688"><span> <i></i>F4表</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="/subsystem/crud/view?id=516"><span> <i></i>电梯系统</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane  active" id="tab-A45" style="margin-top:27px;margin-left: 100px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li  style="width: 50%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 楼控系统</span>
                                                <ul>
                                                    <li id="A45-floorcon">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 给排水</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=312"> <span> <i></i>换热站外污水坑</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=307"><span> <i></i>RF12排风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=310"><span> <i></i>RF13排风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=315"><span> <i></i>RF22进风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=306"><span style="width:160px;"> <i></i>车库西RF11污水坑</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=304"><span> <i></i>中水机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=302"><span> <i></i>洗衣房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=301"><span> <i></i>热水机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=300"><span> <i></i>锅炉水处理间</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=299"><span> <i></i>换热站</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 送排风</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=199"> <span> <i></i>RF12进风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=200"><span> <i></i>RF13排风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=197"><span> <i></i>RF22进风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=201"><span> <i></i>RF23排风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=202"> <span> <i></i>进风机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=198"><span> <i></i>洗衣房 </span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=196"><span> <i></i>给水机房</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=193"><span> <i></i>换热站</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=61"><span> <i></i>热交换</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=318"><span> <i></i>新风</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>

<script type="text/javascript">

    window.onload = function (){

        $(document).ready(function() {

            $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
            $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                }
                e.stopPropagation();
            });

        })

    }

</script>