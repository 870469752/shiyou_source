<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Module $model
 */
$this->title = 'Create Module';
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
