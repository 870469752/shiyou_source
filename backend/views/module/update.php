<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Module $model
 */

$this->title = 'Update Module: ' . $model->name;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
