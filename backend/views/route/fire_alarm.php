<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii\web\View;
use backend\assets\TableAsset;
/**
 * @var yii\web\View $this
 * @var backend\models\Category $model
 * @var yii\widgets\ActiveForm $form
 */

TableAsset::register ( $this );
?>

<style>
    a{text-decoration: none;color:inherit;}
    table tr td{text-align: center}
    table tr th{text-align: center}
</style>

<section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-sortable="false" data-widget-fullscreenbutton='false'>
                <header><span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>火灾报警</h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                </header>
                <div>
                    <div class="jarviswidget-editbox"><input class="form-control" type="text"></div>
                    <div class="widget-body">
                        <table class="table table-striped table-bordered table-hover" style="width:100%;">
                            <thead>
                            <tr>
                                <th data-class="expand">日期</th>
                                <th data-class="expand">报警点</th>
                                <th data-class="expand">报文</th>
                                <th data-class="expand">位置</th>
                                <th data-class="expand">等级</th>
                                <th data-class="expand">状态</th>
                                <th data-class="expand">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7">暂无数据！</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
