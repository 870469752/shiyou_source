<?php

use yii\helpers\Html;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\search\PointSearch $searchModel
 */

TableAsset::register ( $this );
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
   #access_1 table tr td{padding: 3px 3px;}
   #access_2 table tr td{padding: 3px 3px;}
</style>

<section id="widget-grid" class="">
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark"
                 data-widget-deletebutton="false"
                 data-widget-editbutton="false"
                 data-widget-colorbutton="false"
                 data-widget-sortable="false"
                >
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div class="jarviswidget-ctrls">
                        <a class="button-icon" href="javascript:history.go(-1);" data-target="#SaveMenu" rel="tooltip" data-original-title="返回上一步" data-placement="bottom">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                </header>
                <!-- widget div-->
                <div class="well well-sm well-light">
                    <div id="tabs" >
                        <ul>
                            <li><a href="#access_1">刷卡监测</a></li>
                            <li><a href="#access_2">门禁监测</a></li>
                            <li><a href="#access_3" id="read_card">刷卡记录</a></li>
                            <li><a href="#access_4" id="door_alarm">门禁记录</a></li>
                        </ul>

                        <div id="access_1">
                            <table id="datatable_1" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th data-class="expand">时间</th>
                                    <th data-class="expand">卡号</th>
                                    <th data-class="expand">持卡人</th>
                                    <th data-class="expand">门禁</th>
                                    <th data-class="expand">消息</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="access_2">
                            <table id="datatable_2" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th data-class="expand">门禁名</th>
                                    <th data-class="expand">时间</th>
                                    <th data-class="expand">消息</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div id="access_3"></div>

                        <div id="access_4"></div >
                    </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>


<script>

	window.onload = function() {
		var oTable;
        $("#read_card").click(function(){
            getReadingCardInfo();
        })

        $("#door_alarm").click(function(){
            getDoorAlarmInfo();
        })
		$(document).ready(function () {
            $('#tabs').tabs({
                activate: function(event, ui) {
                    ttInstances = TableTools.fnGetMasters();
                    for (i in ttInstances) {
                        if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                    }
                }
            });
            getRealTimeReadingCardInfo();
            getRealTimeDoorAlarmInfo();
		});

        /**
         * 获取刷卡实时信息
         * @private
         */
        function getRealTimeReadingCardInfo() {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/route/get-real-time-read-card-info?area=<?=$area;?>',
                success: function (data) {
                    var str = '';
                    for(var i in data){
                        str += "<tr><td>" + data[i].timestamp+"</td><td>" + data[i].access_card_number+"</td><td>" + data[i].last_name+"</td><td>" + data[i].name+"</td><td>"+ data[i].message+"</td></tr>";
                    }
                    $("#datatable_1 tbody").html("");
                    $("#datatable_1 tbody").append(str);
                },
                complete: function (XHR, TS) { XHR = null }
            });
            getRealTimeDoorAlarmInfo();
        }

        /**
         * 获取门禁实时报警信息
         * @private
         */
        function getRealTimeDoorAlarmInfo() {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/route/get-real-time-door-alarm-info?area=<?=$area;?>',
                success: function (data) {
                    var str = '';
                    for(var i in data){
                        str += "<tr><td>" + data[i].name+"</td><td>" + data[i].timestamp+"</td><td>" + data[i].message+"</td></tr>";
                    }
                    $("#datatable_2 tbody").html("");
                    $("#datatable_2 tbody").append(str);
                },
                complete: function (XHR, TS) { XHR = null }
            });
            return false;
        }

        /**
         * 获取刷卡信息
         * @private
         */
        function getReadingCardInfo() {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/route/get-read-card-info?area=<?=$area;?>',
                success: function (data) {
                    var str = "<table id='datatable_3' class='table table-striped table-bordered table-hover'><thead><tr><th data-class='expand'>时间</th><th data-class='expand'>卡号</th><th data-class='expand'>持卡人</th><th data-class='expand'>门禁</th><th data-class='expand'>消息</th></tr></thead><tbody>";
                    for(var i in data){
                        str += "<tr><td>" + data[i].timestamp+"</td><td>" + data[i].access_card_number+"</td><td>" + data[i].last_name+"</td><td>" + data[i].name+"</td><td>"+ data[i].message+"</td></tr>";
                    }
                    str +="</tbody></table>";
                    $("#access_3").html("");
                    $("#access_3").append(str);
                    initTable('datatable_3');
                }
            });
        }

        /**
         * 获取门禁报警信息
         * @private
         */
        function getDoorAlarmInfo() {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/route/get-door-alarm-info?area=<?=$area;?>',
                success: function (data) {
                    var str = "<table id='datatable_4' class='table table-striped table-bordered table-hover'><thead><tr><th data-class='expand'>门禁名</th><th data-class='expand'>时间</th><th data-class='expand'>消息</th></tr></thead><tbody>";
                    for(var i in data){
                        str += "<tr><td>" + data[i].name+"</td><td>" + data[i].timestamp+"</td><td>" + data[i].message+"</td></tr>";
                    }
                    str +="</tbody></table>";
                    $("#access_4").html("");
                    $("#access_4").append(str);
                    initTable("datatable_4");
                }
            });
        }

		function initTable(table) {
			/* TABLETOOLS */
			table = $('#'+table).dataTable({
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-2'f><'col-xs-12 col-sm-4'><'col-sm-6 col-xs-4 hidden-xs'T>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'li><'col-sm-6 col-xs-12'p>>",
				//"sdom": "Bfrtip",
				"oTableTools": {
					"aButtons": [
						"copy",
						//"csv",
						"xls",
						{
							"sExtends": "print",
							"sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
						}
					],
					"sRowSelect": "os",
					"sSwfPath": "../js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
				},
                "bSort": false,
				"autoWidth": true,
				"lengthMenu": [[5,14,15, 16, 20, 25, 30, -1], [5,14,15, 16, 20, 25, 30, "All"]],
				"iDisplayLength": 15,
				"language": {
					"sProcessing": "处理中...",
					"sClear":"test",
					"sLengthMenu": "显示 _MENU_ 项结果",
					"sZeroRecords": "没有匹配结果",
					"sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
					"sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
					"sInfoFiltered": "(由 _MAX_ 项结果过滤)",
					"sInfoPostFix": "",
					"sSearch": "搜索:",
					"sUrl": "",
					"sEmptyTable": "表中数据为空",
					"sLoadingRecords": "载入中...",
					"sInfoThousands": ",",
					"oPaginate": {
						"sFirst": "首页",
						"sPrevious": "上页",
						"sNext": "下页",
						"sLast": "末页"
					},
					"oAria": {
						"sSortAscending": ": 以升序排列此列",
						"sSortDescending": ": 以降序排列此列"
					}
				}
			});
			/* END TABLETOOLS */
			return table;
		}

        setInterval(getRealTimeReadingCardInfo,7000);
	}
</script>
