<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Index_ele');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #demo-pill-nav{font-size: 50px;}
    #tab-A12 ul li span{display:inline-block;background-color:#000000; width:inherit;color: white; font-size: 18px;width: 120px;border: 1px solid #EC9519}
    #tab-A12 ul li span a{text-decoration: none;color:inherit;}
    #tab-A16 ul li span{display:inline-block;background-color:#000000; width:inherit;color: white; font-size: 18px;width: 120px;border: 1px solid #EC9519}
    #tab-A16 ul li span a{text-decoration: none;color:inherit;}
    #tab-A45 ul li span{display:inline-block;background-color:#000000; width:inherit;color: white; font-size: 18px;width: 120px;border: 1px solid #EC9519}
    #tab-A45 ul li span a{text-decoration: none;color:inherit;}
    #tab-A29 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 120px;border: 1px solid #EC9519}
    #tab-A29 ul li span a{text-decoration: none;color:inherit;}
    #tab-A34 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 120px;border: 1px solid #EC9519}
    #tab-A34 ul li span a{text-decoration: none;color:inherit;}
    #tab-A42 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width:120px;border: 1px solid #EC9519}
    #tab-A42 ul li span a{text-decoration: none;color:inherit;}
    /*#myTabContent{background: url("/img/background.jpg");background-size: 100% 100%;}*/
    #myTabContent{background-color:#222222;background-size: 100% 100%;}
    #demo-pill-nav li{border: 1px solid #EC9519}
    .badge{border-radius:0px;}
</style>
<section id="widget-grid" class="">

    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget-color-blueDark jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div id="myTabContent" class="widget-body" style="height: 700px;">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                <li id="A12">
                                    <a href="#tab-A12" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A12地块</span> </a>
                                </li>
                                <li id="A16">
                                    <a href="#tab-A16" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A16地块</span></a>
                                </li>
                                <li id="A29">
                                    <a href="#tab-A29" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A29地块</span></a>
                                </li>
                                <li id="A34">
                                    <a href="#tab-A34" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A34地块</span></a>
                                </li>
                                <li id="A42">
                                    <a href="#tab-A42" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A42地块</span></a>
                                </li>
                                <li id="A45">
                                    <a href="#tab-A45" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A45地块</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" style="height: 700px; overflow-y: auto;overflow-x: auto">
                                <div class="tab-pane" id="tab-A12" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px"><i class="fa fa-lg fa-minus-circle"></i> 电力检测</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-minus-circle"></i> 高压配电图</span>
                                                        <ul>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1860"><span style="width: 210px"> <i></i>高压系统1</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1861"><span style="width: 210px"> <i></i>高压系统2</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1862"><span style="width: 210px"> <i></i>高压系统3</span></a>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-minus-circle"></i> 低压配电图</span>
                                                        <ul>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1853"><span style="width: 210px"> <i></i>1#配电室1#变压器</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1854"><span style="width: 210px"> <i></i>1#配电室2#变压器</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1855"><span style="width: 210px"> <i></i>2#配电室1#变压器</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1857"><span style="width: 210px"> <i></i>2#配电室2#变压器</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1858"><span style="width: 210px"> <i></i>3#配电室1#变压器</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1859"><span style="width: 210px"> <i></i>3#配电室2#变压器</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A16" style="margin-top:27px;margin-left: 40px">
                                </div>
                                <div class="tab-pane" id="tab-A29" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px"><i class="fa fa-lg fa-minus-circle"></i> 电力检测</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-minus-circle"></i> 低压配电图</span>
                                                        <ul>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=1464"><span style="width: 210px"> <i></i>A1A2变电所低压系统图</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=893"><span style="width: 210px"> <i></i>A1A2站温控器系统图</span></a>
                                                            </li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=893"><span style="width: 210px"> <i></i>A1A2站温控器系统图</span></a>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-minus-circle"></i> 直流屏</span>
                                                        <ul>
                                                            <li><a href="/subsystem/crud/view?id=669"><span style="width: 210px"> <i></i>A1站UPS1控制器信息图</span></a></li>
                                                            <li>
                                                                <a href="/subsystem/crud/view?id=671"><span style="width: 210px"> <i></i>A1站UPS2控制器信息图</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A34" style="margin-top:27px;margin-left: 20px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li id="A34-floorcon"  style="width: 22%; float: left">
                                                <span style="width: 140px"><i class="fa fa-lg fa-minus-circle"></i> 高压配电图</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=847"><span> <i></i> 高压配电图</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A34-floorcon"  style="width: 22%; float: left">
                                                <span style="width: 140px"><i class="fa fa-lg fa-minus-circle"></i> 低压配电图</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1856"><span> <i></i> 低压配电图1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1850"><span> <i></i> 低压配电图2</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1851"><span> <i></i> 低压配电图3</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1852"><span> <i></i> 低压配电图4</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A42" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li id="A42-building" style="width: 22%; float: left">
                                                <span style="width: 150px;"><i class="fa fa-lg fa-minus-circle"></i> 高压配电图</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=765"><span> <i></i>中压配电图</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A42-building" style="width: 22%; float: left">
                                                <span style="width: 150px;"><i class="fa fa-lg fa-minus-circle"></i> 低压配电图</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1842"><span> <i></i>低压配电图1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1847"><span> <i></i>低压配电图2</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1848"><span> <i></i>低压配电图3</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1849"><span> <i></i>总配系统图</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=764"><span> <i></i>温控仪</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A42-building" style="width: 22%; float: left">
                                                <span style="width: 150px;"><i class="fa fa-lg fa-minus-circle"></i> 直流屏</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=689"><span> <i></i>直流屏</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="tab-A45" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li  style="width: 22%; float: left">
                                                <span style="width: 150px;"><i class="fa fa-lg fa-minus-circle"></i> 电力检测</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=689"><span> <i></i>高压配电图</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=898"><span> <i></i>低压配电图</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=690"><span> <i></i>D2直流屏</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>

<script type="text/javascript">

    window.onload = function (){
        
        $(document).ready(function() {
            getActive();
            $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
            $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                }
                e.stopPropagation();
            });
        });
        
        $('#demo-pill-nav li').click(function (){
                var val = '';
                val = this.id;
                $.ajax({
                    type:'get',
                    url:'/route/set-active',
                    data:"active="+val,
                    success:function (msg) {
                    }
                });

            });
        
        function getActive(){
            $(".tab-content div").each(function(){
                $(this).removeClass('active');
            });
            $("#demo-pill-nav li").each(function(){
                $(this).removeClass('active');
            });
            $.ajax({
                type:'get',
                url:'/route/get-active',
                success:function (msg) {
                    $("#"+msg).addClass('active');
                    $("#tab-"+msg).addClass('active');
                }
            });
        }
    }

</script>