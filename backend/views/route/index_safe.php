<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Index_safe');
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    #demo-pill-nav{font-size: 50px;}
    #tab-A45 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A45 ul li span a{text-decoration: none;color:inherit;}
    #tab-A16 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A16 ul li span a{text-decoration: none;color:inherit;}
    #tab-A12 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A12 ul li span a{text-decoration: none;color:inherit;}
    #tab-A29 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 120px}
    #tab-A29 ul li span a{text-decoration: none;color:inherit;}
    #tab-A34 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width: 90px}
    #tab-A34 ul li span a{text-decoration: none;color:inherit;}
    #tab-A42 ul li span{display:inline-block;background-color: #000000; width:inherit;color: white; font-size: 18px;width:100px}
    #tab-A42 ul li span a{text-decoration: none;color:inherit;}
    /*#myTabContent{background: url("/img/background.jpg");background-size: 100% 100%;}*/
    #myTabContent{background-color:#000000;background-size: 100% 100%;}

    #A29-water ul li span{width:80px}
    #A34-others ul li span{width:110px}
    #A34-ele ul li span{width:100px}
    #A34-floorcon ul li span{width:120px}
    #A42-water ul li span{width:140px}
    #A45-floorcon ul li span{width:100px}
    #A45-others ul li span{width:110px}
    #A29-kongtiao ul li span{width:120px}
    #A42-building ul li span{width:120px;}

    .badge{border-radius:0px;}

</style>
<section id="widget-grid" class="">

    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget-color-blueDark jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div id="myTabContent" class="widget-body" style="height: 700px;">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs tabs-left" id="demo-pill-nav" >
                                <li id="A12">
                                    <a href="#tab-A12" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A12地块</span> </a>
                                </li>
                                <li id="A16">
                                    <a href="#tab-A16" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A16地块</span></a>
                                </li>
                                <li id="A29">
                                    <a href="#tab-A29" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A29地块</span></a>
                                </li>
                                <li id="A34">
                                    <a href="#tab-A34" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A34地块</span></a>
                                </li>
                                <li id="A42">
                                    <a href="#tab-A42" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A42地块</span></a>
                                </li>
                                <li id="A45">
                                    <a href="#tab-A45" data-toggle="tab"><span class="badge bg-color-blueDark txt-color-white" style="font-size: 30px;width:150px;">A45地块</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" style="height: 700px; overflow-y: auto;overflow-x: auto">
                                <div class="tab-pane" id="tab-A12" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> A区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1144"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1145"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1147"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1150"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1155"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1157"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1159"><span> <i></i>7F</span></a>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>B区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1161"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1163"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1165"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1167"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>5F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2297"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2298"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>6F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2299"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2300"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>7F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2301"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2302"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>8F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2303"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2304"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>9F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2305"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2306"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>10F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2307"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2308"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>11F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2309"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2311"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>C区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1215"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1219"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1220"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1222"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1225"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1227"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1229"><span> <i></i>7F</span></a>
                                                            </li>

                                                        </ul>
                                                    </li>

                                                    <li style="display:">
                                                        <a href="/subsystem/crud/view?id=2019"><span> <i></i>地下</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>A区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1365"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1431"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1432"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1433"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1434"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1435"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1436"><span> <i></i>7F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>B区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1437"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1438"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1439"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1440"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>5F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1441"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1442"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>6F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1443"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1444"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>7F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1445"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1446"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>8F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1447"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1448"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>9F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1449"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1450"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>10F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1451"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1452"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>11F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1453"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=1454"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>C区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1455"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1456"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1457"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1458"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1459"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1460"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1461"><span> <i></i>7F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="display:">
                                                        <a href="/route/access-card?area=12"><span> <i></i>门禁记录</span></a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left;display: ">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 入侵报警</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>A区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2008"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2009"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2010"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2011"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2012"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2013"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2014"><span> <i></i>7F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2015"><span> <i></i>8F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2016"><span> <i></i>9F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2017"><span> <i></i>10F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2018"><span> <i></i>11F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>B区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2020"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2021"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id="><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=2034"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li>
                                                                <span><i class="fa fa-lg fa-plus-circle"></i>5F</span>
                                                                <ul>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2037"><span> <i></i>B1</span></a>
                                                                    </li>
                                                                    <li style="display:none">
                                                                        <a href="/subsystem/crud/view?id=2037"><span> <i></i>B2</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>


                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i>C区</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>4F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>5F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>6F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=#"><span> <i></i>7F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>


                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left;display: ">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 其他</span>
                                                <ul>
                                                    <li style=" float: left;display: ">
                                                        <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 周界防范</span>
                                                        <ul>
                                                            <li style="display:">
                                                                <a href="/subsystem/crud/view?id=1352"><span> <i></i>A区</span></a>
                                                            </li>
                                                            <li style="display:">
                                                                <a href="/subsystem/crud/view?id=1354"><span> <i></i>B区</span></a>
                                                            </li>
                                                            <li style="display:">
                                                                <a href="/subsystem/crud/view?id=1356"><span> <i></i>C区</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="display:">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>火灾报警</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A16" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                    <ul>
                                    <li style="width: 22%; float: left">
                                        <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i> A区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2118"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2119"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2120"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2121"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2123"><span> <i></i>5F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2124"><span> <i></i>6F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2125"><span> <i></i>7F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2143"><span> <i></i>8F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2149"><span> <i></i>9F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2151"><span> <i></i>10F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2153"><span> <i></i>11F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2155"><span> <i></i>12F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2157"><span> <i></i>13F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2162"><span> <i></i>14F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2166"><span> <i></i>15F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2167"><span> <i></i>16F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2170"><span> <i></i>17F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2171"><span> <i></i>18F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2173"><span> <i></i>19F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2175"><span> <i></i>20F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2177"><span> <i></i>21F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2178"><span> <i></i>22F</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>B区</span>
                                                <ul>

                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2126"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2128"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2130"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2132"><span> <i></i>4F</span></a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>C区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2168"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2174"><span> <i></i>B2</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2134"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2136"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2138"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2140"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2141"><span> <i></i>5F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2144"><span> <i></i>6F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2146"><span> <i></i>7F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2150"><span> <i></i>8F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2154"><span> <i></i>9F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2158"><span> <i></i>10F</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>D区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2214"><span> <i></i>B1-b</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2209"><span> <i></i>B2</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2211"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id="><span> <i></i>4F-b</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2203"><span> <i></i>3F-b</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2192"><span> <i></i>2F-b</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2186"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2189"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2199"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=2205"><span> <i></i>4F</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="display:">
                                                <a href="/subsystem/crud/view?id=2183"><span> <i></i>A.B区地下一层</span></a>
                                            </li>
                                            <li style="display:">
                                                <a href="/subsystem/crud/view?id=2081"><span> <i></i>A.B区地下二层</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li style="width: 22%; float: left">
                                        <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>A区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>地下一层</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>地下二层</span></a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>C区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>地下一层</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>地下二层</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>3F</span></a>
                                                    </li>



                                                </ul>
                                            </li>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>D区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>地下一层</span></a>
                                                    </li>

                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>4F</span></a>
                                                    </li>


                                                </ul>
                                            </li>
                                            <li style="display:none">
                                                <a href="/route/access-card?area=16"><span> <i></i>门禁记录</span></a>
                                            </li>

                                        </ul>
                                    </li>
                                    <li style="width: 22%; float: left;display: ">
                                        <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 入侵报警</span>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>A区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>5F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>6F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>7F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>8F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>9F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>10F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>11F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>12F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>13F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>14F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>15F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>16F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>17F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>18F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>19F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>20F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>21F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>22F</span></a>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li>
                                                <span><i class="fa fa-lg fa-plus-circle"></i>D区</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="display:none">
                                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>4F</span></a>
                                                    </li>

                                                </ul>
                                            </li>


                                        </ul>
                                    </li>
                                    <li style="display:">
                                        <a href="/subsystem/crud/view?id=#"><span> <i></i>火灾报警</span></a>
                                    </li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A29" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=143"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=148"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=153"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=164"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=165"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=167"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=168"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=169"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=208"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=210"><span> <i></i>2F</span> </a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=212"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=213"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=214"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=215"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=220"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=221"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=348"><span> <i></i>B1门禁</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=29"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left;display: none">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 动力中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=854"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=853"><span> <i></i>2F</span> </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=858"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=857"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=856"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=855"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=862"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=861"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=860"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=859"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width:22%;float: left">
                                                <a href="/route/firm-alarm"><span  style="width: 150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 入侵报警</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 数据中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1670"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1672"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1673"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1674"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 办公楼</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1666"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1667"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1668"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1669"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>

                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 动力中心</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=1675"><span> <i></i>1F</span></a>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A34" style="margin-top:27px;margin-left: 20px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=745"><span> <i></i>B1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=713"><span> <i></i>F1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=714"><span> <i></i>F2</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=715"><span> <i></i>F3</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=716"><span> <i></i>F4</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=717"><span> <i></i>F5</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=729"><span> <i></i>F6</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=733"><span> <i></i>F7</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=734"><span> <i></i>F8</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=735"><span> <i></i>F9</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=736"><span> <i></i>F10</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=737"><span> <i></i>F11</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=738"><span> <i></i>F12</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=746"> <span> <i></i>B1</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=747"> <span> <i></i>F1</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=34"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A34-others" style="width:22%;float: left;display: ;">
                                                <span style="width: 140px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 入侵报警</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1990"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1991"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1992"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1993"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=1994"><span> <i></i>5F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2002"><span> <i></i>6F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2003"><span> <i></i>7F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2004"><span> <i></i>8F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2005"><span> <i></i>9F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2006"><span> <i></i>10F</span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=2007"><span> <i></i>11F</span></a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li style="width:22%;float: left">
                                                <a href="/route/fire-alarm"><span  style="width: 150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li style="float: left">
                                                <a href="/subsystem/crud/view?id=740"><span  style="width: 150px;"> <i></i>周界防范</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-A42" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=504"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=505"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=506"><span> <i></i>3F </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=507"><span> <i></i>4F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=530"><span> <i></i>地下</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=509"><span> <i></i>1F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=512"><span> <i></i>2F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=513"><span> <i></i>3F</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=514"><span> <i></i>4F </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=508"><span> <i></i>地下</span> </a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=42"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li id="A42-others" style="width:22%;float: left;display: none">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li style="">
                                                        <a href="/subsystem/crud/view?id=528"><span> <i></i>地下 </span></a>
                                                    </li>
                                                    <li style="">
                                                        <a href="/category/crud/location-only-view?id=441"><span> <i></i>楼顶</span> </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 入侵报警</span>
                                                <ul>
                                                    <li >
                                                        <a href="/subsystem/crud/view?id=1926"><span > <i></i>1F</span></a>
                                                    </li>

                                                    <li >
                                                        <a href="/subsystem/crud/view?id=1986"><span > <i></i>2F</span></a>
                                                    </li>
                                                    <li >
                                                        <a href="/subsystem/crud/view?id=1988"><span > <i></i>3F</span></a>
                                                    </li>

                                                    <li >
                                                        <a href="/subsystem/crud/view?id=1989"><span > <i></i>4F</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 其他</span>
                                                <ul>
                                                    <li >
                                                        <a href="/route/fire-alarm"><span > <i></i>火灾报警</span></a>
                                                    </li>

                                                    <li >
                                                        <a href="/subsystem/crud/view?id=515"><span > <i></i>周界防范</span></a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane  active" id="tab-A45" style="margin-top:27px;margin-left: 40px">
                                    <div class="tree smart-form">
                                        <ul>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 视频监控</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 9#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=127"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=152"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=128"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=129"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=65"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=68"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=74"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=87"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 14#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=135"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=136"><span> <i></i>2F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="/subsystem/crud/view?id=138"><span> <i></i> B1</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 门禁系统</span>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=157"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 11#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=158"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=159"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=160"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 12#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=161"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=162"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=163"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li style="">
                                                        <a href="/route/access-card?area=45"><span> <i></i>门禁记录</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li ID="A45-others" style=" float: left;display: none">
                                                <span style="width: 150px;background-color: #000000"><i class="fa fa-lg fa-minus-circle"></i> 火灾报警</span>
                                                <ul>
                                                    <li style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 9.10#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=225"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=226"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=227"><span> <i></i>3F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/subsystem/crud/view?id=228"><span> <i></i>4F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" " >
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 11#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=209"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=210"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=211"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 12#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=213"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=214"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=221"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 13#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=217"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=218"><span> <i></i>2F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=219"><span> <i></i>3F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        <span><i class="fa fa-lg fa-plus-circle"></i> 14#</span>
                                                        <ul>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=221"><span> <i></i>1F</span></a>
                                                            </li>
                                                            <li style="display:none">
                                                                <a href="/category/crud/location-only-view?id=222"><span> <i></i>2F</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li  style=" ">
                                                        </i><a href="/subsystem/crud/view?id=223"><span> <i></i> B1</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <a href="/route/fire-alarm"><span style="width:150px;"> <i></i>火灾报警</span></a>
                                            </li>
                                            <li style="width: 22%; float: left">
                                                <a href="/subsystem/crud/view?id=2040"><span style="width:150px;"> <i></i>锅炉</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>

<script type="text/javascript">

    window.onload = function (){

        $(document).ready(function() {

            getActive();
            $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
            $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(':visible')) {
                    children.hide('fast');
                    $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                } else {
                    children.show('fast');
                    $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                }
                e.stopPropagation();
            });

        });

        $('#demo-pill-nav li').click(function (){
            var val = '';
            val = this.id;
            $.ajax({
                type:'get',
                url:'/route/set-active',
                data:"active="+val,
                success:function (msg) {
                }
            });

        });

        function getActive(){
            $(".tab-content div").each(function(){
                $(this).removeClass('active');
            });
            $("#demo-pill-nav li").each(function(){
                $(this).removeClass('active');
            });
            $.ajax({
                type:'get',
                url:'/route/get-active',
                success:function (msg) {
                    $("#"+msg).addClass('active');
                    $("#tab-"+msg).addClass('active');
                }
            });

        }
    }

</script>