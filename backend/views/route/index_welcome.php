<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Index_all');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">

    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget-color-blueDark jarviswidget" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                <header>
                   <span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
                    <h2><?= Html::encode($this->title) ?></h2>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body">
                        <div id="myCarousel" class="carousel slide" style="width: 100%;float:left" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                <li data-target="#myCarousel" data-slide-to="4"></li>
                                <li data-target="#myCarousel" data-slide-to="5"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="/img/block/area/A-42.jpg" style="width: 100%;height: 700px" data-src=" " alt="First slide">
                                </div>
                                <div class="item">
                                    <img src="/img/block/area/A-16.jpg" style="width: 100%;height: 700px" data-src="" alt="Second slide">
                                </div>
                                <div class="item">
                                    <img src="/img/block/area/A-29.jpg" style="width: 100%;height: 700px" data-src="" alt="Third slide">
                                </div>
                                <div class="item">
                                    <img src="/img/block/area/A-34.jpg" style="width: 100%;height: 700px" data-src="" alt="foue slide">
                                </div>
                                <div class="item">
                                    <img src="/img/block/area/A-12.jpg" style="width: 100%;height: 700px" data-src="" alt="five slide">
                                </div>
                                <div class="item">
                                    <img src="/img/block/area/A-45_wu.jpg" style="width: 100%;height: 700px" data-src="" alt="six slide">
                                </div>
                            </div>
                            <div class="container">
                                <div class="carousel-caption">
                                    <p>
                                        <font size="3">中国石油的标识为红黄两色构成的十等分花瓣图形。<br>
                                            象征中国石油主营业务的集合。红色基底突显方形一角，不仅表现中国石油的雄厚基础，而且蕴育着中国石油无限的凝聚力和创造力。外观呈花朵状，体现了中国石油创造能源与环境和谐的社会责任。</p>
                                    </font>
                                    <p style="font-size:36px;">
                                        <a class="btn btn-lg btn-primary" href="/route/link?name=1" role="button" style="font-size: 30px">电力监控</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-lg btn-primary" href="/route/link?name=3" role="button" style="font-size: 30px">安全管理</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-lg btn-primary" href="/route/link?name=2" role="button" style="font-size: 30px">楼宇自控</a>
                                    </p>
                                </div>
                            </div>
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->

    </div>

</section>
