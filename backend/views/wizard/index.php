<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 14-12-8
 * Time: 上午9:43
 */
use common\library\MyFunc;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

use backend\models\Protocol;
use backend\models\Bacnet;
use backend\models\Coologic;
use backend\models\Station;

//当前页面 数据提取
$station = new station();
$protocol = ArrayHelper::map(Protocol::getProtocol(), 'id', 'name');
$bacnet = ArrayHelper::map(Bacnet::getAll(), 'id', 'name');
$coologic = ArrayHelper::map(Coologic::getAll(), 'id', 'name');
?>


<style>
    .bootstrapWizard li {width:20%}
    .select2-input {margin-top:-20px}
</style>
<!-- NEW WIDGET START -->
<!-- NEW WIDGET START -->
<article class="col-sm-12 col-md-12 col-lg-12">

<!-- Widget ID (each widget will need unique ID)-->
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
<!-- widget options:
usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

data-widget-colorbutton="false"
data-widget-editbutton="false"
data-widget-togglebutton="false"
data-widget-deletebutton="false"
data-widget-fullscreenbutton="false"
data-widget-custombutton="false"
data-widget-collapsed="true"
data-widget-sortable="false"

-->
<header>
    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
    <h2><?=Yii::t('app','初始化')?></h2>
</header>

<!-- widget div-->
<div>

<!-- widget edit box -->
<div class="jarviswidget-editbox">
    <!-- This area used as dropdown edit box -->

</div>
<!-- end widget edit box -->

<!-- widget content -->
<div class="widget-body">

<div class="row">
<?php
$form = ActiveForm::begin([
        'id' => "wizard-1",
        'enableAjaxValidation' => false,
        'action' => 'wizard/initialize',
        'options' => ['enctype' => 'multipart/form-data'],
    ]);
?>
            <div id="bootstrap-wizard-1" class="col-sm-12">
                <div class="form-bootstrapWizard">
                    <ul class="bootstrapWizard form-wizard">
                        <li class="active" data-target="#step1">
                            <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">站点配置</span> </a>
                        </li>
                        <li data-target="#step2">
                            <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">采集设备添加</span> </a>
                        </li>
                        <li data-target="#step3">
                            <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">点位设置</span> </a>
                        </li>
                        <li data-target="#step4">
                            <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">用户修改</span> </a>
                        </li>
                        <li data-target="#step5">
                            <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title">完成</span> </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>


                <div class="tab-content">
                    <!------------------------------------------    站点配置---------------------------------------------------------->
                    <div class="tab-pane active" id="tab1">
                        <br>
                        <h3><strong>第一步 </strong> - 站点 配置</h3>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-home fa-lg fa-fw"></i></span>
                                        <?=Html::textInput('station[name]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Station Name')])?>
<!--                                        <input class="form-control input-lg" placeholder=--><?//= Yii::t('app', 'Station Name')?><!-- type="text" name="station[name]" id="station_name">-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag fa-lg fa-fw"></i></span>
                                        <?=Html::textInput('station[country]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Country')])?>
<!--                                        <input class="form-control input-lg" placeholder=--><?//= Yii::t('app', 'Country')?><!-- type="text" name="station[country]" id="country">-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-institution fa-lg fa-fw"></i></span>
                                        <?=Html::textInput('station[city]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'City')])?>
<!--                                        <input class="form-control input-lg" placeholder=--><?//= Yii::t('app', 'City')?><!-- type="text" name="station[city]" id="city">-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group1">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-language fa-lg fa-fw"></i></span>
                                            <select name="country" class="form-control input-lg">
                                                <option value="zh-cn">中文</option>
                                                <option value="en-us">English</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group1">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-language fa-lg fa-fw"></i></span>
                                            <?=Html::textInput('station[station_id]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', '站点编号')])?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group1">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-language fa-lg fa-fw"></i></span>
                                            <?=Html::textInput('station[ip]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'IP')])?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- 没搞出来
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group1">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-file-image-o fa-lg fa-fw"></i></span>
                                            <?//=Html::fileInput('image_url', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Image')])?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group1">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-file-image-o fa-lg fa-fw"></i></span>
                                            <?//=Html::fileInput('logo_url', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Logo')]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
-->


                    </div>

                    <!----------------------------------------------      采集协议添加---------------------------------------------------------------->
                    <div class="tab-pane" id="tab2">
                        <br>
                        <h3><strong>第二步 </strong> - 采集设备添加</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-th-list fa-lg fa-fw"></i></span>
                                        <?=Html::dropDownList('protocol',
                                                               null,
                                                               MyFunc::SelectDataAddArrayTop($protocol),
                                                               ['class' => 'select2', 'id' => 'protocol', 'placeholder' => Yii::t('app', 'Select protocol')])?>
<!--                                        <select name="protocol" id = 'protocol' placeholder = '选择采集协议'  class="select2">-->
<!--                                            <option></option>-->
<!--                                            <option value="1">bcu</option>-->
<!--                                            <option value="2">sc</option>-->
<!--                                        </select>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <!-----------------------------  协议对应的 配置  无法确定 各种协议需要什么样的配置 目前采用直接上Html代码------------------------------------------>
                        <!------------------ bcu  page------------------->
                        <div id = 'bacnet' class = 'pro' style = 'display:none'>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-rss-square fa-lg fa-fw"></i></span>
                                            <?=Html::dropDownList('equipment[]',
                                                null,
                                                MyFunc::SelectDataAddArrayTop($bacnet),
                                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Select enabled device').'(bacnet)'])?>
<!--                                            <select name="equipment[]" multiple placeholder = '选择启用设备(bcu)'  class="select2">-->
<!--                                                <option></option>-->
<!--                                                <option value="1">bcu</option>-->
<!--                                                <option value="2">sc</option>-->
<!--                                            </select>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = 'row _bacent'>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw"></i></span>
                                            <?=Html::textInput('bcu_name[]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app','Name')])?>
<!--                                            <input class="form-control input-lg" placeholder="名称" type="text" name="bac_name[]" id="bac_name[]">-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-desktop fa-lg fa-fw"></i></span>
                                            <?=Html::textInput('ip[]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Ip address')])?>
<!--                                            <input class="form-control input-lg"  placeholder="ip 地址" type="text" name="ip[]" id="ip[]">-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-support fa-lg fa-fw"></i></span>
                                            <?=Html::textInput('port[]', null, ['class' => 'form-control input-lg', 'placeholder' => Yii::t('app', 'Port')])?>
<!--                                            <input class="form-control input-lg" placeholder="端口"  type="text" name="port[]" id="port[]">-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="_plus" style = 'margin-top:10px'>
                                        <i class="fa fa-lg fa-plus-circle"></i>
                                    </div>

                                    <div class="_minus" style = 'margin-top:10px;display:none'>
                                        <i class="fa fa-lg fa-minus-circle"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!----coologic     page------------>
                        <div id = 'coologic' class = 'pro' style = 'display:none'>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-rss-square fa-lg fa-fw"></i></span>
                                            <?=Html::dropDownList('equipment[]',
                                                null,
                                                MyFunc::SelectDataAddArrayTop($coologic),
                                                ['class' => 'select2', 'placeholder' => Yii::t('app', 'Select enabled device').'(coologic)'])?>
<!--                                            <select name="equipment[]" multiple placeholder = '选择启用设备(sc)'  class="select2">-->
<!--                                                <option></option>-->
<!--                                                <option value="1">bcu</option>-->
<!--                                                <option value="2">sc</option>-->
<!--                                            </select>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = 'row' class = '_more'>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw"></i></span>
                                            <input class="form-control input-lg" placeholder="名称" type="text" name="location[]" id="port_num">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-support fa-lg fa-fw"></i></span>
                                            <input class="form-control input-lg" placeholder="端口"  type="text" name="port_num[]" id="port_num">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="_plus" style = 'margin-top:10px'>
                                        <i class="fa fa-lg fa-plus-circle"></i>
                                    </div>

                                    <div class="_minus" style = 'margin-top:10px;display:none'>
                                        <i class="fa fa-lg fa-minus-circle"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--------------------------------------       点位设置----------------------------------------------------------->
                    <div class="tab-pane" id="tab3">
                        <br>
                        <h3><strong>第三步 </strong> - 点位设置</h3>
                        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                            <!-- widget div-->
                            <div>
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <!-- end widget edit box -->
                                <!-- widget content -->
                                <div class="widget-body no-padding" style="height:350px;overflow-y:auto ">

                                    <div class="alert alert-info no-margin fade in">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>
                                        设置点位是否上传、屏蔽、记录,若不设置默上传、记录数据，不屏蔽
                                    </div>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>点位名</th>
                                            <th title="是否上传数据">是否上传</th>
                                            <th title="是否屏蔽数据">是否屏蔽</th>
                                            <th title="是否记录数据">是否记录</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($point_all as $v){ ?>
                                        <tr>
                                            <td><?=$v['id']?></td>
                                            <td><?=MyFunc::DisposeJSON($v['name'])?></td>
                                            <td>
                                                <input type="radio" name="Point[<?=$v['id']?>][is_upload]" value="1" <?php if($v['is_upload']) echo 'checked';?> >是&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="Point[<?=$v['id']?>][is_upload]" value="0" <?php if(!$v['is_upload']) echo 'checked';?>>否
                                            </td>
                                            <td>
                                                <input type="radio" name="Point[<?=$v['id']?>][is_shield]" value="1" <?php if($v['is_shield']) echo 'checked';?>>是&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="Point[<?=$v['id']?>][is_shield]" value="0" <?php if(!$v['is_shield']) echo 'checked';?>>否
                                            </td>
                                            <td>
                                                <input type="radio" name="Point[<?=$v['id']?>][is_record]" value="1" <?php if($v['is_record']) echo 'checked';?>>是&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="Point[<?=$v['id']?>][is_record]" value="0" <?php if(!$v['is_record']) echo 'checked';?>>否
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </div>
                    <!--------------------------------------    用户修改----------------------------------------------------->
                    <div class="tab-pane" id="tab4">
                        <br>
                        <h3><strong>第四步 </strong> -用户修改</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag fa-lg fa-fw"></i></span>
                                        <input class="form-control input-lg" placeholder="用户名"  type="text" name="user[username]" id="username">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw"></i></span>
                                        <input class="form-control input-lg" placeholder="密码"  type="password" name="user[password]" id="password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-lg fa-fw"></i></span>
                                        <input class="form-control input-lg" placeholder="邮箱" type="text" name="user[email]" id="email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-mobile fa-lg fa-fw"></i></span>
                                        <input class="form-control input-lg" data-mask="999-9999-9999" placeholder="电话" type="text" name="user[phone_number]" id="hphone">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!----------------------------------------------     完成------------------------------------------------------------->
                    <div class="tab-pane" id="tab5">
                        <br>
                        <h3><strong>第五步</strong> -提交完成</h3>
                        <br>
                        <h1 class="text-center text-success"><strong><i class="fa fa-check fa-lg"></i> <?=Yii::t('app','完成')?></strong></h1>
                        <h4 class="text-center"><?=Yii::t('app','Click finish ')?></h4>
                        <br>
                        <br>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="pager wizard no-margin">
<!--                                    <li class="previous first disabled">-->
<!--                                    <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>-->
<!--                                    </li>-->
                                    <li class="previous disabled">
                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
                                    </li>
<!--                                    <li class="next last">-->
<!--                                    <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>-->
<!--                                    </li>-->
                                    <li class="next">
                                        <a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a>
                                    </li>
                                    <li class="next finish" style="display:none;">
                                        <a href="javascript:void(0);" id = 'finish' class="btn btn-lg txt-color-darken" >Finish</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="bar" class="progress progress-sm progress-striped active" >
                        <div class="progress-bar bg-color-pinkDark bar" ></div>
                    </div>
                </div>
            </div>
<?php ActiveForm::end(); ?>
    </div>

</div>
<!-- end widget content -->

</div>
<!-- end widget div -->

</div>
<!-- end widget -->

</article>
<!-- WIDGET END -->
<?php
$this->registerJsFile("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/fuelux/wizard/wizard.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/fileinput/fileinput.min.js", ['yii\web\JqueryAsset']);
$this->registerCssFile("css/fileinput/fileinput.min.css", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/jqgrid/jquery.jqGrid.min.js", ['yii\web\JqueryAsset']);
$this->registerJsFile("js/plugin/jqgrid/grid.locale-en.min.js", ['yii\web\JqueryAsset']);
?>
<script type="text/javascript">

    window.onload = function () {
        $("#image").fileinput({'showUpload':false, 'previewFileType':'any'});

        /*协议js效果 */
        /*协议下拉菜单联动*/
        $('#protocol').change(function(){
//            $('#tab2').
            var $pro = $('#protocol').find('option:selected').text();
            console.log($pro)
                $(".pro").hide();
                $('#'+$pro+'').show();

        })

        /*设备添加  克隆*/
        $('._plus').click(function() {
            var $row = $(this).parent().parent()
            $('#tab2').append($row.clone().remove());
            console.log($row)
//            $('._plus:not(:eq(1))').remove();
            $row.find('._minus').css('display', '');
//            $row.find('._plus:eq(1)').remove();
//            $row.find('._minus:first()').css('display', 'none');
        })
        $('#tab2').on('click','._minus', function(){
            $(this).parent().parent().remove();
        })


        /* wizard js配置*/
        //应该 是一个表单验证插件
        var $validator = $("#wizard-1").validate({

            rules: {
                email: {
                    required: true,
                    email: "Your email address must be in the format of name@domain.com"
                },
                'station[lang]': {
                    required: true
                },
                'station[country]': {
                    required: true
                },
                'station[city]': {
                    required: true
                },
                'station[name]': {
                    required: true
                },
                'station[station_id]': {
                    required: true
                },
                'station[ip]': {
                    required: true
                },
                protocol :{
                    required: true
                },
                'bac_name[]': {
                    required: true
                },
                'ip[]': {
                    required: true
                },
                'port[]': {
                    required: true
                },
                wphone: {
                    required: true,
                    minlength: 10
                },
                hphone: {
                    required: true,
                    minlength: 10
                },
                'user[username]': {
                    required: true
                },
                'user[password]': {
                    required: true
                },
                'user[mail]': {
                    required: true
                },
                'user[phone_number]': {
                    required: true
                }
            },

            messages: {
                fname: "Please specify your First name",
                lname: "Please specify your Last name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            },

            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        //编辑 wizard 插件
        $('#bootstrap-wizard-1').bootstrapWizard({
            //设置 样式
            'tabClass': 'form-wizard',
            //点击 next button 进行 验证
            'onNext': function (tab, navigation, index) {
                var $valid = $("#wizard-1").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                } else {
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                        'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                        .html('<i class="fa fa-check"></i>');
                }
            },
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#bootstrap-wizard-1').find('.bar').css({width:$percent+'%'});

                // 如果 是最后 一步 将next 变为 finish
                if($current >= $total) {
                    $('#bootstrap-wizard-1').find('.pager .next').hide();
                    $('#bootstrap-wizard-1').find('.pager .finish').show();
                    $('#bootstrap-wizard-1').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#bootstrap-wizard-1').find('.pager .next').show();
                    $('#bootstrap-wizard-1').find('.pager .finish').hide();
                }
            }
    });
        //点击 finish 提交 表单
        $("#finish").click(function (){
            //验证 表单
            var ser = $("#wizard-1").serialize();
//            console.log($validator);
            $("#wizard-1").submit();
        })

    //点位设置
        var jqgrid_data = [{
            id : "1",
            date : "2007-10-01",
            name : "test",
            note : "note",
            amount : "200.00"
        }];
        jQuery("#jqgrid").jqGrid({
            data : jqgrid_data,
            datatype : "local",
            height : 'auto',
            colNames : ['Actions', 'Inv No', 'Date', 'Client', 'Amount', 'Tax'],
            colModel : [
                { name : 'act', index:'act', sortable:false },
                { name : 'id', index : 'id' },
                { name : 'date', index : 'date', editable : true },
                { name : 'name', index : 'name', editable : true },
                { name : 'amount', index : 'amount', align : "right", editable : true },
                { name : 'tax', index : 'tax', align : "right", editable : true }],
            rowNum : 10,
            rowList : [10, 20, 30],
            pager : '#pjqgrid',
            sortname : 'id',
            toolbarfilter: true,
            viewrecords : true,
            sortorder : "asc",
            gridComplete: function(){
                var ids = jQuery("#jqgrid").jqGrid('getDataIDs');
                for(var i=0;i < ids.length;i++){
                    var cl = ids[i];
                    be = "<button class='btn btn-xs btn-default' data-original-title='Edit Row' onclick=\"jQuery('#jqgrid').editRow('"+cl+"');\"><i class='fa fa-pencil'></i></button>";
                    se = "<button class='btn btn-xs btn-default' data-original-title='Save Row' onclick=\"jQuery('#jqgrid').saveRow('"+cl+"');\"><i class='fa fa-save'></i></button>";
                    ca = "<button class='btn btn-xs btn-default' data-original-title='Cancel' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
                    //ce = "<button class='btn btn-xs btn-default' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
                    //jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ce});
                    jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ca});
                }
            },
            editurl : "dummy.html",
            caption : "SmartAmind jQgrid Skin",
            multiselect : true,
            autowidth : true

        });
        jQuery("#jqgrid").jqGrid('navGrid', "#pjqgrid", {
            edit : false,
            add : false,
            del : true
        });
        jQuery("#jqgrid").jqGrid('inlineNav', "#pjqgrid");
        /* Add tooltips */
        $('.navtable .ui-pg-button').tooltip({
            container : 'body'
        });
        // remove classes
        $(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
        $(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
        $(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
        $(".ui-jqgrid-pager").removeClass("ui-state-default");
        $(".ui-jqgrid").removeClass("ui-widget-content");
        // add classes
        $(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
        $(".ui-jqgrid-btable").addClass("table table-bordered table-striped");


        $(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
        $(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
        $(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
        $(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
        $(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
        $(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
        $(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
        $(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

        $( ".ui-icon.ui-icon-seek-prev" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
        $(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

        $( ".ui-icon.ui-icon-seek-first" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
        $(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

        $( ".ui-icon.ui-icon-seek-next" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
        $(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

        $( ".ui-icon.ui-icon-seek-end" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
        $(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");

    $(window).on('resize.jqGrid', function () {
        $("#jqgrid").jqGrid( 'setGridWidth', $("#content").width() );
    })


    }
</script>