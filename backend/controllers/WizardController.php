<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 14-12-8
 * Time: 上午9:40
 */

namespace backend\controllers;

use Yii;
use backend\models\Station;
use backend\models\Bacnet;
use common\models\User;
use backend\models\Point;
use yii\web\UploadedFile;
use common\library\Tool\Upload;
use backend\models\SignupForm;
use common\library\MyFunc;
class WizardController extends MyController{


    public function actionIndex()
    {
        $point_all = Point::findAllPoint();
        return $this->render('index',[
            'point_all' => $point_all,
        ]);
    }

    /**
     * 初始化方法
     * 收集 初始化配置参数：站点配置,采集配置,点位配置,用户配置.
     */
    public function actionInitialize()
    {
        //接受参数
        if($data = Yii::$app->request->post())
        {
            //站点配置
            $station = new station();
            $station->setAttributes($data['station']);
            $station->name = MyFunc::createJson($station->name);
            @$station->save();
            //用户修改
            $user = new SignupForm();
            $user->setAttributes($data['user']);
            @$user->signup();
            //点位设置
            foreach($data['Point'] as $k => $v){
                $point = Point::findOne($k);
                $point->setAttributes($data['Point'][$k]);
                @$point->savePoint();
            }
            /*操作日志*/
            $op['zh'] = '初始化配置';
            $op['en'] = 'Initial Configuration';
            @$this->getLogOperation($op,'');

            $this->redirect('/');
        }

    }

} 