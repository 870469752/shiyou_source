<?php

namespace backend\controllers;

use backend\models\PointData;
use backend\module\point\Module;
use Yii;
use yii\web\Controller;
use backend\models\Station;

use backend\models\ReportManage;
use backend\models\Menu;
use backend\models\SysMenu;
use backend\models\UserMenu;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use backend\models\ModuleRole;
use backend\models\Module as ModuleModel;
use backend\models\OperateLog;
use backend\models\Category;
/**
 * 主控制器，用于初始化作用、全局控制等操作
 * @property array $site_config
 * @author DUCAT
 * @since 1.0
 */

class MyController extends Controller {
	public function init() {
        // judge whether the user is logined
        if(!isset(Yii::$app->user->id)){
            return $this->redirect('/site/login');
        }

        // deal with the information of website
        $this->siteInfo();

        // 权限验证
        $request = Yii::$app->urlManager->parseRequest(Yii::$app->request);

        //如果是最高管理者 可以跳过验证
        $request[0] ? $this->_auth($request[0]) : '';

	}

    /**
     * 站点信息处理，方便重用
     */
    public function siteInfo(){
        /*if (YII_ENV_DEV) {
            Yii::$app->cache->flush();
        }*/
        // 语言和站点配置
        $lang = Yii::$app->request->get ( 'lang' );
        $config_site = Yii::$app->cache->get ( 'config_site' );

        if ($config_site === false) {
            $config_site = Station::findOne(1);
            Yii::$app->cache->set ( 'config_site', $config_site );
        }

        Yii::$app->language = $lang ? $lang : (ArrayHelper::getValue($config_site,'default_language'));

        //权限菜单的获取
        $idd = md5(Yii::$app->user->id);

        $nav = Yii::$app->cache->get($idd);

        if (empty($nav)) {
            //只显示 当前拥有权限的菜单 验证 包括 加密狗内部的模块 和 用户权限
            if(\Yii::$app->user->id != 0){
                $user_menu_data = UserMenu::getAllMenu();
                foreach($user_menu_data as $key => $value){
                    $user_menu_data[$key]['user_id'] = Yii::$app->user->id;
                    if(!empty($value['module_id'])){
                        $temp_path = ModuleModel::findOne($value['module_id']);

                        $path = $temp_path->path;

                        /*if (!ModuleRole::accessAuth($path)) {
                            unset($user_menu_data[$key]);
                        }*/
                        $user_menu_data[$key]['url'] = rtrim($path,"?");
                    }
                }
                $menu_data = $user_menu_data;
            } else {
                $sys_menu_data = SysMenu::getAllMenu();
                foreach($sys_menu_data as $key => $value){
                    $sys_menu_data[$key]['user_id'] = Yii::$app->user->id;
                    if(!empty($value['module_id'])){
                        $sys_menu_data[$key]['url'] = rtrim(ModuleModel::findOne($value['module_id'])->path,"?");
                    }
                }
                $menu_data = $sys_menu_data;
            }
            $nav = MyFunc::TreeData($menu_data);
            $auth_tree = [];
            Yii::$app->cache->delete($idd);
            Yii::$app->cache->set ( $idd, $nav );

            //以分类为主的 菜单展示
            //$category_data = Category::getAllCategory();
            //$category_parent_ids = ArrayHelper::map($category_data, 'parent_id', 'parent_id');
        }
        //温湿度 临时写的，以后修改
        /*$plugin_config = json_decode((new \yii\db\Query())
            ->select('parameter')
            ->from('core.plugin_config')
            ->where(['plugin_id' => 'default'])
            ->one()['parameter'], true);
        $GLOBALS['temperature'] = PointData::pointNewData($plugin_config['temperature']);
        $GLOBALS['humidity'] = PointData::pointNewData($plugin_config['humidity']);*/
    }

    /**
     * 获取所有语言 存入js全局变量
     */
    public function actionLang()
    {
        $file_arr = [];
        $file = MyFunc::getAllLang();
        foreach($file as $key => $value)
        {
            $arr = [];
            foreach($value as $v)
            {
                $arr = array_merge($arr, require_once('../messages/'.$key.'/'.$v));
            }
            $file_arr[$key] = $arr;
        }
        $file_arr['type'] = MyFunc::getCurUrl($_GET['url']);
        echo json_encode($file_arr);
    }

    /**
     *  权限验证权限
     * 1、加密狗中的模块验证
     * 2、用户访问权限验证
     *    用户访问验证有四种形式的存储方式:
     *                                  1、单独一个控制器
     *                                  2、控制器/方法
     *                                  3、模块/控制器
     *                                  4、模块/控制器/方法
     *  @return [type] [description]
     */
    public function _auth_old($url)
    {
//        echo $url;
        $dog_auth = '';
        //从加密狗中获取的系统权限
        //获取前缀 控制器 或 模型/控制器
        $module = strpos($url, '/') === false ? $url : substr($url, 0, strrpos($url, '/'));
        //默认的 模块
        if(in_array($module, Yii::$app->params['default_prefix'])){

            return true;
        }
        //默认访问链接
        else if(in_array($url, Yii::$app->params['default_link'])){

            return true;
        }else{

            //验证当前项目的 模块权限
            if (!in_array($module, [$dog_auth])) {
//                var_dump($url);die;
                //如果是超级管理员 将拥有当前项目的所有模块权限
                if(Yii::$app->user->id == 0){ return true;}
//                if(Yii::$app->user->id != 0){ return true;}//本行暂时加的

                //验证当前用户的访问权限
                if(ModuleRole::accessAuth($url) && !empty(Yii::$app->user->identity->role_id)){
                    return true;
                }else{
                    throw new ForbiddenHttpException('');
                }
            }else{
                throw new ForbiddenHttpException('');
            }

        }

    }

    public function _auth($url){
        return false;
    }
    /**
     * 获取用户request
     * return array
     */
    public function getLogRoute(){
        /*controllers下的控制器类id*/
        $contro = array('usermenu','sysmenu','route','module','my','bacnet','download','info','item','mail','menu','modulerole','role','site','station','systemmodule','user','wizard');
        $route = yii::$app->requestedRoute;
        $route_arr = explode('/',$route);
        if(in_array($route_arr[0],$contro)){
            @$data_route['contro'] = $route_arr[0];
            @$data_route['action'] = $route_arr[1];
        }else{
            @$data_route['module'] = $route_arr[0];
            @$data_route['contro'] = $route_arr[1];
            @$data_route['action'] = $route_arr[2];
        }
        $data_route['action'] = empty($data_route['action'])?'index':$data_route['action'];
        $data['uid'] = Yii::$app->user->id;
        $data['timestamp'] = date('Y-m-d H:i:s');
        $data['func_name'] = json_encode($data_route);
        return $data;
    }
    /**
     * 插入操作日志
     */
    public function getLogOperation($operation=[],$info='',$result=true)
    {
        /*controllers下的控制器类id*/
        $control = array('usermenu','sysmenu','route','module','my','bacnet','download','info','item','menu','module-role','role','site','station','system-module','user','wizard');
        $route = yii::$app->requestedRoute;
        $route_arr = explode('/',$route);
        if(in_array($route_arr[0],$control)){
            @$data_route['control'] = $route_arr[0];
            @$data_route['action'] = empty($route_arr[1])?'index':$route_arr[1];
        }else{
            @$data_route['module'] = $route_arr[0];
            @$data_route['control'] = empty($route_arr[1])?'default':$route_arr[1];
            @$data_route['action'] = empty($route_arr[2])?'index':$route_arr[2];
        }
        $data['uid'] = Yii::$app->user->id;
        $data['create_timestamp'] = date('Y-m-d H:i:s');
        $data['func_name'] = json_encode($data_route);
        if($result){
            $re_cn = "成功";
            $re_us = "successfully";
        }else{
            $re_cn = "失败";
            $re_us = "failure";
        }
        $msg["zh-cn"] =  $operation['zh'].$info.$re_cn;
        $msg["en-us"] = "{$operation['en']} $info $re_us";
        $msg_all['data_type'] = 'description';
        $msg_all['data'] = $msg;
        $data['description'] = json_encode($msg_all);

        $logModel = new OperateLog();
        $logModel->load($data, '');
        $logModel->save();
    }
    public function addMenu($menu_data){
        $role_id = MyFunc::getRoleId();
        if(isset($role_id)) {
            $user_roleId = $role_id;
            $sql = "SELECT id,name,parent_id,role_id FROM core.report_manage  where " . $user_roleId . "= ANY (role_id)";
            $location = ReportManage::findBySql($sql)
                ->asArray()->indexBy('id')->all();

            //所有的parent_id  其他也就是最底层叶子节点
            foreach ($location as $location_key => $location_value) {
                $parent_ids[] = $location_value['parent_id'];
            /*
             *
             * 只有叶子节点带链接,其他的父节点带上文件夹标志
             * 为了避免id重复,统一把报表里的所有id以及parent_id 加上 menu表里菜单的数量
             *
            */
            foreach ($location as $location_key => $location_value) {

                //叶子节点
                if (!in_array($location_key, $parent_ids)) {
                    $location[$location_key] = [
                        'description' => $location_value['name'],
						'icon' => null,
                        'depth' => 1,
                        'idx' => 1,
                        'url' => 'scheduled-report/crud/new-index',
                        'param' => '{"data_type":"url_param","data":{"id":"' . $location_value['id'] . '","type":"view"}}',
                        'user_id' => null,
                        'module_id' => 60,
                        'user_id_new' => '{2}'
                    ];
                    $location[$location_key]['id'] = $location_value['id'] + 1000;
                    $location[$location_key]['parent_id'] = $location_value['parent_id'] + 1000;
                } else {
                    $location[$location_key] = [
                        'description' => $location_value['name'],
						'icon' => 'fa-folder-open',
                        'depth' => 1,
                        'idx' => 1,
                        'url' => null,
                        'param' => null,
                        'user_id' => null,
                        'module_id' => null,
                        'user_id_new' => '{2}'
                    ];
                    $location[$location_key]['id'] = $location_value['id'] + 1000;
                    if ($location_value['parent_id'] == null) {
                        $location[$location_key]['parent_id'] = 3248;
                    } else {
                        $location[$location_key]['parent_id'] = $location_value['parent_id'] + 1000;
                    }
                }
                $menu_data[] = $location[$location_key];
            }
        }

            return $menu_data;
         }
        else {
            return $menu_data;
        }
    }
}