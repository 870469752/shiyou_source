<?php

namespace backend\controllers;

use Yii;
use backend\models\Bacnet;
use backend\models\search\BacnetSearch;
use backend\controllers\MyController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OperateLog;
use yii\web\Session;
/**
 * BacnetController implements the CRUD actions for Bacnet model.
 */
class BacnetController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bacnet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BacnetSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Bacnet model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bacnet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bacnet;

        if ($model->load(Yii::$app->request->post()) && $result = $model->bacnetSave()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '创建bacnet点位';
            $op['en'] = 'Create bacnet point';
            $this->getLogOperation($op,$info,$result);
            //更新session
            $this->actionAjaxAutomaticMonitoring();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bacnet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->bacnetSave()) {
            /*操作日志*/
            $info = $model->name;
            $op['zh'] = '修改bacnet点位';
            $op['en'] = 'Update bacnet point';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bacnet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $result = $this->findModel($id)->delete();
        /*操作日志*/
        $op['zh'] = '删除bacnet点位';
        $op['en'] = 'Delete bacnet point';
        $this->getLogOperation($op,'','');
        //更新session
        $this->actionAjaxAutomaticMonitoring();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Bacnet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bacnet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bacnet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 自动更新bacnet设备
     * 获取当前数据库中的设备数 与 接口返回的设备数进行对比
     * @return int 返回更新的设备数量
     */
    public function actionAjaxAutomaticMonitoring()
    {
        //获取当前数据库中的设备数
        $new_bac_count = Bacnet::getTotal();
        $session = new Session();
        $session->open();
        $old_bac_count = isset($session['bacnet_count']) ? $session['bacnet_count'] : 0;
        //更新session中bacnet_count 的值
        $session['bacnet_count'] = $new_bac_count;
        $session->close();
        $res_bac = $new_bac_count - $old_bac_count;
        //调用bac设备信息接口
        return $res_bac;
    }

    /**
     * 手动更新bacent设备
     * 接收ajax请求调用接口 刷新bac设备表
     * 无返回值
     */
    public function actionAjaxManuallyMonitoring(){
        $testurl = 'www.baidu.com';
        $timeout = 5;
        //接口访问
        $ch = curl_init();                                                                  //初始化
        curl_setopt ($ch, CURLOPT_URL, $testurl);                                           //设置 请求参数
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $res = curl_exec($ch);                                                              //执行
        sleep($timeout);                                                                    //暂停
        curl_close($ch);
        //
        $this->actionAjaxAutomaticMonitoring();
    }
}
