<?php

namespace backend\controllers;

use Yii;
use backend\models\Item;
use backend\models\search\ItemSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use backend\models\AuthChild;
/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends MyController
{
    public function behaviors()
    {
        return [ 
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['post'],
        ],
        ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex($id = 2)
    {
        $query_condition = Yii::$app->request->getQueryParams();
        $query_condition['ItemSearch']['field'][] = 'type';
        $query_condition['ItemSearch']['relation'][] = '=';
        $query_condition['ItemSearch']['value'][] = $id;
        $searchModel = new ItemSearch;
        $dataProvider = $searchModel->search($query_condition);

        return $this->render($id == 2? 'index' : 'index_role', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            ]);
    }

    /**
     * Displays a single Item model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Item;

        if ($model->load(Yii::$app->request->post()) && $model->addItem()) {
            return $this->redirect(['index', 'id' => $id]);
        } else {
            return $this->render($id == 2? 'create' : 'create_role', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->updateaItem()) 
        {
            return $this->redirect(['index', 'id' => Yii::$app->request->get('type')]);
        } 
        else 
        {
            $model->description = MyFunc::DisposeJSON($model->description);
            return $this->render(Yii::$app->request->get('type') == 2? 'update' : 'update_role', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());
        $model->removeItem();
        return $this->redirect(['index', 'id' => Yii::$app->request->get('type')]);
    }

    public function actionAccredit($id)
    {
        if($obj = AuthChild::findModelByP($id))
        {
            $model = $obj;
            $model ->child = AuthChild::findAllChild($id);
        }
        else
        {
            $model = new AuthChild();
            $model->parent = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->updateByParent())
        {
            return $this->redirect(['index', 'id' => 1]);
        } 
        else 
        {
            $c_data = Item::getMenuChild();
            return $this->render('accredit', [
                'model' => $model,
                'c_data' => $c_data,
                ]);
        }
    }
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
