<?php

namespace backend\controllers;

use backend\models\EventPoint;
use backend\models\PointEvent;
use backend\models\WorldArea;
use common\library\MyFunc;
use Yii;
use backend\models\Station;
use backend\models\search\StationSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\models\AtomEvent;
use backend\models\OperateLog;
/**
 * StationController implements the CRUD actions for Station model.
 */
class StationController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Station models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StationSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Station model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Station model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Station;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $area_tree = MyFunc::TreeData(WorldArea::find()->asArray()->all());
            return $this->render('create', [
                'model' => $model,
                'area_tree' => $area_tree
            ]);
        }
    }

    /**
     * Updates an existing Station model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            $model->logo_url = UploadedFile::getInstance($model, 'logo_url');
            $model->image_url = UploadedFile::getInstance($model, 'image_url');
            $upload_path = YII::$app->params['uploadPath'];
            // 上传LOGO如果没有错就上传，有错就取消更新
            if ($model->logo_url->error == 0) {
                $model->logo_url->saveAs($upload_path . $model->logo_url->baseName . '.' . $model->logo_url->extension);
            } else {
                unset($model->logo_url);
            }

            // 上传站点图片如果没有错就上传，有错就取消更新
            if ($model->image_url->error == 0) {
                $model->image_url->saveAs($upload_path . $model->image_url->baseName . '.' . $model->image_url->extension);
            } else {
                unset($model->image_url);
            }

            // 保存所有
            $model->save();
            Yii::$app->cache->set ( 'config_site', false);
            return $this->redirect('');
        } else {
            $area_tree = MyFunc::TreeData(WorldArea::find()->asArray()->all());
            return $this->render('update', [
                'model' => $model,
                'area_tree' => $area_tree
            ]);
        }
    }

    /**
     * Deletes an existing Station model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Station model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Station the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Station::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 绑定 特殊类型的 点位 如 季节点位
     */
    public function actionBinding()
    {
        if ($post = Yii::$app->request->post()) {
            //判断绑定类型 生成不同的 系统元素
             switch($post['type'])
             {
                 case 'seasonal':          //如果是绑定季点位 那么回生成冬夏季报警事件
                       $result = self::seasonalBinding($post['point_id'], 'winter');
                       $result = self::seasonalBinding($post['point_id'], 'summer');
                     break;
             }
            /*操作日志*/
            $op['zh'] = '绑定季节点位';
            $op['en'] = 'Binding season points';
            $this->getLogOperation($op);
            return $this->redirect(['binding']);
        } else {
            return $this->render('binding', [
                ]);
        }
    }

    /**
     * 生成 站点特有含义的点位类型
     * @param $point_id
     * @param $seasonal
     * @return bool
     */
    public static function seasonalBinding($point_id, $seasonal)
    {
        //1、生成对应的 季节事件
        $atom_event = new PointEvent();
        $atom_event -> load([
                              'name' => MyFunc::setSystemName('bind', $seasonal .'_event' ,$point_id),
                              'time_type' => 'day',
                              'time_range' => '2009-09-01 00:00, 2009-09-01 00:00',
                              'trigger' => $seasonal == 'summer' ? '2;3' : '1;1,3;3',
                              'type' => '1',
                              'is_system' => true,
                              'point_id' => $point_id
                            ], '');
        return $atom_event -> bindingAtomEvent(true);
        //2、生成对应的 季节事件点位
//        $event_point = new EventPoint();
//        $event_point -> load([
//                                'name' => $seasonal . '_event_point_' .$point_id,
//                                'event_id' => $event_id,
//                                'is_system' => true,
//                                'is_record' => false,
//                            ], '');
//        $event_point -> bindingPointSave();
    }
}
