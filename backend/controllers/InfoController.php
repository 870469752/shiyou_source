<?php
namespace backend\controllers;

use Yii;
use yii\web\ForbiddenHttpException;
use backend\models\Station;
use yii\helpers\ArrayHelper;
use backend\controllers\MyController;

/**
 * Site controller
 */
class InfoController extends MyController
{
	public function init() {
        $this->siteInfo();
	}
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionForbidden(){
    	throw new ForbiddenHttpException();
    }

}
