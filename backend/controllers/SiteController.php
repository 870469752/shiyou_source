<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\usermenu;
/**
 * Site controller
 */
class SiteController extends MyController
{
    public function init(){
        $this->siteInfo();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            //认证方法过滤
//            'basicAuth' => [
//                'class' => HttpBasicAuth::className(),
//            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        $this->layout = 'common';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->login()) {
            /*操作日志*/
            $user = $model->username;
            $op['zh'] = "$user 登陆";
            $op['en'] = "$user Login";
            $this->getLogOperation($op,'',$result);
            /*$idd ="rules".Yii::$app->user->id;
            $rules = Yii::$app->cache->get($idd);
            //将用户所拥有子系统的点位存入session
            if(empty($rules)) {UserMenu::getUserMenuParam();}*/

            //临时写，获得模式
            $sql = "select value from  core.command_log where point_id = 9156984 order by id desc limit 1 offset 0";
            $m_s = Yii::$app->db->createCommand($sql)->queryAll();
            Yii::$app->redis->set("M_S",json_decode($m_s[0]['value']));
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        $idd = md5(Yii::$app->user->id);
        $allPermissionsIdd = md5('allPermissions'.Yii::$app->user->id);
        Yii::$app->cache->delete($idd);
        Yii::$app->cache->delete($allPermissionsIdd);
        //Yii::$app->cache->flush();
        //UserMenu::getUserMenuParam();//将用户所拥有子系统的点位存入session;
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
