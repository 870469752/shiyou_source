<?php

namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use backend\models\Location;
use backend\models\AlarmLog;
use backend\module\AlarmLog\controllers\CrudController;
use backend\models\PointCategoryRel;
use backend\models\SubSystemCategory;
use backend\models\SubSystemManage;
use backend\models\SubSystem;
use common\library\MyFunc;
/**
 * ItemController implements the CRUD actions for Item model.
 */
class RouteController extends MyController
{
    public function behaviors()
    {
        return [ 
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['post'],
        ],
        ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_name = Yii::$app->user->identity->username;;
        if(strstr($user_name, '电力')){
            return $this->redirect(['//subsystem_manage/crud/electrinic-index']);
        }elseif(strstr($user_name, '安全')){
            return $this->redirect(['//alarm-log/default/logs?type=3']);
        }elseif(strstr($user_name, '设施')){
            return $this->redirect(['//alarm-log/default/logs?type=1']);
        }elseif(strstr($user_name, '消防')){
            return $this->redirect(['//alarm-log/default/logs?type=3']);
        }else{
            return $this->redirect(['//alarm-log/default/logs?type=1']);
        }
    }

    public function actionGetNoRecord(){
        $alarm_log = AlarmLog::byGetNoRecord();
        if($alarm_log){
            $this->getalarmSys($alarm_log);
            return true;
        }else{
            return false;
        }
    }

    //订阅
    public function actionGetNoRecordNew(){
        $log_id = Yii::$app->request->get('id');
        return $this->actionAlarmNoNotice($log_id);
    }

    /**
     * 计算报警日志里未确认的日志,及报警样式(订阅)
     * @return array;
     */
    public function actionAlarmNoNotice($log_id)
    {
        $alarm_log = AlarmLog::getLogsNoNoticeNew($log_id);
        //echo "<pre>";print_r($alarm_log);die;
        $style = [];
        if(!empty($alarm_log)){
            AlarmLog::setNotice($alarm_log);
            foreach ($alarm_log as $k => $v) {
                if(!empty($alarm_log[$k])){
                    $style[$k]['p_name'] = $v['p_name'];
                    $style[$k]['title'] = $v['name'];
                    $style[$k]['time'] = $v['start_time'];
                    $style[$k]['content'] = $v['description'];
                    $style[$k]['color'] = $this->actionAlarmColor($v['notice_style']);
                    $style[$k]['timeout'] = $v['notice_siren'] * 1000;
                    $style[$k]['sound'] = $this->actionAlarmSound($v['notice_fade']);
                    $style[$k]['icon'] = 'fa fa-bell swing animated';
                    $style[$k]['number'] = $v['id'];
                    $style[$k]['alarm_rule_id'] = $v['alarm_rule_id'];
                }
            }
        }else{
            return false;
        }
        return json_encode($style);
    }

    public function getAlarmSys($alarm_log){
        $alarm_log_model = new AlarmLog;
        $key = "sys_pos";
        $Info = $this->object_to_array(json_decode(Yii::$app->redis->get($key)));
        //向报警日志数组里添加报警位置信息
        foreach($alarm_log as $k => $v){
            $alarm_log[$k]['position'] = '';
            foreach($Info['pos'] as $x => $y){
                $temp = explode(",",$y['point_id']);
                if(in_array($v['point_id'],$temp)){
                    $alarm_log[$k]['position'] = $y['name'];
                    break;
                }
            }
        }
        //给各子系统添加报警点位
        foreach($alarm_log as $k => $v){
            $alarm_log[$k]['sys_name'] = '';
            foreach($Info['sys'] as $x => $y){
                if($y['point_id'] !='') {
                    $temp = explode(",", $y['point_id']);
                    if (in_array($v['point_id'], $temp)) {
                        $alarm_log[$k]['sys_name'] = $y['name'];
                        break;
                    }
                }else{
                    continue;
                }
            }
        }

        foreach($alarm_log as $x => $y){
            $tem_arr = array();
            if($y['sys_name']) $tem_arr['sys_name'] = $y['sys_name'];else $tem_arr['sys_name']=null;
            if($y['position']) $tem_arr['position'] = $y['position'];else $tem_arr['position']=null;
            if($y['name']) $tem_arr['p_name'] = $y['name'];else $tem_arr['p_name']=null;
            if($y['link_point']) $tem_arr['p_link'] = $y['link_point'];else $tem_arr['p_link']=null;
            if($y['sys_name']=='冷热站' || $y['sys_name']=='直燃机'|| $y['sys_name']=='电梯系统'|| $y['sys_name']=='智能照明'|| $y['sys_name']=='楼宇自控' || $y['sys_name']=='锅炉房') {
                $tem_arr['sys_id'] = 3;//电力1，安全2，楼控3
            }else if($y['sys_name']=='火灾报警' || $y['sys_name']=='周界防范'|| $y['sys_name']=='入侵报警'|| $y['sys_name']=='门禁系统'|| $y['sys_name']=='视频监控'){
                $tem_arr['sys_id'] = 2;
            }else if($y['sys_name']=='电力监测'){
                $tem_arr['sys_id'] = 1;
            }else{
                $tem_arr['sys_id'] = null;
            }
            $alarm_log_model->updateAlarmLog($y['id'],$tem_arr);
        }
    }

    //对象转数组(包括多维)
    function object_to_array($obj){
        $_arr = is_object($obj) ? get_object_vars($obj) :$obj;
        if(count($_arr)){
            foreach ($_arr as $key=>$val){
                $val = (is_array($val) || is_object($val)) ? $this->object_to_array($val):$val;
                $arr[$key] = $val;
            }
        }
        return $arr;
    }

    /**
     * 获得地理位置及子系统信息
     * @return array
     */
    public function actionGetPointInfo(){
        $location_model = new Location;
        $sub_sys_model = new SubSystemCategory();
        $location_pos = $location_model->allData();//所有地理位置
        $location_sys = $sub_sys_model->allData();
        $point_category_rel_model = new PointCategoryRel;
        foreach($location_pos as $k => $v){
            $location_pos[$k]['point_id'] = $point_category_rel_model->byGetPointIds($v['id']);//添加点位信息
        }
        foreach($location_sys as $k => $v){
            $location_sys[$k]['point_id'] = $point_category_rel_model->byGetPointIds($v['id']);//添加点位信息
        }

        $location_pos_name = $this->actionAddName($this->actionAddName($location_pos));
        $arr['sys'] = $location_sys;
        $arr['pos'] = $location_pos_name;
        return $arr;
    }

    public function actionAddName($arr){
        $temp = $arr;
        foreach($arr as $k => $v){
            if($v['parent_id'] != 1468){
                foreach($temp as $x => $y) {
                    if($v['parent_id']==$y['id']){
                        $arr[$k]['name'] = $y['name'].' '.$v['name'];
                        $arr[$k]['parent_id'] = $y['parent_id'];
                        break;
                    }
                }
            }else{
                continue;
            }
        }
        return $arr;
    }

    public function actionAccessCard($area)
    {
        $p_acl= 'protocol.access_card_log';
        $p_ach= 'protocol.access_card_holder';
        $p_ac= 'protocol.access_card';
        $p_ad ='protocol.access_controller';
        $p_adl ='protocol.access_controller_log';
        if($area){
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                WHERE $p_acl.area = $area
                ORDER BY $p_acl.timestamp DESC
                limit 1000 offset 0
                ";

            $sql2 = "select $p_adl.timestamp,$p_adl.message,$p_ad.name from $p_adl LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                where  $p_adl.area = $area
                ORDER BY $p_adl.timestamp DESC
                limit 1000 offset 0
                ";
            //$p_adl.timestamp > (now()-interval '30 day') and
        }else{
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                ORDER BY $p_acl.timestamp DESC
                limit 1000 offset 0
                ";

            $sql2 = "select $p_adl.timestamp,$p_adl.message,$p_ad.name from $p_adl LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                ORDER BY $p_adl.timestamp DESC
                limit 1000 offset 0
                ";
            //where $p_adl.timestamp > (now()-interval '30 day')
        }
//where $p_acl.timestamp > (now()-interval '30 day')  查询30天内的数据

        $arr1 = Yii::$app->db->createCommand($sql)->queryAll();

        $arr2 = Yii::$app->db->createCommand($sql2)->queryAll();

        foreach($arr2  as $key => $value){
            if($value['message'] == '9') { $arr2[$key]['message'] = '门正常';continue;}
            if($value['message'] == '10') {$arr2[$key]['message'] = '强行打开';continue;}
            if($value['message'] == '11') {$arr2[$key]['message'] = '门未关好';continue;}
        }
        //echo "<pre>";print_r($arr2);die;
        return $this->render('access', [
            'title' => '门禁--A'.$area,
            'area' => $area,
            'arr1' =>$arr1,
            'arr2' =>$arr2
            ]);
    }

    public function actionLink($name){
        switch ($name){
            case '1' :
                return $this->render('index_ele');
                break;
            case '2' :
                return $this->render('index_equi');
                break;
            case '3' :
                return $this->render('index_safe');
                break;
            case '4' :
                return $this->render('index_all');
                break;
        }
    }


    //获取刷卡实时信息
    public function actionGetRealTimeReadCardInfo($area){
        $p_acl= 'protocol.access_card_log';
        $p_ach= 'protocol.access_card_holder';
        $p_ac= 'protocol.access_card';
        $p_ad ='protocol.access_controller';
        if($area){
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                WHERE $p_acl.area = $area
                ORDER BY $p_acl.timestamp DESC
                limit 25 offset 0
                ";
        }else{
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                ORDER BY $p_acl.timestamp DESC
                limit 25 offset 0
                ";
        }
        $arr = Yii::$app->db->createCommand($sql)->queryAll();
        //return $arr;
        return json_encode($arr);
    }

    //获取门禁实时报警信息
    public function actionGetRealTimeDoorAlarmInfo($area){  //get-real-time-door-alarm-info
        $p_ad ='protocol.access_controller';
        $p_adl ='protocol.access_controller_log';
        if($area){
            $sql = "select $p_adl.timestamp,$p_adl.message,$p_ad.name
                from $p_adl
                LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                where  $p_adl.area = $area
                ORDER BY $p_adl.timestamp DESC
                limit 25 offset 0
                ";
        }else{
            $sql = "select $p_adl.timestamp,$p_adl.message,$p_ad.name from $p_adl LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                ORDER BY $p_adl.timestamp DESC
                limit 25 offset 0
                ";
        }
        $arr = Yii::$app->db->createCommand($sql)->queryAll();
        //return $arr;
        return json_encode($arr);
    }

    //获取刷卡信息
    public function actionGetReadCardInfo($area){
        $p_acl= 'protocol.access_card_log';
        $p_ach= 'protocol.access_card_holder';
        $p_ac= 'protocol.access_card';
        $p_ad ='protocol.access_controller';
        if($area){
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                WHERE $p_acl.area = $area
                ORDER BY $p_acl.timestamp DESC
                limit 10000 offset 0
                ";
        }else{
            $sql = "select $p_acl.timestamp,$p_acl.message,$p_acl.access_card_number,$p_ac.access_card_holder_id,$p_ach.last_name,$p_ad.name
                from $p_acl
                LEFT JOIN  $p_ac ON $p_acl.access_card_number = $p_ac.access_card_number
                LEFT JOIN  $p_ach ON $p_ac.access_card_holder_id = $p_ach.id
                LEFT JOIN $p_ad ON $p_acl.access_controller_id = $p_ad.id
                ORDER BY $p_acl.timestamp DESC
                limit 10000 offset 0
                ";
        }
        $arr = Yii::$app->db->createCommand($sql)->queryAll();
        //return $arr;
        return json_encode($arr);
    }

    //获取门禁报警信息
    public function actionGetDoorAlarmInfo($area){  //get-real-time-door-alarm-info
        $p_ad ='protocol.access_controller';
        $p_adl ='protocol.access_controller_log';
        if($area){
            $sql = "select $p_adl.timestamp,$p_adl.message,$p_ad.name
                from $p_adl
                LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                where  $p_adl.area = $area
                ORDER BY $p_adl.timestamp DESC
                limit 10000 offset 0
                ";
        }else{
            $sql = "select $p_adl.timestamp,$p_adl.message,$p_ad.name from $p_adl LEFT JOIN $p_ad ON $p_adl.access_controller_id=$p_ad.id
                ORDER BY $p_adl.timestamp DESC
                limit 10000 offset 0
                ";
        }
        $arr = Yii::$app->db->createCommand($sql)->queryAll();
        //return $arr;
        return json_encode($arr);
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionFireAlarm()
    {
        return $this->render('fire_alarm');
    }
    
    public function actionSetActive(){
        $active = Yii::$app->request->get('active');
        Yii::$app->session->set('active_by', $active);
    }

    public function actionGetActive(){
        if(!isset($_SESSION['active_by'])){
            $_SESSION['active_by'] = 'A45';
        }
        $result = Yii::$app->session->get('active_by');
        return $result;
    }

    public function actionGetDate(){
        echo "<pre>";print_r(Date('Y-m-d',time()));die;
    }
	
	
    //设置演示模式
    public function actionCommandLog(){
        $model_id = [Yii::$app->request->get('val')];
        $timestamp = Date("Y-m-d H:i:s");
        $sql = "INSERT INTO core.command_log(point_id, type, value, timestamp, user_id, status)
                VALUES (9156984, 2, ".$model_id[0].", '".$timestamp."', 0, 0)";
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        Yii::$app->redis->set("M_S",json_decode($model_id[0]));
        return json_encode($result);
    }

    //对subsystem_manage的name字段累加名称
    public function actionPin(){
        $subsystem_manage=SubSystemManage::findBySql("select id, name->'data'->>'zh-cn'as name,parent_id from core.subsystem_manage")->asArray()->all();
        $this->a1($subsystem_manage);

    }

    public function a1($data){
        $result=$data;
        foreach($data as $key => $value){
            if(!empty($value['parent_id'])){
                $this->a2($data);
            }
        }
        $sub_system=SubSystem::findBySql("select id, name->'data'->>'zh-cn' as name,location_id from core.sub_system")->asArray()->all();
        $this->a3($sub_system,$result);
    }
//双重数组的轮回，需要我们在适当的位置，添加节点
    public  function a2($data){
        $temp= $data;
        foreach($data as $key => $value){
            foreach($data as $k => $v){
                if($value['parent_id']==$v['id']){
                    $temp[$key]['name'] = $v['name'].'_'. $data[$key]['name'];
                    $temp[$key]['parent_id'] = $v['parent_id'];
                }else{
                    continue;
                }
            }
        }
        $this->a1($temp);
    }

    public  function a3($data1,$data2){
        foreach($data1 as $key => $value){
            foreach($data2 as  $k => $v){
                if($value['location_id'] == $v['id']){
                    $data1[$key]['name']=$v['name'];
                    break;
                }
            }
        }
        set_time_limit(0);
        foreach($data1 as $kk => $vv){
            $name = [];
            $name['data_type']='description';
            $name['data']['zh-cn']=$vv['name'];
            $name = json_encode($name);
            $sql= "update core.sub_system set name= '".$name."' where id =".$vv['id'];
            Yii::$app->db->createCommand($sql)->queryAll();
        }
        die;
    }

    //根据子系统查找子系统所对应的rule_id
    function actionSubSysRuleId(){
        $ssc = 'core.sub_system_category';
        $pcr = 'core.point_category_rel';
        $pe = 'core.point_event';
        $ar = 'core.alarm_rule';
        $sub_sys_sql = "select id from $ssc";
        $str_ss = "";
        $sub_system = Yii::$app->db->createCommand($sub_sys_sql)->queryAll();
        foreach($sub_system as $k =>$v){
            $str_ss .= $v['id'].",";
        }
        $str_ss = trim($str_ss,",");
        $rule_sql = "select $ar.id,$pcr.category_id from $pcr LEFT JOIN $pe ON $pcr.point_id = $pe.point_id LEFT JOIN $ar ON $pe.id = $ar.event_id where $pcr.category_id in ($str_ss)";
        $sub_rule = Yii::$app->db->createCommand($rule_sql)->queryAll();
        for($i=1470;$i<=1481;$i++){
            $sub_arr[$i] = [];
        }
        foreach($sub_system as $k =>$v){
            foreach($sub_rule as $x =>$y){
                if(!$y['id']){unset($sub_rule[$x]);continue;}
                if($v['id']==$y['category_id']){
                    $sub_arr[$v['id']][]= $y['id'];
                    unset($sub_rule[$x]);
                    continue;
                }else{continue;}
            }
        }
        //电力规则集合
        $ele = $sub_arr['1475'];
        //安全规则集合
        $safe = array_merge($sub_arr['1476'],$sub_arr['1477'],$sub_arr['1478'],$sub_arr['1479'],$sub_arr['1480']);
        //楼控规则集合
        $equi = array_merge($sub_arr['1470'],$sub_arr['1471'],$sub_arr['1472'],$sub_arr['1473'],$sub_arr['1474'],$sub_arr['1481']);
        //火警规则集合
        $fire = $sub_arr['1476'];
        //全部规则集合
        $all = array_merge($sub_arr['1475'],$sub_arr['1476'],$sub_arr['1477'],$sub_arr['1478'],$sub_arr['1479'],$sub_arr['1480'],$sub_arr['1470'],$sub_arr['1471'],$sub_arr['1472'],$sub_arr['1473'],$sub_arr['1474'],$sub_arr['1481']);
        Yii::$app->redis->set("Rule_Ele", json_encode($ele));
        Yii::$app->redis->set("Rule_Safe", json_encode($safe));
        Yii::$app->redis->set("Rule_Equi", json_encode($equi));
        Yii::$app->redis->set("Rule_Fire", json_encode($fire));
        Yii::$app->redis->set("Rule_All", json_encode($all));
        return false;
    }
}
