<?php

namespace backend\controllers;

use Yii;
use backend\models\Menu;
use backend\models\Item;
use backend\models\search\MenuSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\library\MyFunc;
use yii\base\Request;
use backend\models\OperateLog;
use backend\models\AlarmLevel;
/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends MyController
{
    /**
     * 图表 样式 数组
     * @var array
     */


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                        'delete' => ['post'],
                    ],
                ],
        ];
    }


    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Menu;
//        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->MenuSave()) {
//            /*操作日志*/
//            $info = $model->description;
//            $op['zh'] = '创建菜单';
//            $op['en'] = 'Create menu';
//            $this->getLogOperation($op,$info,$result);
//
//            return $this->redirect(['create']);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//                'p_data' => Menu::findcorrespondMenu(),
//                'c_data' => Menu::findUrlMenu(),
//                'i_data' => Yii::$app->params['icon_data'],
//                ]);
//        }
//    }
    public function  actionTest1()
    {
        $model=new Menu();
        $data1 = AlarmLevel::findAllLevel();
        $data=$model->getAllInfo();
        return  $this->render('test1',[
                'data1'=>$data1,
                'data'=>$data,
            ]
            );
    }
    public function actionChart(){
        return $this->render('chart');
    }
    public function  actionTest(){

        $model=new Menu();

        //得到所有菜单
        /*
         *首先是 顶级菜单 即父类id为0，然后以顶级菜单为得到分级菜单信息
         *
        */
        //得到顶级菜单信息
        //传初始值

       $data=$model->getAllInfo();
        return $this->render('test',[

            'data'=>$data,
        ]);
    }
    public function get_submenu(){
        $data=yii::$app->request->post();
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

//    public function actionUpdate_menu($id)
//    {
//        $model = $this->findModel($id);
//        $leaf_node_old=MyFunc::array_multiToSingle(Menu::getArrUrlLeafNode($id));
//        if ($model->load($form_data = Yii::$app->request->post())   ) {
//            //在保存新的leaf_node的parent_id之前  还原本来的leaf_node的parent_id为1
//            //遍历还原 原来的leaf_node的parent_id
//            if(!empty($leaf_node_old)) {
//                foreach ($leaf_node_old as $key => $value) {
//                    $menu_model = Menu::getById($value);
//                    $menu_model ->parent_id = 1;
//                    $menu_model->save();
//                }
//            }
//            //在保存修改后的leaf_node的parent_id等相关信息
//            $result = $model->MenuSave();
//            /*操作日志*/
//            $info = $model->description;
//            $op['zh'] = '修改菜单';
//            $op['en'] = 'Update menu';
//            $this->getLogOperation($op,$info,$result);
//            return $this->redirect(['index']);
//        } else {
//            //if($model->icon==null){ $model->icon='fa-ge';$model->menuSave();}
//            $model->leaf_node = MyFunc::array_multiToSingle(Menu::getArrLeafNode($id));
//            $model->description = MyFunc::DisposeJSON($model->description);
//            $model->icon=null;
//            return $this->render('update', [
//                'model' => $model,
//                'p_data' => Menu::findcorrespondMenu($id),
//                'c_data' => Menu::findUrlMenu(),
//                'i_data' => Yii::$app->params['icon_data'],
//                ]);
//        }
//    }

    /**
     * 更新叶子节点
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function actionUpdate_item($id)
    {
        $model_arr = Item::getObj($id);
        $model = $model_arr[0];
        if ($model->load(Yii::$app->request->post()) && $result = $model->OneColumnSave()) {
            /*操作日志*/
            $info = $model->description;
            $op['zh'] = '修改子菜单';
            $op['en'] = 'Update submenu';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $model->description = MyFunc::DisposeJSON($model->description);
            return $this->render('update_item', [
                'model' => $model,
                'p_data' => Menu::findcorrespondMenu($id),
                ]);
        }
    }
    public function actionSuccess()
    {
        $model = new Menu();
        $data = $model->getAllInfo();
        $id = $_POST['id'];$num=$_POST['num'];
        $i=0;$sortable="sortable".$num;$table="tabel".$num;
        $html =''; //'<tbody id='.$sortable.' class="connectedSortable">';
        foreach($data[$id] as $value)
        {
            $html .= '<tr><td id=' . $data[$id][$i]["id"] . ' class='.$table.'>';
            $html.= $data[$id][$i]['description']. '</td></tr>';
            $i++;
        }
       //$html.='</tbody>';
     return $html;

    }
    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletes($id)
    {
        Menu::deleteById($id);
        /*操作日志*/
        $op['zh'] = '删除子菜单';
        $op['en'] = 'Delete submenu';
        $this->getLogOperation($op);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 返回格式化后的数组
     * @param $data
     * @return array|bool
     */
    public function formatData($data){

        if(!is_array($data)){
            return false;
        }

        $sorted_result = [];
        foreach($data as $tmp_key => $tmp_value)
        {

            $value = MyFunc::arraySortByKey($tmp_value,'idx');
//                print_r($value);
            $sorted_result[$tmp_key]=$value;
        }

        $newData = [];
        $temp_id = '';

        foreach($sorted_result as $parent_id => $value){
            foreach($value as $middle_value){
                $temp_id = $middle_value['id'];
                $newData[$parent_id][$temp_id]=$middle_value;
            }
        }

        return $newData;
    }

    /*获取原排序的子菜单数组
     *
     */
    public function getOriginSubMenu($data)
    {
        $sorted_result = [];
        foreach($data as $tmp_key => $tmp_value)
        {
            $value = MyFunc::arraySortByKey($tmp_value,'idx');
            $sorted_result[$tmp_key]=$value;
        }

        return $sorted_result;
    }


    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $a_menu = $model->get_all_menu();
        $temp = $model->getAllInfo();
        $format_data = $this->getOriginSubMenu($this->formatData($temp));
        $data=$_POST;

        if(\Yii::$app->request->isAjax){

//            echo '<pre>';
//            print_r($_POST);die;
            if ($_POST['symbol'] == 0 && $_POST['url'] == -1) {//
                if (empty($_POST['description'])) {
                    echo 'desc_null';
                    return false;
                }

                if (empty($_POST['modules_id'])) {
                    echo 'modules_id_null';
                    return false;
                }
                if (!isset($_POST['icon'])) {
                    echo 'icon_null';
                    return false;
                }

                if (empty($_POST['posi_data'])) {
                    echo 'posi_data_null';
                    return false;
                }
            }
            //创建菜单（完成）
            $_POST['id'] = $model -> createNewMenu($_POST);
            if(!$_POST['id']){
                return false;
            }

            $_POST['depth'] = $model->getDepth($_POST['parent_id']);
            //更新菜单功能模块 (完成)
            $res = $model -> updateMenuFunc($_POST);
            if(!$res){
                return false;
            }

        }

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->MenuSave()) {
            /*操作日志*/
            $info = $model->description;
            $op['zh'] = '创建菜单';
            $op['en'] = 'Create menu';
            $this->getLogOperation($op,$info,$result);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'p_data' => Menu::findcorrespondMenu(),
                'c_data' => Menu::findUrlMenu(),
                'i_data' => Yii::$app->params['icon_data'],
                'a_data' => json_encode($format_data),
                'a_menu' =>$a_menu,
                'id' => -1,
                'parent_id' => -1,
                'depth' => -1,
                'url' => -1,
                'symbol' => 0,
            ]);
        }
    }
    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate_menu($id)
    {
        $model = $this->findModel($id);
        $a_menu = $model->get_all_menu();
        $temp = $model->getAllInfo();
        $url = Yii::$app->request->get();
        $format_data = $this->getOriginSubMenu($this->formatData($temp));
        $parent_id = $model->getParentId($id);
        $depth = $model->getDepth($id);
        $iconer = $model->getIconer($id);
//        var_dump($iconer);die;

        if(\Yii::$app->request->isAjax){
//            echo '<pre>';
//            print_r($_POST);die;

            //修改菜单
            if ($_POST['symbol'] == 1 && $_POST['url'] == -1) {
                if (empty($_POST['description'])) {
                    echo 'desc_null';
                    return false;
                }

                if (!isset($_POST['icon'])) {
                    echo 'icon_null';
                    return false;
                }

                if (empty($_POST['posi_data'])) {
                    echo 'posi_data_null';
                    return false;
                }
            }

            //修改模块
            if ($_POST['symbol'] == 1 && $_POST['url'] == 1) {

                if (empty($_POST['description'])) {
                    echo 'desc_null';
                    return false;
                }

                if (!isset($_POST['icon'])) {
                    echo 'desc_null';
                    return false;
                }

                if (empty($_POST['posi_data'])) {
                    echo 'posi_data_null';
                    return false;
                }
            }

            //更新菜单功能模块

            $res = $model->updateMenuFunc($_POST);
//            echo '<pre>';
//            var_dump($res);die;
            if ($res) {
                echo true;
            }

        }

        $leaf_node_old=MyFunc::array_multiToSingle(Menu::getArrUrlLeafNode($id));
        if ($model->load($form_data = Yii::$app->request->post())   ) {
            //在保存新的leaf_node的parent_id之前  还原本来的leaf_node的parent_id为1
            //遍历还原 原来的leaf_node的parent_id
            if(!empty($leaf_node_old)) {
                foreach ($leaf_node_old as $key => $value) {
                    $menu_model = Menu::getById($value);
                    $menu_model ->parent_id = 1;
                    $menu_model->save();
                }
            }
            //在保存修改后的leaf_node的parent_id等相关信息
            $result = $model->MenuSave();
            /*操作日志*/
            $info = $model->description;
            $op['zh'] = '修改菜单';
            $op['en'] = 'Update menu';
            $this->getLogOperation($op,$info,$result);
            return $this->redirect(['index']);
        } else {
            //if($model->icon==null){ $model->icon='fa-ge';$model->menuSave();}
            $model->leaf_node = MyFunc::array_multiToSingle(Menu::getArrLeafNode($id));
            $model->description = MyFunc::DisposeJSON($model->description);
            $model->icon=null;
            return $this->render('update', [
                'model' => $model,
                'p_data' => Menu::findcorrespondMenu($id),
                'c_data' => Menu::findUrlMenu($id),
                'i_data' => Yii::$app->params['icon_data'],
                'a_data' => json_encode($format_data),
                'a_menu' => $a_menu,
                'id'=> $id,
                'parent_id' => $parent_id,
                'depth' => $depth,
                'url' =>isset($url['url'])?$url['url']:-1,
                'iconer' => $iconer,
                'symbol' =>1,
            ]);
        }
    }

}
