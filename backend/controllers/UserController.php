<?php

namespace backend\controllers;

use backend\models\ModuleRole;
use common\library\MyArrayHelper;
use common\library\MyFunc;
use Yii;
use common\models\User;
use backend\models\search\UserSearch;
use backend\controllers\MyController;
use yii\helpers\BaseArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\SignupForm;
use backend\models\AuthAssignment;
use backend\models\Role;
use backend\models\SysMenu;
use backend\models\UserMenu;
use backend\models\OperateLog;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends MyController
{
    public function behaviors()
    {
        return [
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['post'],
        ],
        ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */

    public function actionIndex()
    {
        $query_condition = Yii::$app->request->getQueryParams();
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search($query_condition);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /*public function actionIndex()
    {
        $query_condition = Yii::$app->request->getQueryParams();
        $query_condition['UserSearch']['field'][] = 'id';
        $query_condition['UserSearch']['relation'][] = '!=';
        $query_condition['UserSearch']['value'][] = Yii::$app->user->id;
        $user=User::findOne( Yii::$app->user->id);
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search($query_condition);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }*/

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
    }
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $m_r_model = new ModuleRole();
        $s_m_model = new SysMenu();
        $u_m_model = new UserMenu();
        $auth_info = ModuleRole::getRoleAuth();
        $role_info = MyArrayHelper::_map(BaseArrayHelper::map($auth_info, 'role_id', 'role'), 'id', 'name');

        if ($result = $model->load($form_data = Yii::$app->request->post()) && $user = $model->signup()) {
            /****************** 自动创建用户菜单 *********************************/
            $user_id = $user->id;
            $role_array = $form_data['SignupForm']['role_id'];
            $sys_menu = $s_m_model->getAllMenu();//所有系统菜单

            foreach($role_array as $key => $value){
                $temp_menu = $sys_menu;
                $temp_module_id_array[$key] = $m_r_model->findByRoleId($value);//获得用户角色所对应的模块ID

                foreach($temp_module_id_array[$key] as $kk => $vv){
                    $module_id_array[$kk]=$vv['module_id'];
                }

                $menu = $this->find_parent($temp_menu);

                foreach($menu as $k => $v){
                    if(!in_array($v['module_id'],$module_id_array) && !isset($v['have_son'])){
                        unset($menu[$k]);continue;
                    }else{
                        $menu[$k]['user_id']=$user_id;
                        $menu[$k]['role_id']=$value;
                    }
                }

                foreach($menu as $k => $v){
                    $p_id[] = $v['parent_id'];
                }

                foreach($menu as $k => $v){
                    if(isset($v['have_son'])){
                        if(!in_array($v['id'],$p_id)){
                            unset($menu[$k]);
                        }
                    }
                }


                if(count($menu)){
                    $insert_result = $u_m_model->createMenu($menu);
                }
            }

            if ($user) {
                return $this->redirect(['index']);
            }else{
                return $this->render('create', [
                    'model' => $model,
                    'tag' => 'create',
                    'role_info' => $role_info,
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'tag' => 'create',
            'role_info' => $role_info,
        ]);
    }

    function find_parent($arr) {
        foreach($arr as $key => $value){
            foreach($arr as $k => $v){
                if($value['id']==$v['parent_id']){
                    $arr[$key]['have_son']=1;
                    break;
                }
            }
        }
        return $arr;
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new SignupForm();
        $m_r_model = new ModuleRole();
        $s_m_model = new SysMenu();
        $u_m_model = new UserMenu();
        $user_model = $this->findModel($id);
        $role_array_old = explode(',', MyFunc::DisposeJSON($user_model->role_id));
        $auth_info = ModuleRole::getRoleAuth();
        $role_info = MyArrayHelper::_map(BaseArrayHelper::map($auth_info, 'role_id', 'role'), 'id', 'name');
        if ($model->load($form_data = Yii::$app->request->post()) && $user = $model->updateByid($id)) {
            $user_id = $id;
            $role_array = $form_data['SignupForm']['role_id'];
            $sys_menu = $s_m_model->getAllMenu();//所有系统菜单
            $role_delete_array = [];//删除的角色ID
            $role_add_array = [];//增加的角色ID
            foreach($role_array_old as $a => $b){
                if(!in_array($b,$role_array)) $role_delete_array[]=$b;
            }
            foreach($role_array as $x => $y){
                if(!in_array($y,$role_array_old)) $role_add_array[]=$y;
            }
            //删除用户角色所对应的菜单
            if(count($role_delete_array)){
                foreach($role_delete_array as $key => $value){
                    $u_m_model->deleteUserMenuByRoleId($user_id,$value);
                }
            }

            //增加用户角色所对应的菜单
            if(count($role_add_array)){
                foreach($role_add_array as $key => $value){
                    $temp_menu = $sys_menu;
                    $temp_module_id_array[$key] = $m_r_model->findByRoleId($value);//获得用户角色所对应的模块ID

                    foreach($temp_module_id_array[$key] as $kk => $vv){
                        $module_id_array[$kk]=$vv['module_id'];
                    }

                    $menu = $this->find_parent($temp_menu);

                    foreach($menu as $k => $v){
                        if(!in_array($v['module_id'],$module_id_array) && !isset($v['have_son'])){
                            unset($menu[$k]);continue;
                        }else{
                            $menu[$k]['user_id']=$user_id;
                            $menu[$k]['role_id']=$value;
                        }
                    }

                    foreach($menu as $k => $v){
                        $p_id[] = $v['parent_id'];
                    }

                    foreach($menu as $k => $v){
                        if(isset($v['have_son'])){
                            if(!in_array($v['id'],$p_id)){
                                unset($menu[$k]);
                            }
                        }
                    }
                    if(count($menu)){
                        $insert_result = $u_m_model->createMenu($menu);
                    }
                }
            }



            if ($user) {
                return $this->redirect(['index']);
            }else{
                return $this->render('update', [
                    'model' => $model,
                    'tag' => 'update',
                    'role_info' => $role_info,
                ]);
            }
        }
        $model->email = $user_model->email;
        $model->username = $user_model->username;
        $model->phone_number = $user_model->phone_number;
        $model->role_id = $role_array_old;

        return $this->render('update', [
            'model' => $model,
            'tag' => 'update',
            'role_info' => $role_info,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new User();
        $model->shield($id);
        return $this->redirect(['index']);
    }
    /*public function actionDelete($id)
    {
        $u_module=new UserMenu();
        $o_module = new OperateLog();
        $u_module->deleteUserMenuByUserId($id);
        $o_module->deleteOperateLog($id);
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }*/

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetAttr($attr,$val,$id){
        $result = User::findByAttr($attr,$val,$id);
        if(count($result)){
            return true;
        }else{
            return false;
        }
    }
}
