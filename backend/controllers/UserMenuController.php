<?php

namespace backend\controllers;

use Yii;
use backend\models\UserMenu;
use common\library\MyFunc;
use backend\models\ModuleRole;
use backend\models\Module;
use common\models\User;
use backend\models\SubSystem;
use backend\models\ReportManage;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;

/**
 * UserMenuController implements the CRUD actions for UserMenu model.
 */
class UserMenuController extends MyController
{
    static public $childNode=array();//存放父节点和父节点下面的子节点
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = new Session();
        $session->open();
        $user_id = $session['menu_user_id'];
        $username = $session['menu_user_name'];
        $role_id = $session['menu_role_id'];
        $con = 'id != 0';
        if(strstr($username,'电力')){
            $con = "username like '%电力%'";
        }elseif(strstr($username,'安全')){
            $con = "username like '%安全%'";
        }elseif(strstr($username,'楼控')){
            $con = "username like '%楼控%'";
        }elseif(strstr($username,'消防')){
            $con = "username like '%消防%'";
        }
        //查找出所有与当前角色相同的用户
        $all_user = User::get_name_role("id !=$user_id and is_shield = FALSE and $con and (role_id->'data')::text like '%\"".$role_id."\"%'");
        //echo "<pre>";print_r($all_user);die;
        $menu_data = UserMenu::getAllMenu();
        $nav = $this->getMenuTree($menu_data);
        return $this->render('index', [
            'nav' => $nav,
            'all_user' => $all_user,
            'username' => $username
        ]);
    }

    /**
     * Lists all UserMenu models.
     * @return mixed
     */
    public function actionMatchMenu($userid)
    {
        $session = new Session();
        $session->open();
        $user_id = $session['menu_user_id'];
        $role_id = $session['menu_role_id'];
        $menu_data = UserMenu::getAllMenuForMatch($userid,$role_id);
        foreach($menu_data as $k => $v){
            $menu_data[$k]['user_id'] = $user_id;
        }
        $model = new UserMenu();
        $model->deleteUserMenuByRoleId($user_id,$role_id);
        $model->createMenu($menu_data);
        return $this->redirect(['index']);
    }

    public function actionSetSession()
    {
        $user_id = Yii::$app->request->get('id');
        $user = User::getUser("id = $user_id");
        $str_role_id = explode(',', MyFunc::DisposeJSON($user[0]['role_id']));
        $role_id = $str_role_id[0];
        $session = new Session();
        $session->open();
        $session['menu_user_id']=$user_id;
        $session['menu_role_id']=$role_id;
        $session['menu_user_name']=$user[0]['username'];
        $session->close();
        return $this->redirect(['index']);
    }

    /**
     * 递归菜单数据 获得分类后的层级菜单
     * @param  array  $data 操作数组
     * @param  integer $p_id  父级id
     * @return array       排序后的数组
     */
    public static function getMenuTree( $data, $p_id=0 )
    {
        $r = array();
        foreach($data as $id=>$item){
            if($item['parent_id'] == $p_id) {
                $length = count($r);
                $r[$length] = $item;
                if($t = static::getMenuTree($data, $item['id']) ){
                    $r[$length]['children'] = $t;
                }
            }
        }
        return $r;
    }

    /**
     * Creates a new UserMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserMenu;
        $model->isNewRecord = true;
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->UserMenuSave()) {
            return $this->redirect(['index']);
        } else {
            $menu_data = UserMenu::getAllMenu();
            $p_data =  MyFunc::_map($menu_data, 'id', 'description');
            $c_data = MyFunc::_map(ModuleRole::findModuleByRoleId(), 'id', 'name');
            $c_data = ['0'=>'请模块选择']+$c_data;
            return $this->render('create', [
                'model' => $model,
                'p_data' => $p_data,
                'c_data' => $c_data,
                'i_data' => Yii::$app->params['icon_data'],
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate_menu($id)
    {
        $model = new UserMenu();
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $result = $model->UserMenuSave()) {
            return $this->redirect(['index']);
        } else {
            $menu_data = UserMenu::getAllMenu();
            $p_data =  MyFunc::_map($menu_data, 'id', 'description');
            $c_data = MyFunc::_map(ModuleRole::findModuleByRoleId(), 'id', 'name');
            $c_data = ['0'=>'请模块选择']+$c_data;
            $model = $this->findModel($id);
            $model->description = MyFunc::DisposeJSON($model->description);
            return $this->render('update', [
                'model' => $model,
                'p_data' => $p_data,
                'c_data' => $c_data,
                'i_data' => Yii::$app->params['icon_data'],
            ]);
        }
    }

    //获取父下的所有叶子节点
    public function findArrayNode($id,$list){
        foreach ($list as $key => $val){
            if ($id==$val['parent_id']){
                self::$childNode[]=(int)$val['id'];
                self::findArrayNode($val['id'], $list);     //ID
            }
        }
    }

    /**
     * Deletes an existing UserMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletes($id)
    {
        $this->findModel($id)->delete();
        $menu_data = UserMenu::getAllMenu();
        $this->findArrayNode($id,$menu_data);
        $child_id = self::$childNode;
        if(count($child_id)){
            self::$childNode=[];
            foreach($child_id as $k => $v){
                $this->findModel($v)->delete();
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the UserMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSources(){
        $module_id = Yii::$app->request->get('id');
        $m_models = new Module;
        $module_info = $m_models->findModuleById($module_id);
        $data = [];
        if($module_info[0]['path']=='subsystem_manage/crud/location-subsystem'){
            $sb_model = new SubSystem();
            $data = $sb_model->findAllData();
        }elseif($module_info[0]['path']=='scheduled-report/crud/new-index'){
            $rm_model = new ReportManage();
            $data = $rm_model->findAllUrl();
        }
        return json_encode($data);
    }
    public function actionCreate_child($id)
    {
        $model = new UserMenu();
        $model->isNewRecord = true;
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->UserMenuSave()) {
            return $this->redirect(['index']);
        } else {
            $c_data = MyFunc::_map(ModuleRole::findModuleByRoleId(), 'id', 'name');
            $c_data = ['0'=>'请模块选择']+$c_data;
            $model = $this->findModel($id);
            $join=$model->description = MyFunc::DisposeJSON($model->description);
            $like=$model->id;
            $join=[$like=>$join];
            $p_data=$join;
            $model->id=null;
            $model->description=null;
            $model->idx=null;
            $model->module_id=null;
            $model->param=null;
            $model->icon=null;
            return $this->render('addleaf', [
                'model' => $model,
                'p_data' => $p_data,
                'c_data' => $c_data,
                'i_data' => Yii::$app->params['icon_data'],
            ]);
        }
    }

}
