<?php

namespace backend\controllers;

use common\library\MyFunc;
use Yii;
use backend\models\SystemModule;
use backend\models\search\SystemModuleSearch;
use backend\controllers\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\OperateLog;
/**
 * SystemModuleController implements the CRUD actions for SystemModule model.
 */
class SystemModuleController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SystemModule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SystemModuleSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single SystemModule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SystemModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SystemModule;

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->moduleSave()) {
            /*操作日志*/
            $info = $form_data['SystemModule']['description'];
            $op['zh'] = '创建模块';
            $op['en'] = 'Create module';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SystemModule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->moduleSave($model->id)) {
            /*操作日志*/
            $info = $form_data['SystemModule']['description'];
            $op['zh'] = '修改模块';
            $op['en'] = 'Update module';
            $this->getLogOperation($op,$info,$result);

            return $this->redirect(['index']);
        } else {
            $res_arr = json_decode($model->description, true);
            $model->description = $res_arr['data']['zh-cn'];
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SystemModule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除用户添加模块';
        $op['en'] = 'Delete users to add modules';
        $this->getLogOperation($op);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SystemModule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SystemModule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SystemModule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
