<?php

namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\helpers\BaseArrayHelper;
use backend\models\ModuleRole;
use backend\models\SystemModule;
use backend\models\OperateLog;
use backend\models\search\ModuleRoleSearch;
use backend\controllers\MyController;

/**
 * ModuleRoleController implements the CRUD actions for ModuleRole model.
 */
class ModuleRoleController extends MyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * 判断 是创建一个 权限关系 还是修改
     * 一个角色 对应 多个模块权限
     * Lists all ModuleRole models.
     * @return mixed  http://www.ems.com/module-role/create?role_id=2
     */
    public function actionIndex($id)
    {
        $model_role = ModuleRole::getByRoleId($id);
        return $model_role ? $this->redirect('/module-role/update?role_id='.$id) : $this->redirect('/module-role/create?role_id='.$id);
    }

    /**
     * Displays a single ModuleRole model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ModuleRole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($role_id)
    {
        $model = new ModuleRole;
        if ($model->load(Yii::$app->request->post()) && $result = $model->moduleRoleSave()) {
            /*操作日志*/
            $op['zh'] = '添加权限';
            $op['en'] = 'Create privilege';
            $this->getLogOperation($op,'',$result);

            return $this->redirect(['/role']);
        } else {
            $model->role_id = $role_id;
            $system_module = SystemModule::getModuleInfo();
            $module_info = BaseArrayHelper::map($system_module, 'module', 'id');
            $action_info = BaseArrayHelper::map($system_module, 'module', 'action_info');
            return $this->render('create', [
                'model' => $model,
                'module_info' => $system_module,
                'action_info' => $action_info
            ]);
        }
    }

    /**
     * 该表是 角色赋权的中间表没有index界面 所以 当前修改的id 应该为角色id
     * Updates an existing ModuleRole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function  update($role_id)
    {
        $module_model = ModuleRole::getByRoleId($role_id);
        $model = $this->findModel($role_id);
        //根据 角色id找出一条 关联记录
        if ($model->load($form_data = Yii::$app->request->post()) && $result = $model->moduleRoleSave()) {
            /*操作日志*/
            /*$info = $form_data['Role']['name'];
            $op['zh'] = '修改权限';
            $op['en'] = 'Update privilege';
            $this->getLogOperation($op,$info,$result);*/
            return $this->redirect(['/role']);
        } else {
            $action_data = BaseArrayHelper::map($module_model, 'module_id', 'auth_action');
            $system_module = SystemModule::getModuleInfo();
            $module_info = BaseArrayHelper::map($system_module, 'module', 'id');
            $action_info = BaseArrayHelper::map($system_module, 'module', 'action_info');
            return [
                'model' => $model,
                'module_info' => $system_module,
                'action_info' => $action_info,
                'action_data' => $action_data,
            ];
        }

    }
    public function actionUpdate($role_id)
    {
        $update=$this->update($role_id);
            return $this->render('update', [
                'model' => $update['model'],
                'module_info' => $update['module_info'],
                'action_info' => $update['action_info'],
                'action_data' => $update['action_data'],
            ]);
        }


    /**
     * Deletes an existing ModuleRole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /*操作日志*/
        $op['zh'] = '删除角色';
        $op['en'] = 'Delete role';
        $this->getLogOperation($op);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ModuleRole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ModuleRole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ModuleRole::findOne(['role_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
