<?php

namespace backend\controllers;

class DownloadController extends MyController
{
    public function init()
    {
        $this->siteInfo();
    }

    public function actionIndex()
    {
        $this->layout = '/onlyContent';
        return $this->render('index');
    }

    public function actionSao()
    {
        $this->layout = '/onlyContent';
        return $this->render('sao');
    }
}
