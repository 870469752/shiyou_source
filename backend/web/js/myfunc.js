/*
 * 我们JS的函数方法统一都放在这个文件里面。
 * =======================================================================
 * 添加时注意内部是否有同类型函数。
 * =======================================================================
 * 葛盛庭    2014-09-18
 */
/********************************************** 应用外部js ***************************************      start */

// JS原型链引用
document.write("<script language='javascript' src='/js/my_js/my_prototype.js'></script>");
//// 系统自动检测模块引用
//document.write("<script language='javascript' src='/js/my_js/monitoring.js'></script>");

/********************************************** 应用外部js ***************************************      end */

$(document).ready(function(){
    //初始化smartadmin部分插件
    pageSetUp();
    // 删除所有select2的100%宽度
    $(".select2").removeAttr('style');
});





/*   定义 存入所有语言的json格式变量 */
//由于需要在js文件中进行翻译 需要用ajax来获取翻译文件中的 数据 但ajax请求的地址并未包含语言 需要手动添加
var langs = '';
        $.ajax({
            url: '/my/lang',
            async:false,
            dateType: 'json',
            data: 'url='+window.location.href,
            success: function(data)
            {
              langs = JSON.parse(data);
            }
        });
/**
 * 调用该方法即可获取翻译
 * @param _ls                   需要翻译的 文字
 * @returns {*}
 */
function lang(_ls)
{
    return typeof(langs[langs['type']]) === 'undefined'  ? _ls : (typeof(langs[langs['type']][_ls]) === 'undefined' ? _ls : langs[langs['type']][_ls]);
}
/*
 * 此函数主要用来测试不同类型数据输出
 * Alert Dump
 */
function ad(data){
	if(typeof(data) == "object" && Object.prototype.toString.call(data).toLowerCase() == "[object object]" && !data.length){
		alert(JSON.stringify(data));
	}
	else{
		alert(data);
	}
}

/*
 * 表格配置文件
 * @param string id 标签id
 * @param json options 配置参数
 * 
 * 样例：
 * {
 *     "botton":["<a href='#'>创建</>","<a href='#'>搜索</>"],
 *     "tools":["copy", "csv"],
 *     "dom":"t"
 * }
 */
function table_config(id, options){
	
	// 载入时响应式帮助为空
	var responsiveHelper_datatable = undefined;
	var oTableTools = {
    	"aButtons": [
	    	"copy",
	    	"csv",
	    	"xls",
	     	{
	    		"sExtends": "print",
	    		"sMessage": "EMS <i>(press Esc to close)</i>"
	    	}
       	],
       	"sSwfPath": "/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
    };
	/* 
	 * 表格整体布局参数
	 * f 代表查找框
	 * T 导出工具组件
	 * r 处理显示元素
	 * t 表格内容（由上面写的HTML载入）
	 * i 记录信息
	 * p 分页连接
	 */
	var sDom = "<'dt-toolbar'<'col-xs-12 col-sm-6 btn-group'><'col-sm-6 col-xs-6 hidden-xs'T>r>"+"t";
	if(options.tools){
		oTableTools.aButtons = options.tools;
	}
	if(options.dom){
		sDom = options.dom;
	}
	/* TABLETOOLS */
	$('#'+id).dataTable({
		"sDom": sDom,
        
		// 工具组件配置参数
		"oTableTools": oTableTools,
        "ordering": false,
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// 初始化响应式帮助，用于屏幕放不下多列时，可下拉查看.
			if (!responsiveHelper_datatable) {
				responsiveHelper_datatable = new ResponsiveDatatablesHelper($('#datatable'), {tablet : 1024, phone : 480});
			}
		},
		"rowCallback" : function(nRow) {
			// 当屏幕小于设定值时，显示下拉图标。
			responsiveHelper_datatable.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			//表格绘制完毕后的回调应答
			responsiveHelper_datatable.respond();
		}
	});
	for (var index in options.button){
		$("#datatable_wrapper .dt-toolbar .col-xs-12").append(options.button[index]);
	}
	/* END TABLETOOLS */
}


/**
 * 表单编辑JSON名称的脚本,必须得有隐藏框
 * @param model
 * @param lang
 */
function edit_name(model,lang){
    $('#'+model.toLowerCase()+'-name').change(function(){
        var ele_name = $('[name="'+model+'[name]"]');
        var value = JSON.parse(ele_name.val());
        value['data'][lang] = $(this).val();
        ele_name.val(JSON.stringify(value));
    })
}

/**
 * 本地化语言和时间
 */
function highcharts_lang_date(){
    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            months: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
            shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月',  '七月', '八月', '九月', '十月', '十一月', '十二月'],
            weekdays: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']
        },
        exporting:{
            url:'/HighChartsExport.php'
        },
        credits: {
            enabled: false
        }
    });
}

/**
 *
 */
function datepicker_lang_date(min, max){
    $.datepicker.setDefaults({
        minDate: min,
        maxDate: max,
        dateFormat: "yy-mm-dd",
        dayNames: [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ],
        dayNamesMin: [ "日", "一", "二", "三", "四", "五", "六" ],
        monthNames: [ "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" ],
        monthNamesShort: [ "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月" ],
        changeMonth: true,
        numberOfMonths: 1,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>'
    });
}

/**
 * 时间戳转换时间 2014-01-01 00:00:00
 * @param timestamp
 * @returns {string}
 * @constructor
 */
function TimestampToDate(timestamp)   {
    if(timestamp == ''){
        var now = new Date();
    }
    else{
        var now = new Date(parseInt(timestamp));
    }
    var year=now.getFullYear();
    var month=now.getMonth()+1;
    var date=now.getDate();
    var hour=now.getHours();
    var minute=now.getMinutes();
    var second=now.getSeconds();
    return year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second;
}

/**
 * 饼图
 * @param id
 * @param data
 * @param unit
 */
function chartPie(id, data, unit){
    $('#'+id).highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}: {point.y:.2f}</b> ' + unit
                }
            }
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.name}: {point.percentage:.2f}%'
        },
        series: data
    });
}

/**
 * 横条图
 * @param id
 * @param data
 * @param name
 * @param height
 */
function chartBar(id, data, name, height){
    $('#'+id).highcharts({
        chart: {
            type: 'bar',
            height: height
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        xAxis: {
            categories: name
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        legend: {
            reversed: true,
            enabled: true,
            align: '',
            verticalAlign: 'top'
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    '总量: ' + this.point.stackTotal;
            }
        },
        series: data
    });
}

/**
 * 折线图
 * @param id
 * @param data
 * @param unit
 * @param height
 */
function chartLine(id, data, unit, height){
    $('#'+id).highcharts({
        chart: {
            height: height
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        yAxis: {
            labels:{
                format: '{value} '+unit
            },
            title: {
                text: ''
            }
        },
        xAxis: {
            type: 'datetime'
        },
        legend: {
            enabled: true,
            align: '',
            verticalAlign: 'top'
        },
        tooltip: {
            shared: true,
            valueSuffix: unit
        },
        series: data
    });
}

/**
 * 堆叠柱状图
 * @param id
 * @param data
 * @param unit
 * @param height
 */
function chartColumn(id, data, unit){
    $('#'+id).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: unit
            }
        },
        xAxis: {
            type: 'datetime'
        },
        legend: {
            enabled: true,
            align: '',
            verticalAlign: 'top'
        },
        plotOptions: {
            series: {
                stacking: 'normal' // 堆叠
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: unit
        },
        series: data
    });
}

/**
 * 时间选择 插件  注：时间插件的 class 目前需要 用'form_datetime'
 * @param time_type 时间插件格式类型
 * @param obj 时间插件的 dome对象
 */
function timeTypeChange(time_type, obj)
{
    //添加中文 语言
    $.fn.datetimepicker.dates['zh-cn'] = {
        days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
        daysShort: ["日", "一", "二", "三", "四", "五", "六", "日"],
        daysMin: ["日", "一", "二", "三", "四", "五", "六", "日"],
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        today: "今天",
        suffix: [],
        meridiem: ["上午", "下午"],
        weekStart: 1,
        format: "yyyy-mm-dd"
    };

    obj.datetimepicker('remove');
    obj.val('');
    if(time_type == 'year'){
        obj.datetimepicker({
            language: 'zh-cn',
            format: "yyyy",
            startView: 'decade',
            minView: 'decade',
            todayBtn:'linked',
            autoclose: true
        });
        obj.datetimepicker('setStartDate', '2014');
    }
    else if(time_type == 'month')
    {
        obj.datetimepicker({
            language: 'zh-cn',
            format: "yyyy-mm",
            startView: 'year',
            minView: 'year',
            todayBtn:'linked',
            autoclose: true
        });
        obj.datetimepicker('setStartDate', '2014-01');
    }
    else if(time_type == 'hour') {
        obj.datetimepicker({
            language: 'zh-cn',
            format: "yyyy-mm-dd hh:ii:ss",
            startView: 'day',
            minView: 'hour',
            todayBtn:'linked',
            autoclose: true
        });
        obj.datetimepicker('setStartDate', '2014-01-01 00:00');
    }
    else {
        obj.datetimepicker({
            language: 'zh-cn',
            format: "yyyy-mm-dd",
            startView: 'month',
            minView: 'month',
            todayBtn:'linked',
            autoclose: true
        });
        obj.datetimepicker('setStartDate', '2014-01-01');
    }
}

/**
 *  这里 需要用到 Sortable 这个插件
 *  功能是 用于两个方正 进行元素交换
 *  如： 两个table 交换 tr
 */
function c_sortable($jq1_id, $jq2_id, $fill_id)
{
    /***在没有移动前就把 报警规则序列给赋值到input填充框中**/
    var $sort_data = '';
    $('#'+$jq1_id).find('tr').each(function (){
        $sort_data += $(this).data('id') + ',';
    })
    $("#"+$fill_id).val($sort_data);

    $(function() {
        $( "#"+$jq1_id+", #"+$jq2_id).sortable({
            connectWith: ".connectedSortable",
            opacity: 0.7,
//            revert: true,
            //开始拖动
            start:function( event, ui ) {
                //sortable 自生会加一个tr需要减一
                var $length = ui.item.parent().find('tr').length - 1;
                if($length == 1)
                {
                    ui.item.parent().append("<tr id = 'Additional' ><td></td></tr>");
                }
            },
            //停止拖动
            stop:function( event, ui ) {
                //事件处理函数
                ui.item.parent().find('#Additional').remove();
                //将排序好的数据 存入对应的填充参数中
                var $sort_data = '';
                $('#'+$jq1_id).find('tr').each(function (){
                    $sort_data += $(this).data('id') + ',';
                })
                $("#"+$fill_id).val($sort_data)
            }
        }).disableSelection();
    });
}

/**
 * 利用x-editable 生成时间选择插件
 * 形式： seconds: 10
 *       minutes: 30
 *       hourts: 12
 *       days: 24
 *       weeks: 2
 *
 * @param $Obj   jquery 对象 ，需要在对应的标签上添加属性： data-type = 'timec'
 *               注： 会将当前时间转为秒并赋值给其相邻的下一个元素
 */
function timeOfEditable($Obj)
{
    var $next_dome = $Obj.next();
    $Obj.editable({
        placement: 'right',
        validate: function(value) {
            if(value.seconds >= 60) return 'seconds Must be less than 60!';
            if(value.minutes >= 60) return 'minutes Must be less than 60!';
            if(value.hours >= 24) return 'hours Must be less than 24!';
            if(value.days >= 7) return 'days Must be less than 7!';
        },
        display: function(value) {
            if(!value) {
                $(this).empty();
                return;
            }
            var html =
                $('<div>').text(value.seconds).html() + ' 秒, ' +
                    $('<div>').text(value.minutes).html() + ' 分钟, ' +
                    $('<div>').text(value.hours).html() + ' 小时, ' +
                    $('<div>').text(value.days).html() + ' 天, ' +
                    $('<div>').text(value.weeks).html() + ' 星期, ' +
                    $('<div>').text(value.months).html() + ' 月(31天).' +
                    '</b>';
            var $total_time = parseInt('0' + value.seconds) +
                parseInt('0' + value.minutes) * 60 +
                parseInt('0' + value.hours) * 3600 +
                parseInt('0' + value.days) * 86400 +
                parseInt('0' + value.weeks) * 604800 +
                parseInt('0' + value.months) * 2678400;
            $next_dome.val($total_time);
            $(this).html(html);
        }
    });
};