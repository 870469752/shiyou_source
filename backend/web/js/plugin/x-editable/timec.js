/**
 * @auth cre
 *
Timec editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}
@class timec
@extends abstractinput
@final
@example
<a href="#" id="timec" data-type="timec" data-pk="1">awesome</a>
<script>
$(function(){
    $('#timec').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    
    var Timec = function (options) {
        this.init('timec', options, Timec.defaults);
    };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Timec, $.fn.editabletypes.abstractinput);

    $.extend(Timec.prototype, {
        /**
        Renders input from tpl

        @method render() 
        **/        
        render: function() {
           this.$input = this.$tpl.find('input');
        },
        
        /**
        Default method to show value in element. Can be overwritten by display option.
        
        @method value2html(value, element) 
        **/
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }
            var html =  '<b>' +
                        $('<div>').text(value.seconds).html() + '</b>, <b>' +
                        $('<div>').text(value.minutes).html() + '</b>, <b>' +
                        $('<div>').text(value.hours).html() + '</b>, <b>' +
                        $('<div>').text(value.days).html() + '</b>, <b>' +
                        $('<div>').text(value.weeks).html() + '</b>';
            $(element).html(html); 
        },
        
        /**
        Gets value from element's html
        
        @method html2value(html) 
        **/        
        html2value: function(html) {        
          /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g. 
            editable({
                value: {
                    city: "Moscow", 
                    street: "Lenina", 
                    building: "15"
                }
            });
          */ 
          return null;  
        },
      
       /**
        Converts value to string. 
        It is used in internal comparing (not for sending to server).
        
        @method value2str(value)  
       **/
       value2str: function(value) {
           var str = '';
           if(value) {
               for(var k in value) {
                   str = str + k + ':' + value[k] + ';';  
               }
           }
           return str;
       }, 
       
       /*
        Converts string to value. Used for reading value from 'data-value' attribute.
        
        @method str2value(str)  
       */
       str2value: function(str) {
           /*
           this is mainly for parsing value defined in data-value attribute. 
           If you will always set value by javascript, no need to overwrite it
           */
           return str;
       },                
       
       /**
        Sets value of input.
        
        @method value2input(value) 
        @param {mixed} value
       **/         
       value2input: function(value) {
           if(!value) {
             return;
           }
           this.$input.filter('[name="seconds"]').val(value.seconds);
           this.$input.filter('[name="minutes"]').val(value.minutes);
           this.$input.filter('[name="hours"]').val(value.hours);
           this.$input.filter('[name="days"]').val(value.days);
           this.$input.filter('[name="weeks"]').val(value.weeks);
           this.$input.filter('[name="months"]').val(value.weeks);
       },       
       
       /**
        Returns value of input.
        
        @method input2value() 
       **/          
       input2value: function() { 
           return {
               seconds: this.$input.filter('[name="seconds"]').val(),
               minutes: this.$input.filter('[name="minutes"]').val(),
               hours: this.$input.filter('[name="hours"]').val(),
               days: this.$input.filter('[name="days"]').val(),
               weeks: this.$input.filter('[name="weeks"]').val(),
               months: this.$input.filter('[name="months"]').val()
           };
       },        
       
        /**
        Activates input: sets focus on the first field.
        
        @method activate() 
       **/        
       activate: function() {
            this.$input.filter('[name="seconds"]').focus();
       },  
       
       /**
        Attaches handler to submit form in case of 'showbuttons=false' mode
        
        @method autosubmit() 
       **/       
       autosubmit: function() {
           this.$input.keydown(function (e) {
                if (e.which === 13) {
                    $(this).closest('form').submit();
                }
           });
       }       
    });

    Timec.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-timec"><label><span>'+lang('Seconds')+': </span><input type="text" name="seconds" class="input-small"></label></div>'+
             '<div class="editable-timec"><label><span>'+lang('Minutes')+': </span><input type="text" name="minutes" class="input-small"></label></div>'+
             '<div class="editable-timec"><label><span>'+lang('Hours')+': </span><input type="text" name="hours" class="input-mini"></label></div>'+
            '<div class="editable-timec"><label><span>'+lang('Days')+': </span><input type="text" name="days" class="input-mini"></label></div>'+
            '<div class="editable-timec"><label><span>'+lang('Weeks')+': </span><input type="text" name="weeks" class="input-mini"></label></div>'+
            '<div class="editable-timec"><label><span>'+lang('Months')+': </span><input type="text" name="months" class="input-mini"></label></div>',
             
        inputclass: ''
    });

    $.fn.editabletypes.timec = Timec;

}(window.jQuery));