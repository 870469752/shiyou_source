/**
 * Created by cre on 14-11-18.
 */
var data = {};
var _type = '';
var time_data = '';

$("#alarmlog-type").change(function (){
    _type = $(this).val();
    timeTypeChange(_type, $('#timepick'));
})
/*时间选择框中的 值 变化时 ajax载入新的数据*/
$('#timepick').datetimepicker().on('changeDate', function(ev){
    time_data = $("#timepick").val();
    _type = $("#alarmlog-type").val();
    getHigchart();
    getAjax('/alarm-log/crud/log-chart?type='+_type+'&time='+time_data);
});

highcharts_lang_date();
function getHigchart()
{
$('#alarm_chart').highcharts('StockChart', {
            chart: {
                type: 'scatter',
                borderWidth: 0,
                zoomType: 'xy',
                high:'400px'
            },
            credits: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            },
            credits : {
                enabled : false
                },
            title: {
                text: lang('Alarm log scatter plot')
                },
            xAxis: {
                title: {
                enabled: true,
                text: lang('Timeline')
                },
            type: 'datetime',
            minRange: 3600 * 1000,
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
            },
            yAxis: {
                min: 1,
                minTickInterval: 1,
                minRange: 3,
                title: {
                text: lang('Alarm event serial number')
                }
            },
            tooltip: {
                formatter: function() {
                    if(this.y == 0)
                    return ''+ ' <b>df</b> ';
                    else
                    return ''+ ' <b> ' + lang('Event ID') + '</b>: ' + this.y ;
                }
            },
            legend: {
                enabled: false,
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                y: 70,
                floating: true,
                backgroundColor: '#FFFFFF',
                borderWidth: 1
                },
            plotOptions: {
            scatter:
            {
                marker:
                {
                    radius: 5,
                    states:
                    {
                        hover:
                        {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                     }
                 },
                states:
                {
                    hover: {
                                marker:
                                {
                                    enabled: false
                                }
                            }
                }
            }
            },
            series: [{
                name: 'AlarmLog',
                color: 'rgba(223, 83, 83, .5)',
                }]
});
}
/**
 * ajax向后台 获取数据
 * @param url
 */
function getAjax(url)
{
    var chart = $('#alarm_chart').highcharts();
    $.getJSON(url,
        function (result) {
            console.log(result);
            chart.series[0].setData(result);
            chart.hideLoading();
        });
}

/**
 * 切换时间 左右按钮的 事件处理
 * @param time
 * @param type
 * @returns {string}
 * @constructor
 */
function DateCustom(time, type)
{
    var date = new Date(time);
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var day = date.getDate();
    var _date = new Date(year, month, 0);
    var daysCount = _date.getDate();
    var pre_date = new Date(year, month - 1, 0);
    var pre_days = pre_date.getDate();
    //处理 时间移动到 年 或月 显示逻辑
    if(month == 1 && number == -1 && type == 'month')
    {
        year = year - 1;
        month = 13;
    }
    else if(month == 12 && number == 1 && type == 'month')
    {
        year = year + 1;
        month = 0;
    }
    else if(day == daysCount && number == 1 && type == 'day')
    {
        if(month < 12)
        {
            month = month + 1;
            day = 0;
        }
        else
        {
            year = year + 1;
            month = 1;
            day = 0;
        }
    }
    else if(day == 1 && number == -1 && type == 'day')
    {
        if(month > 1)
        {
            month = month -1;
            day = pre_days + 1;
        }
        else
        {
            year = year - 1;
            month = 12;
            day = pre_days + 1;
        }
    }
    else if(day+7 > daysCount && number == 1 && type == 'week')
    {
        if(month < 12)
        {
            month = month + 1;
            day = day - daysCount;
        }
        else
        {
            year = year + 1;
            month = 1;
            day = day - daysCount
        }
    }
    else if(day - 7 < 1 && number == -1 && type == 'week')
    {
        if(month > 1)
        {
            month = month - 1;
            day = pre_days + day;
        }
        else
        {
            year = year - 1;
            month = 12;
            day =  pre_days + day;
        }
    }

    switch(type)
    {
        case 'month':
            return year+'-'+(month+1*number);
        case 'week':
            return year+'-'+month+'-'+(day+7*number);
        case 'day':
            return year+'-'+month+'-'+(day+1*number);
    }
}
/*手动 触发 时间插件 和 类型框 绑定的 事件*/
$('#timepick').trigger('changeDate');
$('#alarmlog-type').trigger('change');
$('#timepick').val(time_data);
/*  点击 左右 切换日期*/
var number = 1;
$('#btn-prev').click(function () {
    number = -1;
    _type = $("#alarmlog-type").val();
    time_data = DateCustom($("#timepick").val(), _type);
    console.log(time_data);
    $('#timepick').val(time_data);
    getHigchart();
    getAjax('/alarm-log/crud/log-chart?type='+_type+'&time='+time_data);
})

$('#btn-next').click(function () {
    number = 1;
    _type = $("#alarmlog-type").val();
    time_data = DateCustom($("#timepick").val(), _type);
    console.log(time_data);
    $('#timepick').val(time_data);
    getHigchart();
    getAjax('/alarm-log/crud/log-chart?type='+_type+'&time='+time_data);
})

