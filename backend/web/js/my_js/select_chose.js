/**
 * Created by cre on 14-11-10.
 *
 * 模块: calculate-point
 * 效果：实现对公式动态编辑
 * 功能描述：该js是有几个js插件组合而成，主要实现了在多选select2插件的基础上可以对每一个 ‘option’ 进行重新选择或可再次进行多选
 * 规则：
 *      1、‘[]’中括号代表替换的option 是一个唯一的单选点位
 *      2、‘{}’大括号代表替换的option 是由几个点位组合(求和，求平均等)的复合点位
 *      3、 公式之间需要 用 空格 分隔开
 *      4、 不论 公式模板 是否是 多选点位 在修改时 同一为 单选
 *      5、 公式模板中 选择的点位 必须是点位单位 在 单位表中存在的
 *      （下一步：将模板公式中的所需替换的点位 规定为当前替换元素的 同一归类）
 */
        /*点位 下拉菜单*/
        var points = [];
        var point_str = '';
        var point_json = '';
        var formula = [];
        var formula_json = '';
        //如果已选择公式 就将名字 赋值给 公式名
        var formula_name = $("input[name='template_name']").val();
        var formula_arr = [];
        var formula_id = '';
        var formula_unit = '';
        var unit_calculate = [];
        var unit_standard = [];
        var unit_result = [];
        var unit_arr = [];
        //在模板lable后添加公式选择框
        $('.field-calculatepoint-formula .control-label').prepend('请选择');
        $('.field-calculatepoint-formula .control-label').append(': <a href="form-x-editable.html#" data-placement="right"  id="formula" data-type="select2" data-pk="1" >'+formula_name+'</a>')
        //获取 点位信息的
        $.ajax({
                    url: '/calculate-point/crud/ajax-point',
                    async:false,
                    dateType: 'json',
                    success: function(data)
                    {
                        point_json = JSON.parse(data)
                        $.each(point_json, function (k, v) {
                            point_str += "'" + v + "',",
                                points.push({
                                    id: k,
                                    text: v
                                });
                        });
                        point_str = '[' + point_str.substring(0,point_str.length - 1) + ']';
                    }
        });
        //获取模板公式的信息 包括 ： 1、公式 2、计算单位
        $.ajax({
            url: '/calculate-point/crud/ajax-formula',
            async:false,
            dateType: 'json',
            success: function(data)
            {
                data = JSON.parse(data);
                //将获取的数据 进行分组
                $.each(data, function (k, v) {
                    //找出 公式数据 并将 器组成 数组
                    if(k == 'formula')
                    {
                        formula_json = v;
                        $.each(v, function (k1, v1) {
                            formula.push({
                                id: k1,
                                text: v1
                            });
                        })
                    }
                    //找出 单位的数据 细化单位的类型
                    else if(k == 'unit')
                    {
                        //将单位 拆分为 计算单位 值单位
                        $.each(v, function (k2, v2) {
                            //计算点位单位的 字符串
                            unit_standard.push({
                                id: v2.name,
                                text: v2.calculate
                            });
                            //计算点位单位的 数组
                            unit_calculate.push({
                                id: v2.name,
                                text: v2.calculate.split(',')
                            });
                            //计算公式值得 单位
                            unit_result.push({
                                t_id: v2.id,
                                id: v2.name,
                                value: v2.result
                            });
                        })
                    }
                });
            }
        });

        var elt = $("#_tags");
        //绑定 多选框事件
        elt.tagsinput({
            allowDuplicates: true
        })
        //绑定 点击事件
        $("#formula").editable({
            showbuttons: false,
            title: 'Choose Formula',
            source: formula,
            select2: {
                width: 200
            }

        });

        //添加 一个提示 在单位 下拉菜单中 插入一个空的 option
        $("#calculatepoint-unit").prepend('<option></option>');
        $("#calculatepoint-unit").attr('placeholder', '请选择单位 (不选择单位为空)')


        //选择公式后  展示在 input框中
        $('#formula').on('save.newuser', function(){
            elt.tagsinput('refresh');
            elt.tagsinput('removeAll');
            var that = this;
            //该事件是 选择‘select’而获取公式的名字 是在该事件发生之后 需要短暂的时间延迟 才会将选择到的公式名字放到所选择的元素上
            setTimeout(function(){
                $(".tag").find('span').remove();
                //短暂延迟 将弹出框事件执行完 公式name的值才会改变
                $.each(formula_json, function(k, v){
                    if(v == $(that).text())
                    {
                        var gs_arr = k.split(' ');
                        $.each(gs_arr, function (k1, v1) {
                            elt.tagsinput('add', v1);
                        })
                        /*给 公式中的每一个元素 tag标签 绑定点击事件*/
                        tagBd();
                    }
                    formula_name =  $(that).text();
                    //赋值公式值的单位
                    $.each(unit_result, function(k , v) {
                        if(v.id == formula_name)
                        {
                            //获取当前模板的值单位
                            formula_unit = v.value;
                            //获得当前模板的id
                            formula_id = v.t_id;
                        }
                        //将模板 id 放入对应的 input 框中
                        $("input[name='CalculatePoint[template_id]']").val(formula_id)
                        //如果模板有自带单位 将单位 填到下拉菜单中
                        $("#calculatepoint-unit").select2("val", '');
                        $("#calculatepoint-unit option").each(function(k, v) {
                            var sel_text = $(this).text();
                            if(sel_text == formula_unit)
                            {
                                $("#calculatepoint-unit").select2("val", $(this).val());
                            }
                        })
//                        console.log($("#calculatepoint-unit").val())
                    })
                })
            },1);
        })

        /*给 tag标签 绑定点击事件*/
        function tagBd()
        {
            formula_name = $('#formula').text();
            //利用 公式name 寻找当前公式的 计算方式 及 单位
            $.each(unit_calculate, function (k, v){
                if(v.id == formula_name)
                {
                    unit_arr = v.text;
                }
            });
            $.each(formula, function (k, v){
                if(v.text == formula_name)
                {
                    formula_arr = v.id.split(' ');
                }
            })
            //找出 选中公式的 的模板 及单位
            var i = 0;      /* i 代表 只有当是 点位时 才会加1*/
            var j = 0;      /* j 代表 只要是 公式内的元素 就会加1*/
            //循环判断 公式中的元素绑定哪种类型的 点击事件
            $(".tag").each(function(k, v){
                var _text = $(this).text();
                //将对应的 公式元素名称 及 单位 列出
                var formula_column = typeof(formula_arr[j]) == 'undefined' ? formula_arr[0] : formula_arr[j];
                var unit_column = typeof(unit_arr[i]) == 'undefined' ? unit_arr[0] : unit_arr[i];
                //如果是唯一点位 则绑定单选点击框
                if(_text.indexOf('[') !== -1 )
                {
                    _text = _text.replace(/[\[\]]/g,'');
                    $(this).attr({
                        'data-type':'select2',
                        'point' : 'point'

                    });
                    $(this).editable({
                        title: formula_column + '(' + unit_column + ')',
                        source: points,
                        select2: {
                            width: 200
                        }
                    });
                    $(this).css({
                        'background-color' : '#428bca',
                        'border-style': 'none'
                    });
                    ++i;
                }
                //如果是 多选点位 绑定多选框
                else if(_text.indexOf("{") !== -1)
                {
                    _text = _text.replace(/[{}]/g,'');
                    $(this).attr({
                        'data-type':'select2',
                        'point' : 'point',
                        'multi' : 'multi'
                    });
                    $(this).editable({
                        title: formula_column + '(' + unit_column + ')',
                        select2: {
                            tags: eval(point_str)
                        }
                    });
                    $(this).css({
                        'background-color' : '#428bca',
                        'border-style': 'none'
                    });
                    ++i;
                }
                //如果 仅是公式的计算符号等 不绑定点击事件
                else
                {
                    $(this).css({'background-color' : '#FFF',
                                'border-style': 'none',
                                'color' : '#000'})
                }
                ++j;
                //将标示符号去掉 以便在提交验证中 能执行 依据点位名称判断是否将全部点位替换为真实点位的方法
                $(this).text(_text);
                //当填写完毕 自动切换到下一个点位进行选择
                $(".tag").on('save.newuser', function(){
                    var i = 0;
                    var that = $(this);
                    var _that = this;
                    setTimeout(function(){
                        $(_that).text($(_that).text().replace(new RegExp(',', 'g'),'+'))
                    },1);
                    if(that.attr('point') == 'point')
                            that.css({'background-color' : '#05467E',
                                      'border-style' : 'none'})
                        do
                        {
                            if(i>20)
                            {
                                break;
                            }
                            that = that.next();
                            i++;
                        }while(typeof(that.attr('point')) == 'undefined')

                    that.editable('show');

                });
            })
            //隐藏删除按钮
            $(".tag").find('span').hide();
        }

        //由于存在bug 需要手动将多余的字符串 删除
        $(".bootstrap-tagsinput").on('click','.tag', function(){
            $('.select2-search-choice').remove()
            $('.editable-input input').val('')
        })
        tagBd();
        //语言切换
        var _ls = 'Please replace all the formulas of point';
        var _lang = typeof(langs[langs['type']]) === 'undefined'  ? _ls : (typeof(langs[langs['type']][_ls]) === 'undefined' ? _ls : langs[langs['type']][_ls]);

        /*给提交按钮 绑定事件 提交之前处理公式*/
        $(".form-actions .btn").click(function()
        {

            var _ids = '';
            var i = 0;
            var _forulm = '';
            //过滤 公式中未替换 或 替换的不是规定单位的点位
            $(".tag").each(function()
            {
                //如果是公式中的点位
                if($(this).attr('point') == 'point')
                {
                    var _value = $(this).text();
                    //判断模板点位 是否 被 替换掉 没有则提示( 依据点位名称判断是否将全部点位替换为真实点位的方法)
                    if(point_str.indexOf(_value) == '-1')
                    {
                        //多选没有规律顺序会跳过前一个判断在此 对多选进行再次过滤
                        if($(this).attr('multi') == 'multi' && _value.indexOf('{') == -1)
                        {
                            var ps_arr = _value.split('+');
                            var ps_points = '';
                            $.each(point_json, function (k, v) {
                                  if($.inArray(v, ps_arr) != -1 || $.inArray(" "+v, ps_arr) != -1)
                                  {
                                      ps_points += 'p' + k  + ',+,';
                                  }
                            })
                            _forulm += ps_points.substring(0,ps_points.length - 2);
                        }
                        else
                        {
                            $('.field-calculatepoint-formula .help-block').css('color', '#b94a48');
                            $('.field-calculatepoint-formula .help-block').text( _lang );
                            i  = 1;
                            return false;
                        }
                    }
                    else
                    {
                        //将提示信息置空
                        $('.field-calculatepoint-formula .help-block').text('');
                        $.each(point_json, function (k, v) {
                            if(v == _value)
                            {
                                _ids += k + ',';
                                _forulm += 'p' + k  + ',';
                            }
                        });
                        i  = 0;
                    }
                }
                else
                {
                    i = 0;
                    _forulm += $(this).text() + ',';
                }
            })
            if(!_forulm)
            {
                $('.field-calculatepoint-formula .help-block').css('color', '#b94a48');
                $('.field-calculatepoint-formula .help-block').text( _lang );
                i += 1;
            }
            if(i)
            {
                return false;
            }
            //赋值给公式框
            elt.val(_forulm);
            //赋值公式值的单位

//            $.each(unit_result, function(k , v) {
//                if(v.id == formula_name)
//                {
//                    //将公式结果单位 赋值给 unit
//                    $("input[name='CalculatePoint[unit]']").val(v.value);
//                    //将公式对应的模板id 赋值给 template_id
//                    $("input[name='CalculatePoint[template_id]']").val(v.t_id);
//                }
//            })
//            //赋值参与计算点位的标准单位
//            $("input[name='CalculatePoint[unit_standard]']").val(unit_standard);
        });

