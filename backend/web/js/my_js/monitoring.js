/**
 * Created by cre on 15-2-4.
 * 文件类型：js监控文件
 * 主要业务：监控 接口的动态，反馈相应的信息
 * 存储方式：本地存储
 * 时间分配机制：
 */


/**
 * 监控配置
 */
var $t_bac,$count = 0;
var $trigger_time = 6 * 1000;
var $interval = 10 * 1000;
//ajax 访问链接                                                         访问方法
var $bac_url = '/bacnet/ajax-automatic-monitoring';                 //bcu 设备数量
var $alarm_url = '/alarm-log/crud/ajax-automatic-monitoring';       //报警日志数量



//当前时间
var $current_time = new Date().format('yyyy-MM-dd hh:mm:ss');


            /************************************
              ***************************  *****
                                      **  *****
             *                       ***  ****
             *                      *****  *********************************
                                   *******  *********************************/

/*********************************************************************        主体代码                 *************/
$(function(){

/************************************************* 监控方法定义 ****************************************************/

/**
 * Bacnet 设备数据获取接口监控
 * session['bacnet_count']
 * 触发机制：将session中存储的bacnet设备数，与数据库中的设备个数进行对比，如果接口返回的设备数大于当前数据库中的设备数那么将会
 * 提示用户有新的bac设备添加到本网段，需要用户对新设备进行配置，如果接口返回的设备数等于数据空中的设备数就
 * 不做任何操作。
 */
function bac_monitoring()
{
    $.post($bac_url, '', function($data){
        console.log($current_time + ' - ' + 'bac');
        if($data != 0){
            $.smallBox({
                title : $current_time + " 系统自动检测提示",
                content : "系统检测到Bacnet设备跟新变动，请前往查看配置信息是否正确",
                color : "#3276B1",
                icon :"fa fa-bell swing animated",
                timeout : 5000,
                sound: false
            });
        }
    })
}

/**
 * 报警自动监测
 * 触发机制： 当前监测到的为确认报警数量大于最后一次监测到的数量时触发
 * session['alarm_log_count']
 */
function alarm_monitoring()
{
    $.post($alarm_url, '', function($data){
        console.log($current_time + ' - ' + 'alarm_log - ' + $data);
        if($data > 0){
            //关联 top 上的 报警 提示
            $('#_load').trigger('click');

            $.smallBox({
                title : $current_time + " 系统报警提示",
                content : "系统检测到有"+$data+"条新的报警，请前往查看报警具体信息",
                color : "#D98C00",
                icon :"fa fa-warning swing animated",
                timeout : 50 * 1000,
                sound: false
            });
        }
    })
}



/**************************************************** 监控定时器定义 ***************************************************/
    $t_bac = setInterval(bac_monitoring, $trigger_time);
    $trigger_time += $interval;                            //增加定时时间间隔 来错开 调用的并发
    $t_alarm = setInterval(alarm_monitoring, $trigger_time);
})
