/**
 * Created by cre on 14-11-5.
 */

function langZh_cn(){
       return  "editable: true,selectable:false,firstDay:1,weekNumbers:true,contentHeight: 300,slotEventOverlap:false,weekMode: 'liquid',axisFormat: 'H:mm',allDayText:'全天',header:{left: '',center: 'title',right: ''},titleFormat: {month: 'yyyy' + '年' + 'MMMM',week: 'MMM d[ yyyy]{'&#8212:'[ MM] d yyyy}',day: 'dddd, MMM d, yyyy'},columnFormat:{month: 'ddd',week: 'ddd',day: '天'},timeFormat:{'': 'h(:mm)t'     },monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],dayNamesShort:['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']";
}
