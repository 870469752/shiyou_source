/**
 * Created by cre on 15-6-5.
 * 搜索的js代码
 * 更具搜索选择的字段类型进行添加适当的Dom表单元素
 * 大致类型有这几种：                       int              string           bool            json            timestamp
 * 对应的表单元素：                   select + input    select + input        radio        select + input    input(时间选择)
 */
var $form_dom  = '';

/**
 *  根据所选择的字段 获取字段名称 和 字段类型
 */
    $('._columns').on('change', function() {
        var $selected = $(this).find('option:selected');
        var $column_name = $selected.val();
        var $column_type = $selected.data('dbtype');
        var $num = $('._columns').index(this);
        //获取 所需的 表单 元素
        $form_dom =  getOptionByType($column_type, $column_name, $num);
        //将表单元素 插入条件中
        var $_condition =  $(this).parent().parent().find('._condition');
        //清空当前行条件下的所有子元素 并添加新条件
        $_condition.empty().append( $form_dom );
        timeTypeChange('hour', $('.form_datetime'));
//        $('.c_select').select2();
    });

/**
 * 根据字段类型判断 所需的表单元素
 * 由于 select 在不同类型下的option不同所以会在此 拼凑好select 的option
 * @param $type
 * @param $name
 */
    function getOptionByType($type, $name, $num)
    {
        var $show_type = 'select';      //条件表单元素的类型 可以为 下拉菜单、单选框等

        var $options = '';               //根据不同的字段类型添加不同的option

        //定义一组option数组 使用的是 SHELL命令里的简写
        var $opt = [];
        $opt['eq'] = "<option value = '='>" + lang('EQ') + '</option>'; //等于
        $opt['nq'] = "<option value = '!='>" + lang('NQ') + '</option>'; //等于
        $opt['gt'] = "<option value = '>'>" + lang('GT') + '</option>'; //大于
        $opt['lt'] = "<option value = '<'>" + lang('LT') + '</option>'; //小于
        $opt['ge'] = "<option value = '>='>" + lang('GE') + '</option>'; //大于等于
        $opt['le'] = "<option value = '<='>" + lang('LE') + '</option>'; //小于等于
        $opt['like'] = "<option value = 'like'>" + lang('LIKE') + '</option>'; //类似
        switch($type) {
            case 'int2':                //整型
                $options = $opt['eq'] + $opt['nq'] + $opt['gt'] + $opt['lt'] + $opt['ge'] + $opt['le'];
            break;
            case 'timestamptz':         //时间
                $show_type = 'timestamp';
                $options = $opt['eq'] + $opt['nq'] + $opt['gt'] + $opt['lt'] + $opt['ge'] + $opt['le'];
            break;
            case 'json':                //json
            case 'string':              //字符串
                $options = $opt['eq'] + $opt['like'];
            break;
            case 'bool':                //bool
                $show_type = 'radio';
            break;
            default:
                return;
        }
        return getHtmlDom($show_type, $options, $name, $num);
    }

/**
 * 根据传递的数据 组合一个表单dom元素插入 条件中
 * @param $show_type
 * @param $options
 * @param $name
 */
    function getHtmlDom($show_type, $options, $name, $num)
    {
        //创建 select 表单元素
        if($show_type == 'select' || $show_type == 'timestamp') {
            $form_dom = "<div class='col-md-5'>" +
                            "<select name = '" + $name + "[" + $num + "][condition]' class = 'c_select form-control' placeholder = '选择条件'>" +
                            $options +
                            "</select>" +
                        "</div>" +
                        "<div class='col-md-7'>" +
                            //如果是timestamp 需要使用时间插件
                            ($show_type == 'select' ? "<input name = '" + $name + "[" + $num + "][value]' type='text' class = 'form-control'/>"
                                                    : "<input name = '" + $name + "[" + $num + "][value]' type='datetime' class='form_datetime form-control'/>") +
                        "</div>";
        }else if($show_type == 'radio') {
            $form_dom = "<div class = 'col-md-6' style = 'margin-top:10px'>" +
                            "<input name = '" + $name + "' type='radio' class = '' value = 1>Y" +
                        "</div>" +
                        "<div class = 'col-md-6' style = 'margin-top:10px'>" +
                            "<input name = '" + $name + "' type='radio' class = '' value = 0>N" +
                        "</div>";
        }
        return $form_dom;
    }

    /**
     * 克隆 事件
     */
    $("#c_add").on('click', function() {

        var $_obj = $('#_content .form-group:eq(0)').clone(true);
        $($_obj).find('.row ._del').css('display', '');
        $('#_content').append($_obj);
    })

    /**
     * 表单数据验证
     */
    $("#c_sub").on('click', function() {
        var $_error = 0;
        $search_data = $('#_cc').serializeArray();
        console.log(typeof($search_data));
        $.each($search_data, function(i, field) {
            if(field.value == '') {
                $('#c_mes').text('请检查条件是否为空！');
                $_error = 1;
            }
        });
        if($_error) return false;
    })

    //手动将option第一的元素选中
    $('._columns').eq(0).find('option').eq(0).attr('selected', 'selected');
    $('._columns').trigger('change');