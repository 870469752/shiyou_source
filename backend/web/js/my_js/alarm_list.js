/**
 * Created by cre on 14-12-3.
 */
var _level_index = '';
var _length = $('tbody ._level').length;
var _level_sequence = $("input[name='AlarmRule[level_sequence]']").val();
if(_level_sequence)
{
    var _sequence_arr = _level_sequence.split(',');
    $.each(_sequence_arr, function(k, v){
        //获取当前元素真实位置对应的tr
        var re_cur = $('._level').eq(k);
        //类似 冒泡 排序 将对应位置一个一个放上去
        $("._level[xl='"+v+"']").insertAfter(re_cur);
    })
}

//添加一个 动画效果
$('.field-alarmrule-level_sequence .control-label').click(function(){
    $('#_alarm_level').toggle();
})
//给 tr添加点击事件
$('._level').on('click', function(k, v){
    $('._level').css('background', '');
    $(this).css('background', '#c79121');
    _level_index = $(this).index();
})

//给 按钮添加 点击事件
$('#_prev').click(function(){
    if(_level_index !== '')
    {
        _cur = $('._level').eq(_level_index);
        if(_level_index == 0)
        {
            var prev = $('._level').last();
            _cur.insertAfter(prev);
            _level_index = _length - 1;
        }
        else
        {
            var prev = _cur.prev();
            _cur.insertBefore(prev);
            _level_index = _level_index - 1;
        }
    }

})
$('#_next').click(function(){
    if(_level_index !== '')
    {
        _cur = $('._level').eq(_level_index);
        if(_level_index == _length - 1)
        {
            var next = $('._level').eq(0);
            _cur.insertBefore(next);
            _level_index = 0;
        }
        else
        {
            var next = _cur.next();
            _cur.insertAfter(next);
            _level_index = _level_index + 1;
        }
    }
})



