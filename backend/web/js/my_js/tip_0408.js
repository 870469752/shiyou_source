/**
 * Created by cre on 14-11-12.
 */
//报警日志id
/*切换提示框中的内容*/
$('.notify').click( function () {
    var notify_index = $(this).index();
    $('.content_notif').css('display', 'none');
    $('.content_notif').eq(notify_index).css('display', 'block');
});

/*点击Loading时刷新提示信息*/
$('#_load').click(function(){
    getAlarmLog(0,0);
});

/*点击查看时日志时刷新*/
$("#activity").click(function(){
    getAlarmLog(0,0);
})

//type:0刷新 1确认
function getAlarmLog(type,ids){
    type = arguments[0] ? arguments[0] : 0;
    ids = arguments[1] ? arguments[1] : 0;
    $.ajax({
        url: '/alarm-log/crud/ajax-sources?type='+type+'&ids='+ids,
        async: false,
        dateType: 'json',
        success: function(data)
        {
            var i = 0;
            var html = ' <ul class="notification-body">';
            $.each(JSON.parse(data), function (k, v) {
                html += '<li data-id ='+ v.id+'>' +
                    '<span class="padding-10 unread">' +
                    '' +
                    '<span style="width: 55%; float: left; font-size: 11px" ><a href="#">' +
                        //以后改为 按照 当前语言
                    eval('(' + v.description + ')').data['zh-cn']+
                    '</a></span>'+
                    '<span style="width: 35%; float: left; font-size: 11px">' +
                    '|' +  v.start_time.substr(2,17) +'|' +
                    '</span>' +
                    '<span style="width: 6%; float: right; font-size: 11px;"><a href="javascript:single_alarm_confirmed('+v.id+')" title="确认报警信息" style="text-align:center;color: rgb(100 , 232, 78)"><span class="fa fa-check _confirm"></span></a></span>'
                '</span>' +
                '</li>';
                i++;
            });
            $('.content_notif :eq(0)').empty();
            $('.content_notif :eq(0)').append(html);
            html += '</ul>';
            $('#time_tip').text(new Date().format('yyyy-MM-dd hh:mm:ss'));
            //更新警报日志数量
            getAlarmLogCount();
            setTimeout(function () {
                $('#_load').button('reset')
            }, 1000);
        }
    });
}

/************************************* 获取所有 在'报警' 中展示的报警日志id ******************************************/
function getAlarmLogId(){
    var $ids = '';
    $('#alarm_log li').each(function(){
        $ids += $(this).data('id') + ',';
    })
    return $ids;
}

/**************************************   确定 按钮点击事件 * start**************************************************/
$("#all_confirm").click(function(){
    var $log_ids = '';
    var $log_ids = getAlarmLogId();
    var $index = $('.notify').filter('.active').index();
    var $url = '';
    //根据选中的内容类型进行操作 ----访问不同的控制器方法
    switch($index)
    {
        case 0:
            if($log_ids != ''){
                getAlarmLog(1,$log_ids);
            }else{
                $url = '#1';
            }
            break;
        case 1:
            $url = '';
            break;
        case 2:
            $url = '';
            break;
    }
    //window.location.href = $url;
});
/**************************************   获得为确认的报警总数 * start**************************************************/
function getAlarmLogCount(){
    $.ajax({
        url: '/alarm-log/crud/alarm-num',
        async: false,
        dateType: 'text',
        success: function(data)
        {
            $('#_a_b').html('');
            $('#_a_b').html(data);
            $('.active span[id="alarm_log_num"]').text(data);//报警部分
        }
    })
}

/**************************************   获得为确认的报警总数 * end**************************************************/
function getAlarmNoNotice(){
    $.ajax({
        url: '/alarm-log/crud/alarm-no-notice',
        async: false,
        dateType: 'json',
        success: function(data)
        {
            if(data){
                var sound_judge = 1;
                $.each(JSON.parse(data), function (k, v) {
                    if(v.timeout){
                        $.bigBox({
                            title: v.title,
                            content: "报文："+v.content+"<br>报警点："+v.pname+"<br>位置："+ v.position,
                            color: v.color,
                            sound: false,
                            timeout: v.timeout,
                            icon: "fa fa-bell swing animated",
                            number: v.sys_name
                        }, function() {
                            //此处可编写关闭警报后的操作，如关闭页面声音
                            $("#bgMusic").attr('src','');
                        });
                    }else{
                        $.bigBox({
                            title: v.title,
                            content: "报文："+v.content+"<br>报警点："+v.pname+"<br>位置："+ v.position,
                            color: v.color,
                            sound: false,
                            icon: "fa fa-bell swing animated",
                            number: v.sys_name
                        }, function() {
                            //此处可编写关闭警报后的操作，如关闭页面声音
                            $("#bgMusic").attr('src','');
                        });
                    }
                    if(v.sound && sound_judge){
                        sound_judge = 0;
                        $("#bgMusic").attr('src',v.sound);
                    }
                })
            }
        }
    });
}

$("#close-sound").click(function(){
    $("#bgMusic").attr('src','');
})

/*确认单条报警信息*/
function single_alarm_confirmed(id){
    getAlarmLog(1,id);
}
//setInterval (function(){getAlarmLog(0,null);getAlarmNoNotice();}, 5000);
setInterval (function(){getAlarmNoNotice();}, 5000);
