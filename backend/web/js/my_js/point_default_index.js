/**
 * 专门为点位选择做的JS
 * @param url 提交到哪个链接
 */
function point_default_index(url){
    // 刷新不丢失
    for (var i = 0, len = localStorage.length; i < len; i++) {
        var input_name = localStorage.key(i);
        var input_value = localStorage.getItem(input_name);
        if (input_name.indexOf('point_id') != -1) {
            $('[name=\"' + input_name + '\"]').attr('checked', true);
        }
        else if (input_name.indexOf('chart') != -1) {
            $('[name=\"' + input_name + '\"]').val(input_value);
        }
    }
    // 设置localStorage
    $('.table_checkbox').change(function (e) {
        if (this.checked == true) {
            // 同时设置点位和图形样式
            localStorage.setItem(this.name, this.value)
            localStorage.setItem('chart[' + this.value + ']', '');
        }
        else {
            // 删除点位和图形样式
            localStorage.removeItem(this.name)
            localStorage.removeItem('chart[' + this.value + ']')
            // 遍历localStorage里还有没有point_id
            for (var i = 0, len = localStorage.length; i < len; i++) {
                if (localStorage.key(i).indexOf('point_id') != -1) {
                    return;
                }
            }
        }
    })
    $('.table_select').change(function (e) {
        localStorage.setItem(this.name, this.value)
    })
}