/**
 * Created by cre on 14-11-12.
 */
//报警日志id
/*切换提示框中的内容*/
//$('.notify').click( function () {
//    var notify_index = $(this).index();
//    $('.content_notif').css('display', 'none');
//    $('.content_notif').eq(notify_index).css('display', 'block');
//});

$("#alarm-tips").click(function () {
    $(".tips-message").css('display','none');
    $("#alarm_log").css('display','block');
});

/*点击Loading时刷新提示信息*/
$('#_load').click(function(){
    //屏蔽报警
    //getAlarmLog(0,0);
});

/*点击查看时日志时刷新*/
$("#activity").click(function(){
    //屏蔽报警
    //getAlarmLog(0,0);
    //模拟点击计划任务
    $("#scheduled-task-tips").trigger("click");
})

$("#scheduled-task-tips").click(function(){
    $(".tips-message").css('display','none');
    $("#scheduled-task-message").css('display','block');
    //获取任务信息
    $.ajax({
        type: "POST",
        url: "/page-loop/crud/ajax-task-data",
            data: {},
            success: function (msg) {
            msg=eval(msg);
                var url="/subsystem_manage/crud/electrinic-index";
            console.log(msg);
            var html='<ul class="notification-body">';
                var task1='';
                var task2='';
            for(var key in msg) {
                var i_info="完成进度";
                var color_class="bg-color-blueLight";
                var loop_a='';
                var show_flag=false;
                var loc_flag=0;
                var name=msg[key]['name'];
                switch (msg[key]['state']){
                    case 1://未开始
                        i_info='<i> 未开始</i>';
                        color_class="bg-color-blueLight";
                        name+=' 开始时间:'+msg[key]['start_time'];
                        break;
                    case 2://可执行但是未开始
                        i_info='<i>可执行</i>';
                        color_class="bg-color-greenLight";
                        loop_a='<a href='+url+'><font color=#D2651D>开始</font></a>';
                        name+=' 结束时间:'+msg[key]['end_time'];
                        show_flag=true;loc_flag=1;
                        break;
                    case 3://可执行并已经开始
                        i_info='<i>已开始</i>';
                        color_class="bg-color-red";
                        loop_a='<a style="" href='+url+'><font color=#D2651D>开始</font></a>';
                        name+=' 结束时间:'+msg[key]['end_time'];
                        show_flag=true;loc_flag=2;
//                        if(msg[key]['complete_percent']=='100%')
//                            show_flag=false;
                        break;
                    case 4://已结束并完成
                        i_info='<i> 已完成</i>';
                        color_class="bg-color-orange";
                        name+=' 结束时间:'+msg[key]['end_time'];
                        show_flag=true;loc_flag=1;
                        break;
                    case 5://已结束但是未完成
                        i_info='<i> 未完成</i>';
                        color_class="bg-color-darken";
                        name+=' 结束时间:'+msg[key]['end_time'];
                        break;
                }
                var temp_html='';
                if(show_flag){
                    temp_html += '<li>' +
                    '<span>' +
                    '<div class="bar-holder no-padding">' +
                    '<div>' +
                    '<i   class="taskInfo fa fa-warning text-warning" ></i> ' +
                    '<strong   >'+name+':</strong> ' +
                    i_info +
                    '<span style="float: right" class="pull-right semi-bold text-muted">'+msg[key]['complete_value']+loop_a+'</span>' +
                    '</div>' +
                    '<div class="progress progress-md progress-striped">'+
                    '<div   class="progress-bar '+color_class+'"  style="width:'+msg[key]['complete_percent']+'"> </div>'+
                    '</div>' +
                    //'<em class="note no-margin">last updated on 12/12/2013</em>'+
                    '</div>' +
                    '</span>' +
                    '</li>';
                }
                if(loc_flag==1)task1+=temp_html;
                if(loc_flag==2)task2+=temp_html;
            }

            html+=task1+task2;
            html+='</ul>';
            $('#scheduled-task-message').empty();
            $('#scheduled-task-message').append(html);
        }
    });

});


//type:0刷新 1确认
function getAlarmLog(type,ids){
    type = arguments[0] ? arguments[0] : 0;
    ids = arguments[1] ? arguments[1] : 0;
    $.ajax({
        url: '/alarm-log/crud/ajax-sources?type='+type+'&ids='+ids,
        async: false,
        dateType: 'json',
        success: function(data)
        {
            var i = 0;
            var html = ' <ul class="notification-body">';
            $.each(JSON.parse(data), function (k, v) {
                html += '<li data-id ='+ v.id+'>' +
                    '<span class="padding-10 unread">' +
                    '' +
                    '<span style="width: 55%; float: left; font-size: 11px" ><a href="#">' +
                        //以后改为 按照 当前语言
                    eval('(' + v.description + ')').data['zh-cn']+
                    '</a></span>'+
                    '<span style="width: 35%; float: left; font-size: 11px">' +
                    '|' +  v.start_time.substr(2,17) +'|' +
                    '</span>' +
                    '<span style="width: 6%; float: right; font-size: 11px;"><a href="javascript:single_alarm_confirmed('+v.id+')" title="确认报警信息" style="text-align:center;color: rgb(100 , 232, 78)"><span class="fa fa-check _confirm"></span></a></span>'
                '</span>' +
                '</li>';
                i++;
            });
            $('.content_notif :eq(0)').empty();
            $('.content_notif :eq(0)').append(html);
            html += '</ul>';
            $('#time_tip').text(new Date().format('yyyy-MM-dd hh:mm:ss'));
            //更新警报日志数量
            getAlarmLogCount();
            setTimeout(function () {
                $('#_load').button('reset')
            }, 1000);
        }
    });
}

/************************************* 获取所有 在'报警' 中展示的报警日志id ******************************************/
function getAlarmLogId(){
    var $ids = '';
    $('#alarm_log li').each(function(){
        $ids += $(this).data('id') + ',';
    })
    return $ids;
}

/**************************************   确定 按钮点击事件 * start**************************************************/
$("#all_confirm").click(function(){
    var $log_ids = '';
    var $log_ids = getAlarmLogId();
    var $index = $('.notify').filter('.active').index();
    var $url = '';
    //根据选中的内容类型进行操作 ----访问不同的控制器方法
    switch($index)
    {
        case 0:
            if($log_ids != ''){
                getAlarmLog(1,$log_ids);
            }else{
                $url = '#1';
            }
            break;
        case 1:
            $url = '';
            break;
        case 2:
            $url = '';
            break;
    }
    //window.location.href = $url;
});
/**************************************   获得为确认的报警总数 * start**************************************************/
function getAlarmLogCount(){
    $.ajax({
        url: '/alarm-log/crud/alarm-num',
        async: true,
        dateType: 'text',
        success: function(data)
        {
            $('#_a_b').html('');
            $('#_a_b').html(data);
            $('.active span[id="alarm_log_num"]').text(data);//报警部分
        }
    })
}

/**************************************   获得为确认的报警总数 * end**************************************************/


function getNoRecord(){
    $.ajax({
        url: '/route/get-no-record',
        async: true,
        dateType: 'json',
        success: function(data)
        {
            return false;
        },
        complete: function (XHR, TS) { XHR = null }
    })
}

function getNoRecord_new(id){
    var num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
    $.ajax({
        url: '/route/get-no-record-new?id='+id,
        async: true,
        dateType: 'json',
        success: function(data)
        {
            if(data){
                if(num>=5){
                    var id = parseInt(($($("#divbigBoxes").find("div")[1]).attr("id")).replace(/[^0-9]/ig,""));
                    var str = 'botClose'+id;
                    $("#"+str).trigger("click");
                }
                var sound_judge = 1;
                $.each(JSON.parse(data), function (k, v) {
                    var content = "<div style='width: 100%'>报警点："+v.p_name+"</div><br>" +
                        "<div style='width: 100%'>时间："+ v.time.substr(0,19)+"</div><p></p>"+
                        "<div style='text-align: center;'>" +
                        "<a  onclick='toSure(this,"+ v.number+")'  class='btn btn-success'> 确认</a>&nbsp;&nbsp;&nbsp;" +
                        "<a href=/alarm-log/default/logs?type=2&alarm_rule_id="+ v.alarm_rule_id+"  class='btn btn-success'> 查看</a>" +
                        "</div>";
                    if(v.timeout){
                        $.bigBox({
                            title: v.title,
                            content: content,
                            color: v.color,
                            sound: false,
                            timeout: v.timeout,
                            icon: "fa fa-bell swing animated",
                            number: v.number
                        }, function() {
                            var temp_num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
                            if(temp_num == 1){
                                $("#bgMusic").attr('src','');
                            }
                        });
                    }else{
                        $.bigBox({
                            title: v.title,
                            content: content,
                            color: v.color,
                            sound: false,
                            icon: "fa fa-bell swing animated",
                            number: v.number
                        }, function() {
                            var temp_num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
                            if(temp_num == 1){
                                $("#bgMusic").attr('src','');
                            }
                        });
                    }
                    if(v.sound && sound_judge){
                        sound_judge = 0;
                        $("#bgMusic").attr('src',v.sound);
                    }
                    return false;
                })
            }else{
                return false;
            }
        },
        complete: function (XHR, TS) { XHR = null }
    })
}

function getAlarmLeveInfo(log_info){
    var num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
    $.ajax({
        url: '/alarm-level/crud/get-level-info?id='+log_info.alarm_level,
        async: true,
        dateType: 'json',
        success: function(data)
        {
            if(num>=5){
                var id = parseInt(($($("#divbigBoxes").find("div")[1]).attr("id")).replace(/[^0-9]/ig,""));
                var str = 'botClose'+id;
                $("#"+str).trigger("click");
            }
            var sound_judge = 1;
            data = JSON.parse(data);
            var content = "<div style='width: 100%'>报警点："+log_info.point_name+"</div><br>" +
                "<div style='width: 100%'>时间："+ log_info.start_time+"</div><p></p>"+
                "<div style='text-align: center;'>" +
                "<a  onclick='toSure(this,"+ log_info.id+")'  class='btn btn-success'> 确认</a>&nbsp;&nbsp;&nbsp;" +
                "<a href=/alarm-log/default/logs?type=2&alarm_rule_id="+ log_info.alarm_rule_id+"  class='btn btn-success'> 查看</a>" +
                "</div>";
            if(data.timeout){
                $.bigBox({
                    title: data.title,
                    content: content,
                    color: data.color,
                    sound: false,
                    timeout: data.timeout,
                    icon: "fa fa-bell swing animated",
                    number: log_info.id
                }, function() {
                    var temp_num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
                    if(temp_num == 1){
                        $("#bgMusic").attr('src','');
                    }
                });
            }else{
                $.bigBox({
                    title: data.title,
                    content: content,
                    color: data.color,
                    sound: false,
                    icon: "fa fa-bell swing animated",
                    number: log_info.id
                }, function() {
                    var temp_num = $('#divbigBoxes').children('div').length;//计算当前报警框个数
                    if(temp_num == 1){
                        $("#bgMusic").attr('src','');
                    }
                });
            }
            if(data.sound && sound_judge){
                sound_judge = 0;
                $("#bgMusic").attr('src',data.sound);
            }
            return false;
        },
        complete: function (XHR, TS) { XHR = null }
    })
}

//alarm_id报警日志ID
function toSure(obj,alarm_id){
    var id = parseInt((obj.parentNode.parentNode.id).replace(/[^0-9]/ig,""));
    var str = 'botClose'+id;
    $.ajax({
        url: '/alarm-log/default/confirm?id='+alarm_id,
        async: true,
        dateType: 'json',
        success: function(data)
        {
            $("#"+str).trigger("click");
            if(window.location.pathname+window.location.search=='/alarm-log/default/logs?type=1'){
                if( $("#alarm_log_"+alarm_id+" td").eq(5).html()!="已恢复"){
                    $("#alarm_log_"+alarm_id).attr("style","color:green;algin:center");
                }
            }
            if(window.location.pathname+window.location.search=='/alarm-log/default/logs?type=3'){
                if( $("#alarm_log_"+alarm_id+" td").eq(5).html()!="已恢复"){
                    $("#alarm_log_"+alarm_id).attr("style","color:green;algin:center");
                }
            }
            return false;
        },
        complete: function (XHR, TS) { XHR = null }
    })
}

$("#close-sound").click(function(){
    $("#bgMusic").attr('src','');
})

/*确认单条报警信息*/
function single_alarm_confirmed(id){
    getAlarmLog(1,id);
}
//setInterval (function(){getAlarmLog(0,null);getAlarmNoNotice();}, 5000);
//setInterval (function(){getNoRecord();}, 9500);
//setInterval (function(){getAlarmNoNotice();}, 11100);
//getNoRecord();
