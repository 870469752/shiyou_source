/**
 * Created by DUCAT on 14-10-20.
 */
$('#nestable').on('click', '.close', function(e) {
    if(confirm("是否删除")==true)
        $(this).parent().parent().remove();
}).on('click', '.add', function(e) {
    var ol = $('<ol>', {"class":"dd-list"});
    var ol_object = $(this).parent().parent().find('ol').first();
    if(ol_object.length <= 0){
        $(this).parent().parent().append(ol);
        ol_object = ol;
    }
    ol_object.append('<li class="dd-item dd3-item" data-id="" data-title="">\
							<div class="dd-handle dd3-handle">Drag</div>\
							<div class="dd3-content">\
								<span class="name">请双击修改名称</span>\
								<em class="close pull-right " style="padding-left:10px;">×</em>\
								<a href="javascript:void(0)" class="pull-right txt-color-blueDark add" rel="tooltip" title="" data-placement="left" data-original-title="添加子类">\
									<i class="fa fa-sitemap fa-fw"></i>\
								</a>\
							</div></li>');
}).on('click', '.name', function(e) {
    var input = $('<input>', {"class":"text-name", "height":"20px", "type":"text", "value":$(this).text()});
    $(this).html(input);
    input.focus();
}).on('blur', '.text-name', function(e) {
    var name_value = $(this).val()?$(this).val():'请双击修改名称';
    $(this).parent().parent().parent().attr('data-title', name_value);
    $(this).parent().html(name_value);
}).on('keydown', '.text-name', function(e) {
    if(e.keyCode==13){
        var name_value = $(this).val()?$(this).val():'请双击修改名称';
        $(this).parent().parent().parent().attr('data-title', name_value);
        $(this).parent().html(name_value);
    }
});
$('#nestable-menu').on('click', function(e) {
    var target = $(e.target), action = target.data('action');
    if (action === 'expand-all') {
        $('.dd').nestable('expandAll');
    }
    else if (action === 'collapse-all') {
        $('.dd').nestable('collapseAll');
    }
    else if (action === 'default') {
        location.reload();
    }
    else if (action === 'save') {
        $.get("test.cgi", $('#nestable').nestable('serialize'),
        function(data){
            alert("Data Loaded: " + data);
        });
    }
});