/**
 * Created by cre on 15-4-1.
 * 此文件 主要是添加 js的 原型链方法
 */

/********************************************  时间格式化原型链  *************************************** start
    使用方法：new Date().format('yyyy-MM-dd hh:mm:ss')
    添加格式化原型 将时间格式化标准时间形式                          ******************************************************/
Date.prototype.format = function(format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
/*************************************************************************************************  时间格式化 end *****/

/*****************************************************  添加一个随机元素 选择器 *****************************************/
//(function($){
//    var random = 0;
//
//    $.expr[':'].random = function(a, i, m, r) {
//        if (i == 0) {
//            random = Math.floor(Math.random() * r.length);
//        }
//        return i == random;
//    };
//
//})(jQuery);