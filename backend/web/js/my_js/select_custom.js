/**
 * Created by cre on 14-11-19.
 */
var points = [];
var point_str = '';
var point_json = '';
var Cal = {'extension1':'Calculation of symbols', '+':'+','-':'-','*':'*','/':'/'};
console.log(Cal)
$.ajax({
    url: '/calculate-point/crud/ajax-point',
    async:false,
    dateType: 'json',
    success: function(data)
    {
        point_json = JSON.parse(data)
        points.push({id: 'extension0',text: 'Point'});
        $.each(point_json, function (k, v) {
            point_str += "'" + v + "',",
                points.push({
                    id: k,
                    text: v
                });
        });
        point_str = '[' + point_str.substring(0,point_str.length - 1) + ']';
    }
});
$.each(Cal, function(k, v) {
    points.push({id :k, text: v});
})

$('.tag').editable({
    inputclass: 'input-large',
    select2: {
        tags: ['html', 'javascript', 'css', 'ajax'],
        tokenSeparators: ["+", " "]
    }
});
var elt = $("#_tags");
elt.tagsinput({
    allowDuplicates: true
})
elt.tagsinput('add', 'df');
//公式编辑里的 所有区块委派 一个鼠标放上事件
$(document).delegate(".tag", "mousemove", function(){
    $(this).attr({
        'data-type':'select2',
        'point' : 'point'

    });
    $(this).editable({
        title: 'Choose Point',
        source: points,
        select2: {
            width: 200
        }
    });

})