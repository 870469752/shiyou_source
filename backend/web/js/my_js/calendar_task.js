$(document).ready(function ()
{
    var date = new Date();
    var i = 1;
    /*添加一个 时间事件*/
    function addEvent(event, end)
    {

        return $('#calendar').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            description:"<span>提示</span>:<li>点击修改该任务</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])
    }

    /*添加一个 时间事件*/
    function addEventByDay(event, end)
    {
        $('#calendar').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            allDay:false,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])

    }

    /*删除一个 时间事件*/
    function removeEvent(calEvent)
    {
        $('#calendar').fullCalendar('removeEvents',calEvent._id);
        $('.qtip-content').parent().remove();
    }

    /*时间操作提示层*/
    function hintEvent(event, cal_arr, revertFunc, start, end)
    {
        bootbox.dialog({
            message: "事件发生冲突或超出范围，请选择操作：",
            title: "冲突操作",
            buttons: {
                success: {
                    label: "处理(合并)事件",
                    className: "btn-success",
                    callback: function() {
                        removeEvent(event);
                        $.each(cal_arr, function (k, v) {
                            removeEvent(v);
                        })
                        //如果含有 小时或分钟 表明该日历view不是month 需要禁用 allDay
                        if(start.getHours() || start.getMinutes()){
                            addEventByDay(start, end);
                        }
                        else{
                            addEvent(start, end);
                        }

                    }

                },
                danger: {
                    label: "返回上次设置",
                    className: "btn-danger",
                    callback: function() {
                        revertFunc();

                    }
                },
                main: {
                    label: "删除事件",
                    className: "btn-primary",
                    callback: function() {
                        removeEvent(event);
                    }
                }
            }
        });
        $('.close').css('display','none');


    }

    /*创建一个弹出层 设置*/
    function crePopup( _jsEvent, _start, view )
    {
        var _height = $("#popover").height();
        var _width = $("#popover").width();
        $("#error").remove()
        $("#num").val('');
        $("#exe").val('');
        if(view.name == 'month')
        {
            $('#_times').text($.fullCalendar.formatDate(_start, "yyyy-MM-dd"));
        }
        else
        {
            $('#_times').text($.fullCalendar.formatDate(_start, "yyyy-MM-dd H:mm"));
        }
        $("#popover").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre").text("创建");
        $("#popover").attr('start_time', _start);
        $("#popover").show();
    }

    /*修改一个弹出层 设置*/
    function updatePopup(_jsEvent, cal, view)
    {
        var v = cal.title;
        $("#num").val(v);
        var _height = $("#popover").height();
        var _width = $("#popover").width();
        if(view.name == 'month')
        {
            $('#_times').text($.fullCalendar.formatDate(cal.start, "yyyy-MM-dd") + ' -- ' + $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd"))
        }
        else
        {
            $('#_times').text($.fullCalendar.formatDate(cal.start, "yyyy-MM-dd H:mm") + ' -- ' + $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd H:mm"))
        }
        $("#popover").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre").text("修改");
        $("#popover").attr('_id',cal. _id);
        $("#popover").attr('start_time', cal.start);
        $("#popover").show();
    }

    /*判断 是否有未添加 值得事件*/
    function VlaueNotNull()
    {
        $('#calendar').fullCalendar('clientEvents', function ( cal )
        {
            if(cal.title == '点击编辑时间事件'){
                $("#popover").hide();
                removeEvent(cal);
            }
        })
    }

    /*日历插件主体*/
    $('#calendar').fullCalendar (
        {
            editable: true,
            selectable:false,
            firstDay:1,
            weekNumbers:true,
            contentHeight: 300,
            slotEventOverlap:false,
            weekMode: 'liquid',
            header:{
                left: '',
                center: 'title',
                right: ''
            },
            columnFormat:{
                //  month: '-',
                week: "ddd",
                day: 'Day',
                default:false
            },

            /*点击时  添加一个时间事件*/
            dayClick: function ( date, allDay, jsEvent, view )
            {
                if(date.getMonth() !== view.start.getMonth())
                {
                    return false;
                }
                else{
                    $('#calendar').fullCalendar('clientEvents', function ( cal )
                    {
                        if(cal.title == '点击编辑时间事件'){
                            removeEvent(cal);
                        }
                        var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-ss");
                        var event_start = $.fullCalendar.formatDate(date, "yyyy-MM-dd-HH-ss");
                        var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-ss");
                        if(cal_start == event_start || cal_start <= event_start && event_start <= cal_end)
                        {
                            i = i + 1;
                            //最后才会 弹出提示框
                            bootbox.alert("时间事件有冲突，请重新添加！", function() {
                                i = 1;
                                $("#popover").hide();
                            });

                            $('.close').css('display','none');
                        }
                    })
                    if(i == 1){
                        i = 1;
                        var view = $('#calendar').fullCalendar('getView');
                        if(date.getHours() || date.getMilliseconds()){
                            addEventByDay(date);
                        }
                        else{
                            addEvent(date);
                        }
                    }
                    crePopup(jsEvent, date, view);
                }
            },

            /* 监听 拖动事件  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc)
            {
                var view = $('#calendar').fullCalendar('getView');
                if(view.name != 'month' && allDay){
                    revertFunc();
                }
                VlaueNotNull();
                $("#popover").hide();
                $('#calendar').fullCalendar('viewRender ', function (view, element){

//                    console.log(view)
                });
//                conflictEvent(event, revertFunc);
            },

            /* 监听 改变时间块儿的大小  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventResize: function(event, dayDelta, minuteDelta, revertFunc)
            {

                VlaueNotNull();
                $("#popover").hide();
//                conflictEvent(event, revertFunc);
            },


            /*友情 提示 事件*/
            eventRender: function ( event, element, view )
            {
                if(view.name == 'month'){
                    element.qtip({
                        content: event.description
                    });
                }
            },
            /*监听鼠标点击事件 ：1、点击对应的事件 将会被删除；2、消除提示框*/
            eventClick: function ( calEvent, jsEvent, view )
            {
                if(calEvent.title == '点击编辑时间事件')
                {
                    crePopup(jsEvent, calEvent.start, view);
                }
                else
                {
                    VlaueNotNull();
                    updatePopup( jsEvent, calEvent, view);
                }
//			removeEvent(calEvent);
            },
            /*渲染*/
            windowResize: function ( view )
            {
                $('#calendar').fullCalendar('render');
                var view = $('#calendar').fullCalendar('getView');
//                console.log(view);
            }
        });
    //时间插件 表单弹出层
    var _div = '<div class = "rows"><div  class="col-md-4 col-sm-6 popover fade top in alert alert-info fade in" id="popover" style="display: none;">' +
        '<div class="arrow"></div>' +
        '<h3 class="popover-title alert alert-danger fade in">任务编辑</h3>' +
        '任务：<input type="text" style = "width: 150px" id="num">' +
        '<div class="arrow"></div><hr />' +
        '&nbsp;&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "del" class = "btn bg-color-purple txt-color-white" style="ime-mode:disabled " >删除</a>' +
        '&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "cre" class = "btn btn-info">创建</a>' +
        '</div></div>';
    $(window).load(function(){
        $(document.body).append(_div);
        $("#del").click(function () {
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
//                    console.log(cal.start + ":" + _start_time)
                    $('#calendar').fullCalendar('removeEvents',cal._id);
                    $('#popover').hide();
                }
            })
        })

        $("#cre").click(function () {
            $("#error").remove();
            var _value = $("#num").val();
            var _desc  = $("#desc").val();
            if(! _value)
            {
                $("#num").after('<br /><span id = "error" style = "color:red;margin-left:40px">任务不能为空</span>');
                return false;
            }
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
                    cal.title = _value;
                    cal.desc = _desc;
                    cal.editable = true;
                    $('#calendar').fullCalendar( 'rerenderEvents' );
                    $('#popover').hide();
                }
            })

            /*收集数据*/
            /*var _datas = '';
            var data = $('#calendar').fullCalendar('clientEvents');
            $.each(data, function (key, value) {
                $.each(value, function (k, v) {
                    if (k == 'start' || k == 'end') {
                        _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm") + ',';
                        if(k == 'end'){
                            _datas += value['title'];
                            _datas += value['desc'];
                        }
                    }

                })

                _datas += ';';
            })
            console.log(_datas);

            $("input[name='detail']").val(_datas);*/
        })

        /*点击表单提交时收集日历上所有的事件 并赋给hidden input框*/
        $('form').submit(function () {
            var _datas = '';
            var data = $('#calendar').fullCalendar('clientEvents');
            $.each(data, function (key, value) {
                $.each(value, function (k, v) {
                    if(k == 'title'){
                        _datas += value['title'] + ',';
                    }else if (k == 'start') {
                        _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm") + ',';
                    }else if(k == 'end'){
                        _datas += $.fullCalendar.formatDate(v, "yyyy-MM-dd H:mm");
                    }
                })
                _datas += ';';
            })
            $("input[name='detail']").val(_datas);
        })

    });
})