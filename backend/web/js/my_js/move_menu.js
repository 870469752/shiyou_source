    $(document).ready(function() {
        $('#nestable').nestable();
        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target), action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }

        });
    })