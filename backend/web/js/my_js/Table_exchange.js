/**
 * Created by cre on 15-1-12.
 * 该js 功能是对表格记录进行 拖动排序，也可以在表格之间 进行交换
 */
$(document).ready(function ()
{
    /**
     *  这里 需要用到 Sortable 这个插件
     */
    function c_sortable(){
        $(function() {
            $( "#sortable1, #sortable2" ).sortable({
                connectWith: ".connectedSortable",
                opacity: 0.7,
//            revert: true,
                start:function( event, ui ) {
                    //sortable 自生会加以个tr需要减一
                    var $length = ui.item.parent().find('tr').length - 1;
                    if($length == 1)
                    {
                        ui.item.parent().append("<tr id = 'Additional' ><td></td></tr>");
                    }
                },
                //停止拖动
                stop:function( event, ui ) {
                    //事件处理函数
                    ui.item.parent().find('#Additional').remove();
                }
            }).disableSelection();
        });

    }



});