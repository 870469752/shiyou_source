$(document).ready(function ()
{
    var date = new Date();
    var i = 1;
    /*添加一个 时间事件*/
    function addEvent(event, end)
    {

        return $('#calendar1').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])
    }

    /*添加一个 时间事件*/
    function addEventByDay(event, end)
    {
        $('#calendar1').fullCalendar( 'addEventSource', [{
            title: '点击编辑时间事件',
            start: event,
            end: end,
            allDay:false,
            description:"<span>提示</span>:<li>点击删除该时间事件</li><li>拖动右边框更改时间</li>",
            className: ["event", "bg-color-darken"]
        }])

    }

    /*删除一个 时间事件*/
    function removeEvent(calEvent)
    {
        $('#calendar1').fullCalendar('removeEvents',calEvent._id);
        $('.qtip-content').parent().remove();
    }

    /*时间操作提示层*/
    function hintEvent(event, cal_arr, revertFunc, start, end)
    {
        bootbox.dialog({
            message: "事件发生冲突或超出范围，请选择操作：",
            title: "冲突操作",
            buttons: {
                success: {
                    label: "合并事件",
                    className: "btn-success",
                    callback: function() {
                        removeEvent(event);
                        $.each(cal_arr, function (k, v) {
                            removeEvent(v);
                        })
                        //如果含有 小时或分钟 表明该日历view不是month 需要禁用 allDay
                        if(start.getHours() || start.getMinutes()){
                            addEventByDay(start, end);
                        }
                        else{
                            addEvent(start, end);
                        }

                    }

                },
                danger: {
                    label: "返回上次设置",
                    className: "btn-danger",
                    callback: function() {
                        revertFunc();

                    }
                },
                main: {
                    label: "删除事件",
                    className: "btn-primary",
                    callback: function() {
                        removeEvent(event);
                    }
                }
            }
        });
        $('.close').css('display','none');


    }

    /*判断时间是否有冲突 -- 判断较多..*/
    function conflictEvent(event, revertFunc)
    {
        var arr_obj = [];
        var cl_start = event.start;
        var cl_end = event.end;
        var _date = $("#calendar1").fullCalendar('getDate');
        var cur_month = _date.getMonth();
        var cur_year = _date.getFullYear();
        var cur_month_day = new Date(cur_year, cur_month+1, 0).getDate();
        if(event.end == null){
            if(event.start.getMonth() != cur_month){
//                revertFunc();
                cl_start = new Date(cur_year, cur_month, cur_month_day);
                // hintEvent(event, event,  revertFunc, _start, event.end);

            }
        }
        else if(event.end.getMonth() != cur_month){
            cl_start =  new Date(cur_year, cur_month, event.start.getDate()- event.end.getDate());
            if(event.start.getMonth() != cur_month)
            {
                cl_start = new Date(cur_year, cur_month, cur_month_day- (event.end.getDate()- event.start.getDate()))
            }
//            revertFunc();
//			event.start = _start;
            cl_end = new Date(cur_year, cur_month, cur_month_day);
            // hintEvent(event, event,  revertFunc, _start, _end);
        }
        $('#calendar1').fullCalendar('clientEvents', function ( cal )
        {
            if(event._id != cal._id)
            {
                var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-mm");
                var event_start = $.fullCalendar.formatDate(cl_start, "yyyy-MM-dd-HH-mm");
                var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-mm");
                var event_end =  $.fullCalendar.formatDate(cl_end, "yyyy-MM-dd-HH-mm");
                var view = $('#calendar1').fullCalendar('getView');
                console.log('event_start:'+event_start+',cal_start:'+cal_start);
                console.log('event_end'+event_end+',cal_end'+cal_end);
                //改为半开区间event_start < cal_end 等。
                if(view.name == 'month' && (cal_start <= event_start &&  event_start <= cal_end ) ||
                    view.name != 'month' && (cal_start <= event_start &&  event_start < cal_end ))
                {
                    console.log(1);
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start){}
                    cl_start = cal.start;
                    if(event_end <= cal_end){
                        if(cl_end == '' || cal.end >= cl_end)
                            cl_end = cal.end;
//							hintEvent(event, cal,  revertFunc, cal.start, cal.end);
                    }
                    else
                    {
                        cl_end = cl_end;
//							hintEvent(event, cal,  revertFunc, cal.start, event.end);
                    }
                }
                else if(cal_start < event_end && event_end <= cal_end)
                {
                    console.log(2);
                    arr_obj.push(cal);
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
                    if(event_start <= cal_start)
                    {
                        cl_start = cl_start
//							hintEvent(event, cal, revertFunc, event.start, cal.end);
                    }
                    else
                    {
                        if(cl_start == '' || cal.start <= cl_start)
                            cl_start = cal.start
//							hintEvent(event, cal, revertFunc, cal.start, cal.end);
                    }

                }
                else if(view.name == 'month' && event_start <= cal_start && event_end >= cal_start ||
                    view.name != 'month' && event_start <= cal_start && event_end > cal_start)
                {
                    console.log(3);
                    arr_obj.push(cal);
                    cl_start = cl_start;
                    cl_end = cl_end;
//						hintEvent(event, cal, revertFunc, event.start, event.end);
                }
                else if(event_end == '' &&
                    ((cal_start <= event_start && event_start < cal_end) ||
                    (cal_start == event_start)))
                {
                    console.log(4);
                    arr_obj.push(cal);
                    if(cl_start == '' || cal.start <= cl_start)
                        cl_start = cal.start;
                    if(cl_end == '' || cal.end >= cl_end)
                        cl_end = cal.end;
//						hintEvent(event, cal, revertFunc, cal.start, cal.end);
                }
            }
            $('.qtip-content').parent().remove();
        });
        console.log(cl_start +'--'+cl_end)
        if(cl_start != event.start || arr_obj.length){
            hintEvent(event, arr_obj, revertFunc, cl_start, cl_end);
        }
    }

    /*创建一个弹出层 设置*/
    function crePopup( _jsEvent, _start, view )
    {
        var _height = $("#popover1").height();
        var _width = $("#popover1").width();
        var _time_type = view.name;
        var day=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
        $("#error1").remove()
        $("#num1").val('');
        console.log(view.name)
        if(_time_type == 'month')
        {
            var gotoDate_day = $.fullCalendar.formatDate(_start, "yyyy-MM");
            //在此 对比的日期是 利用 gotoDate 设置的日期 说明是以月为时间类型
            if(gotoDate_day == '2008-09')
            {
                $('#_times1').text($.fullCalendar.formatDate(_start, "dd") + '日');
            }
            else
            {
                $('#_times1').text($.fullCalendar.formatDate(_start, "MM-dd"));
            }

        }
        else if(_time_type == 'agendaWeek')
        {
            $('#_times1').text(day[_start.getDay()]);
        }
        else if(_time_type == 'agendaDay')
        {
            $('#_times1').text($.fullCalendar.formatDate(_start, "H:mm"));
        }
        $("#popover1").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre1").text("创建");
        $("#popover1").attr('start_time', _start);
        $("#popover1").show();
    }

    /*修改一个弹出层 设置*/
    function updatePopup(_jsEvent, cal, view)
    {
        var v = cal.title.replace(/[A-Za-z ]|[^u4E00-u9FA5|^\.]/g,'');
        $("#num1").val(v);
        var _height = $("#popover1").height();
        var _width = $("#popover1").width();
        var _time_type = view.name;
        var day=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
        console.log(view.name)
        if(_time_type == 'month')
        {
            var gotoDate_day = $.fullCalendar.formatDate(cal.start, "yyyy-MM");
            if(gotoDate_day == '2008-09')
            {
                $('#_times1').text($.fullCalendar.formatDate(cal.start, "dd") + '日 -- ' + $.fullCalendar.formatDate(cal.end, "dd") + '日')
            }
            else
            {
                $('#_times1').text($.fullCalendar.formatDate(cal.start, "MM-dd") + ' -- ' + $.fullCalendar.formatDate(cal.end, "MM-dd"))
            }
        }
        else if(_time_type == 'agendaWeek')
        {
            $('#_times1').text(day[cal.start.getDay()]);
        }
        else if(_time_type == 'agendaDay')
        {
            $('#_times1').text($.fullCalendar.formatDate(cal.start, "H:mm") + ' -- ' + $.fullCalendar.formatDate(cal.end, "H:mm"))
        }
        $("#popover1").css({
            'top' : _jsEvent.pageY - _height - 20,
            'left' : _jsEvent.pageX - _width/2
        })
        $("#cre1").text("修改");
        $("#popover1").attr('_id',cal. _id);
        $("#popover1").attr('start_time', cal.start);
        $("#popover1").show();
    }

    /*判断 是否有未添加 值得事件*/
    function VlaueNotNull()
    {
        $('#calendar1').fullCalendar('clientEvents', function ( cal )
        {
            if(cal.title == '点击编辑时间事件'){
                $("#popover1").hide();
                removeEvent(cal);
            }
        })
    }

    /*日历插件主体*/
    $('#calendar1').fullCalendar (
        {
            editable: false,
            selectable:false,
            firstDay:1,
            weekNumbers:true,
            contentHeight: 300,
            slotEventOverlap:false,
            weekMode: 'liquid',
            header:{
                left: '',
                center: '',
                right: ''
            },
            columnFormat:{
                //  month: '-',
                week: "ddd",
                day: 'Day',
                default:false
            },
            /*点击时  添加一个时间事件*/
            dayClick: function ( date, allDay, jsEvent, view )
            {
                if(date.getMonth() !== view.start.getMonth())
                {
                    return false;
                }
                else{
                    $('#calendar1').fullCalendar('clientEvents', function ( cal )
                    {
                        if(cal.title == '点击编辑时间事件'){
                            removeEvent(cal);
                        }
                        var cal_start = $.fullCalendar.formatDate(cal.start, "yyyy-MM-dd-HH-ss");
                        var event_start = $.fullCalendar.formatDate(date, "yyyy-MM-dd-HH-ss");
                        var cal_end = $.fullCalendar.formatDate(cal.end, "yyyy-MM-dd-HH-ss");
                        if(cal_start == event_start || cal_start <= event_start && event_start <= cal_end)
                        {
                            i = i + 1;
                            //最后才会 弹出提示框
                            bootbox.alert("时间事件有冲突，请重新添加！", function() {
                                i = 1;
                                $("#popover1").hide();
                            });
                            $('.close').css('display','none');
                        }
                    })
                    if(i == 1){
                        i = 1;
                        var view = $('#calendar1').fullCalendar('getView');
                        if(date.getHours() || date.getMilliseconds()){
                            addEventByDay(date);
                        }
                        else{
                            addEvent(date);
                        }
                    }
                    crePopup(jsEvent, date, view);
                }
            },

            /* 监听 拖动事件  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc)
            {
                var view = $('#calendar1').fullCalendar('getView');
                if(view.name != 'month' && allDay){
                    revertFunc();
                }
                VlaueNotNull();
                $("#popover1").hide();
                $('#calendar1').fullCalendar('viewRender ', function (view, element){
                    //console.log(view)
                });
                conflictEvent(event, revertFunc);
            },

            /* 监听 改变时间块儿的大小  判断当前时间事件 是否和其他时间事件 有冲突 */
            eventResize: function(event, dayDelta, minuteDelta, revertFunc)
            {
                VlaueNotNull();
                $("#popover1").hide();
                conflictEvent(event, revertFunc);
            },


            /*友情 提示 事件*/
            eventRender: function ( event, element, view )
            {
                if(view.name == 'month'){
                    element.qtip({
                        content: event.description
                    });
                }
            },
            /*监听鼠标点击事件 ：1、点击对应的事件 将会被删除；2、消除提示框*/
            eventClick: function ( calEvent, jsEvent, view )
            {
                if(calEvent.title == '点击编辑时间事件')
                {
                    crePopup(jsEvent, calEvent.start, view);
                }
                else
                {
                    VlaueNotNull();
                    updatePopup( jsEvent, calEvent, view);
                }
//			removeEvent(calEvent);
            },
            /*渲染*/
            windowResize: function ( view )
            {
                $('#calendar1').fullCalendar('render');
                var view = $('#calendar1').fullCalendar('getView');
                console.log(view);
            }
        });
    //时间插件 表单弹出层
    var _div = '<div class = "rows"><div  class="col-md-4 col-sm-6 popover fade top in alert alert-info fade in" id="popover1" style="display: none;">' +
        '<div class="arrow"></div>' +
        '<h3 class="popover-title alert alert-danger fade in">时间段编辑</h3>' +
        '时间：<span id = "_times1"></span><div></div>' +
        '名字：<input type="text" style = "width: 100px" id="num1">' +
        '<div class="arrow"></div><hr />' +
        '&nbsp;&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "del1" class = "btn bg-color-purple txt-color-white" style="ime-mode:disabled " >删除</a>' +
        '&nbsp;&nbsp;&nbsp;<a href = "javascript:void(0)" id = "cre1" class = "btn btn-info">创建</a>' +
        '</div></div>';
    $(window).load(function(){
        $(document.body).append(_div);
        $("#del1").click(function () {
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar1').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
                    //console.log(cal.start + ":" + _start_time)
                    $('#calendar1').fullCalendar('removeEvents',cal._id);
                    $('#popover1').hide();
                }
            })
        })
        /*创建事件*/
        $("#cre1").click(function () {
            $("#error1").remove();
            var _value = $("#num1").val();
            if(! _value)
            {
                $("#num1").after('<br /><span id = "error1" style = "color:red;margin-left:40px">内容不能为空</span>');
                return false;
            }
            //console.log('value :' + _value);
            var _start_time = $(this).parent().attr('start_time');
            $('#calendar1').fullCalendar('clientEvents', function ( cal ){
                if(cal.start == _start_time){
                    //cal.title = 'value is ' + _value;
                    cal.title = _value;
                    cal.editable = true;
                    $('#calendar1').fullCalendar( 'rerenderEvents' );
                    $('#popover1').hide();
                }
            })
        })
    });
})