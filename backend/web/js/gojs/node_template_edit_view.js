
function init_node_template_edit(){
    var $ = go.GraphObject.make;
    var cellSize = new go.Size(5, 5);

    var Adornment=$(go.Adornment, "Auto",
            $(go.Shape, "RoundedRectangle",
                { fill: null, stroke: "transparent", strokeWidth: 8 }),
            $(go.Placeholder)
        );  // end Adornment
    var mainTemplate=$(go.Node,  "Auto",
        { locationSpot: go.Spot.Center },
        new go.Binding("layerName", "layername"),
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {
            resizable: true,
            rotatable: true,
            resizeObjectName: "pic"
        },
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Picture,
            {
                //desiredSize: new go.Size(75, 75)
                //maxSize: new go.Size(75, 75)
            },
            new go.Binding("source", "img")
        ),
        //new go.Binding("desiredSize","size"),
        { // if double-clicked, an input node will change its value, represented by the color.
            doubleClick: function (e, obj) { //双击事件
                //console.log('删除');
                //console.log('init');
//                var diagram = myDiagram2;
//                        diagram.commandHandler.deleteSelection();
//                        diagram.currentTool.stopTool();
            }
        },
        {
            selectionAdornmentTemplate:Adornment
        }
    );

    var ManyTemplate=$(go.Node,  "Auto",
        { locationSpot: go.Spot.Center },
        new go.Binding("layerName", "layername"),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Picture,
            {
                //desiredSize: new go.Size(75, 75)
                //maxSize: new go.Size(75, 75)
            },
            new go.Binding("source", "img")
        ),
        //new go.Binding("desiredSize","size"),

        {
            selectionAdornmentTemplate:Adornment
        }
    );
    var stateTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Picture,
            {
                //desiredSize: new go.Size(75, 75)
                //maxSize: new go.Size(75, 75)
            },
            new go.Binding("source", "img"),
            new go.Binding("text")
        ),
        {
            selectionAdornmentTemplate:Adornment
        }
    );
    var StateControlTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true,  rotatable: true,resizeObjectName: "pic"},
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Picture,
            {
                //desiredSize: new go.Size(75, 75)
                //maxSize: new go.Size(75, 75)
            },
            new go.Binding("source", "img"),
            new go.Binding("text")
        )
    );

    var editTemplate=
        $(go.Node,"Spot",
            //绑定位置 loc信息
             { locationSpot: go.Spot.Left },//按中心定位
            //{ locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Panel, "Horizontal",//横向排列
                //contextMenu右键菜单属性
                { contextMenu: $(go.Adornment)},
                //绑定位置 输入框text信息
                $(go.TextBlock, "编辑",{
                        font: "normal 14px Arial",
                        //font: "bold 14px sans-serif",
                        stroke: 'white',
                        margin: 0,
                        isMultiline: true,
                        editable: true
                    }
                    ,
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("font","font"),
                    new go.Binding("stroke","color")
                )
            ),

            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件
//                    var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                }
            },
            {
                selectionAdornmentTemplate:Adornment
            }
        );


    var main_editTemplate= $(go.Part, "Table",
        {name:"table"},
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "table"},
        $(go.TextBlock,"name",
            {
                font: "bold 14px Arial",
                //font: "bold 14px sans-serif",
                stroke: 'white',
                margin: 0,
                isMultiline: true,
                editable: true,
                row: 0,
                column: 0,
                alignment: go.Spot.Left,
                margin: new go.Margin(0,  10, 0,  0)
            },
            //暂时无背景颜色
            //new go.Binding("background", "background"),
            new go.Binding("text", "name").makeTwoWay()
            //,
            //new go.Binding("margin", "margin")
        ),
        $(go.Panel, "Auto",//横向排列
            {
                row: 0,
                column: 1,
                alignment: go.Spot.Right
            },
            //$(go.Picture,'/uploads/pic/按钮6.png',
            //
            //    {
            //        name: "PIC",
            //        desiredSize: new go.Size(100, 25),
            //        //width: 60,
            //        //height: 80,
            //        //margin: 2,
            //        //background: "chartreuse",
            //        //imageStretch: go.GraphObject.Fill
            //        maxSize:new go.Size(100, 25)
            //    },
            //    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            //    new go.Binding("source", "img")
            //),
            $(go.TextBlock,"value",
                {
                    //font: "normal normal 400 10px /1.2 Georgia",
                    font: "bold 14px Arial",
                    stroke: '#D2651D',
                    margin: 0,
                    //margin: new go.Margin(0,  15, 0,  0),

                    //isMultiline: true,
                    editable: false
                },
                //暂时无背景颜色
                //new go.Binding("background", "background"),
                new go.Binding("text", "value"))
        ),
        {
            selectionAdornmentTemplate:Adornment
        }
    );
    var only_valueTemplate= $(go.Node,"Spot",
        //绑定位置 loc信息
         { locationSpot: go.Spot.Right },//靠右定位
        //{ locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Horizontal",//横向排列
            //contextMenu右键菜单属性
            { contextMenu: $(go.Adornment)},
            //绑定位置 输入框text信息
            $(go.TextBlock, "only_value",{
                    font: "normal 14px Arial",
                    //font: "bold 14px sans-serif",
                    stroke: '#D2651D',
                    margin: 0,
                    isMultiline: true,
                    editable: true
                }
                ,
                //new go.Binding("background", "background"),
                new go.Binding("text", "text").makeTwoWay(),
                new go.Binding("font","font"),
                new go.Binding("stroke","color")
                //,
                //new go.Binding("stroke","color")
            )
        ),
        {
            selectionAdornmentTemplate:Adornment
        }
    );

    var rule_valueTemplate= $(go.Node,"Spot",
        //绑定位置 loc信息
        { locationSpot: go.Spot.Right },//靠右定位
        //{ locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Horizontal",//横向排列
            //contextMenu右键菜单属性
            { contextMenu: $(go.Adornment)},
            //绑定位置 输入框text信息
            $(go.TextBlock, "rule_value",{
                    font: "normal 14px Arial",
                    //font: "bold 14px sans-serif",
                    stroke: '#D2651D',
                    margin: 0,
                    isMultiline: true,
                    editable: false
                }
                ,
                //new go.Binding("background", "background"),
                new go.Binding("text", "text").makeTwoWay(),
                new go.Binding("config", "config"),
                new go.Binding("font","font")
                //,
                //new go.Binding("stroke","color")
            )
        )
    );

    var Value_1Template= $(go.Node,"Spot",
        //绑定位置 loc信息
        { locationSpot: go.Spot.Right },//靠右定位
        //{ locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Horizontal",//横向排列
            //contextMenu右键菜单属性
            { contextMenu: $(go.Adornment)},
            //绑定位置 输入框text信息
            $(go.TextBlock, "only_value",{
                    font: "normal 14px Arial",
                    //font: "bold 14px sans-serif",
                    stroke: '#D2651D',
                    margin: 0,
                    isMultiline: true,
                    editable: true
                }
                ,
                //new go.Binding("background", "background"),
                new go.Binding("text", "text").makeTwoWay(),
                new go.Binding("font","font"),
                new go.Binding("config","config")
                //,
                //new go.Binding("stroke","color")
            )
        )
    );

    var valueTemplate=
        $(go.Node,"Spot",
            //绑定位置 loc信息
            { locationSpot: go.Spot.Center },//靠右定位
            //{ locationSpot: new go.Spot(0, 0, cellSize.width / 2, cellSize.height / 2) },//按cellsize定位
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Panel, "Horizontal",//横向排列
                //contextMenu右键菜单属性
                { contextMenu: $(go.Adornment)},
                //绑定位置 输入框text信息
                $(go.TextBlock, "only_value",{
                        font: "normal 14px Arial",
                        //font: "bold 14px sans-serif",
                        stroke: '#D2651D',
                        margin: 0,
                        isMultiline: true,
                        editable: false
                    }
                    ,
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("font","font"),
                    new go.Binding("stroke","color")

                )
            ),
            {
                selectionAdornmentTemplate:Adornment
            }
        );
    var StateValueTemplate=
        $(go.Node,"Spot",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Panel, "Auto",//横向排列
                $(go.TextBlock,"state_value",
                    {
                        //font: "normal normal 200 10px /1.2 微软雅黑",
                        font: "normal 14px Arial",
                        stroke: '#8FBC8F',
                        margin: 0,
                        isMultiline: true,
                        editable: false
                    },
                    //暂时无背景颜色
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("font","font"),
                    new go.Binding("value","value")
                )
            ),
            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件
                    //console.log('改变');
//                    var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                }
            },
            {
                selectionAdornmentTemplate:Adornment
            }
        );
    var stateEventTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        //new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Panel, "Vertical",//竖着排列   Vertical
            $(go.Picture,
                {
                    //desiredSize: new go.Size(75, 75)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("source", "img"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("text", "text")
            ),
            $(go.TextBlock,"报警/正常",
                {

//                                background: "lightgreen",
//                        font: "normal normal 200 10px /1.2 微软雅黑",
                    font: "bold 14px Arial",
                    stroke: 'red',
                    margin: 0,
                    isMultiline: true,
                    editable: false
                },
                //暂时无背景颜色
                //new go.Binding("background", "background"),
                new go.Binding("text", "name")
            )

        )

    );

    var stateDefendEventTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {
            resizable: true
            ,
            resizeObjectName: "pic"
        },
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Panel, "Vertical",//竖着排列   Vertical
            $(go.Picture,
                {
                    //desiredSize: new go.Size(75, 75)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("source", "img"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("text", "text")
            )
        )
    );
    //防区
    var stateDefend_1Template=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {
            resizable: true
            ,
            resizeObjectName: "pic"
        },
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Panel, "Vertical",//竖着排列   Vertical
            $(go.Picture,
                {
                    //desiredSize: new go.Size(75, 75)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("source", "img"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("text", "text")
            )
        )
    );
    var stateDefendControlTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {
            resizable: true
            ,
            resizeObjectName: "pic"
        },
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Panel, "Vertical",//竖着排列   Vertical
            $(go.Picture,
                {
                    //desiredSize: new go.Size(75, 75)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("source", "img"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("text", "text")
            )
        )
    );
    var OnOffValueTemplate=
        $(go.Node,"Spot",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Panel, "Auto",//横向排列
                $(go.TextBlock,"开/关",
                    {
                        //font: "normal normal 200 10px /1.2 微软雅黑",
                        font: "normal 14px Arial",
                        stroke: '#8FBC8F',
                        margin: 0,
                        isMultiline: true,
                        editable: false
                    },
                    //暂时无背景颜色
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("font","font")
                )
            ),
            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件
                    //console.log('改变');
//                    var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                }
            },
            {
                selectionAdornmentTemplate:Adornment
            }
        );
    var AlarmValueTemplate=
        $(go.Node,"Spot",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Panel, "Auto",//横向排列

                $(go.TextBlock,"报警/正常",
                    {
                        //font: "normal normal 200 10px /1.2 微软雅黑",
                        font: "normal 14px Arial",
                        stroke: '#8FBC8F',
                        margin: 0,
                        isMultiline: true,
                        editable: false
                    },
                    //暂时无背景颜色
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("font","font")
                )
            ),
            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件
                    //console.log('改变');
//                    var diagram = myDiagram2;
//                            diagram.commandHandler.deleteSelection();
//                            diagram.currentTool.stopTool();
                }
            },
            {
                selectionAdornmentTemplate:Adornment
            }
        );

    var ButtonTemplate=
        $(go.Node,"Auto",
            { locationSpot: go.Spot.Center },
            {resizable: true, resizeObjectName: "PIC"},
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),

            $(go.Panel, "Horizontal",//横向排列
                //contextMenu右键菜单属性
                {
                    contextMenu: $(go.Adornment)
                },

                $(go.Picture,
                    {
                        name: "PIC"
//                            desiredSize: new go.Size(75, 75)
                        //maxSize: new go.Size(75, 75)
                    },
                    new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                    new go.Binding("source", "img"))

            ),
            $(go.Panel, "Horizontal",//横向排列
                //contextMenu右键菜单属性
                {
                    contextMenu: $(go.Adornment)
                },

                $(go.TextBlock,"报警/正常",
                    {

//                                background: "lightgreen",
//                        font: "normal normal 200 10px /1.2 微软雅黑",
                        font: "bold 14px Arial",
                        stroke: 'white',
                        margin: 0,
                        isMultiline: true,
                        editable: false
                    },
                    //暂时无背景颜色
                    //new go.Binding("background", "background"),
                    new go.Binding("text", "text"))

            ),
            { // if double-clicked, an input node will change its value, represented by the color.
                doubleClick: function (e, obj) { //双击事件



                }
            },
            {
                selectionAdornmentTemplate:Adornment
            }
        );

    var TestTemplate=
        $(go.Node,"Auto",
            { locationSpot: go.Spot.Center },
//                    {resizable: true, resizeObjectName: "PIC"},
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Picture,
                {
                    name: "PIC",
                    desiredSize: new go.Size(100, 25)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("source", "img")),


            $(go.TextBlock,"报警/正常",
                {

//                                background: "lightgreen",
//                    font: "normal normal 200 15px /1.2 微软雅黑",
                    font: "normal 14px Arial",
                    stroke: '#333',
                    margin: 10,
                    isMultiline: true,
                    editable: false
                },
                //暂时无背景颜色
                //new go.Binding("background", "background"),
                new go.Binding("text", "text")),
            {
                selectionAdornmentTemplate:Adornment
            }

        );

    var ControlTemplate=
        $(go.Node,"Auto",
            { locationSpot: go.Spot.Center },
            {resizable: true, resizeObjectName: "PIC"},
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            $(go.Picture,
                {
                    name: "PIC",
                    desiredSize: new go.Size(100, 25)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("source", "img")),


            $(go.TextBlock,"报警/正常",
                {

//                                background: "lightgreen",
//                    font: "normal normal 200 15px /1.2 微软雅黑",
                    font: "normal 14px Arial",
                    stroke: '#333',
                    margin: 10,
                    isMultiline: true,
                    editable: false
                },
                //暂时无背景颜色
                //new go.Binding("background", "background"),
                new go.Binding("stroke","color"),
                new go.Binding("text", "text")),
            {
                selectionAdornmentTemplate:Adornment
            }

        );
    var GifTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        $(go.Picture,
            {
            },
            new go.Binding("source", "img"),
            new go.Binding("text")
        ),
        { // handle mouse enter/leave events to show/hide the ports
            //mouseEnter: function(e, node) { console.log('enter'); },
            //mouseLeave: function(e, node) { console.log('leave'); }
        },
        {
            selectionAdornmentTemplate:Adornment
        }
    );
    var Gif_ValueTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        $(go.Picture,
            {
            },
            new go.Binding("source", "img"),
            new go.Binding("text")
        ),
        { // handle mouse enter/leave events to show/hide the ports
            //mouseEnter: function(e, node) { console.log('enter'); },
            //mouseLeave: function(e, node) { console.log('leave'); }
        }
    );
    InitScrollingTable();

    var  FigureTemplate=$(go.Node, "Vertical",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        {
            locationSpot: go.Spot.Center,  // the location is the center of the Shape
            locationObjectName: "SHAPE",
            selectionAdorned: false,  // no selection handle when selected
            resizable: true, resizeObjectName: "SHAPE",  // user can resize the Shape
            rotatable: true, rotateObjectName: "SHAPE",  // rotate the Shape without rotating the label
            // don't re-layout when node changes size
            layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized
        },

        $(go.Shape,
            {
                name: "SHAPE",  // named so that the above properties can refer to this GraphObject
                width: 50, height: 50,
                fill: "lightgray",
                stroke: "lightslategray",
                strokeWidth: 2

            },
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            // bind the Shape.figure to the figure name, which automatically gives the Shape a Geometry
            new go.Binding("figure", "figure"))
    );

    var TableTemplate= $(go.Node, "Vertical",
        { locationSpot: go.Spot.Center },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        {
            selectionObjectName: "SCROLLING",
            resizable: true, resizeObjectName: "SCROLLING"
        },
        $(go.TextBlock,
            {
                //font: "bold 14px sans-serif"
                font: "bold 14px Arial"
            },
            new go.Binding("text", "title")),
        $(go.Panel, "Auto",
            //contextMenu右键菜单属性
            { contextMenu: $(go.Adornment)},
            $(go.Shape, { fill: "white" }),
            $("ScrollingTable","TABLE",
                new go.Binding("TABLE.itemArray", "items"),
                {
                    name: "SCROLLING",
                    desiredSize: new go.Size(NaN, NaN),
                    maxSize: new go.Size(NaN, 200),
                    "TABLE.itemTemplate":
                        $(go.Panel, "TableRow",
                            //$(go.Picture,"/uploads/pic/test.png" , { column: 0 }),
                            $(go.Picture,"/uploads/pic/按钮7.png" , { column: 1,height: 30 }),
                            $(go.TextBlock, { editable: false },{ column: 0 ,margin: new go.Margin(0, 10, 0, 10),alignment: go.Spot.Left,font: "bold 14px Arial"}, new go.Binding("text", "name")),
                            $(go.TextBlock, { editable: false },{ column: 1 ,margin: new go.Margin(10,  0, 0,  0), font: "bold 14px Arial"}, new go.Binding("text", "value"))
                        ),
                    "TABLE.defaultColumnSeparatorStroke": "gray",
                    "TABLE.defaultColumnSeparatorStrokeWidth": 0.5,
                    "TABLE.defaultRowSeparatorStroke": "gray",
                    "TABLE.defaultRowSeparatorStrokeWidth": 0.5
//                                "TABLE.defaultSeparatorPadding": new go.Margin(1, 3, 0, 3)
                }
            )
        )
    );
    var stateEventTemplate=$(go.Node, "Spot",
        { locationSpot: go.Spot.Center },
        new go.Binding("angle").makeTwoWay(),
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        //new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
        {resizable: true, resizeObjectName: "pic"},
        //contextMenu右键菜单属性
        { contextMenu: $(go.Adornment)},
        $(go.Panel, "Vertical",//竖着排列   Vertical
            $(go.Picture,
                {
                    //desiredSize: new go.Size(75, 75)
                    //maxSize: new go.Size(75, 75)
                },
                new go.Binding("source", "img"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("text", "text")
            ),
            $(go.TextBlock,"报警/正常",
                {

//                                background: "lightgreen",
//                        font: "normal normal 200 10px /1.2 微软雅黑",
                    font: "bold 14px Arial",
                    stroke: 'red',
                    margin: 0,
                    isMultiline: true,
                    editable: false
                },
                //暂时无背景颜色
                //new go.Binding("background", "background"),
                new go.Binding("text", "name")
            )

        ),
        {
            selectionAdornmentTemplate:Adornment
        }

    );
    var Group1Template= $(go.Group, "Vertical",
        { selectionObjectName: "PH",
            locationObjectName: "PH",
            resizable: true,
            mouseDrop: function(e, grp) {  // dropping a copy of some Nodes and Links onto this Group adds them to this Group
                // 不可以拖动一个大的  分组到另一个分组内
                // don't allow drag-and-dropping a mix of regular Nodes and Groups
                //if (e.diagram.selection.all(function(n) { return !(n instanceof go.Group); })) {
                if (e.diagram.selection.all(function(n) { return n; })) {
                    var ok = grp.addMembers(grp.diagram.selection, true);
                    if (!ok) grp.diagram.currentTool.doCancel();
                }
            },
            resizeObjectName: "PH" },
        { locationSpot: go.Spot.Center },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape,  // using a Shape instead of a Placeholder
            {
                name: "PH",
                stroke: "white",
                fill: "transparent"
            },
            new go.Binding("stroke", "stroke"),
            new go.Binding("fill", "fill"),
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify))
    );


    var GroupTemplate= $(go.Group, "Vertical",
        {

            selectionObjectName: "SHAPE",  // selecting a lane causes the body of the lane to be highlit, not the label
            layerName: "Background",  // all lanes are always behind all nodes and links
            background: "transparent",  // can grab anywhere in bounds
            movable: true, // allows users to re-order by dragging
            copyable: false,  // can't copy lanes
//                        minLocation: new go.Point(-Infinity, NaN),  // only allow horizontal movement
//                        maxLocation: new go.Point(Infinity, NaN),
//            layout: $(go.GridLayout,  // automatically lay out the lane's subgraph
//                {
//                    wrappingColumn: 2,
//                    cellSize: new go.Size(1, 1),
//                    spacing: new go.Size(5, 5),
//                    alignment: go.GridLayout.Position,
//                    comparer: function(a, b) {
//                        var ay = a.location.y;
//                        var by = b.location.y;
//                        if (isNaN(ay) || isNaN(by)) return 0;
//                        if (ay < by) return -1;
//                        if (ay > by) return 1;
//                        return 0;
//                    }
//                }),
            computesBoundsAfterDrag: true,  // needed to prevent recomputing Group.placeholder bounds too soon
            handlesDragDropForMembers: true,  // don't need to define handlers on member Nodes and Links
            mouseDrop: function(e, grp) {  // dropping a copy of some Nodes and Links onto this Group adds them to this Group
                // 不可以拖动一个大的  分组到另一个分组内
                // don't allow drag-and-dropping a mix of regular Nodes and Groups
                //if (e.diagram.selection.all(function(n) { return !(n instanceof go.Group); })) {
                if (e.diagram.selection.all(function(n) { return n; })) {
                    var ok = grp.addMembers(grp.diagram.selection, true);
                    if (!ok) grp.diagram.currentTool.doCancel();
                }
            }

            //subGraphExpandedChanged: function(grp) {
            //    var shp = grp.selectionObject;
            //    if (grp.diagram.undoManager.isUndoingRedoing) return;
            //    if (grp.isSubGraphExpanded) {
            //        shp.width = grp._savedBreadth;
            //    } else {
            //        grp._savedBreadth = shp.width;
            //        shp.width = NaN;
            //    }
            //}
        },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("isSubGraphExpanded", "expanded").makeTwoWay(),
        // the lane header consisting of a Shape and a TextBlock
        $(go.Panel, "Horizontal",
            { name: "HEADER",
                angle: 0,  // maybe rotate the header to read sideways going up
                alignment: go.Spot.Center
                //,
                //resizable: true
            },
            $(go.Panel, "Horizontal",  // this is hidden when the swimlane is collapsed
                new go.Binding("visible", "isSubGraphExpanded").ofObject(),
                $(go.Shape, "Diamond",
                    { width: 8, height: 8, fill: "white" },
                    new go.Binding("fill", "color")),
                $(go.TextBlock,  // the lane label
                    { font: "bold 13pt sans-serif", editable: true, margin: new go.Margin(2, 0, 0, 0) },
                    new go.Binding("text", "text").makeTwoWay())
            ),
            $("SubGraphExpanderButton", { margin: 5 })  // but this remains always visible!
        ),  // end Horizontal Panel



        $(go.Panel, "Auto",  // the lane consisting of a background Shape and a Placeholder representing the subgraph
            $(go.Shape, "Rectangle",  // this is the resized object
                { name: "SHAPE", fill: "white", stroke: "gray" },
                new go.Binding("fill", "color"),
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)),
            $(go.Placeholder,
                { padding: 12, alignment: go.Spot.TopLeft }),
            $(go.TextBlock,  // this TextBlock is only seen when the swimlane is collapsed
                { name: "LABEL",
                    font: "bold 13pt sans-serif", editable: true,
                    angle: 90, alignment: go.Spot.TopLeft, margin: new go.Margin(4, 0, 0, 2) },
                new go.Binding("visible", "isSubGraphExpanded", function(e) { return !e; }).ofObject(),
                new go.Binding("text", "text").makeTwoWay())
        )  // end Auto Panel



    );  // end Group
    return  {
        main:mainTemplate,
        edit:editTemplate,
        value:valueTemplate,
        onoff:OnOffValueTemplate,
        alarm:AlarmValueTemplate,
        button:ButtonTemplate,
        test:TestTemplate,
        gif:GifTemplate,
        gif_value:Gif_ValueTemplate,
        table:TableTemplate,
        state_event:stateEventTemplate,
        group:Group1Template,
        figure:FigureTemplate,
        main_edit:main_editTemplate,
        only_value:only_valueTemplate,
        state:stateTemplate,
        state_control:StateControlTemplate,
        state_value:StateValueTemplate,
        state_event:stateEventTemplate,
        state_defend:stateDefendEventTemplate,
        state_defend_1:stateDefend_1Template,
        state_defend_ctrl:stateDefendControlTemplate,
        control:ControlTemplate,
        many:ManyTemplate,
        value_1:Value_1Template,
        rule_value:rule_valueTemplate

    };

}
function init_context_menu(element,diagram){

    // This is the actual HTML context menu:
    var cxElement = document.getElementById(element);
    //var cxElement = document.getElementById("contextMenu");
    // We don't want the div acting as a context menu to have a (browser) context menu!
    cxElement.addEventListener("contextmenu", function(e) { e.preventDefault(); return false; }, false);
    cxElement.addEventListener(element, function(e) { e.preventDefault(); return false; }, false);
    cxElement.addEventListener("blur", function(e) { cxMenu.stopTool(); }, false);
    // Override the ContextMenuTool.showContextMenu and hideContextMenu methods
    // in order to modify the HTML appropriately.
    var cxTool = diagram.toolManager.contextMenuTool;

    //重写showContextMenu方法
    cxTool.showContextMenu = function(contextmenu, obj) {
        //var diagram = this.diagram;
        if (diagram === null) return;
        // Hide any other existing context menu
        if (contextmenu !== diagram.currentContextMenu) {
            this.hideContextMenu();
        }
        // Show only the relevant buttons given the current state.
        var cmd = diagram.commandHandler;
//        document.getElementById("cut").style.display = cmd.canCutSelection() ? "block" : "none";
//        document.getElementById("copy").style.display = cmd.canCopySelection() ? "block" : "none";
//        document.getElementById("paste").style.display = cmd.canPasteSelection() ? "block" : "none";
//        document.getElementById("delete").style.display = cmd.canDeleteSelection() ? "block" : "none";
//        document.getElementById("color").style.display = obj !== null ? "block" : "none";
        // Now show the whole context menu element
        cxElement.style.display = "block";

        // we don't bother overriding positionContextMenu, we just do it here:
        var mousePt = diagram.lastInput.viewPoint;
        cxElement.style.left = mousePt.x +10+ "px";
        cxElement.style.top = mousePt.y +70+ "px";
        // Remember that there is now a context menu showing
        this.currentContextMenu = contextmenu;
    }

    //
    cxTool.hideContextMenu = function() {
        if (diagram.currentContextMenu === null) return;
        cxElement.style.display = "none";
        this.currentContextMenu = null;
    }
}

