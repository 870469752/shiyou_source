<style>
    html { overflow-x: hidden; overflow-y: hidden; }
    #camera div{width: 50%;height:50%; float: left;padding-top: 0px}
    #camera embed{width: 100%;height:100%; float: left;padding-top: 0px}
    body {width: 100%;height:100%;margin-left: 0px;margin-top: 0px;margin-bottom: 0px}
</style>
<script src="js/vedio/jquery-1.7.1.min.js"></script>
<script src="js/socket/socket.io.js"></script>
<div id="camera">
    <div id="camera1">
        <embed
            type="application/x-vlc-plugin"
            pluginspage="http://www.videolan.org"
            version="VideoLAN.VLCPlugin.2"
            id="vlc1"
            loop="false"
            autoplay="yes"
            controls="false"
            target="">
        </embed>
    </div>
    <div id="camera2">
        <embed
            type="application/x-vlc-plugin"
            pluginspage="http://www.videolan.org"
            version="VideoLAN.VLCPlugin.2"
            id="vlc2"
            loop="false"
            autoplay="yes"
            controls="false"
            target="">
        </embed>
    </div>
    <div id="camera3">
        <embed
            type="application/x-vlc-plugin"
            pluginspage="http://www.videolan.org"
            version="VideoLAN.VLCPlugin.2"
            id="vlc3"
            loop="false"
            autoplay="yes"
            controls="false"
            target="">
        </embed>
    </div>
    <div id="camera4">
        <embed
            type="application/x-vlc-plugin"
            pluginspage="http://www.videolan.org"
            version="VideoLAN.VLCPlugin.2"
            id="vlc4"
            loop="false"
            autoplay="yes"
            controls="false"
            target="">
        </embed>
    </div>
</div>

<script language="JavaScript">
    window.onload = function () {
        //var FullDiv = document.documentElement;
        //requestFullScreen(FullDiv);
        var tem_camera = '';
        var arr = []; //存放摄像头的IP及CHANNEL
        var judge_f11 = 0;
        var tem_interval1 = 0;
        var iframe_index = 1;
        var first = true;
        var key_index = 0; //摄像头数组的指针
        var vlc1 = document.getElementById("vlc1");//用$("#vlc1")的方式不行
        var vlc2 = document.getElementById("vlc2");
        var vlc3 = document.getElementById("vlc3");
        var vlc4 = document.getElementById("vlc4");
        var camera_id = "8985302,8985293,8985294,8985292,11635,11636,11637,11639,360,361,362,363";
        //var camera_id = "11190,11191,9030010,9030011,8985302,8985293,8985294,8985292";
        $(document).ready(function(){
            var wth= screen.width/2;
            var hth= screen.height/2;
//            var wth= $("#camera1").width();
//            var hth= $("#camera1").height();
            changeWH(wth,hth);
            getCameraInfo(camera_id);

            var socket = io('http://11.29.1.17:9090');
            socket.on('connection', function () {
                console.log('connection setup for socket.io')
            });

            socket.on('CameraPoint', function (msg) {
                arr = []; //存放摄像头的IP及CHANNEL
                iframe_index = 1; //判断调用的窗口
                tem_interval = 0; //定时器
                camera_str = msg;
                first = true;
                key_index = 0; //摄像头数组的指针
                window.clearInterval(tem_interval1);
                window.clearInterval(tem_interval2);
                getCameraInfo(camera_str);
            });
        })

        function getCameraInfo(camera_id){
            if(tem_camera == camera_id) {return false;}
            tem_camera = camera_id;
            $.ajax({
                url: '/video-surveillance/crud/get-info-by-id?id='+camera_id,
                async: false,
                dateType: 'json',
                success: function(data)
                {
                    var temp_camera_array = JSON.parse(data).info;
                    if(temp_camera_array){
                        var i = 0;
                        $.each(temp_camera_array, function (k, v) {
                            tem_arr = [];
                            tem_arr[0]= v.ip;
                            tem_arr[1]= v.channel;
                            arr[i]=tem_arr;
                            i++;
                        })
                        if(tem_interval1){window.clearInterval(tem_interval1);}
                        tem_interval1 = setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},300);
                    }else{
                        return false;
                    }
                }
            });
        }

        function RealPlay(ip,channel){
            if(ip.indexOf("192.168.16.")==-1){
                var username = "admin"; var password = "12345";
                if( ip == '11.8.173.51' || ip == '11.8.173.52' || ip == '11.8.173.53' || ip == '11.8.173.54'){
                    password = "7ca29e68";
                }
                var str = "rtsp://"+username+":"+password+"@"+ip+":554/h264/ch"+channel+"/main/av_stream"
            }else{
                var username = "admin"; var password = "admin";
                var str = "rtsp://"+username+":"+password+"@"+ip+":554/cam/realmonitor?channel="+channel+"&subtype=0"
            }
            switch(iframe_index)
            {
                case 1:
                    vlc1.playlist.clear();
                    var id = vlc1.playlist.add(str);
                    vlc1.playlist.playItem(id);
                    break;
                case 2:
                    vlc2.playlist.clear();
                    var id = vlc2.playlist.add(str);
                    vlc2.playlist.playItem(id);
                    break;
                case 3:
                    vlc3.playlist.clear();
                    var id = vlc3.playlist.add(str);
                    vlc3.playlist.playItem(id);
                    break;
                case 4:
                    vlc4.playlist.clear();
                    var id = vlc4.playlist.add(str);
                    vlc4.playlist.playItem(id);
                    break;
                default:
                    alert(iframe_index);
            }
            iframe_index++;
            //循环数组结束后将指针重新指向第一位
            if(key_index == arr.length-1 && arr.length>4){key_index=0;}else{key_index++;}
            //如果是第一次循环&&数组长度大于子窗口数
            if(first && arr.length>4 && iframe_index == 5){
                iframe_index = 1;
                window.clearInterval(tem_interval1);
                tem_interval2 = setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},5000);
                first = false;
            }
            if(iframe_index == 5){iframe_index = 1;}
            //当数组长度小于窗口数&&数组指针大于数组长度时停止定时
            if(arr.length<5 && key_index >= arr.length){
                window.clearInterval(tem_interval1);return false;
            }
        }

        function sleep(n) {
            var  start=new Date().getTime();
            while(true) if(new Date().getTime()-start>n)  break;
        }




        function xKeyEvent(e){
            var e = (e)?e:((window.event)?window.event:"");
            keyCode = e.keyCode? e.keyCode:(e.which?e.which: e.charCode);
            //F11
            if(keyCode==122){
                if(judge_f11==0){
                    var wth= screen.width/2;
                    var hth= screen.height/2;
                    judge_f11 = 1;
                }else{
                    var wth= $("#camera1").width();
                    var hth= $("#camera1").height();
                    judge_f11 = 0;
                }
                changeWH(wth,hth);
                return false;
            }
        }
        //获取键盘F11事件
        //document.onkeyup = xKeyEvent;
        function changeWH(wth,hth){
            vlc1.video.aspectRatio = wth+":"+hth;
            vlc2.video.aspectRatio = wth+":"+hth;
            vlc3.video.aspectRatio = wth+":"+hth;
            vlc4.video.aspectRatio = wth+":"+hth;
        }

        /*function requestFullScreen(element) {
            // 判断各种浏览器，找到正确的方法
            var requestMethod = element.requestFullScreen || //W3C
                element.webkitRequestFullScreen ||    //Chrome等
                element.mozRequestFullScreen || //FireFox
                element.msRequestFullScreen; //IE11
            if (requestMethod) {
                requestMethod.call(element);
            }
            else if (typeof window.ActiveXObject !== "undefined") {//for Internet Explorer
                var wscript = new ActiveXObject("WScript.Shell");
                if (wscript !== null) {
                    wscript.SendKeys("{F11}");
                }
            }
        }*/
        function requestFullScreen(element) {
            if(element.requestFullscreen){
                element.requestFullscreen();
            }else if(element.mozRequestFullScreen){
                element.mozRequestFullScreen();
            }else if(element.webketRequestFullscreen){
                element.webketRequestFullscreen();
            }else if(element.msRequestFullscreen){
                element.msRequestFullscreen();
            }
        }
    }
</script>

