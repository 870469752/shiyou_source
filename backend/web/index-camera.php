

<script src="js/vedio/jquery-1.7.1.min.js"></script>
<script src="js/vedio/demo.js"></script>
<script src="js/vedio/webVideoCtrl.js"></script>
<!--position:absolute; left:0; top:0px;-->
<div id="haikang" style="position:absolute; left:0; top:0px;  width: 100%;height: 100%;border: 0px">

</div>
<div style="position:absolute; left:0; top:0px; width: 100%;height: 100%;border: 0px;display: none">
    <div id="div_1" class="plugin" style="width: 50%;height:50%;padding-top:0px;float: left;background-color: #0000ff">111</div>
    <div id="div_2" style="width: 50%;height:50%;float: left;background-color: yellow">222</div>
    <div id="div_3" style="width: 50%;height:50%;float: left;background-color: red">333</div>
    <div id="div_4" style="width: 50%;height:50%;float: left;background-color: rosybrown">444</div>
</div>
<script language="JavaScript">
    window.onload = function () {
        szPort = 80;
        szUsername = "admin";
        szPassword = "12345";
        indexNum = 0;
        key_index = 0;
        first = true;
        arr = [];

        // 初始化插件参数及插入插件
        WebVideoCtrl.I_InitPlugin('100%', '100%', {
            iWndowType: 2,
            bDebugMode:true,
            cbSelWnd: function (xmlDoc) {
                g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
            }
        });
        WebVideoCtrl.I_InsertOBJECTPlugin("haikang");

        // 窗口事件绑定
        $(window).bind({
            resize: function () {
                var $Restart = $("#restartDiv");
                if ($Restart.length > 0) {
                    var oSize = getWindowSize();
                    $Restart.css({
                        width: oSize.width + "px",
                        height: oSize.height + "px"
                    });
                }
            }
        });

        //初始化日期时间
        var szCurTime = dateFormat(new Date(), "yyyy-MM-dd");
        $("#starttime").val(szCurTime + " 00:00:00");
        $("#endtime").val(szCurTime + " 23:59:59");

        $(document).ready(function () {
            WebVideoCtrl.I_FullScreen(true);
            //requestFullScreen();
            to_login('403,404,405,311,451,452,382,383');
        });
    }

    function show_video(){
        $.ajax({
            url: '/video-surveillance/crud/get-camera-point'    ,
            async: false,
            dateType: 'json',
            success: function(data)
            {
                camera_patrol(data);
            }
        });
    }

    function to_login(id){
        $.ajax({
            url: '/video-surveillance/crud/get-info-by-id?id='+id,
            async: false,
            dateType: 'json',
            success: function(data)
            {
                var i = 0;
                $.each(JSON.parse(data).ips, function (k, v) {
                    clickLogin3(v);
                    sleep(100);
                })
                setTimeout(function(){camera_patrol(data);},500);
            }
        });
    }

    function camera_patrol(data){
        $.each(JSON.parse(data).info, function (k, v) {
            var tem_arr = [];
            tem_arr[0]= v.ip;
            tem_arr[1]= v.channel;
            arr[k]=tem_arr;
        })
        tem_interval = setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},300);
    }

    //显示图像
    function RealPlay(ip,channel) {
        if(key_index == 3 && first){
            window.clearInterval(tem_interval);
            first = false;
            setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},5000);
        }
        if(indexNum==4){indexNum = 0;}
        WebVideoCtrl.I_Stop(indexNum);
        var iRet = WebVideoCtrl.I_StartRealPlay(ip, {
            iWndIndex:indexNum,
            iStreamType: 1,
            iChannelID: channel,
            bZeroChannel: false
        });
        indexNum++;
        if (0 == iRet) {
            szInfo = "开始预览成功！";
        } else {
            szInfo = "开始预览失败！";
        }
        key_index++;
        if(key_index >= arr.length){
            //return false;
            key_index=0;
        }
        console.log(ip +"**"+channel+ "**" + szInfo);
    }

    /*function camera_patrol(data){
        var temp_data = data;
        var temp_len = (JSON.parse(data).info).length;
        //alert(temp_len);return false;
        $.each(JSON.parse(data).info, function (k, v) {
            var tem_ip = v.ip;   var tem_channel = v.channel;
            RealPlay(tem_ip,tem_channel);
            if(first){
                sleep(300);
            }else{
                sleep(3000);
            }
            if(k==temp_len){
                camera_patrol(temp_data);
            }
        })
    }*/

    function sleep(n)
    {
        var  start=new Date().getTime();
        while(true) if(new Date().getTime()-start>n)  break;
    }

    // 登录
    function clickLogin3(szIP) {
        var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUsername, szPassword, {
            success: function (xmlDoc) {
                console.log(szIP + " 登录成功！");
            },
            error: function () {
                console.log(szIP + " 登录失败！");
            }
        });
        if (-1 == iRet) {
            console.log(szIP + " 已登录过！");
        }
    }

    // 退出
    function clickLogout2(szIP) {
        var iRet = WebVideoCtrl.I_Logout(szIP);
        if (0 == iRet) {
            szInfo = "退出成功！";
        } else {
            szInfo = "退出失败！";
        }
        console.log(szIP + " " + szInfo);
    }

    function requestFullScreen() {
        var element = $("#haikang");
        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;
        if (requestMethod) {
            requestMethod.call(element);
        } else if (typeof window.ActiveXObject !== "undefined") {
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    }
</script>

