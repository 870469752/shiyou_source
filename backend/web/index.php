<?php

defined('YII_DEBUG') or define('YII_DEBUG', true); // 代码错误显示语句和行号
defined('YII_ENV') or define('YII_ENV', 'prod'); // 产品版和开发版："prod" and "dev"

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/predis/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/aliases.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
