<style>
    #camera div{width: 50%;height:50%;float: left;}
    iframe{width: 100%;height: 100%}
    html { overflow-x: hidden; overflow-y: hidden; }
</style>
<script src="js/vedio/jquery-1.7.1.min.js"></script>
<script src="js/socket/socket.io.js"></script>
<div id="camera" style="position:absolute; left:0; top:0px;width: 100%;height: 100%">
    <div id="div_1">
        <iframe  name="frame_1"  src="camera_1.php" >
        </iframe>
    </div>
    <div id="div_2" >
        <iframe src="camera_2.php" name="frame_2">
        </iframe>
    </div>
    <div id="div_3"  >
        <iframe src="camera_3.php" name="frame_3">
        </iframe>
    </div>
    <div id="div_4" >
        <iframe src="camera_4.php" name="frame_4">
        </iframe>
    </div>
</div>
<script language="JavaScript">
    window.onload = function () {
        tem_camera = '';
        camera_str = '';
        arr = []; //存放摄像头的IP及CHANNEL
        iframe_index = 1; //判断调用的窗口
        key_index = 0; //摄像头数组的指针
        tem_interval1 = 0;
        first = true;
        var camera_id = "8985302,8985293,8985294,8985292,11635,11636,11637,11639,360,361,362,363";
        $(document).ready(function () {
            var FullDiv = document.getElementById("camera");
            //requestFullScreen(FullDiv);
            to_login(camera_id);
            var socket = io('http://11.29.1.17:9090');
            socket.on('connection', function () {
                console.log('connection setup for socket.io')
            });

            socket.on('CameraPoint', function (msg) {
                arr = []; //存放摄像头的IP及CHANNEL
                iframe_index = 1; //判断调用的窗口
                tem_interval = 0; //定时器
                camera_str = msg;
                first = true;
                key_index = 0; //摄像头数组的指针
                window.clearInterval(tem_interval1);
                window.clearInterval(tem_interval2);
                setTimeout(function(){
                    to_login(camera_str);
                },300);
            });
        });
    }

    function to_login(camera_id){
        //alert(tem_camera+"***********"+camera_id);
        if(tem_camera == camera_id) {return false;}
        tem_camera = camera_id;
        $.ajax({
            url: '/video-surveillance/crud/get-info-by-id?id='+camera_id,
            async: false,
            dateType: 'json',
            success: function(data)
            {
                setTimeout(function(){
                    camera_patrol(JSON.parse(data).info);
                },300)
                return false;
            }
        });
    }

    function camera_patrol(data){
        arr = [];
        var i = 0;
        if(data){
            $.each(data, function (k, v) {
                tem_arr = [];
                tem_arr[0]= v.ip;
                tem_arr[1]= v.channel;
                arr[i]=tem_arr;
                i++;
            })

            if(tem_interval1){window.clearInterval(tem_interval1);}

            tem_interval1 = setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},300);
        }else{
            return false;
        }


        //tem_interval = setInterval(function(){RealPlay("192.168.45.190",1);},4000);

    }

    function RealPlay(ip,channel){
        //alert(arr.length +"*********"+ key_index+"*********"+ iframe_index);
        switch(iframe_index)
        {
            case 1:
                frame_1.window.clickLogin(ip,channel);
                iframe_index++;
                break;
            case 2:
                frame_2.window.clickLogin(ip,channel);
                iframe_index++;
                break;
            case 3:
                frame_3.window.clickLogin(ip,channel);
                iframe_index++;
                break;
            case 4:
                frame_4.window.clickLogin(ip,channel);
                iframe_index++;
                break;
            default:
                alert(iframe_index);
        }
        //循环数组结束后将指针重新指向第一位
        if(key_index == arr.length-1 && arr.length>4){key_index=0;}else{key_index++;}
        //如果是第一次循环&&数组长度大于子窗口数
        if(first && arr.length>4 && iframe_index == 5){
            iframe_index = 1;
            window.clearInterval(tem_interval1);
            tem_interval2 = setInterval(function(){RealPlay(arr[key_index][0],arr[key_index][1]);},5000);
            first = false;
        }
        if(iframe_index == 5){iframe_index = 1;}
        //当数组长度小于窗口数&&数组指针大于数组长度时停止定时
        if(arr.length<5 && key_index >= arr.length){
            window.clearInterval(tem_interval1);return false;
        }
    }

    function sleep(n) {
        var  start=new Date().getTime();
        while(true) if(new Date().getTime()-start>n)  break;
    }

    function requestFullScreen(element) {
        // 判断各种浏览器，找到正确的方法
        var requestMethod = element.requestFullScreen || //W3C
            element.webkitRequestFullScreen ||    //Chrome等
            element.mozRequestFullScreen || //FireFox
            element.msRequestFullScreen; //IE11
        if (requestMethod) {
            requestMethod.call(element);
        }
        else if (typeof window.ActiveXObject !== "undefined") {//for Internet Explorer
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    }

    //退出全屏 判断浏览器种类
    function exitFull() {
        // 判断各种浏览器，找到正确的方法
        var exitMethod = document.exitFullscreen || //W3C
            document.mozCancelFullScreen ||    //Chrome等
            document.webkitExitFullscreen || //FireFox
            document.webkitExitFullscreen; //IE11
        if (exitMethod) {
            exitMethod.call(document);
        }
        else if (typeof window.ActiveXObject !== "undefined") {//for Internet Explorer
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    }
</script>

