<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.collector".
 *
 * @property integer $id
 * @property integer $collector_id
 * @property string $ip
 * @property string $last_update
 * @property string $name
 */
class Collector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.collector';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collector_id', 'ip', 'last_update'], 'required'],
            [['collector_id'], 'integer'],
            [['ip', 'last_update'], 'string'],
            [['name'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'collector_id' => 'Collector ID',
            'ip' => 'Ip',
            'last_update' => 'Last Update',
            'name' => 'Name',
        ];
    }
}
