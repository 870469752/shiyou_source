<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.bacnet_controller".
 *
 * @property integer $id
 * @property string $name
 * @property integer $protocol_id
 * @property integer $image_id
 * @property boolean $is_available
 * @property boolean $is_shield
 * @property string $update_timestamp
 * @property integer $src_eid
 * @property integer $src_controller_id
 * @property string $create_timestamp
 * @property string $src_name
 * @property string $ip_address
 * @property integer $port
 * @property integer $object_instance_number
 * @property string $object_name
 */
class BacnetController extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.bacnet_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'update_timestamp', 'create_timestamp', 'ip_address'], 'string'],
            [['protocol_id', 'image_id', 'src_eid', 'src_controller_id', 'port', 'object_instance_number'], 'integer'],
            [['is_available', 'is_shield'], 'boolean'],
            [['src_name', 'object_name'], 'string', 'max' => 255],
            [['src_eid', 'src_controller_id'], 'unique', 'targetAttribute' => ['src_eid', 'src_controller_id'], 'message' => 'The combination of Src Eid and Src Controller ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'protocol_id' => 'Protocol ID',
            'image_id' => 'Image ID',
            'is_available' => 'Is Available',
            'is_shield' => 'Is Shield',
            'update_timestamp' => 'Update Timestamp',
            'src_eid' => 'Src Eid',
            'src_controller_id' => 'Src Controller ID',
            'create_timestamp' => 'Create Timestamp',
            'src_name' => 'Src Name',
            'ip_address' => 'Ip Address',
            'port' => 'Port',
            'object_instance_number' => 'Object Instance Number',
            'object_name' => 'Object Name',
        ];
    }
}
