<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.sub_system_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $category_id
 */
class Sub_System_Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'string'],
            [['parent_id'], 'integer'],
            [['category_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
        ];
    }
    public static function getAll(){
        return self::find()
            ->select(['id', 'name'])
            ->where('id!=0')
            ->asArray()->indexBy('id')->all();
    }
}
