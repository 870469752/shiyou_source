<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "core.discuss".
 *
 * @property string $content
 * @property string $timestamp
 * @property integer $user_id
 */
class Discuss extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.discuss';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'timestamp'], 'string'],
            [['user_id'], 'required'],
            [['user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content' => Yii::t('app', 'Content'),
            'timestamp' => Yii::t('app', 'Timestamp'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
