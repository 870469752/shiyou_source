<?php

namespace backend\models;

use Yii;
use backend\models\Role;

/**
 * This is the model class for table "core.report_manage".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_type
 * @property integer $parent_id
 * @property string $category_id
 * @property string $location_type
 * @property string $report_rel
 * @property integer $role_id
 */
class ReportManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.report_manage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'location_type', 'report_rel', 'role_id'], 'string'],
            [['category_type', 'parent_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_type' => 'Category Type',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
            'location_type' => 'Location Type',
            'report_rel' => 'Report Rel',
            'role_id' => 'Role ID',
        ];
    }

    /*
     * 查找角色
     */
    public function findRole()
    {
       $role=Role::find()->select(["id","name->'data'->>'zh-cn' as name" ])->asArray()->all();
        $roleArray=array();
        $roleArray['']='';

        for($i=0;$i<count($role);$i++)
        {
            $roleArray[$role[$i]['id']]=$role[$i]['name'];
        }
      /*  echo "<pre>";
        print_r($roleArray);
        die;*/
        return $roleArray;
    }

    /*
     * 根据用户角色ID查找报表
     */
    public function getReports($user_roleId)
    {
    /*    return ReportManage::find()
            ->select(['id','title'=>'name','parent_id','role_id'])
            ->where(['array_length(role_id, 1)'=>3])
            ->OrderBy('id desc')
            ->asArray()->all();*/
        $sql="SELECT id,name as title,parent_id,role_id FROM core.report_manage ".
            "where ".$user_roleId."= ANY (role_id) ";
        return self::findBySql($sql) ->asArray()->all();
    }

    /**
     * 根据id修改字段report_rel的内容
     * @param $id
     * @param $report_rel
     * @return true or false
     */
    public function updateReportRel($id,$report_rel)
    {
        $data = ['report_rel'=>$report_rel];
        return self::find()->createCommand()->update('core.report_manage', $data, "id = $id")->execute();
    }
}
