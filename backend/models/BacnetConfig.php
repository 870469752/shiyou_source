<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.bacnet_config".
 *
 * @property integer $id
 * @property integer $data_query_interval
 * @property integer $info_update_interval
 */
class BacnetConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.bacnet_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_query_interval', 'info_update_interval'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data_query_interval' => Yii::t('app', 'Data Query Interval'),
            'info_update_interval' => Yii::t('app', 'Info Update Interval'),
        ];
    }
}
