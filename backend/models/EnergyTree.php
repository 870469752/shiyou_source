<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.energy_tree".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $parent_serial
 * @property string $name
 * @property integer $generate_point
 * @property integer $relation_point
 */
class EnergyTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.energy_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['relation_point', 'name'], 'string'],
            [['parent_id', 'generate_point'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'parent_serial' => Yii::t('app', 'Parent Serial'),
            'name' => Yii::t('app', 'Name'),
            'generate_point' => Yii::t('app', 'Generate Point'),
            'relation_point' => Yii::t('app', 'Relation Point'),
        ];
    }

    public static  function findFirstId(){
        return @static::find()->select('id')->where(['parent_id' => 0])->orderBy(['id' => SORT_ASC])->one()->id;
    }

    public static function findChildren($id){
        return static::find()->where(['parent_id' => $id]);
    }

    public static function ChildrenSumFormula($id){
        return static::findChildren($id)->select(['formula' => "'sum(p' || array_to_string(ARRAY_AGG(generate_point), ' + p') || ')'"]);
    }
}
