<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.control_info".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $related_id
 * @property integer $time_mode
 * @property string $data
 */
class LoopRecord extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.loop_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loop_info','start_time','end_time'], 'string'],
            ['loop_state', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loop_info' => 'LoopInfo',
            'loop_state' => 'LoopState',
            'start_time' => 'StartTime',
            'end_time'=>'EndTime',
        ];
    }
    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            $model = new LoopRecord();
            return $model;
        }
    }
}
