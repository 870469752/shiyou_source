<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.mail_config".
 *
 * @property integer $id
 * @property string $from
 * @property string $host
 * @property string $user
 * @property string $password
 * @property boolean $smtp_auth
 */
class MailConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.mail_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail_from', 'mail_host', 'mail_user', 'mail_password'], 'required'],
            [['smtp_auth'], 'boolean'],
            [['mail_from', 'mail_host', 'mail_user', 'mail_password'], 'string', 'max' => 50]
        ];
    }

    public function mailConfigSave()
    {
        return $this->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mail_from' => Yii::t('app', 'From'),
            'mail_host' => Yii::t('app', 'Host'),
            'mail_user' => Yii::t('app', 'User'),
            'mail_password' => Yii::t('app', 'Password'),
            'smtp_auth' => Yii::t('app', 'Smtp Auth'),
        ];
    }
}
