<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.access_controller_log".
 *
 * @property integer $id
 * @property integer $access_controller_id
 * @property string $timestamp
 * @property string $message
 * @property integer $code
 */
class AccessControllerLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.access_controller_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_controller_id', 'timestamp', 'code'], 'required'],
            [['access_controller_id', 'code'], 'integer'],
            [['timestamp'], 'string'],
            [['message'], 'string', 'max' => 2048]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '门禁设备日志编号',
            'access_controller_id' => '门禁设备编号，对应 access_controller 表中的 id 字段
',
            'timestamp' => '时间',
            'message' => '门禁设备故障消息',
            'code' => '门禁设备故障编码',
        ];
    }
}
