<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.command_log".
 *
 * @property integer $id
 * @property integer $point_id
 * @property integer $type
 * @property string $value
 * @property string $timestamp
 * @property integer $user_id
 * @property integer $status
 */
class CommandLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.command_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'value', 'user_id', 'status'], 'required'],
            [['point_id', 'type', 'user_id', 'status'], 'integer'],
            [['value', 'timestamp'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'type' => 'Type',
            'value' => 'Value',
            'timestamp' => 'Timestamp',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }
}
