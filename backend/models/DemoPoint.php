<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "protocol.demo_point".
 *
 * @property integer $id
 * @property integer $interval
 * @property string $name
 * @property integer $protocol_id
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_shield
 * @property boolean $is_upload
 * @property boolean $is_record
 * @property boolean $is_system
 * @property string $last_update
 * @property string $last_value
 * @property integer $unit
 * @property string $value_filter
 * @property string $value_type
 */
class DemoPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.demo_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interval', 'protocol_id', 'unit'], 'integer'],
            [['name', 'base_value', 'base_history', 'last_update', 'last_value', 'value_filter'], 'string'],
            [['base_history', 'last_update', 'min_value', 'max_value'], 'required'],
            [['is_available', 'is_shield', 'is_upload', 'is_record', 'is_system'], 'boolean'],
            [['value_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'interval' => '记录间隔',
            'name' => Yii::t('app', 'Name'),
            'protocol_id' => Yii::t('app', 'Protocol ID'),
            'base_value' => Yii::t('app', 'Base Value'),
            'base_history' => Yii::t('app', 'Base History'),
            'is_available' => Yii::t('app', 'Is Available'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'is_record' => '是否记录',
            'is_system' => Yii::t('app', 'Is System'),
            'last_update' => Yii::t('app', 'Last Update'),
            'last_value' => Yii::t('app', 'Last Value'),
            'unit' => Yii::t('app', 'Unit'),
            'value_filter' => Yii::t('app', 'Value Filter'),
            'value_type' => '数值类型',
            'max_value' => '最大值',
            'min_value' => '最小值'
        ];
    }

    public function saveDemoPoint()
    {

        $_his = array(
            'start' => $this->last_update,
            'end' => null,
            'value' => 0
        );
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name);
            $this->base_history = MyFunc::createJson( $_his, 'base_history' );
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        $this->last_update = date('Y-m-d H:i:s');
        return $this->save();
    }
}
