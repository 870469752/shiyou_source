<?php

namespace backend\models;

use common\library\MyFunc;
use Yii;

/**
 * This is the model class for table "core.task".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $args
 *
 * @property ScheduleTask[] $scheduleTasks
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'args'], 'string'],
            [['type'], 'required'],
            [['type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'description' => 'Description',
            'type' => 'Type',
            'args' => 'Args',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheduleTasks()
    {
        return $this->hasMany(ScheduleTask::className(), ['task_id' => 'id']);
    }

    public function getNameMap()
    {
//        $task=Task::find()->select(["id","name->'data'->>'zh-cn' as name" ])->asArray()->all();
//        $taskArray=array();
//        $taskArray['']='';
//
//        for($i=0;$i<count($task);$i++)
//        {
//            $taskArray[$task[$i]['id']]=$task[$i]['name'];
//        }
//        return $taskArray;
        $model=Task::find()->all();
        return MyFunc::_map($model,'id','name');
    }
}
