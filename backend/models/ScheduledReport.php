<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.scheduled_report".
 *
 * @property integer $id
 * @property string $create_timestamp
 * @property integer $owner
 * @property string $config
 * @property string $name
 * @property string $header
 * @property string $start_timestamp
 * @property string $end_timestamp
 * @property string $last_generate_timestamp
 * @property string $next_generate_timestamp
 * @property string $auth_users
 * @property integer $count
 * @property string $type
 *
 * @property ScheduledReportLog[] $scheduledReportLogs
 * @property User $user
 */
class ScheduledReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $type;
    public static function tableName()
    {
        return 'core.schedule_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_timestamp', 'config', 'name','header','start_timestamp','end_timestamp','last_generate_timestamp','next_generate_timestamp','auth_users','auth_roles'], 'string'],
            [['owner', 'config','header','auth_users'], 'required'],
            [['owner','count',], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_timestamp' => '创建时间',
            'owner' => '创建者',
            'config' => '配置',
            'name' => '报表名称',
            'start_timestamp'=>'开始时间',
            'end_timestamp'=>'结束时间',
            'last_generate_timestamp'=>'上次生成时间',
            'next_generate_timestamp'=>'下次生成时间',
            'count'=>'已生成报表个数',
            'type'=>'报表类型'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheduledReportLogs()
    {
        return $this->hasMany(ScheduledReportLog::className(), ['report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    public function findLastId($user_id)
    {
        return $this->find()->select('id')->where("owner = $user_id")->orderBy("create_timestamp desc")->limit(1)->asArray()->all();
    }

    public function ScheduledReportSave()
    {
        //echo '<pre>';
        $data = Yii::$app->request->post();
        $this->setAttribute('config',$data['ScheduledReport']['config']);
        $arr = explode("&&",($this->config));
        $this->name = $arr[0];
        array_splice($arr,0,1);
        $data_hour = '';
        $data_day = '';
        $data_month = '';
        $data_gap = '';
        $data_source = '';
        $data_type['type'] = $arr[0];

        if($arr[1] == 'hour'){
            $hour_arr = explode('-',$arr[4]);
            foreach($hour_arr as $k => $v){
                $data_hour['hour'][$k]['start'] = $v;
                $data_hour['hour'][$k]['end'] = $v;
                $data_hour['hour'][$k]['name'] = $v.':00';
            }
        }elseif($arr[1] == 'auto') {
            $hour_arr = explode(';', $arr[4]);
            foreach ($hour_arr as $k => $v) {
                $v = explode('-', $v);
                $data_hour['hour'][$k]['start'] = $v[1];
                $data_hour['hour'][$k]['end'] = $v[2];
                $data_hour['hour'][$k]['name'] = $v[0];
            }
        }else{
            $data_hour['hour'] = 'null';
        }
        //print_r($data_hour);
        if($arr[0] =='day'){
            $data_month['month'] ='null';
            $data_day['day'] = 'null';
        }elseif($arr[0] == 'month'){
            $data_month['month'] = 'null';
            $day_arr = explode('-',$arr[3]);
            foreach($day_arr as $k => $v){
                $data_day['day'][$k]['start'] = $v;
                $data_day['day'][$k]['end'] = $v;
                $data_day['day'][$k]['name'] = $v.'日';
            }
        }elseif($arr[0] == 'year'){
            $data_day['day'] = 'null';
            $month_arr = explode('-',$arr[2]);
            foreach($month_arr as $k => $v){
                $data_month['month'][$k]['start'] = $v;
                $data_month['month'][$k]['end'] = $v;
                $data_month['month'][$k]['name'] = $v.'月';
            }
        }

        if($data_hour['hour'] != 'null'){
            $data_gap['gap']['type'] = 'hour';
            foreach($data_hour['hour'] as $k => $v){
                $data_gap['gap']['data'][$k]['start'] = $v['start'];
                $data_gap['gap']['data'][$k]['end'] = $v['end'];
            }
        }
        if($data_hour['hour'] == 'null' && $data_day['day'] != 'null'){
            $data_gap['gap']['type'] = 'day';
            foreach($data_day['day'] as $k => $v){
                $data_gap['gap']['data'][$k]['start'] = $v['start'];
                $data_gap['gap']['data'][$k]['end'] = $v['end'];
            }
        }
        if($data_hour['hour'] == 'null' && $data_day['day'] == 'null' && $data_month['month'] != 'null'){
            $data_gap['gap']['type'] = 'month';
            foreach($data_month['month'] as $k => $v){
                $data_gap['gap']['data'][$k]['start'] = $v['start'];
                $data_gap['gap']['data'][$k]['end'] = $v['end'];
            }
        }
        //print_r($data_gap);
        $data_source_arr = explode(';',$arr[5]);
        foreach($data_source_arr as $k => $v){
            $v = explode('##',$v);
            $data_source['data_source'][$k]['name'] = $v[0].'('.$v[2].')';
            $data_source['data_source'][$k]['column'] = $k;
            $data_source['data_source'][$k]['formula'] = $v[1];
            $data_source['data_source'][$k]['unit'] = $v[2];
        }

        $json_data = array_merge($data_type,$data_month,$data_day,$data_hour,$data_gap,$data_source);

        $this->config = MyFunc::createJson($json_data, 'scheduled_report');
        //print_r( $json_data);die;

        if($this->isNewRecord) {
            $this->name = MyFunc::createJson($this->name);
        } else {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        $this->timestamp = date('Y-m-d H:i:s');
        $this->user_id = Yii::$app->user->identity->id.'';
        if (!$this->save()) {
            var_dump($this->errors);die;
        }else{
            return $this->save();
        }
    }
}
