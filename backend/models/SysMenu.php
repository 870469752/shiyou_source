<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;

/**
 * This is the model class for table "core.sys_menu".
 *
 * @property integer $id
 * @property string $description
 * @property string $icon
 * @property string $depth
 * @property integer $module_id
 * @property integer $parent_id
 * @property integer $idx
 */
class SysMenu extends \yii\db\ActiveRecord
{
    public $leaf_node;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sys_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leaf_node','description','param'], 'string'],
            [['module_id', 'parent_id', 'idx'], 'integer'],
            [['icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('app', 'Menu Name'),
            'icon' => Yii::t('app', 'Icon'),
            'module_id' => Yii::t('app', 'Function module'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'idx' => Yii::t('app', 'Idx'),
        ];
    }

    //获取所有系统菜单
    public static function getAllMenu()
    {
        return self::find()->orderBy('idx asc')->asArray()->all();
    }

    /**
     * 获得 所有无连接、深度不超过6、非自己及子级菜单的菜单项
     * @return [type] [description]
     */
    public static function findcorrespondMenu($id = 'null')
    {
        $chl_ids = (intval($id))?self::findALLChl($id):'';
        //控制 深度 只有6级菜单 即 文件夹 可含有5层
        $max_depth = $chl_ids ? self::find()->select(['max(depth) as max_p', 'min(depth) as min_p'])
            ->where('id in ('.$chl_ids.')')
            ->asarray()->all():0;
        $where = (intval($id)) ? 'depth < '.(6 - ($max_depth[0]['max_p'] - $max_depth[0]['min_p'])) .' and id not in('.$chl_ids.')':('depth < 5');
        return self::find()->where($where)->andWhere(['not in', 'id', [0]])->andWhere('module_id is null')->orderBy('idx')->asarray()->all();
    }

    public static function findUrlMenu($id = null)
    {
        return Module::findModule();
    }

    /**
     * 获取所有 子菜单
     * @param  [type] $id [description]
     * @return String     返回子菜单id以逗号分隔
     */
    public static function findAllChl($id)
    {
        static $s = '';
        $chl = self::find()->select('id')
            ->where('parent_id in ('.$id.')')
            ->asArray()->all();
        $ids = !empty($chl)?implode(',',MyFunc::array_multiToSingle($chl, true)):$id;
        if($id !== $ids){
            self::findAllChl($ids);
        }
        $s .= ','.$id;
        return implode(',', array_unique(explode(',', trim($s, ','))));
    }

    /**
     * 菜单创建保存方法
     * @return [type] [description]
     */
    public function SysmenuSave()
    {
        if($this->id){
            $old_attr = $this->oldAttributes;
            $this->description = MyFunc::updateJson($this->description, $old_attr['description']);
        }else{
            $this->description = MyFunc::createJson($this->description);
        }
        $this->icon = !empty($this->icon) ? $this->icon : 'fa-folder-open';
        $p_data = self::getById($this->parent_id);
        $res = self::save();
        return true;
    }

    public static function getById($id){
        return SysMenu::findOne($id);
    }
}
