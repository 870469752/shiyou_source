<?php

namespace backend\models;

use Yii;
use backend\models\Point;
use common\library\MyFunc;

/**
 * This is the model class for table "core.point_data".
 *
 * @property string $id
 * @property integer $point_id
 * @property string $value
 * @property string $timestamp
 */
class PointDataNew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $point_name;
    public static function tableName()
    {
        return 'core.point_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'value'], 'required'],
            [['point_id'], 'integer'],
            [['value', 'timestamp','name'], 'string'],
            [['point_id', 'timestamp'], 'unique', 'targetAttribute' => ['point_id', 'timestamp'], 'message' => 'The combination of Point ID and Timestamp has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'value' => 'Value',
            'timestamp' => 'Timestamp',
        ];
    }

    public function getName($point_id)
    {
        $point=Point::find()->select(['name'])->Where(['id' => $point_id])->one();
        $name= MyFunc::DisposeJSON($point->name);
        return $name;
    }

}
