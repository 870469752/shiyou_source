<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.world_area".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 */
class WorldArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.world_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'name'], 'required'],
            [['id', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'parent_id'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
