<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.role".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name', 'description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '角色名称'),
            'description' => Yii::t('app', '角色描述'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleRole()
    {
        return $this->hasMany(ModuleRole::className(), ['role_id' => 'id']);
    }

    /**
     * 根据语言保存数据
     * @return bool
     */
    public function roleSave()
    {
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name);
            $this->description = MyFunc::createJson($this->description);
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
            $this->description = MyFunc::updateJson($this->description, $this->oldAttributes['description']);
        }
        $result=$this->save();
        return [
            'id'=>$this->id,
            'result'=>$result,
        ];
    }

}
