<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_device_rel".
 *
 * @property integer $id
 * @property integer $point_id
 * @property integer $device_id
 *
 * @property Device $device
 */
class PointDeviceRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_device_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'device_id'], 'required'],
            [['point_id', 'device_id'], 'integer'],
            [['device_id', 'point_id'], 'unique', 'targetAttribute' => ['device_id', 'point_id'], 'message' => 'The combination of Point ID and Device ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => '点位ID',
            'device_id' => '设备ID',
            'name'=>'设备名称'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }
}
