<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use yii\web\Session;
use backend\models\SubSystem;

/**
 * This is the model class for table "core.user_menu".
 *
 * @property integer $id
 * @property string $description
 * @property string $icon
 * @property integer $module_id
 * @property integer $parent_id
 * @property integer $idx
 * @property integer $user_id
 */
class UserMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.user_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','param'], 'string'],
            [['module_id', 'parent_id', 'idx', 'user_id','role_id'], 'integer'],
            [['user_id'], 'required'],
            [['icon'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'icon' => 'Icon',
            'module_id' => 'Module ID',
            'parent_id' => 'Parent ID',
            'idx' => 'Idx',
            'user_id' => 'User ID',
            'role_id' => 'Role ID',
            'param' => 'Param',
        ];
    }

    //获取用户所在角色所有菜单
    public static function getAllMenu()
    {
        $role_id = isset($_SESSION['menu_role_id'])?$_SESSION['menu_role_id']:MyFunc::getRoleId();
        $user_id = isset($_SESSION['menu_user_id'])?$_SESSION['menu_user_id']:Yii::$app->user->id;
        return self::find()->orderBy('idx asc')->where("user_id = $user_id and role_id = $role_id")->asArray()->all();
    }


    //根据user_id,role_id获取用户菜单
    public static function getAllMenuForMatch($user_id,$role_id)
    {
        return self::find()->orderBy('idx asc')->where("user_id = $user_id and role_id = $role_id")->asArray()->all();
    }

    /**
     * 创建用户菜单
     * @param  $menu 菜单数组
     * @return true/false
     */
    public function createMenu($menu){
        $id = $this->find()->select('max(id) as maxid')->asArray()->all();
        $max_id = $id[0]['maxid'];
        if(!$max_id) $max_id = 0;
        foreach($menu as $k => $v){
            $id = intval($v['id']+$max_id);
            if($v['parent_id']==0){
                $parent_id = $v['parent_id'];
            }else{
                $parent_id = intval($v['parent_id']+$max_id);
            }
            if(!isset($v['idx'])) $v['idx']=0;
            if(empty($v['param'])){
                $sql = "INSERT INTO core.user_menu(id, description, icon, module_id, parent_id, idx, user_id, role_id) VALUES(".$id.",'".$v['description']."','".$v['icon']."',".$v['module_id'].",".$parent_id.",".$v['idx'].",".$v['user_id'].",".$v['role_id'].")";
            }else{
                $sql = "INSERT INTO core.user_menu(id, description, icon, module_id, parent_id, idx, user_id, role_id, param) VALUES(".$id.",'".$v['description']."','".$v['icon']."',".$v['module_id'].",".$parent_id.",".$v['idx'].",".$v['user_id'].",".$v['role_id'].",'".$v['param']."')";
            }
            Yii::$app->db->createCommand($sql)->execute();
        }
    }

    /**
     * 删除用户时同时删除用户菜单
     * @param  $user_id 用户id
     * @return true/false
     */
    public function deleteUserMenuByUserId($user_id){
        return Yii::$app->db
            ->createCommand("delete from core.user_menu where user_id = $user_id")
            ->execute();
    }

    /**
     * 根据角色ID，用户ID 删除所对应菜单
     * @param  $user_id 用户id
     * @param  $role_id 角色id
     * @return true/false
     */
    public function deleteUserMenuByRoleId($user_id,$role_id){
        return Yii::$app->db
            ->createCommand("delete from core.user_menu where user_id = $user_id and role_id = $role_id")
            ->execute();
    }

    /**
     * 菜单创建保存方法
     * @return [type] [description]
     */
    public function UserMenuSave()
    {
        $session = new Session();
        $session->open();
        $role_id = $session['menu_role_id'];
        $user_id = $session['menu_user_id'];
        $m_models = new Module;
        $module_info = $m_models->findModuleById($this->module_id);
        if(!$this->idx) $this->idx = 0;
        if($this->id){
            $this->icon = !empty($this->icon) ? $this->icon : 'fa-folder-open';
            $this->role_id = $role_id;
            $this->user_id = $user_id;
            $old_attr = $this->oldAttributes;
            if(!empty($module_info) && $module_info[0]['path']=='subsystem_manage/crud/location-subsystem'){
                if($this->param!=$old_attr['param'])
                $this->param = 'id='.$this->param;
            }elseif(!empty($module_info) && $module_info[0]['path']=='scheduled-report/crud/new-index'){
                if($this->param!=$old_attr['param'])
                $this->param = 'type=view&id='.$this->param;
            }else{
                $this->param = null;
            }
            $this->description = MyFunc::updateJson($this->description, $old_attr['description']);
            $res = self::save();
        }else{

            $this->icon = !empty($this->icon) ? $this->icon : 'fa-folder-open';
            $id = $this->find()->select('max(id) as maxid')->asArray()->all();
            $maxid = $id[0]['maxid']+1;
            if(!empty($module_info) && $module_info[0]['path']=='subsystem_manage/crud/location-subsystem'){

                $this->param = 'id='.$this->param;
            }elseif( !empty($module_info) && $module_info[0]['path']=='scheduled-report/crud/new-index'){
                $this->param = 'type=view&id='.$this->param;
            }else{
                $this->param = null;
            }
            $this->description = MyFunc::createJson($this->description);

            Yii::$app->db
                ->createCommand("INSERT INTO core.user_menu(id, description, icon, module_id, parent_id, idx, user_id, role_id,param) VALUES(".$maxid.",'".$this->description."','".$this->icon."',".$this->module_id.",".$this->parent_id.",".$this->idx.",".$user_id.",".$role_id.",'".$this->param."')")
                ->execute();
        }
        return true;
    }

    public static function getById($id){
        return SysMenu::findOne($id);
    }

    //将用户所拥有子系统的点位规则存入redis;
    public static function getUserMenuParam(){
        $idd ="rules".Yii::$app->user->id;

        $r_id = MyFunc::getRoleId();
        $u_id = Yii::$app->user->id;
        $param_arr = self::find()->select("param")->where("user_id = $u_id and role_id = $r_id and param like 'id=%'")->asArray()->all();

        $str = '';
        foreach($param_arr as $v){
            $str .= intval(ltrim($v['param'],"id=")).',';
        }
        $str = rtrim($str,",");
        $points = [];
        if($str!=''){
            $s_s_model = new SubSystem;
            $temp_points = $s_s_model->find()->select("points->'data'->>'points' as points")->where("location_id in ($str)")->asArray()->all();
            foreach($temp_points as $key => $value){
                $temp_points[$key]=explode(',',rtrim(ltrim($value['points'],'['),']'));
                $points = array_merge($points,$temp_points[$key]);
            }
            $points=array_unique($points);
            $points_str = '';
            foreach($points as $v){
                if(intval($v)){
                    $points_str .= intval($v).',';
                }else{
                    continue;
                }
            }
            $points_str = rtrim($points_str,",");

            if($points_str!=''){
                $a = "core.alarm_rule";
                $p = "core.point_event";
                $sql = "select $a.id from $p left join $a on $p.id = $a.event_id where $p.point_id in($points_str)";
                $arr = Yii::$app->db->createCommand($sql)->queryAll();

                $rule_arr = [];
                foreach($arr as $key => $value){
                    if($value['id']){
                        $rule_arr[]=$value['id'];
                    }
                }
                Yii::$app->cache->set($idd, json_encode($rule_arr));
            }
        }
        return false;
    }
}
