<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.alarm_rule".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $delay
 * @property string $description
 * @property string $level_sequence
 */
class AlarmRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.alarm_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'description', 'name', 'delay'], 'required'],
            [['event_id', 'delay'], 'integer'],
            ['is_system', 'boolean'],
            ['name', 'trim'],
            [['description', 'level_sequence'], 'string']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlarmEvent()
    {
        return $this->hasOne(AlarmEvent::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtomEvent()
    {
        return $this->hasOne(AtomEvent::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlarmLog()
    {
        return $this->hasMany(AlarmLog::className(), ['alarm_rule_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'delay' => Yii::t('app', 'Delay'),
            'description' => Yii::t('app', 'Alarm information'),
            'level_sequence' => Yii::t('app', 'Level Sequence'),
            'name' => Yii::t('app', 'Name'),
            'rule' => Yii::t('app', '$rule'),
        ];
    }

    /**
     *获取最近的100条log日志与对应的时间名称:
     * 对应关系:
     * alarm_log.alarm_rule_id -> alarm_rule.id -> event.name
     * @return array
     */
    public static function getRecentLog()
    {
        $log_arrs = [];
        $res = AlarmRule::find()->with([
                //关联log表取出最近的100条记录
                'alarmLog' => function($query)
                    {
                        $query->where('start_time between :start and :end', ['start' => $GLOBALS['start_time'], 'end' => $GLOBALS['end_time']])->OrderBy('start_time desc');
                    },
                //关联事件表 取出对应的名称
                'alarmEvent'
            ])->asArray()->all();

        foreach($res as $key => $value)
        {
            if(!empty($value['alarmLog']) && !empty($value['alarmEvent']))
            {
                foreach($value['alarmLog'] as $k => $v)
                {
                    $log_arrs[] = [strtotime($v['start_time'])* 1000, $value['alarmEvent']['id']];
                }

            }
        }
        array_multisort($log_arrs,  SORT_ASC);
        return $log_arrs;
    }

    /**
     * 报警规则的 创建 更新方法
     * @return bool
     */
    public function RuleSave()
    {
        $sequence = explode(',', trim($this->level_sequence, ','));
        foreach($sequence as $k =>$v){
            $sequence[$k] = intval($v);
        }
        if($this->isNewRecord)
        {
            $this->description = MyFunc::createJson($this->description, 'description', $this->is_system ? 'zh-cn' : '');
            $this->name = MyFunc::createJson($this->name, 'description', $this->is_system ? 'zh-cn' : '');
            //$this->remark = MyFunc::createJson($this->remark, 'description');
        }
        else
        {
            $this->description = MyFunc::updateJson($this->description, $this->oldAttributes['description'], $this->is_system ? 'zh-cn' : '');
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name'], $this->is_system ? 'zh-cn' : '');
            //$this->remark = MyFunc::updateJson($this->remark, $this->oldAttributes['remark']);
        }
        $this->level_sequence = MyFunc::createJson($sequence, 'list');
        return $this->insert();
    }

    /**
     * 利用 参数 创建 规则
     * @param $name
     * @param $level_sequence   报警序列
     * @return CompoundEvent
     */
    public static function getRuleObjBySystem($name, $description, $event_id, $delay, $level_sequence, $remark)
    {
        if(!$remark){$remark=$description;}
        if(!$delay){$delay=1800;}
        if(!$level_sequence){$level_sequence=5;}
        $alarm_rule = new AlarmRule();
        $alarm_rule->load([
                'name' => $name,
                'description' => $remark,
                'level_sequence' => $level_sequence,
                'event_id' => $event_id,
                'delay' => $delay,
                'is_system' => true],'');
        return $alarm_rule;
    }

    /**
     * 报警规则 绑定 判断
     * @param $is_only
     * @return mixed
     */
    public function bindingRuleEvent($is_only)
    {
        /*判断当前绑定类型 是否存在 相应事件事件*/
        /*如果
            是唯一的 查找系统生成除point_id 外的部分name
          否则
            找全部 name;
        */
        $system_prefix = $is_only ? substr($this->description, 0, strrpos($this->description, '-')) : $this->description;
        $is_exist = self::fuzzySearch($system_prefix);
        if(1)
        {
            return $this->RuleSave();
        }
        else
        {
            $model = self::findOne($is_exist->id);
            $model->load([
                    'name' => $this->name,
                    'description' => $this->description,
                    'level_sequence' => $this->level_sequence,
                    'event_id' => $this->event_id,
                    'delay' => 1800,
                    'is_system' => true],'');
            return $model->RuleSave();
        }
    }

    /**
     * 根据 系统生成 的名字进行模糊查找
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */

    public static function fuzzySearch($name)
    {
        return self::find()->where("description->'data'->>'zh-cn' like '" .$name."%'")->one();
    }

    /**
     * 根据 事件ID查找规则
     * @param $event_id
     * @return array|null|\yii\db\ActiveRecord
     */

    public static function findOneByEventId($event_id)
    {
        return self::find()->where("event_id = $event_id" )->one();
    }

}
