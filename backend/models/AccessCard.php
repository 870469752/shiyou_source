<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.access_card".
 *
 * @property integer $id
 * @property string $access_card_number
 * @property integer $access_card_holder_id
 * @property boolean $is_deleted
 */
class AccessCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.access_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_card_number', 'access_card_holder_id'], 'required'],
            [['access_card_holder_id'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['access_card_number'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_card_number' => '卡号
',
            'access_card_holder_id' => '持卡人编号
',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
