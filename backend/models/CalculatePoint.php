<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
/**
 * This is the model class for table "protocol.calculate_point".
 *
 * @property integer $id
 * @property boolean $is_shield
 * @property string $interval
 * @property string $name
 * @property string $protocol_id
 * @property string $last_update
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_upload
 * @property string $formula
 * @property string $dependence
 */
class CalculatePoint extends \yii\db\ActiveRecord
{
    public $category_id;
    public $filter_max_value;
    public $filter_min_value;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.calculate_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'formula'], 'required'],
            ['name', 'filter', 'filter' => 'trim'],
            [['is_shield', 'is_available', 'is_upload'], 'boolean'],
            [['name', 'base_history', 'formula'], 'string'],
            [['protocol_id', 'unit', 'filter_max_value', 'filter_min_value','interval', 'template_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'interval' => Yii::t('app', 'Record Interval'),
            'name' => Yii::t('app', 'Name'),
            'base_history' => Yii::t('app', 'Base History'),
            'is_available' => Yii::t('app', 'Is Available'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'formula' => Yii::t('app', '点位计算公式'),
            'dependence' => Yii::t('app', '计算点位依赖关系。本字段是一个整型数组，列出了本计算点位所依赖的点位。本字段非空。'),
            'unit' => Yii::t('app', '单位'),
            'filter_max_value' => Yii::t('app', 'Filter Max Value'),
            'filter_min_value' => Yii::t('app', 'Filter Min Value')
        ];
    }

    /**
     * 默认查询ID和名称并且是非屏蔽可用的
     * @return static
     */
    public static function baseSelect(){
        return static::find()->where(['is_shield' => false, 'is_available' => true, 'is_system' => false]);
    }


    /**
     * 添加 、更新 计算点位
     * @return bool
     */
    public function Calsave()
    {
        trim($this->name);
        $formula_ele = explode(',', $this->formula);
        //将点位的 单位 转化为 标准单位
        foreach($formula_ele as $key => $value)
        {
            //判断 是否是 点位 id
            if($id = intval(str_replace('p', '', $value)))
            {
                //如果是点位进行单位换算
                $point_unit = Point::getOfStandardUnit(['id' => $id]);
                if(isset($point_unit[0]['proportion']) && !empty($point_unit[0]['proportion']))
                {
                    //如果单位有 比例系数 就将比例系数 将比例系数加上
                    if($point_unit[0]['proportion'] != 1)
                    {
                        $formula_ele[$key] =   $point_unit[0]['proportion'] .'*'. $value;
                    }
                }
            }
        }
        //以 空格作为 两个元素间的 分隔
        $this->formula = implode(' ', $formula_ele);

        $this->protocol_id = '4';
        $this->update_timestamp = date("Y-m-d h:i:s");
        $this->base_value = 0;
        $_his = array(
            'start' => $this->update_timestamp,
            'end' => null,
            'value' => 0
        );
        if($this->isNewRecord)
        {
            //print_r(11111);die;
            $this->name = MyFunc::createJson($this->name);
            $this->base_history = MyFunc::createJson( $_his, 'base_history' );
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }

        //处理过滤值
        $filter_data = ['min' => $this->filter_min_value,
                        'max' => $this->filter_max_value];
        $this->value_filter = MyFunc::createJson($filter_data, 'value_filter');
        if($this->interval == 'Empty'){
            $this->interval = 300;
        }
        echo "<pre>";print_r($this->save());die;
        return ;
    }
}
