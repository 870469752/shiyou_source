<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.schema_config".
 *
 * @property integer $id
 * @property string $attrname
 * @property string $attribute
 * @property integer $type
 */
class schemaManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.time_mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'data'], 'default'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'data' => '时间配置',
        ];
    }
}
