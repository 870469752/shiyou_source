<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_energy_tree_rel".
 *
 * @property integer $energy_tree_id
 * @property integer $point_id
 */
class PointEnergyTreeRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_energy_tree_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['energy_tree_id', 'point_id'], 'required'],
            [['energy_tree_id', 'point_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'energy_tree_id' => Yii::t('app', 'Energy Tree ID'),
            'point_id' => Yii::t('app', 'Point ID'),
        ];
    }
}
