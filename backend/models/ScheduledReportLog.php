<?php

namespace backend\models;

use Yii;
use backend\models\ScheduledReport;
use backend\models\search\ScheduledReportSearch;

/**
 * This is the model class for table "core.scheduled_report_log".
 *
 * @property integer $id
 * @property integer $report_id
 * @property string $start_timestamp
 * @property string $end_timestamp
 * @property string $data
 * @property string $name
 *
 * @property ScheduledReport $report
 */
class ScheduledReportLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public static function tableName()
    {
        return 'core.schedule_report_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'start_timestamp', 'end_timestamp'], 'required'],
            [['report_id'], 'integer'],
            [['start_timestamp', 'end_timestamp', 'data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name'=>'报表名称',
            'report_id' => 'Report ID',
            'start_timestamp' => '开始时间',
            'end_timestamp' => '结束时间',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(ScheduledReport::className(), ['id' => 'report_id']);
    }

    public function getData($id)
    {
        return $this->find()->where(['id'=>$id])->asArray()->one();
    }

}
