<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use common\library\MyFormatter;
/**
 * This is the model class for table "core.alarm_level".
 *
 * @property integer $id
 * @property string $name
 * @property string $upgrade_delay
 * @property string $mail_receivers
 * @property string $sms_receivers
 * @property boolean $log_setting
 * @property boolean $upload_setting
 * @property boolean $mail_setting
 * @property boolean $sms_setting
 */
class AlarmLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.alarm_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','upgrade_delay'], 'required'],
            [['name', 'upgrade_delay'], 'string'],
            [['log_setting', 'upload_setting', 'mail_setting', 'sms_setting','notice_setting'], 'boolean'],
            [['mail_receivers', 'sms_receivers'], 'default'],
            [['notice_siren','notice_fade','notice_style'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Level'),
            'name' => Yii::t('app', 'Name'),
            'upgrade_delay' => Yii::t('app', 'Upgrade Delay(s)'),
            'mail_receivers' => Yii::t('app', 'Mail Receivers'),
            'sms_receivers' => Yii::t('app', 'Sms Receivers'),
            'log_setting' => Yii::t('app', 'Log Setting'),
            'upload_setting' => Yii::t('app', 'Upload Setting'),
            'mail_setting' => Yii::t('app', 'Mail Setting'),
            'sms_setting' => Yii::t('app', 'Sms Setting'),
            'notice_setting' => Yii::t('app', 'Notice Setting'),
            'notice_style' => Yii::t('app', 'Notice Style'),
            'notice_siren' => Yii::t('app', 'Notice Siren'),
            'notice_fade' => Yii::t('app', 'Notice Fade'),
        ];
    }

    /**
     * 报警等级 创建 更新方法
     * @return bool
     */
    public function AlarmLevelsave()
    {
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name);
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        $this->mail_receivers = MyFunc::createJson($this->mail_receivers, 'list');
        $this->sms_receivers = MyFunc::createJson($this->sms_receivers, 'list');


        return $this->save();
    }

    /**
     * 获取所有 报警等级
     * @return array
     */
    public static function findAllLevel()
    {
        $MF_model = new MyFormatter();
        $res = [];
        $data = self::find()->orderBy('id')->asArray()->all();
        //处理 数据
        foreach($data as $key => $value)
        {
            foreach($value as $k => $v)
            {
                if($k == 'id')
                {
                    $res[$key]['Level'] = $v;
                    continue;
                }
                else if($k == 'name')
                {
                    $res[$key][$k] = MyFunc::DisposeJSON($v);
                    continue;
                }
                else if($k == 'mail_receivers' || $k == 'sms_receivers')
                {
                    $res[$key][$k] = $MF_model->asUser($v);
                    continue;
                }else{
                    continue;
                }
            }
        }
        return $res;
    }

    /**
     *  报警等级信息
     * @return array
     */
    public static function findLevelInfo($id)
    {
        $select = "name->'data'->>'zh-cn' as name,notice_style,notice_siren,notice_fade";
        $result = self::find()->select($select)->where("id = $id")->asArray()->all();
        //echo "<pre>";print_r($result);die;
        return $result;
    }
}
