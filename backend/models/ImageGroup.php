<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.image_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $data
 */
class ImageGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'data'], 'string'],
            [['category_id', 'location_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'data' => 'Data',
            'category_id'=>'Category_id',
            'location_id'=>'Location_id',
        ];
    }
    public static function findImageGroupModel($id)
    {
        if (($model = ImageGroup::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new ImageGroup();
            return $model;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
