<?php

namespace backend\models;

use Yii;
use backend\models\EnergyCategory;
use backend\models\Location;

/**
 * This is the model class for table "core.point_category_config".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $point_id
 * @property boolean $is_skipped
 */
class PointCategoryConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_category_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'point_id'], 'required'],
            [['category_id', 'point_id'], 'integer'],
            [['is_skipped'], 'boolean'],
            [['category_id', 'point_id'], 'unique', 'targetAttribute' => ['category_id', 'point_id'], 'message' => 'The combination of Category ID and Point ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'point_id' => Yii::t('app', 'Point ID'),
            'is_skipped' => Yii::t('app', 'Is Skipped'),
        ];
    }

    /**
     * @param $category_id
     * @return array|\yii\db\ActiveRecord[]
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @date 2016-1-5
     */
    public static function getId($category_id)
    {
        return self::find()->select('point_id')->where(['category_id'=>$category_id, 'is_skipped' => false])->orderBy('point_id desc')->asArray()->column();
    }

    /**
     * @param $location_id_array
     * @param $category_id_array
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public static function getSpecificId($location_id_array, $category_id_array)
    {
        return self::find()->select('point_id')->where(['category_id'=>$location_id_array])->andWhere(['point_id'=>$category_id_array])->asArray()->column();
    }

    /**
     * @param $location_id
     * @param $energy_id
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function returnPointIds($location_id, $energy_id)
    {
        $category_id_array = [];
        $location_id_array = [];
        $point_ids = [];
        $category_id_array_temp = EnergyCategory::find()->select('id')->where("category_id @> '{".$energy_id."}'")->asArray()->column();
        $location_id_array = Location::find()->select('id')->where("category_id @> '{".$location_id."}'")->asArray()->column();
        $category_id_array = self::find()->select('point_id')->where(['category_id'=>$category_id_array_temp])->asArray()->column();

        if ($category_id_array && $location_id_array) {
            $point_ids = self::getSpecificId($location_id_array, $category_id_array);
        }
        return $point_ids;
    }
}
