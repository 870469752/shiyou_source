<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use backend\models\PointData;
use common\library\MyExport;
/**
 * This is the model class for table "core.export_config".
 *
 * @property string $point_id
 * @property string $statistics_type
 * @property string $statistics_range
 * @property string $path
 * @property integer $id
 * @property string $format
 * @property string $saving_mode
 */
class Export extends \yii\db\ActiveRecord
{
    //导出文件的方式
    public $method;

    public function init(){
        $this->statistics_type = 'day';
        $this->statistics_range = 'month';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.export_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'statistics_type', 'statistics_range'], 'required'],
            [['saving_mode'], 'string'],
            [['path'], 'string', 'max' => 255],
            [['statistics_type', 'statistics_range', 'format', 'statistics_mode', 'method'], 'string', 'max' => 32],
            ['path', 'default', 'value' => './'],
            ['format', 'default', 'value' => 'excel'],
            ['saving_mode', 'default', 'value' => 'save'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point_id' => Yii::t('app', '导出点位序列'),
            'statistics_type' => Yii::t('app', '数据统计时间类型'),
            'statistics_range' => Yii::t('app', '统计时间间隔'),
            'path' => Yii::t('app', '导出路径'),
            'id' => Yii::t('app', 'ID'),
            'format' => Yii::t('app', '导出格式'),
            'saving_mode' => Yii::t('app', '导出方式'),
            'statistics_mode' => Yii::t('app', '统计方式')
         ];
    }

    /**
     * 导出配置添加、处理、更新
     * @return bool
     */
    public function exprotSave()
    {
        $this->point_id = MyFunc::createJson($this->point_id, 'list');
        return $this->save();
    }

    /**
     * 自动导出 获取处理数据
     */
    public function autoExport()
    {
        //利用 point_data 方法 查询 导出的 点位数据
        $model_pd = new PointData();
        //初始化 pointdata 模型
        $model_pd->load([
                        'point_id' => $this->point_id,
                        'start_time' => $this->statistics_range[0],
                        'end_time' => $this->statistics_range[1],
                        'group' => $this->statistics_type,
                        'method' => $this->statistics_mode],'');
        //调用 行转列方法 获取 数据
        $data = $model_pd ->ColRowTransition()->asArray()->all();
        $title = $this->statistics_range[0].'至'.$this->statistics_range[1].'数据报表';
        MyExport::methodOfExport($data, $this->format, $this->saving_mode, $title,  $this->path, $this->method);
    }
}
