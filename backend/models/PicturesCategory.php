<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.pictures_category".
 *
 * @property integer $id
 * @property string $name
 */
class PicturesCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.pictures_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function getPictures()
    {
        return $this->hasMany(Pictures::className(), ['category_id' => 'id']);
    }
}
