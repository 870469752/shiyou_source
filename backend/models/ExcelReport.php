<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.excel_report".
 *
 * @property integer $id
 * @property string $name
 * @property integer $time_mode_id
 * @property string $report_title
 */
class ExcelReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.excel_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_mode_id'], 'integer'],
            [['report_title'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '报表名'),
            'time_mode_id' => Yii::t('app', '时间模式'),
            'report_title' => Yii::t('app', '报表表头'),
        ];
    }
}
