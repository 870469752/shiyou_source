<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;
/**
 * This is the model class for table "core.system_module".
 *
 * @property integer $id
 * @property string $action_info
 * @property string $additional
 * @property string $module
 */
class SystemModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.menu_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'required'],
            [['description'], 'string'],
            ['url', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', '模块描述'),
            'url' => Yii::t('app', '模块地址'),
//            'module' => Yii::t('app', '模块([模块名/]控制器)'),
        ];
    }

    public function moduleSave($id = null)
    {
        $self = [];
        if(!empty($id)){
            foreach($this->getModuleInfo($id) as $value){
                foreach($value as $key => $val){
                    $self[$key] = $val;
                }
            };
        }

        $this->description = MyFunc::createJson($this->description, 'description');
        $this->parent_id = !empty($id)?$self['parent_id']:1;
        $this->depth = !empty($id)?$self['depth']:2;
        $this->idx = !empty($id)?$self['idx']:$this->getMaxIdx();
        $this->icon = 'fa-folder-open';
        return $this->save();
    }

    /**
     * 获取系统所有模块
     * @return array    返回只为一个数组
     * 返回的数据形式：
     *  'module_name' => [
     *                      'action_info' => 'some_data',
     *                      'additional' => 'some_data',
     *                  ]
     */
    public static function getModuleInfo($id = null)
    {
        $res = self::find()->where(['id'=>$id])->asArray()->all();
        return BaseArrayHelper::index($res, 'module');
    }

    /**
     * get the max idx of default modules family which parent_id =1
     */
    public function getMaxIdx($id = 1){
        $temp = self::find()->select('idx')->where(['parent_id' => $id])->asArray()->all();
        $tmp = [];
        foreach($temp as $key => $value){
            $tmp[] = $value['idx'];
        }
        asort($tmp);
        $idx = array_pop($tmp);
        return $idx;
    }
}
