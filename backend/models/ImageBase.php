<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.image_base".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $path
 * @property string $timestamp
 */
class ImageBase extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.image_base';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp'], 'string'],
            [['name', 'type', 'path'], 'string', 'max' => 255],
//            [['image'], 'image', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('image', 'ID'),
            'name' => Yii::t('image', 'name'),
            'path' => Yii::t('image', 'Path'),
            'timestamp' => Yii::t('image', 'timestamp'),
            'image' => Yii::t('image', 'upload_image'),
        ];
    }
    /*
     * 获取所有底图
     * @return array
     */
    public static function getAllImageBase(){
        return ImageBase::find()->asArray()->all();
    }
    /**
     * 获得一个底图
     * @param int image_base_id
     * @return array
     */
    public static function getOneImageBase($id){
        return ImageBase::find()->select("id,path")->where(['id'=>$id])->asArray()->all();
    }
}
