<?php
namespace backend\models\form;

use backend\models\EnergyTree;
use backend\models\Point;
use backend\models\PointData;
use common\library\MyFunc;
use common\library\TimeHelper;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class EnergyTreeForm extends Model
{
    public $report_time_type;
    public $energy_id;
    public $date;

    private $_time;
    private $_point_id;
    private $_max_min;

    public function init(){
        $this->report_time_type = 'hour';
        $this->energy_id = EnergyTree::findFirstId();
        $this->date = date('Y-m-d', strtotime('-1 day'));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['report_time_type', 'string'],
            ['energy_id', 'integer'],
            ['date', 'string'],
        ];
    }

    public function getPointId(){
        if(!isset($this->_point_id)){
            $this->_point_id = MyFunc::arrayTwoOne(
                EnergyTree::findChildren($this->energy_id)
                    ->select('generate_point')
                    ->andWhere('generate_point is not null')->all(),
                'generate_point'
            );
        }
        return $this->_point_id;
    }

    public function getTime(){
        if(!isset($this->_time)){
            $this->_time = TimeHelper::StartEndTime($this->date);
        }
        return $this->_time;
    }

    public function getMaxMin($key = 'max')
    {
        if (!isset($this->_max_min)) {
            $this->_max_min = PointData::findTime([]);
        }
        return isset($this->_max_min[$key])?MyFunc::TimeFormat($this->_max_min[$key]):false;
    }

    public function getTotal(){
        return Point::findPointDataTotal(
            $this->getPointId(),
            $this->getTime()['start_time'],
            $this->getTime()['end_time']
        );
    }

    public function getData(){
        return Point::findOptimizePointData(
            $this->getPointId(),
            $this->getTime()['start_time'],
            $this->getTime()['end_time'],
            $this->report_time_type,
            'sum'
        );
    }

    public function attributeLabels()
    {
        return [
            'report_time_type' => \Yii::t('app', '报表时间类型'),
            'energy_id' => \Yii::t('app', '能源类型'),
            'date' => \Yii::t('app', '日期'),
        ];
    }
}
