<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-4-11
 * Time: 下午12:29
 * 分类数据描述： 最多只有两个分类， 代表数据表的 行 和 列
 */

namespace backend\models\form;


use backend\models\PointData;
use common\library\MyFunc;

use backend\models\Category;
use backend\models\Point;
use common\library\MyStringHelper;

class CategoryDataForm extends \yii\base\Model
{
    /**
     * @var 分类id  可能是一个 或 多个
     */
    public $category_ids;

    /**
     * @var 对比时间类型
     */
    public $time_type;

    /**
     * @var 对比时间
     */
    public $date_time;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_type','category_ids'], 'required'],
            ['date_time', 'string'],
            ['time_type', 'default', 'value' => 'day'],
            ['date_time', 'default', 'value' => MyFunc::toTimeFormat(date('Y-m-d H:i:s'), 'day')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'time_type' => Yii::t('app', '时间类型'),
            'category_ids' => Yii::t('app', '分类id'),
            'date_time' => Yii::t('app', '查询时间'),
        ];
    }


    public function getCategoryDataById($type, $_unit = false, $_hl = 0)
    {
        $this->time_type = isset($this->time_type) && !empty($this->time_type) ? $this->time_type : 'day';
        $this->date_time = isset($this->date_time) && !empty($this->date_time) ? $this->date_time : MyFunc::toTimeFormat(date('Y-m-d H:i:s'), $this->time_type);
        $category_arr = explode(',', $this->category_ids);
        if($_hl){
            $category_arr = MyFunc::Row_transform($category_arr);
        }
            if($type == 'chart') {
                $category_arr = MyFunc::Row_transform($category_arr);
            }

            //将 单个分类展示和 一组分类展示分开
            if(isset($category_arr[0]) && isset($category_arr[1]) && !empty($category_arr[0]) && !empty($category_arr[1])) {
                $res_data = $this->getPointDataByCategoryIds($category_arr[0], $category_arr[1], $_unit);
            }else {
                $point_ids = Category::findCategoryPoint([[$this->category_ids]]);
                if($point_ids) {
                $res_data = PointData::statisticalByTime([$this->date_time], $this->time_type, $point_ids, 'other'); //  默认时间格式化方法
                //获取名称
                if($_unit && isset($point_ids[0])){
                    $category_unit = Point::findPointUnit($point_ids[0])[0]['unit']['name'];
                }
            }
                if(!isset($res_data)) {
                    return false;
                }
            $res_data = array_merge($res_data, ['unit' => $category_unit]);
        }
        return $res_data;
    }

    /**
     * 查询一组 category id 对应的点位数据
     * @param $id_1  分类id
     * @param $id_2  分类id
     * @param $_unit 是否需要点位
     * @return array 返回值
     */
    public function getPointDataByCategoryIds($id_1, $id_2, $_unit)
    {
        $category_data = [];
        $category_unit = '';
        $category_1 = Category::getSubId($id_1);
        $category_2 = Category::getSubId($id_2);
        $category_arr1 = !empty($category_1) ? MyFunc::analyse_json($category_1) : [Category::getCategoryById($id_1)];
        $category_arr2 = !empty($category_2) ? MyFunc::analyse_json($category_2) : [Category::getCategoryById($id_2)];
        foreach($category_arr1 as $c_1) {
            foreach($category_arr2 as $c_2) {
                //将所有分类统计的数据放入数组中
                $point_ids = Category::findCategoryPoint([[$c_1['id']], [$c_2['id']]]);
                if(!empty($point_ids)) {
                    $point_data = PointData::statisticalByTime([$this->date_time], $this->time_type, $point_ids)['data'];
                    //再次统计整天的数据
                    $statistic = 0;
                    foreach($point_data as $value) {
                        $statistic += $value['value'];
                    }
                    if(!$category_unit && $_unit && isset($point_ids[0])){
                        $category_unit = Point::findPointUnit($point_ids[0])[0]['unit']['name'];
                    }
                }else {
                    $statistic = 'NULL';
                }
                $category_data[$c_1['name']][$c_2['name']] = $statistic;
            }
        }
        return $_unit ? ['category_data' => $category_data, 'unit' => $category_unit] : $category_data;
    }

    /**
     * 分类统计 按分类进行数据处理
     * @return array  返回数据数组 和对应的分类名称数组
     */
    public function statisticDataById($offset = '', $limit = '', $time_format = true)
    {
        //由于有些调用并没有进行load 所以查询前再次进行验证
        $this->time_type = isset($this->time_type) && !empty($this->time_type) ? $this->time_type : 'day';
        $this->date_time = isset($this->date_time) && !empty($this->date_time) ? $this->date_time : MyFunc::toTimeFormat(date('Y-m-d H:i:s'), $this->time_type);
        $category_info = Category::getCategoryById($this->category_ids);
        //取出所查询节点的儿子节点
        $sub_info = Category::getSubId($this->category_ids, $offset, $limit);
        $category_data_info = ['data' => [], 'name' => []];
        //如果有子类就展示当前分类下的子类统计数据
        if($sub_info) {
            foreach($sub_info as $key => $value) {
                $point_ids = Category::findCategoryPoint([[$value['id']]]);
                //目前只将所有点位的数据总和
                $category_data = PointData::categoryStatistical($this->date_time, $this->time_type, $point_ids, $time_format);
                $category_data_info['data'][$key] = isset($category_data['data'][0]['value']) ? intval($category_data['data'][0]['value']) : Null;
                $category_data_info['name'][$key] = MyFunc::DisposeJSON($value['name']);
            }
        } else {
            //如果没有就展示自己关联点位的数据统计
            $point_ids = Category::findCategoryPoint([[$this->category_ids]]);
            $category_data = PointData::categoryStatistical($this->date_time, $this->time_type, $point_ids, $time_format);
            $category_data_info['data'][] = isset($category_data['data'][0]['value']) ? intval($category_data['data'][0]['value']) : Null;
            $category_data_info['name'][] = $category_info['name'];
        }
        return ['category_data_info' => $category_data_info,
                'category_info' => $category_info];
    }


    /**
     *  按时间统计分类数据
     */
    public function statisticDataByDate()
    {
        $category_info = Category::getCategoryById($this->category_ids);
        //取出所查询节点的儿子节点
        $sub_info = Category::getSubId($this->category_ids);
        if(!empty($sub_info)) {
            $sub_category = [];
            foreach($sub_info as $key => $value) {
                $sub_category[0][$key] = $value['id'];
            }
        }
        else {
            //如果没有子分类就展示自己的
            $sub_category = [[$this->category_ids]];

        }
        //获取点位
        $point_ids = Category::findCategoryPoint($sub_category);
        $category_data = PointData::categoryStatistical($this->date_time, $this->time_type, $point_ids);

        return [
            'category_data' => !empty($category_data) ? $category_data : [],
            'category_info' => $category_info
        ];
    }

    /**
     * 获取子类点位以时间为统计的 highcharts 和 table的数据
     * @return array
     */
    public function statisticDataBySub()
    {
        $category_info = Category::getCategoryById($this->category_ids);
        //取出所查询节点的儿子节点
        $sub_info = Category::getSubId($this->category_ids);
        $category_data_info = [];
        $table_data = [];
        //如果有子类就展示当前分类下的子类统计数据
        if($sub_info) {
            foreach($sub_info as $key => $value) {
                $point_ids = Category::findCategoryPoint([[$value['id']]]);
                //目前只将所有点位的数据总和
                $category_data = PointData::categoryStatistical($this->date_time, $this->time_type, $point_ids);
                $data = [];
                $data['name'] = MyFunc::DisposeJSON($value['name']);
                //处理 highchart图 数据
                foreach($category_data['data'] as $key => $value) {
                    $timestamp = MyStringHelper::getTimeByType($value['_timestamp'], $this->time_type);
                    $data['data'][] = [$timestamp, floatval(number_format($value['value'], 2 ,'.', ''))];
                }
                //处理table数据
                foreach($category_data['data'] as $key => $value) {
                    $table_data['name'][$data['name']] = $data['name'];
                    $table_data['data'][$value['_timestamp']][] = floatval(number_format($value['value'], 2 ,'.', ''));
                }

                $category_data_info[] = $data;
            }
        } else {
            //如果没有就展示自己关联点位的数据统计
            $point_ids = Category::findCategoryPoint([[$this->category_ids]]);
            $category_data = PointData::categoryStatistical($this->date_time, $this->time_type, $point_ids);
            //处理 highchart图 数据
            $data = [];
            $data['name'] = $category_info['name'];
            foreach($category_data['data'] as $key => $value) {
                $timestamp = MyStringHelper::getTimeByType($value['_timestamp'], $this->time_type);
                $data['data'][] = [$timestamp, floatval(number_format($value['value'], 2 ,'.', ''))];
            }
            //处理table数据
            foreach($category_data['data'] as $key => $value) {
                $table_data['name'][$data['name']] = $data['name'];
                $table_data['data'][$value['_timestamp']][] = floatval(number_format($value['value'], 2 ,'.', ''));
            }

            $category_data_info[] = $data;
        }
            //进一步处理 数据表所需数据 将时间作为 数组的值 插入数组的首位
        if(isset($table_data['name']) && isset($table_data['data'])) {
            foreach($table_data['data'] as $key => $value) {
                array_unshift($table_data['data'][$key],$key);
            }
            //进一步处理 数据表所需数据  将存储分类名称的数组变为索引数组
            $table_data['name'] = array_values($table_data['name']);
            $table_data['data'] = array_values($table_data['data']);
        } else {
            $table_data = ['name' => '', 'data' => ''];
        }
        return ['category_data_info' => $category_data_info,
                'table_data' => $table_data,
                'category_info' => $category_info];
    }
}