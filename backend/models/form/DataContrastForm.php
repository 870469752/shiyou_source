<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 上午10:53
 *
 * 点位数据对比表单模型
 */

namespace backend\models\form;

use backend\models\PointData;
use common\library\MyFunc;

class DataContrastForm extends \yii\base\Model
{
    /**
     * @var 对比点位id 数组
     */
    public $point_ids;

    /**
     * @var 对比时间类型
     */
    public $time_type;

    /**
     * @var 对比起始时间数组
     */
    public $date_time;

//    /**
//     * @var 对比时间数
//     */
//    public $interval_num;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_type','point_ids'], 'required'],
            ['date_time', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'time_type' => Yii::t('app', '对比类型'),
            'point_ids' => Yii::t('app', '对比点位'),
            'interval_num' => Yii::t('app', '对比时段'),
            'date_time' => Yii::t('app', '起始时间'),
        ];
    }

    /**
     * 利用 传递的参数来 获取对比图数据
     */
    public function getCharData()
    {
        //从point_data表中获取数据

       // $char_data = PointData::statisticalByTime($this->date_time, $this->time_type, $this->point_ids);
        $char_data = PointData::statisticalByTime($this->date_time, $this->time_type,173);
        foreach($char_data['data'] as $key => $value){
            $format_time = Myfunc::toTimeFormat($value['_timestamp'], $this->time_type);
            unset($value['point_id']);
            unset($value['id']);
            if($this->time_type != 'week'){
                $group_data[$format_time][] = [strtotime($value['_timestamp']) * 1000, floatval($value['value'])];
            }else{
                foreach($char_data['format'] as $key =>  $time){

                   if($time['start'] < $format_time &&  $format_time < $time['end']){
                       $group_data['第'.($key + 1).'个星期'][] = [strtotime($value['_timestamp']) * 1000, floatval($value['value'])];
                    }
                }
            }
        }
        return isset($group_data) ? ['data' => $group_data, 'dataProvider' => $char_data['dataProvider'], 'time_format' => $char_data['format']]
                                    : ['data' => '', 'dataProvider' => $char_data['dataProvider'], 'time_format' => $char_data['format']];
    }
} 