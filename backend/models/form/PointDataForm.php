<?php

namespace backend\models\form;

use backend\models\Point;
use backend\models\PointData;
use common\library\MyFunc;
use common\library\TimeHelper;
use Yii;


class PointDataForm extends \yii\base\Model
{

    public $value;
    public $timestamp;
    // 点位ID 数组
    public $point_id;
    // 报表类型 日月年
    public $report_type = '';
    // 日期
    public $date = '';
    public $start_time;
    public $end_time;
    // 聚合函数 max, min, avg, sum
    public $fetch_value_method = '';
    //选择分组的形式
    public $group_default = false;
    // 最大最小时间
    private $_time;

    public function init()
    {
        $this->point_id = [Point::findFirstId()];
    }

    public function getPoint()
    {
        return $this->hasOne(Point::className(), ['id' => 'point_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['point_id', 'integer'],
            [['report_type', 'fetch_value_method', 'start_time', 'end_time', 'group_default'], 'string'],
            [['date'], 'date'],
        ];
    }

    public function getTime()
    {
        if ($this->_time === NULL) {
            $this->_time = PointData::findTime([]);
            $this->_time['min'] = MyFunc::TimeFormat($this->_time['min']);
            $this->_time['max'] = MyFunc::TimeFormat($this->_time['max']);
        }
        return $this->_time;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point_id' => \Yii::t('app', '选择点位'),
            'report_type' => \Yii::t('app', '报表类型'),
            'fetch_value_method' => \Yii::t('app', '取值方式'),
            'date' => \Yii::t('app', '选择日期'),
        ];
    }

    public function fields()
    {
        return [
            'point_id',
            'report_type',
            'start_time' => function () {
                    return $this->start_time ? $this->start_time : TimeHelper::StartEndTime($this->date, 'start_time');
                },
            'end_time' => function () {
                    return $this->end_time ? $this-> end_time : TimeHelper::StartEndTime($this->date, 'end_time');
                },
            'fetch_value_method',
            'group_default'
        ];
    }

    public function findTableData()
    {
        $fields = $this->toArray();
        return PointData::ColRowTransition(
            $fields['point_id'],
            $fields['start_time'],
            $fields['end_time'],
            $fields['report_type'],
            $fields['fetch_value_method'],
            $fields['group_default']
        );
    }

    public function findChartData()
    {
        $fields = $this->toArray();
        return Point::findOptimizePointData(
            $fields['point_id'],
            $fields['start_time'],
            $fields['end_time'],
            $fields['report_type'],
            $fields['fetch_value_method']
        );
    }

    /**
     * 点位查看数据 提取
     * @param $id
     * @param $start_time
     * @param $end_time
     * @return static
     */
    public function findIdTimeRange($id, $start_time, $end_time)
    {
        return PointData::find()->select(['timestamp as _timestamp', 'value as _value'])
                                ->where(['point_id' => $id])
                                ->andWhere(['between', 'timestamp', $start_time, $end_time]);
    }
}

