<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.access_card_holder".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 */
class AccessCardHolder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.access_card_holder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '持卡人编号',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }
}
