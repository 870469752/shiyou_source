<?php

namespace backend\models;

use Yii;
use yii\helpers\Json;
/**
 * This is the model class for table "core.time_mode".
 *
 * @property integer $id
 * @property string $name
 * @property string $repeat
 * @property string $interval
 * @property string $timestamp
 */
class TimeMode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.time_mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'interval', 'timestamp'], 'string'],
            [['repeat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('time', 'ID'),
            'name' => Yii::t('time', '模式名'),
            'repeat' => Yii::t('time', '重复时间'),
            'interval' => Yii::t('time', '划分间隔'),
            'timestamp' => Yii::t('time', 'Timestamp'),
        ];
    }
    /*
     * 保存,数据处理好恶心！
     */
    public function saveTimeMode($data)
    {
        $this->name = $data['name'];
        $this->repeat = $data['repeat'];
        $tmp = explode(';',$data['interval']);
        $tmp_day = explode(';',$data['interval_day']);
        $interval_day = [];
        foreach($tmp_day as $k => $v)
        {
            if(strlen($v))
            {
                $t1 = explode(',',$v);
                @$interval_day[] = ['title'=>$t1[0],'start'=>$t1[1],'end'=>$t1[2]];
            }
        }
        $tmp_inter = [];
        foreach($tmp as $key=>$value)
        {
            if(strlen($value))
            {
                $t2 = explode(',',$value);
                @$tmp_inter[] = ['title'=>$t2[0],'start'=>$t2[1],'end'=>$t2[2]];
            }
        }
        $_data = [
            'type' => 'day',
            'time_range' => $tmp_inter,
            'interval_day' => $interval_day
        ];
        $this->interval = Json::encode([
            'data_time' => 'time',
            'data' => $_data
        ]);
        $_model = clone $this;
        return $_model->save();
    }
    /*
     * 查询多个字段用“,”隔开
     * @param $col string
     */
    public static function getOneDate($col)
    {
        return self::find()->select($col)->asArray()->all();
    }
}
