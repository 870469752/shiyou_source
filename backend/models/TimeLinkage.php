<?php

namespace backend\models;

use Yii;

class TimeLinkage extends \yii\db\ActiveRecord
{
    public $time_mode_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.time_linkage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'related_id', 'data'], 'string'],
            [['type', 'time_mode_id'], 'required']
//            ,
//            [['type', 'time_mode_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'related_id' => 'Related ID',
            'time_mode_id' => 'Time Mode',
            'data' => 'Data',
        ];
    }
}
