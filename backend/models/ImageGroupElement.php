<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.image_group_element".
 *
 * @property integer $id
 * @property integer $image_group_id
 * @property integer $binding_type
 * @property string $binding_id
 * @property string $config
 * @property integer $element_id
 */
class ImageGroupElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_group_id'], 'required'],
            [['image_group_id', 'binding_type', 'element_id'], 'integer'],
            [['config'], 'string'],
            [['binding_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_group_id' => 'Image Group ID',
            'binding_type' => 'Binding Type',
            'binding_id' => 'Binding ID',
            'config' => 'Config',
            'element_id' => 'Element ID',
        ];
    }

    /*  In

    $data= [
       'image_group_id'=>1,    //id
        'element_id'=>1,        //key
    ];

    out
       ImageGroupElement 模型
 */
    public static function findImageGroupElementModelByData($data)
    {
        $image_group_id=$data['image_group_id'];
        $element_id=$data['element_id'];
        $data=ImageGroupElement::find()
            ->select('*')
            ->where(['image_group_id'=>$image_group_id,'element_id'=>$element_id])->asArray()->all();

        if(empty($data)){
            $model=new ImageGroupElement();
            return $model;
        }
        else {
            $id = $data[0]['id'];

            return ImageGroupElement::findImageGroupElementModelById($id);
        }
    }

    public static function findImageGroupElementModelById($id)
    {
        if (($model = ImageGroupElement::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new ImageGroupElement();
            return $model;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
