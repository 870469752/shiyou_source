<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "project.sc_address".
 *
 * @property integer $id
 * @property string $name
 * @property string $ip
 * @property integer $port
 */
class ScAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project.sc_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'string'],
            [['port'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'ip' => Yii::t('app', 'Ip'),
            'port' => Yii::t('app', '端口'),
        ];
    }
}
