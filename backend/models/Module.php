<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;

/**
 * This is the model class for table "core.module".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $path
 * @property string $action_list
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'action_list'], 'string'],
            [['path'], 'required'],
            [['path'], 'string', 'max' => 127]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'path' => 'Path',
            'action_list' => 'Action List',
        ];
    }

    /**
     * 模块 创建 更新方法
     * @return bool
     */
    public function Modulesave()
    {
        $action_list_arr = [];
        $record_arr = explode(';', trim($this->action_list, ';'));              //将 action_list 拆开
        foreach($record_arr as $key => $value){
            $value_arr = explode(',', trim($value, ','));
            $action_list_arr[$key]['name']=$value_arr[0];
            $action_list_arr[$key]['id']=$value_arr[1];
            $action_list_arr[$key]['description']=$value_arr[2];
        }
        //echo "<pre>";print_r($action_list_arr);die;
        $this->action_list = MyFunc::createJson($action_list_arr, 'action_info');
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name);
            $this->description = MyFunc::createJson($this->description);
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
            $this->description = MyFunc::updateJson($this->description, $this->oldAttributes['description']);
        }
        return $this->save();
    }

    //查找所有模块（系统菜单处用）
    public static function findModule()
    {
        return self::find()->select("id,name")->orderBy('id asc')->asArray()->all();
    }

    //根据module_id查找模块所对应的信息
    public static function findModuleById($id)
    {
        return self::find()->select("id,name->'data'->>'zh-cn' as name,path")->where("id = $id")->asArray()->all();
    }
}
