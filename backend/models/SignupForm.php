<?php
namespace backend\models;

use common\library\MyFunc;
use common\models\User;
use yii\base\Model;
use Yii;
use common\library\Tool\Upload;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $phone_number;
    public $head_portrait;
    public $role_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['phone_number', 'required'],
            ['phone_number', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This phone number address has already been taken.'],

            ['head_portrait', 'file'],
            ['role_id', 'required'],
            ['role_id', 'filter', 'filter' => 'trim'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $this->role_id = MyFunc::createJson($this->role_id, 'list');
        $this->head_portrait = Upload::uploadByFile($this, 'head_portrait');
        if ($this->validate()) {
            return User::create($this->attributes);
        }
        return null;
    }

    /**
     * 根据user_id  修改 对应用户的信息
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function updateByid($id)
    {
        $this->role_id = MyFunc::createJson($this->role_id, 'list');
        $this->head_portrait = Upload::uploadByFile($this, 'head_portrait');
        $user = User::findIdentity($id);
        $user->setPassword($this->password);
        $user->setAttributes($this->attributes);
        $user->generateAuthKey();
        return $user->save();
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('app', 'Username'),
            'password' => \Yii::t('app', 'Password'),
            'email' => \Yii::t('app', 'Email'),
            'phone_number' => \Yii::t('app', 'Phone Number'),
            'head_portrait' => \Yii::t('app', 'Head Portrait'),
            'role_id' => \Yii::t('app', 'Role assignment'),
        ];
    }
}
