<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.database_backup_log".
 *
 * @property integer $id
 * @property string $date_id
 * @property string $timestamp
 * @property string $data_start_id
 * @property string $data_end_id
 * @property integer $task_id
 */
class DatabaseLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.database_backup_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_id', 'data_start_id', 'data_end_id'], 'required'],
            [['timestamp'], 'string'],
            [['data_start_id', 'data_end_id', 'task_id'], 'integer'],
            [['date_id'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date_id' => Yii::t('app', 'Date ID'),
            'timestamp' => Yii::t('app', 'Creation time'),
            'data_start_id' => Yii::t('app', 'Data Start ID'),
            'data_end_id' => Yii::t('app', 'Data End ID'),
            'task_id' => Yii::t('app', 'Task ID'),
        ];
    }
}
