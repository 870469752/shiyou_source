<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modbus_config".
 *
 * @property integer $id
 * @property integer $type
 * @property string $rtu_port
 * @property integer $rtu_baud_rate
 * @property integer $rtu_parity
 * @property integer $rtu_data_bits
 * @property integer $rtu_stop_bits
 * @property string $tcp_ip
 * @property integer $tcp_port
 * @property integer $is_shield
 * @property integer $is_available
 */
class Modbus_config extends \yii\db\ActiveRecord
{
    public static function  getDb(){
        return Yii::$app->get('dbsql');
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modbus_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'rtu_baud_rate', 'rtu_parity', 'rtu_data_bits', 'rtu_stop_bits', 'tcp_port', 'is_shield', 'is_available'], 'integer'],
            [['rtu_port', 'tcp_ip'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('moubus', 'ID'),
            'type' => Yii::t('moubus', 'Type'),
            'rtu_port' => Yii::t('moubus', 'Rtu Port'),
            'rtu_baud_rate' => Yii::t('moubus', 'Rtu Baud Rate'),
            'rtu_parity' => Yii::t('moubus', 'Rtu Parity'),
            'rtu_data_bits' => Yii::t('moubus', 'Rtu Data Bits'),
            'rtu_stop_bits' => Yii::t('moubus', 'Rtu Stop Bits'),
            'tcp_ip' => Yii::t('moubus', 'Tcp Ip'),
            'tcp_port' => Yii::t('moubus', 'Tcp Port'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_available' => Yii::t('app', 'Is Available'),
        ];
    }
}
