<?php

namespace backend\models;

use Yii;
use common\library\Tool\Upload;
use common\library\MyFunc;
/**
 * This is the model class for table "core.image".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $width
 * @property integer $height
 * @property string $url
 * @property string $phase_config
 * @property boolean $is_system
 */
class Image extends \yii\db\ActiveRecord
{

    public $default;
    public $active;
    public $inactive;
    public $Gif;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phase_config','url'], 'string'],
            [['type', 'width', 'height'], 'integer'],
            [['width', 'height', 'url'], 'required'],
            [['is_system'], 'boolean'],
            //[['url'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'type' =>  Yii::t('app', 'Type'),
            'width' => Yii::t('app','Width'),
            'height' => Yii::t('app', 'Height'),
            'url' => Yii::t('app', 'Url'),
            'phase_config' => Yii::t('app', 'Phase Config'),
            'is_system' => Yii::t('app', 'Is System'),
            'default'=>Yii::t('app', 'Default'),
            'active'=>Yii::t('app', 'Active'),
            'inactive'=>Yii::t('app', 'Inactive'),
        ];
    }

    public static function findAllImage(){
        return self::find()->asArray()->all();
    }

    public static function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new Image();
            return $model;
        }
    }
    public function DefaultSave(){
        if(is_null(json_decode($this->name))){
            $this->name = MyFunc::createJson($this->name);
        }
        $this->url=Upload::uploadByFile($this, 'default');
        $this->default=Upload::uploadByFile($this, 'default');
        $this->active=Upload::uploadByFile($this, 'active');
        $this->inactive=Upload::uploadByFile($this, 'inactive');
        $this->Gif=Upload::uploadArrayFile($this,'Gif');
        $this->width=600;
        $this->height=800;

        //把default  avtive inactive  三个组成一个phase_config
        //Upload::uploadByFile($this, 'default');
        $phase_config=[
            'data_type'=>'image_phase_config',
            'data'=>[
                'default'=>$this->default,
                'active'=>$this->active,
                'inactive'=>$this->inactive,
                'Gif'=>$this->Gif,
            ]
        ];
        $this->phase_config=json_encode($phase_config);
        $result=$this->save();
        return  $result;

    }

}
