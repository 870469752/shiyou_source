<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.equipment_image_group_relation".
 *
 * @property integer $id
 * @property integer $equipment_id
 * @property integer $image_group_id
 * @property string $image_group_x
 * @property string $image_group_y
 * @property integer $image_group_width
 * @property integer $image_group_height
 * @property integer $point_id
 * @property string $dynamic_rule
 * @property string $dynamic_value
 */
class EquipmentImageGroupRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.equipment_image_group_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipment_id', 'image_group_id'], 'required'],
            [['equipment_id', 'image_group_id', 'image_group_width', 'image_group_height', 'point_id'], 'integer'],
            [['image_group_x', 'image_group_y', 'dynamic_value'], 'string'],
            [['dynamic_rule'], 'string', 'max' => 255]
        ];
    }

    /**
     * 比较规则
     * @var array
     */
    private $_dynamic_rule=[
        '等于','不等于','大于','小于','大于等于','小于等于',
    ];
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('image', 'ID'),
            'equipment_id' => Yii::t('image', '设备'),
            'image_group_id' => Yii::t('image', '组图'),
            'image_group_x' => Yii::t('image', 'Image Group X'),
            'image_group_y' => Yii::t('image', 'Image Group Y'),
            'image_group_width' => Yii::t('image', 'Image Group Width'),
            'image_group_height' => Yii::t('image', 'Image Group Height'),
            'point_id' => Yii::t('image', '点位名'),
            'dynamic_rule' => Yii::t('image', '比较规则'),
            'dynamic_value' => Yii::t('image', '比较值'),
        ];
    }
    /**
     * save
     *
     */
    public static function saveImageGroup($data){
        $sql = "UPDATE core.equipment_image_group_relation
              SET image_group_x={$data['image_group_x']},image_group_y={$data['image_group_y']},image_group_width={$data['image_group_width']},image_group_height={$data['image_group_height']} WHERE id={$data['id']};";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }
    /**
     * 获得相应设备的组图
     * @param int equipment_id
     * @return array
     */
    public static function getOneImageGroup($id){
        $img_group_id = EquipmentImageGroupRelation::find()->where("equipment_id=$id")->asArray()->all();
        $img_group = [];
        foreach($img_group_id as $v){
            $info = ImageGroup::getOneImageGroup($v['image_group_id']);
            foreach($info as $val){
                $tmp['id'] = $v['id'];
                $tmp['group_id'] = $val['id'];
                $tmp['group_name'] = $val['name'];
                $tmp['static_path'] = empty($val['static_path'])?'uploads/0.jpg':$val['static_path'];
                $tmp['dynamic_path'] = empty($val['dynamic_path'])?'uploads/0.jpg':$val['dynamic_path'];
                $tmp['width'] = empty($v['image_group_width'])?40:$v['image_group_width'];
                $tmp['height'] = empty($v['image_group_height'])?40:$v['image_group_height'];
                $tmp['group_x'] = $v['image_group_x'];
                $tmp['group_y'] = $v['image_group_y'];
                $tmp['point_id'] = $v['point_id'];
                $tmp['dynamic_rule'] = $v['dynamic_rule'];
                $tmp['dynamic_value'] = $v['dynamic_value'];
                $img_group[] = $tmp;
            }
        }
        return $img_group;
    }
}
