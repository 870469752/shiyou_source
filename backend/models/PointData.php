<?php

namespace backend\models;

use common\library\MyStringHelper;
use Yii;
use common\library\MyFunc;

/**
 * This is the model class for table "core.point_data".
 *
 * @property string $id
 * @property array $point_id
 * @property string $value
 * @property string $timestamp
 *
 * @property Point $point
 */
class PointData extends \yii\db\ActiveRecord
{
    /**
     * @var 点位ID
     */
    public $point_id;
    /**
     * @var 开始时间
     */
    public $start_time;
    /**
     * @var 结束时间
     */
    public $end_time;
    /**
     * @var 分组方式 year, month, day, hour
     */
    public $group;
    /**
     * @var 聚合函数 max, min, avg, sum
     */
    public $method;

    private $_time = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time'], 'date'],
            ['point_id', 'default', 'value' => [Point::findFirstId()]],
            ['start_time', 'default', 'value' => $this->getTime('min')],
            ['end_time', 'default', 'value' => $this->getTime('max')],
            ['group', 'string'],
            ['method', 'string'],
        ];
    }

    public function getTime($key = 'max')
    {
        if ($this->_time === false) {
            $this->_time = PointData::findTime([]);
        }
        return isset($this->_time[$key])?MyFunc::TimeFormat($this->_time[$key]):false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'point' => Yii::t('app', '点位名称'),
            'id' => Yii::t('app', '点位数据编号'),
            'value' => Yii::t('app', '点位数值'),
            'timestamp' => Yii::t('app', 'Time'),
            'start_time' => \Yii::t('app', 'Start Time'),
            'end_time' => \Yii::t('app', 'End Time'),
            'point_id' => \Yii::t('app', 'Point'),
            'group' => \Yii::t('app', '报表类型'),
            'method' => \Yii::t('app', '报表取值'),
            'date' => \Yii::t('app', '日期'),
        ];
    }

    public function fields()
    {
        return [
            'point_id',
            'start_time',
            'end_time' => function () {
                    // 如果已经有时分秒了就不加了
                    if(!strpos($this->end_time,':')){
                        return date('Y-m-d', strtotime($this->end_time)).' 23:59:59';
                    }
                    return $this->end_time;
                },
            'group',
            'method',
        ];
    }

    /**
     * 重写框架方法，如果值不存在就获取FALSE，而不是抛出异常
     * @param string $name
     * @return bool|mixed
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            // read property, e.g. getName()
            return $this->$getter();
        } else {
            return false;
        }
    }

    /**
     * 重写框架方法，如果设置的值从来没设置过，绕过行为直接设置
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            // set property
            $this->$setter($value);

            return;
        } elseif (strncmp($name, 'on ', 3) === 0) {
            // on event: attach event handler
            $this->on(trim(substr($name, 3)), $value);

            return;
        } elseif (strncmp($name, 'as ', 3) === 0) {
            // as behavior: attach behavior
            $name = trim(substr($name, 3));
            $this->attachBehavior($name, $value instanceof Behavior ? $value : Yii::createObject($value));

            return;
        } else {

            $this->$name = $value;

            return;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(Point::className(), ['id' => 'point_id']);
    }

    /**
     * 查询数据库内的最大时间或最小时间
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findTime($func)
    {
        $field = 'timestamp';
        if(is_array($func)){
            return static::find()->select(['max' => 'max('.$field.')','min' => 'min('.$field.')'])->asArray()->one();
        }
        return static::find()->$func($field);
    }

    /**
     * 基础字段
     * @return static
     */
    public static function BaseSelect()
    {
        return static::find()->select(['timestamp', 'point_id', 'value']);
    }

    /**
     * 查找某个点位的值
     * @param $id
     * @return mixed
     */
    public static function findPointId($id)
    {
        return static::BaseSelect()->andWhere(['point_id' => $id]);
    }

    /**
     * 查找某点位在相应时间范围内数值
     * @param array $id
     * @param $start_time
     * @param $end_time
     * @return mixed
     */
    public static function findIdTimeRange($id, $start_time, $end_time)
    {
        return static::findPointId($id)->andWhere(['between', 'timestamp', $start_time, $end_time]);
    }

    /**
     * 大数据时按照ID优化数据个数
     * @param $query
     * @param $start_time
     * @param $end_time
     */
    public static function optimizeData($query, $start_time, $end_time, $group, $method){
        $method = MyStringHelper::getRelMethod($method);
        $group = MyStringHelper::getRelGroup($group);
        // 如果选择了汇总方式
        if($group !== ['timestamp' => 'max(timestamp)']){
            $sql_formula = [$group['timestamp'], 'point_id'];
        } else{ // 如果没有限制1000个点
            $optimizing_interval = floor($query->count()/YII::$app->params['HighChartsPointMax']);
            // 每一个点位都按间隔分组
            $sql_formula = 'floor(id/' . $optimizing_interval.'), point_id';
        }
        $query->select(array_merge($group, ['point_id'], $method))
            ->andWhere(['between', 'timestamp', MyFunc::TimeFormat($start_time), MyFunc::TimeFormat($end_time)])
            ->groupBy($sql_formula)
            ->orderBy('timestamp');
    }

    /**
     * 计算总数
     * @param $query
     * @param $start_time
     * @param $end_time
     */
    public static function totalData($query, $start_time, $end_time){
        $query->select(['point_id', 'total' => 'sum(value)'])
            ->andWhere(['between', 'timestamp', MyFunc::TimeFormat($start_time), MyFunc::TimeFormat($end_time)])
            ->groupBy('point_id');
    }

    /**
     * 从10分钟前到当前最新的一个数
     * @param $point_id
     * @return string
     */
    public static function pointNewData($point_id){
        $result = static::findIdTimeRange(
            $point_id,
            date('Y-m-d H:i:s',strtotime('-10 minute')),
            date('Y-m-d H:i:s')
        )->orderBy('timestamp desc')->limit(1)->asArray()->one();
        return isset($result['value'])?$result['value']:'';
    }

    /**
     * 行转列，查询一个时间内每个点位值各是多少。
     * @param $id
     * @param $start_time
     * @param $end_time
     * @param array $Point
     */
    public static function ColRowTransition($point_id, $start_time, $end_time, $group, $method, $grp_default = false)
    {

        $point_values = '$$values';
        $col_name_all = 't (time varchar,';
        foreach($point_id as $key => $v){
            $point_values .= '('.$v.'::int4),';
            // 补齐字段类型和逗号
            $col_name_all .= 'point_id_'.$v.' decimal,';
        }
        $col_name_all = substr($col_name_all, 0, -1).')';
        $point_values = substr($point_values, 0, -1).'$$';
        // 行转列之前SQL
        if($start_time){
            $inner = static::findIdTimeRange($point_id, $start_time, $end_time)
                ->orderBy(['1' => SORT_DESC,'2' => SORT_ASC]);
        }else{
            $inner = static::findPointId($point_id)->orderBy(['1' => SORT_DESC,'2' => SORT_ASC]);
        }
        // SQL分组代码
        $method = MyStringHelper::getRelMethod($method);
        $group = $grp_default == false ? MyStringHelper::getRelGroup($group) : MyStringHelper::getRelDefault($group);

        if($group !== ['timestamp' => 'max(timestamp)']){ // 如果选择了汇总方式
            $method = ($method === ['value'])?['value' => 'max(value)']:$method; // 分组时最大值就是默认值
            $inner->select(array_merge($group, ['point_id'], $method))->groupBy([$group['timestamp'], 'point_id']);
        }

        // 把字段上的双引号去掉，一个单引号替换成两个，兼容PGSQL的字符串
        $inner_sql = str_replace('"', '', $inner->createCommand()->rawSql);
        $inner_sql = str_replace("'", "''", $inner_sql);

        return static::find()->from([$col_name_all => "crosstab('".$inner_sql."',".$point_values.")"]);
    }

    /**
     * 查找多段日期的点位数据
     * @param $id
     * @param $date
     * @return mixed
     */
    public static function findMultiDate($id, $date){
        static $MultiDate;
        if(!isset($MultiDate)){
            $MultiDate = MyFunc::MultiDateSql($date);
        }
        return static::findPointId($id)->andWhere($MultiDate)->asArray();
    }

    /**
     * 以时间为统计标准并以 点位id为分组的 分组数据处理
     * @param $time_arr  Array    时间数组        eg:['2014-01-01', '2015-01-01']
     * @param $time_type String   时间类型        可以是 year,month,week,day.
     * @param $point_id           点位id
     * @return Array
     */
    public static function statisticalByTime($time_arr, $time_type, $point_id, $time_format = '')
    {
        //将时间参数格式化 最终将其转化为 sql子句
        foreach($time_arr as $time){
            $format_time = MyFunc::TimeFloor($time, $time_type);
            $_format[] = $format_time;
            $_where[] = "timestamp between '{$format_time['start']}' and '{$format_time['end']}'";
        }
        $_where = isset($_where) && !empty($_where) ? implode(' or ', $_where) : '';
        //根据不同的时间类型 进行不同的分组统计处理
        $date_format = $time_format == '' ? MyStringHelper::getFormatTimeDefault($time_type) : MyStringHelper::getFormatTime($time_type);

        //连接数据库进行查询
        $dataProvider = PointData::find()->select([$date_format, 'point_id', 'sum(value) as value', 'min(id) as id '])
            ->Where(['point_id'=>$point_id])
            ->with(['point' => function ($query){ $query->select(['name', 'id']);}])
            ->andWhere($_where)
            ->groupBy(['_timestamp', 'point_id'])
            ->orderBy('_timestamp');
        //获取数据
        $data = $dataProvider->asArray()->all();
        return ['data' => $data,
                'format' => $_format,
                'dataProvider' => $dataProvider];
    }


    /**
     * 时间的范围来进行分组查询数据
     * @param $query
     * @param $start_time
     * @param $end_time
     */
    public static function getDataByTimeType($query, $start_time, $end_time, $group, $method)
    {
        $method = MyStringHelper::getRelMethod($method);
        $group = MyStringHelper::getFormatTimeDefault($group);
        return $query->select(array_merge([$group], ['point_id'], $method))
            ->andWhere(['between', 'timestamp', MyFunc::TimeFormat($start_time), MyFunc::TimeFormat($end_time)])
            ->groupBy(['_timestamp', 'point_id'])
            ->orderBy('_timestamp');
    }

    public static function getDataTimeRangeByPointId($point_id)
    {
        return self::find()->select(['max(timestamp) as max_time', 'min(timestamp) as min_time'])
                           ->where(['point_id' => $point_id])
                           ->asArray()->one();
    }

    /**
     * 分类统计数据库查询
     * @param $time_arr  Array    时间数组        eg:['2014-01-01', '2015-01-01']
     * @param $time_type String   时间类型        可以是 year,month,week,day.
     * @param $point_id           点位id
     * @return Array
     */
    public static function categoryStatistical($time_date, $time_type, $point_id, $time_format = '')
    {
        $format_time = MyFunc::TimeFloor($time_date, $time_type);
        $_format[] = $format_time;
        $_where[] = "timestamp between '{$format_time['start']}' and '{$format_time['end']}'";
        $_where = isset($_where) && !empty($_where) ? implode(' or ', $_where) : '';
        //根据不同的时间类型 进行不同的分组统计处理 只有两种处理方式
        $date_format = $time_format == '' ? MyStringHelper::getFormatTimeDefault($time_type) : MyStringHelper::getFormatTime($time_type);
        //连接数据库进行查询
        $dataProvider = PointData::find()->select([$date_format, 'min(point_id) as s_id', 'sum(value) as value'])
            ->Where(['point_id'=>$point_id])
            ->andWhere($_where)
            ->groupBy(['_timestamp'])
            ->orderBy('_timestamp');
        //获取数据
        $data = $dataProvider->asArray()->all();
        return ['data' => $data,
            'format' => $_format,
            'dataProvider' => $dataProvider];
    }
}
