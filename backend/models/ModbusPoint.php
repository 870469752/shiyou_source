<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.modbus_point".
 *
 * @property integer $id
 * @property integer $interval
 * @property string $name
 * @property integer $protocol_id
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_shield
 * @property boolean $is_upload
 * @property boolean $is_record
 * @property boolean $is_system
 * @property string $last_update
 * @property string $last_value
 * @property integer $unit
 * @property integer $value_type
 * @property string $value_filter
 * @property integer $address
 * @property integer $length
 * @property integer $device_id
 * @property integer $config_id
 * @property string $ratio
 * @property integer $type
 * @property string $fcu
 */
class ModbusPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.modbus_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interval', 'protocol_id', 'unit', 'value_type', 'address', 'length', 'controller_id', 'config_id', 'type'], 'integer'],
            [['name', 'base_value', 'base_history', 'last_update', 'last_value', 'value_filter', 'ratio'], 'string'],
            [['protocol_id', 'base_history', 'last_update', 'ratio'], 'required'],
            [['is_available', 'is_shield', 'is_upload', 'is_record', 'is_system'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'interval' => Yii::t('app', 'Interval'),
            'name' => Yii::t('app', 'Name'),
            'protocol_id' => Yii::t('app', 'Protocol ID'),
            'base_value' => Yii::t('app', 'Base Value'),
            'base_history' => Yii::t('app', 'Base History'),
            'is_available' => Yii::t('app', 'Is Available'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'is_record' => Yii::t('app', 'Is Record'),
            'is_system' => Yii::t('app', 'Is System'),
            'last_update' => Yii::t('app', 'Last Update'),
            'last_value' => Yii::t('app', 'Last Value'),
            'unit' => Yii::t('app', 'Unit'),
            'value_type' => Yii::t('app', 'Value Type'),
            'value_filter' => Yii::t('app', 'Value Filter'),
            'address' => Yii::t('app', 'Address'),
            'length' => Yii::t('app', 'Length'),
            'controller_id' => Yii::t('app', 'Controller ID'),
            'config_id' => Yii::t('app', 'Config ID'),
            'ratio' => Yii::t('app', 'Ratio'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
