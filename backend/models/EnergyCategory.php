<?php

namespace backend\models;
use yii\helpers\BaseArrayHelper;
use Yii;
use common\library\MyFunc;

/**
 * This is the model class for table "core.energy_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_type
 * @property integer $parent_id
 * @property string $category_id
 */
class EnergyCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.energy_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id','rgba'], 'string'],
            [['category_type', 'parent_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_type' => 'Category Type',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @desc 获取树状菜单数据
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @date 2015-12-18
     */
    public static function getAllCategory()
    {
        return self::find()
            ->select(['id', 'name', 'parent_id'])
            ->asArray()->indexBy('id')->all();
    }
 	//获取最顶层分类信息
    public static function getTopnode(){
        $node_data= self::find()->select(['id','name'])->where(['parent_id'=>null])->andWhere('id <> 0')->asArray()->all();
        $node_data = BaseArrayHelper::map($node_data, 'id','name');

        foreach($node_data as $key=>$value) {
            $node_data[$key] = MyFunc::DisposeJSON($node_data[$key]);
        }
        return $node_data;
    }

    /**
     * 获取所有分类的数据
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCategoryInfo()
    {
        return self::find()->select(['id as key', 'name as title', 'parent_id'])->asArray()->all();
    }

    public static function getChildrenById($id){
        $children=self::find()->where(['parent_id'=>$id])->asArray()->all();
        $children=BaseArrayHelper::map($children, 'id', 'name');
        foreach($children as $key=>$value) {
            $children[$key] = MyFunc::DisposeJSON($children[$key]);
        }
        return $children;
    }
        
    public static function getCategoryIds($id)
    {
        $condition = "category_id @> '{".$id."}'";
        return self::find()->select('id')->where($condition)->asArray()->all();
    }

}
