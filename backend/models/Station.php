<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use common\library\Tool\Upload;
/**
 * This is the model class for table "core.station".
 *
 * @property integer $id
 * @property string $area_code
 * @property string $address
 * @property string $ip
 * @property string $image_url
 * @property boolean $initialized
 * @property string $logo_url
 * @property string $default_language
 * @property string $name
 */
class Station extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'station_id', 'country', 'city'], 'required'],
            [['ip', 'name', 'address', 'run_time', 'building_type'], 'string', 'max' => 255],
            ['area_code', 'string', 'max' => 20],
            [['logo_url', 'image_url'], 'file'],
            ['default_language', 'string', 'max' => 10],
            [
                [
                    'web_port',
                    'building_year',
                    'building_storey',
                    'building_area',
                    'air_condition_area',
                    'chiller_quantity',
                    'cooling_pump_quantity',
                    'frozen_pump_quantity',
                    'cooling_tower_quantity',
                    'category_level',
                ],
                'number'
            ],
            ['category_level', 'default', 'value' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '站点编号'),
            'station_id' => Yii::t('app', '站点代码'),
            'country' => Yii::t('app', '国家'),
            'city' => Yii::t('app', '城市'),
            'address' => Yii::t('app', '详细地址'),
            'ip' => Yii::t('app', 'IP地址'),
            'web_port' => Yii::t('app', '端口'),
            'image_url' => Yii::t('app', '站点图片'),
            'area_code' => Yii::t('app', '选择地区'),
            'is_initialized' => Yii::t('app', '初始化'),
            'logo_url' => Yii::t('app', 'logo'),
            'default_language' => Yii::t('app', '默认语言'),
            'name' => Yii::t('app', '站点名称'),
            'building_year' => Yii::t('app', '建筑年代'),
            'building_storey' => Yii::t('app', '建筑层数'),
            'building_area' => Yii::t('app', '建筑面积'),
            'air_condition_area' => Yii::t('app', '空调覆盖面积'),
            'chiller_quantity' => Yii::t('app', '冷机数量'),
            'cooling_pump_quantity' => Yii::t('app', '冷却泵数量'),
            'frozen_pump_quantity' => Yii::t('app', '冷冻泵数量'),
            'cooling_tower_quantity' => Yii::t('app', '冷却塔数量'),
            'run_time' => Yii::t('app', '运行时间'),
            'building_type' => Yii::t('app', '建筑类型'),
            'category_level' => Yii::t('app', 'Category Menu Level'),
        ];
    }
    
}
