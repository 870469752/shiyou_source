<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_link".
 *
 * @property integer $id
 * @property string $name
 * @property string $main_point
 * @property string $link_point
 */
class PointLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'main_point', 'link_point'], 'string'],
            [['main_point', 'link_point'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'main_point' => 'Main Point',
            'link_point' => 'Link Point',
        ];
    }
}
