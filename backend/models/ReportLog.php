<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.report_export_log".
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $timestamp
 * @property string $file_name
 * @property string $file_type
 */
class ReportLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.report_export_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'file_name', 'file_type'], 'required'],
            [['task_id'], 'integer'],
            [['timestamp'], 'string'],
            [['file_name'], 'string', 'max' => 255],
            [['file_type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'timestamp' => Yii::t('app', 'Creation time'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_type' => Yii::t('app', 'File Type'),
        ];
    }
}
