<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.equipment_point_relation".
 *
 * @property integer $id
 * @property integer $equipment_id
 * @property integer $point_id
 */
class EquipmentPointRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.equipment_point_relation';
    }

    /**
     * 比较规则
     * @var array
     */
    private $_dynamic_rule=[
        '等于','不等于','大于','小于','大于等于','小于等于',
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipment_id', 'point_id'], 'required'],
            [['related_point_x', 'related_point_y'], 'string'],
            [['equipment_id', 'point_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('equipment', 'ID'),
            'equipment_id' => Yii::t('equipment', 'Equipment ID'),
            'point_id' => Yii::t('equipment', 'Point ID'),
        ];
    }
    /**
     * save
     */
    public static function savePointPosition($data){
        $sql = "UPDATE core.equipment_point_relation SET related_point_x={$data['related_point_x']},related_point_y={$data['related_point_y']}  WHERE equipment_id={$data['equipment_id']} AND point_id={$data['point_id']};";
        $db = Yii::$app->db;
        return $db->createCommand($sql)->queryScalar();
    }
    /**
     * 获取一台设备的所有关联point
     * @param $id equipment_id
     * @return array
     */
    public static function getOnePoint($id){
        $point_ids = EquipmentPointRelation::find()->select("id,point_id,related_point_x,related_point_y")->where("equipment_id=$id")->asArray()->all();
        $info = [];
        foreach($point_ids as $v){
            $tmp = point::getPointInformation($v['point_id']);
            $tmp['r_id'] = $v['id'];
            $tmp['point_x'] = $v['related_point_x'];
            $tmp['point_y'] = $v['related_point_y'];
            $info[] = $tmp;
        }
        return $info;
    }
    /*
     *@param int dian
     */
    public function getOneModel($id){
        $point_ids = EquipmentPointRelation::find()->select("point_id")->where("equipment_id=$id")->asArray()->all();
        $info = [];
        foreach($point_ids as $v){
            $info[] = point::getPointInformation($v);
        }
        return $info;
    }
}
