<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.detail_task".
 *
 * @property string $name
 * @property string $desc
 * @property integer $id
 */
class DetailTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.detail_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','detail'], 'string'],
            [['desc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', '任务描述'),
            'time' => Yii::t('app', '任务时间'),
        ];
    }
    /*
     * 保存任务
     */
    public function saveTask($data)
    {
        $detail = explode(';',$data['detail']);
        foreach($detail as $key => $value){
            if(strlen($value)>0){
                $tmp = explode(',',$value);
                $t_model = clone $this;
                $t_model->desc = isset($tmp[0])?$tmp[0]:'';
                $t_model->time = isset($tmp[1])&&isset($tmp[2])?"$tmp[1] -- $tmp[2]":'';
            }
            $t_model->save();
        }
    }
    /*应急废弃*/
    public function taskSave(){
        $tmp = explode(';',trim($this->detail,';'));
        $tmp_t = [];
        foreach($tmp as $v){
            $tmp_r = explode(',',$v);
            $tmp_t['start'] = $tmp_r[0];
            $tmp_t['end'] = empty($tmp_r[1]) ? '' : $tmp_r[1];

            $_model = clone $this;
            $_model->time = Json_encode($tmp_t);
            $_model->detail = isset($tmp_r[2])?$tmp_r[2]:'';
            $_model->desc = '';
            return $_model->save();
        }
    }
    
    public function getTask(){
        $date = date('Y-m',time());
        return static::find()->select("id, time, desc")
            ->where("time->>'start' > '$date-01 0:00'")
            ->andWhere("time->>'end' < '$date-30 23:59'")
            ->asArray()->all();
    }

}
