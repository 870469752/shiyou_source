<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.bacnet_point".
 *
 * @property integer $id
 * @property integer $interval
 * @property string $name
 * @property integer $protocol_id
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_shield
 * @property boolean $is_upload
 * @property string $last_update
 * @property string $last_value
 * @property integer $unit
 * @property integer $controller_id
 * @property string $object_name
 * @property integer $object_instance_number
 * @property string $object_units
 * @property string $object_type
 *
 * @property BacnetController $controller
 */
class BacnetPoint extends \yii\db\ActiveRecord
{

    public $category_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.bacnet_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['category_id', 'default', 'value' => [[0]]],
            ['base_value', 'default', 'value' => 0],
            ['base_value', 'number'],
            ['interval', 'integer'],
            ['is_shield', 'boolean'],
            ['is_upload', 'boolean'],
            ['unit', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'controller_id' => Yii::t('app', 'Controller ID'),
            'object_name' => Yii::t('app', 'Object Name'),
            'object_instance_number' => Yii::t('app', 'Object Instance Number'),
            'object_units' => Yii::t('app', 'Object Units'),
            'object_type' => Yii::t('app', '设备对象类型'),
            'interval' => Yii::t('app', '记录间隔'),
            'name' => Yii::t('app', 'Name'),
            'base_value' => Yii::t('app', 'Base Value'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'last_update' => Yii::t('app', 'Last Update'),
            'unit' => Yii::t('app', 'Unit'),
            'category' => Yii::t('app', '分类'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(BacnetController::className(), ['id' => 'controller_id']);
    }

    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('core.point_category_rel', ['point_id' => 'id']);
    }

    /**
     * 默认查询ID和名称并且是非屏蔽可用的
     * @return static
     */
    public static function baseSelect(){
        return static::find()->select('id, name')
            ->where(['is_shield' => false, 'is_available' => true]);
    }

    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit']);
    }
}
