<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.atom_event".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property boolean $is_valid
 * @property integer $point_id
 * @property string $time_range
 * @property string $trigger
 *
 * @property Point $point
 */
class PointEvent extends \yii\db\ActiveRecord
{
    public $time_type;
    public $Summer_max_value;
    public $Summer_min_value;
    public $value;
    public $operator;
    public $rule;
    public $trigger_type;
    public $original_time;
    public $level_sequence;
    public $remark;
    public $delay;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name','type', 'time_range', 'trigger', 'rule' ,'trigger_type','remark','delay'], 'string'],
            [['is_valid', 'is_system'], 'boolean'],
            [['point_id'], 'required'],
            [['point_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'is_valid' => Yii::t('app', 'Is Valid'),
            'point_id' => Yii::t('app', 'Trigger Point'),
            'time_range' => Yii::t('app', 'Trigger Time Range'),
            'trigger' => Yii::t('app', 'Trigger Value Range'),
            'time_type' => Yii::t('app', 'Repeat Types'),
            'trigger_type' => Yii::t('app', 'Trigger Type'),
            'operator' => Yii::t('app', 'Operator'),
            'rule' => Yii::t('app', 'Rule'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(Point::className(), ['id' => 'point_id']);
    }
    public static  function getPointEventInfoById($id){
        return PointEvent::find()->Where(['id' => $id])->asArray()->all();

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlarmRules()
    {
        return $this->hasMany(AlarmRule::className(), ['event_id' => 'id']);
    }

    /**
     * 获取对应点为 的系统原子事件 与 规则
     * 需要注意的是 返回的是系统生成的事件 并需要排除通过绑定页面 生成的系统事件
     * @param $point_str
     */
    public function getAlarmParam($point_str)
    {
        $point_str = !empty($point_str) ? $point_str : -1;
        $point_arr = explode(',', $point_str);
        $alarm_param = Point::baseSelect()->with([
            'atomEvents' => function ($query){
                self::findRuleByOrm($query->orderBy('id desc'));
            },
        ])->where(['id' => $point_arr]);
        return $alarm_param;
    }

    /**
     * 获取对应点为 的系统原子事件 与 规则
     * 需要注意的是 返回的是系统生成的事件 并需要排除通过绑定页面 生成的系统事件
     * @param $point_str
     */
    public function getAlarmParamNew($point_str)
    {
        $point_str = !empty($point_str) ? $point_str : -1;
        $point_arr = explode(',', $point_str);//echo "<pre>";print_r($point_arr);die;
        $alarm_param = Point::baseSelect()->with([
            'atomEvents' => function ($query){
                //$query ->where(['point_id' => $point_arr]);
                self::findRuleByOrm($query->orderBy('id desc'));
            },
        ])->where(['id' => $point_arr]);//echo "<pre>";print_r($alarm_param);die;
        return $alarm_param;
    }

    /**
     * 获取 时间id对应的 规则
     * @param $query
     * @return mixed
     */
    public static function findRuleByOrm($query)
    {
        $result = $query->with([
                'alarmRules' => function ($query){
                       $query->where(['is_system' => 'true']);
                    }
            ]);
        return $result;
    }
    /**
     * 获取 所有非屏蔽点位
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPointData()
    {
        $data = Point::findAllPoint();
        foreach($data as $key => $value)
        {
            if($value['is_shield'] || $value['is_system'])
            {
                unset($data[$key]);
            }
        }
        return $data;
    }

    /**
     * 更新数据 并保存
     * @return bool
     */
    public function saveEvent()
    {

        //time 时间的处理
        $time_info = MyFunc::getTimeRangeOfAlarm($this->time_range, $this->time_type);
        $this->time_range = $time_info['range_time'];
        $this->original_time = $time_info['original_time'];
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name, 'description', $this->is_system ? 'zh-cn' : '');
        }
        else
        {
            //$this->name = MyFunc::createJson($this->name, 'description', $this->is_system ? 'system' : '');
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name'], $this->is_system ? 'zh-cn' : '');
        }

        //trigger 处理  以逗号 为 一组区间
        $_tri_arr = explode(';', trim($this->trigger, ','));

        $trigger['trigger_type'] = $_tri_arr[0];
        $trigger['operator'] = $_tri_arr[1];
        $trigger['value'] = $_tri_arr[2];
        $this->trigger = MyFunc::createJson($trigger, 'trigger');
        return  $this->insert() ? $this->id : false;
    }

    /**
     * 判断是否含有系统事件 并作出处理
     * @param bool  $is_only 是否是在系统中唯一 存在的
     * @return bool
     */
    public function bindingAtomEvent($is_only)
    {

        /*判断当前绑定类型 是否存在 相应事件事件*/
        /*如果
            是唯一的 查找系统生成除point_id 外的部分name
          否则
            找全部 name;
        */
        // 在此 的逻辑是 将冬夏季绑定的事件 一并删除

        $system_prefix = $is_only ? substr($this->name, 0, strrpos($this->name, '-')) : $this->name;
        $is_exist = self::fuzzySearch($system_prefix,'*');

        if(1)//
        {
           return $this->saveEvent();
        }
        else
        {
            $model = self::findOne($is_exist->id);
            $model -> load([
                            'name' => $this->name,
                            'point_id' => $this->point_id,
                            'time_range' => $this->time_range,
                            'trigger' => $this->trigger,
                            'is_system' => true,
                        ], '');
            return $model->saveEvent();
        }
    }

    /**
     *
     * 点位 报警设置 页面 生成的 原子事件，复合事件，报警规则
     *
     * 大致步骤：
     *          1、首先 创建 触发值为[max_value, min_value] 的一个 原子事件  并将event_id 保存下来
     *          2、判断 是否存在冬夏季数值有则 创建 对应的 季节温度时间 匹配 name 前缀为 'System_alarm_event_summer-point_id',
     *          3、创建 所有对应点位的 原子 和 复合事件规则
     * @param $is_only
     * @return bool
     */
    public function isTemperatureEvent($is_only)
    {
        /* 首先 创建 以最大最小值为 值范围的系统事件*/
        $event_id['event'] = $this->bindingAtomEvent($is_only);

        /* 在创建时 判断 如果存在 夏季范围值 并且 最大值 大于 最小值时  创建一个该温度点位的夏季温度事件 并 创建一个 夏季温度事件 && 夏季事件点位 的一个复合事件*/
        if(!empty($this->Summer_max_value) && !empty($this->Summer_min_value) && $this->Summer_max_value >= $this->Summer_min_value)
        {
            //创建一个 新的事件对象 触发值为 最大最小值 名称为系统 规则名称
            $summer_event_name = MyFunc::setSystemName('alarm', 'summer_event' , $this->point_id);
            $summer_trigger = $this->Summer_max_value.';'.$this->Winter_max_value;
            $sum_model = self::getAtomEventObjBySystem($summer_event_name, $this->point_id,$summer_trigger);
            //获取 生成的温度事件 的id
            $temp_event_id['summer'] = $sum_model->bindingAtomEvent($is_only);
            //获取 季节模式点位 生成的 夏季事件id => 'System_bind_summer_event-'
            $summer_event = self::fuzzySearch('System_bind_summer_event-','*');

            if(isset($summer_event) && !empty($summer_event))
            {
                //季节温度复合 事件规则 数组
                $tem_comp_rule['summer'] ='('. 'E'.$temp_event_id['summer'] .' AND '. 'E'.$summer_event->id .')';
            }
        }
        /*同理 创建冬季温度事件 和 冬季温度复合事件*/
        if(!empty($this->Winter_max_value) && !empty($this->Winter_min_value) && $this->Winter_max_value >= $this->Winter_min_value)
        {
            $winter_event_name = MyFunc::setSystemName('alarm', 'winter_event' , $this->point_id);
            $winter_trigger = $this->Winter_max_value.';'.$this->Winter_min_value;
            $win_model = self::getAtomEventObjBySystem($winter_event_name, $this->point_id,$winter_trigger);
            $temp_event_id['winter'] =  $win_model->bindingAtomEvent($is_only);
            //获取 季节模式点位 生成的 夏季事件id => 'System_bind_winter_event-'
            $win_event = self::fuzzySearch('System_bind_winter_event-','*');

            if(isset($win_event) && !empty($win_event))
            {
                //季节温度复合 事件规则 数组
                $tem_comp_rule['winter'] =  '('. $temp_event_id['winter'] .' AND '. $win_event->id .')';
            }
        }
        /*判断 是否 存在 季节温度 规则
             存在 生成 复合事件规则 (T1 AND S1) OR (T2 AND S2)*/
        //目前先 这样处理
        if(isset($tem_comp_rule) && !empty($tem_comp_rule))
        {
            //生成 复合事件 的规则
            if(isset($tem_comp_rule['summer']) && isset($tem_comp_rule['winter']))
            {
                $comp_rule = implode(' OR ', $tem_comp_rule);
            }
            else if(isset($tem_comp_rule['summer']))
            {
                $comp_rule = $tem_comp_rule['summer'];
            }
            else if(isset($tem_comp_rule['winter']))
            {
                $comp_rule = $tem_comp_rule['winter'];
            }
            //生成 复合事件
            /*最终 与 夏季季节事件生成 复合事件
                    复合 事件的 名字 以 $event_id['event'] 为后缀
                            name 即 'System_alarm_compound_event-原子事件id*/
            $comp_name = MyFunc::setSystemName('alarm', 'compound_event' , $event_id['event']);
            //初始化复合事件
            $comp_event = CompoundEvent::getCompoundEventObjBySystem($comp_name, $comp_rule);
            $event_id['compound'] = $comp_event->bindingCompoundEvent(false);
        }
        /* 报警规则 创建
                规则 name 前缀 : 'System_alarm_rule-'*/
        foreach($event_id as $key => $value)
        {
            //报警规则名称
            $rule_name = MyFunc::setSystemName('alarm', 'rule' , $value);
            //报文 设置为 当前事件id发生报警
            $rule_description = '';
            $alarm_rule = AlarmRule::getRuleObjBySystem($rule_name, $rule_description, $value,$this->delay, $this->rule, $this->remark);
            $alarm_rule->bindingRuleEvent(false);
        }
        return true;
    }

    /**
     * 利用 参数 创建 原子事件对应
     * @param $name
     * @param $point_id
     * @param $trigger
     * @param string $time_range
     * @return AtomEvent
     */
    public static function getAtomEventObjBySystem($name, $point_id, $trigger, $time_type = 'day',$time_range = '2009-09-01 00:00, 2009-09-01 00:00')
    {
       $model = new PointEvent();
       $model->load([
                    'name' => $name,
                    'trigger' => $trigger,
                    'time_type' => $time_type,
                    'time_range' => $time_range,
                    'is_system' => true,
                    'type' => '1',
                    'point_id' => $point_id], '');
       return $model;
    }


    /**
     * 根据 系统生成 的名字进行模糊查找
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */

    public static function fuzzySearch($name,$option)
    {
        return self::find()
            ->select($option)
            ->where("name->'data'->>'zh-cn' like '%" .$name."%'")
            ->asArray()->all();
    }
    /**
     * 根据 点位的名字进行模糊查找点位ids
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */

    public static function SearchEventsByPointNameSearch($name)
    {
        $sql="SELECT id,name,point_id,type FROM core.point_event ".
                "where point_id in (".
                "select id ".
                "FROM core.point ".
                "where (name->'data'->>'zh-cn')::text like '%".$name."%')";

        return self::findBySql($sql)->asArray()->all();
    }
    /**
     * 创建一个原子事件对象 根据数组数据 对 对象进行赋值
     * @param $column
     */
    public static function createObj($arr_column)
    {
        $model = new PointEvent();
        !empty($arr_column) && is_array($arr_column) ? $model->load($arr_column, '') : '';
        return $model;
    }


    /**
     * 删除原子事件 报警规则
     * @param $event_id
     */
    public static function _delete($arr)
    {
        $model = new PointEvent();
        //$model->delete()->where('id')
        return $model;
    }

    /**
     * 获取 点位的事件和规则
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getEventAndRule($point_id)
    {
        $a_r = 'core.alarm_rule';
        $p_e = 'core.point_event';
        $data = self::find()->select("$p_e.name->'data'->>'zh-cn' as event_name,$p_e.trigger->'data'->>'trigger_type' as trigger_type,$p_e.trigger->'data'->>'operator' as operator,$p_e.trigger->'data'->>'value' as value,$a_r.level_sequence->'data' as rule_list")->leftJoin($a_r,"$a_r.event_id = $p_e.id")->where("$p_e.point_id = $point_id")->asArray()->all();
        return $data;
    }
}