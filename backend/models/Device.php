<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.device".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $gojs_data
 *
 * @property PointDeviceRel[] $pointDeviceRels
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'gojs_data'], 'string'],
            [['name', 'gojs_data','type'], 'required'],
            [['type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '设备名称',
            'type' => 'Type',
            'gojs_data' => 'Gojs Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointDeviceRels()
    {
        return $this->hasMany(PointDeviceRel::className(), ['device_id' => 'id']);
    }
}
