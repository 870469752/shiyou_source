<?php

namespace backend\models;

use backend\models\PointData;
use backend\models\PointDataStatistic;
use backend\models\PointCategoryRel;
use backend\models\PointCategoryConfig;
use yii\helpers\BaseArrayHelper;
use yii\helpers\ArrayHelper;
use common\library\MyFunc;
use Yii;
/**
 * This is the model class for table "core.point_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 *
 * @property PointCategoryRel $pointCategoryRel
 * @property Point[] $Point
 */
class Category extends \yii\db\ActiveRecord
{
    public $id;
    public $start_time;
    public $end_time;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['name','parent_id'], 'required'],
            [['parent_id'], 'integer'],
            ['id', 'default', 'value' => [[0]]],
            ['start_time', 'default', 'value' => $this->getTime('min')],
            ['end_time', 'default', 'value' => $this->getTime('max')]
        ];
    }

    public function getTime($key = 'max')
    {
        $pointData = new PointData();
        return $pointData->getTime($key);
    }

    public function fields()
    {
        return [
            'id',
            'start_time',
            'end_time' => function () {
                    // 如果已经有时分秒了就不加了
                    if(!strpos($this->end_time,':')){
                        return date('Y-m-d', strtotime($this->end_time)).' 23:59:59';
                    }
                    return $this->end_time;
                },
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('category', '分类编号'),
            'name' => Yii::t('category', '分类名称'),
            'parent_id' => Yii::t('category', '父级分类'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasMany(Point::className(), ['id' => 'point_id'])
            ->viaTable('core.point_category_rel', ['category_id' => 'id']);
    }


    public function getCategorys()
    {
        return $this->hasOne(Category::className(), ['id' => 'id']);
    }
    /**
     * ID和父ID对应，便于获取子类
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(static::className(), ['parent_id' => 'id']);
    }
    //获取最顶层分类信息
    public static function getTopnode(){
        $node_data= self::find()->select(['id','name'])
            ->where(['parent_id'=>null])
            ->andWhere('category_type=1 or category_type=2')
            ->andWhere('id <> 0')->asArray()->all();
        $node_data = BaseArrayHelper::map($node_data, 'id','name');
        foreach($node_data as $key=>$value) {
            $node_data[$key] = MyFunc::DisposeJSON($node_data[$key]);
        }
        return $node_data;
    }
    public static function getcolor(){
        $color=self::find()->select(['id','rgba'])
            ->where('category_type=1 or category_type=2')
            ->asArray()->all();
        $color=BaseArrayHelper::map($color,'id','rgba');
        return $color;
    }
    public static function BaseSelect()
    {
        return static::find()->select(['id', 'name', 'parent_id']);
    }

    /**
     * 通过分类ID查找对应点位，并转化为一维数组
     *
     * @param $category_id 如我要查找属于“电”的点位，传入 [[1]] 即可。数字1代表电。
     * 如果要查找查找是“电”或者“水”并且是“商业楼”内的只要传入 [[1,2],[3]] 数字1，2，3分别代表电、水、商业楼
     * @return array|bool|null
     */
    public static function findCategoryPoint($category_id)
    {


        if($category_id === [[0]]){ // 如果没有选择就返回空
            //TODO null好还是[]空数组好
            return null;
        }
        foreach($category_id as $arr_id){
            $ids = [];
            foreach($arr_id as $id){
                $categorys = static::find()->select('id')
                    ->where("category_id @> '{".$id."}'")  //判断json中是否含有 当前的id
                    ->asArray()->all();
                $ids += ArrayHelper::map($categorys, 'id', 'id');
            }

            $tmp = PointCategoryRel::find()->select('point_id')->where(['category_id' => $ids])->asArray()->all();

            $tmp = MyFunc::arrayTwoOne($tmp, 'point_id');
            $point_id = isset($point_id)?array_intersect($point_id, $tmp):$tmp; // 几种并列条件的交集就是所有条件满足的点位。
        }

        // 如果选择了但是没有点位就FALSE
        return $point_id?$point_id:FALSE;
    }
    public static function getCategoryPointNew($category_id){
        $data=PointCategoryConfig::find()->select('point_id')
            ->where(['category_id'=>$category_id])
            ->asArray()->all();
        $points=[];
        foreach($data as $key=>$value){
            $points[]=$value['point_id'];
        }
            return $points;
    }
            /* new  分类点位查找函数   加入type字段限制 */
    public static function getCategoryPoint($category_id,$category_type)
    {
        if($category_id === [[0]]){ // 如果没有选择就返回空
            //TODO null好还是[]空数组好
            return null;
        }
        foreach($category_id as $arr_id){
            $ids = [];
            foreach($arr_id as $id){
                $categorys = static::find()->select('id')
                    ->where("category_id @> '{".$id."}'")  //判断json中是否含有 当前的id
                    ->asArray()->all();
                $ids += ArrayHelper::map($categorys, 'id', 'id');
            }
            $tmp = PointCategoryRelNew::find()
                ->select('point_id')
                ->where(['category_id' => $ids])
                //->andWhere(['category_type'=>$category_type])
                ->asArray()->all();
            $tmp = MyFunc::arrayTwoOne($tmp, 'point_id');
            $point_id = isset($point_id)?array_intersect($point_id, $tmp):$tmp; // 几种并列条件的交集就是所有条件满足的点位。
        }
        // 如果选择了但是没有点位就FALSE
        return $point_id?$point_id:[];
    }


    public static function getChildrenById($id){
        $children=self::find()->where(['parent_id'=>$id])->asArray()->all();
        $children=BaseArrayHelper::map($children, 'id', 'name');
        foreach($children as $key=>$value) {
            $children[$key] = MyFunc::DisposeJSON($children[$key]);
        }
        return $children;
    }
    public function CategoryPointData()
    {
        $point_id = static::findCategoryPoint($this->id);

        // 取出相应点位优化后的数据
        return Point::findOptimizePointData($point_id, $this->start_time, $this->end_time);
    }

    public function CategoryPointDataTotal()
    {
        $point_id = $this->findCategoryPoint();

        // 取出一段时间内的总数
        return Point::findPointDataTotal($point_id, $this->start_time, $this->toArray()['end_time']);
    }
    public function printf($node){
        echo'<pre>';
       print_r($node);
    }

    //获取$query_param时间内的所有点位data数据
    public function getPointdata_($query_param){
        $all_value=null;
        if(empty($query_param)) {
            $all_value=PointData::find()->select(['point_id','sum(value) as value'])->groupBy('point_id')->asArray()->all();
        }
        else {
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
                    break;
                case 'month':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
                    break;
                case 'day':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
                    break;
                case 'hour':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')='" . $date_time . "'";
                    break;
            }
            $all_value=PointData::find()->select(['point_id','sum(value) as value'])->andWhere($date_formate)->groupBy('point_id')->asArray()->all();

        }


        return $all_value;
    }


    //获取$query_param时间内的所有点位data数据
    public function getPointdata_new($query_param){

        $points=Point::findAllPoint();

        $query_param=[
            'time_type'=>'year',
            'date_time'=>'2015',
            'Point'=>[
                'category_id'=>170
            ]
        ];

//        默认年
        if(empty($query_param)) {
            $all_value=self::find()
                ->select(['point_id','sum(value) as value'])
                ->andWhere(['interval_type'=>3])
                ->groupBy('point_id')
                ->asArray()->all();
        }
        else {
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $interval_type=3;
                    break;
                case 'month':
                    $interval_type=2;
                    break;
                case 'day':
                    $interval_type=1;
                    break;
            }
//              year  month day
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";

            $all_value=self::find()
                ->select(['point_id as id','sum(value) as value'])
                ->andWhere(['interval_type'=>$interval_type])
//                ->andWhere($date_formate)
                ->groupBy('point_id')
//                ->createCommand()->getRawSql();
                ->asArray()
                ->all();
        }
        return $all_value;
    }
    
    //获取$query_param时间内的所有点位data数据
    /**
     * @param $query_param
     * @return array|null|\yii\db\ActiveRecord[]
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @description rewrite the function to generate the table data of the backend index
     * @sql select point_id from core.point_category_rel where category_id in (select id from core.energy_category where category_id @> '{29}')
     */
    public function getPointdataIndex($location_id, $query_param){
        $pointcategoryconfig = new PointCategoryConfig();
        $all_value=null;
        $category_ids = [];
        $point_ids = [];
        $energy_id = $query_param['Point']['category_id'];

        $point_ids = $pointcategoryconfig->returnPointIds($location_id, $energy_id);

        if (!$point_ids) {
            return null;
        }

        if(empty($query_param)) {
            $interval_type = 3;
            $value_type = 5;
            $all_value = PointDataStatistic::categoryStatisticalCustom($interval_type, $value_type, $query_param['date_time'], $query_param['time_type'],$point_ids);
        }
        else {
            $interval_type = $query_param['interval_type'];
            $value_type = $query_param['value_type'];
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
                    break;
                case 'month':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
                    break;
                case 'day':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
                    break;
                case 'hour':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')='" . $date_time . "'";
                    break;
            }
            $all_value = PointDataStatistic::categoryStatisticalCustom($interval_type, $value_type, $query_param['date_time'], $query_param['time_type'], $point_ids);
        }
        return $all_value;
    }

    public function Getdata()
    {
        $category_ids=self::getTopnode();
//        echo'<pre>';
//        print_r($category_ids);
//        die;
       $categorydata=self::Getlinkdata(201);
        return $categorydata;
    }

    //遍历数组
    function arr_foreach ($arr,$path)
    {
        if (!is_array ($arr))
        {
            return false;
        }

        foreach ($arr as $key => $val )
        {
            print_r($key);

            if($key!='name'&&$key!='value'&&$key!='children'){
              $path[]=$key; //print_r($path);
            }
            if (is_array ($val))
            {
                //print_r($path);
                self::arr_foreach ($val,$path);
            }
            else
            {
                //print_r($path);
                array_pop($path);
                //print_r($val);
                //echo '  ';
            }
        }
    }
    //处理数据为链表结构(未完成?)
    public function Getlinkdata($category_id)
    {
        $data=array();$key_value=null;
        $categorys=self::getChildrenById($category_id);
        //查找下属点位计算电量
        foreach($categorys as $categorys_key=>$categorys_value) {
            $data[$categorys_key]=['name'=>$categorys_value,'value'=>self::getCategoryDataById($categorys_key),'parent'=>$category_id,'children'=>null];
            //如果叶子不为空 继续下级
            if(!self::leafIsEmpty($categorys_key)) {
                $aaa=self::Getlinkdata($categorys_key);
                foreach($aaa as $aaa_key=>$aaa_value){
                    $aaa_value['children']=$categorys_key;
                        $data[$aaa_key]=$aaa_value;
                        //$data[$aaa_key]=['children'=>$categorys_key];
                }
            }
        }
        return  $data;
    }

    /**
     * @param $query_param
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function GetDataGroupByDiffTime($query_param)
    {
        $all_value=null;
        if(empty($query_param)) {
            $group_date="to_char(timestamp, 'yyyy')";
            $all_value=PointData::find()->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])->groupBy(['point_id,'.$group_date])->asArray()->all();
        }
        else {
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM')";
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
                    break;
                case 'month':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD')";
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
                    break;
                case 'day':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')";
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
                    break;
                case 'hour':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY')";
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')='" . $date_time . "'";
                    break;
            }
            $all_value=PointData::find()
                ->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])
                ->andWhere($date_formate)
                //->join('LEFT JOIN','core.point z','z.id=point_data.point_id')
                ->groupBy(['point_id,'.$group_date])
                ->asArray()->all();

        }
        return $all_value;
    }

    //分类查找数据 并按时间分类
    public function GetDataGroupByTime($query_param){
            $all_value=null;
            if(empty($query_param)) {
                $group_date="to_char(timestamp, 'yyyy')";
                $all_value=PointDataStatistic::find()
                    ->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])
                    ->where(['value_type'=>5])
                    ->andWhere(['interval_type'=>1])
                    ->groupBy   (['point_id,'.$group_date])
                    ->asArray()->all();
            }
            else {
                $time_type = $query_param['time_type'];
                $date_time = (string)$query_param['date_time'];
                switch ($time_type) {
                    case 'year':
                        $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM')";
                        $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
                        break;
                    case 'month':
                        $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD')";
                        $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
                        break;
                    case 'day':
                        $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')";
                        $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
                        break;
                    case 'hour':
                        $group_date="TO_CHAR(TIMESTAMP,'YYYY')";
                        $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')='" . $date_time . "'";
                        break;
                }
                $all_value=PointDataStatistic::find()
                    ->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])
                    ->where(['value_type'=>5])
                    ->andWhere(['interval_type'=>1])
                    ->andWhere($date_formate)
                    //->join('LEFT JOIN','core.point z','z.id=point_data.point_id')
                    ->groupBy(['point_id,'.$group_date])
//                    ->createCommand()->getSql();
                    ->asArray()->all();

            }
            return $all_value;
        }



    //柱状图数据
    public function category_column($query_param,$all_value,$category_id){

        $result=array();
        $data=array();
        //得到$cagegory_id 下级分类
        $categorys=self::getChildrenById($category_id);

        if(empty($categorys)){
            $category_info[]=self::getCategoryById($category_id);
            $categorys=BaseArrayHelper::map($category_info, 'id', 'name');
//            $categorys=self::getChildrenById($category_info['parent_id']);
        }
        //echo '<pre>';
        //初始化数组
        $times=self::get_time($query_param);

        foreach ($times as $time_value) {
            foreach ($categorys as $category_key=>$category_value) {
                $result[$time_value][$category_key]=[
                    'value'=>0,
                    'category_name'=>$category_value
                ];
            }
        }
//       echo '<pre>';
        //print_r($result);
        //给数组里填值
        foreach ($categorys as $category_key=>$category_value ) {
            $data=self::category_column_data($all_value,$category_key);

//            print_r($category_key);
//            print_r($category_value);
//            print_r($data);
            foreach($data as $data_key=>$data_value){
                $result[$data_key][$category_key]['value']=$data_value;
//                $result[$data_key][$category_key]=[
//                    'value'=>$data_value,
//                    'category_name'=>$category_value
//                ];
            }
        }
        //print_r($result);
        return ['result'=>$result,'categorys'=>$categorys];
    }

    //根据$category_id 查找本分类下的数据  按给定的时间 年月日 分类
    public function category_column_data($all_value,$category_id){
        //$all_value=$this->GetDataGroupByTime($query_param);
        //findCategoryPoint()弃用了 都使用pointcategoryconfig查询
//        $point_ids = self::findCategoryPoint([$category_id]]);
        $point_ids_=PointCategoryConfig::find('point_id')->where(['category_id'=>$category_id])->asArray()->all();
        $point_ids=[];
        foreach($point_ids_ as $key=>$value){
            $point_ids[]=$value['point_id'];
        }
        $point_ids=array_unique($point_ids);

        $data=array();
        $last_data=array();
        //TODO 两个数组循环查找  未想到改进方法
        if(!empty($point_ids)) {
            //去掉重复值
            $point_ids=array_unique($point_ids);
            //总值计算
            foreach ($point_ids as $points_key => $points_value) {
                //如果 $point_id属于此分类 则
                foreach($all_value as $key=>$value){
                    if($points_value==$value['point_id']){
                        if(array_key_exists($value['timestamp'],$data)){
                            $data[$value['timestamp']]=$data[$value['timestamp']]+$value['value'];
                        }
                        else $data[$value['timestamp']]=$value['value'];
                    }
                }
            }
            //处理为categories[]  chart_data[];
//            foreach($data as $key=>$value){
//                $last_data['categories'][]=$key;
//                $last_data['chart_data'][]=$value;
//            }
            return $data;
        }else return $data;
    }


    //获取data数据
    public function GetDataByTime($query_param){
        $all_value=null;
        if(empty($query_param)) {
            $all_value=PointData::find()->select(['point_id','sum(value) as value'])->groupBy('point_id')->asArray()->all();
//            $all_value=Po::find()->select(['point_data.point_id as point_id','max(z.last_value) as last_value','sum(point_data.value) as value'])
//                ->join('INNER JOIN','core.point z','z.id=point_data.point_id')
//                ->groupBy('point_id')
//                ->createCommand()->getRawSql();
//
//           //???执行
//            $all_value=PointData::findBySql($all_value)->asArray()->all();
        }
        else {
            $all_value =Array();
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
                    break;
                case 'month':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
                    break;
                case 'day':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
                    break;
                case 'hour':
                    $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')='" . $date_time . "'";
                    break;
            }
                    //加入季度查询三个月
                    if($time_type=='quarter'){
                        $time[]=date('Y-m', strtotime($date_time));
                        $time[]=date('Y-m', strtotime("-1 months",strtotime($date_time)));
                        $time[]=date('Y-m', strtotime("-2 months",strtotime($date_time)));
                        foreach($time as $time_value) {
                            $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $time_value . "'";
                            $all_value_temp = PointData::find()->select(['point_id', 'sum(value) as value'])->andWhere($date_formate)->groupBy('point_id')->asArray()->all();
                            foreach($all_value_temp as $value){
                                $all_value[]=$value;
                            }
                        }
                    }
            else $all_value=PointData::find()->select(['point_id','sum(value) as value'])->andWhere($date_formate)->groupBy('point_id')->asArray()->all();
        }
        return $all_value;
    }


    //获取树状data数据
    public function getPointdata($query_param){
            $point_data_statistic=new PointDataStatistic();
            $location_id=array();


        $startTime1=microtime(true);
            //获取所有符合时间的点位数据
            $all_value=$point_data_statistic->getPointdata($query_param);

            $value_with_time=$point_data_statistic->GetDataGroupByTime($query_param);


//            $value_with_time=$this->GetDataGroupByTime($query_param);
//            $all_value=$this->getPointdata_($query_param);
//        echo '<pre>';
//            print_r($all_value);
////        print_r($value_with_time);
//        die;
        foreach ($all_value as $key=>$value) {
                $points[]=$value['point_id'];

            }

        $all_value=BaseArrayHelper::map( $all_value, 'point_id', 'value');
        $category_ids =self::getTopnode();

            $color=self::getcolor();

            if(!empty($query_param)) {
                foreach($query_param['Point'] as $category_value){
                    $location_id[]=[$category_value];
                }
//                $location_id = $query_param['Point']['category_id'];
            }
        $startTime=microtime(true);
            $point_category_rel=PointCategoryConfig::find()->select(['point_id','category_id'])
                ->where(['point_id'=>$points])
                ->asArray()->all();
            $category_points=[];
            foreach ($point_category_rel as $key=>$value) {

                    if(isset($category_points[$value['category_id']])) $category_points[$value['category_id']]+=$all_value[$value['point_id']];
                    else   $category_points[$value['category_id']]=0;
                }

            $categorydata=array();
            //处理为树状结构
            foreach ($category_ids as $ids_key => $category_name) {
                $categorydata[$ids_key] = [
                    'name' => $category_name,
                    'color'=>$color[$ids_key],
//                    'value' => self::getCategoryDataById($ids_key,$all_value,$location_id),
                    'value' => isset($category_points[$ids_key])?$category_points[$ids_key]:0,
                    'parent' => $ids_key,
                    'children' => self::getNextCategotyValue($ids_key,$category_points,$all_value,$value_with_time,$location_id),
//                    'detail'=>self::category_column_data($value_with_time,$ids_key)
                ];
            }
//        echo '<pre>';
//        print_r($categorydata);
//        die;
        $endTime=microtime(true);
//        die;
            return $categorydata;
    }

    public function data_with_location($query_param){
        $all_value=$this->getPointdata_($query_param);
        $all_value=BaseArrayHelper::map( $all_value, 'point_id', 'value');

        $data=self::getCategoryDataById(170,$all_value,[[213]]);
        return $data;
    }


    //得到下属的分类用量 y['category_id'=>['name'=>,'value'=>,'children'=>[]]  ,....]
    public function getNextCategotyValue($category_id,$category_points,$all_value,$value_with_time,$location_id){
        $data=null;

        $categorys=self::getChildrenById($category_id);
//        echo '<pre>';
//        print_r($category_points);
//        print_r($categorys);
//        die;
        $color=self::getcolor();
        //查找下属点位计算电量
        foreach($categorys as $categorys_key=>$categorys_value) {
            $data[$categorys_key]=[
                'name'=>$categorys_value,
                'color'=>$color[$categorys_key],
//                'value'=>self::getCategoryDataById($categorys_key,$all_value,$location_id),
                'value' => isset($category_points[$categorys_key])?$category_points[$categorys_key]:0,
                'parent'=>$category_id,
                'children'=>null,
//                'detail'=>self::category_column_data($value_with_time,$categorys_key)
            ];
            //如果叶子不为空 继续下级
            if(!self::leafIsEmpty($categorys_key)) {
                $aaa=self::getNextCategotyValue($categorys_key,$category_points,$all_value,$value_with_time,$location_id);
                $data[$categorys_key]['children']=$aaa;
            }
        }

        return $data;
    }


    //得到id=$id 类以及符合$query_parm时间的用量   再加上要符合相应地理位置ing
    //$id int;
    //$location_id array;
    public function getCategoryDataById($id,$all_value,$location_id){
        $data=0;
        //处理location_id 内的category_id为[[id1],[id2],...]格式
//        $energy_point_ids = self::getCategoryPoint([[$id]],1);
//        if(empty($location_id))
//        $location_point_ids = self::getCategoryPoint([$location_id],2);
//        else $location_point_ids = self::getCategoryPoint($location_id,2);
//       if(empty($location_id[0][0]))$point_ids=$energy_point_ids;
//       else $point_ids=array_intersect($energy_point_ids,$location_point_ids);

        $energy_point_ids = self::getCategoryPointNew($id);
      $point_ids=$energy_point_ids;

        if(!empty($point_ids)) {
            //去掉重复值
            $point_ids=array_unique($point_ids);
            //总值计算
            foreach ($point_ids as $points_key => $points_value) {
                //$value = Point::find()->select(['last_value'])->where(['id' => $points_value])->asarray()->all();
                if(!array_key_exists($points_value,$all_value))
                    $data=$data;
                else {
                    $data = $data + $all_value[$points_value];
                }
            }
            return $data;
        }else return $data=0;
    }

    public static function getChildNode($id){
        return self::find()->select(['id','name'])
            ->where(['parent_id' => $id])
            ->asarray()->all();
    }

    //判断是否有子菜单
    public static function leafIsEmpty($id){
        $result=sizeof(self::getChildNode($id));
        if($result) return false;
        else  return true;
    }

    /**
     * 利用英文名称 找到对应的分类
     * @param $lg
     * @param $name
     */
    public static function findByEnglishName($english_name)
    {
        return self::find()->where(["name->'data'->>'en-us'" => $english_name])->one();
    }

    /**
     * 获取所有分类的数据
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCategoryInfo()
    {
        return self::find()->select(['id as key', 'name as title', 'parent_id'])->asArray()->all();
    }

    public static function getPointDataByCate($parent_id)
    {
        $data = self::find()->select(['id', 'name', 'category_id', 'parent_id'])
                            ->where("category_id @> '{".$parent_id."}'")
                            ->andWhere(['not in', ['id'], $parent_id])
                            ->with(['point' => function ($query){
                                                Point::_getPointData($query);
                                            }
                                    ])
                            ->asArray()->all();
        return $data;
//        echo '<pre>';
//        var_dump($data);die;
    }

    /**
     * 找寻所有儿子节点的分类
     * 支持分页
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSubId($parent_id, $offset= '', $limit = '')
    {
        return self::find()->select(['name', 'id'])->where(['parent_id' => $parent_id])->offset($offset)->limit($limit)->asArray()->all();
    }

    /**
     * 查询所选分类的信息
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCategoryById($id)
    {
        $category =  self::find()->where(['id' => $id])->asarray()->one();
        if(!empty($category)){
            $category['name'] = MyFunc::DisposeJSON($category['name']);
        } else {
            $category['name'] = 'Null';
        }
        return $category;
    }

    /**
     * 根据父级id找出所有子节点
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSubCategory($id)
    {
       return static::find()->select(['id', 'name', 'parent_id'])
            ->where("category_id @> '{".$id."}'")  //判断json中是否含有 当前的id
            ->asArray()->all();
    }

    public static function getAllCategory()
    {
        return self::find()
                ->select(['id', 'name', 'parent_id'])
                ->asArray()->indexBy('id')->all();
    }



    /**
     * get the SubCategory's id
     * @param inter $parent_id
     * @return array $subCategoryId 一维数组[
     *                                     subcategory_id_1,
     *                                     subcategory_id_2,
     *                                     subcategory_id_3,
     *                                     subcategory_id_4,
     *                                     ....
     *                                     ]
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     *
     */
    public function getSubCategoryId($parent_id)
    {
        $children = self::find()->select('id,category_id')->where(['parent_id'=>$parent_id])->asArray()->all();
        $subCategoryId = [];
        if(!$children) {
            return $subCategoryId;
        } else {
            foreach ($children as $key => $value) {
                $subCategoryId[$value['id']] = self::getSubCategoryId($value['id']);
            }
        }
        return $subCategoryId;
    }
	
	/**
     * 获取某一分类下所有id
     * @auther pzzrudlf pzzrudlf@gmail.com
	 * @date 2015-12-21
     */
    public function getAllSubCategoryIdsByCategoryId($id)
    {
        $chil =  self::find()->select(['id'])->where("category_id @> '{".$id."}'")->asArray()->all();
        return $chil;
    }

    /**
     * return regenerated variables $query_param that added the key end_time (e.g. $query_param['end_time']) in order to compare with another
     * @param $query_param
     * @return mixed
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public function handleTime($query_param)
    {
        $time_type = isset($query_param['time_type'])?$query_param['time_type']:'month';
        $date_time = isset($query_param['date_time'])?$query_param['date_time']:date('Y-m',time());
        $compareType = $query_param['compare_type'];//0:year on year; 1:month on month
        $end_time = null;
        $timer = 2;

        switch ($time_type) {
            case 'year':
                $temp = $date_time.'-01-01';
                // if ($compareType == 0) {
                    $end_time = date('Y',strtotime("$temp -1 year"));
                $timer = 3;

                // }
                break;
            case 'month':
                if ($compareType == 0) {
                    $end_time = date('Y-m',strtotime("$date_time -1 year"));
                } elseif ($compareType == 1) {
                    $end_time = date('Y-m',strtotime("$date_time -1 month"));
                }
                $timer = 2;

                break;
            case 'day':
                if ($compareType == 0) {
                    $end_time = date('Y-m-d',strtotime("$date_time -1 year"));
                } elseif ($compareType == 1) {
                    $end_time = date('Y-m-d',strtotime("$date_time -1 month"));
                }
                $timer = 1;

                break;
            case 'hour':
                if ($compareType == 0) {
                    $end_time = date('Y-m-d H:i:s',strtotime("$date_time -1 year"));
                } elseif ($compareType == 1) {
                    $end_time = date('Y-m-d H:i:s',strtotime("$date_time -1 month"));
                }
                break;
        }
        $query_param['compare_date_time'] = $end_time;
        $query_param['interval_type'] = ($timer-1);
        return $query_param;
    }

    /**
     * 从点位数据表里直接获取点位数据
     * @param $pointId
     * @param $query_param
     */
    public  function getWantedData($point_ids, $query_param)
    {
//        echo '<pre>';
//        print_r($point_ids);die;
        foreach ($point_ids as $key => $value) {
            $chart = PointData::categoryStatistical($query_param['date_time'], $query_param['time_type'], $value);
        }
        return $chart;
    }


    public function days($time){
        $year=date('Y',strtotime($time));
        $month=date('m',strtotime($time));
        if (in_array($month, array(1, 3, 5, 7, 8, 01, 03, 05, 07,10, 12))) {
            $text = '31';
        }elseif ($month == 2){
            if ($year % 400 == 0 || ($year % 4 == 0 && $year % 100 !== 0)) {        //�ж��Ƿ�������
                $text = '29';
            } else {
                $text = '28';
            }
        } else {
            $text = '30';
        }
        return $text;
    }

    public function get_time($query_param){
//        $query_param=Array
//        (
//
//            "time_type" => 'year',
//            "date_time" => date('Y',time()),
//
//        );
        $time_type=$query_param['time_type'];
        $date_time=$query_param['date_time'];

        $time=array();
        switch($time_type){
            case 'year':
                $year=$date_time;
                for($i=1;$i<=12;$i++)
                    if($i>0 && $i<10)
                    $time[]=$year.'-'.'0'.$i;
                    else   $time[]=$year.'-'.$i;
                break;
            case 'month':
                $days=self::days($date_time);
                $month=$date_time;
                for($i=1;$i<=$days;$i++)
                    if($i>0 && $i<10){
                        $time[]=$month.'-'.'0'.$i;
                    }
                    else
                        $time[]=$month.'-'.$i;
                break;
            case 'day':
                $day=$date_time;
                for($i=0;$i<=23;$i++)
                    $time[]=$day.' '.$i ;
                break;
        }
//        echo '<pre>';
//        print_r($time);
//        die;
        return $time;
    }

}
