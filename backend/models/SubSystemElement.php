<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.sub_system_element".
 *
 * @property integer $id
 * @property integer $sub_system_id
 * @property integer $element_id
 * @property integer $element_type
 * @property string $config
 */
class SubSystemElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_system_id', 'element_id', 'element_type'], 'required'],
            [['sub_system_id', 'element_id', 'element_type'], 'integer'],
            [['config'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_system_id' => 'Sub System ID',
            'element_id' => 'Element ID',
            'element_type' => 'Element Type',
            'config' => 'Config',
        ];
    }


    public static function findModelByData($image_group_id,$element_id)
    {

        $data=self::find()
            ->select('*')
            ->where(['sub_system_id'=>$image_group_id,'element_id'=>$element_id])->asArray()->all();

        if(empty($data)){
            $model=new SubSystemElement();
            return $model;
        }
        else {
            $id = $data[0]['id'];

            return self::findModelById($id);
        }
    }

    public static function findModelById($id)
    {
        if (($model = SubSystemElement::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new SubSystemElement();
            return $model;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
