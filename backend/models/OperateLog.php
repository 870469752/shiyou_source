<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.operate_log".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $timestamp
 * @property string $func_name
 * @property string $description
 */
class OperateLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.operate_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'create_timestamp', 'func_name', 'description'], 'required'],
            [['uid'], 'integer'],
            [['create_timestamp', 'func_name', 'description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', '用户'),
            'create_timestamp' => Yii::t('app', '时间'),
            'description' => Yii::t('app', '操作描述'),
        ];
    }
}
