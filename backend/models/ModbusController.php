<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modbus_device".
 *
 * @property integer $id
 * @property integer $slave_id
 * @property integer $config_id
 * @property integer $is_shield
 * @property integer $is_available
 */
class ModbusController extends \yii\db\ActiveRecord
{
//    public static function  getDb(){
//    return Yii::$app->get('dbsql');
//    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.modbus_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slave_id', 'config_id'], 'required'],
            [['slave_id', 'config_id', 'is_shield', 'is_available'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slave_id' => Yii::t('moubus', 'Slave ID'),
            'config_id' => Yii::t('moubus', 'Config ID'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_available' => Yii::t('app', 'Is Available'),
        ];
    }
}
