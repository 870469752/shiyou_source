<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "core.point_category_config".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $point_id
 * @property boolean $is_skipped
 */
class PointCategroyConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_category_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'point_id'], 'required'],
            [['category_id', 'point_id'], 'integer'],
            [['is_skipped'], 'boolean'],
            [['category_id', 'point_id'], 'unique', 'targetAttribute' => ['category_id', 'point_id'], 'message' => 'The combination of Category ID and Point ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'point_id' => 'Point ID',
            'is_skipped' => 'Is Skipped',
        ];
    }


    /**
     * 通过分类ID查找对应点位，并转化为一维数组
     *
     * @param $category_id 如我要查找属于“电”的点位，传入 [[1]] 即可。数字1代表电。
     * 如果要查找查找是“电”或者“水”并且是“商业楼”内的只要传入 [[1,2],[3]] 数字1，2，3分别代表电、水、商业楼
     * @return array|bool|null
     */
    public static function getCategoryPoint($category_id) {

        if($category_id === [[0]]){ // 如果没有选择就返回空
            //TODO null好还是[]空数组好
            return null;
        }
        foreach($category_id as $arr_id){
            $points = [];

            foreach($arr_id as $id){
                $point_ids=self::find()
                    ->select(['point_id'])
                    ->where(['category_id'=>$id])
                    ->andWhere(['is_skipped'=>'false'])
                    ->asArray()->all();

                if(!empty($point_ids)) {
                    foreach ($point_ids as $value) {
                        $points[]=$value['point_id'];
                    }
                }
            }
            $point_id = isset($point_id)?array_intersect($point_id, $points):$points; // 几种并列条件的交集就是所有条件满足的点位。
        }


        // 如果选择了但是没有点位就FALSE
        return $point_id?$point_id:FALSE;
    }

}
