<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.custom_report".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $point_info
 * @property string $user_id
 * @property string $name
 */
class CustomReport extends \yii\db\ActiveRecord
{

    public $left_point;                     //左轴点位
    public $right_point;                    //右轴点位
    public $end_time;                       //开始时间
    public $start_time;                     //结束时间
    public $start_time_type;                //开始时间类型
    public $end_time_type;                  //结束时间类型
    public $custom_start_time_num;          //自定义开始时间 数值
    public $custom_end_time_num;            //自定义结束时间 数值
    public $custom_start_time_type;         //自定义开始时间 类型
    public $custom_end_time_type;          //自定义结束时间 类型

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.custom_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['timestamp', 'left_point', 'right_point',
              'end_time', 'start_time', 'start_time_type', 'end_time_type',
               'custom_start_time_type', 'custom_end_time_type'], 'safe'],
            [['point_info', 'name', 'time_range'], 'string'],
            [['custom_start_time_num', 'custom_end_time_num'], 'integer'],
            ['custom_start_time_num', 'default', 'value' => 11111]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('custom_report', 'ID'),
            'name' => Yii::t('custom_report', 'Name'),
            'timestamp' => Yii::t('custom_report', 'Timestamp'),
            'point_info' => Yii::t('custom_report', 'Point Info'),
            'user_id' => Yii::t('custom_report', 'User ID'),
            'time_range' => Yii::t('custom_report', 'Time range'),
        ];
    }

    public function customReportSave()
    {
        //point_info
        $this->point_info = MyFunc::createJson(['left_point' => $this->left_point,
                'right_point' => $this->right_point], 'report_point');
        //name
        if($this->isNewRecord) {
            $this->name = MyFunc::createJson($this->name);
        } else {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        $this->time_range = MyFunc::createJson($this, 'time_span');
        //timestamp
        $this->timestamp = date('Y-m-d H:i:s');
        //user_id
        $this->user_id = Yii::$app->user->identity->id;

        return $this->save();
    }

    /**
     * 根据自定报表的id 查找对应点位的数据
     * @param $id
     */
    public static function getCustomPointData($id)
    {
        if(!$point_info = self::getCustomPointInfo($id)){return false;}
        $point_info['point_ids'] = array_merge($point_info['point_id']['right'], $point_info['point_id']['left']);
        return $point_info;
    }

    public static function getCustomPointInfo($id)
    {
        $model = self::findOne(['id' => $id]);

        $custom_point = MyFunc::DisposeJSON($model->point_info);
        if(!empty($custom_point['left_point']) && !empty($custom_point['right_point'])) {
            $point_ids =  ['left' => $custom_point['left_point'], 'right' => $custom_point['right_point']];
        } else if(!empty($custom_point['left_point'])){
            $point_ids = ['left' => $custom_point['left_point'], 'right' => []];
        } else if(!empty($custom_point['left_point'])){
            $point_ids = ['right' => $custom_point['right_point'], 'left' => []];
        } else {
            return false;
        }
        //timerange 处理
        $params = $model->preTimeRange();
        //将参数处理成 custom_report\...\CrudController :: timeProcess 方法能识别的数据
        // 注： 再次将所有的相对时间 归为自定义 以便处理
        //处理开始时间参数
        return ['point_id' => $point_ids,
                'report_name' => MyFunc::DisposeJSON($model->name),
                'params' => $params];
    }

    /**
     * 专门处理 绝对、相对时间范围
     */
    public function preTimeRange()
    {
        $params = [];       //创建一个参数 数组
        $this->time_range = MyFunc::DisposeJSON($this->time_range);
        if($start_time = $this->time_range['start']) {
            $this->start_time_type = isset($start_time['type']) ? $start_time['type'] : '';
            $params['start_time_type'] = $this->start_time_type;
            //判断开始时间类型
            if($this->start_time_type == 'relative') {
                //如果是 相对时间类型 需要进一步处理
                $this->custom_start_time_type = isset($start_time['unit']) ? $start_time['unit'] : '';
                $this->custom_start_time_num = isset($start_time['value']) ? $start_time['value'] : '';
                $this->start_time = [$this->custom_start_time_num, $this->custom_start_time_type];
                $params['start_time'] = 'custom';
                $params['custom_start_time_type'] = $this->custom_start_time_type;
                $params['custom_start_time_num'] = $this->custom_start_time_num;
            } else {
                $this->custom_start_time_num = isset($start_time['timestamp']) ? $start_time['timestamp'] : '';
                $params['start_time'] = $this->custom_start_time_num;
            }
        }
        //处理结束时间范围
        if($end_time = $this->time_range['end']) {
            $this->end_time_type = isset($end_time['type']) ? $end_time['type'] : '';
            $params['end_time_type'] = $this->end_time_type;
            //判断结束时间类型
            if($this->end_time_type == 'relative') {
                //如果是 相对时间类型 需要进一步处理
                $this->custom_end_time_type = isset($end_time['unit']) ? $end_time['unit'] : '';
                $this->custom_end_time_num = isset($end_time['value']) ? $end_time['value'] : '';
                $this->end_time = [$this->custom_end_time_num, $this->custom_end_time_type];
                $params['end_time'] = 'custom';
                $params['custom_end_time_type'] = $this->custom_end_time_type;
                $params['custom_end_time_num'] = $this->custom_end_time_num;
            } else {
                $this->custom_end_time_num = isset($end_time['timestamp']) ? $end_time['timestamp'] : '';
                $params['end_time'] = $this->custom_end_time_num;
            }
        }

        return $params;
    }
}
