<?php

namespace backend\models;

use common\library\MyFunc;
use Yii;
use yii\helpers\ArrayHelper;
use common\library\MyArrayHelper;
use yii\helpers\BaseArrayHelper;
/**
 * This is the model class for table "core.unit".
 *
 * @property integer $id
 * @property string $name
 * @property integer $standard_unit
 * @property string $coefficient
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'standard_unit', 'coefficient'], 'required'],
            [['standard_unit'], 'string'],
            [['flag'],'boolean'],
            [['category_id'],'integer'],
            [['unit_symbol'],'string'],
            ['name', 'string', 'max' => 50],
            ['name', 'unique', 'targetClass' => '\backend\models\Unit', 'message' => 'This username has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'category_id' => Yii::t('app', 'Category Id'),
            'standard_unit' => Yii::t('app', 'Standard Unit'),
            'coefficient' => Yii::t('app', 'Coefficient'),
        ];
    }

    /**
     * 获取 所有 标准单位
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getStandardUnit()
    {
        return self::find()->where(['coefficient' => 1])->andWhere('id=standard_unit')->asarray()->all();
    }

    /**
     *  判断 单位是否为  标准单位 以什么形式 进行处理
     **/
    public function saveUnit()
    {
        //处理名字

//        if(is_null(json_decode($this->name))){
//
//
//            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
//
//        }

        $result = $this->save();
//        // 如果当前 添加单位 与 标准单位 一致 就代表是 标准单位的添加
//        if($this->name == $this->standard_unit)
//        {
//            /* 先将当前 添加的单位存到 数据库
//                再将新添加的 id号 更新到数据库中*/
//            $this->standard_unit = $this->id;
//            $this->coefficient = 1;
//            $result = $this->save();
//
//        }

        return [
            'id'=>$this->id,
            'result'=>$this->save(),
        ];
    }

    /**
     * 将单位归类： 以 标准单位的键名 进行归类
     *      [
     *          ['kw' =>
     *                  [
     *                      'id' => 1,
     *                      'unit' => 'kw'
     *                  ],
     *                  [
     *                      'id' => 2,
     *                      'unit' => 'w'
     *                  ]
     *          ],
     *          ['kg' =>
     *                  [
     *                      'id' => 3,
     *                      'unit' => 'g'
     *                  ]
     *          ]
     *      ]
     * @return array  返回一个 归类后 去掉键名的 二维数组
     */
    public static function getAllUnit()
    {
        //获取 所有单位
        $data = self::find()->asarray()->all();
        $unit = BaseArrayHelper::map($data, 'id', 'name');
        //处理name格式
        foreach($unit as $key=>$value){
            $unit[$key]=MyFunc::DisposeJSON($unit[$key]);
        }
//        /******************************   将单位 以其对比的标准单位 进行 归类 ****************************/
//        $res = [];
//        foreach($data as $key => $value)
//        {
//            $res[$value['standard_unit']][] = [  'id' => $value['id'],
//                'unit' => $value['name']];
//        }
//
//        /*********************************       去掉键名          **************************************/
//        $_res = [];
//        //获取 所有标准单位
//        $standard_unit = ArrayHelper::map(self::getStandardUnit(), 'id', 'name');
//        foreach($res as $key => $value)
//        {
//            foreach($value as $v)
//            {
//                //将归类键值id 转为 当前 标准单位name
//                $_res[$standard_unit[$key] . '(单位组)'][$v['id']] = $v['unit'];
//            }
//        }
        $_res=$unit;
        return $_res;
    }

    public static function getById($id)
    {
        return self::findOne($id);
    }

    public static function getByPointId($id)
    {
        //处理 id
        if(!is_array($id)) {
            if($id == ''){
                return '';
            }else if(strpos($id , ',')){
                $id = explode(',', $id);
            }
        }
        $unit_id = MyArrayHelper::getColumn(Point::find()->select('unit')->where(['id' => $id])->asArray()->all(), 'unit');
        $unit_name = MyArrayHelper::map(self::find()->select('name, id')->where(['id' => $unit_id])->asArray()->all(), 'id', 'name');
        return $unit_name;
    }
}