<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.info_item".
 *
 * @property integer $id
 * @property string $data
 * @property integer $info_group_id
 */
class InfoItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.info_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'string'],
            [['info_group_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data'),
            'info_group_id' => Yii::t('app', 'Info Group ID'),
        ];
    }

    public function getGroup()
    {
        return $this->hasOne(InfoGroup::className(), ['id' => 'info_group_id']);
    }
}
