<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.access_controller".
 *
 * @property integer $id
 * @property string $name
 */
class AccessController extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.access_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '门禁设备编号',
            'name' => '门禁设备名',
        ];
    }
}
