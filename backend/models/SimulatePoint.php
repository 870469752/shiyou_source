<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;

/**
 * This is the model class for table "protocol.simulate_point".
 *
 * @property integer $id
 * @property boolean $is_shield
 * @property string $interval
 * @property string $name
 * @property string $protocol_id
 * @property string $last_update
 * @property string $categories
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_upload
 * @property string $time_range
 * @property string $value
 */
class SimulatePoint extends \yii\db\ActiveRecord
{
    public $time_type;
    public $filter_max_value;
    public $filter_min_value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.simulate_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_shield', 'is_available', 'is_upload'], 'boolean'],
            ['name', 'filter', 'filter' => 'trim'],
            [['name', 'protocol_id', 'last_update', 'value_pattern', 'time_type'], 'string'],
            [['name', 'value_pattern' ], 'required'],
            [['interval', 'unit', 'filter_max_value', 'filter_min_value'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'interval' => Yii::t('app', 'Record Interval'),
            'name' => Yii::t('app', 'Name'),
            'protocol_id' => Yii::t('app', 'Protocol ID'),
            'last_update' => Yii::t('app', 'Last Update'),
            'categories' => Yii::t('app', 'Categories'),
            'base_value' => Yii::t('app', 'Base Value'),
            'base_history' => Yii::t('app', 'Base History'),
            'is_available' => Yii::t('app', 'Is Available'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'time_type' => Yii::t('app', 'Repeat Types'),
            'value_pattern' => Yii::t('app', 'Trigger Time Range'),
            'unit' => Yii::t('app', 'Unit'),
            'filter_max_value' => Yii::t('app', 'Filter Max Value'),
            'filter_min_value' => Yii::t('app', 'Filter Min Value')
        ];
    }

    public function SimulateSave()
    {
        $this->protocol_id = '7';
        $this->last_update = date("Y-m-d h:i:s");
        $_his = array(
            'start' => $this->last_update,
            'end' => null,
            'value' => $this->base_value
        );
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name, 'description', $this->is_system ? 'system' : '');
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name'], $this->is_system ? 'system' : '');
        }
        if(empty($this->value_pattern))
        {
            return $this->save();
        }
        $value_patten['type'] = $this->time_type;
        $this->value_pattern = trim($this->value_pattern, ';');
        $time_arr = explode(';', $this->value_pattern);
        foreach($time_arr as $key => $value)
        {
            $value = trim($value, ',');
            $_time = explode(',', $value);
            if(!isset($_time[0]) || empty($_time[0]))
                continue;
            $value_patten['value'][$key]['start'] = $_time[0];
            $value_patten['value'][$key]['end'] = !empty($_time[1]) ? $_time[1] : $_time[0];
            $value_patten['value'][$key]['formula'] = $_time[2];
        }
        $this->value_pattern = MyFunc::createJson($value_patten, 'value_pattern');

        //处理过滤值
        if(isset($this->filter_min_value) && isset($this->filter_max_value)) {
            $filter_data = ['min' => $this->filter_min_value,
                            'max' => $this->filter_max_value];
            $this->value_filter = MyFunc::createJson($filter_data, 'value_filter');
        }

        return $this->save();
    }

    public static function getSystemPointByName($sys_name)
    {
        return  self::find()->where("name->'data'->>'system' like '" .$sys_name."%'")->one();
    }
}
