<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.subsystem_manage".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_type
 * @property integer $parent_id
 * @property string $category_id
 * @property string $rgba
 * @property string $location_type
 * @property integer $image_id
 * @property string $points
 * @property string $role_id
 */
class SubSystemManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.subsystem_manage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'location_type', 'role_id'], 'string'],
         //   [['category_type'], 'required'],
            [['category_type', 'parent_id', 'image_id'], 'integer'],
            [['rgba'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_type' => 'Category Type',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
            'rgba' => 'Rgba',
            'location_type' => 'Location Type',
            'image_id' => 'Image ID',
            'role_id'=>'Role ID',
        ];
    }

    /*
 * ���ҽ�ɫ
 */
    public function findRole()
    {
        $role=Role::find()->select(["id","name->'data'->>'zh-cn' as name" ])->asArray()->all();
        $roleArray=array();
        $roleArray['']='';

        for($i=0;$i<count($role);$i++)
        {
            $roleArray[$role[$i]['id']]=$role[$i]['name'];
        }
        /*  echo "<pre>";
          print_r($roleArray);
          die;*/
        return $roleArray;
    }
    /*
 * ����û���ɫID���ұ���
 */
    public function getSubsystems($user_roleId)
    {
        /*    return ReportManage::find()
                ->select(['id','title'=>'name','parent_id','role_id'])
                ->where(['array_length(role_id, 1)'=>3])
                ->OrderBy('id desc')
                ->asArray()->all();*/
//        $sql="SELECT id,name as title,parent_id,role_id FROM core.subsystem_manage ".
//            "where ".$user_roleId."= ANY (role_id) ";
        $sql="SELECT id,name as title,parent_id,role_id FROM core.subsystem_manage ";
        return self::findBySql($sql) ->asArray()->all();
    }
}
