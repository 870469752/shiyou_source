<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use common\library\MyStringHelper;

/**
 * This is the model class for table "core.point_data_statistic".
 *
 * @property integer $id
 * @property integer $point_id
 * @property string $interval_type
 * @property string $value_type
 * @property string $value
 * @property string $timestamp
 */
class PointDataStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_data_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'interval_type', 'value_type', 'value'], 'required'],
            [['interval_type', 'value_type', 'value','point_id'], 'integer'],
            [['timestamp'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point_id' => 'Point ID',
            'interval_type' => 'Interval Type',
            'value_type' => 'Value Type',
            'value' => 'Value',
            'timestamp' => 'Timestamp',
        ];
    }

    //获取$query_param时间内的所有点位data数据
    public function getPointdata($query_param){

//        $query_param=[
//            'time_type'=>'day',
//            'date_time'=>'2016-06-06',
//            'Point'=>[
//                'category_id'=>29
//            ]
//        ];
//        echo '<pre>';
//        print_r($query_param);
//        die;
        if(empty($query_param)) {
            $start_time=date('Y',time()).'-01-01 00:00:00';
            $end_time  =date('Y',time()).'-12-31 23:59:59';
            $sql='select point_id,"sum"("value")as "value"'.
                'from core.point_data_statistic '.
                'where '.
                'interval_type=3 '.
                'and value_type=5 '.
                'and timestamp>=\''.$start_time.'\' '.
                'and timestamp<=\''.$end_time.'\' '.
                'GROUP BY point_id';

            $all_value=self::findBySql($sql)->asArray()->all();

        }
        else {
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $start_time=$date_time.'-01-01 00:00:00';
                    $end_time  =$date_time.'-12-31 23:59:59';
                    $interval_type=3;
                    break;
                case 'month':
                    $start_time=$date_time.'-01 00:00:00';
                    $end_time  =$date_time.'-'.date('t',strtotime($date_time.'-01')).' 23:59:59';
                    $interval_type=2;
                    break;
                case 'day':
                    $start_time=$date_time.' 00:00:00';
                    $end_time  =$date_time.' 23:59:59';
                    $interval_type=1;
                    break;
            }
            //地理位置分类sql
            $point_in_sql=' ';
            if(isset($query_param['Point'])) {
                $category_id = $query_param['Point']['category_id'];
                if($category_id!='')
                $point_in_sql='and point_id in'.
                    '('.
                    'SELECT point_id '.
                    'from core.point_category_config '.
                    'where category_id='.$category_id.' '.
                    ')';
            }
//            $start_time1=microtime(true);
                $sql='select point_id,"sum"("value")as "value"'.
                            'from core.point_data_statistic '.
                            'where '.
                            'interval_type='.$interval_type.' '.
                            'and value_type=5 '.
                            $point_in_sql.
                            'and timestamp>=\''.$start_time.'\' '.
                            'and timestamp<=\''.$end_time.'\' '.
                            'GROUP BY point_id';

                $all_value=self::findBySql($sql)->asArray()->all();
//                $end_time1=microtime(true);
//                echo $end_time1-$start_time1;
//                die;


        }

        return $all_value;
    }


    //分类查找数据 并按时间分类
    public function GetDataGroupByTime($query_param){
        $all_value=null;
        if(empty($query_param)) {
            $group_date="to_char(timestamp, 'yyyy')";
            $all_value=self::find()->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])->groupBy(['point_id,'.$group_date])->asArray()->all();
        }
        else {
            $time_type = $query_param['time_type'];
            $date_time = (string)$query_param['date_time'];
            switch ($time_type) {
                case 'year':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM')";
                    $interval_type=3;
                    break;
                case 'month':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD')";
                    $interval_type=2;
                    break;
                case 'day':
                    $group_date="TO_CHAR(TIMESTAMP,'YYYY-MM-DD hh24')";
                    $interval_type=1;
                    break;
            }
            $all_value=self::find()
                ->select(['point_id','sum(value) as value,'.$group_date.'as timestamp'])
                ->andWhere(['interval_type'=>$interval_type])
                ->andWhere(['value_type'=>5])
                ->groupBy(['point_id,'.$group_date])
                ->asArray()->all();
//              year  month day
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY')='" . $date_time . "'";
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM')='" . $date_time . "'";
//              $date_formate = "TO_CHAR(TIMESTAMP,'YYYY-MM-DD')='" . $date_time . "'";
        }
        return $all_value;
    }


    //生成随机时间函数
    public static function rand_time($start_time,$end_time){

        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time);
        return date('Y-m-d H:i:s', mt_rand($start_time,$end_time));
    }

    /**
     * @param $interval_type
     * @param $value_type
     * @param $time_date
     * @param $time_type
     * @param $point_id
     * @param string $time_format
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public static function categoryStatistical($interval_type, $value_type, $time_date, $time_type, $point_id, $time_format = '')
    {
        $format_time = MyFunc::TimeFloor($time_date, $time_type);
        $_format[] = $format_time;
        $_where[] = "timestamp between '{$format_time['start']}' and '{$format_time['end']}'";
        $_where = isset($_where) && !empty($_where) ? implode(' or ', $_where) : '';

        $date_format = $time_format == '' ? MyStringHelper::getFormatTimeDefault($time_type) : MyStringHelper::getFormatTime($time_type);

        $dataProvider = PointDataStatistic::find()->select([$date_format, 'min(point_id) as s_id', 'sum(value) as value'])
            ->Where(['point_id'=>$point_id])
            ->andWhere($_where)
            ->andWhere(['interval_type'=>$interval_type])
            ->andWhere(['value_type'=>$value_type])
            ->groupBy(['_timestamp'])
            ->orderBy('_timestamp');

        $data = $dataProvider->asArray()->all();
        return ['data' => $data,
            'format' => $_format,
            'dataProvider' => $dataProvider];
    }

    /**
     * @param $interval_type
     * @param $value_type
     * @param $time_date
     * @param $time_type
     * @param $point_id
     * @param string $time_format
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public static function categoryStatisticalCustom($interval_type, $value_type, $time_date, $time_type, $point_id, $time_format = '')
    {
        $format_time = MyFunc::TimeFloor($time_date, $time_type);
        $_format[] = $format_time;
        $_where[] = "timestamp between '{$format_time['start']}' and '{$format_time['end']}'";
        $_where = isset($_where) && !empty($_where) ? implode(' or ', $_where) : '';
        $date_format = $time_format == '' ? MyStringHelper::getFormatTimeDefault($time_type) : MyStringHelper::getFormatTime($time_type);
        $dataProvider = PointDataStatistic::find()->select(['point_id','sum(value) as value'])
            ->Where(['point_id'=>$point_id])
            ->andWhere($_where)
//            ->andWhere(['interval_type'=>$interval_type])
            ->andWhere(['value_type'=>$value_type])
            ->addGroupBy(['point_id']);
        $data = $dataProvider->asArray()->all();
        return ['data' => $data,
            'format' => $_format,
            'dataProvider' => $dataProvider];
    }
}
