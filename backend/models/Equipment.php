<?php

namespace backend\models;

use Yii;
use backend\models\EquipmentPointRelation;
use backend\models\Point;
use common\library\MyFunc;
use backend\models\EquipmentImageBaseRelation;
use backend\models\EquipmentImageGroupRelation;
/**
 * This is the model class for table "core.equipment".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $add_time
 * @property string $is_del
 * @property string $del_time
 * @property string $description
 */
class Equipment extends \yii\db\ActiveRecord
{
    public $point_ids;
    public $image_base_id;
    public $image_group_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'point_ids'], 'required'],
            [['is_available','sequence','address'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('equipment', 'Equipment name'),
            'type' => Yii::t('equipment', 'Type'),
            'sequence' => Yii::t('equipment', 'sequence'),
            'address'=>Yii::t('equipment', 'Address'),
            'point_ids' => Yii::t('equipment', 'Relation point'),
            'image_base_id' => Yii::t('equipment', 'Relation Image Base'),
            'image_group_id' => Yii::t('equipment', 'Relation Image Group'),
        ];
    }
    /**
     * save equipment_point_relation
     */
    public function savePointRelation(){
        $PointRelation = new EquipmentPointRelation;
        foreach($this->point_ids as $attr){
            $_model = clone $PointRelation;
            $_model->setAttribute('equipment_id',$this->id);
            $_model->setAttribute('point_id',$attr);
            $_model->save();
        }
    }
    /**
     * save equipment_image_base_relation
     */
    public function saveImageBaseRelation(){
        $ImgBase = new EquipmentImageBaseRelation;
            $_ImgBase = clone $ImgBase;
            $_ImgBase->setAttribute('equipment_id',$this->id);
            $_ImgBase->setAttribute('image_base_id',$this->image_base_id);
            $_ImgBase->save();
    }
    /**
     * save equipment_image_group_relation
     */
    public function saveImageGroupRelation(){
        $ImgGroup = new EquipmentImageGroupRelation;
        foreach($this->image_group_id as $attr) {
            $_ImgGroup = clone $ImgGroup;
            $_ImgGroup->setAttribute('equipment_id', $this->id);
            $_ImgGroup->setAttribute('image_group_id', $attr);
            $_ImgGroup->save();
        }
    }
    /**
     * 获取点位
     * @return array
     */
    public function getAllPoint(){
        $point_info = point::findAllPoint();
        $point_all = [];
        foreach($point_info as $k => $v){
            $point_all[$v['id']] = MyFunc::DisposeJSON($v['name']);
        }
        return $point_all;
    }
    /**
     * 获取所有底图
     * @return array
     */
    public function getAllImageBase(){
        $img_base = ImageBase::getAllImageBase();
        $img_base_all = [];
        foreach($img_base as $v){
            $img_base_all[$v['id']] = $v['name'];
        }
        return $img_base_all;
    }
    /**
     * 获取所有图组
     * @return array
     */
    public function getAllImageGroup(){
        $img_group = ImageGroup::getAllImageGroup();
        $img_group_all = [];
        foreach($img_group as $v){
            $img_group_all[$v['id']] = $v['name'];
        }
        return $img_group_all;
    }

}
