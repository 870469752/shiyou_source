<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_category_rel_new".
 *
 * @property integer $id
 * @property integer $point_id
 * @property integer $category_type
 * @property integer $category_id
 */
class PointCategoryRelNew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_category_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'point_id', 'category_id'], 'required'],
            [[  'point_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'point_id' => 'Point ID',
//            'category_type' => 'Category Type',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @param $point_id
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @date 2015-12-20
     */
    public static function getPointCategoryId($point_id)
    {
        return self::find()->select('category_id')->where(['point_id' => $point_id])->asArray()->all();
    }
}
