<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.coologic_controller".
 *
 * @property integer $id
 * @property string $location
 * @property string $port_num
 */
class Coologic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.coologic_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['port_num'], 'string'],
            [['location'], 'string', 'max' => 127]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'location' => Yii::t('app', 'Location'),
            'port_num' => Yii::t('app', 'Port Num'),
        ];
    }

    /**
     * 获取 所有 协议
     * @return array|static[]
     */
    public static function getAll()
    {
        return self::find()->asarray()->all();
    }
}
