<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.auth_assignment".
 *
 * @property string $item_name
 * @property string $user_id
 * @property integer $created_at
 */
class AuthAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.auth_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['created_at'], 'integer'],
            [['user_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app', 'Item Name'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * 获取操作权限的 类型
     * huoqu
     * @param $userId
     * @param $con
     * @return static
     */
    public static function getOperate($userId, $con)
    {
        $obj = AuthAssignment::findModelByUid($userId);
        $res = AuthChild::findByParentChild($obj->item_name, $con);
        return $res;


    }

    /**
     * 利用 user_id 查找 对应的记录
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public static function findModelByUid($userId)
    {
        return AuthAssignment::findOne(['user_id' => $userId]);
    }

    /** 
     * 更新 权限操作
     * @param  [type] $old_id [description]
     * @param  [type] $new_id [description]
     * @return [type]         [description]
     */
    public static function updateUid($old_id, $new_id)
    {
        return AuthAssignment::updateAll(['user_id' => $new_id],['user_id' => $old_id]);
    }

    /**
     * 利用 user_id 删除对应的记录
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function deleteByUid($id)
    {
        $auth = Yii::$app->authManager;
        return $auth->revokeAll($id);
         
    }

    /**
     * 根据 user_id 跟新 记录
     * @return [type] [description]
     */
    public function updateByUid()
    {
        AuthAssignment::deleteByUid($this->user_id);
        $auth = Yii::$app->authManager;
        foreach($this->item_name as $value){
            $role = $auth->createRole($value);
            $userId = $this->user_id;
            $auth->assign($role, $userId);
        }
        return true;
    }

    /** 
     * 通过user_id 找到所有关联的角色
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    public static function findAllRole($uid)
    {
        $auth = Yii::$app->authManager;
        $objs = $auth->getRolesByUser($uid);
        $roles = [];
        foreach($objs as $obj){
            $roles[] = $obj->name;
        }
        return $roles;
    }
}
