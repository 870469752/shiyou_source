<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_control".
 *
 * @property integer $id
 * @property string $time
 * @property integer $point_id
 * @property string $value
 */
class PointControl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time', 'point_id', 'value'], 'required'],
            [['point_id'], 'integer'],
            [['value'], 'string'],
            [['time'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'point_id' => 'Point ID',
            'value' => 'Value',
        ];
    }

    /**
     * 更新数据 并保存
     * @return bool
     */
    public function saveEvent()
    {
        return  $this->insert() ? $this->id : false;
    }

    public function _delete ($id){
        $sql = "delete from core.point_control where id = $id";
        $result = Yii::$app->db->createCommand($sql);
        $query = $result->queryAll();
        return true;
    }
}
