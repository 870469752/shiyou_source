<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.pictures".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $path
 */
class Pictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.pictures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'path' => Yii::t('app', 'Path'),
        ];
    }

    public function getPicturesGroup()
    {
        return $this->hasMany(PicturesGroup::className(), ['pictures_id' => 'id']);
    }
}
