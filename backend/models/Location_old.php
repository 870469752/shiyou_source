<?php

namespace backend\models;

use Yii;
use yii\helpers\BaseArrayHelper;
use common\library\MyFunc;
/**
 * This is the model class for table "core.location".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $parent_id
 * @property string $category_id
 * @property integer $image_id
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',  'category_id','rgba'], 'string'],
            [['parent_id', 'image_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
            'image_id' => 'Image ID',
        ];
    }


    /**
     * return single instance
     */
    public function getOne($id)
    {
        return self::find()->where(['id'=>$id])->asArray()->one();
    }

    /**
     * 获取所有分类的数据
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCategoryInfo()
    {
        return self::find()->select(['id as key', 'name as title', 'parent_id'])->asArray()->all();
    }


    public static function getAllCategory()
    {
        return self::find()
            ->select(['id', 'name', 'parent_id'])
            ->asArray()->indexBy('id')->all();
    }

    /**
     * 根据父级id找出所有子节点
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSubCategory($id)
    {
        return static::find()->select(['id', 'name', 'parent_id'])
            ->where("category_id @> '{".$id."}'")  //判断json中是否含有 当前的id
            ->asArray()->all();
    }

	 /**
     * 根据多个父级id找出所有子节点
     * @param $ids array
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getManySubCategory($ids)
    {
        $category_data=self::getCategoryInfo();
        foreach ($category_data as $key=>$value) {
            $value['category_id']=substr($value['category_id'],1,strlen($value['category_id'])-2);
            $category_data[$key]['category_id'] = explode(',',$value['category_id']);
            $intersection_ids=array_intersect($ids,$category_data[$key]['category_id']);
//            $intersection_ids=array_intersect([1,2,3],[5]);
            if(!empty($intersection_ids))
            {
                 $location_ids[]=$value['key'];
            }
        }
 
        return  $location_ids;
    }
    public static function getChildrenById($id){
        $children=self::find()->where(['parent_id'=>$id])->asArray()->all();
        $children=BaseArrayHelper::map($children, 'id', 'name');
        foreach($children as $key=>$value) {
            $children[$key] = MyFunc::DisposeJSON($children[$key]);
        }
        return $children;
    }

    //获取最顶层分类信息
    public static function getTopnode(){
        $node_data= self::find()->select(['id','name'])->where(['parent_id'=>null])->andWhere('id <> 0')->asArray()->all();
        $node_data = BaseArrayHelper::map($node_data, 'id','name');
        foreach($node_data as $key=>$value) {
            $node_data[$key] = MyFunc::DisposeJSON($node_data[$key]);
        }
        return $node_data;
    }
    public static function getChildNode($id){
        return self::find()->select(['id','name'])
            ->where(['parent_id' => $id])
            ->asarray()->all();
    }
    //判断是否有子菜单
    public static function leafIsEmpty($id){
        $result=sizeof(self::getChildNode($id));
        if($result) return false;
        else  return true;
    }

    //根据$category_id 查找本分类下的数据  按给定的时间 年月日 分类
    public function category_column_data($all_value,$category_id){
        $category=new Category();
        //$all_value=$this->GetDataGroupByTime($query_param);
        $point_ids = $category->findCategoryPoint([[$category_id]]);

        $data=array();
        $last_data=array();
        //TODO 两个数组循环查找  未想到改进方法
        if(!empty($point_ids)) {
            //去掉重复值
            $point_ids=array_unique($point_ids);
            //总值计算
            foreach ($point_ids as $points_key => $points_value) {
                //如果 $point_id属于此分类 则
                foreach($all_value as $key=>$value){
                    if($points_value==$value['point_id']){
                        if(array_key_exists($value['timestamp'],$data)){
                            $data[$value['timestamp']]=$data[$value['timestamp']]+$value['value'];
                        }
                        else $data[$value['timestamp']]=$value['value'];
                    }
                }
            }
            //处理为categories[]  chart_data[];
//            foreach($data as $key=>$value){
//                $last_data['categories'][]=$key;
//                $last_data['chart_data'][]=$value;
//            }
            return $data;
        }else return $data;
    }
    //获取树状data数据
    public function getPointdata($query_param){

        $category=new Category();
        $location_id=array();
        //获取所有符合时间的点位数据
        $all_value=$category->getPointdata_($query_param);
        $value_with_time=$category->GetDataGroupByTime($query_param);
        $all_value=BaseArrayHelper::map( $all_value, 'point_id', 'value');
        $category_ids =Location::getTopnode();
        if(!empty($query_param)) {
            foreach($query_param['Point'] as $category_value){
                $location_id[]=[$category_value];
            }
        }
        $categorydata=array();
        //处理为树状结构
        foreach ($category_ids as $ids_key => $category_name) {
            $categorydata[$ids_key] = [
                'name' => $category_name,
                'value' => self::getCategoryDataById($ids_key,$all_value,$location_id),
                'parent' => $ids_key,
                'children' => self::getNextCategotyValue($ids_key,$all_value,$value_with_time,$location_id),
                'detail'=>self::category_column_data($value_with_time,$ids_key)
            ];
        }
        return $categorydata;
    }

    /**
     * @param $query_param
     * @return array
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function getPointdataName($query_param){

        $category=new Category();
        $location_id=array();
        //获取所有符合时间的点位数据
        $all_value=$category->getPointdata_($query_param);
        $value_with_time=$category->GetDataGroupByTime($query_param);
//        echo '<pre>';
//        print_r($all_value);
//        echo '<hr>';
//        print_r($value_with_time);
//        echo '<hr>';
        $all_value=BaseArrayHelper::map( $all_value, 'point_id', 'value');
//        print_r($all_value);die;
        $category_ids =Location::getTopnode();
        if(!empty($query_param)) {
            foreach($query_param['Point'] as $category_value){
                $location_id[]=[$category_value];
            }
        }
        $categorydata=array();
        //处理为树状结构
        foreach ($category_ids as $ids_key => $category_name) {
            $categorydata[$ids_key] = [
                'name' => $category_name,
//                'value' => self::getCategoryDataById($ids_key,$all_value,$location_id),
//                'parent' => $ids_key,
                'children' => self::getNextCategotyName($ids_key,$all_value,$value_with_time,$location_id),
//                'detail'=>self::category_column_data($value_with_time,$ids_key)
            ];
        }
        return $categorydata;
    }

    public function data_with_location($query_param){
        $all_value=$this->getPointdata_($query_param);
        $all_value=BaseArrayHelper::map( $all_value, 'point_id', 'value');

        $data=self::getCategoryDataById(170,$all_value,[[213]]);
        return $data;
    }


    //得到下属的分类用量 y['category_id'=>['name'=>,'value'=>,'children'=>[]]  ,....]
    public function getNextCategotyValue($category_id,$all_value,$value_with_time,$location_id){
        $data=null;
//        echo $category_id;
//        die;
        $categorys=self::getChildrenById($category_id);
        //查找下属点位计算电量
        foreach($categorys as $categorys_key=>$categorys_value) {
            $data[$categorys_key]=[
                'name'=>$categorys_value,
                'value'=>self::getCategoryDataById($categorys_key,$all_value,$location_id),
                'parent'=>$category_id,
                'children'=>null,
                'detail'=>self::category_column_data($value_with_time,$categorys_key)
            ];
            //如果叶子不为空 继续下级
            if(!self::leafIsEmpty($categorys_key)) {
                $aaa=self::getNextCategotyValue($categorys_key,$all_value,$value_with_time,$location_id);
                $data[$categorys_key]['children']=$aaa;
            }
        }

        return $data;
    }

    /**
     * @param $category_id
     * @param $all_value
     * @param $value_with_time
     * @param $location_id
     * @return null
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function getNextCategotyName($category_id,$all_value,$value_with_time,$location_id){
        $data=null;
//        echo $category_id;
//        die;
        $categorys=self::getChildrenById($category_id);
        //查找下属点位计算电量
        foreach($categorys as $categorys_key=>$categorys_value) {
            $data[$categorys_key]=[
                'name'=>$categorys_value,
//                'value'=>self::getCategoryDataById($categorys_key,$all_value,$location_id),
//                'parent'=>$category_id,
                'children'=>null,
//                'detail'=>self::category_column_data($value_with_time,$categorys_key)
            ];
            //如果叶子不为空 继续下级
            if(!self::leafIsEmpty($categorys_key)) {
                $aaa=self::getNextCategotyName($categorys_key,$all_value,$value_with_time,$location_id);
                $data[$categorys_key]['children']=$aaa;
            }
        }

        return $data;
    }

    //得到id=$id 类以及符合$query_parm时间的用量   再加上要符合相应地理位置ing
    //$id int;为地理位置
    //$enerygy_id array;为能源分类
    public function getCategoryDataById($id,$all_value,$enerygy_id){
//        echo '<pre>';
//        print_r($location_id);
//        die;
        $category=new Category();
        $data=0;
        //处理location_id 内的category_id为[[id1],[id2],...]格式
        $energy_point_ids = $category->getCategoryPoint([[$id]],2);
        if(empty($enerygy_id))
            $location_point_ids = $category->getCategoryPoint([$enerygy_id],1);
        else $location_point_ids = $category->getCategoryPoint($enerygy_id,1);
        if(empty($location_id[0][0]))$point_ids=$energy_point_ids;

        else $point_ids=array_intersect($energy_point_ids,$location_point_ids);


        if(!empty($point_ids)) {
            //去掉重复值
            $point_ids=array_unique($point_ids);
//            print_r($point_ids);
            //总值计算
            foreach ($point_ids as $points_key => $points_value) {
                //$value = Point::find()->select(['last_value'])->where(['id' => $points_value])->asarray()->all();
                if(!array_key_exists($points_value,$all_value))
                    $data=$data;
                else {
                    $data = $data + $all_value[$points_value];
//                    print_r($all_value[$points_value]);
                }
            }
            return $data;
        }else return $data=0;
    }


    /**
     * get the category_id
     * @auther pzzrudlf pzzrudlf@gmail.com
     */
    public function getLevelData($dataGroupByCategoryId)
    {
        $temp = [];
        foreach ($dataGroupByCategoryId as $subSystemCategoryId => $value) {
            foreach ($value as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    if ($key2 == 'location_id') {
                        $temp[$subSystemCategoryId][$key1] = $this->getOne($value2);
                    }
                }
            }
        }
        return $temp;
    }

    public static function IsHaveChildren($id)
    {
        if (self::find()->select('*')->where(['parent_id'=>$id])->asArray()->all()) {
            return true;
        } else {
            return false;
        }
    }

    public static function IsHaveParent($id)
    {
        if ($pid = self::find()->select('parent_id')->where(['id'=>$id])->asArray()->column()) {
            return $pid;
        } else {
            return false;
        }
    }
}
