<?php

namespace backend\models;

use common\library\MyFunc;
use common\models\User;
use Yii;

/**
 * This is the model class for table "core.custom_feature".
 *
 * @property integer $id
 * @property string $url
 * @property string $description
 * @property string $param
 * @property integer $user_id
 * @property string $timestamp
 */
class CustomFeature extends \yii\db\ActiveRecord
{
    public function init(){
        $this->timestamp = date('Y-m-d H:i:s');
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.custom_feature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'param', 'timestamp'], 'string'],
            [['user_id'], 'integer'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    public function fields()
    {
        return [
            'url',
            'description' => function(){
                    return MyFunc::createJson($this->description);
                },
            'param' => function(){
                    parse_str($this->param, $arr);
                    return json_encode(['data_type' => 'url_param', 'data' => $arr], JSON_UNESCAPED_UNICODE);
                },
            'user_id',
            'timestamp'
        ];
    }

    public function getLink()
    {
        return $this->url.'?'.http_build_query(json_decode($this->param, true)['data']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', '链接'),
            'description' => Yii::t('app', '报表名称'),
            'param' => Yii::t('app', 'Param Config'),
            'menu' => Yii::t('app', '保存菜单'),
            'user' => Yii::t('app', '用户名'),
            'timestamp' => Yii::t('app', '保存时间'),
            'link' => Yii::t('app', '链接'),
        ];
    }
}
