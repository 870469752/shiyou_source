<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "protocol.event_point".
 *
 * @property integer $id
 * @property integer $interval
 * @property string $name
 * @property integer $protocol_id
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_shield
 * @property boolean $is_upload
 * @property string $last_update
 * @property string $last_value
 * @property string $unit
 * @property integer $event_id
 * @property string $dependence
 */
class EventPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.event_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interval', 'protocol_id', 'event_id'], 'integer'],
            [['name', 'base_value', 'base_history', 'last_update', 'last_value', 'dependence'], 'string'],
//            [['protocol_id', 'base_history', 'last_update', 'dependence'], 'required'],
            [['is_available', 'is_shield', 'is_upload', 'is_system'], 'boolean'],
            [['unit'], 'string', 'max' => 24]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'interval' => Yii::t('app', 'Interval'),
            'name' => Yii::t('app', 'Name'),
            'protocol_id' => Yii::t('app', 'Protocol ID'),
            'base_value' => Yii::t('app', 'Base Value'),
            'base_history' => Yii::t('app', 'Base History'),
            'is_available' => Yii::t('app', 'Is Available'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_upload' => Yii::t('app', 'Is Upload'),
            'last_update' => Yii::t('app', 'Last Update'),
            'last_value' => Yii::t('app', 'Last Value'),
            'unit' => Yii::t('app', 'Unit'),
            'event_id' => Yii::t('app', 'Event ID'),
            'dependence' => Yii::t('app', 'Dependence'),
        ];
    }

    /**
     * 创建 更新 事件点位
     * @return bool
     */
    public function saveEventPoint()
    {
        if($this->isNewRecord)
        {

            $this->name = MyFunc::createJson($this->name, $this->is_system ? 'description_system' : 'description');
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
       return $this->save();
    }

    public function bindingPointSave()
    {
        //判断当前绑定类型 是否存在 相应事件事件点位
        $system_prefix = substr($this->name, 0, strrpos($this->name, '_'));
        $is_exist = self::findByName('system', $system_prefix);
        if(!$is_exist)
        {
            return $this->saveEventPoint();
        }
        else
        {
            $model = self::findOne($is_exist->id);
            $model -> load([
                    'name' => $this->name,
                    'event_id' => $this->event_id,
                ], '');
            return $model->saveEventPoint();
        }
    }

    /**
     * 利用 点位的名字 找出对应的点位  可进行模糊查询
     * @param $lg               name的语言类型 目前有： zh-cn en-us system
     * @param $name
     */
    public static function findByName($lg, $name)
    {
        return self::find()->where("name->'data'->>'".$lg."' like '" .$name."%'")->one();
    }
}
