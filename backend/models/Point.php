<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.point".
 *
 * @property integer $id
 * @property boolean $is_shield
 * @property string $interval
 * @property string $name
 * @property string $protocol_id
 * @property string $last_update
 * @property string $base_value
 * @property string $base_history
 * @property boolean $is_available
 * @property boolean $is_upload
 *
 * @property PointCategoryRel[] $pointCategoryRels
 * @property AtomEvent[] $atomEvents
 * @property Protocol $protocol
 */
class Point extends \yii\db\ActiveRecord
{
    public $category_id;
    public $location_id;
    public $filter_max_value;
    public $filter_min_value;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['value_type', 'interval'], 'required'],
            ['name', 'string'],
            ['category_id','default', 'value' => [[0]]],
            ['location_id','default', 'value' => [[0]]],
            [['value_type', 'filter_max_value', 'filter_min_value'], 'integer'],
            ['base_value', 'default', 'value' => 0],
            ['base_value', 'number'],
            ['interval', 'integer'],
            ['is_shield', 'boolean'],
            ['is_upload', 'boolean'],
            ['unit', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'is_shield' => Yii::t('app', '是否屏蔽'),
            'interval' => Yii::t('app', '记录间隔'),
            'name' => Yii::t('app', '点位名称'),
            'protocol_id' => Yii::t('app', '协议'),
            'value_timestamp' => Yii::t('app', '最后更新'),
            'base_value' => Yii::t('app', '基础值'),
            'base_history' => Yii::t('app', '历史值'),
            'is_available' => Yii::t('app', '是否可用'),
            'is_upload' => Yii::t('app', '是否上传'),
            'category' => Yii::t('app', '分类'),
            'oneData' => Yii::t('app', '最新数据'),
            'filter_max_value'=> Yii::t('app', '最大过滤值'),
            'filter_min_value' => Yii::t('app', '最小过滤值'),
            'value_type' => Yii::t('app', '数值类型'),
            'unit' => Yii::t('app', '单位')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('core.point_category_rel', ['point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtomEvents()
    {
        return $this->hasMany(PointEvent::className(), ['point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(Protocol::className(), ['id' => 'protocol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointData()
    {
        return $this->hasMany(PointData::className(), ['point_id' => 'id']);
    }

    public function getOneData()
    {
        return $this->hasOne(PointData::className(), ['point_id' => 'id']);
    }

    /**
     * 默认查询ID和名称并且是非屏蔽可用的
     * @return static
     */
    public static function baseSelect(){
        return static::find()->select("id, name->'data'->>'".Yii::$app->language."' as name, value_type")
            ->where(['is_shield' => false, 'is_available' => true]);
    }
    //上个出错了  生成的语句为 is_shield= and is_vailable=1    1=bool类型报错

    public static function baseSelect_new(){
        return static::find()->select("id, name->'data'->>'".Yii::$app->language."' as name, value_type");
    }
    /**
     * 获取 所有点位
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findAllPoint()
    {
        return static::find()->asarray()->all();
    }

    /**
     * 获取 所有点位
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findAllPointByIds($ids)
    {
        return static::find()->where("id in ($ids)")->asarray()->all();
    }

    /**
     * 废弃请勿模仿
     * @return \yii\db\ActiveQuery
     */
    public function getRangePointData($start_time = '', $end_time = '')
    {
        $object = $this->hasMany(PointData::className(), ['point_id' => 'id'])
            ->select(['value','timestamp'])
            ->orderBy('timestamp');
        if($start_time && $end_time){
            return $object->andWhere(['between', 'timestamp', $start_time, $end_time]);
            /*    ->andWhere('timestamp >= :start_time',[':start_time' => $start_time])
                ->andWhere('timestamp < :end_time',[':end_time' => $end_time]);*/
        }
        return $object;
    }

    public static function findOptimizePointData($point_id, $start_time, $end_time, $group, $method)
    {
        return static::baseSelect()->where(['id' => $point_id])->with([
            'pointData' => function ($query) use($start_time, $end_time, $group, $method) {
                    PointData::optimizeData($query, $start_time, $end_time, $group, $method);
                }
        ])->asArray()->all();
    }

    public static function findPointDataTotal($point_id, $start_time, $end_time)
    {
        return static::baseSelect()->where(['id' => $point_id])->with([
            'pointData' => function ($query) use ($start_time, $end_time) {
                    PointData::totalData($query, $start_time, $end_time);
                }
        ])->asArray()->all();
    }

    public static function findFirstId(){
        return static::baseSelect()->min('id');
    }

    /**
     *  用 主键id查找 对应点位的 信息
     * @param $id 查询点位的 id
     */
    public static function getById($id)
    {
        return Point::findOne($id);
    }


    public static function getPointUnitById($id)
    {
        return Point::find()->with([
                'unit'
            ])->Where(['id' => $id])->asArray()->all();
    }

    /**
     * 根据点位id获取 一个点位的所有信息 包括 单位 分类 及事件
     * @param $id
     */
    public static function getPointInformation($id)
    {
        return Point::find($id)->with([
                'unit', 'category', 'atomEvents'
            ])->andWhere(['id' => $id])->asArray()->one();
    }

    /**
     * 获取 点位单位能在 单位中找到的的所有单位
     * @param string $where     过滤条件
     * @return array
     */
    public static function getOfStandardUnit($where = '')//unit is not null
    {
        //获取 含有单位的点位
        $data = Point::find()->with([
                'unit'
        ])->andWhere($where)->asArray()->all();
        //echo "<pre>";print_r($data);die;
        //排除 无法匹配到各单位表中的点位
        $res = [];
        foreach($data as $key => $value)
        {
            if(1)//$value['unit'] !== null
            {
                $res[$key]['id'] = $value['id'];
                $res[$key]['name'] = $value['name'];
                $res[$key]['unit'] = $value['unit'];
                $res[$key]['proportion'] = $value['unit']['coefficient'];
            }
        }
        return $res;
    }

    /**
     * 获取 所有非屏蔽点位
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAvailablePoint()
    {
        $data = self::findAllPoint();
        foreach($data as $key => $value)
        {
            if($value['is_shield'])
            {
                unset($data[$key]);
                continue;
            }
        }
        return $data;
    }

    /**
     * 点位ID以","隔开并对应相应单位，获取分组单位
     * @param $point_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findPointUnit($point_id)
    {
        return Point::find()->select(['id' => "array_to_string(array_agg(id),',')", 'unit'])
->where(['and', ['id' => $point_id], 'unit is not null'])
->groupBy('unit')->with('unit')->asArray()->all();
}

    /**
     * 点位信息的更新
     */
    public function savePoint()
    {
//        print_r($this);
//        die;
        //处理名字
        if(is_null(json_decode($this->name))){
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        //处理过滤值
        $filter_data = ['min' => $this->filter_min_value,
                        'max' => $this->filter_max_value];
        $this->value_filter = MyFunc::createJson($filter_data, 'value_filter');
//        echo'<pre>';
//        print_r($this);
//        die;
        $res = $this->save();

        $category_rle_model = new PointCategoryRel();
        //删除所有该点位已存在的分类
        //$category_rle_model::deleteCategoryByPointId($this->id);
        //处理其他 点位信息 1、将分类关联到分类表中

        if($this->category_id){
            $category_arr = is_string($this->category_id) ? explode(',', $this->category_id) : $this->category_id;
            foreach($category_arr as $cal){
                $category_rle_model = new PointCategoryRelNew();
                $category_rle_model->load([
                        'point_id' => $this->id,
                        'category_id' => $cal,
                        //'category_type'=>1
                    ], '');

                $res = $category_rle_model->save();
            }
        }

        if($this->location_id){
            $category_arr = is_string($this->location_id) ? explode(',', $this->location_id) : $this->location_id;
            foreach($category_arr as $cal){
                $category_rle_model = new PointCategoryRelNew();
                $category_rle_model->load([
                    'point_id' => $this->id,
                    'category_id' => $cal,
                    //'category_type'=>2
                ], '');
                $res = $category_rle_model->save();
            }
        }
        return $res;
    }

    /**
     * 利用点位id 查找点位的字段信息
     * @param $id
     * @param $_cloumn
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCloumnById($point_id, $_field)
    {
        return self::find()->select($_field)->where(['id' => $point_id])->asArray()->all();
    }

    /**
     * 批量修改 点位的属性
     * @param $point_ids
     */
    public static function saveBatchPoint($point_ids, $param)
    {

        if(isset($point_ids) && !empty($point_ids)) {
            foreach($point_ids as $key => $value) {
                if($value) {
                    $point_model = self::findOne($value);
                    $point_model->load($param);
                    $point_model->savePoint();
                }
            }
        }
        return true;
    }

    public static function getPointInfoById($id)
    {
        return Point::find()->Where(['id' => $id])->asArray()->all();
    }

    public static function getPointNameById($id)
    {
        return Point::find()->select("id,name->'data'->>'zh-cn' as cn, name->'data'->>'en-us' as us")->Where(['id' => $id])->asArray()->one();
    }

    public static function getPointDataByType($point_id, $start_time, $end_time, $group, $method)
    {
        return self::baseSelect()->where(['id' => $point_id])->with([
                'pointData' => function ($query) use($start_time, $end_time, $group, $method) {
                        PointData::getDataByTimeType($query, $start_time, $end_time, $group, $method);
                    }
            ])->asArray()->all();
    }

    public static function _getPointData($query)
    {
        return $query->select(['id', 'name'])->with([
                    'pointData' => function($query) {
                            $query->select(['sum(value) as value', "to_char(timestamp, 'YYYY-MM-DD') as _timestamp", 'min(point_id) as point_id'])
                            ->andWhere(['between', 'timestamp', '2015-05-20 00:00', '2015-05-20 23:00'])
                                ->groupby(['_timestamp']);
                        }
                ]);
    }
}
