<?php

namespace backend\models;

use common\library\MyFunc;
use Yii;

/**
 * This is the model class for table "core.system_template".
 *
 * @property integer $id
 * @property string $name
 * @property string $template
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.system_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'template'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'template' => Yii::t('app', '模板'),
        ];
    }

    /**
     * 获取所模板表里 所有的公式
     * @return array 返回一个 二维数组 [
     *                                  'formula' =>  [公式，公式名]，
     *                                  'unit' => ['公式id', '公式名', '点位标准单位', '公式结果单位']
     *                                  ]
     */
    public static function getFormula()
    {
        $formula_arr = [];
        $formula_unit = [];
        $res = is_array(self::find()->asArray()->all())?self::find()->asArray()->all():[];
        foreach($res as $value)
        {
            $name = MyFunc::DisposeJSON($value['name']);
            foreach($value as $k => $v){
                if($k == 'template')
                {
                        $formula_parse = MyFunc::DisposeJSON($v);
                        $formula_parse['unit']['name'] = $name;
                        $formula_parse['unit']['id'] = $value['id'];
                        $formula_unit[] = $formula_parse['unit'];
                        $formula_arr[$formula_parse['formula']] = $name;
                }
            }
        }
        return [
                  'formula' => $formula_arr,
                  'unit' => $formula_unit
                ];
    }

    public static function getNameById($id)
    {
        $model = self::findOne($id);
        return MyFunc::DisposeJSON($model->name);
    }
}