<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modbus_point".
 *
 * @property integer $id
 * @property string $name
 * @property integer $address
 * @property integer $length
 * @property integer $controller_id
 * @property integer $config_id
 * @property double $ratio
 * @property integer $is_shield
 * @property integer $is_available
 * @property integer $type
 */
class Modbus_point extends \yii\db\ActiveRecord
{
    public static function  getDb(){
        return Yii::$app->get('dbsql');
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modbus_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['address', 'length', 'controller_id', 'config_id', 'is_shield', 'is_available', 'type'], 'integer'],
            [['ratio'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('moubus', 'Name'),
            'address' => Yii::t('moubus', 'Address'),
            'length' => Yii::t('moubus', 'Length'),
            'controller_id' => Yii::t('app', 'Controller ID'),
            'config_id' => Yii::t('app', 'Config ID'),
            'ratio' => Yii::t('moubus', 'Ratio'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_available' => Yii::t('app', 'Is Available'),
            'type' => Yii::t('moubus', 'Type'),
        ];
    }
}
