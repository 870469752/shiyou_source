<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.pictures_group".
 *
 * @property integer $pictures_path
 * @property integer $pictures_id
 * @property string $max
 * @property string $min
 * @property integer $point_id
 */
class PicturesGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.pictures_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pictures_path', 'pictures_id'], 'required'],
            [['pictures_id', 'point_id'], 'integer'],
            [['pictures_path'], 'string'],
            [['max', 'min'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pictures_path' => Yii::t('app', 'Pictures Path'),
            'pictures_id' => Yii::t('app', 'Pictures ID'),
            'max' => Yii::t('app', 'Max'),
            'min' => Yii::t('app', 'Min'),
            'point_id' => Yii::t('app', 'Point ID'),
        ];
    }
}
