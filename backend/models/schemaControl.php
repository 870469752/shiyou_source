<?php

namespace backend\models;

use Yii;
use backend\models\schemaManage;
/**
 * This is the model class for table "core.schema_control".
 *
 * @property integer $id
 * @property string $control_info
 * @property string $name
 * @property string $shema_manage_id
 */
class schemaControl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $TimeMode;
    public static function tableName()
    {
        return 'core.schema_control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shema_manage_id'], 'safe'],
            [['control_info','name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '模式名称',
            'shema_manage_id'=>'时间模式'
        ];
    }
    /*
 * 查找角色
 */
    public function findSelect()
    {
        $model=new schemaManage();
        $select=$model::find()->select("id,name")->asArray()->all();
        $selectarray=array();
        $selectarray['']='';
        for($i=0;$i<count($select);$i++)
        {
            $title=json_decode($select[$i]['name'],1);
            $selectarray[$select[$i]['id']]=$title['data']['zh-cn'];
        }
      /*  echo '<pre>';
        print_r($selectarray);
        die;*/

        return $selectarray;
    }
}
