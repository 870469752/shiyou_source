<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use yii\helpers\ArrayHelper;
use common\library\MyArrayHelper;
/**
 * This is the model class for table "core.compound_event".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property boolean $is_valid
 * @property string $rule
 * @property string $dependence
 */
class CompoundEvent extends \yii\db\ActiveRecord
{
    public $event_id;
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.compound_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'event_id', 'rule', 'name', 'gojs_data'], 'string'],
            [['is_system'], 'boolean'],
            [['gojs_data', 'name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'rule' => Yii::t('app', '组合规则'),
            'dependence' => Yii::t('app', '依赖事件数组'),
        ];
    }

    /**
     * 保存 处理 GoJs 数据
     * @return int
     */
    public function saveEvent()
    {

        /******************************************获取gojs json数据 开始处理********************************************************************/
        $rule = json_decode($this->gojs_data, true);
        /*重组 nodeDataArray 将有需要的进行组合
            结果 会以 key 作为 分组的条件，category 作为分组元素的键名，event_id 作为键值*/
        $rule_data_group_key = ArrayHelper::map($rule['nodeDataArray'],   'category', 'event_id', 'key');

        $rule_link = $rule['linkDataArray'];
        //添加$rule_link 中form 所代表的 逻辑符号(category) 如 -5 代表 not ,点位名称 代表 input
        foreach($rule_link as $key => $value)
        {
            $rule_link[$key]['category'] =  key($rule_data_group_key[$value['from']]);
        }

        $rule_tree = MyArrayHelper::getGoJsTree($rule_link, 'output');
        $rule_arr = MyArrayHelper::getGoJsRule($rule_tree);

        /*******************************************  最后处理 每一个分支 ******************************************************************/
        //将 逻辑表示key 转为 有意义的 字母 其中 '[]'包围的 是需要 rult_data 替换的点位 及 逻辑符号；'{}'是替换逻辑表达式
        $rule_str = $rule_arr[0];
        //替换逻辑表达式 寻找含有 '{}'包含的 元素
        while($str = strpos($rule_str, '{'))
        {
            preg_match_all("/{(-?[1-9]\d*)}/", $rule_str, $match);
            $rule_str = str_replace($match[0][0], $rule_arr[1][$match[1][0]],$rule_str);
        }
        //将 生成好的复合事件规则替换 对应需要替换的元素 寻找 '[]' 包含的 元素
        preg_match_all("/\[(-?[\x80-\xff_a-zA-Z0-9\*-]+)\]/", $rule_str, $match);
        foreach($match[0] as $key => $value)
        {
            $category = key($rule_data_group_key[$match[1][$key]]);
            $event_id = $rule_data_group_key[$match[1][$key]][$category];
            $rule_str = str_replace($value, $category == 'input'?'E'.$event_id:$category,$rule_str);
        }
        /********************************   至此 gojs 数据处理 本项目基本完成***********************************************************************************/
        $this->rule = $rule_str;
        return $this->saveCompoundEvent();
    }





    public function saveCompoundEvent()
    {
        if($this->isNewRecord)
        {
            $this->name = MyFunc::createJson($this->name, 'description', $this->is_system ? 'system' : '');
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name'], $this->is_system ? 'system' : '');
        }
        $this->save();
        return $this->id;
    }

    /**
     * 利用 参数 创建 复合事件对应
     * @param $name
     * @param $rule
     * @return CompoundEvent
     */
    public static function getCompoundEventObjBySystem($name, $rule)
    {
        $comp_event = new CompoundEvent(['scenario' => 'system']);
        $comp_event->load([
                'name' => $name,
                'type' => '2',
                'rule' => $rule,
                'is_system' => true],'');
        return $comp_event;
    }

    public function bindingCompoundEvent($is_only)
    {

        /*判断当前绑定类型 是否存在 相应事件事件*/
        /*如果
            是唯一的 查找系统生成除point_id 外的部分name
          否则
            找全部 name;
        */
        $system_prefix = $is_only ? substr($this->name, 0, strrpos($this->name, '-')) : $this->name;
        $is_exist = self::fuzzySearch($system_prefix);
        if(!$is_exist)
        {
            return $this->saveCompoundEvent();
        }
        else
        {
            $model = self::findOne($is_exist->id);
            $model->scenario = 'system';
            $model -> load([
                    'name' => $this->name,
                    'type' => '2',
                    'rule' => $this->rule,
                    'is_system' => true
                ], '');
            return $model->saveCompoundEvent();
        }
    }

    /**
     * 根据 系统生成 的名字进行模糊查找
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */

    public static function fuzzySearch($name)
    {
        return self::find()->where("name->'data'->>'system' like '" .$name."%'")->one();
    }

}
