<?php

namespace backend\models;

use Yii;
use yii\helpers\BaseArrayHelper;
use common\library\MyFunc;
/**
 * This is the model class for table "core.point_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $category_id
 */
class PointCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id','rgba'], 'string'],
            [['parent_id'], 'integer'],
            [['parent_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }
    //
    public function getChildrenById($id){
        $children=self::find()->where(['parent_id'=>$id])->asArray()->all();
        $children=BaseArrayHelper::map($children, 'id', 'name');
        return $children;
    }

    //获取叶子节点 并输出路径（xx-xx-xx）
    public function getLeafnode(){
        $topnode=self::getTopnode();
        foreach($topnode as $key=>$value)
            $ids[]=$key;
        //遍历$ids得到分类关系图

        foreach ($ids as $ids_value) {
            $data=self::getChildNode($ids_value);
            if(!empty($data)){
                foreach($data as $data_key=>$data_value);

            }
            $data = BaseArrayHelper::map($data, 'id','name');
            $leafarray[$ids_value]=$data;
        }

        return $leafarray;
    }


    //返回同一父类的菜单信息 $data[parent_id] {id parent_id  name}
    public function getsubmenu($parent_id){
        $info=self::find()->where(['parent_id'=>$parent_id])->asArray()->all();
        foreach($info as $value) {
            $name = MyFunc::DisposeJSON($value['name']);
            $data[] = ['id' => $value['id'],'name' => $name,'parent_id' => $value['parent_id']];
        }
        return $data;

    }

    // 得到所有菜单信息 按父类id分类  arr[parent_id]=菜单信息
    public function getAllInfo(){
        $ids=self::find()->distinct()->select('parent_id')->asArray()->all();
        foreach($ids as $idarr) {
            foreach($idarr as $id) {
                if (self::leafIsEmpty($id)) {
                    $data[$id] ='';
                } else {
                    $data1 = self::getsubmenu($id);
                    $data[$id] = $data1;

                }
            }
        }
        return $data;
    }


    /**
     * @param  [type] $id [description]
     * @return [type]     [description] 返回主键值
     */
    public static function getChildNode($id){
        return self::find()->select(['id','name'])
            ->where(['parent_id' => $id])
            ->asarray()->all();
    }

    //判断是否有子菜单
    public function leafIsEmpty($id){
        $result=sizeof(self::getChildNode($id));
        if($result) return false;
        else  return true;
    }



    //获取最顶层分类信息
    public static function getTopnode(){
        $node_data= self::find()->select(['id','name'])->where(['parent_id'=>null])->andWhere('id <> 0')->asArray()->all();
        $node_data = BaseArrayHelper::map($node_data, 'id', 'name');
        return $node_data;
    }

    public static function getAllCategory()
    {
        return self::find()
            ->select(['id', 'name', 'parent_id'])
            ->asArray()->indexBy('id')->all();
    }
}
