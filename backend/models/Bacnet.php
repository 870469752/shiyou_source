<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.bacnet_controller".
 *
 * @property integer $id
 * @property string $name
 * @property string $ip_address
 * @property string $port
 * @property integer $object_instance_number
 * @property string $object_name
 * @property boolean $is_shield
 * @property boolean $is_available
 *
 * @property BacnetPoint[] $bacnetPoints
 */
class Bacnet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.bacnet_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_address'], 'required'],
            [['port'], 'string'],
            [['is_shield', 'is_available'], 'boolean'],
//            ['object_instance_number', 'default', 'value' => 1],
            [['name'], 'string', 'max' => 100],
            [['ip_address'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'port' => Yii::t('app', 'Port'),
            'object_instance_number' => Yii::t('app', 'Object Instance Number'),
            'object_name' => Yii::t('app', 'Object Name'),
            'is_shield' => Yii::t('app', 'Is Shield'),
            'is_available' => Yii::t('app', 'Is Available'),
        ];
    }

    public function bacnetSave()
    {
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBacnetPoints()
    {
        return $this->hasMany(BacnetPoint::className(), ['controller_id' => 'id']);
    }

    /**
     * 获取 所有 协议
     * @return array|static[]
     */
    public static function getAll()
    {
        return self::find()->asarray()->all();
    }

    /**
     * 获取设备总数
     */
    public static function getTotal()
    {
        return self::find()->count();
    }
}
