<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
use yii\helpers\BaseArrayHelper;
/**
 * This is the model class for table "core.auto_table".
 *
 * @property integer $id
 * @property string $create_date
 * @property string $table_task
 * @property string $user_id
 * @property string $name
 */
class AutoTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.auto_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_date', 'table_task', 'user_id', 'name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_date' => '创建日期',
            'table_task' => 'Table Task',
            'user_id' => '创建者',
            'name' => '报表名称',
        ];
    }

    public function AutoTableSave()
    {

        /*$data = Yii::$app->request->post();
        $this->setAttribute('table_task',$data['AutoTable']['table_task']);
        //echo "<pre>";
        //print_r($this->table_task);die;
        $arr = explode("&&",($this->table_task));

        $this->name=$arr[0];
        array_splice($arr,0,1);echo "<pre>"; print_r($arr);die;
        $str_date_type=array("type"=>$arr[0]);
        if($arr[0]=='day'){
            $str_data_month=array("month"=>"null");
            if($arr[1]=='all'){
                $str_data_day=array("day"=>array('start'=>'1','end'=>'365','name'=>''));
            }elseif($arr[1]=='work_day'){
                $str_data_day=array("day"=>array('start'=>'1','end'=>'5','name'=>''));
            }else{
                $str_data_day=array("day"=>array('start'=>'6','end'=>'7','name'=>''));
            }
            if($arr[2]='hour'){
                $str_data_hour[$i]=Array('start'=>0,'end'=>23,'name'=>'null');
            }elseif($arr[2]='auto'){

            }else{
                $interval_time=explode(";",$arr[2]);
                for($i=0;$i<count($interval_time);$i++){
                    $interval_time_arr=explode("-",$interval_time[$i]);
                    for($j=0;$j<count($interval_time_arr);$j++){
                        $str_data_hour[$i]=Array('start'=>$interval_time_arr[1],'end'=>$interval_time_arr[2],'name'=>$interval_time_arr[0]);
                    }
                }
            }
        }elseif($arr[0]=='week'){
            $str_data_month=array("month"=>"null");//月
            $day_arr = explode('-',$arr[1]);
            $day_start = 0;$day_end = 0;$day_temp_arr = array();
            for($i=0;$i<count($day_arr);$i++){
                $day_start=$arr[$i];
            }
            $str_data_day=array("day"=>array('start'=>'1','end'=>'365','name'=>''));
        }

        $this->table_task = MyFunc::createJson($arr, 'auto_table');
        //name
        if($this->isNewRecord) {
            $this->name = MyFunc::createJson($this->name);
        } else {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        $this->timestamp = date('Y-m-d H:i:s');
        $this->user_id = Yii::$app->user->identity->id;

        return $this->save();*/
    }
}
