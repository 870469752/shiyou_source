<?php

namespace backend\models;

use Yii;
use backend\models\ImageBase;
/**
 * This is the model class for table "core.equipment_image_base_relation".
 *
 * @property integer $id
 * @property integer $equipment_id
 * @property integer $image_base_id
 * @property string $image_base_x
 * @property string $image_base_y
 * @property integer $image_base_width
 * @property integer $image_base_height
 */
class EquipmentImageBaseRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.equipment_image_base_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipment_id', 'image_base_id'], 'required'],
            [['equipment_id', 'image_base_id', 'image_base_width', 'image_base_height'], 'integer'],
            [['image_base_x', 'image_base_y'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('image', 'ID'),
            'equipment_id' => Yii::t('image', 'Equipment ID'),
            'image_base_id' => Yii::t('image', 'Image Base ID'),
            'image_base_x' => Yii::t('image', 'Image Base X'),
            'image_base_y' => Yii::t('image', 'Image Base Y'),
            'image_base_width' => Yii::t('image', 'Image Base Width'),
            'image_base_height' => Yii::t('image', 'Image Base Height'),
        ];
    }
    /**
     * save
     *
     */
    public static function saveImageBase($data){
        $sql = "UPDATE core.equipment_image_base_relation SET image_base_height={$data['image_base_height']},image_base_width={$data['image_base_width']} WHERE id={$data['id']};";
        $db = Yii::$app->db;
        return $db->createCommand($sql)->queryScalar();
    }
    /**
     * 获得相应设备的底图
     * @param int equipment_id
     * @return array
     */
    public static function getOneImageBase($id){
        $img_base_id = EquipmentImageBaseRelation::find()->select("id,image_base_id,image_base_width,image_base_height")->where("equipment_id=$id")->asArray()->all();
        $img_base = [];
        foreach($img_base_id as $v){
            $info = ImageBase::getOneImageBase($v['image_base_id']);
            foreach($info as $val){
                $img_base['id'] = $v['id'];
                $img_base['base_id'] = $val['id'];
                $img_base['path'] = empty($val['path'])?'uploads/0.jpg':$val['path'];
                $img_base['width'] = empty($v['image_base_width'])?600:$v['image_base_width'];
                $img_base['height'] = empty($v['image_base_height'])?400:$v['image_base_height'];
            }
        }
        return $img_base;
    }
    /**
     * 根据
     * @param $id int
     * @return object
     */
    public function getModel($id){
        return EquipmentImageBaseRelation::find()->where("id=$id");
    }
}
