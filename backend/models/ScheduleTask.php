<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.schedule_task".
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $name
 * @property string $schedule_mode
 * @property string $create_timestamp
 * @property string $update_timestamp
 * @property integer $creator
 * @property string $assign_users
 * @property string $assign_roles
 * @property string $last_exec_timestamp
 * @property string $next_exec_timestamp
 * @property name
 * @property User $creator0
 * @property Task $task
 *
 */
class ScheduleTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.schedule_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'time_mode', 'creator','name'], 'required'],
            [['task_id', 'creator'], 'integer'],
            [['name','time_mode', 'create_timestamp', 'update_timestamp', 'assign_users', 'assign_roles', 'last_exec_timestamp', 'next_exec_timestamp'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'time_mode' => 'Time Mode',
            'create_timestamp' => '创建时间',
            'update_timestamp' => '更新时间',
            'start_timestamp' => '开始时间',
            'end_timestamp' => '结束时间',
            'start_timestamp_event' => '任务执行开始时间',
            'end_timestamp_event' => '任务执行结束时间',
            'creator' => '创建者',
            'assign_users' => 'Assign Users',
            'assign_roles' => 'Assign Roles',
            'last_exec_timestamp' => '上次执行时间',
            'next_exec_timestamp' => '下次执行时间',
            'name'=>'任务名称'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator0()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    /*
    * 查找角色
    */
    public function findRole()
    {
        $role=Role::find()->select(["id","name->'data'->>'zh-cn' as name" ])->asArray()->all();
        $roleArray=array();
        for($i=0;$i<count($role);$i++)
        {
            $roleArray[$role[$i]['id']]=$role[$i]['name'];
        }

        return $roleArray;
    }
}
