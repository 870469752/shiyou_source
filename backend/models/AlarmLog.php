<?php

namespace backend\models;
use yii\helpers\ArrayHelper;
use Yii;
use common\library\MyFunc;
use  yii\web\Session;
/**
 * This is the model class for table "core.alarm_log".
 *
 * @property integer $id
 * @property integer $alarm_rule_id
 * @property string $alarm_level
 * @property boolean $is_confirmed
 * @property string $send_status
 * @property string $start_time
 * @property string $end_time
 * @property string $description
 *
 * @property AlarmRule $alarmRule
 */
class AlarmLog extends \yii\db\ActiveRecord
{
    public $type;
    public $time;
    public $data;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.alarm_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alarm_rule_id', 'alarm_level', 'start_time', 'description'], 'required'],
            [['alarm_rule_id'], 'integer'],
            [['alarm_level', 'send_status', 'start_time', 'end_time', 'description', 'type', 'time'], 'string'],
            [['is_confirmed'], 'boolean'],
            ['type', 'default', 'value' => 'month'],
            ['time', 'default', 'value' => date('Y-m')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '日志 ID'),
            'alarm_level' => Yii::t('app', '报警级别'),
            'is_confirmed' => Yii::t('app', '是否确认'),
            'send_status' => Yii::t('app', '发送状态'),
            'start_time' => Yii::t('app', '开始时间'),
            'end_time' => Yii::t('app', '结束时间'),
            'description' => Yii::t('app', '日志描述'),
            'type' => Yii::t('app', '报表类型'),
            'time' => Yii::t('app', '日期'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlarmRule()
    {
        return $this->hasOne(AlarmEvent::className(), ['id' => 'event_id'])->viaTable('alarm_rule', ['id' => 'alarm_rule_id']);
    }

    /**
     * 取出未处理的最新10条日志记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLogs()
    {
        return self::find()->where(['is_confirmed' => 'false','end_time' => null])->orderBy('start_time desc')->limit(10)->asArray()->all();
    }

    /**
     * 取出所有日志记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public function byGetLogsAll_Old($condition)
    {
        $str = '';
        $where = " id !=0 ";
        //被注释掉的是正常应用
        //$rule_ids = json_decode(Yii::$app->cache->get("rules".Yii::$app->user->id));
        $user_name = Yii::$app->user->identity->username;
        if(strstr($user_name, '电力')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Ele"));
        }elseif(strstr($user_name, '安全')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Safe"));
        }elseif(strstr($user_name, '设施')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Equi"));
        }elseif(strstr($user_name, '消防')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Fire"));
        }else{
            $rule_ids  = $rule_ids = json_decode(Yii::$app->redis->get("Rule_All"));
        }
        if(!empty($rule_ids)){
            foreach($rule_ids as $k => $v){
                if($v){$str.=$v.',';}else{continue;}
            }
            $str = rtrim($str,',');
            if($str){
                $where .= " and alarm_rule_id in ($str) ";
            }
        }

        $select_content = "id,alarm_level,trim(sys_name) as sys_name,start_time,is_confirmed,end_time,trim(p_name) as p_name,description->'data'->>'zh-cn' as description,trim(p_link) as p_link ";
        if($condition=='day'){
            //$where .= " and start_time > '".date("Y-m-d")." 00:00:00' and start_time < '".date("Y-m-d")." 23:59:59'";
            $result = self::find()->select($select_content)->where($where)->orderBy("id desc")->limit(100)->asArray()->all();
        }else{
            $result = self::find()->select($select_content)->where($where)->orderBy("id desc")->asArray()->all();
        }
        return $result;
    }

    public function byGetLogsAll($condition)
    {
        $str = '';
        $where = " id !=0 ";
        //被注释掉的是正常应用
        //$rule_ids = json_decode(Yii::$app->cache->get("rules".Yii::$app->user->id));
        $user_name = Yii::$app->user->identity->username;
        /*if(strstr($user_name, '电力')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Ele"));
        }elseif(strstr($user_name, '安全')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Safe"));
        }elseif(strstr($user_name, '设施')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Equi"));
        }elseif(strstr($user_name, '消防')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Fire"));
        }else{
            $rule_ids  = null;
        }
        if(!empty($rule_ids)){
            foreach($rule_ids as $k => $v){
                if($v){$str.=$v.',';}else{continue;}
            }
            $str = rtrim($str,',');
            if($str){
                $where .= " and alarm_rule_id in ($str) ";
            }
        }*/
        if(strstr($user_name, '电力')){
            $where = "and category_id = 1475";
        }elseif(strstr($user_name, '安全')){
            $where = "and (category_id = 1476 or category_id = 1477 or category_id = 1478 or category_id = 1479)";
        }elseif(strstr($user_name, '设施')){
            $where = "and (category_id = 1470 or category_id = 1471 or category_id = 1472 or category_id = 1473 or category_id = 1474 or category_id = 1481)";
        }elseif(strstr($user_name, '消防')){
            $where = "and category_id = 1476";
        }
        $select_content = "id,alarm_level,start_time,is_confirmed,end_time,point_name->'data'->>'zh-cn' as p_name,description->'data'->>'zh-cn' as description,link_point as p_link,point_id";
        $sql = "select $select_content from public.view_alarm_log_info where". $where."  order by start_time desc ";
        if($condition=='day'){
            $sql .=" limit 50 offset 0 ";
        }
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }

    /**
     * 取出所有火灾日志记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public function byGetFireLogsAll($condition)
    {
        $str = '';
        $where = " id !=0 ";
        //被注释掉的是正常应用
        /*$rule_ids = json_decode(Yii::$app->redis->get("Rule_Fire"));
        if(!empty($rule_ids)){
            foreach($rule_ids as $k => $v){
                if($v){$str.=$v.',';}else{continue;}
            }
            $str = rtrim($str,',');
            if($str){
                $where .= " and alarm_rule_id in ($str) ";
            }
        }*/
        $where .= " and category_id = 1476";

        $select_content = "id,alarm_level,start_time,is_confirmed,end_time,point_id,point_name->'data'->>'zh-cn' as p_name,description->'data'->>'zh-cn' as description,link_point as p_link ";
        $sql = "select $select_content from public.view_alarm_log_info where". $where." order by start_time desc limit 50 offset 0 ";
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }

    /**
     * 根据ID获取报警信息
     * @return array|\yii\db\ActiveRecord[]
     */
    public function byGetAlarmById($id)
    {
        $select_content = "id,alarm_level,trim(sys_name) as sys_name,start_time,is_confirmed,end_time,point_id,trim(p_name) as p_name,description->'data'->>'zh-cn' as description,trim(p_link) as p_link ";
        $result = self::find()->select($select_content)->where("id=$id")->orderBy("id desc")->asArray()->all();
        return $result;
    }

    /**
     * 取出未处理的日志记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLogsAll()
    {
        return self::find()->select('id')->where(['is_confirmed' => 'false','end_time' => null])->orderBy('start_time desc')->asArray()->all();
    }

    /**
     * 取出所有未处理过的记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function byGetNoRecord()
    {
        $a_l = "core.alarm_log";
        $a_r = "core.alarm_rule";
        $p_e = "core.point_event";
        $p = "core.point";
        $p_l = "core.point_link";
        $select_content = "$a_l.id,$p_e.point_id,$p.name->'data'->>'zh-cn' as name,$p_l.link_point";
        $where = "$a_l.sys_id is null";
        $judge = self::find()->select("$a_l.id")->where($where)->orderBy("$a_l.id desc")->asArray()->all();
        if(!empty($judge)){
            $result = self::find()->select($select_content)->leftJoin($a_r,"$a_l.alarm_rule_id = $a_r.id")->leftJoin($p_e,"$a_r.event_id = $p_e.id")->leftJoin($p,"$p_e.point_id = $p.id")->leftJoin($p_l,"$p.id = $p_l.main_point")->where($where)->orderBy("$a_l.id desc")->asArray()->all();
            return $result;
        }else{
            die;
        }
    }

    /**
     * 取出所有未处理过的记录(订阅)
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function byGetNoRecordNew($log_id)
    {
        $a_l = "core.alarm_log";
        $a_r = "core.alarm_rule";
        $p_e = "core.point_event";
        $p = "core.point";
        $p_l = "core.point_link";
        $select_content = "$a_l.id,$p_e.point_id,$p.name->'data'->>'zh-cn' as name,$p_l.link_point";
        $where = "$a_l.id = $log_id";
        $judge = self::find()->select("$a_l.id,$a_l.alarm_rule_id")->where($where)->orderBy("$a_l.id desc")->asArray()->all();
        if(!empty($judge)){
            $result = self::find()->select($select_content)->leftJoin($a_r,"$a_l.alarm_rule_id = $a_r.id")->leftJoin($p_e,"$a_r.event_id = $p_e.id")->leftJoin($p,"$p_e.point_id = $p.id")->leftJoin($p_l,"$p.id = $p_l.main_point")->where("$a_l.p_name is null ")->orderBy("$a_l.id desc")->asArray()->all();
            return $result;
        }else{
            die;
        }
    }

    /**
     * 取出未提示的报警记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLogsNoNotice($sys_id)
    {
        $a_l = "core.alarm_log";
        $a_level = "core.alarm_level";
        $select_content = "$a_l.id,$a_l.alarm_level,$a_l.alarm_rule_id,$a_l.send_status->'data'->>'mail' as mail,$a_l.send_status->'data'->>'vedio' as vedio,$a_l.is_confirmed,$a_l.start_time,$a_l.sys_id,$a_l.sys_name,$a_l.p_name,$a_l.position,$a_l.send_status->'data'->>'sms' as sms,$a_l.description->'data'->>'zh-cn' as description,$a_level.name->'data'->>'zh-cn' as name,$a_level.notice_style,$a_level.notice_siren,$a_level.notice_fade";
        $condition = ["$a_l.is_confirmed" => 'false',"$a_l.alarm_level" => 1,"$a_l.end_time" => null,"$a_l.send_status->'data'->>'notice'" => '0',"$a_level.notice_setting" => 'true'];
        $tem_con = "";
        if(!$sys_id){
            $tem_con .= " $a_l.sys_id is not null";
        }else{
            $tem_con .= " $a_l.sys_id = $sys_id";
        }
        $result = self::find()->select($select_content)->leftJoin($a_level,"$a_l.alarm_level = $a_level.id")->where($tem_con)->andWhere($condition)->orderBy("$a_l.id asc")->asArray()->all();
        //print_r($result);die;
        //设置notice为已经报警
        if(count($result)){
            return $result;
        }
    }

    /**
     * 取出未提示的报警记录(订阅)
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLogsNoNoticeNew($log_id)
    {
        $a_l = "core.alarm_log";
        $a_level = "core.alarm_level";
        $select_content = "$a_l.id,$a_l.alarm_level,$a_l.alarm_rule_id,$a_l.send_status->'data'->>'mail' as mail,$a_l.send_status->'data'->>'vedio' as vedio,$a_l.is_confirmed,$a_l.start_time,$a_l.p_name,$a_l.send_status->'data'->>'sms' as sms,$a_l.description->'data'->>'zh-cn' as description,$a_level.name->'data'->>'zh-cn' as name,$a_level.notice_style,$a_level.notice_siren,$a_level.notice_fade";
        $condition = ["$a_l.alarm_level" => 1,"$a_level.notice_setting" => 'true'];
        $tem_con = "$a_l.id = $log_id";
        $result = self::find()->select($select_content)->leftJoin($a_level,"$a_l.alarm_level = $a_level.id")->where($tem_con)->andWhere($condition)->orderBy("$a_l.id asc")->asArray()->all();
        if(count($result)){
            return $result;
        }
    }

    public static function setNotice($data){
        if(count($data)){
            foreach($data as $k => $v){
                $tem=['data_type'=> 'send_status','data' =>''];
                $tem['data']['mail']=intval($v['mail']);
                $tem['data']['sms']=intval($v['sms']);
                $tem['data']['notice']=1;
                $tem['data']['vedio']=intval($v['vedio']);
                $tem=json_encode($tem);
                $sql= "update core.alarm_log set send_status='".$tem."' where id =".$v['id'];
                Yii::$app->db->createCommand($sql)->queryAll();
            }
        }
    }

    /**
     * 取出未弹出视频的记录
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getLogsNoVedio($log_id)
    {
        $str = '';
        $where = " id =$log_id ";
        $user_name = Yii::$app->user->identity->username;;
        if(strstr($user_name, '电力')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Ele"));
        }elseif(strstr($user_name, '安全')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Safe"));
        }elseif(strstr($user_name, '设施')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Equi"));
        }elseif(strstr($user_name, '消防')){
            $rule_ids = json_decode(Yii::$app->redis->get("Rule_Fire"));
        }else{
            $rule_ids  = null;
        }
        $str = '';
        if(!empty($rule_ids)){
            foreach($rule_ids as $k => $v){
                if($v){$str.=$v.',';}else{continue;}
            }
            $str = rtrim($str,',');
            if($str){
                $where .= " and alarm_rule_id in ($str) ";
            }
        }
        $select_content = "id,p_link,send_status->'data'->>'mail' as mail,send_status->'data'->>'notice' as notice,send_status->'data'->>'sms' as sms";
        $result = self::find()->select($select_content)->where($where)->orderBy("id asc")->asArray()->all();
            return $result;
    }

    public static function setVedio($data){
        if(count($data)){
            foreach($data as $k => $v){
                $tem=['data_type'=> 'send_status','data' =>''];
                $tem['data']['mail']=intval($v['mail']);
                $tem['data']['sms']=intval($v['sms']);
                $tem['data']['notice']=intval($v['notice']);
                $tem['data']['vedio']=1;
                $tem=json_encode($tem);
                $sql= "update core.alarm_log set send_status='".$tem."' where id =".$v['id'];
                Yii::$app->db->createCommand($sql)->queryAll();
            }
        }
    }

    /**
     * 利用 id 修改记录
     * @param $id       对应的 id
     * @param $column   是一个数组 ： ['id' => 1, 'name' => 'c'...] 数组的键代表字段名， 值代表修改的数据
     */
    public static function updateById($id, $columns)
    {
        $model = self::getObjById($id);
        $attributes = $model->oldAttributes;
        $columns_info = $model->tableSchema->columns;
        foreach($columns as $key => $value)
        {
            //不用isset 因为值可能为空
            if(array_key_exists($key, $attributes))
            {
                echo $key;
                if($columns_info[$key]->dbType == 'json'){
                    $value = MyFunc::createJson($value, 'description');
                }
                $model->$key = $value;
            }
        }
        echo $model->save();die;
    }

    public static function updateConfirmById($id)
    {
        return self::find()->createCommand()->update('core.alarm_log', ['is_confirmed' => true], "id in ( $id )")->execute();
    }

    public function updateAlarmLog($id,$arr)
    {
        return self::find()->createCommand()->update('core.alarm_log', $arr, "id = $id")->execute();
    }


    /**
     * 利用id  创建对象
     * @param $id
     * @return mixed
     */
    public static function getObjById($id)
    {
        return self::findOne($id);
    }

    /**
     * 获取报警日志未处理总数
     */
    public static function getTotal()
    {
        return self::find()->where(['is_confirmed' => false])->count();
    }

    public static function getLogByPage($offset, $limit)
    {
        return self::find()->offset($offset)->limit($limit)->orderBy(['id' => SORT_DESC])->asArray()->all();
    }

    public static function count()
    {
        return self::find()->count();
    }

    public function _delete_all ($id){
        $sql = "delete from core.alarm_log where id = $id";
        $result = Yii::$app->db->createCommand($sql);
        $query = $result->queryAll();
        return true;
    }
}
