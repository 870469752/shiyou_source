<?php

namespace backend\models;

use Yii;
use backend\models\PointCategory;

/**
 * This is the model class for table "core.point_category_rel".
 *
 * @property integer $id
 * @property integer $point_id
 * @property integer $category_id
 */
class PointCategoryRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_category_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id', 'category_id'], 'required'],
            [['point_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '关系编号'),
            'point_id' => Yii::t('app', '点位编号'),
            'category_id' => Yii::t('app', '分类编号')
        ];
    }

    /**
     * 把以点位把多个分类用逗号黏在一起
     * @param $category_id
     * @return static
     */
    public static function CategoryAGG($category_id)
    {
        return static::find()
            ->select([
                'pc.id',
                'point_id','category_id' => "
                '{\"data_type\":\"description\",\"data\":{\"".YII::$app->language."\":\"'||array_to_string(ARRAY_AGG(pc.name->'data'->>'".YII::$app->language."'),',')||'\"}}'"
            ])
            ->where(['pc.id' => $category_id])
            ->leftJoin(['pc' => 'core.point_category'], 'pc.id = category_id')
            ->groupBy('point_id');
    }

    public static function deleteCategoryByPointId($point_id)
    {
        $model = self::find()->where(['point_id' => $point_id])->all();
        foreach($model as $value) {
            if($value)
                $value->delete();
        }
    }

    /**
     * 获取能源分类category_id下所有点位id,$point_ids = [$category_id=>[]]
     * @param $category_id
     * @return bool array[$category_id][]=$point_ids
     * @auther pzzrudlf <pzzrudlf@gmail.com>
     */
    public static function getPointIds($category_id)
    {
        $res = [];
        $temp = self::find()->select('point_id,category_id')->where(['in','category_id',$category_id])->asArray()->all();
        if($temp) {
            foreach($temp as $key => $value) {
                $res[$category_id[0]][] = $value['point_id'];
            }
        }else {
            $res[$category_id[0]] = [];
        }

        return $res;
    }

    /**
     * @param $category_id
     * @return array|\yii\db\ActiveRecord[]
     * @auther pzzrudlf pzzrudlf@gmail.com
     * @date 2016-1-5
     */
    public static function getId($category_id)
    {
        $condition = "category_id @> '{".$category_id."}'";
        return self::find()->select('point_id')->where(['category_id'=>PointCategory::find()->select('id')->where($condition)->asArray()->column()])->orderBy('point_id desc')->asArray()->column();
    }


    /**
     * 获取分类category_id下所有点位
     * @param $category_id
     * @auther by
     */
    public static function byGetPointIds($category_id)
    {
        $result = self::find()->select('point_id')->where('category_id ='.$category_id)->asArray()->all();
        $arr_point = '';
        if(!empty($result)){
            foreach($result as $k => $v){
                $arr_point .= ','.$v['point_id'];
            }
        }
        return $arr_point;
    }
}
