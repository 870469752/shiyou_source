<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $menu_id
 *
 * @property AuthAssignment $authAssignment
 * @property AuthItemChild $authItemChild
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['name', 'type', 'description'], 'required'],
        [['type', 'created_at', 'updated_at', 'menu_id'], 'integer'],
        [['description', 'data'], 'string'],
        [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        'name' => Yii::t('app', 'Name'),
        'type' => Yii::t('app', 'Type'),
        'description' => Yii::t('app', 'Description'),
        'rule_name' => Yii::t('app', 'Rule Name'),
        'data' => Yii::t('app', 'Data'),
        'created_at' => Yii::t('app', 'Created At'),
        'updated_at' => Yii::t('app', 'Updated At'),
        'menu_id' => Yii::t('app', 'Menu ID'), 
        'Permit' => Yii::t('app', 'Permit'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasOne(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChild()
    {
        return $this->hasOne(AuthItemChild::className(), ['child' => 'name']);
    }


    /**
     * 利用主键name更新 记录的 多个 字段
     * 
     * @param  String $name  [description]
     * @param  Array $param 
     *                       ['id' => 1,
     *                        'type' => 'null']
     * @return [type]        [description]
     */
    public static function updateByName($name, $param = [])
    {

        $model = Item::getObj($name);
        $column_type = $model[0]->getColumnType();
        foreach($model as $obj){
            foreach($param as $key => $value){
                $obj->$key = Item::updateColumn($column_type[$key],$obj->oldAttributes[$key], $value);
            }
            $obj->save();
        }
        return true;
    }



    /**
     * 更新 当前对象的 某个属性
     * switch 判断当前属性的类型
     */
    public function updateOneColumn($column = 'description')
    {
        $old_attr = $this->oldAttributes;
        $column_type = $this->getColumnType();
        $this->$column = Item::updateColumn($column_type[$column],isset($old_attr[$column])?$old_attr[$column]:'', $this->$column);

    }


    /**
     * 更新一个字段
     * @return [type] [description]
     */
    public function OneColumnSave($column = 'description')
    {
        $this->updateOneColumn($column);
        Yii::$app->cache->flush();
        return $this->save();
    }

    /**
     * 创建一个角色 或 权限
     */
    public function addItem()
    {
        $auth = Yii::$app->AuthManager;
        $Item = $this->createItem();
        $this->updateOneColumn();
        $Item->description = $this->description;
        return $auth->add($Item);
    }

    /**
     * 判断 权限 还是角色 进行创建对象
     * @return [type] [description]
     */
    public function createItem()
    {
        $auth = Yii::$app->AuthManager;
        if($this->type == 1){
            $createItem = $auth->createRole($this->name);
        }else{
            $createItem = $auth->createPermission($this->name);
        }
        return $createItem;
    }

    /**
     * 删除一个角色 或权限
     * @return [type] [description]
     */
    public function removeItem()
    {
        $auth = Yii::$app->authManager;
        $Item = $this->createItem();
        $auth->remove($Item);
    }

    /**
     * 更新一个角色 或 权限
     * @return [type] [description]
     */
    public function updateaItem()
    {
        $auth = Yii::$app->authManager;
        $Item = $this->createItem();
        $this->updateOneColumn('description');
        $Item->description = $this->description;
        $oldAttributes = $this->oldAttributes;
        return $auth->update($oldAttributes['name'],$Item);
    }

    /**
     *  获取 当前对象所有字段dbType
     * @return [type] [description]
     */
    public function getColumnType()
    {
        $col_arr = $this->tableSchema->columns;
        $col_type = [];
        foreach($col_arr as $key => $value){
            $col_type[$key] = $value->dbType; 
        }
        return $col_type;
    }
    /**
     * 判断当前 字段的类型 进行对应修改
     * #可以用 对象自带的dataType 来判断
     * @param  [type] $column_type     改动属性的 类型
     * @param  [type] $column          改动属性的 值
     * @return [type]                  [description]
     */
    public static function updateColumn($column_type ,$old_attr, $column)
    {
         switch($column_type){
                case 'json':
                    $newValue = empty($old_attr) ? MyFunc::createJson($column) : MyFunc::updateJson($column, $old_attr);
                    break;
                default:
                    $newValue = $column;
            }
        return  $newValue;
    }


     /**
     * 利用 type字段 查询 所有菜单的叶子菜单 或 角色
     * @return [type] [description]
     */
     public static function getMenuChild($id = '2',$colums_arr = [])
    {
        return Item::find()->select($colums_arr)->where(['type' => $id])->asarray()->all();
    }

    /**
     * 根据 $name 参数的类型 来判断当前获取的对象为一个 还是多个
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public static function getObj($name)
    {
        if($name){
            if(strpos($name, ',')){
                return Item::find()
                ->where('name in ('.$name.')')
                ->all();
            }else{
                return [Item:: findOne(is_array($name) ? ($name) : ['name' => trim($name, "'")])];
            }
        }else{
             throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

      /**
     * 根据参数和 条件 查询对应的记录
     * @return [type] [description]
     */
    public static function findByParam($columns = '*', $where = 1){
       return Item::find()
                   ->select($columns)
                   ->where($where)
                   ->asarray()->all();
    }


}
