<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.event".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property boolean $is_valid
 * @property boolean $trigger_status
 * @property boolean $is_system
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'string'],
            [['is_valid', 'trigger_status', 'is_system'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' =>  Yii::t('app','事件名称'),
            'type' =>  Yii::t('app','事件类型'),
            'is_valid' =>  Yii::t('app','是否有效'),
            'trigger_status' =>  Yii::t('app','是否触发'),
            'is_system' =>  Yii::t('app','系统事件'),
        ];
    }

    /**
     * 创建 更新 事件点位
     * @return bool
     */
    public function saveEventPoint()
    {
        if($this->isNewRecord)
        {

            $this->name = MyFunc::createJson($this->name, $this->is_system ? 'description_system' : 'description');
        }
        else
        {
            $this->name = MyFunc::updateJson($this->name, $this->oldAttributes['name']);
        }
        return $this->save();
    }

    public function bindingPointSave()
    {
        //判断当前绑定类型 是否存在 相应事件事件点位
        $system_prefix = substr($this->name, 0, strrpos($this->name, '_'));
        $is_exist = self::findByName('system', $system_prefix);
        if(!$is_exist)
        {
            return $this->saveEventPoint();
        }
        else
        {
            $model = self::findOne($is_exist->id);
            $model -> load([
                'name' => $this->name,
                'event_id' => $this->event_id,
            ], '');
            return $model->saveEventPoint();
        }
    }

    /**
     * 利用 点位的名字 找出对应的点位  可进行模糊查询
     * @param $lg               name的语言类型 目前有： zh-cn en-us system
     * @param $name
     */
    public static function findByName($lg, $name)
    {
        return self::find()->where("name->'data'->>'".$lg."' like '" .$name."%'")->one();
    }
}
