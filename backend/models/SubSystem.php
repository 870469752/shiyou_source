<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;

/**
 * This is the model class for table "core.sub_system".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_type
 * @property integer $parent_id
 * @property string $category_id
 * @property string $data
 * @property string $sub_system_type
 * @property integer $location_id
 */
class SubSystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'data', 'sub_system_type','rgba'], 'string'],
            [['category_type', 'parent_id', 'location_id'], 'integer']
//            ,
//            [['category_id', 'sub_system_type'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_type' => 'Category Type',
            'parent_id' => 'Parent ID',
            'category_id' => 'Category ID',
            'data' => 'Data',
            'sub_system_type' => 'Sub System Type',
            'location_id' => 'Location ID',
        ];
    }

    //$sub_system 为子系统数据组成的数组
    public static function getSubSystemPoint($sub_system){
        //得到子系统内所有绑定的点位 组成数组
        $points=[];$sub_system_data=[];
        $camera_points=[];
        $keys=[];
        $point_keys=[];
        $camera_key=[];
        $events=[];
        $events_keys=[];
        foreach ($sub_system as $key=>$system) {
            $data=json_decode($system['data'],1);
            $data=json_decode($data['data'],1)['nodeDataArray'];

//            echo '<pre>';
//            print_r($data);
//            die;
            foreach ($data as $data_key=>$data_value) {
                //group 不存在category字段
                $nodecategory=isset($data_value['category'])?$data_value['category']:'';
                switch($nodecategory){
                    case 'main':
                    case 'many':
                    case 'value':
                    case 'onoffvalue':
                    case 'alarmvalue':
                    case 'main_edit':
                    case 'only_value':
                    case 'state':
                    case 'state_event':
                    case 'state_defend':
                    case 'state_defend_1':
                    case 'state_defend_ctrl':
                    case 'gif':
                    case 'gif_value':
                    case 'control':
                    case 'state_value':
                    case 'value_1':
                        //得到 需要查询节点的key
                        if( $data_value['key']!=null) {
                            $keys[] = $data_value['key'];
                            $sub_system_data[$system['id']][$data_value['key']] = null;
                        }
//                    if($data_value['img']=="/uploads/pic/摄像头1.jpg")
                        //判断是不是摄像头的点
                        if(isset($data_value['img']))
                            if(
                                $data_value['img']=="/uploads/pic/摄像头1.jpg" || $data_value['img']=="/uploads/pic/摄像头2.jpg" ||
                                $data_value['img']=="/uploads/pic/摄像头3.jpg" || $data_value['img']=="/uploads/pic/摄像头4.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头5.jpg"||$data_value['img']=="/uploads/pic/摄像头6.jpg"||
                                $data_value['img']=="/uploads/pic/摄像头7.jpg"||$data_value['img']=="/uploads/pic/摄像头8.jpg"
                            ) {
                                $camera_key[] =$data_value['key'];
                            }
                        break;
                    case 'table':
                        $keys[] = $data_value['key'];
                        $sub_system_data[$system['id']][$data_value['key']] = null;
                        break;
                }
            }


            //根据子系统id 和key 找到需要查询的点位集合
            $sub_system_id=$system['id'];
            $configs=SubSystemElement::find()->select('element_id,config')
                ->where(['sub_system_id'=>$sub_system_id])
                ->andWhere(['element_id'=>$keys])
                ->asArray()->all();
            $type='points';

            foreach ($configs as $key=>$configs_value) {
                $data=json_decode($configs_value['config'],1)['data'];
                if(isset($data['points'])){
                    $type='points';
                    $points_info=$data['points'];
                }
                if(isset($data['events'])) {
                    $type='events';
                    $points_info = $data['events'];

                }
                if(isset($data['point_event'])) {
                    $type='point_event';
                    $points_info = $data['point_event'];

                }
//                echo '<pre>';
//                print_r($points_info);
//                die;
                //获取的data信息中  points 可能为点位id  可能为数组信息[id1=>name1,id2=>name2,...]
                if($points_info!="") {

                    if (!is_array($points_info)) {
                        $points[] = $points_info;
                        $point_keys[]=$configs_value['element_id'];
                        $sub_system_data[$system['id']][$configs_value['element_id']] = ['id' => $points_info];
                        if(!empty($camera_key)){
                            $camera_points[]= $points_info;
                        }
                    } else {
//                        echo '<pre>';
//                        print_r($points_info);

                        foreach ($points_info as $point_id => $point_name) {
                            if($type=='events' || $type=='point_event')  {
                                if($point_id!='name'){
                                    $events[]=$point_id;
                                    $events_keys[]=$configs_value['element_id'];
                                }
                            }
                            if($type=='points')  {$points[] = $point_id; $point_keys[]=$configs_value['element_id'];}
                            $sub_system_data[$system['id']][$configs_value['element_id']][] = ['id' => $point_id, 'name' => $point_name];

                        }

                    }
                }

            }
        }

        if(!empty($sub_system_data )){
            foreach($sub_system_data as $sub_system_id=>$sub_system){
                foreach($sub_system as $key=>$value){
                    if(empty($value))
                        unset($sub_system_data[$sub_system_id][$key]);
                }
            }
        }
        return [
            'data'=>$sub_system_data,
            'points'=>array_unique($points),'points_key'=>array_unique($point_keys),
            'camera_points'=>array_unique($camera_points),
            'events'=>array_unique($events),'events_key'=>array_unique($events_keys)
        ];
    }

    //得到event对应的点位信息
    public static function GetEventPoint($events){
        $points=PointEvent::find()
            ->select('point_id')
            ->where(['id'=>$events])
            ->asArray()->all();
        $result=[];
        if(!empty($points)){
            foreach ($points as $key=>$value) {
                $result[]=$value['point_id'];
            }
            $result=array_unique($result);
        }
        else $result;

        return $result;
    }
    //得到 points 点位 写入
    public static function SavePointCommand($points){
        $user_id = Yii::$app->user->id;
        $save_state='save';
        if(empty($points)){
            return 'points empty';
        }
        else {
            foreach ($points as $key => $point) {
                $command_log = new CommandLog();
//            echo '<pre>';
//            print_r($data);
//            die;
                $time = date('Y-m-d H:i:s', time());
                $data['CommandLog'] = [
                    'point_id' => $point,
                    'type' => 1,
                    'value' => '-1',
                    'timestamp' => $time,
                    'user_id' => $user_id,
                    'status' => 1
                ];
                $command_log->load($data);
                $command_log->save();
                /*if(!empty($command_log->errors)){
                    echo '<pre>';
                    print_r($command_log->errors);
                    die;
                    $save_state='save_error';
                }*/
            }
            return $save_state;
        }


    }


    //把点位对应的子系统信息记录组成 PointSubsystem 写在redis内
    public static function PointSubsystemRel(){
        $subsystems=SubSystem::find()->select(['id','points','name'])->asArray()->all();
        $data=[];
        foreach ($subsystems as $key=>$value) {

            $points=json_decode($value['points'],1)['data']['points'];
            if(!empty($points)){
                foreach ($points as $num=>$point) {
                    $data[$point][]=[
                        'id'=>$value['id'],
                        'name'=>MyFunc::DisposeJSON($value['name'])
                    ];
                }
            }
        }
        $data=msgpack_pack($data);
        Yii::$app->redis->set('PointSubsystem',$data);
    }

    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            $model=new SubSystem();
            return $model;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 查找所有数据
     */
    public function findAllData()
    {
        return self::find()->select("location_id as id,name->'data'->>'zh-cn' as name")->asArray()->all();
    }
}
