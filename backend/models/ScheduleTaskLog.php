<?php

namespace backend\models;

use Yii;
use common\library\MyFunc;
/**
 * This is the model class for table "core.schedule_task_log".
 *
 * @property integer $id
 * @property integer $schedule_task_id
 * @property string $start_timestamp
 * @property string $end_timestamp
 * @property string $status
 * @property integer $performer
 *
 * @property User $performer0
 */
class ScheduleTaskLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.schedule_task_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_task_id', 'status', 'performer'], 'required'],
            [['schedule_task_id', 'performer'], 'integer'],
            [['start_timestamp', 'end_timestamp', 'status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_task_id' => 'Schedule Task ID',
            'start_timestamp' => 'Start Timestamp',
            'end_timestamp' => 'End Timestamp',
            'status' => 'Status',
            'performer' => 'Performer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformer0()
    {
        return $this->hasOne(User::className(), ['id' => 'performer']);
    }

    public static function initTodayTaskData(){
        $today_task_old=Yii::$app->cache->get("TodayTask");
        $init_state=[
            "A12" => false, "A16" => false, "A29" => false,
            "A34" => false, "A42" => false, "A45" => false
        ];
        //得到当前用户   当天的计划任务
        $today=date("Y-m-d",time());
        $start_today=$today.' 00:00:00';
        $end_today=$today.' 23:59:59';
        $today_task=self::getTaskByTime($start_today,$end_today);

        //得到当天任务的name信息
        $tasks_old_data=[];
        if(!empty($today_task_old)){
            $tasks_old=$today_task_old['tasks'];
            foreach ($tasks_old as $key=>$value) {
                $tasks_old_data[$value['id']]=$value['action'];
            }
        }

        $schedule_task_id=[];
        foreach ($today_task as $key=>$value ) {
            $schedule_task_id[]=$value['schedule_task_id'];
            $today_task[$key]['action']=isset($tasks_old_data[$value['id']])?$tasks_old_data[$value['id']]:$init_state;
        }

        $schedule_task_info=ScheduleTask::find()->where(['id'=>$schedule_task_id])->asArray()->all();
        $schedule_task_info=MyFunc::_map($schedule_task_info,'id','name');
        Yii::$app->redis->set("TASK_UPDATE","0");
        Yii::$app->cache->set("TodayTask",[
            'tasks'=>$today_task,
            'tasks_name'=>$schedule_task_info
        ]);
    }
    //得到当前用户   一段时间内  的计划任务     start<end
    //得到当前用户   这个时间点  的计划任务     start=end
    //
    public static function getTaskByTime($start_time,$end_time){
        $user_id=Yii::$app->user->id;
        if($start_time<$end_time)
            $task=ScheduleTaskLog::find()
                ->Where('start_timestamp < \''.$start_time.'\''.'and end_timestamp > \''.$start_time.'\'')
                ->orWhere('start_timestamp < \''.$end_time.'\''.'and end_timestamp > \''.$end_time.'\'')
                ->orWhere('start_timestamp > \''.$start_time.'\''.'and end_timestamp < \''.$end_time.'\'')
//                ->createCommand()->getSql();
                ->asArray()->all();
        if($start_time==$end_time){
            $task=ScheduleTaskLog::find()
                ->where(['performer'=>$user_id])
                ->andWhere('start_timestamp < \''.$start_time.'\'')
                ->andWhere('end_timestamp > \''.$end_time.'\'')
//                ->createCommand()->getSql();
                ->asArray()->all();
        }
        return $task;
    }

}
