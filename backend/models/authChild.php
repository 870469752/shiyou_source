<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.auth_item_child".
 *
 * @property string $parent
 * @property string $child
 * @property string $operate
 */
class AuthChild extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.auth_item_child';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['parent', 'child', 'operate'], 'required'],
        [['operate'], 'string'],
        [['parent'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        'parent' => Yii::t('app', 'Parent'),
        'child' => Yii::t('app', 'Child'),
        'operate' => Yii::t('app', 'Operate'),
        ];
    }

    /**
     * 创建一个授权
     * @return [type] [description]
     */
    public function updateByParent()
    {
       AuthChild::deleteByParent($this->parent);
       $auth = Yii::$app->authManager;
       foreach($this->child as $key => $value){
           $parent = $auth->createRole($this->parent);
           $child = $auth->createPermission($value);
           $auth->addChild($parent, $child);
       }
       $this->updateOperateByP($this->parent);
       return true;
   }


   /**
    * 更新 授权操作
    * @param  [type] $parent [description]
    * @return [type]         [description]
    */
    public function updateOperateByP($parent){
        $all_obj = AuthChild::findModelsByP($parent);
        foreach($all_obj as $obj){
            $obj->operate = $this->operate;
            $obj->save();
        }
    }

     /**
      * 利用 parent 将所有关于当前角色的 授权 删除
      * @param  [type] $parent [description]
      * @return [type]         [description]
      */
     public static function deleteByParent($parent)
     {
        AuthChild::deleteAll(['parent' => $parent]);
    }

    /**
     * 利用parent  实例化一个authchild对象
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function findModelByP($id)
    {
        if (($model = AuthChild::findOne(['parent' => $id])) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * 利用parent  实例多个个authchild对象
     * @param  [type] $parent [description]
     * @return [type]         [description]
     */
    public static function findModelsByP($parent)
    {
        if (($models = AuthChild::findAll(['parent' => $parent])) !== null) {
            return $models;
        } else {
            return false;
        }
    }

    /**
     * 获取当前parent所有权限
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function findAllChild($id)
    {
        if($all_obj = AuthChild::findModelsByP($id)){
            $childs = [];
            foreach($all_obj as $obj){
                $childs[] = $obj->child;
            }   
            return $childs;
        }
        return false;
    }

    /**
     * 通过当前的 parent 和 child 确定 操作
     * @param  [type] $parent [description]
     * @param  [type] $child  [description]
     * @return [type]         [description]
     */
    public static function findByParentChild($parent, $child){
        return AuthChild::findOne(['parent' => $parent, 'child' => $child]);
    }
}
