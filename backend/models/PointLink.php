<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.point_link".
 *
 * @property integer $id
 * @property string $name
 * @property integer $main_point
 * @property string $main_point_name
 * @property string $link_point
 * @property string $link_point_name
 */
class PointLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.point_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'main_point_name', 'link_point', 'link_point_name'], 'string'],
            [['main_point', 'link_point'], 'required'],
            [['main_point'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'main_point' => 'Main Point',
            'main_point_name' => 'Main Point Name',
            'link_point' => 'Link Point',
            'link_point_name' => 'Link Point Name',
        ];
    }
}
