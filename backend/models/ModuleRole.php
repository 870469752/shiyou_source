<?php

namespace backend\models;

use common\library\MyFunc;
use Yii;
use yii\helpers\BaseArrayHelper;
use backend\models\Module;
use yii\web\Session;

/**
 * This is the model class for table "core.module_role".
 *
 * @property integer $id
 * @property integer $module_id
 * @property string $auth_action
 * @property string $role_id
 */
class ModuleRole extends \yii\db\ActiveRecord
{
    public $action_module;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.module_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id','auth_action'], 'required'],
            [['module_id'], 'integer'],
            [['auth_action', 'action_module'], 'string'],
            [['role_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module_id' => Yii::t('app', '模块'),
            'auth_action' => Yii::t('app', '权限方法设置'),
            'role_id' => Yii::t('app', '角色id'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    /**
     * @param string $_where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRoleAuth($_where = '')
    {
         return self::find()->with([
                'role' => function($query){
                        $query->select('id, name');
                    },
                'module' => function($query){
                        $query->select('id, module');
                    }
            ])->where($_where)->asArray()->all();
    }


    public static function getByRoleId($role_id)
    {
        return self::find()->where(['role_id' => $role_id])->asarray()->all();
    }

    public function findByRoleId($id)
    {
        return self::find()->select("module_id")->where("role_id=$id")->asArray()->all();
    }

    //根据角色ID查找模块信息
    public static function findModuleByRoleId()
    {
        $session = new Session();
        $session->open();
        $role_id = $session['menu_role_id'];
        $m = 'core.module';
        $m_r = 'core.module_role';
        $select = "$m.id,$m.name";
        return self::find()->select($select)->leftJoin($m,"$m_r.module_id = $m.id")->where("role_id = $role_id")->asArray()->all();
    }

    public function moduleRoleSave($data)
    {
        if(count($data['all_data']['delete'])){
            foreach($data['all_data']['delete'] as $key => $value){
                $model = self::getOneByRoleIdAndModuleId($value, $data['role_id']);
                $model->delete();
            }
        }
        if(count($data['all_data']['update'])){
            foreach($data['all_data']['update'] as $k => $v){
                $model = self::getOneByRoleIdAndModuleId($v[0], $data['role_id']);
                $au_ac = $this->changeDataType($v);
                $result =MyFunc::createJson($au_ac, 'list');
                self::find()->createCommand()->update('core.module_role', ['auth_action' => $result], "role_id=".$data['role_id']."and module_id=".$v[0])->execute();
            }
        }
        if(count($data['all_data']['insert'])){
            foreach($data['all_data']['insert'] as $k => $v){
                $au_ac = $this->changeDataType($v);
                $this->objOfNewSave($v[0],$au_ac,$data['role_id']);
            }
        }
        return true;
    }

    public function changeDataType($data){
        if(count($data)==1){
            $au_ac = [];
        }else{
            $au_ac = array_slice($data,1);
        }
        return $au_ac;
    }

    public function moduleRoleSaveNew($data)
    {
        foreach($data['auth_array'] as $k => $v){
            $au_ac = $this->changeDataType($v);
            $result=$this->objOfNewSave($v[0],$au_ac,$data['role_id']);
            if($result)continue;
        }        return true;
    }

    public static function objOfNewSave($module_id, $action_info, $role_id)
    {
        $model = new ModuleRole();
        $model->module_id = $module_id;
        $model->role_id = (string)$role_id;
        $model->auth_action =  MyFunc::createJson($action_info, 'list');
        return $model->save();
    }

    public static function getOneByRoleIdAndModuleId($module_id, $role_id)
    {
        return self::find()->where(['role_id' => $role_id, 'module_id' => $module_id])->one();
    }

    public static function accessAuth($request)
    {
        $allPermissionsIdd = md5('allPermissions'.Yii::$app->user->id);
        $roleIdOfUser = MyFunc::getRoleId();
        $allPermissions = Yii::$app->cache->get($allPermissionsIdd);
        if ($allPermissions == false) {

            $allPermissions = self::getRoleAuth(['role_id'=>$roleIdOfUser]);
            Yii::$app->cache->set($allPermissionsIdd, $allPermissions);

        };

        foreach($allPermissions as $indexValue => $singlePermission) {
            if (!empty($singlePermission['module']['module']) && !empty($singlePermission['auth_action'])) {

                $authActionArray = json_decode($singlePermission['auth_action'], true);
                $moduleId = json_decode($singlePermission['module']['id'], true);

                $modulePath = Module::find()->select('id,path')->where(['id'=>$moduleId])->asArray()->all();

                $request = explode('/', $request)[0];

                $tmp = (strpos($modulePath[0]['path'], $request));

                if (!is_bool($tmp)) {
                    return true;
                }

//                foreach ($authActionArray['data'] as $singleAction) {
//                    if ($request === $singlePermission['module']['module'].'/'.$singleAction ||
//                        $request === $singlePermission['module']['module'] ||
//                        $request === $singlePermission['module']['module'].'/index'
//                    ) {
//                        return true;
//                    }
//                }

            }
        }
        return false;
    }

    /************************权限 验证 * start******************************/
//    public static function accessAuth($request)
//    {
//        $user_role = json_decode(Yii::$app->user->identity->role_id, true);
//
//        //获取 当前用户的所有角色的权限
//        $res = self::getRoleAuth(['role_id' => $user_role['data']]);
//
//        //循环验证 权限
//        foreach($res as $value)
//        {
//           if(!empty($value['module']['module']) && !empty($value['auth_action'])){
//                $action_arr = json_decode($value['auth_action'], true);
//                // 循环 方法 进行验证
//                foreach($action_arr['data'] as $action){
//                    /**
//                     * 验证方式：                                                                    处理
//                     *          1、请求连接 包含 [模块名/]控制器/方法                               直接进行验证
//                     *          2、请求连接 只含有 [模块名/]控制器                                 进行模块的权限验证
//                     *          3、请求链接 包含 [模块名/]控制器/index                           将拥有的权限模块后加 '/index'
//                     */
//                    if($request === $value['module']['module'].'/'.$action ||
//                       $request === $value['module']['module'] ||
//                       $request === $value['module']['module'].'/index'){
//                        return true;
//                    }
//                }
//           }
//        }
//        return false;
//    }
}
