<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "protocol.access_card_log".
 *
 * @property integer $id
 * @property string $timestamp
 * @property integer $access_card_id
 * @property string $message
 * @property integer $access_controller_id
 */
class AccessCardLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol.access_card_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp', 'access_card_id', 'access_controller_id'], 'required'],
            [['timestamp'], 'string'],
            [['access_card_id', 'access_controller_id'], 'integer'],
            [['message'], 'string', 'max' => 2048]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '门禁刷卡记录
',
            'timestamp' => 'Timestamp',
            'access_card_id' => '门禁卡编号，对应 access_card 表中的 id 字段
',
            'message' => '刷卡消息',
            'access_controller_id' => '门禁设备编号，对应 access_controller 表中的 id',
        ];
    }
}
