<?php

namespace backend\models;

use Yii;
use backend\models\Item;
use backend\models\Module;
use common\library\MyFunc;
use yii\db\Connection;
/**
 * This is the model class for table "core.menu_config".
 *
 * @property integer $id
 * @property string $description
 * @property string $depth
 * @property integer $module_id
 * @property integer $parent_id
 * @property string $icon
 *
 * @property MenuConfig[] $menuConfigs
 * @property AuthItem[] $authItems
 */
class Menu extends \yii\db\ActiveRecord
{
    public $leaf_node;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.menu_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ['leaf_node', 'default', 'value'=>null],
        [['description', 'parent_id','depth'], 'required'],
        [['icon', 'url', 'param'], 'string', 'max' => 255],
        [['user_id', 'idx'], 'integer'], 
        [['user_id_new'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        'description' => Yii::t('app', 'Menu Name'),
        'parent_id' => Yii::t('app', 'Parent ID'),
        'icon' => Yii::t('app', 'Icon'),
        'leaf_node' => Yii::t('app', 'Function module'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuConfigs()
    {
        return $this->hasMany(MenuConfig::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItems()
    {
        return $this->hasMany(AuthItem::className(), ['menu_id' => 'id']);
    }

    /**
     * 获得 所有无连接、深度不超过4、非自己及子级菜单的菜单项
     * @return [type] [description]
     */
    public static function findcorrespondMenu($id = 'null')
    {
        $chl_ids = (intval($id))?Menu::findALLChl($id):'';
        //控制 深度 只有4级菜单 即 文件夹 可含有3层
        $max_depth = $chl_ids ? Menu::find()->select(['max(depth) as max_p', 'min(depth) as min_p'])
                                            ->where('id in ('.$chl_ids.')')
                                            ->asarray()->all():0;
        $where = (intval($id)) ? 'depth < '.(4 - ($max_depth[0]['max_p'] - $max_depth[0]['min_p'])) .' and id not in('.$chl_ids.')':('depth < 3');
        return Menu::find()->where($where)->andWhere(['not in', 'id', [0]])->andWhere('url is null')->orderBy('idx')->asarray()->all();
    }

    public static function findUrlMenu($id = null)
    {
        return self::find()->Where('url is not null')->andWhere(['not in', 'id', [0]])->andwhere(['in','parent_id',[1,$id]])->asArray()->all();
    }


    /** 
     * 获取所有 子菜单  只是用 3层菜单 2层文件
     * @param  [type] $id [description]
     * @return String     返回子菜单id以逗号分隔
     */
    public static function findAllChl($id)
    {
        static $s = '';
        $chl = Menu::find()->select('id')
            ->where('parent_id in ('.$id.')')
            ->asArray()->all();
        $ids = !empty($chl)?implode(',',MyFunc::array_multiToSingle($chl, true)):$id;
        if($id !== $ids){
            self::findAllChl($ids);
        }
        $s .= ','.$id;
        return implode(',', array_unique(explode(',', trim($s, ','))));
    }

    /**
     * 菜单创建保存方法
     * 判断当前菜单是否插入成功，成功后用当前插入id修改叶子节点的menu_id
     * 最后清空 缓存文件
     * @return [type] [description]
     */
    public function menuSave()
    {
        if($this->id){
            $old_attr = $this->oldAttributes;
            $this->description = MyFunc::updateJson($this->description, $old_attr['description']);
            //如果 父级菜单改变 将会使其及其所有的子菜单 depth、idx发生改变
            if($this->parent_id != $old_attr['parent_id']){
                $parent_model = self::getById($this->parent_id);
                $this->depth = $parent_model->depth + 1;
                $sub_ids = self::findAllChl($this->id);
                Yii::$app->db
                        ->createCommand('UPDATE core.menu_config SET depth = depth + 1 WHERE id in ('.$sub_ids.')')
                        ->execute();;
            }
        }else{
            $this->idx = self::findMaxSerial() + 1;
            $this->description = MyFunc::createJson($this->description);
        }
        $url = $this->url;
        $this->icon = !empty($this->icon) ? $this->icon : (!empty($url) ? '#' : 'fa-folder-open');
        $p_data = Menu::getById($this->parent_id);
        $this->depth = $p_data->depth + 1;
        $res = Menu::save();

        //未能修改原来的leaf_node的parent_id
        if($res && !empty($this->leaf_node)){
            //更新 leaf_node parent_id
            foreach($this->leaf_node as $key => $value){
                $model = self::getById($value);
                $model->parent_id = $this->id;
                $model->depth = $this->depth + 1;
                $model->idx = self::findMaxSerial() + 1;
                $model->save();
            }
            Yii::$app->cache->flush();
            return true;
        }
        return true;
    }

    /**
     * 查找当前最大 序列号
     * @return int
     */
    public static function findMaxSerial()
    {
        return self::find()->max('idx');
    }

    /**
     * 利用 主键 删除 所有叶子节点
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function clearLeafNodeById($id)
    {
        $leaf_str = MyFunc::getArr2Str(Menu::getArrLeafNode($id));
        if($leaf_str)
            return Item::updateByName($leaf_str, ['menu_id'=>0]);
        return true;

    }
    
    public static function getById($id){
        return Menu::findOne($id);
    }

    /**
     * 利用 主键 id 删除记录
     * 判断 是否有子节点 和叶子节点  逐一删除
     * 如果存在子菜单
     *              1、先将子菜单包括本身 下的所有叶子节点的 父级id变为0
     *              2、删除子菜单及本身
     * 删除缓存文件
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function deleteById($id){
        $menu_arr = [];
        //找出所有 当前删除菜单的 子菜单
        $menu_arr = Menu::find()->select('id')->where(['parent_id'=>$id])->asarray()->all();
        //将本身 也放入菜单数组中
        array_push($menu_arr, intval($id));
        $menu_arr = MyFunc::array_multiToSingle($menu_arr);
        //将子菜单包括本身菜单 下的叶子节点 的父级 id 变为0
        $leaf_arr = Menu::getLeafNode($menu_arr);
        if(!empty($leaf_arr))
        {
            foreach($leaf_arr as $key => $value){
                $model = self::findOne($value);
                $model->parent_id =1;//把所有没有所属的功能模块parent_id改成1；实际id=1没有菜单正好不显示出来;
                $model->save();
            }
        }
        //得到所有的url节点
        $menu_url=Menu::find()->select('id')->Where('url is not null')->asArray()->all();

        //因为以前会删掉功能模块所以处理菜单数组
        //去掉菜单数组中的功能模块节点再删除
      for($i=0;$i<count($menu_url);$i++){
            if(in_array($menu_url[$i]['id'],$menu_arr)){
                $j=array_search($menu_url[$i]['id'],$menu_arr);
               array_splice($menu_arr,$j,1);
            }
        }
        //删除 菜单
        $result = Menu::deleteAll(['id'=>$menu_arr]);
        //清空缓存
        Yii::$app->cache->flush();
        return $result;
    }

    /**
     * 利用 主键 找到所有叶子节点
     * @param  [type] $id [description]
     * @return [type]     [description] 返回主键值
     */
    public static function getLeafNode($id){
        return self::find()->select('id')
        ->where(['parent_id' => $id])
        ->asarray()->all();
    }

    //返回同一父类的菜单信息 $data[parent_id] {id parent_id  name}
    public function getsubmenu($parent_id){
                $info=self::find()->where(['parent_id'=>$parent_id])->asArray()->all();
               foreach($info as $value) {
                   $name = MyFunc::DisposeJSON($value['description']);
                   $data[] = ['parent_id' => $value['parent_id'], 'description' => $name, 'id' => $value['id'], 'idx' => $value['idx'], 'depth' => $value['depth']];

        }
                    return $data;

    }
    // 得到所有菜单信息 按父类id分类  arr[parent_id]=菜单信息
    public function getAllInfo(){
        $ids=self::find()->distinct()->select('parent_id')->asArray()->all();
        foreach($ids as $idarr) {
            foreach($idarr as $id) {
                if (self::leafIsEmpty($id)) {
                    $data[$id] ='';
                } else {
                    $data1 = self::getsubmenu($id);
                    $data[$id] = $data1;
                }
            }
        }
        return $data;
    }

  //判断是否有子菜单
      public function leafIsEmpty($id){
          $result=sizeof(self::getLeafNode($id));
          if($result) return false;
          else  return true;
      }

    /**
     * 利用菜单主键 获取 所有叶子节点 以一维数组的形式返回
     * 解析数组[
     *     [0] => [
     *         'name' => 'cre',
     *     ],
     *     [1] => [
     *         'name' => 'adc',
     *     ],
     * ]
     * 为
     *        ['cre','adc']
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public static function getArrLeafNode($id){
        return self::find()->select('id')->where(['parent_id' => $id])->asArray()->all();
    }
    public static function getArrUrlLeafNode($id){
        return self::find()->select('id')->where(['parent_id' => $id])->andWhere('url is not null')->asArray()->all();
    }
    /**
     * 获取整个树状菜单
     * @return array
     */
    public static function findTreeMenu()
    {
        return MyFunc::TreeData(Menu::find()->select(['id','name' => 'description', 'parent_id'])
            ->asArray()->indexBy('id')->all());
    }
    
    public function getParentId($id)
    {
        $temp = self::find()->select('parent_id')->where(['id' => $id])->asArray()->all();
        return $temp[0]['parent_id'];
//        return self::findOne($id);
    }

//    public static function getAllMenu()
//    {
//        return self::find()->asArray()->orderBy('idx')->all();
//    }

    public function getModule()
    {
        return $this->hasMany(Module::className(), ['id' => 'module_id']);
    }


    public static function getAllMenu()
    {
        return self::find()->where("user_id_new @> '{".Yii::$app->user->id."}'")->orderBy('idx')->asArray()->all();
    }

    public function getDepth($id){
        $temp = self::find()->select('depth')->where(['id' => $id])->asArray()->all();
        return $temp[0]['depth'];
    }

    public function getIconer($id)
    {
        $temp = self::find()->select('icon')->where(['id' => $id])->asArray()->all();
        return $temp[0]['icon'];
    }
    public function getMaxIdx($id){
        $temp = self::find()->select('idx')->where(['parent_id' => $id])->asArray()->all();
        $tmp = [];
        foreach($temp as $key=>$value){
            $tmp[]=$value['idx'];
        }
//        return $tmp;
        asort($tmp);
        $idx = array_pop($tmp);
        return $idx;

    }

    /**
     * @return mixed
     */
    public function get_all_menu(){
        $temp = self::find()->distinct()->select('parent_id')->where('id!=0')->asArray()->all();
        foreach($temp as $value){
            $id = $value['parent_id'];

            $tmp = self::find()->select('description')->where(['id' => $id])->asArray()->all();
            foreach($tmp as $val){
                $tempp = json_decode($val['description'],true);
                $res[$id] = $tempp['data']['zh-cn'];
            }

//            $res[] = $tmp[0]['description'];
        }
        return $res;
    }

    /**
     * 处理update_menu菜单
     *
     */
    public function updateMenuFunc($POST){

        //更新菜单，更新模块共有的变量
        $parent_id = $_POST['parent_id'];
        $description = $_POST['description'];
        $description = '{"data_type":"description","data":{"zh-cn":"'.$description.'"}}';
        $icon = !empty($_POST['icon'])?$_POST['icon']:'fa-folder-open';

        $posi_data = $_POST['posi_data'];
        $originial_data = array_filter($_POST['originial_data']);
        asort($originial_data);
        $symbol = $_POST['symbol'];

        if($_POST['symbol'] == 1) {//修改菜单、修改模块操作
            $depth = Menu::getDepth($parent_id) + 1;
        }

        if($_POST['symbol'] == 0) {//新建菜单操作
            $depth = $_POST['depth']+1;
        }
        $depth = ($depth > 3)?3:$depth;

        $id = $_POST['id'];

        //判断修改之后的位置是否是最后
        if($posi_data == 'last') {//是最后一位
            $new_idx = array_pop($originial_data) + 1;
            $sql = "update core.menu_config set icon='".$icon."',idx=$new_idx,parent_id=$parent_id,depth=$depth,description='".$description."' where id=$id";
            $command = \Yii::$app->db->createCommand($sql);
            if(!($command->execute())) {
                return false;
            }
        }

        if($posi_data != 'last') {//不是最后一位
            $temp = [];
            foreach($originial_data as $key => $value) {
                if($originial_data[$key] < $posi_data){
                    $temp[$key] = $value;
                }
                if($originial_data[$key] >= $posi_data){
                    $temp[$key] = $value+1;
                }
            }
            $temp[$id] = $posi_data;
            asort($temp);

            foreach($temp as $key=>$value) {
                if($temp[$key] > $posi_data) {
                    $sql = "update core.menu_config set idx=$temp[$key] where id=$key";
                    $command = \Yii::$app->db->createCommand($sql);
                    if(!($command->execute())) {
                        return false;
                    }
                }

                if($temp[$key] == $posi_data) {
                    $sql = "update core.menu_config set icon='".$icon."',idx=$temp[$key],parent_id=$parent_id,depth=$depth,description='".$description."' where id=$key";
                    $command = \Yii::$app->db->createCommand($sql);
                    if(!($command->execute())) {
                        return false;
                    }
                }
            }
        }

        /*
         * modify the modules of the specific menu
         * first step: delete the originial modules
         * second step: add the committed modules
         */
        if (!isset($_POST['modules_id'])) {
            //first step
            $ori_data = [];
            $temp_data = self::find()->select('id')->where("parent_id = $id and url is not null")->asArray()->all();//查找父id下的功能模块的id
            if ($temp_data) {
                foreach ($temp_data as $key => $value) {
                    $ori_data[] = $value['id'];
                }
            }

            foreach ($ori_data as $key => $value) {
                $sql = "update core.menu_config set parent_id=1 where id={$value} and url is not null";
                $command = \Yii::$app->db->createCommand($sql);
                if(!($command->execute())) {
                    return false;
                }
            }
        }

        if(isset($_POST['modules_id']))//
        {
            //first step
            $ori_data = [];
            $temp_data = self::find()->select('id')->where("parent_id = $id and url is not null")->asArray()->all();//查找父id下的功能模块的id
            if ($temp_data) {
                foreach ($temp_data as $key => $value) {
                    $ori_data[] = $value['id'];
                }
            }

            foreach ($ori_data as $key => $value) {
                $sql = "update core.menu_config set parent_id=1 where id={$value} and url is not null";
                $command = \Yii::$app->db->createCommand($sql);
                if(!($command->execute())) {
                    return false;
                }
            }

            //second step
            if(count($_POST['modules_id']) != 0) {
                foreach($_POST['modules_id'] as $key => $value) {
                    $sql = "update core.menu_config set parent_id=$id where id=$value";
                    $command = \Yii::$app->db->createCommand($sql);
                    if(!$command->execute()) {
                        return false;
                    }
                }
            }
        }

        \Yii::$app->cache->flush();

        return true;

    }

    /**
     * @param $data
     * @return bool|mixed
     */
     public function createNewMenu($data){

        $result = [];
        $data = isset($data)?$data:'';

        //组装创建新菜单所需的必须字段
        $depth = $this->getDepth($_POST['parent_id']);
        $result['depth'] = $depth;
        $depth++;
        $depth = ($depth > 3)?3:$depth;

        $description_temp = '{"data_type":"description","data":{"zh-cn":"'.$_POST['description'].'"}}';
        if(!isset($_POST['icon'])) {
            $icon = 'fa-folder-open';
        }else{
            $icon = $_POST['icon'];
        }
        $idx = $this->getMaxIdx($_POST['parent_id']);
        $idx++;

        //组装指定格式数据
        $data = [
            'Menu'=>[
                'description'=>$description_temp,
                'icon'=>$icon,
                'depth'=>$depth,
                'parent_id'=>$_POST['parent_id'],
                'idx'=>$idx,
            ]
        ];
        //加载数据模型
        $this->load($data);
        //生成新菜单
        $result = $this->save();
        if(!$result) {
            return false;
        }
         //返回主键id
         return $newid = $this->primaryKey;

    }

}
