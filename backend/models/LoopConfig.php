<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.control_info".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $related_id
 * @property integer $time_mode
 * @property string $data
 */
class LoopConfig extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.loop_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'times', 'subsystems'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'times' => 'Times',
            'subsystems' => 'Subsystems',
        ];
    }
    public static function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            $model = new LoopConfig();
            return $model;
        }
    }
}
