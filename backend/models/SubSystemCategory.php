<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.sub_system_category".
 *
 * @property integer $id
 * @property string $name
 */
class SubSystemCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.sub_system_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * 查找出所有子节点
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function allData()
    {
        return static::find()->select("id, name->'data'->>'zh-cn' as name")
            ->asArray()->all();
    }
}
