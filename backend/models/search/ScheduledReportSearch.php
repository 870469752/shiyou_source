<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScheduledReport;
use common\library\MyFunc;

/**
 * ScheduledReportSearch represents the model behind the search form about `backend\models\ScheduledReport`.
 */
class ScheduledReportSearch extends ScheduledReport
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'owner'], 'integer'],
            [['create_timestamp', 'config', 'name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ScheduledReport::find()->where(Yii::$app->user->identity->id ."= ANY (auth_users) ")->orderBy("id desc");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }

    public function newSearch($ids,$params)
    {
        $con = "id in $ids";
        if($ids=="()" || $ids ==""){
            $con = "id = 0";
        }
        $query = ScheduledReport::find()->where($con)->orderBy("id desc");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
