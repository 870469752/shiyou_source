<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScheduledReportLog;
use common\library\MyFunc;

/**
 * ScheduledReportLogSearch represents the model behind the search form about `backend\models\ScheduledReportLog`.
 */
class ScheduledReportLogSearch extends ScheduledReportLog
{
    public $name;
    public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'report_id'], 'integer'],
            [['start_timestamp', 'end_timestamp', 'data'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$id)
    {
        //echo "<pre>";
        $query = ScheduledReportLog::find()->where('report_id = '.$id)->orderBy('start_timestamp desc');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);
        //print_r($dataProvider);die;
        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }

        $this->load($params);
        $this->validate();
        //print_r($dataProvider);die;
        return $dataProvider;
    }

    public function searchData($id)
    {
        $data = ScheduledReportLog::find()->select('data')->where('id = '.$id)->orderBy('id desc')->asArray()->all();
        $data = MyFunc::JsonFindValue($data[0]['data'],'data');
        $arr = $this->arrayLevel($data['data']);

        //最小日期与数据组合
        $arr_last_level=$arr[$arr['level']-1];
        $arr_data = $arr['data'];
        $j=0;
        for($i=0; $i<count($arr_last_level)-1; $i++){
            $z=0;
            $arr_temp['num']=$arr_last_level['num'];
            $arr_temp[$i][]=$arr_last_level[$i];
            for($k=$j; $k<count($arr_data); $k++){
                $arr_temp[$i][]=$arr_data[$k];
                $j++;
                $z++;
                if($z==count($arr['name'])){
                    break;
                }
            }
        }
        $arr[$arr['level']-1]=$arr_temp;
        //echo "<pre>";print_r($arr);die;
        //计算合并的行数row_num
        for($i=0; $i<$arr['level']-1; $i++){
            $arr[$i]['row_num']=1;
            for($j=$i+1; $j<$arr['level']; $j++){
                $arr[$i]['row_num']*=$arr[$j]['num'];
            }
        }

        //组合所有层
        for($i=$arr['level']-2; $i>=0; $i--){
            for($k=0; $k<count($arr[$i])-2; $k++){
                for($z=0; $z<$arr[$i]['row_num']; $z++){
                    if($z==0){
                        $tem[$k][]=$arr[$i][$k];
                    }else {
                        $tem[$k][] = $arr[$i][$k];
                        //$tem[$k][] = ' ';
                    }
                }
            }
            $arr['num'][]=$arr[$i]['num'];
            $arr['row_num'][]=$arr[$i]['row_num'];
            $arr[$i]=$tem;
            $tem='';
            $tem_i = 0;
            $tem_j = $arr[$i+1];
            for($j=0; $j<count($arr[$i]); $j++){
                 for($z=0; $z<count($arr[$i][$j]); $z++){
                     $tem_arr[$tem_i][] = $arr[$i][$j][$z];
                     /*if($arr[$i][$j][$z] != ' '){
                         $tem_arr[$tem_i][] = $arr[$i][$j][$z];
                     }*/
                     for($c=0; $c<count($tem_j[$tem_i]); $c++){
                         $tem_arr[$tem_i][] = $tem_j[$tem_i][$c];
                         /*if( $tem_j[$tem_i][$c] !=' '){
                             $tem_arr[$tem_i][] = $tem_j[$tem_i][$c];
                         }*/
                     }
                     $tem_i++;
                }
            }
            $arr[$i]=$tem_arr;
            $tem_arr='';
        }
        //echo "<pre>";print_r($arr);die;
        //组成新数组
        if(isset($arr[0]['num'])){ unset($arr[0]['num']);}
        $new_arr['data']=$arr[0];//行数据
        //$new_arr['arr_num']=array_reverse($arr['num']);//每层实际个数
        //$new_arr['row_num']=array_reverse($arr['row_num']);//每层需要合并的行数
        $new_arr['name']=$arr['name'];//表头名称
        $new_arr['level']=$arr['level'];//层数
        //echo "<pre>";print_r($new_arr);die;
        return $new_arr;
    }

    /**
     * 返回数组的维度
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    function arrayLevel($arr)
    {
        $column = array();
        $level = -1;
        function aL($arr,&$column,$level){
            if(isset($arr[0]['has_child'])){
                $level++;
                $column[$level]['num'] = count($arr);
                //print_r($column);die;
                foreach($arr as $k => $v){
                    $column[$level][] = $v['name'];
                    if($arr[0]['has_child'] == 0){
                        foreach($v['data'] as $key => $value){
                            $column['data'][] = $value['value'];
                        }
                    }
                    aL($v['data'],$column,$level);
                }
            }else{
                foreach($arr as $k => $v){
                    $column['name'][$k] = $v['name'];
                }
                $column['level']=$level+1;
            }
            return $column;
        }

        aL($arr,$column,$level);

        //echo "<pre>";print_r($column);die;
        return $column;
    }

    /**
     * 返回数组的维度
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    function arrayLevel2($arr){
        $al = array(0);
        function aL($arr,&$al,$level=0){
            if(is_array($arr)){
                $level++;
                $al[] = $level;
                foreach($arr as $v){
                    aL($v,$al,$level);
                }
            }
        }
        aL($arr,$al);
        return max($al);
    }
}
