<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DemoPoint;
use common\library\MyFunc;

/**
 * DemoPointSearch represents the model behind the search form about `backend\models\DemoPoint`.
 */
class DemoPointSearch extends DemoPoint
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'interval', 'protocol_id', 'unit'], 'integer'],
            [['name', 'base_value', 'base_history', 'last_update', 'last_value', 'value_filter', 'value_type'], 'safe'],
            [['is_available', 'is_shield', 'is_upload', 'is_record', 'is_system'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = DemoPoint::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);
        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
