<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BacnetPoint;
use common\library\MyFunc;
use backend\models\Category;

/**
 * BacnetPointSearch represents the model behind the search form about `backend\models\BacnetPoint`.
 */
class BacnetPointSearch extends BacnetPoint
{
	
    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = static::baseSelect()->select('*')->orderBy('id desc')->with(['category', 'unit'])->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);
        $this->validate();
        if($this->name){
            $query->andWhere("name->'data'->>'".Yii::$app->language."' like :name",[':name' => '%'.$this->name.'%']);
        }
        $query->andFilterWhere(['id' => Category::findCategoryPoint($this->category_id)]);

        return $dataProvider;
    }
}
