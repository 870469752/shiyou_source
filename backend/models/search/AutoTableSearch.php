<?php
/**
 * Created by PhpStorm.
 * User: baiyang
 * Date: 2015/8/10
 * Time: 14:20
 */

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\library\MyFunc;
use backend\models\AutoTable;

class AutoTableSearch extends AutoTable{
    public $field;
    public $relation;
    public $value;

    public static function tableName()
    {
        return 'core.auto_table';
    }

    public function rules()
    {
        return [
            [['name', 'table_task', 'create_date'], 'string']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = AutoTable::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}