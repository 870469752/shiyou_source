<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Module;
use common\library\MyFunc;

/**
 * ModuleSearch represents the model behind the search form about `backend\models\Module`.
 */
class ModuleSearch extends Module
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description', 'path', 'action_list'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Module::find()->orderBy("id desc");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }

    public function _searchByRoleId($id)
    {
        $mr = 'core.module_role';
        $m = 'core.module';
        $select = "$mr.id as mr_id,$mr.module_id,$mr.auth_action->'data' as auth_action,
        $mr.role_id,$m.id as m_id,$m.name->'data'->>'zh-cn' as name,$m.description->'data'->>'zh-cn' as description,
        $m.action_list->'data' as action_list" ;
        $result_role_module = Module::find()->select($select)->leftJoin($mr,"$m.id=$mr.module_id")->Where("$mr.role_id=$id")->asArray()->all();
        $result_all_module = $this->_searchAllMoudle();
        foreach($result_all_module as $k => $v){
            if(count($result_role_module)){
                foreach($result_role_module as $kk => $vv){
                    if($v['m_id']==$vv['m_id']){
                        $result_all_module[$k]=$result_role_module[$kk];
                        unset($result_role_module[$kk]);
                        break;
                    }
                }
            }else{
                break;
            }
        }
        return $result_all_module;
    }

    public function _searchAllMoudle()
    {
        $select = "id as m_id,name->'data'->>'zh-cn' as name,description->'data'->>'zh-cn' as description,
        action_list->'data' as action_list" ;
        $result = Module::find()->select($select)->Where("id !=0")->asArray()->all();
        foreach($result as $k => $v){
            $result[$k]['mr_id']='';
            $result[$k]['module_id']='';
            $result[$k]['auth_action']='';
            $result[$k]['role_id']='';
        }
        return $result;
    }

    public function _search()
    {
        $m = 'core.module';
        $select = "$m.id,$m.name->'data'->>'zh-cn' as name,$m.description->'data'->>'zh-cn' as description,
        $m.action_list->'data' as action_list" ;
        $result = Module::find()->select($select)->asArray()->all();
        return $result;
    }
}
