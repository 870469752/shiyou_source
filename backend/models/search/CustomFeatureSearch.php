<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CustomFeature;
use common\library\MyFunc;

/**
 * ScAddressSearch represents the model behind the search form about `backend\models\ScAddress`.
 */
class CustomFeatureSearch extends CustomFeature
{
	
    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = static::find()->with('user');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);
        if(isset($params['url'])){
            $query->where(['url' => $params['url']]);
        }

        return $dataProvider;
    }
}
