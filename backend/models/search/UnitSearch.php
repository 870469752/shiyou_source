<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Unit;
use common\library\MyFunc;

/**
 * UnitSearch represents the model behind the search form about `backend\models\Unit`.
 */
class UnitSearch extends Unit
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'standard_unit'], 'integer'],
            [['name', 'coefficient'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        //初始化页面与查找页面 查询不同 判断传入的值

//        if (array_key_exists("view", $params) || empty($params)) {
////            $query = Unit::find()
////                ->select(['unit.id as id','unit.name as name','y.name as standard_unit','unit.coefficient as coefficient'])
////                ->from('core.unit')
////                ->join('INNER JOIN','core.unit y','y.id=unit.standard_unit');
//            $query = Unit::find()
//                ->select(['unit.id as id','unit.name as name','y.name as standard_unit','unit.coefficient as coefficient','unit.category_name as category_name'])
//                ->join('LEFT JOIN','core.unit_category y','y.id=unit.category_id');
//        }
//        else {
//            $query = Unit::find();
//        }
        $query = Unit::find()
            ->select(['unit.id as id','unit.unit_symbol as unit_symbol','unit.name as name','z.name as standard_unit','unit.coefficient as coefficient','y.name as category_id'])
            ->where(['not in','unit.coefficient',[1]])
            ->where(["unit.flag"=>"true"])
            ->join('INNER JOIN','core.unit z','z.id=unit.standard_unit')
            ->join('LEFT JOIN','core.unit_category y','y.id=unit.category_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);
        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        $FieldType = $this->tableSchema->columns[$field]->dbType;
        $relation = $this->relation[$i];

        // 制造数组传入模型验证
        $params[$model_name] += [$field => $v];

        if(strpos($relation, 'like') !== false){
            $v = '%'.$v.'%';
        }
        $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

    }
        $this->load($params);
        $this->validate();
//        echo'<pre>';
//        print_r($dataProvider);
//        die;
        return $dataProvider;
    }
    public function _search($params)
    {
        //初始化页面与查找页面 查询不同 判断传入的值

//        if (array_key_exists("view", $params) || empty($params)) {
////            $query = Unit::find()
////                ->select(['unit.id as id','unit.name as name','y.name as standard_unit','unit.coefficient as coefficient'])
////                ->from('core.unit')
////                ->join('INNER JOIN','core.unit y','y.id=unit.standard_unit');
//            $query = Unit::find()
//                ->select(['unit.id as id','unit.name as name','y.name as standard_unit','unit.coefficient as coefficient','unit.category_name as category_name'])
//                ->join('LEFT JOIN','core.unit_category y','y.id=unit.category_id');
//        }
//        else {
//            $query = Unit::find();
//        }
        $query = Unit::find()
            ->select(['unit.id as id','unit.unit_symbol as unit_symbol','unit.name as name','z.name as standard_unit','unit.coefficient as coefficient','y.name as category_id'])
            ->where(['not in','unit.coefficient',[1]])
            ->where(["unit.flag"=>"1"])
            ->join('INNER JOIN','core.unit z','z.id=unit.standard_unit')
            ->join('LEFT JOIN','core.unit_category y','y.id=unit.category_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();
//        echo'<pre>';
//        print_r($dataProvider);
//        die;
        return $dataProvider;
    }

}