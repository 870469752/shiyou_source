<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Event;
use common\library\MyFunc;

/**
 * EventSearch represents the model behind the search form about `backend\models\Event`.
 */
class EventSearch extends Event
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'type'], 'safe'],
            [['is_valid', 'trigger_status', 'is_system'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Event::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }

    /**
     * 此方法需要 返回报警设置选中分类对应点位 或直接 点位 对应的报警事件触发值 和 报警规则
     * @param $params
     * @return ActiveDataProvider
     */
    public function _searchList($params)
    {
        $e = "core.event";
        //先将 对应的点位id找到
        $query = Event::find()->select("$e.id,$e.name->'data'->>'zh-cn' as name,$e.type,$e.is_valid,")->where("$e.id != 0 ")->orderBy("$e.id desc");
        //where("$e.id not in(select event_id from core.alarm_rule)")->
        $point_obj = new Event();
        $point_obj->load($params, '');
        $point_obj->validate();
        //处理 查询点位的 条件
        if($params['event_condition'])
        {
            //可以用 sql正则 目前用Like
            $condition_arr = explode(',', $params['event_condition']);
            foreach($condition_arr as $value)
            {
                $query->andWhere("$e.name->'data'->>'zh-cn' like '%".$value."%'");
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
        return $dataProvider;
    }
}
