<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 2016/6/24
 * Time: 10:22
 */

namespace backend\models\search;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\schemaControl;
use common\library\MyFunc;

class schemaControlSearch extends schemaControl
{
    public $field;
    public $relation;
    public $value;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [[ 'control_info'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = schemaControl::find()->
        select("core.schema_control.id as id, core.schema_control.name as name,core.schema_config.name as TimeMode")->
        leftJoin('core.schema_config','core.schema_config.id = core.schema_control.shema_manage_id')->
        orderBy('core.schema_control.id ');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}