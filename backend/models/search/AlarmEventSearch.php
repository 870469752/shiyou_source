<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AlarmEvent;
use common\library\MyFunc;

/**
 * AlarmEventSearch represents the model behind the search form about `backend\models\AlarmEvent`.
 */
class AlarmEventSearch extends AlarmEvent
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'type'], 'safe'],
            [['is_valid'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = AlarmEvent::find()->where(['is_system' => false]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];

        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
            //如果字段是name,要处理json格式数据
            if($field == 'name'){

                if(strpos($relation, 'like') !== false){
                    $v = '%'.$v.'%';
                    $condition = "$field->'data'->>'zh-cn'{$relation}'{$v}'";
                }else{
                    $condition = "$field->'data'->>'zh-cn'='{$v}'";
                }
                $query->andWhere($condition);

            }else{

                if(strpos($relation, 'like') !== false){
                    $v = '%'.$v.'%';
                }
                $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

            }
        	
        }
        $this->load($params);
        $this->validate();
        return $dataProvider;
    }
}
