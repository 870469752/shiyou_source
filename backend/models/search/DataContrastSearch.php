<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-3-25
 * Time: 下午3:08
 */

namespace backend\models\search;

use backend\models\form\DataContrastForm;
use backend\models\Point;
use common\library\MyFunc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DataContrastSearch extends DataContrastForm{
    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->load($params);
        return $dataProvider;
    }

} 