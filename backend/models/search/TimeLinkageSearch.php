<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TimeLinkage;
use common\library\MyFunc;

/**
 * StationSearch represents the model behind the search form about `backend\models\Station`.
 */
class TimeLinkageSearch extends ControlInfo
{
    public $field;
    public $relation;
    public $value;
    public function rules()
    {
        return [
            [['name', 'time_mode_name','related_id', 'data'], 'string'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TimeLinkage::find()->select(['control_info.id as id','control_info.name as name','y.name as time_mode_name'])
            ->join('LEFT JOIN','core.schema_config y','y.id=
            .time_mode');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
