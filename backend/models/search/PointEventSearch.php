<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PointEvent;
use common\library\MyFunc;
use backend\models\Point;
use backend\models\Category;
use yii\helpers\BaseArrayHelper;
/**
 * AtomEventSearch represents the model behind the search form about `backend\models\AtomEvent`.
 */
class PointEventSearch extends PointEvent
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'point_id'], 'integer'],
            [['name', 'type', 'time_range', 'trigger'], 'safe'],
            [['is_valid', 'is_system'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PointEvent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 500,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];

        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];

        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }

    /**
     * 此方法需要 返回报警设置选中分类对应点位 或直接 点位 对应的报警事件触发值 和 报警规则
     * @param $params
     * @return ActiveDataProvider
     */
    public function _search($params)
    {
        //先将 对应的点位id找到
        $query = Point::baseSelect()->select('id,name')->orderBy('id desc')->limit(5000);

        $point_obj = new Point();
        $point_obj->load($params, '');
        $point_obj->validate();
        //处理 查询点位的 条件
        if($params['point_condition'])
        {
            //可以用 sql正则 目前用Like
            $point_condition_arr = explode(',', $params['point_condition']);
            foreach($point_condition_arr as $value)
            {
                $query->andWhere("name->'data'->>'".Yii::$app->language."' like '%".$value."%'");
            }
        }
        //$point_data = $query->andFilterWhere(['id' => Category::findCategoryPoint($point_obj->category_id)])->asArray()->all();
        $point_data = $query->asArray()->all();

        $point_ids = implode(',', BaseArrayHelper::getColumn($point_data, 'id'));
        $query1 = PointEvent::getAlarmParam($point_ids);//echo "<pre>";print_r($query1);die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query1,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
        return $dataProvider;
    }

    /**
     * 此方法需要 返回报警设置选中分类对应点位 或直接 点位 对应的报警事件触发值 和 报警规则
     * @param $params
     * @return ActiveDataProvider
     */
    public function _searchList($params)
    {
        $p = "core.point";
        $pv = "core.point_event";
        $ar = "core.alarm_rule";
        //先将 对应的点位id找到
        $query = PointEvent::find()->select("$p.name,$ar.level_sequence,$pv.id,$pv.point_id,$pv.trigger->'data'->>'trigger_type' as trigger_type, $pv.trigger->'data'->>'operator' as operator, $pv.trigger->'data'->>'value' as value")->leftJoin([$p], "$pv.point_id = $p.id")->leftJoin([$ar],"$pv.id = $ar.event_id")->where('type = 1')->orderBy("$pv.point_id asc,$pv.id desc");
        $point_obj = new PointEvent();
        $point_obj->load($params, '');
        $point_obj->validate();
        //处理 查询点位的 条件
        if($params['point_condition'])
        {
            //可以用 sql正则 目前用Like
            $point_condition_arr = explode(',', $params['point_condition']);
            foreach($point_condition_arr as $value)
            {
                $query->andWhere("core.point.name->'data'->>'".Yii::$app->language."' like '%".$value."%'");
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
        return $dataProvider;
    }
}
