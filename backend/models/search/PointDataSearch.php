<?php

namespace backend\models\search;

use backend\models\Point;
use backend\models\Unit;
use common\library\MyFunc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PointData;

/**
 * PointSearch represents the model behind the search form about `backend\models\Point`.
 */
class PointDataSearch extends \backend\models\form\PointDataForm
{

    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        isset($params[$this->formName()])?$this->load($params):$this->load($params, '');
        $dataProvider = new ActiveDataProvider([
            'query' => $this->findTableData(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $dataProvider;
    }


    public function _search($id, $start_time, $end_time)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->findIdTimeRange($id, $start_time, $end_time),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $dataProvider;
    }

    /**
     * 根据查询的点位 拼凑数据表头
     * 制造行列转换后的列名
     */
    public function createColumn()
    {
        $Point = Point::find()->select('id, name, unit')->where(['id' => $this->point_id])->indexBy('id')->all();
        $col = [[
            'attribute' => 'time',
            'label' => Yii::t('app', 'Time'),

            'format' => [
                'date',
                'format' => $this->group_default == false ?                //根据统计的类型来判断 展示时间格式化的方式
                            MyFunc::TimeCategory($this->report_type) :
                            MyFunc::TimeDefaultCategory($this->report_type)
            ]
        ]];

        foreach($this->point_id as $v){
            $unit = Unit::getById($Point[$v]['unit'])->name;
            $col[] = [
                'attribute' => 'point_id_'.$v,
                'label' => MyFunc::DisposeJSON($Point[$v]['name']) .'(' .$unit .')',
                'format' => 'floor'
            ];
        }
        return $col;
    }
}
