<?php

namespace backend\models\search;

use backend\models\Image;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Point;
use common\library\MyFunc;
use backend\models\Category;
/**
 * PointSearch represents the model behind the search form about `backend\models\Point`.
 */
class PointSearch extends Point
{
	
	public $field;
	public $relation;
	public $value;
	public $category_id;
    public function rules()
    {
        return [
            [['id', 'interval', 'protocol_id', 'unit', 'value_type', 'category_id'], 'integer'],
            [['name', 'base_value', 'base_history', 'value_timestamp', 'value', 'value_filter'], 'safe'],
            [['is_available', 'is_shield', 'is_upload', 'is_record', 'is_system'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Point::find()->where('is_shield=false');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];

        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];

        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }


    public function _search($params)
    {
        $pageSize = 10;
        $query = static::baseSelect_new()->select('')->orderBy(['id' => SORT_DESC])->with(['oneData' => function($query) use($pageSize){
                    $query->orderBy(['timestamp' => SORT_DESC])->limit($pageSize);
                }, 'unit', 'category'])->asArray();

        $this->load($params, '');
        $this->validate();
        if($this->name){
            $query->andWhere("name->'data'->>'".Yii::$app->language."' like :name",[':name' => '%'.$this->name.'%']);
        }

        if($this->protocol_id) {
            $query->andWhere(['protocol_id' => $this->protocol_id]);
        }
        if($this->category_id) {
            $query->andFilterWhere(['id' => Category::findCategoryPoint([[$this->category_id]])]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
       /*$model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];
        foreach ($this->value as $i => $v){
            // 如果是名称的话就搜索相应语言
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];

        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];

        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
            // 如果是名称的话就搜索相应语言
            $field_name = $field == 'name'?$field."->'data'->>'".Yii::$app->language."'":$field;
            // 增加下标防止替换相同字段
        	$query->andWhere($field_name.' '.$relation." :".$field.$i,[':'.$field.$i => $v]);

        }
        $this->load($params);
        $this->validate();*/
        return $dataProvider;
    }

    //2015-12-7 jlt 重写_search 未知是否能删除原_search
    public function _search_test($params)
    {
        $pageSize = 10;
        $query = static::baseSelect_new()
//            ->select('')
            ->orderBy(['id' => SORT_DESC])
//            ->with(['oneData' => function($query) use($pageSize){
//            $query->orderBy(['timestamp' => SORT_DESC])->limit($pageSize);
//        }, 'unit', 'category'])
            ->asArray();

        $this->load($params, '');
        $this->validate();
        if($this->name){
            $query->andWhere("name->'data'->>'".Yii::$app->language."' like :name",[':name' => '%'.$this->name.'%']);
        }
        if($this->protocol_id) {
            $query->andWhere(['protocol_id' => $this->protocol_id]);
        }
        if(!empty($this->category_id)) {
                $query->andFilterWhere(['id' => Category::findCategoryPoint($this->category_id)]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        return $dataProvider;
    }

    public function _search_by($params)
    {
        $query = Point::baseSelect()->select('id,name,value,is_shield,is_upload,protocol_id')->orderBy('id desc');
        $this->load($params, '');
        $this->validate();
        //处理 查询点位的 条件
        if($params['point_condition'])
        {
            //可以用 sql正则 目前用Like
            $point_condition_arr = explode(',', $params['point_condition']);
            foreach($point_condition_arr as $value)
            {
                $query->andWhere("name->'data'->>'".Yii::$app->language."' like '%".$value."%'");
            }
        }
        //$point_data = $query->asArray()->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);
        return $dataProvider;
    }
}
