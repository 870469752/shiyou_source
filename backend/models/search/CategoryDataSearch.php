<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-5-6
 * Time: 上午11:51
 */

namespace backend\models\search;

use backend\models\form\CategoryDataForm;
use backend\models\Point;
use common\library\MyFunc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CategoryDataSearch extends CategoryDataForm{
    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        $this->load($params);
        return $dataProvider;
    }

    /**
     * 对应category_data/crudController 下的actionInherentCategory
     * @param $params
     * @param $query
     * @return ActiveDataProvider
     */
    public function inherentSearch($params, $query)
    {
        return $this->search($params, $query);
    }

    public function StatisticSearch()
    {

    }
} 