<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\library\MyFunc;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
	
	public $field;
	public $relation;
	public $value;
	
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'role', 'status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $username = Yii::$app->user->identity->username;
        $con = 'id != 0';
        if(strstr($username,'电力')){
            $con = "username like '%电力%'";
        }elseif(strstr($username,'安全')){
            $con = "username like '%安全%'";
        }elseif(strstr($username,'楼控')){
            $con = "username like '%楼控%'";
        }elseif(strstr($username,'消防')){
            $con = "username like '%消防%'";
        }
        $query = User::find()->where("is_shield = FALSE")->andWhere($con)->orderBy('id desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $query->andWhere('id != 0');
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
