<?php

namespace backend\models\search;

use backend\models\Image;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\library\MyFunc;

/**
 * ImageSearch represents the model behind the search form about `common\models\Image`.
 */
class ImageSearch extends Image
{

    public $field;
    public $relation;
    public $value;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'phase_config','default','active','inactive'], 'string'],
            [['type', 'width', 'height'], 'integer'],
            [['is_system'], 'boolean'],
            [['url'], 'string', 'max' => 1023]
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Image::find()->select(["id,name->'data' ->> 'zh-cn'::TEXT as name,phase_config->'data'->>'default' as default,
                phase_config->'data'->>'active' as active,phase_config->'data'->>'inactive' as inactive"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';

        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $query->andWhere('id != 0');
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
