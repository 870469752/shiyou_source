<?php
/**
 * Created by PhpStorm.
 * User: jia
 * Date: 2016/6/8
 * Time: 17:09
 */

namespace backend\models\search;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PointDataNew;
use common\library\MyFunc;

class PointDataNewSearch extends PointDataNew{
    public $field;
    public $relation;
    public $value;
    public function rules()
    {
        return [
            [['id', 'point_id'], 'integer'],
            [['value', 'timestamp'], 'safe'],
        ];
    }
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$point_id,$start,$end)
    {
        if($start==0 && $end==0){
            $query = PointDataNew::find()->
            select(['core.point_data.point_id as point_id','core.point.name as point_name','core.point_data.value as value','core.point_data.timestamp as timestamp'])->
            where('core.point_data.point_id='.$point_id)->
            leftJoin('core.point','core.point.id = core.point_data.point_id')->
            orderBy('core.point_data.timestamp  desc');
        }else{
            $start=$start.' '.'00:00:00';
            $end=$end.' '.'23:59:59';
            $query = PointDataNew::find()->
            select(['core.point_data.point_id as point_id','core.point.name as point_name','core.point_data.value as value','core.point_data.timestamp as timestamp'])->
            where('core.point_data.point_id='.$point_id)->
             andWhere(['between' ,'core.point_data.timestamp',$start,$end])->
            leftJoin('core.point','core.point.id = core.point_data.point_id')->
            orderBy('core.point_data.timestamp  desc');
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
            $field = $this->field[$i];
            $FieldType = $this->tableSchema->columns[$field]->dbType;
            $relation = $this->relation[$i];

            // 制造数组传入模型验证
            $params[$model_name] += [$field => $v];

            if(strpos($relation, 'like') !== false){
                $v = '%'.$v.'%';
            }
            $query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);

        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}