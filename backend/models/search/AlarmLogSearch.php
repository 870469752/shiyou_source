<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AlarmLog;
use common\library\MyFunc;

/**
 * AlarmLogSearch represents the model behind the search form about `backend\models\AlarmLog`.
 */
class AlarmLogSearch extends AlarmLog
{

    public $field;
    public $relation;
    public $value;

    public function rules()
    {
        return [
            [['id', 'alarm_rule_id'], 'integer'],
            [['alarm_level', 'send_status', 'start_time', 'end_time', 'description'], 'safe'],
            [['is_confirmed'], 'boolean'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = AlarmLog::find()->orderby('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        //获取 model中所有字段的类型dbType 及信息
        $columns_db_type = $this->TableSchema->columns;
        $db_full_name = $this->TableSchema->fullName;
        //如果有查询参数 将参数添加到查询语句中
        if(isset($params['columns']) && !empty($params['columns'])) {
            foreach($params['columns'] as $key => $column) {
                if(isset($params[$column]) && $params[$column] != '') {
                    //如果参数条件为数组
                    if(is_array($params[$column])) {
                        //在此的类型 为 int 、 string 、 json
                        foreach($params[$column] as $k => $v) {
                            //如果存在 条件 和 值
                            if(isset($v['condition']) && !empty($v['condition']) && isset($v['value'])) {
                                //如果查询时like 需要做处理
                                if($v['condition'] == 'like') {
                                    //如果是json
                                    if($columns_db_type[$column]->dbType == 'json'){
                                        //由于根据现有条件无法得知当前json类型办法有两种一种手动填写，另外写一个判断方法:如果注释完善可通过注释判断，但目前不考虑.
                                        $json_type = MyFunc::getColumnJsonType($db_full_name . '.' .$column);
                                        foreach($json_type['data'] as $n => $type) {
                                            $query->andWhere($column."->'data'->>'".$type."' " .$v['condition'] ." :col",[':col' => '%'.$v['value'].'%']);
//                                                      : $query->orWhere($column."->'data'->>'".$type."' " .$v['condition'] ." :col",[':col' => '%'.$v['value'].'%']);
                                        }
                                    }else {
                                        $query->andWhere($column.' '.$v['condition']." :col",[':col' => '%'.$v['value'].'%']);
                                    }
                                } else {
                                    //如果是 json
                                    if($columns_db_type[$column]->dbType == 'json'){
                                        $json_type = MyFunc::getColumnJsonType($db_full_name . '.' .$column);
                                        foreach($json_type['data'] as $n => $type) {
                                            $query->andWhere($column."->'data'->>'".$type."' " .$v['condition'] ." :col",[':col' => $v['value']]);
//                                                      : $query->orWhere($column."->'data'->>'".$type."' " .$v['condition'] ." :col",[':col' => $v['value']]);
                                        }
                                    }else {
                                        $query->andWhere($column.' '.$v['condition']." :col",[':col' => $v['value']]);
                                    }
                                }
                            }
                        }
                    }else {
                        //在此的类型 为 timestamp 或 bool
                        echo $params['column'];
                        $query->andWhere([$column => $params[$column]]);
                    }
                }
            }
        }

//        $model_name = MyFunc::getLastName(__CLASS__);
//        $condition = isset($params[$model_name])?$params[$model_name]:'';
//
//        $this->field = isset($condition['field'])?$condition['field']:[];
//        $this->relation = isset($condition['relation'])?$condition['relation']:[];
//        $this->value = isset($condition['value'])?$condition['value']:[];
//
//        foreach ($this->value as $i => $v){
//        	$field = $this->field[$i];
//        	$FieldType = $this->tableSchema->columns[$field]->dbType;
//        	$relation = $this->relation[$i];
//
//        	// 制造数组传入模型验证
//        	$params[$model_name] += [$field => $v];
//
//        	if(strpos($relation, 'like') !== false){
//        		$v = '%'.$v.'%';
//        	}
//        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
//
//        }
//
//        $this->load($params);
//        $this->validate();
//        echo '<pre>';
//        var_dump($query->asArray()->all(),$query->createCommand()->rawSql);die;
        return $dataProvider;
    }
}
