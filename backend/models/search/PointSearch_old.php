<?php

namespace backend\models\search;

use backend\models\Category;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Point;
use common\library\MyFunc;

/**
 * PointSearch represents the model behind the search form about `backend\models\Point`.
 */
class PointSearch extends Point
{
	
    public function rules()
    {
        return parent::rules();
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $pageSize = 10;
        $query = static::baseSelect()->select('')->orderBy(['id' => SORT_DESC])->with(['oneData' => function($query) use($pageSize){
                $query->orderBy(['timestamp' => SORT_DESC])->limit($pageSize);
            }, 'unit', 'category'])->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);

        $this->load($params);
        $this->validate();
        if($this->name){
            $query->andWhere("name->'data'->>'".Yii::$app->language."' like :name",[':name' => '%'.$this->name.'%']);
        }
        $query->andFilterWhere(['id' => Category::findCategoryPoint($this->category_id)]);

        /*$model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];
        foreach ($this->value as $i => $v){
            // 如果是名称的话就搜索相应语言
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
            // 如果是名称的话就搜索相应语言
            $field_name = $field == 'name'?$field."->'data'->>'".Yii::$app->language."'":$field;
            // 增加下标防止替换相同字段
        	$query->andWhere($field_name.' '.$relation." :".$field.$i,[':'.$field.$i => $v]);
        	
        }
        $this->load($params);
        $this->validate();*/

        return $dataProvider;
    }
}
