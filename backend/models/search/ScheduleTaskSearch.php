<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ScheduleTask;
use common\library\MyFunc;
use common\models\User;

/**
 * ScheduleTaskSearch represents the model behind the search form about `backend\models\ScheduleTask`.
 */
class ScheduleTaskSearch extends ScheduleTask
{
	
	public $field;
	public $relation;
	public $value;
    public $name;
	
    public function rules()
    {
        return [
            [['id', 'task_id', 'creator'], 'integer'],
            [['time_mode', 'create_timestamp', 'update_timestamp', 'assign_users', 'assign_roles', 'last_exec_timestamp', 'next_exec_timestamp'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        //获取当前用户的角色
        $usermodel=new User();
        $user_roles=$usermodel->find()->select(['role_id'])->where(['id'=>Yii::$app->user->identity->id])->asArray()->one();
        $user_roles=$user_roles['role_id'];
        $user_roles=json_decode($user_roles,1);
        $user_roles_data_array=$user_roles['data'];
        //查看该用户,该角色授权下的任务
        $role_str='';
        for($i=0;$i<count($user_roles_data_array);$i++)
        {
            $role_str=$role_str.$user_roles_data_array[$i].',';
        }
        $role_str_final='{'.substr($role_str,0,strlen($role_str)-1).'}';
     //   $query = ScheduleTask::findBySql("select * from core.schedule_task where ".Yii::$app->user->identity->id ."= ANY (assign_users)  or ('".$role_str_final." '&& assign_roles =true )")->orderBy("id desc");

        $query=ScheduleTask::find()->where(Yii::$app->user->identity->id ."= ANY (assign_users)  or ('".$role_str_final." '&& assign_roles =true )")->orderBy('id desc');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
        ]);
        $model_name = MyFunc::getLastName(__CLASS__);
        $condition = isset($params[$model_name])?$params[$model_name]:'';
        
        $this->field = isset($condition['field'])?$condition['field']:[];
        $this->relation = isset($condition['relation'])?$condition['relation']:[];
        $this->value = isset($condition['value'])?$condition['value']:[];

        foreach ($this->value as $i => $v){
        	$field = $this->field[$i];
        	$FieldType = $this->tableSchema->columns[$field]->dbType;
        	$relation = $this->relation[$i];
        	
        	// 制造数组传入模型验证
        	$params[$model_name] += [$field => $v];
        	
        	if(strpos($relation, 'like') !== false){
        		$v = '%'.$v.'%';
        	}
        	$query->andWhere($field.' '.$relation." :".$field,[':'.$field => $v]);
        	
        }
        $this->load($params);
        $this->validate();

        return $dataProvider;
    }
}
