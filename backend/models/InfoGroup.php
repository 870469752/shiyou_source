<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.info_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $field
 */
class InfoGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.info_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '信息组名称'),
            'field' => Yii::t('app', '字段名称'),
        ];

    }

    public function getItem()
    {
        return $this->hasMany(InfoItem::className(), ['info_group_id' => 'id']);
    }
}
