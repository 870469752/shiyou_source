<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.alarm_batch".
 *
 * @property integer $id
 * @property string $point_id
 * @property integer $max_value
 * @property integer $min_value
 * @property string $rule
 */
class AlarmBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.alarm_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_id'], 'required'],
            [[ 'rule'], 'string'],
            [['max_value', 'min_value', 'Summer_max_value', 'Summer_min_value', 'Winter_max_value', 'Winter_min_value'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'point_id' => Yii::t('app', 'Point( ID | Chinese name | English name... )'). Yii::t('app', 'prompt: ') .Yii::t('app', 'Press CTRL + a is the selection, press CTRL + d is delete all.'),
            'max_value' => Yii::t('app', 'Max Value'),
            'min_value' => Yii::t('app', 'Min Value'),
            'Summer_max_value' => Yii::t('app', 'The Lowest Value in Summer Condition'),
            'Summer_min_value' => Yii::t('app', 'The Highest Value in Summer Condition'),
            'Winter_max_value' => Yii::t('app', 'The Lowest Value in Winter Condition'),
            'Winter_min_value' => Yii::t('app', 'The Highest Value in Winter Condition'),
            'rule' => Yii::t('app', 'Rule List'),
        ];
    }

    /**/
    public function batchAlarmSave()
    {
        echo '<pre>';
        //根据点位 id生成一个报警原子事件
        foreach($this->point_id as $key => $value)
        {
            $atom_model = new AtomEvent();
            $atom_model -> type = 1;
            $atom_model -> point_id = $value;
            $atom_model -> name = 'Point Id Is '.$value;
            $atom_model -> time_type = 'day';
            $atom_model -> time_range = '2008-09-01 00:00, 2008-09-01 00:00;';
            $atom_model -> trigger = $this->min_value .';'. $this->max_value;
            $atom_model -> Summer_max_value = $this -> Summer_max_value;
            $atom_model -> Summer_min_value = $this -> Summer_min_value;
            $atom_model -> Winter_max_value = $this -> Winter_max_value;
            $atom_model -> Winter_min_value = $this -> Winter_min_value;

            $atom_id =  $atom_model -> saveEvent();
            var_dump($atom_id);die;
        }

    }
}
