<?php

namespace backend\models;

use common\library\MyFunc;
use Yii;

/**
 * This is the model class for table "core.scheduled_task".
 *
 * @property integer $id
 * @property string $type
 * @property string $schedule_mode
 * @property string $args
 */
class ScheduledTask extends \yii\db\ActiveRecord
{
    /*  导出类型  */
    public $export_type;
    /* 导出文件路径   */
    public $export_path;
    /* 导出方式  */
    public $export_method;
    /* 导出点位id列表 */
    public $export_ids;
    /* 导出间隔时间 */
    public $export_interval_time;
    /* 导出统计时间 */
    public $statistic_time;
    /*  导出标题    */
    public $export_title;
    /*  导出水印 */
    public $export_water_mark;
    /*  表格目录  */
    public $export_directory;
    /* 标题序号 */
    public $export_order;
    /* 开始时间  */
    public $export_start;
    /* 结束时间 */
    public $export_end;
    /* 计划任务模式 */
    public $mission_mode;
    /* 数据库备份类型 */
    public $db_backup_type;
    /*  */
    /*   */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.scheduled_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['export_start', 'mission_mode', 'type'], 'required'],
            [['export_type', 'export_method', 'export_path', 'export_title',
                    'export_water_mark', 'export_directory', 'export_order', 'export_start', 'export_end', 'type', 'db_backup_type'], 'string'],
            [['type'], 'string', 'max' => 20],
            [['type'], 'default', 'value' => 'report'],
            [['export_interval_time'], 'default', 'value' => 604800],
            [['statistic_time'], 'default', 'value' => 3600],
            [['export_ids'], 'default' , 'value' => '']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Task Type'),
            'schedule_mode' => Yii::t('app', 'Schedule Mode'),
            'args' => Yii::t('app', 'Args'),
            'export_ids' => Yii::t('app', 'Export Point Ids'),
            'export_start' => Yii::t('app', 'Task Start Time'),
            'export_end' => Yii::t('app', 'Task End Time'),
            'export_type' => Yii::t('app', 'Export Type'),
            'export_method' => Yii::t('app', 'Export Method'),
            'export_interval_time' => Yii::t('app', 'Repeat Interval'),
            'export_title' => Yii::t('app', 'Export Title'),
            'export_water_mark' => Yii::t('app', 'Export Water Mark'),
            'export_path' => Yii::t('app', 'Export Path'),
            'export_directory' => Yii::t('app', 'Report Catalog'),
            'export_order' => Yii::t('app', 'Reports No.'),
            'statistic_time' => Yii::t('app', 'Statistic Time'),
            'mission_mode' => Yii::t('app', 'Mission Mode'),
            'db_backup_type' => Yii::t('app', 'Data Backup Type')
        ];
    }

    public function saveTask()
    {
        //在修改时 判断间隔时间 和 统计时间中是否存在逗号既没有修改，这个字段就不做更新
        if(strrpos($this->export_interval_time, ',') && !$this->isNewRecord)
        {
            $this->export_interval_time = MyFunc::JsonFindValue($this->oldAttributes['schedule_mode'], 'data.interval');
        }
        if(strrpos($this->statistic_time, ',') && !$this->isNewRecord)
        {
            $this->statistic_time = MyFunc::JsonFindValue($this->oldAttributes['args'], 'data.statistic_gap');
        }
        //处理 成schedule_mode -json
        $this->schedule_mode = MyFunc::createJson($this, 'schedule_mode');
        $this->args = MyFunc::createJson($this, 'args_' . $this->type);
        return $this->save();
    }

    public static function getDataBackup()
    {
        return self::find()->where(['type' => 'db_backup'])->one();
    }
}
