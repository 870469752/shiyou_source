<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.control_info".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $related_id
 * @property integer $time_mode
 * @property string $data
 */
class ControlInfo extends \yii\db\ActiveRecord
{
    public $time_mode_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.control_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'related_id', 'data'], 'string'],
            [['type', 'time_mode'], 'required'],
            [['type', 'time_mode'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'related_id' => 'Related ID',
            'time_mode' => 'Time Mode',
            'data' => 'Data',
        ];
    }
}
