<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.event".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property boolean $is_valid
 *
 * @property AlarmRule[] $alarmRules
 */
class AlarmEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type'], 'string'],
            [['is_valid'], 'boolean'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '事件编号'),
            'name' => Yii::t('app', '事件名称'),
            'type' => Yii::t('app', '事件类型'),
            'is_valid' => Yii::t('app', '事件是否有效'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlarmRules()
    {
        return $this->hasMany(AlarmRule::className(), ['event_id' => 'id']);
    }

    /**
     * 获取 所有事件
     */
    public static function getAllEvent()
    {
        return self::find()->where(['is_system' => 'false'])->asArray()->all();
    }

    public static function getAvailableEvent()
    {
        return self::find()->where(['is_valid' => 'true','is_system' => 'false'])->asArray()->all();
    }
}
