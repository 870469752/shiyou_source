<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "core.protocol".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property boolean $is_virtual
 *
 * @property Point[] $points
 */
class Protocol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core.protocol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['is_virtual'], 'boolean'],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'is_virtual' => Yii::t('app', 'Is Virtual'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasMany(Point::className(), ['protocol_id' => 'id']);
    }

    public static function getProtocol()
    {
        return self::find()->where(['is_virtual' => 'false'])->asArray()->all();
    }

    public static function getById($id)
    {
        return self::findOne($id);
    }
}
