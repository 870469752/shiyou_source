<?php

return [
    'Equipments' => '设施',
    'Equipment name' => '设施名',
    'Type' => '型号',
    'Add time' => '添加时间',
    'Description' => '描述',
    'Relation point' => '关联点位',
    'Relation Image Base' => '关联底图',
    'Relation Image Group' => '关联图组',
    'sequence' => '显示顺序',
    'Path' => '缩略图',
    'Address'=>'地理位置',
    'Equal' => '等于',
    'Not Equal' => '不等于',
    'Greater Than' => '大于',
    'Less Than' => '小于',
    'Greater Equal' => '大于等于',
    'Less Equal' => '小于等于',

];