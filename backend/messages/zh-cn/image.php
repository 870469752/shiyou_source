<?php

return [
    'name' => '图片名称',
    'timestamp' => '上传时间',
    'thumb' => '缩略图',
    'upload_image' => '上传图片',
    'Dynamic image' => '动态图片',
    'Static image' => '静态图片',
];