<?php 
return [
	//demo
	'Energy trend' => '能耗趋势',
	'Energy Monitor' => '能耗监测',
	'Formerly situation' => '以往形势',
	'Historical spending' => '历史支出',
	'Real time' => '实时',
	'Electric' => '电',
	'Water' => '水',
	'Gas' => '燃气',
	'CO2' => '二氧化碳',
	'Today' => '今日',
	'This month' => '本月',
	'This year' => '今年',
	'Residue electric' => '剩余电量',
	'Residue gas' => '剩余水量',
	'Residue trend' => '剩余燃气',
	'Residue CO2' => '剩余排放',
	'NumberName' => '编号',
	'RegionName' => '地块',
	'BuildingName' => '楼宇',
	'FloorName' => '楼层',

	'Area Overview' => '区域总览',
	'Name' => '名称',
	'Total' => '合计',
];
?>