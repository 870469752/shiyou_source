<?php

    return [
        'Level' => '报警等级',
        'Upgrade Delay' => '升级延迟时间',
        'Mail Receivers' => '邮件接收者',
        'Sms Receivers' => '短信接收者列表',
        'Log Setting' => '是否日志记录',
        'Upload Setting' => '是否上传',
        'Mail Setting' => '是否发送邮件',
        'Sms Setting' => '是否发送短信',
        'Description' => '报警日志',
        'System Log' => '系统日志',
        'Remark' => '备注',
        'Notice Setting' => '是否弹出警报框',
        'Notice Style' => '警报框样式',
        'Notice Siren' => '警报框显示时常',
        'Notice Fade' => '警报提示音',
    ];