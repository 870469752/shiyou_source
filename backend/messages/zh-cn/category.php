<?php

return [
    'Restore Modify' => '还原修改',
    'Expand All' => '全部展开',
    'Collapse All' => '全部合并',
    'Add Category' => '添加种类',
    'Save Modify' => '保存修改',
];