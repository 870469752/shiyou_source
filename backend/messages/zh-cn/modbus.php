<?php

return [
    'Type' => '类型',
    'Rtu Port' => '设备端口',
    'Rtu Baud Rate' => '波特率',
    'Rtu Parity' => '奇偶校验',
    'Rtu Data Bits' => '数据位',
    'Rtu Stop Bits' => '截至位',
    'Tcp Port' => 'TCP端口',
    'Slave ID' =>'Slave ID',
    'Config ID'=>'Conn ID',
    'address' => '地址',
    'length' => '长度',
    'controller_id' => '设备ID',
    'config_id' => 'Conn_ID',
    'ratio' => '比率',
    'type' => '类型',
    'fcu' => '风机盘管',
];