<?php
/**
 * Created by PhpStorm.
 * User: cre
 * Date: 15-5-12
 * Time: 下午4:47
 */
return [
    'ID' => '编号',
    'Name' => '报表名称',
    'Timestamp' => '创建时间',
    'Point Info' => '关联点位信息',
    'User ID' => '用户名',
    'Create {modelClass}' => '创建自定义报表',
    'Update {modelClass}' => '更新自定义报表',
    'Left Axis Point' => '左轴点位',
    'Right Axis Point' => '右轴点位'
];