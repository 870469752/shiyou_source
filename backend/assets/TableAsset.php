<?php
/**
 * @link http://www.gcxj.com/
 * @copyright Copyright (c) 2014 gcxj
 * @license http://www.gcxj.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author 葛盛庭 <ducooo@163.com>
 * @since 1.0
 */
class TableAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/datatables/jquery.dataTables.min.css",
        "css/datatables/buttons.dataTables.min.css",
        "css/datatables/select.dataTables.min.css",
        "css/datatables/editor.dataTables.min.css",
    ];
    public $js = [
        //"js/plugin/datatables/jquery-1.11.3.min.js",
    	'js/plugin/datatables/jquery.dataTables.min.js',
        //'js/plugin/datatables/dataTables.buttons.min.js',
        //'js/plugin/datatables/dataTables.select.min.js',
        //'js/plugin/datatables/dataTables.editor.js',
    	'js/plugin/datatables/dataTables.colVis.min.js',
    	'js/plugin/datatables/dataTables.tableTools.min.js',
    	'js/plugin/datatables/dataTables.bootstrap.min.js',
    	'js/plugin/datatable-responsive/datatables.responsive.min.js',

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
