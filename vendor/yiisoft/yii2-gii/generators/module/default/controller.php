<?php
/**
 * This is the template for generating a controller class within a module.
 *
 * @var yii\web\View $this
 * @var yii\gii\generators\module\Generator $generator
 */
echo "<?php\n";
?>

namespace <?= $generator->getControllerNamespace() ?>;

use backend\controllers\MyController;

class DefaultController extends MyController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
