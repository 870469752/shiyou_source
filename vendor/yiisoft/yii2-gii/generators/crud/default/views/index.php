<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var yii\gii\generators\crud\Generator $generator
 */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
use Yii\web\View;
use common\library\MyFunc;
use backend\assets\TableAsset;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var <?= ltrim($generator->searchModelClass, '\\') ?> $searchModel
 */
 
TableAsset::register ( $this );
$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
	<!-- row -->
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- 不写ID不会应用本地变量，也就是说不会保存修改的颜色标题等。 -->
			<div class="jarviswidget jarviswidget-color-blueDark" 
			data-widget-deletebutton="false" 
			data-widget-editbutton="false"
			data-widget-colorbutton="false"
			data-widget-sortable="false">
			<header>
				<span class="widget-icon">
					<i class="fa fa-table"></i>
				</span>
				<h2><?= "<?= " ?>Html::encode($this->title) ?></h2>
			</header>
			<!-- widget div-->
			<div>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
    	'formatter' => ['class' => 'common\library\MyFormatter'],
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'widget-body no-padding'],
    	'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
    		'width' => '100%',
    		'id' => 'datatable'
    	],
    	'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['data-hide' => 'phone']],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
		if($name == 'id'){
			continue;
		}
		$count++;
        if ($count == 2) {
            echo "            ['attribute' => '".$name."', 'headerOptions' => ['data-hide' => 'phone']],\n";
        } elseif($count > 2) {
        	echo "            ['attribute' => '".$name."', 'headerOptions' => ['data-hide' => 'phone,tablet']],\n";
        } else {
            echo "            '" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if($format !== 'text'){
        	$format = ", 'format' => '".$format."'";
        }
        else {
        	$format = '';
        }
        if($column->name == 'id'){
        	continue;
        }
        $count++;
        if ($count == 2) {
        	echo "            ['attribute' => '".$column->name."'".$format.", 'headerOptions' => ['data-hide' => 'phone']],\n";
        } elseif($count > 2) {
        	echo "            ['attribute' => '".$column->name."'".$format.", 'headerOptions' => ['data-hide' => 'phone,tablet']],\n";
        } else {
        	echo "            ['attribute' => '".$column->name."'".$format."],\n";
        }
    }
}
?>

            ['class' => 'yii\grid\ActionColumn', 'headerOptions' => ['data-class' => 'expand']],
        ],
    ]); ?>

<?= "<?php\n" ?>
// 搜索和创建按钮
$search_button = '<button class="btn btn-default" data-toggle="modal" data-target="#myModal">'.YII::t('app', 'Search').'</button>';
$create_button = Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']);

// 添加两个按钮和初始化表格
$js_content = <<<JAVASCRIPT
var options = {"button":[]};
options.button.push('{$search_button}');
options.button.push('{$create_button}');
table_config('datatable', options);						
JAVASCRIPT;

$this->registerJs ( $js_content, View::POS_READY);
?>
<!-- 搜索表单和JS --> 
<?= '<?php' ?> MyFunc::TableSearch($searchModel) ?>

<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'summaryOptions' => ['class' => 'col-sm-6 col-xs-12 hidden-xs dataTables_info'],
    	'layout' => "{items}<div class='dt-toolbar-footer'>{summary}<div class='col-sm-6 col-xs-12'>{pager}</div></div>",
        'itemView' => function ($model, $key, $index, $widget) {
        	$left = Html::tag('div', Html::encode($model-><?= $nameAttribute ?>), ['class' => 'col-sm-6']);
        	$right = Html::tag('div', Html::encode($model-><?= $nameAttribute ?>), ['class' => 'col-sm-6 text-right']);;
        	$a = Html::a($left.$right, ['view', <?= $urlParams ?>], ['class' => 'row']);
            return $a;
        },
    ]) ?>
<?php endif; ?>
    			</div>
			</div>
		</article>
	</div>
</section>
